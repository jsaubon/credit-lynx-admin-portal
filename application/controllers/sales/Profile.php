<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller { 
	
	public function index()
	{
		$this->checklog(); 
		$userdata['userdata'] = $this->session->userdata('user_data'); 
		$data['state_provinces'] = $this->getStateProvinces(); 
		$data['page_title'] = 'Profile';
		$data['page_title_s'] = 'profile';
		$data['page_controller'] = 'profile';  
		$data['page_folder'] = 'sales';
		$this->load->view('sales/includes/header.php',$userdata);
		$this->load->view('sales/includes/nav-left.php',$userdata);
		$this->load->view('sales/includes/nav-top.php',$userdata);
		$this->load->view('sales/Profile.php',$data);
		$this->load->view('sales/includes/footer.php',$userdata);
	}  

	function uploadProfilePicture() {
		$sale_id = $this->input->post('sale_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/sales/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $data = $this->db->query("UPDATE sales SET photo='$photo' WHERE sale_id=$sale_id");
	}





	function getDetails() {
		$sale_id = $this->input->post('sale_id');
		$data = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM sales WHERE sale_id = $sale_id");
		echo json_encode($data);
	}

	function saveTask() {
		$sale_id = $this->input->post('sale_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Sales Team';
		$tasks->task_assigned_id = $sale_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$sale_id = $this->input->post('sale_id');
		$data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$sale_id AND task_assigned='Sales Team' AND task_active <> 2 ORDER BY task_id DESC");
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveNote() {
		$sale_id = $this->input->post('sale_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Sales Team';
		$notes->note_assigned_id = $sale_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$sale_id = $this->input->post('sale_id');
		$data = $this->fetchRawData("SELECT * FROM notes WHERE note_assigned_id=$sale_id AND note_assigned='Sales Team' ORDER BY note_id DESC");
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$data = $this->db->query("UPDATE notes SET note_sticky=$note_sticky WHERE note_id=$note_id");
		echo "updated";
	}

	function getMyTimeWorked() {
		$userdata = $this->session->userdata('user_data');
		$worker_id = $userdata['user_id'];
		$curdate = date('Y-m-d H:i:s');
		$data = $this->fetchRawData("
									SELECT hourly,`name`
										,
										photo,
										sales_time_worked.* 
									FROM
										sales
										INNER JOIN ( SELECT * FROM (SELECT HOUR(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `hours` ,  MINUTE(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `minutes`, DATE(start_time) as `work_date`, work_hours.* FROM work_hours GROUP BY worker,worker_id,DATE(start_time) ) as `view_employees_time_worked` WHERE worker = 'Sales Team' AND worker_id=$worker_id ) AS sales_time_worked ON sales.sale_id = sales_time_worked.worker_id");
		echo json_encode($data);
	}
	
}
