<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller { 
	
	public function index()
	{
		$this->checklog(); 
		$userdata['userdata'] = $this->session->userdata('user_data');
		$this->load->view('sales/includes/header.php',$userdata);
		$this->load->view('sales/includes/nav-left.php',$userdata);
		$this->load->view('sales/includes/nav-top.php',$userdata);
		$this->load->view('sales/Dashboard.php');
		$this->load->view('sales/includes/footer.php',$userdata);
	} 
	
}
