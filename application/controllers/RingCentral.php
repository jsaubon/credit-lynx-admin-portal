<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'ringcentral/vendor/autoload.php';
use RingCentral\SDK\SDK;

class RingCentral extends MY_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function createRCWebhook() {
		// echo "string";
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$token = $platform->auth()->accessToken();
		// echo $token;
		// echo 'Creating...';
		// create webhook_sms
		$apiResponseCreate = $rcsdk->platform()->post('/subscription', array(
			'eventFilters' => array(
				'/restapi/v1.0/account/~/extension/~/message-store/instant?type=SMS',
			),
			'deliveryMode' => array(
				'transportType' => 'WebHook',
				'address' => 'https://admin.creditlynx.com/ringCentral/webhook_sms?auth_token=' . $token,
			),
		));
		$respCreate = $apiResponseCreate->json();
		$respCreate = json_decode(json_encode($apiResponseCreate->json()), TRUE);
		$this->pprint($respCreate);
		// echo 'Done Create webhook_sms. Ready to receive sms';
		// echo '<br>';
	}

	function createRCWebhookCall() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$token = $platform->auth()->accessToken();
		// echo 'Creating...';
		// create webhook_sms
		$apiResponseCreate = $rcsdk->platform()->post('/subscription', array(
			'eventFilters' => array(
				'/restapi/v1.0/account/~/extension/~/presence?detailedTelephonyState=true',
			),
			'deliveryMode' => array(
				'transportType' => 'WebHook',
				'address' => 'https://admin.creditlynx.com/ringCentral/webhook_call?auth_token=' . $token,
			),
		));
		$respCreate = $apiResponseCreate->json();
		$respCreate = json_decode(json_encode($apiResponseCreate->json()), TRUE);
		$this->pprint($respCreate);
		// echo 'Done Create webhook_sms. Ready to receive sms';
		// echo '<br>';
	}

	function renewRCWebhook() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}
		$apiResponseGet = $rcsdk->platform()->get('/subscription');
		$response = $apiResponseGet->json();
		$response = json_decode(json_encode($response), TRUE);
		// $this->pprint($response);;
		if ($response['records'] == []) {
			// echo "create";
			$this->createRCWebhook();
		} else {
			foreach ($response['records'] as $key => $value) {
				if ($value['status'] != 'Blacklisted') {
					$apiResponseRenew = $rcsdk->platform()->post('/subscription/' . $value['id'] . '/renew');
					$responseRenew = $apiResponseRenew->json();
					$this->pprint($responseRenew);
				} else {
					$apiResponseDelete = $rcsdk->platform()->delete('/subscription/' . $value['id']);
					$this->createRCWebhook();
				}

			}

		}

	}

	function deleteWebhooks() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}
		$apiResponseGet = $rcsdk->platform()->get('/subscription');
		$response = $apiResponseGet->json();
		$response = json_decode(json_encode($response), TRUE);
		// $this->pprint($response);;
		if ($response['records'] == []) {
			// echo "create";
			$this->createRCWebhook();
		} else {
			foreach ($response['records'] as $key => $value) {
				$apiResponseDelete = $rcsdk->platform()->delete('/subscription/' . $value['id']);
			}

		}
	}

	function webhook_sms() {
		$v = isset($_SERVER['HTTP_VALIDATION_TOKEN']) ? $_SERVER['HTTP_VALIDATION_TOKEN'] : '';

		if (strlen($v) > 0) {
			header("Validation-Token: {$v}");
		}
		$data_ = file_get_contents('php://input');

		$this->db->query("INSERT INTO webhook (`response`) VALUES ('$data_')");
		if ($data_ != '') {
			$this->db->query("INSERT INTO webhook (`response`) VALUES ('insert_$data_')");
			// $data_ = $this->input->post('value');

			$data = json_decode($data_, TRUE);
			$from = $data['body']['from']['phoneNumber'];
			$is_mms = $this->getAttachmentFromRingCentral($data);
			if ($is_mms != false) {
				$message = $is_mms;
				$title = 'mms';
			} else {
				$message = $data['body']['subject'];
				$title = '';
			}

			$client_id = $this->getClientIDByPhone($from);
			$client_type = 'client';
			if ($client_id == false) {
				$client_id = $this->getClientJointIDByPhone($from);
				$client_type = 'joint';
			}

			if ($client_id != 0) {
				$this->load->model('Model_client_etexts');
				$client_etexts = new Model_client_etexts();
				$client_etexts->client_id = $client_id;
				$client_etexts->client_type = $client_type;
				$client_etexts->type = 'from_client';
				$client_etexts->title = $title;
				$client_etexts->text_message = $message;
				$client_etexts->date = date('Y-m-d H:i:s');
				$client_etexts->phone_number = $from;
				$client_etexts->save();
			} else {
				$broker_id = $this->getBrokerIDByPhone($from);
				if ($broker_id != 0) {
					$this->load->model('Model_broker_etexts');
					$broker_etexts = new Model_broker_etexts();
					$broker_etexts->broker_id = $broker_id;
					$broker_etexts->type = 'from_client';
					$broker_etexts->title = $title;
					$broker_etexts->text_message = $message;
					$broker_etexts->date = date('Y-m-d H:i:s');
					$broker_etexts->phone_number = $from;
					$broker_etexts->save();
				} else {
					// $agent = $this->getAgentIDByPhone($from);
					// if ($agent != 0) {
					// 	$this->load->model('Model_broker_etexts');
					//        $broker_etexts = new Model_broker_etexts();
					//        $broker_etexts->agent = $agent;
					//        $broker_etexts->type = 'from_client';
					//        $broker_etexts->title = '';
					//        $broker_etexts->text_message = $message;
					//        $broker_etexts->date = date('Y-m-d H:i:s');
					//        $broker_etexts->save();
					// }
				}
			}
		} else {
			$this->db->query("INSERT INTO webhook (`response`) VALUES ('create_$data_')");
		}

	}

	function testing123() {
		$data_ = '{"uuid":"5816104641488919052","event":"/restapi/v1.0/account/62428299006/extension/62428299006/message-store/instant?type=SMS","timestamp":"2019-04-08T19:33:12.679Z","subscriptionId":"4ade379f-f2c7-4a57-b0f5-861939a76496","ownerId":"62428299006","body":{"id":"1114505949006","to":[{"phoneNumber":"+15138780181","name":"Brent Baumann","location":"Cincinnati, OH"}],"from":{"phoneNumber":"+18134409146","location":"Tampa Central, FL"},"type":"SMS","creationTime":"2019-04-08T19:33:12.441Z","lastModifiedTime":"2019-04-08T19:33:12.441Z","readStatus":"Unread","priority":"Normal","attachments":[{"id":"1114505949006","type":"Text","contentType":"text/plain"}],"direction":"Inbound","availability":"Alive","subject":"Replied","messageStatus":"Received","conversation":{"id":"8618018671185819129"}}}';
		$data = json_decode($data_, TRUE);
		$from = $data['body']['from']['phoneNumber'];
		$is_mms = $this->getAttachmentFromRingCentral($data);
		if ($is_mms != false) {
			$message = $is_mms;
			$title = 'mms';
		} else {
			$message = $data['body']['subject'];
			$title = '';
		}

		$client_id = $this->getClientIDByPhone($from);
		echo $client_id;
		echo "<br>";
		echo $from;
		if ($client_id != 0) {
			$this->load->model('Model_client_etexts');
			$client_etexts = new Model_client_etexts();
			$client_etexts->client_id = $client_id;
			$client_etexts->type = 'from_client';
			$client_etexts->title = $title;
			$client_etexts->text_message = $message;
			$client_etexts->date = date('Y-m-d H:i:s');
			$client_etexts->save();
		} else {
			$broker_id = $this->getBrokerIDByPhone($from);
			if ($broker_id != 0) {
				$this->load->model('Model_broker_etexts');
				$broker_etexts = new Model_broker_etexts();
				$broker_etexts->broker_id = $broker_id;
				$broker_etexts->type = 'from_client';
				$broker_etexts->title = $title;
				$broker_etexts->text_message = $message;
				$broker_etexts->date = date('Y-m-d H:i:s');
				$broker_etexts->save();
			} else {
				// $agent = $this->getAgentIDByPhone($from);
				// if ($agent != 0) {
				// 	$this->load->model('Model_broker_etexts');
				//        $broker_etexts = new Model_broker_etexts();
				//        $broker_etexts->agent = $agent;
				//        $broker_etexts->type = 'from_client';
				//        $broker_etexts->title = '';
				//        $broker_etexts->text_message = $message;
				//        $broker_etexts->date = date('Y-m-d H:i:s');
				//        $broker_etexts->save();
				// }
			}
		}
	}
	function webhook_call() {
		$v = isset($_SERVER['HTTP_VALIDATION_TOKEN']) ? $_SERVER['HTTP_VALIDATION_TOKEN'] : '';

		if (strlen($v) > 0) {
			header("Validation-Token: {$v}");
		}
		$data_ = file_get_contents('php://input');

		// $data = json_decode($data_, TRUE);
		// $from = $data['body']['from']['phoneNumber'];
		// $message = $data['body']['subject'];
		// $this->db->query("INSERT INTO webhook (`response`) VALUES ('$data')");

		// $client_id = $this->getClientIDByPhone($from);
		// $this->load->model('Model_client_etexts');
		// $client_etexts = new Model_client_etexts();
		// $client_etexts->client_id = $client_id;
		// $client_etexts->type = 'to_client';
		// $client_etexts->title = '';
		// $client_etexts->text_message = $message;
		// $client_etexts->date = date('Y-m-d H:i:s');
		// $client_etexts->save();

	}

	function test123() {
		echo $this->getClientIDByPhone('18134409146');
	}

	function getClientIDByPhone($cell_phone_trimmed) {
		$cell_phone_trimmed = str_replace(' ', '', $cell_phone_trimmed);
		// if (strpos($cell_phone_trimmed, '+') == false) {
		// 	$cell_phone_trimmed = "+" . $cell_phone_trimmed;
		// }
		$cell_phone_trimmed = str_replace('+1', '', $cell_phone_trimmed);
		$data = $this->fetchRawData("SELECT client_id FROM view_clients_table WHERE cell_phone_trimmed='$cell_phone_trimmed'");
		if (count($data) > 0) {
			return $data[0]['client_id'];
		} else {
			return false;
		}
	}
	function getClientJointIDByPhone($cell_phone_trimmed) {
		$cell_phone_trimmed = str_replace(' ', '', $cell_phone_trimmed);
		// if (strpos($cell_phone_trimmed, '+') == false) {
		// 	$cell_phone_trimmed = "+" . $cell_phone_trimmed;
		// }
		$cell_phone_trimmed = str_replace('+1', '', $cell_phone_trimmed);
		$data = $this->fetchRawData("SELECT client_joint_id FROM view_clients_table WHERE joint_cell_phone_trimmed='$cell_phone_trimmed'");
		if (count($data) > 0) {
			return $data[0]['client_joint_id'];
		} else {
			return false;
		}
	}

	function getBrokerIDByPhone($cell_phone_trimmed) {
		$cell_phone_trimmed = str_replace(' ', '', $cell_phone_trimmed);
		// if (strpos($cell_phone_trimmed, '+') == false) {
		// 	$cell_phone_trimmed = "+" . $cell_phone_trimmed;
		// }

		$cell_phone_trimmed = str_replace('+1', '', $cell_phone_trimmed);
		$data = $this->fetchRawData("SELECT broker_id FROM brokers WHERE REPLACE(cell,'-','')='$cell_phone_trimmed'");
		if (count($data) > 0) {
			return $data[0]['broker_id'];
		} else {
			return false;
		}

	}

	function getRCExtensions() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$extensions = $platform->get('/account/~/extension', array('perPage' => 10))->json()->records;
		echo 'Users loaded ' . count($extensions);
		// Load presence
		$this->pprint($extensions);
		$presences = $platform->get('/account/~/extension/' . $extensions[0]->id . ',' . $extensions[0]->id . '/presence')
			->multipart();
		echo 'Presence loaded ' .
		$extensions[0]->name . ' - ' . $presences[0]->json()->presenceStatus . ', ' .
		$extensions[0]->name . ' - ' . $presences[1]->json()->presenceStatus;
	}

	function getCallLogs() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$callLogRecords = $platform->get('/account/~/extension/~/call-log', array(
			'dateFrom' => $credentials['dateFrom']))
			->json();

		$response = json_decode(json_encode($callLogRecords->records), TRUE);

		$filterBy = '+18139517686'; // or Finance etc.

		$new_arr = array_filter($response, function ($var) use ($filterBy) {
			if ($var['direction'] == 'Inbound') {
				return ($var['from']['phoneNumber'] == $filterBy);
			} else {
				return ($var['to']['phoneNumber'] == $filterBy);
			}
		});
		$this->pprint($new_arr);
		// foreach ($new as $key => $call) {
		// 	$to = $call['to']['phoneNumber'];
		// 	$from = $call['from']['phoneNumber'];
		// 	$duration = $call['duration'];
		// 	$startTime = date('Y-m-d H:i:s',strtotime($call['startTime']));
		// 	$direction = $call['direction'];
		// 	$result = $call['result'];
		// 	if ($direction == 'Inbound') {
		// 		$client_number = $from;
		// 	} else {
		// 		$client_number = $to;
		// 	}

		// 	$client_id = $this->getClientIDByPhone($client_number);
		// 	if ($client_id != false) {
		// 		echo $client_id;
		//  	echo "<br>";
		//  	echo $direction;
		//  	echo "<br>";
		//  	echo "CALLING :";
		//  	echo $client_number;
		//  	echo "<br>";
		//  	echo "Status :";
		//  	echo $result;
		//  	echo "<br>";
		//  	echo "Time :";
		//  	echo $startTime;
		//  	echo "<br>";
		//  	echo "Length :";
		//  	echo $duration;
		//  	echo "<br>";
		//  	echo "<br>";
		// 	}

		// }

		// $res = $platform->get('account/~/extension/~/phone-number')->json();
		// $this->pprint($res);
	}

	function testting() {
		$is_https = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? true : false;
		if ($is_https) {
			echo $_SERVER['request_uri'];
		} else {
			// redirect
		}
	}

	function ringOut() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$response = $platform->post('/account/~/extension/~/ringout', array(
			'from' => array('phoneNumber' => '+18134409146'),
			'to' => array('phoneNumber' => '+18139517686'),
		));
		$json = $response->json();
		$lastStatus = $json->status->callStatus;
		// Poll for call status updates
		while ($lastStatus == 'InProgress') {
			$current = $platform->get($json->uri);
			$currentJson = $current->json();
			$lastStatus = $currentJson->status->callStatus;
			echo 'Status: ' . json_encode($currentJson->status) . PHP_EOL;
			sleep(2);
		}
	}

	function sendRCMmsTest() {

		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		// $response = $platform
		// 	->post('/account/~/extension/~/sms', array(
		// 		'from' => array('phoneNumber' => $credentials['smsNumber']),
		// 		'to' => array(
		// 			array('phoneNumber' => '8134409146'),
		// 		),
		// 		'text' => 'test from sandbox app RC API - joshua please reply',
		// 	))->json();

		$response = $rcsdk->createMultipartBuilder()
			->setBody(array(
				'from' => array('phoneNumber' => $credentials['smsNumber']),
				'to' => array(
					array('phoneNumber' => '8134409146'),
				),
			))
			->add(fopen('http://sandbox.creditlynx.com/assets/images/LynxImage.png', 'r'))
			->request('/account/~/extension/~/sms');
		//print $request->getBody() . PHP_EOL;
		$response = $platform->sendRequest($response)->json();
		$this->pprint($response);

	}

	function testing() {
		$data_ = '{"uuid":"5768785063667364879","event":"/restapi/v1.0/account/62428299006/extension/62428299006/message-store/instant?type=SMS","timestamp":"2019-03-20T18:33:46.216Z","subscriptionId":"4d847011-dce0-4b59-bc06-896b8ea01d86","ownerId":"62428299006","body":{"id":"1103720529007","to":[{"phoneNumber":" 15138780181","name":"Brent Baumann","location":"Cincinnati, OH"}],"from":{"phoneNumber":" 18134409146","location":"Tampa Central, FL"},"type":"SMS","creationTime":"2019-03-20T18:33:46.199Z","lastModifiedTime":"2019-03-20T18:33:46.199Z","readStatus":"Unread","priority":"Normal","attachments":[{"id":"1103720529007","type":"Text","contentType":"text/plain"},{"id":"201148540007","type":"MmsAttachment","uri":"https://media.ringcentral.com/restapi/v1.0/account/62428299006/extension/62428299006/message-store/1103720529007/content/201148540007","contentType":"image/jpeg","size":318635}],"direction":"Inbound","availability":"Alive","subject":" ","messageStatus":"Received","conversation":{"id":"8618018671185819129"}}}';

		$data = json_decode($data_, TRUE);
		// $message_id = $data['body']['id'];
		// // if (isset($data['body']['attachments'][1])) {
		// $attachment_id = $data['body']['attachments'][1]['id'];

		// // echo $message_id;
		// // echo "<br>";
		// // echo $attachment_id;
		// $this->pprint($data);
		// // }

		echo $this->getAttachmentFromRingCentral($data);

	}

	function getAttachmentFromRingCentral($data) {
		if (isset($data['body']['attachments'][1])) {
			$uri = $data['body']['attachments'][1]['uri'];
			$message_id = $data['body']['id'];
			$attachment_id = $data['body']['attachments'][1]['id'];

			$credentials = $this->rcCredentialsProduction();
			$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
			$platform = $rcsdk->platform();
			// Authorize
			if (!$rcsdk->platform()->loggedIn()) {
				$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
			}
			$response = $platform
				->get($uri);
			$ext = $response->response()->getHeader('Content-Type')[0];
			$filename = str_replace('inline;filename=', '', $response->response()->getHeader('Content-Disposition')[0]);

			$file_path = 'assets/mms/attachments/' . $filename;
			file_put_contents($file_path, $response->raw());
			return base_url($file_path);
		} else {
			return false;
		}

	}

}
