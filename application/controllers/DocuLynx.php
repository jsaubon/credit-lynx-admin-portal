<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'PdfToImage/pdf.php';
use Spatie\PdfToImage;

class DocuLynx extends MY_Controller {

	public function index() {
		$this->checklog();
		$data['view'] = $this->input->get('view');

		$data['userdata'] = $this->session->userdata('user_data');
		$data['leads'] = $this->getLeads();
		$data['joint_leads'] = $this->getJointLeads();
		$data['templates'] = $this->getTemplates();
		$data['template_sent'] = $this->getTemplateSent($data['view']);
		$data['template_email_settings'] = $this->getTemplateEmailSettings();
		$this->load->view('admin/DocuLynx.php', $data);
	}

	function getTemplateEmailSettings() {
		$data = $this->fetchRawData("SELECT * FROM template_email_settings ");
		return $data;
	}

	function getLeads() {
		$data = $this->fetchRawData("SELECT name,email_address,client_id FROM view_clients_table   UNION ALL  SELECT joint_name as `name`,joint_email_address as `email_address`,client_joint_id FROM view_clients_table ORDER BY name ASC ");
		return $data;
	}

	function getJointLeads() {
		$data = $this->fetchRawData("SELECT joint_name as `name`,joint_email_address as `email_address`,client_joint_id FROM view_clients_table ORDER BY joint_name ASC ");
		return $data;
	}

	function deleteTemplate() {
		$template_id = $this->input->post('template_id');

		$delete = $this->db->query("DELETE FROM templates WHERE template_id=$template_id");
		$delete = $this->db->query("DELETE FROM template_images WHERE template_id=$template_id");
		// $delete = $this->db->query("DELETE FROM template_fields WHERE template_id=$template_id");
	}


	function testing321(){
		echo $this->get_encrypted_string('https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=88&id1=33&id2=&name1=Emma+Shepherd&email1=joshuasaubon@gmail.com&name2=&email2=&subject=Credit+Lynx+Service+Agreement&message=Please+review+and+sign..&sent_id=1513');
	}


	function test_decrypt() {
		$temp = 'BDBXNQsoADtROQw8BjpcIVJjU21adQd1BScEMFc+A3IAYFZqU3NZOQUPBW1RYlA/U2wIblN0DGsFNQRlUTdRNgFkCX5WOwJnBzlRMQV3WTIEMFc7CzkAY1FrDBcGalw/UmdTKFpSBzsFNgQlVzsDZwB+Vm9TIVk5BT0FZVFvUG5TZQhrUzgMbQUiBDxRf1FkASQJOVYnAmEHZFFiBRFZOwQ8VzcLNQA+UXgMMQZoXD9SIFNtWmAHPgU2BGdXbgMkAGlWZlNmWTUFPAU2UTtQJFMnCCNTMAxoBTQEN1F+UTgBFAkqVjcCZwdiUXgFelkQBChXOAskAHlRBQw3BnVcJFJvU2BaZAd4BRIEMlchA2cAaVZmU2JZMgUkBSJRa1BnUycIJVMzDGUFNARpUVpRaQEyCTlWIQJmByBRfgU0WSoEOFczCysAeVE3DDwGY1x5UnVTalpmBz0FfQR7V3UDcQBpVmVTc1kDBTkFYFE7UDNTYQhnU2Q=';

		$this->pprint($this->get_decrypted_string($temp));
	}

	function get_temp_array($temp) {
		$temp =  $this->get_decrypted_string($temp);
		$temp = explode('&', $temp);
		// $this->pprint($temp);
		$arr_ = array();
		foreach ($temp as $key => $value) {
			$val = explode('=', $value);
			$arr_[$val[0]] = !empty($val[1]) ? str_replace('+', ' ', $val[1]) : '';
		}

		return $arr_;
	}

	function template() {
		$temp = $this->input->get('temp');
		// echo $temp;
		if ($temp != '') {
			$data = $this->get_temp_array($temp);

			$template_id = $data['template_id'] ? $data['template_id'] : '';
			$id1 = $data['id1'] ? $data['id1'] : '';
			$id2 = $data['id2'] ? $data['id2'] : '';
			$name1 = $data['name1'] ? $data['name1'] : '';
			$email1 = $data['email1'] ? $data['email1'] : '';
			$name2 = $data['name2'] ? $data['name2'] : '';
			$email2 = $data['email2'] ? $data['email2'] : '';
			$subject = $data['subject'] ? $data['subject'] : '';
			$message = $data['message'] ? $data['message'] : '';
			$action = $data['action'] ? $data['action'] : '';
			$sent_id = $data['sent_id'] ? $data['sent_id'] : '';
			$viewer = !empty($data['viewer']) ? $data['viewer'] : '';
			$view = !empty($data['view']) ? $data['view'] : '';
			$data['template_id'] = $template_id;
			$data['id1'] = $id1;
			$data['id2'] = $id2;
			$data['name1'] = $name1;
			$data['email1'] = $email1;
			$data['name2'] = $name2;
			$data['email2'] = $email2;
			$data['subject'] = $subject;
			$data['message'] = $message;
			$data['action'] = $action;
			$data['sent_id'] = $sent_id;
			$data['view'] = $view;
			$data['viewer'] = $viewer;

			$data['template_images'] = $this->getTemplateImages($template_id);
			if ($sent_id == '') {
				$userdata['userdata'] = $this->session->userdata('user_data');
				$this->load->view('admin/includes/header.php', $userdata);
				// $this->load->view('admin/includes/nav-left.php',$userdata);
				$this->load->view('admin/includes/nav-top.php', $userdata);
				$this->load->view('admin/doculynx/Template.php', $data);
				$this->load->view('admin/includes/footer.php', $userdata);
			} else {
				$data['status'] = $this->getSentStatus($sent_id);
				$status = $data['status'];
				if ($status != 'voided') {
					if ($status != 'Completed') {

						$template_email_settings = $this->getTemplateEmailSettings();
						if ($template_email_settings[0]['for_sender_view_envelope'] == 'on') {
							$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href FROM template_sent WHERE sent_id=$sent_id");
							$template_name = $data_sent[0]['template_name'];
							$href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;

							if ($viewer == 'name1') {
								$subject = $name1 . ' viewed ' . $template_name;
								$message = 'at ' . date('m/d/Y h:i:s A') . ', ' . $name1 . ' opened and viewed your documents, ' . $template_name;
								$this->sendDocuLynxEmailViewed('docs@creditlynx.com', $href, $subject, $message);
								$client_id = $this->getClientIDByName($name1); 
								$this->load->model('Model_notes');
								$notes = new Model_notes();
			 
								$notes->note_assigned = 'Clients';
								$notes->note_assigned_id = $client_id;
								
								$notes->sender = 'Credit Lynx';
								$notes->sender_id = '0';
								$notes->sender_photo = 'LynxLogo.png';
								$notes->sender_name = 'Credit Lynx';
								$notes->note = $message;
								$notes->note_date = date('Y-m-d H:i:s');
								$notes->note_sticky = 0;
								$notes->save();
							}
							if ($viewer == 'name2') {
								$subject = $name2 . ' viewed ' . $template_name;
								$message = 'at ' . date('m/d/Y h:i:s A') . ', ' . $name2 . ' opened and viewed your documents, ' . $template_name;
								$this->sendDocuLynxEmailViewed('docs@creditlynx.com', $href, $subject, $message);
								$client_joint_id = $this->getClientJointIDByName($name2); 
								$this->load->model('Model_notes');
								$notes = new Model_notes();
			 
								$notes->note_assigned = 'Client Joints';
								$notes->note_assigned_id = $client_joint_id;
								
								$notes->sender = 'Credit Lynx';
								$notes->sender_id = '0';
								$notes->sender_photo = 'LynxLogo.png';
								$notes->sender_name = 'Credit Lynx';
								$notes->note = $message;
								$notes->note_date = date('Y-m-d H:i:s');
								$notes->note_sticky = 0;
								$notes->save();
							}
						}

						$date_lc = date('Y-m-d H:i:s');
						$data_update = $this->db->query("UPDATE template_sent SET status='seen',last_changed='$date_lc' WHERE sent_id='$sent_id'");
							

					}

					$this->load->view('admin/includes/header.php');
					$this->load->view('admin/doculynx/Template_sent.php', $data);
					$this->load->view('admin/includes/footer.php');
				} else {
					$this->load->view('email_templates/DocuLynx_template_voided_page');
				}

			}
		} else {
			echo "Please contact Creditlynx Support, Thank you";
		}
			

	}

	function getSentStatus($sent_id) {
		$data = $this->fetchRawData("SELECT * FROM template_sent WHERE sent_id=$sent_id ");
		return $data[0]['status'];
	}

	function getTemplates() {
		$data = $this->fetchRawData("SELECT * FROM templates WHERE template_id IN (SELECT MAX(template_id) FROM templates GROUP BY template_type)");
		return $data;
	}

	function getTemplateImages($template_id) {
		$data = $this->fetchRawData("SELECT * FROM template_images WHERE template_id=$template_id ");
		return $data;
	}

	function getSAFields() {
		$template_id = $this->input->get('template_id');
		$name1 = $this->input->get('name1');
		$name2 = $this->input->get('name2');
		$action = $this->input->get('action');
		if ($action == 'send' || $action == 'edit') {
			$data = $this->fetchRawData("SELECT
											*
										FROM
											template_fields
										WHERE
											template_id = $template_id
											AND name1 IS NULL
										GROUP BY field_id,drop_container");
		} else {
			$data = $this->fetchRawData("SELECT
											*
										FROM
											template_fields
										WHERE
											f_id IN (SELECT MAX(f_id) FROM template_fields WHERE template_id = $template_id
											AND name1='$name1' AND name2='$name2'
										GROUP BY field_id,drop_container ) ");
		}
		// $this->pprint($data);
		echo json_encode($data);
	}

	function saveSAField() {
		$f_id = $this->input->post('f_id');
		$field_id = $this->input->post('field_id');
		$style = $this->input->post('style');
		$drop_container = $this->input->post('drop_container');
		$input_container = $this->input->post('input_container');
		$name1 = $this->input->post('name1');
		$name2 = $this->input->post('name2');
		$email1 = $this->input->post('email1');
		$email2 = $this->input->post('email2');
		$action = $this->input->post('action');
		$template_id = $this->input->post('template_id');

		$data_exist = $this->fetchRawData("SELECT * FROM template_fields WHERE f_id='$f_id'");

		if ($action == 'edit') {

			if (count($data_exist) > 0) {
				$data_update = $this->db->query("UPDATE template_fields SET input_container='$input_container',style='$style',drop_container='$drop_container' WHERE f_id='$f_id'");
			} else {
				$data_insert = $this->db->query("INSERT INTO template_fields (template_id,input_container,drop_container,field_id,style) VALUES ('$template_id','$input_container','$drop_container','$field_id','$style')");
			}
		}
		if ($action == 'send') {
			$data_insert = $this->db->query("INSERT INTO template_fields (template_id,name1,email1,name2,email2,input_container,drop_container,field_id,style) VALUES ('$template_id','$name1','$email1','$name2','$email2','$input_container','$drop_container','$field_id','$style')");
		}

		if ($action == 'sent') {

			if (count($data_exist) > 0) {
				$data_update = $this->db->query("UPDATE template_fields SET input_container='$input_container',style='$style',drop_container='$drop_container' WHERE f_id='$f_id'");
			} else {
				$data_insert = $this->db->query("INSERT INTO template_fields (template_id,name1,email1,name2,email2,input_container,drop_container,field_id,style) VALUES ('$template_id','$name1','$email1','$name2','$email2','$input_container','$drop_container','$field_id','$style')");
			}
		}

		echo $action;
	}

	function deleteSAFieldByID() {
		$f_id = $this->input->post('f_id');
		$data_delete = $this->db->query("DELETE FROM template_fields WHERE f_id='$f_id'");
		echo "deleted";
	}

	function uploadTemplate() {
		$template_name = $this->input->post('template_type');
		// $template_description = $this->input->post('template_description');
		$template_type = $this->input->post('template_type');

		$template_file = $_FILES['template_file']['name'];

		if (!empty($_FILES['template_file']['name'])) {
			$filetmp = $_FILES["template_file"]["tmp_name"];
			$filename = $_FILES["template_file"]["name"];
			$filetype = $_FILES["template_file"]["type"];
			$filepath = 'assets/templates/' . $template_file;

			$this->load->model('Model_templates');
			$templates = new Model_templates();
			$templates->template_type = $template_type;
			$templates->template_name = $template_name;
			// $templates->template_description = $template_description;
			$templates->template_file = $template_file;
			$templates->save();

			$this->load->model('Model_template_images');

			// echo $filetmp;
			$filepath_image = 'assets/template_images/';
			$pdf = new Spatie\PdfToImage\Pdf($filetmp);
			$pages = $pdf->getNumberOfPages();

			for ($i = 1; $i <= $pages; $i++) {
				$file_name = $templates->template_id . '_' . $template_name . '_' . $i . '.jpg';
				$pdf->setPage($i)->saveImage($filepath_image . $file_name);
				$template_images = new Model_template_images();
				$template_images->template_id = $templates->template_id;
				$template_images->image_name = $file_name;
				$template_images->save();
			}
			move_uploaded_file($filetmp, $filepath);
		}

		redirect(base_url('DocuLynx'));
	}

	function templateSent() {
		$href = $this->input->post('href');
		$template_id = $this->input->post('template_id');
		$client_id = $this->input->post('client_id');
		$name1 = $this->input->post('name1');
		$name2 = $this->input->post('name2');
		$email1 = $this->input->post('email1');
		$email2 = $this->input->post('email2');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$href = str_replace('#', '', $href);
		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->href = $href;
		$template_sent->template_id = $template_id;
		$template_sent->client_id = $client_id;
		$template_sent->name1 = $name1;
		$template_sent->name2 = $name2;
		$template_sent->email1 = $email1;
		$template_sent->email2 = $email2;
		$template_sent->subject = $subject;
		$template_sent->message = $message;
		$template_sent->date_sent = date('Y-m-d H:i:s');
		$template_sent->save();
		// echo json_encode($data);
		echo "sent!";

		$template_email_settings = $this->getTemplateEmailSettings();
		if ($template_email_settings[0]['for_signers_to_sign'] == 'on') {
			$this->sendDocuLynxEmail($email1, $name1, $href . '&sent_id=' . $template_sent->sent_id . '&viewer=name1', $subject, $message);
			if ($email2 != '') {
				$this->sendDocuLynxEmail($email2, $name2, $href . '&sent_id=' . $template_sent->sent_id . '&viewer=name2', $subject, $message);
			}
		}

	}


	function templateSentFromProfile() {
		$href = $this->input->post('href');
		$template_id = $this->input->post('template_id');
		$client_id = $this->input->post('client_id');
		$name1 = $this->input->post('name1');
		$name2 = $this->input->post('name2');
		$email1 = $this->input->post('email1');
		$email2 = $this->input->post('email2');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$href = str_replace('#', '', $href);
		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->href = $href;
		$template_sent->template_id = $template_id;
		$template_sent->client_id = $client_id;
		$template_sent->name1 = $name1;
		$template_sent->name2 = $name2;
		$template_sent->email1 = $email1;
		$template_sent->email2 = $email2;
		$template_sent->subject = $subject;
		$template_sent->message = $message;
		$template_sent->date_sent = date('Y-m-d H:i:s');
		$template_sent->save();
		// echo json_encode($data);
		echo "sent!";

		$template_fields_default = $this->fetchRawData("SELECT * FROM template_fields WHERE name1 IS NULL AND name2 IS NULL AND email1 IS NULL AND email2 IS NULL AND template_id=$template_id");

		foreach ($template_fields_default as $key => $value) {
			$input_container = $value['input_container'];
			$drop_container = $value['drop_container'];
			$field_id = $value['field_id'];
			$style = $value['style']; 
			$data_insert = $this->db->query("INSERT INTO template_fields (template_id,name1,email1,name2,email2,input_container,drop_container,field_id,style) VALUES ('$template_id','$name1','$email1','$name2','$email2','$input_container','$drop_container','$field_id','$style')");
		}

		$template_email_settings = $this->getTemplateEmailSettings();
		if ($template_email_settings[0]['for_signers_to_sign'] == 'on') {
			$this->sendDocuLynxEmail($email1, $name1, $href . '&sent_id=' . $template_sent->sent_id . '&viewer=name1', $subject, $message);
			if ($email2 != '') {
				$this->sendDocuLynxEmail($email2, $name2, $href . '&sent_id=' . $template_sent->sent_id . '&viewer=name2', $subject, $message);
			}
		}

	}

	function getTemplateSent($status) {
		$status = strtolower($status);
		if ($status == 'waiting' || $status == '') {
			$data = $this->fetchRawData("SELECT *,(SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name` FROM template_sent WHERE status<>'Completed' AND status <> 'voided' ORDER BY sent_id DESC");
		} else {
			if ($status == 'completed') {
				$data = $this->fetchRawData("SELECT *,(SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name` FROM template_sent WHERE status='Completed' ORDER BY sent_id DESC");
			} else {
				$data = $this->fetchRawData("SELECT *,(SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name` FROM template_sent WHERE status='$status' ORDER BY sent_id DESC");
			}

		}

		return $data;
	}

	function updateFieldValue() {
		$f_id = $this->input->post('f_id');
		$value = $this->input->post('value');
		$value = addslashes($value);
		$data = $this->db->query("UPDATE template_fields SET value='$value' WHERE f_id='$f_id'");
		// echo "updated";

		$data_name = $this->fetchRawData("SELECT name1,field_id FROM template_fields WHERE f_id='$f_id'");
		$name1 = $data_name[0]['name1'];
		$field = $data_name[0]['field_id'];

		$data_lead = $this->fetchRawData("SELECT * FROM clients WHERE name='$name1'");
		$client_id = $data_lead[0]['client_id'];

		$data_billing = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id=$client_id");
		if (count($data_billing) == 0) {
			$data_insert = $this->db->query("INSERT INTO client_billings (client_id) VALUES ('$client_id')");
		}

		if ($field == 'ipp_init' || $field == 'cppp_init' || $field == 'iep_init' || $field == 'cepp_init') {
			if ($field == 'ipp_init') {
				$value = '149';
			}
			if ($field == 'cppp_init') {
				$value = '219';
			}
			if ($field == 'iep_init') {
				$value = '149';
			}
			if ($field == 'cepp_init') {
				$value = '219';
			}
			$data_update = $this->db->query("UPDATE client_billings SET setup_fee='$value' WHERE client_id='$client_id'");
		}

		if ($field == 'ipp_monthly' || $field == 'cppp_monthly' || $field == 'iep_result' || $field == 'cepp_result') {
			if ($field == 'ipp_monthly') {
				$value = '99';
			}
			if ($field == 'cppp_monthly') {
				$value = '179';
			}
			if ($field == 'iep_result') {
				$value = '50';
			}
			if ($field == 'cepp_result') {
				$value = '35';
			}
			$data_update = $this->db->query("UPDATE client_billings SET setup_fee='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_fee_date') {
			$value = date('m/d/Y', strtotime($value));
			$data_update = $this->db->query("UPDATE client_billings SET paid='$value' WHERE client_id='$client_id'");
			// $date_value = (int) date('m',strtotime($value));
			$data_update = $this->db->query("UPDATE client_billings SET monthly_due='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_due_date') {
			$data_update = $this->db->query("UPDATE client_billings SET monthly_due='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_card_number') {
			$data_update = $this->db->query("UPDATE client_billings SET card_number='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_expiration') {
			$data_update = $this->db->query("UPDATE client_billings SET card_expiration='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_cvv') {
			$data_update = $this->db->query("UPDATE client_billings SET cvv_code='$value' WHERE client_id='$client_id'");
		}
		if ($field == 'input_billing_zip') {
			$data_update = $this->db->query("UPDATE client_billings SET zip='$value' WHERE client_id='$client_id'");
		}

	}

	function updateTemplateSentStatus() {
		$sent_id = $this->input->post('sent_id');
		$status = $this->input->post('status');
		$date_lc = date('Y-m-d H:i:s');
		if ($status == 'Completed') {
			$template_email_settings = $this->getTemplateEmailSettings();
			if ($template_email_settings[0]['for_sender_is_completed'] == 'on') {
				$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,name1,name2 FROM template_sent WHERE sent_id=$sent_id");

				$name1 = $data_sent[0]['name1'];
				$name2 = $data_sent[0]['name2'];

				$href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
				$template_name = $data_sent[0]['template_name'];
				$subject = $name1 . ' completed ' . $template_name;
				$this->sendDocuLynxEmailCompleted('docs@creditlynx.com', $href . '&viewer=name1', $template_name, $subject);
				if ($name2 != '') {
					$subject = $name2 . ' completed ' . $template_name;
					$this->sendDocuLynxEmailCompleted('docs@creditlynx.com', $href . '&viewer=name2', $template_name, $subject);
				}

			}

			if ($template_email_settings[0]['for_signers_is_completed'] == 'on') {

				$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,email1,email2,name1,name2 FROM template_sent WHERE sent_id=$sent_id");
				$href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
				$template_name = $data_sent[0]['template_name'];
				$email1 = $data_sent[0]['email1'];
				$email2 = $data_sent[0]['email2'];
				$name1 = $data_sent[0]['name1'];
				$name2 = $data_sent[0]['name2'];

				$client_id = $this->getClientIDByName($name1);

				$subject = $name1 . ' completed ' . $template_name;
				$this->sendDocuLynxEmailCompleted($email1, $href, $template_name, $subject);
				$this->load->model('Model_client_documents');
				$client_documents = new Model_client_documents();
				$client_documents->client_id = $client_id;
				$client_documents->category = $template_name;
				$client_documents->file_download = $this->get_encrypted_string($href);
				$client_documents->file_size = '';
				$client_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_documents->save();

				if ($template_name == 'Cancellation' || $template_name == 'Credit Lynx Federal Notice of Cancellation') {
					$this->load->model('Model_clients');
					$clients = new Model_clients();
					$clients->load($client_id);
					$clients->client_status = 'Cancelled';
					$clients->auto_alert = '0';
					$clients->save();

					$date_of_cancellation = date('m/d/Y');
					$this->db->query("UPDATE client_enrollments SET date_of_cancellation='$date_of_cancellation' WHERE client_id=$client_id");
				}
				$client_id = $this->getClientIDByName($name1); 
				$this->load->model('Model_notes');
				$notes = new Model_notes();

				$notes->note_assigned = 'Clients';
				$notes->note_assigned_id = $client_id;
				
				$notes->sender = 'System';
				$notes->sender_id = '0';
				$notes->sender_photo = 'LynxLogo.png';
				$notes->sender_name = 'System';
				$notes->note = $name1.' Completed '.$template_name;
				$notes->note_date = date('Y-m-d H:i:s');
				$notes->note_sticky = 0;
				$notes->save();
				if ($email2 != '') {
					$subject = $name2 . ' completed ' . $template_name;
					$this->sendDocuLynxEmailCompleted($email2, $href, $template_name, $subject);
					$this->load->model('Model_client_joint_documents');
					$client_joint_id = $this->getClientJointIDByName($name2);
					$client_joint_documents = new Model_client_joint_documents();
					$client_joint_documents->client_joint_id = $client_joint_id;
					$client_joint_documents->category = $template_name;
					$client_joint_documents->file_download = $this->get_encrypted_string($href);
					$client_joint_documents->file_size = '';
					$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
					$client_joint_documents->save();

					if ($template_name == 'Cancellation' || $template_name == 'Credit Lynx Federal Notice of Cancellation') {
						$this->load->model('Model_client_joints');
						$client_joints = new Model_client_joints();
						$client_joints->load($client_joint_id);
						$client_joints->client_status = 'Cancelled';
						$client_joints->auto_alert = '0';
						$client_joints->save();

						$date_of_cancellation = date('m/d/Y');
						$this->db->query("UPDATE client_joint_enrollments SET date_of_cancellation='$date_of_cancellation' WHERE client_joint_id=$client_joint_id");
					}

					$client_joint_id = $this->getClientIDByName($name1); 
					$this->load->model('Model_notes');
					$notes = new Model_notes();

					$notes->note_assigned = 'Client Joints';
					$notes->note_assigned_id = $client_joint_id;
					
					$notes->sender = 'System';
					$notes->sender_id = '0';
					$notes->sender_photo = 'LynxLogo.png';
					$notes->sender_name = 'System';
					$notes->note = $name2.' Completed '.$template_name;
					$notes->note_date = date('Y-m-d H:i:s');
					$notes->note_sticky = 0;
					$notes->save();


				}

			}
		}
		$data = $this->db->query("UPDATE template_sent SET status='$status',last_changed='$date_lc' WHERE sent_id='$sent_id'");
		echo "updated";
	}

	

	function getClientIDByName($name) {
		$data = $this->fetchRawData("SELECT client_id FROM clients WHERE name='$name'");
		if (count($data) > 0) {
			return $data[0]['client_id'];
		} else {
			return $this->getClientJointIDByName($name);
		}
		
	}

	function getClientJointIDByName($name) {
		$data = $this->fetchRawData("SELECT client_joint_id FROM client_joints WHERE name='$name'");
		return $data[0]['client_joint_id'];
	}

	function sendDocuLynxEmailCompleted($to, $href, $template_name, $subject) {
		$data['href'] = $this->get_encrypted_string($href);
		$data['template_name'] = $template_name;
		$data['to'] = $to;
		$data['subject'] = $subject;
		$body = $this->load->view('email_templates/DocuLynx_template_completed', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('info@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject('Completed: ' . $template_name);
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {

		}
	}

	function sendDocuLynxEmailViewed($to, $href, $subject, $message) {
		$data['href'] = $this->get_encrypted_string($href);
		$data['header_message'] = $subject;
		$data['message'] = $message;
		$data['to'] = $to;
		$body = $this->load->view('email_templates/DocuLynx_template_viewed', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('info@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {

		}
	}

	function sendDocuLynxEmailVoided($to, $header_message, $reason) {
		$data['header_message'] = $header_message;
		$data['reason'] = $reason;
		$data['to'] = $to;
		$body = $this->load->view('email_templates/DocuLynx_template_voided', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('docs@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject('Voided: ' . $header_message);
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {

		}
	}

	function test() {
		$data['href'] = '';
		$data['header_message'] = 'wew';
		$data['message'] = 'wew';
		$data['to'] = 'to';
		$data['name'] = 'name';
		$data['reason'] = 'reason';
		$this->load->view('email_templates/DocuLynx_template_voided', $data);
	}

	function sendDocuLynxEmail($to, $name, $href, $subject, $message) {
		$data['href'] = $this->get_encrypted_string($href);
		$data['name'] = $name;
		$data['message'] = $message;
		$data['to'] = $to;

		$body = $this->load->view('email_templates/DocuLynx_template', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('docs@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {
			$template_email_settings = $this->getTemplateEmailSettings();
			if ($template_email_settings[0]['for_sender_to_recipient_fails'] == 'on') {
				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);

				$this->email->initialize($config);
				$this->email->from('docs@creditlynx.com', 'Credit Lynx');
				$this->email->to('docs@creditlynx.com');
				$this->email->subject('Document sending failed');
				$this->email->message('Email Document send to ' . $to . ' failed');
				if ($this->email->send()) {
					// echo "Emial has been sent";
					// return true;
				} else {
				}
			}
			// return $this->email->print_debugger();

		}
	}

	function saveEmailSettings() {
		$select_all_for_signers = $this->input->post('select_all_for_signers');
		$for_signers_to_sign = $this->input->post('for_signers_to_sign');
		$for_signers_is_completed = $this->input->post('for_signers_is_completed');
		$for_signers_is_corrected = $this->input->post('for_signers_is_corrected');
		$for_signers_declines_to_sign = $this->input->post('for_signers_declines_to_sign');
		$for_signers_is_vioded = $this->input->post('for_signers_is_vioded');
		$select_all_for_sender = $this->input->post('select_all_for_sender');
		$for_sender_is_completed = $this->input->post('for_sender_is_completed');
		$for_sender_view_envelope = $this->input->post('for_sender_view_envelope');
		$for_sender_declines_to_sign = $this->input->post('for_sender_declines_to_sign');
		$for_sender_to_recipient_fails = $this->input->post('for_sender_to_recipient_fails');

		$this->load->model('Model_template_email_settings');
		$template_email_settings = new Model_template_email_settings();
		$template_email_settings->es_id = 1;
		$template_email_settings->select_all_for_signers = $select_all_for_signers;
		$template_email_settings->for_signers_to_sign = $for_signers_to_sign;
		$template_email_settings->for_signers_is_completed = $for_signers_is_completed;
		$template_email_settings->for_signers_is_corrected = $for_signers_is_corrected;
		$template_email_settings->for_signers_declines_to_sign = $for_signers_declines_to_sign;
		$template_email_settings->for_signers_is_vioded = $for_signers_is_vioded;
		$template_email_settings->select_all_for_sender = $select_all_for_sender;
		$template_email_settings->for_sender_is_completed = $for_sender_is_completed;
		$template_email_settings->for_sender_view_envelope = $for_sender_view_envelope;
		$template_email_settings->for_sender_declines_to_sign = $for_sender_declines_to_sign;
		$template_email_settings->for_sender_to_recipient_fails = $for_sender_to_recipient_fails;
		$template_email_settings->save();

		redirect(base_url('DocuLynx'));
	}

	function voidTemplate() {
		$sent_id = $this->input->post('sent_id');
		$reason = $this->input->post('reason');
		$status = 'voided';
		$data = $this->db->query("UPDATE template_sent SET status='$status',void_reason='$reason' WHERE sent_id='$sent_id'");

		$template_email_settings = $this->getTemplateEmailSettings();
		if ($template_email_settings[0]['for_signers_is_vioded'] == 'on') {
			$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,template_sent.* FROM template_sent WHERE sent_id=$sent_id");
			$template_name = $data_sent[0]['template_name'];
			$email1 = $data_sent[0]['email1'];
			$email2 = $data_sent[0]['email2'];
			$name1 = $data_sent[0]['name1'];
			$name2 = $data_sent[0]['name2'];

			$header_message = $template_name . ' - ' . $name1;
			$this->sendDocuLynxEmailVoided($email1, $header_message, $reason);
			$header_message = $template_name . ' - ' . $name1;

			if ($name2 != '') {
				$header_message = $template_name . ' - ' . $name2;
				$this->sendDocuLynxEmailVoided($email2, $header_message, $reason);
			}
		}

		if ($reason != 'Decline to Sign') {
			redirect(base_url('DocuLynx'));
		} else {
			echo "voided";
		}

	}

	function route() {
		$page = $this->input->get('page');
		if ($page == 'decline_to_sign') {
			$this->load->view('email_templates/DocuLynx_template_voided_page');
		} else {
			$this->load->view('email_templates/DocuLynx_template_finish_later_page');
		}

	}

	function getWaitingTemplates() {
		$data = $this->fetchRawData("SELECT * FROM template_sent WHERE status='seen' OR status ='unseen'");
		// $this->pprint($data);
		foreach ($data as $key => $value) {
			$email1 = $value['email1'];
			$name1 = $value['name1'];
			$email2 = $value['email2'];
			$name2 = $value['name2'];
			$href = $value['href'];
			$sent_id = $value['sent_id'];
			$subject = $value['subject'];
			$message = $value['message'];
			$this->sendDocuLynxEmail($email1, $name1, $href . '&sent_id=' . $sent_id . '&viewer=name1', $subject, $message);
			if ($email2 != '') {
				$this->sendDocuLynxEmail($email2, $name2, $href . '&sent_id=' . $sent_id . '&viewer=name2', $subject, $message);
			}
		}
	}

	function saveToServer() {
		$data = $this->input->post('datas');
		echo $data;
	}

}
