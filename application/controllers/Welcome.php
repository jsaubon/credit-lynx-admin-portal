<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require '3rd-party/Bitly.php';
class Welcome extends MY_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		redirect(base_url('admin/login'));
	}

	function forecasting() {
		$this->load->view('welcome_message.php');
	}

	

	function test() {
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://lookup.binlist.net/45717360',
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// echo $resp;
		$this->pprint(json_decode($resp, TRUE));
		// Close request to clear up some resources
		curl_close($curl);
	}
	public function temp() {

		echo $string_to_encrypt = "SE2DnUHughiw4lquNLO0Eg";
		$password = "password";
		echo "<br>";
		// echo $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",$password);
		echo "<br>";
		echo $decrypted_string = openssl_decrypt($string_to_encrypt, "AES-128-ECB", $password);
	}


	function testing222() {
		$data['name'] = 'Josh';
		$data['username'] = 'joshuasaubon@gmail.com';
		$data['password'] = 'Joshua';
		$data['href'] = '';
		$data['to'] = 'joshuasaubon@gmail.com';
		$this->load->view('email_templates/invoice_template',$data);
	}

	function generateCSV() {
		$this->Model_query->selects = ['name', 'cell_phone', 'joint_name', 'joint_cell_phone'];
		$clients = $this->Model_query->getView('view_clients_table', []);
		$data = array();
		foreach ($clients as $key => $client) {
			$newData = array(
				'Name' => $client['name'],
				'Phone Number' => $client['cell_phone'],
			);
		}
		$fileName_1 = 'Manifest.csv';
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename={$fileName_1}");
		header("Expires: 0");
		header("Pragma: public");
		$fh1 = @fopen('php://output', 'w');
		$headerDisplayed1 = false;
		foreach ($data as $data1) {
			// Add a header row if it hasn't been added yet
			if (!$headerDisplayed1) {
				// Use the keys from $data as the titles
				fputcsv($fh1, array_keys($data1));
				$headerDisplayed1 = true;
			}
			// Put the data into the stream
			fputcsv($fh1, $data1);
		}
		// Close the file
		fclose($fh1);
		// Make sure nothing else is sent, our file is done
		exit;
	}

	function testingtesting() {
		$data['name'] = 'Joshua';
		$data['session'] = '1231asdzc';
		$this->load->view('email_templates/DocuLynx_template_signup.php', $data);
	}

	function testing() {
		$date_today = date('D');
		echo $date_today;
	}


	function test_verif_email() { 
		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

	
		$this->email->initialize($config);
		

			$from = 'no-reply@creditlynx.com';
		
		$this->email->from($from, 'CreditLynx');
		$this->email->to('joshuasaubon@gmail.com');
		$this->email->subject('WHAT TO EXPECT');

		$data_template['subject'] = 'WHAT TO EXPECT';
		$data_template['name'] = 'Joshua';
		$data_template['note'] = 'Once your verifications or other required documents have been submitted, our Dispute Team will be able to begin challenging the questionable negative items on your report to see if they are meeting the requirements outlined in the Fair Credit Reporting Act (FCRA).


According to the FCRA, the three credit bureaus (Equifax, Experian, TransUnion) and your creditors have a reasonable amount of time to verify the information on your report. If they cannot verify the information within the federally regulated 30-day time frame, or it is found to be inaccurate, then they are required to either correct it or remove it.


When their investigations are complete, they should be sending you responses. Typically, clients get them within 45-50 days, or sooner, from when the dispute letters were drafted and sent. This process remains the same for each round attempted on your behalf.
';




		$data_template['to'] = 'joshuasaubon@gmail.com';
		$this->load->view('email_templates/verif_template', $data_template);
		$body = $this->load->view('email_templates/verif_template', $data_template, TRUE);
		$this->email->message($body);

		if ($this->email->send()) {
		}
	}

}
