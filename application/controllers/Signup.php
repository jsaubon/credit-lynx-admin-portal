<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');

define("AUTHORIZENET_LOG_FILE", "phplog");

class Signup extends MY_controller {

	function getProfileSignup() {
		$session = $this->input->post('session');
		$type = $this->input->post('type');
		$data_session = $this->fetchRawData("SELECT * FROM signup_sessions WHERE session='$session' AND status=0");
		if (count($data_session) > 0) {
			$data_session = reset($data_session);
			if ($data_session['type'] == 'client') {
				$client_id = $data_session['id'];
				if ($type == 'renew') {
					$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_clients_table WHERE client_id = $client_id");
					$client_id = $data['profile'][0]['client_id'];
					$this->db->query("UPDATE clients SET lead_status='SA viewed' WHERE client_id=$client_id");
				} else {
					$data['profile'] = $this->fetchRawData("SELECT * FROM clients WHERE client_id = $client_id");
					$this->db->query("UPDATE clients SET lead_status='SA viewed' WHERE client_id=$client_id");
				}

				

				$data['billing'] = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id = $client_id");

				$note_date = date('Y-m-d H:i:s');
				$this->db->query("INSERT INTO notes 
					(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
					VALUES 
					('Clients','$client_id','Credit Lynx','0','LynxLogo.png','Credit Lynx','SA viewed','$note_date','1')
				");

			} else {
				$client_joint_id = $data_session['id'];
				if ($type == 'renew') {
					$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_clients_table WHERE client_joint_id = $client_joint_id");
					$client_id = $data['profile'][0]['client_id'];
					$this->db->query("UPDATE clients SET lead_status='SA viewed' WHERE client_id=$client_id");
				} else {
					$data['profile'] = $this->fetchRawData("SELECT * FROM client_joints WHERE client_joint_id = $client_joint_id");
					$this->db->query("UPDATE clients SET lead_status='SA viewed' WHERE client_id=$client_joint_id");
				}

				$data['billing'] = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id = $client_joint_id");

				$note_date = date('Y-m-d H:i:s');
				$this->db->query("INSERT INTO notes 
					(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
					VALUES 
					('Client Joints','$client_joint_id','Credit Lynx','0','LynxLogo.png','Credit Lynx','SA viewed','$note_date','1')
				");

			}
			echo json_encode($data);
		} else {
			echo "0";
		}
	}

	function getProfileSignupRenew() {
		$session = $this->input->post('session');
		$data_session = $this->fetchRawData("SELECT * FROM signup_sessions WHERE session='$session' AND status=0");
		if (count($data_session) > 0) {
			$data_session = reset($data_session);
			if ($data_session['type'] == 'client') {
				$client_id = $data_session['id'];
				$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_clients_table WHERE client_id = $client_id");
				$data['billing'] = $this->fetchRawData("SELECT * FROM client_billings WHERE client_billing_id IN (SELECT MAX(client_billing_id) FROM client_billings WHERE client_id=$client_id)");

				$client_id = $data['profile'][0]['client_id'];
				$this->db->query("UPDATE clients SET lead_status='SA viewed' WHERE client_id=$client_id");

				$note_date = date('Y-m-d H:i:s');
				$this->db->query("INSERT INTO notes 
					(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
					VALUES 
					('Clients','$client_id','Credit Lynx','0','LynxLogo.png','Credit Lynx','SA viewed','$note_date','1')
				");
			} else {
				$client_joint_id = $data_session['id'];
				$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_clients_table WHERE client_joint_id = $client_joint_id");
				$data['billing'] = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id = $client_joint_id");

				$note_date = date('Y-m-d H:i:s');
				$this->db->query("INSERT INTO notes 
					(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
					VALUES 
					('Client Joints','$client_joint_id','Credit Lynx','0','LynxLogo.png','Credit Lynx','SA viewed','$note_date','1')
				");
			}
			echo json_encode($data);
		} else {
			echo "0";
		}
	}

	function array_flatten($array) {
		if (!is_array($array)) {
			return FALSE;
		}
		$result = array();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$result = array_merge($result, $this->array_flatten($value));
			} else {
				$result[$key] = $value;
			}
		}

		foreach ($result as $field => $value) {
			if (!is_array($value)) {
				$result[$field] = $this->trim_data($value);
			}
		}
		return $result;
	}

	public function trim_data($str) {
		$str = trim($str);
		$str = addslashes($str);
		$str = strip_tags($str);
		return $str;
	}

	function cleanPhoneNumber($str) {
		$str = str_replace('(', '', $str);
		$str = str_replace(')', '', $str);
		$str = str_replace(' ', '', $str);
		$str = str_replace('-', '', $str);
		return $str;
	}

	function getClientIDBySession($session) {
		$data_session = $this->fetchRawData("SELECT * FROM signup_sessions WHERE session='$session'");
		$data_session = reset($data_session);
		if ($data_session['type'] == 'client') {
			$client_id = $data_session['id'];
		} else {
			$id = $data_session['id'];
			$data = $this->fetchRawData("SELECT client_id FROM client_joints WHERE client_joint_id=$id");
			$client_id = $data[0]['client_id'];
		}

		return $client_id;
	}

	function get_pool_employee_id() {
        $data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' AND pool=1 AND status=1");
        if (count($data) > 0) {
            $data = reset($data);
            $data = reset($data);
            return $data;
        } else {
            return 0;
        }
    }

    function get_next_pool_add_new($employee_id) {
        $this->load->model('Model_employees');
        $employees = new Model_employees();
        $data = $employees->search(['type'=>'Sales Team','status'=>1],'employee_id','ASC');
        // $this->pprint($data);
        $index = array_search($employee_id, array_column($data, 'employee_id'));
        if ($index < (count($data) -1)) {
            $this->set_lead_pool_add_new($data[$index + 1]['employee_id']);
        } else {
            $this->set_lead_pool_add_new($data[0]['employee_id']);
        }
    }

    function set_lead_pool_add_new($employee_id) {
        $pool = 1;
        $this->db->query("UPDATE employees SET pool=0 WHERE type='Sales Team'");

        $this->db->query("UPDATE employees SET pool=$pool WHERE employee_id=$employee_id");
    }

	public function insert_new_lead_2() {
		$string = $this->input->post();
		// $string = json_decode($string, TRUE);
		if (isset($string['g-recaptcha-response'])) {
			unset($string['g-recaptcha-response']);
			$post_fields = array();
			foreach ($string as $key => $v) {
				$new_field = [str_replace('profile_', '', $key) => $v];
				array_push($post_fields, $new_field);
			}

			$post_fields = $this->array_flatten($post_fields);

			// $this->pprint($post_fields);

			// VARIABLES
			$session = empty($post_fields['session']) ? '' : $post_fields['session'];

			if ($session != '') {
				$client_id = $this->getClientIDBySession($session);
			} else {
				$client_id = '';
			}

			$name = addslashes($post_fields['name']);
			$email_address = addslashes($post_fields['email_address']);
			$cell_phone = addslashes($post_fields['cell_phone']);
			$carrier = addslashes($post_fields['carrier']);
			$address = addslashes($post_fields['address']);
			$state_province = addslashes($post_fields['state_province']);
			$city = addslashes($post_fields['city']);
			$zip_postal_code = addslashes($post_fields['zip_postal_code']);
			$date_of_birth = addslashes($post_fields['date_of_birth']);
			$ss = addslashes($post_fields['ss']);

			$username = addslashes($post_fields['email_address']);
			$password = $this->get_default_password($name);
			$password = password_hash($password, PASSWORD_BCRYPT);

			$joint_name = addslashes($post_fields['joint_name']);
			$joint_email_address = addslashes($post_fields['joint_email_address']);
			$joint_cell_phone = addslashes($post_fields['joint_cell_phone']);
			$joint_carrier = addslashes($post_fields['joint_carrier']);
			$joint_ss = addslashes($post_fields['joint_ss']);
			$joint_date_of_birth = addslashes($post_fields['joint_date_of_birth']);

			$joint_username = addslashes($post_fields['joint_email_address']);
			$password = $this->get_default_password($joint_name);
			$joint_password = password_hash($password, PASSWORD_BCRYPT);

			$billing_paid = addslashes($post_fields['billing_paid']);
			$billing_monthly_due = date('m/d/Y', strtotime("$billing_paid + 1 month"));
			$billing_card_number = str_replace(' ', '', addslashes($post_fields['billing_card_number']));
			$billing_card_expiration = addslashes($post_fields['billing_card_expiration']);
			$billing_cvv_code = addslashes($post_fields['billing_cvv_code']);
			$billing_card_holder = addslashes($post_fields['billing_card_holder']);
			$billing_payment_method = addslashes($post_fields['billing_payment_method']);
			$billing_address = addslashes($post_fields['billing_address']);
			$billing_city = addslashes($post_fields['billing_city']);
			$billing_state = addslashes($post_fields['billing_state']);
			$billing_zip = addslashes($post_fields['billing_zip']);
			$client_type = empty($post_fields['client_type']) ? 'Single' : $post_fields['client_type'];
			$coupon_code = '';

			$date_created = date('Y-m-d H:i:s');
			$client_status = '';
			$lead_status = 'New';

			$plan = $post_fields['billing_plan'];
			$coupon_code = '';
			$setup_fee = $this->getPlanDetail($plan, 'setup_fee');
			$monthly_fee = $this->getPlanDetail($plan, 'monthly_fee');

			$font_array = array('Georgia, serif', '"Palatino Linotype", "Book Antiqua", Palatino, serif', '"Times New Roman", Times, serif', 'Arial, Helvetica, sans-serif', '"Arial Black", Gadget, sans-serif', '"Comic Sans MS", cursive, sans-serif', 'Impact, Charcoal, sans-serif', '"Lucida Sans Unicode", "Lucida Grande", sans-serif', 'Tahoma, Geneva, sans-serif', '"Trebuchet MS", Helvetica, sans-serif', 'Verdana, Geneva, sans-serif', '"Courier New", Courier, monospace', '"Lucida Console", Monaco, monospace');
			$font = $font_array[rand(0, 12)];

			if ($client_id == '') {
				$this->load->model('Model_clients');
				$clients = new Model_clients();
				$clients->converted = 0;
				$clients->name = $name;
				$clients->email_address = $email_address;
				$clients->cell_phone = $cell_phone;
				$clients->carrier = $carrier;
				$clients->address = $address;
				$clients->state_province = $state_province;
				$clients->city = $city;
				$clients->zip_postal_code = $zip_postal_code;
				$clients->username = $username;
				$clients->password = $password;
				$clients->ss = $ss;
				$clients->date_of_birth = $date_of_birth;
				$clients->client_type = $client_type;
				$clients->date_created = $date_created;
				$clients->client_status = $client_status;
				$clients->lead_status = $lead_status;
				$clients->font = $font;
				$clients->save();

				$this->sendRCSms('+18139517686', 'New lead: '.$name.' Phone Number: '.$cell_phone);
				$message = 'New lead: '.$name.'<br>Phone Number: '.$cell_phone.'<br>Email: '.$email_address;
				$this->send_custom_email('New Lead','',$message,['joshuasaubon@gmail.com','baumab03@dialyourleads.com']);

				$client_id = $clients->client_id;

				$this->load->model('Model_client_assignments');
				$client_assignments = new Model_client_assignments();
				$client_assignments->client_id = $client_id;

		        $sale_id = $this->get_pool_employee_id();
		        $client_assignments->sale_id = $sale_id;
		        
		        $client_assignments->save();
		        
		        $this->get_next_pool_add_new($sale_id);
				


				$this->load->model('Model_client_billings');
				$client_billings = new Model_client_billings();
				$client_billings->client_id = $client_id;
				$client_billings->paid = $billing_paid;
				$client_billings->monthly_due = $billing_monthly_due;
				$client_billings->card_number = $billing_card_number;
				$client_billings->card_expiration = $billing_card_expiration;
				$client_billings->cvv_code = $billing_cvv_code;
				$client_billings->card_holder = $billing_card_holder;
				$client_billings->payment_method = $billing_payment_method;
				$client_billings->state = $billing_state;
				$client_billings->zip = $billing_zip;
				$client_billings->setup_fee = $setup_fee;
				$client_billings->monthly_fee = $monthly_fee;
				$client_billings->plan = $plan;
				$client_billings->coupon_code = $coupon_code;
				$client_billings->save();

				$client_joint_id = '';
				if ($client_type == 'Joint') {
					$this->load->model('Model_client_joints');
					$client_joints = new Model_client_joints();
					$client_joints->client_status = $client_status;
					$client_joints->client_id = $client_id;
					$client_joints->name = $joint_name;
					$client_joints->cell_phone = $joint_cell_phone;
					$client_joints->carrier = $joint_carrier;
					$client_joints->email_address = $joint_email_address;
					$client_joints->ss = $joint_ss;
					$client_joints->date_of_birth = $joint_date_of_birth;
					$client_joints->username = $joint_username;
					$client_joints->password = $joint_password;
					$client_joints->save();

					$this->sendRCSms('+18139517686', 'New lead: '.$joint_name.' Phone Number: '.$joint_cell_phone);


					$message = 'New lead: '.$joint_name.'<br>Phone Number: '.$joint_cell_phone.'<br>Email: '.$joint_email_address;
					$this->send_custom_email('New Lead','',$message,['joshuasaubon@gmail.com','baumab03@dialyourleads.com']);

					$client_joint_id = $client_joints->client_joint_id;

					$this->load->model('Model_client_joint_billings');
					$client_joint_billings = new Model_client_joint_billings();
					$client_joint_billings->client_joint_id = $client_joint_id;
					$client_joint_billings->paid = $billing_paid;
					$client_joint_billings->monthly_due = $billing_monthly_due;
					$client_joint_billings->card_number = $billing_card_number;
					$client_joint_billings->card_expiration = $billing_card_expiration;
					$client_joint_billings->cvv_code = $billing_cvv_code;
					$client_joint_billings->card_holder = $billing_card_holder;
					$client_joint_billings->payment_method = $billing_payment_method;
					$client_joint_billings->state = $billing_state;
					$client_joint_billings->zip = $billing_zip;
					$client_joint_billings->setup_fee = $setup_fee;
					$client_joint_billings->monthly_fee = $monthly_fee;
					$client_joint_billings->plan = $plan;
					$client_joint_billings->coupon_code = $coupon_code;
					$client_joint_billings->save();

				}

				
			} else {
				$this->load->model('Model_clients');
				$clients = new Model_clients();
				$clients->load($client_id);
				$clients->name = $name;
				$clients->email_address = $email_address;
				$clients->cell_phone = $cell_phone;
				$clients->carrier = $carrier;
				$clients->address = $address;
				$clients->state_province = $state_province;
				$clients->city = $city;
				$clients->zip_postal_code = $zip_postal_code;
				$clients->username = $username;
				$clients->password = $password;
				$clients->ss = $ss;
				$clients->date_of_birth = $date_of_birth;
				$clients->save();

				$this->load->model('Model_client_billings');
				$client_billings = new Model_client_billings();
				$data_client_billings = $client_billings->search(['client_id' => $client_id]);
				$data_client_billings = reset($data_client_billings);
				$data_client_billings = reset($data_client_billings);

				$client_billings = new Model_client_billings();
				$client_billings->load($data_client_billings);
				$client_billings->paid = $billing_paid;
				$client_billings->monthly_due = $billing_monthly_due;
				$client_billings->card_number = $billing_card_number;
				$client_billings->card_expiration = $billing_card_expiration;
				$client_billings->cvv_code = $billing_cvv_code;
				$client_billings->card_holder = $billing_card_holder;
				$client_billings->state = $billing_state;
				$client_billings->zip = $billing_zip;
				$client_billings->setup_fee = $setup_fee;
				$client_billings->monthly_fee = $monthly_fee;
				$client_billings->plan = $plan;
				$client_billings->coupon_code = $coupon_code;
				$client_billings->client_id = $client_id;
				$client_billings->save();

				if ($joint_email_address != '') {
					$this->load->model('Model_client_joints');
					$client_joints = new Model_client_joints();
					$data_client_joints = $client_joints->search(['client_id' => $client_id]);
					$data_client_joints = reset($data_client_joints);
					$data_client_joints = reset($data_client_joints);
					$client_joints = new Model_client_joints();
					$client_joints->load($data_client_joints);
					$client_joints->name = $joint_name;
					$client_joints->cell_phone = $joint_cell_phone;
					$client_joints->carrier = $joint_carrier;
					$client_joints->email_address = $joint_email_address;
					$client_joints->ss = $joint_ss;
					$client_joints->date_of_birth = $joint_date_of_birth;
					$client_joints->username = $joint_username;
					$client_joints->password = $joint_password;
					$client_joints->client_id = $client_id;
					$client_joints->save();

					$client_joint_id = $client_joints->client_joint_id;
					$this->load->model('Model_client_joint_billings');
					$client_joint_billings = new Model_client_joint_billings();
					$data_client_joint_billings = $client_joint_billings->search(['client_joint_id' => $client_joint_id]);
					$data_client_joint_billings = reset($data_client_joint_billings);
					$data_client_joint_billings = reset($data_client_joint_billings);
					$client_joint_billings->load($data_client_joint_billings);
					$client_joint_billings->paid = $billing_paid;
					$client_joint_billings->monthly_due = $billing_monthly_due;
					$client_joint_billings->card_number = $billing_card_number;
					$client_joint_billings->card_expiration = $billing_card_expiration;
					$client_joint_billings->cvv_code = $billing_cvv_code;
					$client_joint_billings->card_holder = $billing_card_holder;
					$client_joint_billings->state = $billing_state;
					$client_joint_billings->zip = $billing_zip;
					$client_joint_billings->setup_fee = $setup_fee;
					$client_joint_billings->monthly_fee = $monthly_fee;
					$client_joint_billings->plan = $plan;
					$client_joint_billings->coupon_code = $coupon_code;
					$client_joint_billings->save();
				}
			}

			$template_ids = $this->getTemplateIdByPlan($plan);
			$template_id_plan = $template_ids['plan'];
			if ($template_id_plan != 0) {
				$client_id = $client_id;
				$name1 = $name;
				$email1 = $email_address;
				$cell1 = $cell_phone;
				$message = 'Please review and sign.';

				$name2 = $joint_name;
				$email2 = $joint_email_address;
				$cell2 = $joint_cell_phone;

				$billings['billing_paid'] = $billing_paid;
				$billings['setup_fee'] = $setup_fee;
				$billings['billing_monthly_due'] = $billing_monthly_due;
				$billings['monthly_fee'] = $monthly_fee;
				$billings['billing_card_number'] = $billing_card_number;
				$billings['billing_card_expiration'] = $billing_card_expiration;
				$billings['billing_cvv_code'] = $billing_cvv_code;
				$billings['billing_card_holder'] = $billing_card_holder;
				$billings['billing_address'] = $billing_address;
				$billings['billing_city'] = $billing_city;
				$billings['billing_state'] = $billing_state;
				$billings['billing_zip'] = $billing_zip;

				// echo $post_fields['signature'];

				$sent_id_sa = $this->saveNewTemplateSent($template_id_plan, $client_id, $name1, $email1, 'Credit Lynx Service Agreement', $message, $post_fields['signature'], false, $billings, $date_of_birth, $ss);

				// Credit Lynx Service Agreement and Limited Letter of Authorization
				$href_sa = 'https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=' . $template_id_plan . '&id1=' . $client_id . '&id2=&name1=' . $name1 . '&email1=' . $email1 . '&name2=&email2=&subject=Credit Lynx Service Agreement&message=' . $message . '.';
				$href_sa = str_replace(' ', '+', $href_sa);
				$href_sa = str_replace('#', '', $href_sa);
				$href_sa = $href_sa . '&sent_id=' . $sent_id_sa;

				$this->sendDoculynxMergedSA($name1, $email1, $href_sa, $cell1);

				$this->db->query("UPDATE clients SET lead_status='Need Payment' WHERE client_id=$client_id");
				$this->db->query("DELETE FROM template_sent WHERE subject='Email Sent' AND email1='$email1'");
				if ($email2 != '') {
					$sent_id_sa = $this->saveNewTemplateSent($template_id_plan, $client_joint_id, $name2, $email2, 'Credit Lynx Service Agreement', $message, $post_fields['joint_signature'], false, $billings, $joint_date_of_birth, $joint_ss);

					// Credit Lynx Service Agreement and Limited Letter of Authorization
					$href_sa = 'https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=' . $template_id_plan . '&id1=' . $client_joint_id . '&id2=&name1=' . $name2 . '&email1=' . $email2 . '&name2=&email2=&subject=Credit Lynx Service Agreement&message=' . $message . '.';
					$href_sa = str_replace(' ', '+', $href_sa);
					$href_sa = str_replace('#', '', $href_sa);
					$href_sa = $href_sa . '&sent_id=' . $sent_id_sa;

					$this->sendDoculynxMergedSA($name2, $email2, $href_sa, $cell2);
					$this->db->query("DELETE FROM template_sent WHERE subject='Email Sent' AND email2='$email2'");
				}

				$this->db->query("UPDATE signup_sessions SET status = 1 WHERE session='$session'");
				$sa_date = date('m/d/Y');
				$client_enrollments = $this->fetchRawData("SELECT * FROM client_enrollments WHERE client_id=$client_id");
				if (count($client_enrollments) > 0) {
					$this->db->query("UPDATE client_enrollments SET service_agreement = '$sa_date' WHERE client_id='$client_id'");
				} else {
					$this->db->query("INSERT INTO client_enrollments (client_id,service_agreement) VALUES ($client_id,'$sa_date')");
				}
				if ($email2 != '') {
					$client_joint_enrollments = $this->fetchRawData("SELECT * FROM client_joint_enrollments WHERE client_joint_id=$client_joint_id");
					if (count($client_joint_enrollments) > 0) {
						$this->db->query("UPDATE client_joint_enrollments SET service_agreement = '$sa_date' WHERE client_joint_id='$client_joint_id'");
					} else {
						$this->db->query("INSERT INTO client_joint_enrollments (client_joint_id,service_agreement) VALUES ($client_joint_id,'$sa_date')");
					} 
				}

				redirect('https://www.creditlynx.com/upload-successful/');
			} else {
				echo "There is a problem with your signup form, please contact administrator. Thank you.";

			}

		}

	}

	function getPlanDetail($plan, $field) {
		$this->load->model('Model_subscription');
		$subscription = new Model_subscription();
		$subscription->selects = [$field];
		$data = $subscription->searchWithSelect(['name' => $plan]);
		$data = reset($data);
		$data = reset($data);
		return $data;

	}

	public function insert_new_client() {
		$string = $this->input->post('info');
		$string = json_decode($string, TRUE);
		// $this->pprint($string);
		$post_fields = array();
		foreach ($string as $key => $v) {
			$new_field = [str_replace('client_', '', $key) => $v];
			array_push($post_fields, $new_field);
		}

		$post_fields = $this->array_flatten($post_fields);

		$this->load->model('Model_clients');
		$clients = new Model_clients();
		foreach ($post_fields as $key => $value) {
			if (isset($clients->$key)) {
				if ($key != 'g-recaptcha-response' && $key != 'your-message') {
					$clients->$key = $value;
				}

			}

		}
		$clients->converted = 1;
		$clients->client_status = 'Active';
		$clients->save();

	}

	function saveNewTemplateSent($template_id, $client_id, $name, $email, $subject, $message, $signature, $no_value, $billings, $dob, $ssn) {
		$href = 'https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=' . $template_id . '&id1=' . $client_id . '&id2=&name1=' . $name . '&email1=' . $email . '&name2=&email2=&subject=' . $subject . '&message=' . $message . '.';
		$href = str_replace(' ', '+', $href);

		$href = str_replace('#', '', $href);

		if ($subject == 'Credit Lynx Federal Notice of Cancellation') {
			$href = $href . '&viewer=name1';
		}

		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->href = $href;
		$template_sent->template_id = $template_id;
		$template_sent->client_id = $client_id;
		$template_sent->name1 = $name;
		$template_sent->email1 = $email;
		$template_sent->name2 = '';
		$template_sent->email2 = '';
		$template_sent->subject = $subject;
		$template_sent->message = $message;
		$template_sent->date_sent = date('Y-m-d H:i:s');
		$template_sent->save();

		$data_get_tfields = $this->fetchRawData("SELECT * FROM template_fields WHERE name1 IS NULL AND name2 IS NULL AND email1 IS NULL AND email2 IS NULL AND template_id=$template_id");

		foreach ($data_get_tfields as $key => $field) {
			$input_container = $field['input_container'];
			$drop_container = $field['drop_container'];
			$field_id = $field['field_id'];
			$style = $field['style'];
			$value = '';
			if ($no_value == false) {
				if ($field_id == 'input_fullname1') {
					$value = $name;
				}
				if ($field_id == 'signature1') {
					$value = $signature;
				}
				if ($field_id == 'date_signed1') {
					$value = date('m/d/Y h:i:s A');
				}

				if ($field_id == 'profile_joint_signature') {
					$value = $signature;
				}
				if ($field_id == 'date_signed2') {
					$value = date('m/d/Y h:i:s A');
				}
				if ($field_id == 'cancel_date') {
					$value = date('m/d/Y', strtotime(date('m/d/y') . ' + 3 days'));
				}
				if ($field_id == 'input_fee_date') {
					$value = $billings['billing_paid'];
				}
				if ($field_id == 'input_due_date') {
					$value = $billings['billing_monthly_due'];
				}
				if ($field_id == 'input_setup_fee') {
					$value = $billings['setup_fee'];
				}
				if ($field_id == 'input_monthly_fee') {
					$value = $billings['monthly_fee'];
				}
				if ($field_id == 'input_name_on_card') {
					$value = $billings['billing_card_holder'];
				}
				if ($field_id == 'input_card_number') {
					$value = $billings['billing_card_number'];
				}
				if ($field_id == 'input_expiration') {
					$value = $billings['billing_card_expiration'];
				}
				if ($field_id == 'input_cvv') {
					$value = $billings['billing_cvv_code'];
				}
				if ($field_id == 'input_billing_address') {
					$value = $billings['billing_address'];
				}
				if ($field_id == 'input_billing_city') {
					$value = $billings['billing_city'];
				}
				if ($field_id == 'input_billing_state') {
					$value = $billings['billing_state'];
				}
				if ($field_id == 'input_billing_zip') {
					$value = $billings['billing_zip'];
				}
				if ($field_id == 'input_dob_1') {
					$value = date('m/d/Y', strtotime($dob));
				}
				if ($field_id == 'input_ssn_1') {
					$value = $ssn;
				}
			} else {
				if ($field_id == 'cancel_date') {
					$value = date('m/d/Y', strtotime(date('m/d/y') . ' + 3 days'));
				}
				if ($field_id == 'signature1') {
					$value = '';
				}
				if ($field_id == 'date_signed1') {
					$value = '';
				}
			}

			$value = addslashes($value);
			$value = strip_tags($value);
			$data_insert = $this->db->query("INSERT INTO template_fields (template_id,name1,email1,name2,email2,input_container,drop_container,field_id,style,value) VALUES ('$template_id','$name','$email','','','$input_container','$drop_container','$field_id','$style','$value')");

		}
		if ($no_value) {
			$this->updateTemplateSentStatusSignup($template_sent->sent_id, 'unseen');
		} else {
			$this->updateTemplateSentStatusSignup($template_sent->sent_id, 'Completed',date('Y-m-d',strtotime($billings['billing_paid'])));
		}

		return $template_sent->sent_id;
	}

	function updateTemplateSentStatusSignup($sent_id, $status,$setup_fee_date = '') {
		$date_lc = date('Y-m-d H:i:s');
		// if ($status == 'Completed') {
		$template_email_settings = $this->getTemplateEmailSettings();
		if ($template_email_settings[0]['for_sender_is_completed'] == 'on') {
			$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,name1,name2 FROM template_sent WHERE sent_id=$sent_id");

			$name1 = $data_sent[0]['name1'];
			$name2 = $data_sent[0]['name2'];

			$href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
			$template_name = $data_sent[0]['template_name'];
			$subject = $name1 . ' completed ' . $template_name;
			// $this->sendDocuLynxEmailCompleted('docs@creditlynx.com',$href.'&viewer=name1','Service Agreement',$subject);
			if ($name2 != '') {
				$subject = $name2 . ' completed ' . $template_name;
				// $this->sendDocuLynxEmailCompleted('docs@creditlynx.com',$href.'&viewer=name2','Service Agreement',$subject);
			}

		}

		if ($template_email_settings[0]['for_signers_is_completed'] == 'on') {

			$data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,email1,email2,name1,name2 FROM template_sent WHERE sent_id=$sent_id");
			$href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
			$template_name = $data_sent[0]['template_name'];
			$email1 = $data_sent[0]['email1'];
			$email2 = $data_sent[0]['email2'];
			$name1 = $data_sent[0]['name1'];
			$name2 = $data_sent[0]['name2'];

			if ($template_name != 'Esignature Disclosure' && $template_name != 'Federal Disclosure' && $template_name != 'Cancellation' && $template_name != 'Payment Change Request Form' && $template_name != 'Billing Authorization Form') {
				$template_name = 'Service Agreement';
			}

			$subject = $name1 . ' completed ' . $template_name;
			if ($status == 'Completed') {
				// $this->sendDocuLynxEmailCompleted($email1,$href,$template_name,$subject);
			} else {
				// $this->sendDocuLynxEmailCompleted($email1,$href.'&viewer=name1',$template_name,$subject);
			}

			$client_joint_id = $this->getClientJointIDByName($name1);
			if ($client_joint_id != false) {
				$this->load->model('Model_client_joint_documents');
				$client_joint_documents = new Model_client_joint_documents();
				$client_joint_documents->client_joint_id = $client_joint_id;
				$client_joint_documents->category = $template_name;
				$client_joint_documents->file_download = $this->get_encrypted_string($href);
				$client_joint_documents->file_size = '';
				$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_joint_documents->save();
 
				$this->load->model('Model_notes');
				$notes = new Model_notes();
 
				$notes->note_assigned = 'Client Joints';
				$notes->note_assigned_id = $client_joint_id;
			 
				$notes->sender = 'System';
				$notes->sender_id = 0;
				$notes->sender_photo = 'LynxLogo.png';
				$notes->sender_name = 'Credit Lynx';
				$notes->note = 'Service Agreement Completed';
				$notes->note_date = date('Y-m-d H:i:s');
				$notes->note_sticky = 0;
				$notes->save();
				if (date('Y-m-d') == $setup_fee_date) {
					$this->get_client_primary_card_info($client_joint_id,'joint');
				}
				

			} else {
				$client_id = $this->getClientIDByName($name1);
				$this->load->model('Model_client_documents');
				$client_documents = new Model_client_documents();
				$client_documents->client_id = $client_id;
				$client_documents->category = $template_name;
				$client_documents->file_download = $this->get_encrypted_string($href);
				$client_documents->file_size = '';
				$client_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_documents->save();

				$this->load->model('Model_notes');
				$notes = new Model_notes();
 
				$notes->note_assigned = 'Clients';
				$notes->note_assigned_id = $client_id;
			 
				$notes->sender = 'System';
				$notes->sender_id = 0;
				$notes->sender_photo = 'LynxLogo.png';
				$notes->sender_name = 'Credit Lynx';
				$notes->note = 'Service Agreement Completed';
				$notes->note_date = date('Y-m-d H:i:s');
				$notes->note_sticky = 0;
				$notes->save();


				if (date('Y-m-d') == $setup_fee_date) {
					$this->get_client_primary_card_info($client_id,'client');
				}
				
			}
		}
		// } else {

		// }
		$data = $this->db->query("UPDATE template_sent SET status='$status',last_changed='$date_lc' WHERE sent_id='$sent_id'");
		// echo "updated";
	}

	function get_client_primary_card_info($id,$type) {
		if ($type == 'client') {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM clients WHERE client_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,setup_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,setup_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing);
		}


		// RESTRICTIONS
		$restrictions = array('Cancelled','Terminated','Archived');
		if (!in_array($client['client_status'], $restrictions)) {
			// ASSIGN DATA
			$email = $client['email_address'];
			$plan = $billing['plan'];
			$setup_fee = $billing['setup_fee'];
			// ADD 15 to monthly fee 
            $sub_total = number_format($setup_fee,2);
			
			$card_info['number'] = $billing['card_number'];
	        $card_info['expiration_date'] = $billing['card_expiration'];
	        $card_info['code']  = $billing['cvv_code'];

	        $full_name = explode(' ', $billing['card_holder']);
	        $last_name = $full_name[count($full_name) -1];
	        unset($full_name[count($full_name)  -1]); 
	        $first_name = implode(' ', $full_name);

			$shipping_address['first_name'] = $first_name;
	        $shipping_address['last_name'] = $last_name;
	        $shipping_address['company'] = '';
	        $shipping_address['address'] = $billing['address'];
	        $shipping_address['city'] = $billing['city'];
	        $shipping_address['state'] = $billing['state'];
	        $shipping_address['zip'] = $billing['zip'];
	        $shipping_address['country'] = 'USA';
	        $shipping_address['phone_number'] = $client['cell_phone'];
	        $shipping_address['fax_number'] = '';
			// $this->pprint($this->input->post());

			$month_names = date('F');
			// $this->pprint($month_names);
			// $this->pprint($email);
			// $this->pprint($plan);
			// $this->pprint($sub_total);
			// $this->pprint($card_info);
			// $this->pprint($shipping_address);
			// echo 'charge this card';
			// CHARGE MONTHLY
			$this->chargeAuthorizeCreditCardSetupFee($email, $plan, $sub_total, $card_info, $shipping_address, $month_names,$id,$type);
		}
			
	}


	function chargeAuthorizeCreditCardSetupFee($email, $plan, $setup_fee, $card_info, $shipping_address, $month_name,$id,$type) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Plan Setup Fee ($' . $setup_fee . ')');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		// $customerAddress->setCompany($shipping_address['company']);
		// $customerAddress->setAddress($shipping_address['address']);
		// $customerAddress->setCity($shipping_address['city']);
		// $customerAddress->setState($shipping_address['state']);
		// $customerAddress->setZip($shipping_address['zip']);
		// $customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($setup_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					// echo "success_" . $tresponse->getTransId();
					// // echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					// echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					// echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					// echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					// echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
					if ($type == 'client') {
						$this->load->model('Model_clients');
						$clients = new Model_clients();
						$clients->load($id);
						$clients->converted = 1;
						$clients->converted_date = date('Y-m-d H:i:s');
						$clients->save();
					} else {
						$this->load->model('Model_client_joints');
						$client_joints = new Model_client_joints();
						$client_joints->load($id);

						$this->load->model('Model_clients');
						$clients = new Model_clients();
						$clients->load($client_joints->client_id);
						$clients->converted = 1;
						$clients->converted_date = date('Y-m-d H:i:s');
						$clients->save();
					}
						


					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com');

					$this->email->subject("Transaction Success");
					$this->email->message('Charged Setup Fee for '.$shipping_address['first_name'].' '.$shipping_address['last_name']);

					if ($this->email->send()) { 
					} else { 
					}
				} else {
					// echo "error_";
					// echo "Transaction Failed <br>";
					// if ($tresponse->getErrors() != null) {
					// 	echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					// 	echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					// }

					$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
					$message = "System failed to charge $full_name Setup Fee";
					$this->sendRCSms('+18139517686', $message);
 

					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com');

					$this->email->subject("Transaction Failed");
					$this->email->message($message);

					if ($this->email->send()) { 
					} else { 
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				// echo "error_";
				// echo "Transaction Failed <br>";
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getErrors() != null) {
					// echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					// echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				} else {
					// echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
					// echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				}

				$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
				$message = "System failed to charge $full_name Setup Fee";
				$this->sendRCSms('+18139517686', $message);
 

				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);
				$this->email->initialize($config);
				$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
				$this->email->to('joshuasaubon@gmail.com');

				$this->email->subject("Transaction Failed");
				$this->email->message($message);

				if ($this->email->send()) { 
				} else { 
				}
			}
		} else {
			//echo "No response returned <br>";
		}

		return $response;
	}

	function test() {
		$client_joint_id = $this->getClientJointIDByName('Mitchell Estill');
		$this->pprint($client_joint_id);
	}

	function getClientJointIDByName($name1) {
		$this->Model_Query->selects = ['client_joint_id'];
		$data = $this->Model_Query->getView('view_clients_table', ['joint_name' => $name1], 'client_joint_id', 'desc', 1);
		if (count($data) > 0) {
			$data = reset($data);
			$data = reset($data);
			return $data;
		} else {
			return false;
		}
	}

	function sendDocuLynxEmailCompleted($to, $href, $template_name, $subject) {
		$data['href'] = $this->get_encrypted_string($href);
		$data['template_name'] = $template_name;
		$data['to'] = $to;
		$data['subject'] = $subject;
		$body = $this->load->view('email_templates/DocuLynx_template_completed', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('info@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject('Completed: ' . $template_name);
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {

		}
	}

	function getClientIDByName($name) {
		$data = $this->fetchRawData("SELECT client_id FROM clients WHERE name='$name'");
		return $data[0]['client_id'];
	}

	function getTemplateEmailSettings() {
		$data = $this->fetchRawData("SELECT * FROM template_email_settings ");
		return $data;
	}

	function getTemplateIdByPlan($plan) {
		$plan = $this->fetchRawData("SELECT template_id FROM templates WHERE template_type = 'OH $plan' ORDER BY template_id DESC LIMIT 1");

		$template_id = 0;
		if (count($plan) > 0) {
			$template_id = $plan[0]['template_id'];
		}
		return array('plan' => $template_id);
	}

	function validate_client_username() {
		$username = $this->input->post('username');
		$type = $this->input->post('type');
		if ($type == 'joint') {
			$this->load->model('Model_client_joints');
			$client_joints = new Model_client_joints();
			$data = $client_joints->search(['username' => $username]);
			// $data = $this->fetchRawData("SELECT * FROM client_joints WHERE username='$username' ");
		} else {
			$this->load->model('Model_clients');
			$clients = new Model_clients();
			$data = $clients->search(['username' => $username]);
			// $data = $this->fetchRawData("SELECT * FROM clients WHERE username='$username' ");
		}

		echo count($data);
		// $this->pprint($data);
	}

	function validateCoupon() {
		$coupon_code = strtolower($this->input->post('coupon_code'));
		// $coupon_code = strtolower('HOLIDAY');
		$this->load->model('Model_coupons');
		$coupons = new Model_coupons();
		$data = $coupons->search(['LOWER(`coupon_code`)' => $coupon_code], '', '', 1);
		$data = reset($data);

		$response = [];
		if (!empty($data)) {
			$used = $data['used'];
			$limit = $data['limit'];
			$amount = $data['amount'];
			$date_start = date('Y-m-d', strtotime($data['date_start']));
			$date_end = date('Y-m-d', strtotime($data['date_end']));
			$date_today = date('Y-m-d');
			if ($date_today > $date_start && $date_today < $date_end) {
				if ($used <= $limit) {
					$response['status'] = 'success';
					$response['message'] = $amount;
					$coupons = new Model_coupons();
					$coupons->load($data['coupon_id']);
					$coupons->used = $used - 1;
				} else {
					$response['status'] = 'error';
					$response['message'] = "Sorry, this promotional offer has expired. Finish this form and call a Credit Specialist about extending this offer and applying the credit to your account.";
				}
			} else {
				$response['status'] = 'error';
				$response['message'] = "Sorry, this promotional offer has expired. Finish this form and call a Credit Specialist about extending this offer and applying the credit to your account.";
			}
		} else {
			$response['status'] = 'error';
			$response['message'] = "Sorry, the promotional code that you have entered does not exist.";

		}

		echo json_encode($response);
	}

	function sendDoculynxMergedSA($name, $to, $href_sa, $cell_phone) {
		$data['href_sa'] = $this->get_encrypted_string($href_sa);
		$data['to'] = $to;
		$data['subject'] = 'View ' . $name . ' Service Agreements';
		$body = $this->load->view('email_templates/DocuLynx_template_merged', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('info@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);
		$this->email->subject('Completed: Service Agreement');
		$this->email->message($body);
		if ($this->email->send()) {
			$this->email->initialize($config);
			$this->email->from('info@creditlynx.com', 'Credit Lynx');
			$this->email->to('info@creditlynx.com');
			$this->email->subject('Completed: Service Agreement');
			$this->email->message($body);
			// if ($this->email->send()) {
			// 	// echo "Emial has been sent";
			// 	// return true;
			// } else {

			$cell_phone = '+1'.$this->cleanPhoneNumber($cell_phone);
			$this->sendRCSms($cell_phone, "Congratulations!  Your service agreement was completed successfully and the signed agreement will be sent to you via email.");

			// }
		} else {

		}
	}

	function getPlanPrices() {
		$plan = $this->input->post('plan');
		$this->load->model('Model_subscription');
		$subscription = new Model_subscription();
		$where = [];
		if ($plan != 'All') {
			$where = ['name' => $plan];
		}
		$data = $subscription->search($where);
		echo json_encode($data);
	}

	function insert_new_lead_contact_us() {
		$post_fields = $this->input->post();
		unset($post_fields['g-recaptcha-response']);
		$this->load->model('Model_clients');
		$clients = new Model_clients();
		foreach ($post_fields as $field => $value) {
			$clients->$field = $value;
		}
		$clients->save();
		redirect('https://www.creditlynx.com/upload-successful/', 'refresh');
	}

	function testingUpdateCurrentLead() {
		$this->update_current_lead('X1Rs4XsQln2yX1Rs4XsQln10X1Rs4XsQlnNxif3N.V1CqKCB88azcD/OdHTp0CLY1/2nyXhK.VFMQluEJb60rXC');
	}

	function update_current_lead() {
		$session = $this->input->post('session');
		$data_session = $this->fetchRawData("SELECT * FROM signup_sessions WHERE session='$session' AND status = 0");
		if (count($data_session) > 0) {
			$data_session = reset($data_session);
			if ($data_session['type'] == 'client') {
				$id = $data_session['id'];
				$profile = $this->fetchRawData("SELECT * FROM clients WHERE client_id = $id");
				$billing = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id = $id");

			} else {
				$id = $data_session['id'];
				$profile = $this->fetchRawData("SELECT * FROM client_joints WHERE client_joint_id = $id");
				$billing = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id = $id");
			}

			$profile = reset($profile);
			$billing = reset($billing);

			$template_ids = $this->getTemplateIdByPlan($billing['plan']);
			$template_id_plan = $template_ids['plan'];
			if ($template_id_plan != 0) {
				$name = $profile['name'];
				$email = $profile['email_address'];
				$cell = $profile['cell_phone'];
				$date_of_birth = $profile['date_of_birth'];
				$ss = $profile['ss'];
				$message = 'Please review and sign.';

				$billings['billing_paid'] = $billing['paid'];
				$billings['setup_fee'] = $billing['setup_fee'];
				$billings['billing_monthly_due'] = date('d', strtotime($billing['monthly_due']));
				$billings['monthly_fee'] = $billing['monthly_fee'];
				$billings['billing_card_number'] = str_replace(' ', '', $billing['card_number']);
				$billings['billing_card_expiration'] = $billing['card_expiration'];
				$billings['billing_cvv_code'] = $billing['cvv_code'];
				$billings['billing_card_holder'] = $billing['card_holder'];
				$billings['billing_address'] = $billing['address'];
				$billings['billing_city'] = $billing['city'];
				$billings['billing_state'] = $billing['state'];
				$billings['billing_zip'] = $billing['zip'];

				$sent_id_sa = $this->saveNewTemplateSent($template_id_plan, $id, $name, $email, 'Credit Lynx Service Agreement', $message, $name, false, $billings, $date_of_birth, $ss);
				// Credit Lynx Service Agreement and Limited Letter of Authorization
				$href_sa = 'https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=' . $template_id_plan . '&id1=' . $id . '&id2=&name1=' . $name . '&email1=' . $email . '&name2=&email2=&subject=Credit Lynx Service Agreement&message=' . $message . '.';
				$href_sa = str_replace(' ', '+', $href_sa);
				$href_sa = str_replace('#', '', $href_sa);
				$href_sa = $href_sa . '&sent_id=' . $sent_id_sa;

				$this->sendDoculynxMergedSA($name, $email, $href_sa, $cell);

				$this->db->query("UPDATE clients SET lead_status='Need Payment' WHERE client_id=$id");
					

				$this->db->query("UPDATE signup_sessions SET status = 1 WHERE session='$session'");
				$sa_date = date('m/d/Y');
				if ($data_session['type'] == 'client') {
					$client_enrollments = $this->fetchRawData("SELECT * FROM client_enrollments WHERE client_id=$id");
					if (count($client_enrollments) > 0) {
						$this->db->query("UPDATE client_enrollments SET service_agreement = '$sa_date' WHERE client_id='$id'");
					} else {
						$this->db->query("INSERT INTO client_enrollments (client_id,service_agreement) VALUES ($id,'$sa_date')");
					}

					$note_date = date('Y-m-d H:i:s');
					$this->db->query("INSERT INTO notes 
						(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
						VALUES 
						('Clients','$id','Credit Lynx','0','LynxLogo.png','Credit Lynx','Signup Completed','$note_date','1')
					");
					
				} else {
					$client_joint_enrollments = $this->fetchRawData("SELECT * FROM client_joint_enrollments WHERE client_joint_id=$id");
					if (count($client_joint_enrollments) > 0) {
						$this->db->query("UPDATE client_joint_enrollments SET service_agreement = '$sa_date' WHERE client_joint_id='$id'");
					} else {
						$this->db->query("INSERT INTO client_joint_enrollments (client_joint_id,service_agreement) VALUES ($id,'$sa_date')");
					}

					$this->db->query("UPDATE client_joint_enrollments SET service_agreement = '$sa_date' WHERE client_joint_id='$id'");

					$note_date = date('Y-m-d H:i:s');
					$this->db->query("INSERT INTO notes 
						(note_assigned,note_assigned_id,sender,sender_id,sender_photo,sender_name,note,note_date,note_sticky) 
						VALUES 
						('Client Joints','$id','Credit Lynx','0','LynxLogo.png','Credit Lynx','Signup Completed','$note_date','1')
					");
				}
				 
				redirect('https://www.creditlynx.com/upload-successful/');
			} else {
				echo "There is a problem with your signup form, please contact adminitrator. Thank you.";
			}

		}
	}

}
