<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller { 
	
	public function index()
	{
		$this->checklog(); 
		$userdata['userdata'] = $this->session->userdata('user_data');
		$this->load->view('processors/includes/header.php',$userdata);
		$this->load->view('processors/includes/nav-left.php',$userdata);
		$this->load->view('processors/includes/nav-top.php',$userdata);
		$this->load->view('processors/Dashboard.php');
		$this->load->view('processors/includes/footer.php',$userdata);
	} 
	
}
