<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// date_default_timezone_set(timezone_name_from_abbr("EST"));
date_default_timezone_set('America/Los_Angeles');
// date_default_timezone_set('America/New_York');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');

define("AUTHORIZENET_LOG_FILE", "phplog");

require 'ringcentral/vendor/autoload.php';
use RingCentral\SDK\SDK;

class EmailCampaign extends MY_controller {

	function emailReminder() {
		$day = $this->input->get('day');
		$data = $this->fetchRawData("
			SELECT
				( SELECT name FROM clients WHERE client_id = client_billings.client_id ) AS `name` ,
				( SELECT email_address FROM clients WHERE client_id = client_billings.client_id ) AS `email_address` ,
				DATE_ADD( CURRENT_DATE, INTERVAL $day DAY ) as `due_date`,
				monthly_fee
			FROM
				client_billings
			WHERE
				client_id IN ( SELECT client_id FROM clients WHERE client_status = 'Active' AND converted = 1 )
				AND dayofmonth( str_to_date( `monthly_due`, '%m/%d/%Y' ) ) = DAY ( DATE_ADD( CURRENT_DATE, INTERVAL $day DAY ) )

			UNION ALL

			SELECT
				( SELECT name FROM client_joints WHERE client_joint_id = client_joint_billings.client_joint_id ) AS `name` ,
				( SELECT email_address FROM client_joints WHERE client_joint_id = client_joint_billings.client_joint_id ) AS `email_address` ,
				DATE_ADD( CURRENT_DATE, INTERVAL $day DAY ) as `due_date`,
				monthly_fee
			FROM
				client_joint_billings
			WHERE
				client_joint_id IN ( SELECT client_joint_id FROM client_joints WHERE client_status = 'Active' AND client_id IN (SELECT client_id FROM clients WHERE client_id=client_joints.client_id AND converted=1) )
				AND dayofmonth( str_to_date( `monthly_due`, '%m/%d/%Y' ) ) = DAY ( DATE_ADD( CURRENT_DATE, INTERVAL $day DAY ) )
		");

		foreach ($data as $key => $value) {
			$due_day = date('l', strtotime(date($value['due_date'])));
			$due_month = date('F', strtotime(date($value['due_date'])));
			$due_date = date('j', strtotime(date($value['due_date'])));
			$monthly_fee = $value['monthly_fee'];
			$email_address = $value['email_address'];
			$name = $value['name'];
			echo "<h2>$name</h2><input style='width: 100%' class='copyText' value='CreditLynx Upcoming Payment Reminder'><br>";
			$body = "<input style='width: 100%' class='copyText' value='$email_address'><br><textarea rows='10' class='copyText' style='width: 100%;white-space: pre-line;'>Hello!

This is a friendly notice that your monthly payment of \$$monthly_fee, will due on $due_day, $due_month $due_date.

Thank you!

Best,

CreditLynx Billing Team</textarea>";

			echo "$body<br><hr><br><hr><br><hr>";

		}

		echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
			<script>
				$('.copyText').on('click',function(){
					$(this).select();
					document.execCommand(\"copy\");
				});
			</script>";
	}

	function emailMissed() {
		$data = $this->fetchRawData("
			SELECT
				( SELECT name FROM clients WHERE client_id = client_billings.client_id ) AS `name` ,
				( SELECT email_address FROM clients WHERE client_id = client_billings.client_id ) AS `email_address` ,
				CURRENT_DATE as `due_date`,
				monthly_fee
			FROM
				client_billings
			WHERE
				client_id IN ( SELECT client_id FROM clients WHERE client_status = 'NSF' AND converted = 1 )
				AND dayofmonth( str_to_date( `monthly_due`, '%m/%d/%Y' ) ) = DAY ( CURRENT_DATE )

			UNION ALL


			SELECT
				( SELECT name FROM client_joints WHERE client_joint_id = client_joint_billings.client_joint_id ) AS `name` ,
				( SELECT email_address FROM client_joints WHERE client_joint_id = client_joint_billings.client_joint_id ) AS `email_address` ,
				CURRENT_DATE as `due_date`,
				monthly_fee
			FROM
				client_joint_billings
			WHERE
				client_joint_id IN ( SELECT client_joint_id FROM client_joints WHERE client_status = 'NSF' AND client_id IN (SELECT client_id FROM clients WHERE client_id=client_joints.client_id AND converted=1) )
				AND dayofmonth( str_to_date( `monthly_due`, '%m/%d/%Y' ) ) = DAY ( CURRENT_DATE )
		");

		foreach ($data as $key => $value) {
			$due_day = date('l', strtotime(date($value['due_date'])));
			$due_month = date('F', strtotime(date($value['due_date'])));
			$due_date = date('j', strtotime(date($value['due_date'])));
			$monthly_fee = $value['monthly_fee'];
			$email_address = $value['email_address'];
			$name = $value['name'];
			echo "<h2>$name</h2><input style='width: 100%' class='copyText' value='Oops, something is wrong with your CreditLynx account!'><br>";
			$body = "<input style='width: 100%' class='copyText' value='$email_address'><br>
<textarea rows='10' class='copyText' style='width: 100%;white-space: pre-line;'>Declined Notice

Status: Oops, something is wrong with your account!

Hello!

This is a friendly notice that your monthly payment of \$$monthly_fee, was due on $due_day, $due_month $due_date and it was not able to be processed. Please contact our billing team at 513-878-0181 immediately to make your missed payment.

Thank you!

Best,

CreditLynx Billing Team</textarea>";

			echo "$body<br><hr><br><hr><br><hr>";

		}

		echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
			<script>
				$('.copyText').on('click',function(){
					$(this).select();
					document.execCommand(\"copy\");
				});
			</script>";
	}



	

	function invoice() {
		$session = $this->input->get('id'); 
		$edit = $this->input->get('edit'); 
		$this->load->model('Model_client_invoices');
		$client_invoices = new Model_client_invoices();
		$invoice = $client_invoices->search(['session_id'=>$session]);
		$invoice = reset($invoice);

		$view_inv = $this->load->view('invoices/'.$invoice['invoice_no'],[],TRUE); 
		$script = ''; 
		$html = '';
		if ($edit == 'true') {
			$script .= "<script>
				var tbody_tr = $('.table tbody tr');
			    $.each(tbody_tr, function(index, tr) {
			        $(tr).find('td:nth-child(1)').find('b').append(' <a href=\"#\" class=\"text-danger btn_delete_row\"><i class=\"fa-sm fa fa-times\"></i></a>');
			    });

			    $('.table tbody').on('click', '.btn_delete_row', function(event) {
			        event.preventDefault();
			        var tr = $(this).closest('tr');
			        var amount = tr.find('td:nth-child(3)').html();
			        amount = parseFloat(amount.replace('$',''));
			        var tr_length = $('.table tbody tr').length; 
			        var sub_total_tr = $('.table tbody').find('tr:nth-child('+(tr_length - 2)+')').find('td:nth-child(3)').find('.sub_total'); 
			        var sub_total = parseFloat(sub_total_tr.html()); 

			        var balance_due_tr = $('.table tfoot').find('tr').find('.balance_due');;
			        var balance_due = parseFloat(balance_due_tr.html());   

			        sub_total_tr.html((sub_total - amount).toFixed(2));
			        $('.sub_total').html((sub_total - amount).toFixed(2));
			        balance_due_tr.html((balance_due - amount).toFixed(2));
			        tr.remove();

			        var pseudoElementContent = window.getComputedStyle($('.table-ribbon-footer')[0], ':before').getPropertyValue('top');
			        pseudoElementContent = pseudoElementContent.replace('px','');
			        pseudoElementContent = pseudoElementContent - 43;
			        pseudoElementContent = pseudoElementContent + 'px'; 
			        $('head').append('<style>.table-ribbon-footer:before{top: '+pseudoElementContent+' !important;}</style>');
			    });

			    $('.save_invoice').on('click',function(){
			    	var session_id = $('#session_id').val();  
			    	$('.btn_delete_row').remove();
			    	$(this).remove();
			    	var scripts = $('script').length;
			    	($('script')[scripts -1]).remove();
			    	var html_invoice = $('html').html();  
			    	$.post('save_invoice', {session_id,html_invoice}, function(data, textStatus, xhr) {
            	    	console.log(data);
            		});
		    	});
			</script>";

			$html = '<button class="save_invoice btn btn-danger btn-sm" style="position:fixed; top: 5px;right: 5px;">Save Invoice</button>';
		}
		if ($invoice['status'] == 'Paid') { 
				$script .=  "<script>
					document.addEventListener('DOMContentLoaded', function(){ 
					    $('.overlay').removeClass('hide');
					}, false);

				</script>"; 
		}

		

		if ($invoice['status'] == 'Scheduled') {
			$this->load->model('Model_clients_pay_later');
			$clients_pay_later = new Model_clients_pay_later();
			$pay_date = $clients_pay_later->search(['session_id'=>$session]); 
			if (count($pay_date) > 0) {
				$pay_date = reset($pay_date);
				$pay_date = date('m/d/Y',strtotime($pay_date['pay_date'])); 
				$script .=  "<script>
					document.addEventListener('DOMContentLoaded', function(){ 
					    $('.input_pay_later_date').val('$pay_date');
					    $('.input_pay_later_date').attr('disabled',true);
					}, false);

				</script>"; 
			}
		} 
			

		echo $view_inv.$html.$script;
			
	}

	function save_invoice() { 
		$session_id = $this->input->post('session_id');  
		$html_invoice = $this->input->post('html_invoice');  
		$this->load->model('Model_client_invoices');
		$client_invoices = new Model_client_invoices();
		$invoice = $client_invoices->search(['session_id'=>$session_id]);
		$invoice = reset($invoice);
		// echo $session_id; 
		// echo $html_invoice;
		$html_invoice = '<!DOCTYPE html>'.$html_invoice; 
		$invoice_path = FCPATH.'application/views/invoices/'.$invoice['invoice_no'].'.php'; 
		
		if ( ! write_file($invoice_path, $html_invoice))
		{
	        echo 'Unable to write the file';
		}
		else
		{
	        echo 'File written!';
		}

	}

	function receipt() {
		$receipt = $this->input->get('id');
		$this->load->view('receipts/'.$receipt);
	}

 

	function get_month_list($id,$type) {
 
		$last_trans = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions GROUP BY `name` )  
					ORDER BY
					NAME"
				); 

 		$return = [];
		foreach ($last_trans as $key => $value) {  
			// GET MONTH NAME OF LAST TRANSACTION 
			// CHECK IF STATUS IS DECLINED, THEN PROCEED
			if ($value['status'] == 'declined') { 
				$card_holder = addslashes($value['name']);
				$card_number = $value['account_number'];
				$data = $this->fetchRawData("SELECT IF(type='client',
								(SELECT client_status FROM clients WHERE client_id=id),
								(SELECT client_status FROM client_joints WHERE client_joint_id=id)) as `client_status`,
								IF(type='client',
								(SELECT name FROM clients WHERE client_id=id),
								(SELECT name FROM client_joints WHERE client_joint_id=id)) as `name`,
								id,type
								FROM view_client_cards WHERE RIGHT(card_number,4)=RIGHT('$card_number',4) AND LCASE(name)=LCASE('$card_holder')"
							);
 
				if (count($data) > 0) { 
					$data = reset($data);
					if ($data['id'] == $id && $data['type'] == $type) {  
						// GET Client NAME,TYPE,CLIENT STATUS AND ID  
						$restrictions = array('Cancelled','Terminated','Archived');

						// CHECK IF CLIENT STATUS IS NOT IN RESTRICTIONS
						if (!in_array($data['client_status'], $restrictions)) {
							// CONDITION THAT ONLY EMMA's ACCOUNT WILL GO THROUGH THE PROCESS
							// NOTE THAT EMMA's Client ID is 33 
								// GET LAST SUCCESSFUL TRANSACTION TO GET MONTH NAME OF NEXT DECLINED SERVICE 
								$month_list = $this->get_last_success_transaction($card_number,$card_holder);

		 						$return = count($month_list) > 0 ? $month_list : [] ;
							
						}
					} 
				}
			} 	
		}

		return $return;
			
	}

	function get_last_success_transaction($card_number,$card_holder) {
		$last_success_trans = $this->fetchRawData("SELECT submitted_local FROM authorize_transactions WHERE name='$card_holder' AND RIGHT(account_number,4)=RIGHT('$card_number',4) AND status = 'settledSuccessfully' ORDER BY submitted_local DESC LIMIT 1");
		$last_success_trans = reset($last_success_trans); 
		$month_now = date('F');
		$month_name = date('F',strtotime($last_success_trans['submitted_local']));
		$month_list = array();
		$counter = 1;
		while ($month_now != $month_name) {
			$month_name = date('F',strtotime($last_success_trans['submitted_local'] . " + $counter months"));
			array_push($month_list, $month_name);
			$counter++; 
		} 
		return $month_list;
	}



	function email_welcome() {
		$data['body'] = 'There was an issue with you recent payment.  Please click the link below to review your account information.';
		$data['href'] = 'http://sandbox.creditlynx.com/emailCampaign/invoice';
		$data['name'] = 'Joshua';
		$data['to'] = 'joshuasaubon@gmail.com';
		$data['username'] = 'joshuasaubon@gmail.com';
		$data['password'] = '09109680920';
		$body = $this->load->view('email_templates/invoice_template', $data, TRUE);

		echo $body;

		// $this->load->library('email');
		// $config = Array(
		// 	'mailtype' => 'html',
		// );

		// $this->email->initialize($config);
		// $this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		// $this->email->to(['joshuasaubon@gmail.com', 'claire042297@gmail.com', 'brent@creditlynx.com', 'kayci@creditlynx.com']);
		// $this->email->subject("Welcome to the program!");
		// $this->email->message($body);
		// if ($this->email->send()) {
		// 	// echo "Emial has been sent";
		// 	// return true;
		// } else {
		// 	echo "error";
		// }
	}

	function Unsubscribe() {
		echo "under maintinance";
	}

	

	function saveNewTemplateSent($template_id, $client_id, $name, $email, $subject, $message, $signature,$billings = []) {
      $href = 'https://admin.creditlynx.com/DocuLynx/template?action=sent&template_id=' . $template_id . '&id1=' . $client_id . '&id2=&name1=' . $name . '&email1=' . $email . '&name2=&email2=&subject=' . $subject . '&message=' . $message . '.';
      $href = str_replace(' ', '+', $href);

      $href = str_replace('#', '', $href);

      if ($subject == 'Credit Lynx Federal Notice of Cancellation') {
        $href = $href . '&viewer=name1';
      }

      $this->load->model('Model_template_sent');
      $template_sent = new Model_template_sent();
      $template_sent->href = $href;
      $template_sent->template_id = $template_id;
      $template_sent->client_id = $client_id;
      $template_sent->name1 = $name;
      $template_sent->email1 = $email;
      $template_sent->name2 = '';
      $template_sent->email2 = '';
      $template_sent->subject = $subject;
      $template_sent->message = $message;
      $template_sent->date_sent = date('Y-m-d H:i:s');
      $template_sent->save();

      $data_get_tfields = $this->fetchRawData("SELECT * FROM template_fields WHERE name1 IS NULL AND name2 IS NULL AND email1 IS NULL AND email2 IS NULL AND template_id=$template_id");

      foreach ($data_get_tfields as $key => $field) {
          $input_container = $field['input_container'];
          $drop_container = $field['drop_container'];
          $field_id = $field['field_id'];
          $style = $field['style'];
          $value = ''; 
          if ($field_id == 'input_fullname1') {
            $value = $name;
          }
          if ($field_id == 'signature1') {
            $value = $signature;
          }
          if ($field_id == 'date_signed1') {
            $value = date('m/d/Y h:i:s A');
          } 
          if ($field_id == 'cancel_date') {
            $value = date('m/d/Y', strtotime(date('m/d/y') . ' + 3 days'));
          }
          if (count($billings) > 0) { 
            if ($field_id == 'input_name_on_card') {
              $value = $billings['card_holder'];
            }
            if ($field_id == 'input_card_number') {
              $value = $billings['card_number'];
            }
            if ($field_id == 'input_expiration') {
              $value = $billings['card_expiration'];
            }
            if ($field_id == 'input_cvv') {
              $value = $billings['cvv_code'];
            }
            if ($field_id == 'input_address') {
              $value = $billings['address'];
            }
            if ($field_id == 'input_city') {
              $value = $billings['city'];
            }
            if ($field_id == 'input_state') {
              $value = $billings['state'];
            }
            if ($field_id == 'input_zip') {
              $value = $billings['zip'];
            }
          }
             

          $value = addslashes($value);
          $value = strip_tags($value);
          $data_insert = $this->db->query("INSERT INTO template_fields (template_id,name1,email1,name2,email2,input_container,drop_container,field_id,style,value) VALUES ('$template_id','$name','$email','','','$input_container','$drop_container','$field_id','$style','$value')");

      } 
      $this->updateTemplateSentStatusSignup($template_sent->sent_id, 'Completed');
      

      return $template_sent->sent_id;
  	}

  	function updateTemplateSentStatusSignup($sent_id, $status) {
      $date_lc = date('Y-m-d H:i:s');
      // if ($status == 'Completed') {
      $template_email_settings = $this->getTemplateEmailSettings();
      if ($template_email_settings[0]['for_sender_is_completed'] == 'on') {
          $data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,name1,name2 FROM template_sent WHERE sent_id=$sent_id");

          $name1 = $data_sent[0]['name1'];
          $name2 = $data_sent[0]['name2'];

          $href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
          $template_name = $data_sent[0]['template_name'];
          $subject = $name1 . ' completed ' . $template_name;
          $this->sendDocuLynxEmailCompleted('docs@creditlynx.com',$href,$template_name,$subject);
          if ($name2 != '') {
              $subject = $name2 . ' completed ' . $template_name;
              $this->sendDocuLynxEmailCompleted('docs@creditlynx.com',$href,$template_name,$subject);
          }

      }

      if ($template_email_settings[0]['for_signers_is_completed'] == 'on') {

          $data_sent = $this->fetchRawData("SELECT (SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name`,href,email1,email2,name1,name2 FROM template_sent WHERE sent_id=$sent_id");
          $href = $data_sent[0]['href'] . '&sent_id=' . $sent_id;
          $template_name = $data_sent[0]['template_name'];
          $email1 = $data_sent[0]['email1'];
          $email2 = $data_sent[0]['email2'];
          $name1 = $data_sent[0]['name1'];
          $name2 = $data_sent[0]['name2'];

          if ($template_name != 'Esignature Disclosure' && $template_name != 'Federal Disclosure' && $template_name != 'Cancellation' && $template_name != 'Payment Change Request Form' && $template_name != 'New Payment Authorization Form') {
              $template_name = 'Service Agreement';
          }

          $subject = $name1 . ' completed ' . $template_name;
          if ($status == 'Completed') {
              $this->sendDocuLynxEmailCompleted($email1,$href,$template_name,$subject);
          } else {
              $this->sendDocuLynxEmailCompleted($email1,$href.'&viewer=name1',$template_name,$subject);
          }

          $client_joint_id = $this->getClientJointIDByName($name1);
          if ($client_joint_id != false) {
              $this->load->model('Model_client_joint_documents');
              $client_joint_documents = new Model_client_joint_documents();
              $client_joint_documents->client_joint_id = $client_joint_id;
              $client_joint_documents->category = $template_name;
              $client_joint_documents->file_download = $href;
              $client_joint_documents->file_size = '';
              $client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
              $client_joint_documents->save();
          } else {
              $client_id = $this->getClientIDByName($name1);
              $this->load->model('Model_client_documents');
              $client_documents = new Model_client_documents();
              $client_documents->client_id = $client_id;
              $client_documents->category = $template_name;
              $client_documents->file_download = $href;
              $client_documents->file_size = '';
              $client_documents->date_uploaded = date('Y-m-d H:i:s');
              $client_documents->save();
          }
      }
      // } else {

      // }
      $data = $this->db->query("UPDATE template_sent SET status='$status',last_changed='$date_lc' WHERE sent_id='$sent_id'");
      // echo "updated";
  	}

  	function getTemplateEmailSettings() {
	      $data = $this->fetchRawData("SELECT * FROM template_email_settings ");
	      return $data;
  	}

  	function getClientJointIDByName($name1) {
	      $this->Model_Query->selects = ['client_joint_id'];
	      $data = $this->Model_Query->getView('view_clients_table', ['joint_name' => $name1], 'client_joint_id', 'desc', 1);
	      if (count($data) > 0) {
	          $data = reset($data);
	          $data = reset($data);
	          return $data;
	      } else {
	          return false;
	      }
  	}

  	function getClientIDByName($name) {
      	$data = $this->fetchRawData("SELECT client_id FROM clients WHERE name='$name'");
      	return $data[0]['client_id'];
  	}


  	function send_new_payment_form($billings,$id,$type) { 
 
	    if ($type == 'client') {
	        $this->load->model('Model_clients');
	        $clients = new Model_clients();
	        $clients->load($id);

	        $subject = 'Credit Lynx New Payment Authorization Form';
	        $message = 'Please review and sign.';
	        $this->saveNewTemplateSent(52, $id, $clients->name, $clients->email_address, $subject, $message, $clients->name,$billings); 
	    } else {

	        $this->load->model('Model_client_joints');
	        $client_joints = new Model_client_joints();
	        $client_joints->load($id);

	        $subject = 'Credit Lynx New Payment Authorization Form';
	        $message = 'Please review and sign.';
	        $this->saveNewTemplateSent(52, $id, $client_joints->name, $client_joints->email_address, $subject, $message, $client_joints->name,$billings); 
	    }


  	}

  	function sendDocuLynxEmailCompleted($to, $href, $template_name, $subject) {
	    $data['href'] = $href;
	    $data['template_name'] = $template_name;
	    $data['to'] = $to;
	    $data['subject'] = $subject;
	    $body = $this->load->view('email_templates/DocuLynx_template_completed', $data, TRUE);

	    $this->load->library('email');
	    $config = Array(
	      'mailtype' => 'html',
	    );

	    $this->email->initialize($config);
	    $this->email->from('info@creditlynx.com', 'Credit Lynx');
	    $this->email->to($to);
	    $this->email->subject('Completed: ' . $template_name);
	    $this->email->message($body);
	    if ($this->email->send()) {
	      // echo "Emial has been sent";
	      // return true;
	    } else {

	    }
  	}
 
	function chargeAuthorizeCreditCardMonthly($email, $plan, $monthly_fee, $card_info, $shipping_address) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Monthly Fee For Overdue Services');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		$customerAddress->setCompany($shipping_address['company']);
		$customerAddress->setAddress($shipping_address['address']);
		$customerAddress->setCity($shipping_address['city']);
		$customerAddress->setState($shipping_address['state']);
		$customerAddress->setZip($shipping_address['zip']);
		$customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($monthly_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					echo "success_" . $tresponse->getTransId();
					// echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
				} else {
					echo "error_";
					echo "Transaction Failed <br>";
					if ($tresponse->getErrors() != null) {
						echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
						echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				echo "error_";
				echo "Transaction Failed <br>";
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getErrors() != null) {
					echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				} else {
					echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
					echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				}
			}
		} else {
			echo "No response returned <br>";
		}

		return $response;
	}



	function pay_later_date() {
		$session = $this->input->post('session');
		$pay_date = $this->input->post('pay_later_date');

		$session_data = $this->fetchRawData("SELECT * FROM client_invoices WHERE session_id='$session'");
		$session_data = reset($session_data); 

		$id = $session_data['id'];
		$type = $session_data['type'];
		$status = $session_data['status'];
 
		if ($status == 0) {
			$pay_later_date = $this->fetchRawData("SELECT * FROM clients_pay_later WHERE session_id='$session'");
			if (count($pay_later_date) == 0) {
				$this->load->model('Model_clients_pay_later');
				$clients_pay_later = new Model_clients_pay_later();
				$clients_pay_later->id = $id;
				$clients_pay_later->session_id = $session;
				$clients_pay_later->type = $type;
				$clients_pay_later->pay_date = date('Y-m-d',strtotime($pay_date));
				$clients_pay_later->save(); 


				if ($type == 'client') {
					$this->load->model('Model_clients');
					$clients = new Model_clients();
					$clients->load($id);
					$full_name = $clients->name;
				} else { 
					$this->load->model('Model_client_joints');
					$client_joints = new Model_client_joints();
					$client_joints->load($id);
					$full_name = $client_joints->name;
				}


				$this->load->model('Model_notes');
				$notes = new Model_notes();

				if ($type == 'joint') {
					$notes->note_assigned = 'Client Joints';
					$notes->note_assigned_id = $id;
				} else {
					$notes->note_assigned = 'Clients';
					$notes->note_assigned_id = $id;
				}
				$notes->sender = 'System';
				$notes->sender_id = 0;
				$notes->sender_photo = 'LynxLogo.png';
				$notes->sender_name = 'Credit Lynx';
				$notes->note = 'Payment Rescheduled to '.date('m/d/Y',strtotime($pay_date));
				$notes->note_date = date('Y-m-d H:i:s');
				$notes->note_sticky = 0;
				$notes->save();


				
				
				$message = $full_name.' Payment Rescheduled to '.date('m/d/Y',strtotime($pay_date));
				$this->sendRCSms('+18139517686', $message);
 

				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);
				$this->email->initialize($config);
				$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
				$this->email->to('joshuasaubon@gmail.com');

				$this->email->subject("Invoice Rescheduled");
				$this->email->message($message);

				if ($this->email->send()) { 
					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('brent@creditlynx.com');

					$this->email->subject("Invoice Rescheduled");
					$this->email->message($message);

					if ($this->email->send()) { 
					} else { 
					}
				} else { 
				}


				$this->db->query("UPDATE client_invoices SET status='Scheduled' WHERE session_id='$session'");;
			} 
				
		}
			

	}

	function use_other_account() {
		$session = $this->input->post('session');
		$session_data = $this->fetchRawData("SELECT * FROM client_invoices WHERE session_id='$session'");
		$session_data = reset($session_data); 

		$id = $session_data['id'];
		$type = $session_data['type'];
		$status = $session_data['status'];

		if ($session_data['type'] == 'client') {
			$client = $this->fetchRawData("SELECT * FROM clients WHERE client_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else { 
			$client = $this->fetchRawData("SELECT * FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing); 
		}

		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$client_other_cards->card_holder = $billing['card_holder'];
		$client_other_cards->card_number = $billing['card_number'];
		$client_other_cards->card_exp = $billing['card_expiration'];
		$client_other_cards->card_cvv = $billing['cvv_code'];
		$client_other_cards->shipping_address = $billing['address'];
		$client_other_cards->shipping_city = $billing['city'];
		$client_other_cards->shipping_state = $billing['state'];
		$client_other_cards->shipping_zip = $billing['zip'];
		$client_other_cards->id = $id;
		$client_other_cards->type = $type;
		$client_other_cards->save();

		// echo $id;
		if ($type == 'client') {
			$this->load->model('Model_client_billings');
			$client_billings = new Model_client_billings();
			$client_billings->load($billing['client_billing_id']);
			$client_billings->card_holder = $this->input->post('billing_card_holder');
			$client_billings->card_number = $this->input->post('billing_card_number');
			$client_billings->card_expiration = $this->input->post('billing_card_expiration');
			$client_billings->cvv_code = $this->input->post('billing_cvv_code');
			$client_billings->address = $this->input->post('billing_address');
			$client_billings->city = $this->input->post('billing_city');
			$client_billings->state = $this->input->post('billing_state');
			$client_billings->zip = $this->input->post('billing_zip');
			$client_billings->save(); 
			// $this->pprint($client_billings);
			
		} else {
			$this->load->model('Model_client_joint_billings');
			$client_joint_billings = new Model_client_joint_billings();
			$client_joint_billings->load($billing['client_billing_id']);
			$client_joint_billings->card_holder = $this->input->post('billing_card_holder');
			$client_joint_billings->card_number = str_replace(' ', '', $this->input->post('billing_card_number'));
			$client_joint_billings->card_expiration = $this->input->post('billing_card_expiration');
			$client_joint_billings->cvv_code = $this->input->post('billing_cvv_code');
			$client_joint_billings->address = $this->input->post('billing_address');
			$client_joint_billings->city = $this->input->post('billing_city');
			$client_joint_billings->state = $this->input->post('billing_state');
			$client_joint_billings->zip = $this->input->post('billing_zip');
			$client_joint_billings->save();
		}

		$new_billing['card_holder'] = $this->input->post('billing_card_holder');
		$new_billing['card_number'] = str_replace(' ', '', $this->input->post('billing_card_number'));
		$new_billing['card_expiration'] = $this->input->post('billing_card_expiration');
		$new_billing['cvv_code'] = $this->input->post('billing_cvv_code');
		$new_billing['address'] = $this->input->post('billing_address');
		$new_billing['city'] = $this->input->post('billing_city');
		$new_billing['state'] = $this->input->post('billing_state');
		$new_billing['zip'] = $this->input->post('billing_zip');

		$this->send_new_payment_form($new_billing,$id,$type);

		$pay_date = $this->input->post('pay_date');
		if (isset($pay_date)) {
			if ($status == 0) {
				$pay_later_date = $this->fetchRawData("SELECT * FROM clients_pay_later WHERE session_id='$session'");
				if (count($pay_later_date) == 0) {
					$this->load->model('Model_clients_pay_later');
					$clients_pay_later = new Model_clients_pay_later();
					$clients_pay_later->id = $id;
					$clients_pay_later->session_id = $session;
					$clients_pay_later->type = $type;
					$clients_pay_later->pay_date = date('Y-m-d',strtotime($pay_date));
					$clients_pay_later->save(); 

					$this->db->query("UPDATE client_invoices SET status='Scheduled' WHERE session_id='$session'");;
				} 
			}
				
		} else {
			$name = explode(' ', $this->input->post('billing_card_holder'));
	        $first_name = '';
	        $last_name = $name[count($name) -1];
	        if (count($name) > 2) {
	             $name = substr($name, count($name) - 1); 
	             $first_name = implode(' ', $name);
	        } else {
	            $first_name = $name[0];
	        }

			$shipping_address['first_name'] = $first_name;
			$shipping_address['last_name'] = $last_name;
			$shipping_address['company'] = '';
			$shipping_address['address'] = $this->input->post('billing_address');
			$shipping_address['city'] = $this->input->post('billing_city');
			$shipping_address['state'] = $this->input->post('billing_state');
			$shipping_address['zip'] = $this->input->post('billing_zip');
			$shipping_address['country'] = 'USA';
			$shipping_address['phone_number'] = $client['cell_phone'];
			$shipping_address['fax_number'] = '';

			$card_info['number'] = str_replace(' ', '', $this->input->post('billing_card_number'));
			$card_info['expiration_date'] = $this->input->post('billing_card_expiration');
			$card_info['code'] = $this->input->post('billing_cvv_code');

			// $this->pprint($this->input->post());
			echo "Charge this card";

			$fee = $this->input->post('fee');
			$this->chargeAuthorizeCreditCardMonthly($client['email_address'], $billing['plan'], $fee, $card_info, $shipping_address);
			$this->db->query("UPDATE client_invoices SET status='Paid' WHERE session='$session'");
			$this->db->query("DELETE FROM clients_pay_later WHERE session_id='$session'");

		}
	}

	function pay_now() {
		$session = $this->input->post('session');
		$this->load->model('Model_client_invoices');
		$client_invoices = new Model_client_invoices();
		$session_data = $client_invoices->search(['session_id'=>$session,'status'=>0]);

		// $session_data = $this->fetchRawData("SELECT * FROM client_invoices WHERE session_id='$session' AND status=0");
		$session_data = reset($session_data); 

		$id = $session_data['id'];
		$type = $session_data['type'];

		if ($session_data['type'] == 'client') {
			$client = $this->fetchRawData("SELECT * FROM clients WHERE client_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else { 
			$client = $this->fetchRawData("SELECT * FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing); 
		}

		$name = explode(' ', $client['name']);
        $first_name = '';
        $last_name = $name[count($name) -1];
        if (count($name) > 2) {
             $name = substr($name, count($name) - 1); 
             $first_name = implode(' ', $name);
        } else {
            $first_name = $name[0];
        }

		$shipping_address['first_name'] = $first_name;
		$shipping_address['last_name'] = $last_name;
		$shipping_address['company'] = '';
		$shipping_address['address'] = $billing['address'];
		$shipping_address['city'] = $billing['city'];
		$shipping_address['state'] = $billing['state'];
		$shipping_address['zip'] = $billing['zip'];
		$shipping_address['country'] = 'USA';
		$shipping_address['phone_number'] = $client['cell_phone'];
		$shipping_address['fax_number'] = '';

		$card_info['number'] = str_replace(' ', '', $billing['card_number']);
		$card_info['expiration_date'] = $billing['card_expiration'];
		$card_info['code'] = $billing['cvv_code'];

		echo "charge this card";
		$fee = $this->input->post('fee');
		$this->chargeAuthorizeCreditCardMonthly($client['email_address'], $billing['plan'], $fee, $card_info, $shipping_address);
		$this->db->query("UPDATE client_invoices SET status='Paid' WHERE session_id='$session'");
		$this->db->query("DELETE FROM clients_pay_later WHERE session_id='$session'");
	}

	function testing_alert_template() {
		$data['to'] = 'joshuasaubon@gmail.com';
		$data['alert_subject'] = 'Pending Equifax Reports';
		$data['alert_note'] = 'Please upload the Equifax reports as soon as possible. Their responses will be in plain, white envelopes and appear as junk mail.';
		$this->load->view('email_templates/alert_email_template', $data);
	}

	function bulk_sms() {
		$data = $this->fetchRawData("SELECT * FROM (SELECT
													'client' AS `type`,
													`clients`.`client_id` AS `id`,
													`clients`.`email_address` AS `email_address` ,
													clients.client_status,
													cell_phone
												FROM
													`clients` WHERE converted=1 UNION ALL
												SELECT
													'joint' AS `type`,
													`client_joints`.`client_joint_id` AS `id`,
													`client_joints`.`email_address` AS `email_address` ,
													client_joints.client_status,
													cell_phone
												FROM
													`client_joints` WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1) UNION ALL
												SELECT
													`client_other_emails`.`type` AS `type`,
													`client_other_emails`.`id` AS `id`,
													`client_other_emails`.`email_address` AS `email_address` ,
													IF(type = 'client', (SELECT client_status FROM clients WHERE client_id=id LIMIT 1),(SELECT client_status FROM client_joints WHERE client_joint_id=id LIMIT 1)) as `client_status`,
													IF(type = 'client', (SELECT cell_phone FROM clients WHERE client_id=id LIMIT 1),(SELECT cell_phone FROM client_joints WHERE client_joint_id=id LIMIT 1)) as `cell_phone`
												FROM
													`client_other_emails`) as aaa WHERE aaa.client_status IN ('Active', 'Review', 'AV', 'AV2', 'AVSSN', 'Verifications' ) LIMIT 30 OFFSET 120");
		  
		$to_numbers = array_column($data, 'cell_phone');
		foreach ($to_numbers as $key => $value) {
			if ($value == '') {
				unset($to_numbers[$key]);
			} else {
				$to_numbers[$key] = $this->phone_trim($value);
			} 
		}

		$message = 'Hey there! We noticed that many clients’ dashboards do not appear to be up to date as far as alerts go. So we apologize for this inconvenience and are working on syncing the information in order for you to view what we see in our system. Thank you and have a great day!';
		$this->sendBulkRCSmsManual($to_numbers,$message); 
		$this->pprint($to_numbers);

	}

	function sendBulkRCSmsManual($to_numbers,$message) {  
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$for_send_numbers = array();
		foreach ($to_numbers as $key => $value) {
			array_push($for_send_numbers, array('phoneNumber'=>$value));
		}
		foreach ($for_send_numbers as $key => $to_number) {
			$response = $platform
				->post('/account/~/extension/~/sms', array(
					'from' => array('phoneNumber' => $credentials['smsNumber']),
					'to' => array($to_number),
					'text' => $message,
				)); 
		} 

		 
	}

	
}
