<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_companies extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checklog();  
	}
	public function index() { 
		$data['userdata'] = $this->session->userdata('user_data');
	 	$data['page_controller'] = 'Partner_companies'; 
	 	$data['partner_companies'] = $this->get_partner_companies($this->input->get('partner_type'));
		$this->load->view('admin/Partner_companies.php', $data);

	} 

	function get_partner_companies($partner_type) {
		$data = $this->fetchRawData("SELECT * FROM partner_companies WHERE partner_type='$partner_type'");
		return $data;
	}

	function save_new_company() {
		$this->load->model('Model_partner_companies');
		$partner_companies = new Model_partner_companies();
		$partner_companies->company_name = $this->input->post('company_name');
		$partner_companies->partner_type = $this->input->post('partner_type');
		$partner_companies->save();
		redirect(base_url('admin/partner_companies?partner_type='.$this->input->post('partner_type')));
	}
 
 
}
 