<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require '3rd-party/PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Reports extends MY_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
		$data['userdata'] = $this->session->userdata('user_data');
		$data['page_title'] = 'Reports';
		$data['page_title_s'] = 'Reports';
		$data['page_controller'] = 'reports';
		$data['client_statuses'] = $this->getClientStatuses();
		$data['lead_statuses'] = $this->getLeadStatuses();
		$data['brokers'] = $this->getBrokers();
		$data['agents'] = $this->getAgents();
		$data['sales'] = $this->getSales();
		$this->load->view('admin/Reports.php', $data);
	}

	function getClientStatuses() {
		$data = $this->fetchRawData("SELECT * FROM client_statuses ORDER BY client_status");
		return $data;
	}
	function getLeadStatuses() {
		$data = $this->fetchRawData("SELECT * FROM lead_statuses ORDER BY lead_status");
		return $data;
	}
	function getBrokers() {
		$data = $this->fetchRawData("SELECT CONCAT(first_name,' ',last_name) as `broker` FROM brokers ORDER BY broker");
		return $data;
	}
	function getAgents() {
		$data = $this->fetchRawData("SELECT CONCAT(first_name,' ',last_name) as `agent` FROM brokers ORDER BY agent");
		return $data;
	}
	function getSales() {
		$data = $this->fetchRawData("SELECT name FROM employees WHERE type='Sales Team' ORDER BY name ASC");
		return $data;
	}

	function exportFilter() {
		$type = $this->input->get('type');
		$field = $this->input->get('field');
		$value = $this->input->get('value');
		$min = $this->input->get('min');
		$max = $this->input->get('max');

		if ($field == 'dfr') {
			$data = $this->getFilterList($type, $field, '', $min, $max);
		} else {
			$data = $this->getFilterList($type, $field, $value);
		}

		$this->generateClientsDataExcel($type.' '.$value, $data);
	}

	function generateClientsDataExcel($filename, $data) {
		// // Create new Spreadsheet value
		$spreadsheet = new Spreadsheet();

		// // Set document properties
		$spreadsheet->getProperties()->setCreator('Credit Lynx')
			->setTitle('Credit Lynx List');

		$sheet = $spreadsheet->getActiveSheet(0);

		// Get Clients data
		// Get Headers

		// $this->pprint($data);
		// Add Data
		for ($i = 0; $i < count($data) - 1; $i++) {
			// echo $data[$i]['Name'];
			$sheet->setCellValue('A' . $i, $data[$i]['Name']);
			$sheet->setCellValue('B' . $i, $data[$i]['Phone']);
			$sheet->setCellValue('C' . $i, $data[$i]['Email']);
		}
		// // Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('List');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		$this->downloadExcel($spreadsheet, $filename );
		// echo "<script>window.close();</script>";
	}

	function getFilterList($type, $field, $value, $min = 0, $max = 0) {
		if ($type == 'Client') {
			$converted = 1;
		} else {
			$converted = 0;
		}
		if ($field == 'dfr') {
			$result = $this->fetchRawData("SELECT
			 								name as `Name`,
			 								cell_phone as `Phone`,
			 								email_address as `Email`
			 							FROM view_export_list WHERE converted=$converted AND $field BETWEEN $min AND $max ORDER BY date_created DESC");
		} else {
			if ($value != '') {
				$result = $this->fetchRawData("SELECT
			 								name as `Name`,
			 								cell_phone as `Phone`,
			 								email_address as `Email`
			 							FROM view_export_list WHERE converted=$converted AND $field='$value' ORDER BY date_created DESC");
			} else {
				$result = $this->fetchRawData("SELECT
			 								name as `Name`,
			 								cell_phone as `Phone`,
			 								email_address as `Email`
			 							FROM view_export_list WHERE converted=$converted ORDER BY date_created DESC");
			}
			
		}

		$excel_headers = array('Name' => 'Name', 'Phone' => 'Phone', 'Email' => 'Email');
		array_unshift($result, $excel_headers);
		return $result;
	}

	function test() { 
		$result = $this->fetchRawData("SELECT
			 								name as `Name`,
			 								cell_phone as `Phone`,
			 								email_address as `Email`,
			 								date_created as `Date Created`
			 							FROM view_export_list WHERE converted=0 WHERE YEAR(date_created)=2019 AND MONTH(date_created) IN (8,9) ORDER BY date_created DESC"); 
			
		

		$excel_headers = array('Name' => 'Name', 'Phone' => 'Phone', 'Email' => 'Email','Date Created' => 'Date Created');
		array_unshift($result, $excel_headers);
		return $result;
	}

	function downloadExcel($spreadsheet, $filename) {
		// // Redirect output to a client’s web browser (Xlsx)
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
		header('Cache-Control: max-age=0');
		// // If you're serving to IE 9, then the following may be needed
		// header('Cache-Control: max-age=1');

		// // If you're serving to IE over SSL, then the following may be needed
		// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		// header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Csv');
		$writer->save('php://output');
	}

}
