<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->checklog();
		$this->load->model('Model_tasks');
		$this->load->model('Model_client_billings');
		$this->load->model('Model_client_joint_billings');
		$this->load->model('Model_clients');
	}
	public function index() {
		$data['userdata'] = $this->session->userdata('user_data');
		// $data['appts'] = $this->getAppointmentsForThisWeek();
		$data['task_temp'] = $this->TaskTemp();
		$data['total_clients'] = $this->getTotalClients();
		$data['total_leads'] = $this->getTotalLeads();
		$data['all_payment_today'] = $this->getAllPaymentToday();
		$data['close_rate'] = $this->get_close_rate();
		$data['pending_waiting'] = $this->getPendingWaitingDocuLynx();
		$data['total_active_clients'] = $this->getTotalActiveClients();
		$data['proj_sales'] = $this->get_projected_sales();
		$data['avg_income_per_client'] = $this->get_avg_income_per_client();

		$this->load->view('admin/Dashboard.php', $data);
	}

	function get_projected_sales() {
		$declined_rate = $this->get_declined_rate();
		$total_amount = $this->get_clients_monthly_fees(); 
		$proj_sales = $total_amount * (1.00 - $declined_rate);
		return $proj_sales;
	}

	function get_avg_income_per_client() {
		$data = $this->fetchRawData("SELECT
										SUM(amount) 
									FROM
										authorize_transactions 
									WHERE
										LCASE( `name` ) IN ( SELECT
										LCASE(`name`) 
									FROM
										view_client_cards 
									WHERE
									IF
										( type = 'client', id IN ( SELECT client_id FROM clients WHERE client_id=id AND converted = 1 AND client_status NOT IN ( 'Archived', 'Cancelled', 'Terminated' )) , id IN (SELECT client_joint_id FROM client_joints WHERE client_joint_id=id AND client_id IN (SELECT client_id FROM clients WHERE converted=1) AND client_status NOT IN (client_status NOT IN ('Archived','Cancelled','Terminated'))))
										GROUP BY LCASE(`name`)  )");
		$data = reset($data);
		$total_amount = reset($data);

		$active_clients = $this->getTotalActiveClients();

		return number_format($total_amount / $active_clients , 2);


	}

	function get_declined_rate() {
		$total_clients = $this->getTotalActiveClients();

		$total_declined = $this->fetchRawData("SELECT
													* 
												FROM
													authorize_transactions 
												WHERE
													YEAR ( submitted_local ) = YEAR ( CURRENT_DATE ) 
													AND MONTH ( submitted_local ) = MONTH ( CURRENT_DATE ) 
													AND NAME NOT IN (
												SELECT NAME 
												FROM
													authorize_transactions 
												WHERE
													STATUS IN ( 'settledSuccessfully', 'capturedPendingSettlement' ) 
													AND YEAR ( submitted_local ) = YEAR ( CURRENT_DATE ) 
													AND MONTH ( submitted_local ) = MONTH ( CURRENT_DATE ) 
													) AND account_number NOT IN (
												SELECT account_number 
												FROM
													authorize_transactions 
												WHERE
													STATUS IN ( 'settledSuccessfully', 'capturedPendingSettlement' ) 
													AND YEAR ( submitted_local ) = YEAR ( CURRENT_DATE ) 
													AND MONTH ( submitted_local ) = MONTH ( CURRENT_DATE ) 
													) GROUP BY NAME");
		$declined_rate = (count($total_declined) / $total_clients); 

		return $declined_rate;
	}

	function get_clients_monthly_fees() {
		$data_client = $this->fetchRawData("SELECT SUM(monthly_fee) FROM client_billings WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')) AND monthly_due <> ''");
		$data_joint = $this->fetchRawData("SELECT SUM(monthly_fee) FROM client_joint_billings WHERE client_joint_id IN (SELECT client_joint_id FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1) AND client_status NOT IN ('Archived','Cancelled','Terminated')) AND monthly_due <> ''");
		$data_client = reset($data_client);
		$data_client = reset($data_client);
		$data_joint = reset($data_joint);
		$data_joint = reset($data_joint);
		return $data_joint + $data_client;
	}

	function get_close_rate() {
		$data_clients = $this->fetchRawData("SELECT * FROM clients WHERE converted=1 AND YEAR(converted_date)=YEAR(CURRENT_DATE) AND MONTH(converted_date)=MONTH(CURRENT_DATE)");
		$data_client_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=client_joints.client_id  AND YEAR(converted_date)=YEAR(CURRENT_DATE) AND MONTH(converted_date)=MONTH(CURRENT_DATE))");
		$count_client = count($data_clients) + count($data_client_joints);

		$data_leads = $this->fetchRawData("SELECT * FROM clients WHERE YEAR(date_created)=YEAR(CURRENT_DATE) AND MONTH(date_created)=MONTH(CURRENT_DATE)");
		$data_lead_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE client_id=client_joints.client_id  AND YEAR(date_created)=YEAR(CURRENT_DATE) AND MONTH(date_created)=MONTH(CURRENT_DATE))");
		$count_lead = count($data_leads) + count($data_lead_joints);

		// $this->pprint($data_clients);
		// $this->pprint($data_client_joints);
 	// 	echo $count_client;
 	// 	echo "<br>";
 	// 	echo $count_lead;
 	// 	echo "<br>"; 
		return ($count_lead == 0 ? 0 : number_format($count_client / $count_lead,2)) ;
	}

	function getAllPaymentToday() {
		$data = $this->Model_Query->getView('view_all_payments_today', ['DAY(STR_TO_DATE(`monthly_due`,"%m/%d/%Y"))' => date('d'),'auto_alert <>' => 1]);
		return $data;
	}

	function getAllPaymentTodayTest() {
		$data = $this->Model_Query->getView('view_all_payments_today', ['DAY(STR_TO_DATE(`monthly_due`,"%m/%d/%Y"))' => date('d'),'auto_alert <>' => 1]);
		// return $data;
		$this->pprint($data);
		echo $this->db->last_query();
	}

	public function TaskTemp() {
		$query = $this->Model_Query->get_table('task_templates');
		return $query;
	}

	public function getTemplate() {
		$id = $this->input->post('task_template_id');
		$query = $this->Model_Query->sel_table('task_templates', 'task_template_id', $id);
		echo json_encode($query->result_array());
	}

	function getPendingWaitingDocuLynx() {
		$data = $this->fetchRawData("SELECT *,(SELECT template_name FROM templates WHERE template_id=template_sent.template_id) as `template_name` FROM template_sent WHERE status<>'Completed' AND status <> 'voided' ORDER BY sent_id DESC");
		return count($data);
	}

	function getTotalClients() {
		$data = $this->fetchRawData("SELECT
										sum( user_count ) AS `user_count`
									FROM
										(
									SELECT
										count( client_id ) AS `user_count`
									FROM
										clients
									WHERE
										( ( client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ) OR client_status IS NULL )
										AND converted = 1 UNION ALL
									SELECT
										count( client_joint_id ) AS `user_count`
									FROM
										client_joints
										INNER JOIN clients ON client_joints.client_id = clients.client_id AND clients.client_type='Joint' AND (clients.client_status <> 'Archived' OR clients.client_status IS NULL) and clients.converted=1) AS aaa
									");
		return $data[0]['user_count'];
	}

	function getTotalActiveClients() {
		$data_clients = $this->fetchRawData("SELECT * FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$data_client_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=client_joints.client_id) AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$count = count($data_clients) + count($data_client_joints);
		return $count;
	}

	public function getAllEmails() {
		$query = $this->Model_Query->get_table_limit('all_emails');
		$fullname = '';
		$converted = '';
		$data = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$id = $rows->agent_id;
				$table = $rows->type_table;
				if ($table == 'agent') {
					$get = $this->Model_Query->sel_table('agents', 'agent_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
					}
				} elseif ($table == 'broker') {
					$get = $this->Model_Query->sel_table('brokers', 'broker_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
					}
				} elseif ($table == 'client') {
					$get = $this->Model_Query->sel_table('clients', 'client_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->name;
						$converted = $getr->converted;
						$client_type = $getr->client_type;
					}
				}
				$data[] = array(
					'id' => $id,
					'fullname' => str_replace('\\', '', $fullname),
					'date' => $rows->date,
					'subject' => str_replace('\\', '', $rows->subject),
					'message' => str_replace('\\', '', $rows->message),
					'converted' => $converted,
					'account_type' => $table,
					'client_type' => $client_type
				);
			}
		}
		echo json_encode($data);
	}

	public function getAllEtext() {
		$query = $this->Model_Query->get_table_limit('all_etext');
		$data = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$id = $rows->client_id;
				$table = $rows->type_table;
				if ($table == 'agent') {
					$get = $this->Model_Query->sel_table('agents', 'agent_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
					}
				} elseif ($table == 'broker') {
					$get = $this->Model_Query->sel_table('brokers', 'broker_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
					}
				} elseif ($table == 'client') {
					$get = $this->Model_Query->sel_table('clients', 'client_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->name;
						$converted = $getr->converted;
						$client_type = $getr->client_type;
					}
				}
				$data[] = array(
					'id' => $id,
					'fullname' => str_replace('\\', '', $fullname),
					'date' => $rows->date,
					'subject' => str_replace('\\', '', $rows->title),
					'message' => str_replace('\\', '', $rows->text_message),
					'converted' => $converted,
					'account_type' => $table,
					'client_type' => $client_type
				);
			}
		}
		echo json_encode($data);
	}

	public function getAllNotif() {
		$query = $this->Model_Query->get_table_limit('all_notif');
		$fullname = '';
		$converted = '';
		$data = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$id = $rows->agent_id;
				$table = $rows->type_table;
				$client_type = '';
				if ($table == 'agent') {
					$get = $this->Model_Query->sel_table('agents', 'agent_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
						$client_type = 'agent';
					}
				} elseif ($table == 'broker') {
					$get = $this->Model_Query->sel_table('brokers', 'broker_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->first_name . ' ' . $getr->last_name;
						$converted = 0;
						$client_type = 'broker';
					}
				} elseif ($table == 'from_client') {
					$get = $this->Model_Query->sel_table('clients', 'client_id', $id);
					foreach ($get->result() as $getr) {
						$fullname = $getr->name;
						$converted = $getr->converted;
						$client_type = $getr->client_type;
					}
				}
				$data[] = array(
					'id' => $id,
					'fullname' => str_replace('\\', '', $fullname),
					'date' => $rows->date,
					'subject' => str_replace('\\', '', $rows->subject),
					'message' => trim(strip_tags(str_replace('\\', '', $rows->message))),
					'converted' => $converted,
					'notif_type' => $rows->notif_type,
					'account_type' => $table,
					'client_type' => $client_type
				);
			}
		}
		echo json_encode($data);
	}

	public function getAccount() {
		$account_type = $this->input->post('account_type');
		// $account_type = 'Broker';
		$fullname = '';
		$id = '';
		$data = array();
		if ($account_type == 'Lead') {
			$query = $this->Model_Query->sel_table('clients', 'converted', '0');
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = array(
						'id' => $row->client_id,
						'fullname' => $row->name,
					);
				}
			}
		} elseif ($account_type == 'Client') {
			$query = $this->Model_Query->sel_table('clients', 'converted', '1');
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = array(
						'id' => $row->client_id,
						'fullname' => $row->name,
					);
				}
			}
		} elseif ($account_type == 'Agent') {
			$query = $this->Model_Query->get_table('agents');
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = array(
						'id' => $row->agent_id,
						'fullname' => $row->first_name . ' ' . $row->last_name,
					);
				}

			}
		} elseif ($account_type == 'Broker') {
			$query = $this->Model_Query->get_table('brokers');
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = array(
						'id' => $row->broker_id,
						'fullname' => $row->first_name . ' ' . $row->last_name,
					);
				}
			}
		}
		echo json_encode($data);
	}

	function getTotalLeads() {
		$data = $this->fetchRawData("SELECT
										sum( user_count ) AS `user_count`
									FROM
										(
									SELECT
										count( client_id ) AS `user_count`
									FROM
										clients
									WHERE
										( ( client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ) OR client_status IS NULL )
										AND converted = 0 UNION ALL
									SELECT
										count( client_joint_id ) AS `user_count`
									FROM
										client_joints
										INNER JOIN clients ON client_joints.client_id = clients.client_id AND clients.client_type='Joint' AND (clients.client_status <> 'Archived' OR clients.client_status IS NULL) and clients.converted=0) AS aaa
									");
		return $data[0]['user_count'];
	}

	public function getAppointmentsForThisWeekDay() {
		$date_start = $this->input->post('date_start');
		// $date_start = '2018-11-10 10:00:00';
		$query = $this->Model_Query->sel_tableL('view_appointments', 'date_start', $date_start);
		echo json_encode($query->result_array());
	}

	function getAppointmentsForThisWeek() {
		$userdata = $this->session->userdata('user_data');
		if ($userdata['login_type'] == 'Administrator') {
			$data = $this->fetchRawData("SELECT * FROM view_appointments WHERE WEEKOFYEAR(date_start) = WEEKOFYEAR(NOW()) GROUP BY date_start");
		}

		// return $data;
		echo json_encode($data);
	}

	function getTasksForThisWeek() {
		// $userdata = $this->session->userdata('user_data');
		// $user_id = $userdata['user_id'];

		$data = $this->Model_Query->getView('view_tasks', ['task_active' => 0], 'task_date', 'DESC');
		// $data = $this->fetchRawData("SELECT * FROM tasks WHERE task_active=0 ORDER by task_id desc");
		// return $data;
		echo json_encode($data);

	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveTask() {

		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$task_id = $this->input->post('account_id');
		$task_name = $this->input->post('account_names');
		$task_ass = $this->input->post('account_type');
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];

		// $query = $this->Main_Query->sel_table('tasks', 'task_assigned_id', $task_id);
		// foreach ($query->result() as $key => $value) {
		// 	# code...
		// }

		$this->load->model('Model_tasks');
		$tasks = new Model_tasks();

		$tasks->task_assigned = $task_ass;
		$tasks->task_assigned_id = $task_id;
		$tasks->task_assigned_name = $task_name;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = date('Y-m-d H:i:s', strtotime($task_date));
		$tasks->task_active = 0;
		$tasks->save();
		// redirect(base_url('admin/dashboard'));
	}

	function getLeadsFromMonthYear() {
		$date = $this->input->post('date');
		$direction = $this->input->post('direction');

		if ($direction == 'init') {
			$data_arr['date'] = $date;
			$data_arr['month'] = date('m', strtotime($date));
			$data_arr['year'] = date('Y', strtotime($date));
		} else if ($direction == 'prev') {
			$data_arr['date'] = date('Y-m-d', strtotime($date . '-1 months'));
			$data_arr['month'] = date('m', strtotime($data_arr['date']));
			$data_arr['year'] = date('Y', strtotime($data_arr['date']));
		} else {
			$data_arr['date'] = date('Y-m-d', strtotime($date . '+1 months'));
			$data_arr['month'] = date('m', strtotime($data_arr['date']));
			$data_arr['year'] = date('Y', strtotime($data_arr['date']));
		}

		$days = cal_days_in_month(0, $data_arr['month'], $data_arr['year']);
		$data_arr['leads'] = [];
		$data_arr['clients'] = [];
		for ($i = 1; $i <= $days; $i++) {
			$data_arr['leads'][$i] = 0;
			$data_arr['clients'][$i] = 0;
		}

		$clients = new Model_clients();
		$clients->selects = ['(SUM(IF(client_type="Joint",2,0))+SUM(IF(client_type<>"Joint",1,0))) as `count`', 'DAY(date_created) as `day`'];
		$data_leads = $clients->searchWithSelect([
			'YEAR(date_created)' => $data_arr['year'],
			'MONTH(date_created)' => $data_arr['month'
			]], 0, 0, '', '', 'DATE(date_created)');
		// echo $this->db->last_query();
		foreach ($data_leads as $key => $value) {
			$data_arr['leads'][$value['day']] = $value['count'];
		}

		$clients = new Model_clients();
		$clients->selects = ['(SUM(IF(client_type="Joint",2,0))+SUM(IF(client_type<>"Joint",1,0))) as `count`', 'DAY(converted_date) as `day`'];
		$data_clients = $clients->searchWithSelect([
			'converted' => 1,
			'YEAR(converted_date)' => $data_arr['year'],
			'MONTH(converted_date)' => $data_arr['month'],
		], 0, 0, '', '', 'DATE(converted_date)');

		// echo $this->db->last_query();
		foreach ($data_clients as $key => $value) {
			$data_arr['clients'][$value['day']] = $value['count'];
		}

		echo json_encode($data_arr);

	}

	function get_active_clients_by_year_month() {
		$data_clients = $this->fetchRawData("SELECT count(client_id) as `count`', 'DAY(date_created) as `day` FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')"); 

		return $count;
		// return count($data_clients);

	}

	function processors() {
		$data['userdata'] = $this->session->userdata('user_data'); 
		$data['need_action'] = $this->get_need_action();
		$data['active_clients'] = $this->active_clients();
		$data['month_leads'] = $this->month_leads();
		$data['month_sales'] = $this->month_sales();
		$data['review_clients'] = $this->get_review_clients();
		$data['verif_clients'] = $this->get_verif_clients();
		$data['all_clients'] = $this->getAllClients();
		$this->load->view('admin/dashboard/Processors.php', $data);
	}


	function getAllClients() {
		$this->Model_Query->selects = ['client_id', 'name', 'client_type', 'client_joint_id', 'joint_name', 'cell_phone', 'email_address', 'ss', 'joint_cell_phone', 'joint_email_address', 'joint_ss'];
		$data = $this->Model_Query->getView('view_clients_table', ['converted' => 1], 'client_id', 'DESC');
		return $data;
	}

	function sales() {
		$data['userdata'] = $this->session->userdata('user_data'); 
		$data['all_clients'] = $this->getAllLeads();
		$this->load->view('admin/dashboard/Sales.php', $data);
	}


	function getAllLeads() {
		$this->Model_Query->selects = ['*', 'IF(STR_TO_DATE(monthly_due, \'%d,%m,%Y\') IS NOT NULL,DAY(STR_TO_DATE(monthly_due, \'%d,%m,%Y\')),monthly_due) as `monthly_due_formated`'];
		$data = $this->Model_Query->getView('view_clients_table', ['converted' => 0], 'client_id', 'DESC');
		return $data;
	}

	function active_clients() {
		$data_clients = $this->fetchRawData("SELECT * FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$data_client_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=client_joints.client_id) AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$count = count($data_clients) + count($data_client_joints);
		return $count;
		// return count($data_clients);

	}

	function month_leads() {
		$data_clients = $this->fetchRawData("SELECT * FROM clients WHERE client_status NOT IN ('Archived','Cancelled','Terminated') AND YEAR(date_created)=YEAR(CURRENT_DATE) AND MONTH(date_created)=MONTH(CURRENT_DATE)");
		$data_client_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE client_id=client_joints.client_id AND YEAR(date_created)=YEAR(CURRENT_DATE) AND MONTH(date_created)=MONTH(CURRENT_DATE)) AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$count = count($data_clients) + count($data_client_joints);
		return $count;
		// return count($data_clients);

	}

	function month_sales() {
		$data_trans = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE YEAR(submitted_local)=YEAR(CURRENT_DATE) AND MONTH(submitted_local)=MONTH(CURRENT_DATE) AND status IN ('settledSuccessfully','capturedPendingSettlement','refundSettledSuccessfully')");
		$amount = 0;
		foreach ($data_trans as $key => $value) {
			if ($value['status'] == 'refundSettledSuccessfully') {
				$amount -= $value['amount'];
			} else {
				$amount += $value['amount'];
			}
				
		}

		return $amount;
	}

	function get_need_action() {
		$data = $this->fetchRawData("SELECT * FROM view_client_dfrs WHERE dfr <= 5 ORDER BY dfr ASC");
		return $data;
	}

	function get_review_clients() {
		$data['dfr_nulls'] = $this->fetchRawData("	
										SELECT * FROM (SELECT (SELECT STR_TO_DATE(ss_proof_received,'%m/%d/%Y') FROM client_enrollments WHERE client_id=view_clients.client_id) as `ss_date`,name,client_id as `id`,'client' as `type` FROM view_clients WHERE converted=1 AND client_status='Review' AND dfr IS NULL
											UNION ALL
										SELECT (SELECT STR_TO_DATE(ss_proof_received,'%m/%d/%Y') FROM client_joint_enrollments WHERE client_joint_id=view_client_joints.client_joint_id) as `ss_date`,name,client_joint_id as `id`,'joint' as `type` FROM view_client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=view_client_joints.client_id) AND client_status='Review'  AND dfr IS NULL) as aaa ORDER BY aaa.ss_date ASC
									");
		$data['social_date_order'] = $this->fetchRawData("	
										SELECT * FROM (SELECT (SELECT STR_TO_DATE(ss_proof_received,'%m/%d/%Y') FROM client_enrollments WHERE client_id=view_clients.client_id) as `ss_date`,name,client_id as `id`,'client' as `type` FROM view_clients WHERE converted=1 AND client_status='Review' AND dfr IS NOT NULL
											UNION ALL
										SELECT (SELECT STR_TO_DATE(ss_proof_received,'%m/%d/%Y') FROM client_joint_enrollments WHERE client_joint_id=view_client_joints.client_joint_id) as `ss_date`,name,client_joint_id as `id`,'joint' as `type` FROM view_client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=view_client_joints.client_id) AND client_status='Review'  AND dfr IS NOT NULL) as aaa ORDER BY aaa.ss_date ASC
									");
		return $data;

	}
	function get_verif_clients() {
		$data = $this->fetchRawData("	
										SELECT name,client_id as `id`,'client' as `type` FROM clients WHERE converted=1 AND client_status IN ('Verifications','AV','AVSSN','AV2','SSN')
											UNION ALL
										SELECT name,client_joint_id as `id`,'joint' as `type` FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=client_joints.client_id) AND client_status IN ('Verifications','AV','AVSSN','AV2','SSN')
									");
		return $data;

	}

}
