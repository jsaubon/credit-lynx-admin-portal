<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checklog();
		$this->load->model('Model_task_templates');
		$this->load->model('Model_subscription');
		$this->load->model('Model_gameplan_timeline');
		$this->load->model('Model_creditors_account_type');
		$this->load->model('Model_creditors_recommendations');
		$this->load->model('Model_client_gameplan_points');
		$this->load->model('Model_client_joint_gameplan_points');
		$this->load->model('Model_game_plan_accounts');
		$this->load->model('Model_client_gameplan');
		$this->load->model('Model_client_joint_gameplan');
		$this->load->model('Model_letter_templates');
		$this->load->model('Model_clients_account_types');
		$this->load->model('Model_clients_dispute_verbiages');
		$this->load->model('Model_clients_dispute_verbiage_assignments');
		$this->load->model('Model_clients_account_statuses');
		$this->load->model('Model_clients_account_payment_statuses');
		$this->load->model('Model_clients_account_comments');
		$this->load->model('Model_client_statuses');
		$this->load->model('Model_lead_statuses');
		$this->load->model('Model_clients');
		$this->load->model('Model_client_joints');
		$this->load->model('Model_client_enrollments');
		$this->load->model('Model_client_billings');
		$this->load->model('Model_client_joint_enrollments');
		$this->load->model('Model_client_joint_billings');
		$this->load->model('Model_client_etexts');
		$this->load->model('Model_client_emails');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
		$this->load->model('Model_client_account_details');
		$this->load->model('Model_client_joint_account_details');
		$this->load->model('Model_client_alerts');
		$this->load->model('model_client_alerts_templates');
		$this->load->model('Model_clients_dispute_verbiages_assigned');
		$this->load->model('Model_client_joint_rounds');
		$this->load->model('Model_client_rounds');
		$this->load->model('Model_support_tickets');
		$this->load->model('Model_support_ticket_responses');
		$this->load->model('Model_client_joint_documents');
		$this->load->model('Model_client_documents');
		$this->load->model('Model_letter_creditors');
		$this->load->model('Model_client_assignments');
		$this->load->model('Model_client_joint_assignments');
		$this->load->model('Model_client_joint_customer_profile');
		$this->load->model('Model_client_customer_profile');
		$this->load->model('Model_creditors_account');
		$this->load->model('Model_bureau_letters_template_headers');
		$this->load->model('Model_authorize_transactions');
		$this->load->model('Model_authorize_subscriptions');
		$this->load->model('Model_clients_fax');

	}
	public function index() {
		$get_client_id = $this->input->get('id');
		$get_client_type = $this->input->get('type');
		$data['get_by'] = $this->input->get('by');
		$data['get_order'] = $this->input->get('order');
		$data['go'] = $this->input->get('go');
		$data['filter'] = $this->input->get('filter');
		$data['search'] = $this->input->get('search');
		if ($get_client_type == 'joint') {
			$client_id = $this->getClientId($get_client_id);
			$data['from_email'] = $get_client_id . '_' . $get_client_type . '_' . $client_id;
		} else {
			$data['from_email'] = $get_client_id . '_' . $get_client_type;
		}
		$data['userdata'] = $this->session->userdata('user_data');
		// $data['users'] = $this->getClients($data['get_by'],$data['get_order']);
		$data['all_clients'] = $this->getAllClients();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Clients';
		$data['page_title_s'] = 'Client';
		$data['page_controller'] = 'clients';
		$data['page_folder'] = 'clients';

		$data['clients_account_types'] = $this->getClientsAccountTypes();
		$data['subscription_type'] = $this->subscription_type();
		$data['note_templates'] = $this->getNoteTemplatesInit();
		$data['call_templates'] = $this->getCallTemplatesInit();
		$data['task_templates'] = $this->getTaskTemplatesInit();
		$data['alert_templates'] = $this->getAlertTemplatesInit();
		
		$data['state_provinces'] = $this->getStateProvinces();
		$data['agents'] = $this->getAgents();
		$data['brokers'] = $this->getBrokers();
		$data['sales'] = $this->getSales();
		$data['processors'] = $this->getProcessors();
		$data['client_statuses'] = $this->getClientStatuses();
		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses();
		$data['dispute_verbiages'] = $this->getDisputeVerbiages();
		$data['template_headers'] = $this->getTemplateHeaders();

		$this->load->view('admin/Clients.php', $data);

	}

	function send_verif_email() {
		$email_address = $this->input->post('email_address');
		$name = $this->input->post('name');
		$name = explode(' ', $name);
		
		$name = $name[0];
		


		$subject = 'WHAT TO EXPECT';
		$note = 'Once your verifications or other required documents have been submitted, our Dispute Team will be able to begin challenging the questionable negative items on your report to see if they are meeting the requirements outlined in the Fair Credit Reporting Act (FCRA).


According to the FCRA, the three credit bureaus (Equifax, Experian, TransUnion) and your creditors have a reasonable amount of time to verify the information on your report. If they cannot verify the information within the federally regulated 30-day time frame, or it is found to be inaccurate, then they are required to either correct it or remove it.


When their investigations are complete, they should be sending you responses. Typically, clients get them within 45-50 days, or sooner, from when the dispute letters were drafted and sent. This process remains the same for each round attempted on your behalf.';

		$this->send_custom_email($subject,$name,$note,$email_address);
	}


	function get_alert_templates_init() {
		$this->load->model('Model_client_alerts_templates');
		$client_alerts_templates = new Model_client_alerts_templates();
		$data = $client_alerts_templates->search([], 'template_shortname', 'ASC');
		echo json_encode($data);
	}

	function getCreditorLetterTitles() {
		$letter_templates = new Model_letter_templates();
		$data = $letter_templates->search(['template_header' => 'Creditor Letters']);
		echo json_encode($data);
	}

	function deleteBureauLetterTemplateTitle() {
		$blt_id = $this->input->post('blt_id');

		$letter_templates = new Model_letter_templates();
		$letter_templates->load($blt_id);
		$letter_templates->delete();
	}

	function saveBureauLettersTemplateTitle() {
		$blt_id = $this->input->post('blt_id');
		$template_header = $this->input->post('template_header');
		$template_title = $this->input->post('template_title');

		$letter_templates = new Model_letter_templates();
		if ($blt_id != '') {
			// $letter_templates->blt_id = $blt_id;
			$letter_templates->load($blt_id);
		}
		$letter_templates->template_header = $template_header;
		$letter_templates->template_title = $template_title;
		$letter_templates->save();
	}

	function deleteBureauLetterTemplateHeader() {
		$blth_id = $this->input->post('blth_id');
		$bureau_letters_template_headers = new Model_bureau_letters_template_headers();
		$bureau_letters_template_headers->load($blth_id);
		$bureau_letters_template_headers->delete();
	}

	function saveBureauLettersTemplateHeader() {
		$template_header = $this->input->post('template_header');
		$blth_id = $this->input->post('blth_id');

		$bureau_letters_template_headers = new Model_bureau_letters_template_headers();
		if ($blth_id != '') {
			$bureau_letters_template_headers->blth_id = $blth_id;
		}
		$bureau_letters_template_headers->template_header = $template_header;
		$bureau_letters_template_headers->save();
	}

	function getBureauLetterTemplateHeaders() {
		$bureau_letters_template_headers = new Model_bureau_letters_template_headers();
		$data = $bureau_letters_template_headers->search([]);
		echo json_encode($data);
	}

	function getTableClientsLeads() {

		if ($this->input->post('page_title') == 'Leads') {
			$order = 'client_id';
			$by = 'DESC';
		} else {
			$order = 'converted_date';
			$by = 'DESC';
		}

		$limit = 10;
		$offset = 0;
		if ($this->input->post('page_title') == 'Leads') {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'paid',
				'broker',
				'agent',
				'lead_status',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'joint_paid',
				'broker',
				'agent',
				'lead_status',
				'client_joint_id',
			];
		} else {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'paid',
				'cycle',
				'client_status',
				'dfr',
				'broker',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'joint_paid',
				'joint_cycle',
				'joint_client_status',
				'joint_dfr',
				'broker',
				'client_joint_id',
			];
		}

		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i);
					if ($order == 'cycle') {
						$this->db->where('cycle !=', '');
					}

					if ($order == 'dfr') {
						$this->db->where('dfr !=', '');
					}
					if ($order == 'paid') {
						$this->db->where('paid !=', '');
					}

					if ($this->input->post('page_title') == 'Clients') {
						$order = $order . ',converted_date';
					} else {
						$order = $order . ',client_id';
					}

				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search to get all data
		if ($this->input->post('sSearch') != '') {
			$filter = $this->input->post('filter');
			$search = $this->trim_str($this->input->post('sSearch'));
			if ($filter != "0") {
				if ($filter == 'cycle' || $filter == 'dfr') {
					$this->db->group_start();
					$this->db->or_where($filter, $search);
					$this->db->or_where('joint_' . $filter, $search);
					$this->db->group_end();
				} else if ($filter == 'NSF' || $filter == 'NSF2' || $filter == 'Terminated') {
					$this->db->group_start();
					$this->db->or_where('client_status', $filter);
					$this->db->or_where('joint_client_status', $filter);
					$this->db->group_end();
					if ($search != '') {
						$this->db->group_start();
						$this->db->or_like('name', $search);
						$this->db->or_like('joint_name', $search);
						$this->db->group_end();
					}

				} else {
					$this->db->like($filter, $search);
				}
			} else {
				$this->db->group_start();
				foreach ($aColumns as $key => $value) {
					$this->db->or_like($value, $search);
				}
				foreach ($bColumns as $key => $value) {
					$this->db->or_like($value, $search);
				}
				$this->db->group_end();
			}

			if ($this->input->post('page_title') == 'Leads') {
				$this->db->where('converted', 0);
			} else {
				$this->db->where('converted', 1);
			}

		} else {
			if ($this->input->post('page_title') == 'Leads') {
				$this->db->where('converted', 0);
			} else {
				$this->db->where('converted', 1);
			}

			$filter = $this->input->post('filter');

			if ($filter == 'NSF' || $filter == 'NSF2' || $filter == 'Terminated') {
				$this->db->group_start();
				$this->db->or_where('client_status', $filter);
				$this->db->or_where('joint_client_status', $filter);
				$this->db->group_end();
			} else {
				$this->db->where('client_status !=', 'Archived');
				$this->db->where('client_status !=', 'Cancelled');
				$this->db->where('client_status !=', 'Terminated');
			}

		}

		// $this->db->group_by('client_id');

		$userdata = $this->session->userdata('user_data');
		$login_type = $userdata['login_type'];
		$user_id = $userdata['user_id'];
		if ($login_type == 'Sales Team') {
			$this->db->where('sale_id',$user_id);
		}
		$count_all_data = $this->Model_Query->getView('view_clients_table', [], $order, $by, 0, 0);

		//search to get diplay data
		if ($this->input->post('sSearch') != '') {
			$filter = $this->input->post('filter');
			$search = $this->trim_str($this->input->post('sSearch'));
			if ($filter != "0") {
				if ($filter == 'cycle' || $filter == 'dfr') {
					$this->db->group_start();
					$this->db->or_where($filter, $search);
					$this->db->or_where('joint_' . $filter, $search);
					$this->db->group_end();
				} else if ($filter == 'NSF' || $filter == 'NSF2' || $filter == 'Terminated') {
					$this->db->group_start();
					$this->db->or_where('client_status', $filter);
					$this->db->or_where('joint_client_status', $filter);
					$this->db->group_end();
					if ($search != '') {
						$this->db->group_start();
						$this->db->or_like('name', $search);
						$this->db->or_like('joint_name', $search);
						$this->db->group_end();
					}

				} else {
					$this->db->like($filter, $search);
				}
			} else {
				$this->db->group_start();
				foreach ($aColumns as $key => $value) {
					$this->db->or_like($value, $search);
				}
				foreach ($bColumns as $key => $value) {
					$this->db->or_like($value, $search);
				}
				$this->db->group_end();
			}

			if ($this->input->post('page_title') == 'Leads') {
				$this->db->where('converted', 0);
			} else {
				$this->db->where('converted', 1);
			}

		} else {
			if ($this->input->post('page_title') == 'Leads') {
				$this->db->where('converted', 0);
			} else {
				$this->db->where('converted', 1);
			}

			$filter = $this->input->post('filter');

			if ($filter == 'NSF' || $filter == 'NSF2' || $filter == 'Terminated') {
				$this->db->group_start();
				$this->db->or_where('client_status', $filter);
				$this->db->or_where('joint_client_status', $filter);
				$this->db->group_end();
			} else {
				$this->db->where('client_status !=', 'Archived');
				$this->db->where('client_status !=', 'Cancelled');
				$this->db->where('client_status !=', 'Terminated');
			}

		}

		if ($login_type == 'Sales Team') {
			$this->db->where('sale_id',$user_id);
		}
		$dataTable = $this->Model_Query->getView('view_clients_table', [], $order, $by, $limit, $offset);

		$last_query = $this->db->last_query();
		$client_statuses = $this->getClientStatuses();
		$lead_statuses = $this->getLeadStatuses();

		$client_ids = array_filter(array_column($dataTable, 'client_id'), 'strlen');
		$client_joint_ids = array_filter(array_column($dataTable, 'client_joint_id'), 'strlen');

		$clients_date_paid = count($client_ids) > 0 ? $this->getClientsDatePaid($client_ids) : [];
		$joints_date_paid = count($client_joint_ids) > 0 ? $this->getJointsDatePaid($client_joint_ids) : [];

		$data['data'] = [];
		if ($this->input->post('page_title') == 'Leads') {
			foreach ($dataTable as $key => $value) {

				$lead_status = $value['lead_status'];
				$lead_status_options = '';
				foreach ($lead_statuses as $kkk => $vvv) {
					if ($vvv['lead_status'] != 'Archived') {
						if ($vvv['lead_status'] == $lead_status) {
							$lead_status_options .= '<option selected value="' . $vvv['lead_status'] . '">' . $vvv['lead_status'] . '</option>';
						} else {
							$lead_status_options .= '<option value="' . $vvv['lead_status'] . '">' . $vvv['lead_status'] . '</option>';
						}
					}
				}
				$select_lead_status = "<select client_id=" . $value['client_id'] . " class='selectLeadStatus' >
											$lead_status_options
										</select>";

				// TD
				// NAME
				$name = "<a href='#' class='goToProfile'> <u>" . $value['name'] . "</u> </a>";
				$joint_name = "<a value='" . $value['client_joint_id'] . "' href='#' class='goToJointProfile'><u>" . $value['joint_name'] . "</u></a>";
				// CELL PHONE
				$cell_phone = "<a client_type='client' class='btnCallClientRC' href='#'>" . $value['cell_phone'] . "</a> ";
				$joint_cell_phone = "<a client_type='joint' class='btnCallClientRC' href='#'>" . $value['joint_cell_phone'] . "</a>";
				// EMAIL
				$email_address = $value['email_address'];
				$joint_email_address = $value['joint_email_address'];
				// LAST ALERT
				// $last_alert = $value['last_alert'] ? date('m/d/Y h:i:s a', strtotime($value['last_alert'])) : '';
				// $joint_last_alert = $value['joint_last_alert'] ? date('m/d/Y h:i:s a', strtotime($value['joint_last_alert'])) : '';
				// PAID
				$paid = '<span style="color: transparent;font-size: 0.5px">' . strtotime($value['setup_fee_date']) . '</span>' . $value['setup_fee_date'];
				$joint_paid = '<span style="color: transparent;font-size: 0.5px">' . strtotime($value['joint_setup_fee_date']) . '</span>' . $value['joint_setup_fee_date'];

				
				$btn_delete_lead_client = '<button client_id="'.$value['client_id'].'" class="btn_delete_lead_client btn btn-danger btn-sm" type="button"> <i class="mdi mdi-trash"></i> Delete</button>';
				if ($value['client_type'] == 'Joint') {
					$btn_delete_lead_joint = '<button client_joint_id="'.$value['client_joint_id'].'" class="btn_delete_lead_joint btn btn-danger btn-sm" type="button"> <i class="mdi mdi-trash"></i> Delete</button>';

					if ($value['client_status'] != 'Archived') {
						$data['data'][] = [
							'<span class="rowClientID hide">' . $value['client_id'] . '</span>' .
							$name . '<br>' . $joint_name,
							$cell_phone . '<br>' . $joint_cell_phone,
							$email_address . '<br>' . $joint_email_address,
							$value['last_contact'] == '' ? '' : date('m/d/Y h:i:s a', strtotime($value['last_contact'])),
							$paid . '<br>' . $joint_paid,
							$value['broker'],
							$value['agent'],
							$select_lead_status,
							$btn_delete_lead_client
						];
					}
				} else { 
					if ($value['client_status'] != 'Archived') {
						$data['data'][] = [
							'<span class="rowClientID hide">' . $value['client_id'] . '</span>' .
							$name,
							$cell_phone,
							$email_address,
							$value['last_contact'] == '' ? '' : date('m/d/Y h:i:s a', strtotime($value['last_contact'])),
							$paid,
							$value['broker'],
							$value['agent'],
							$select_lead_status,
							$btn_delete_lead_client
						];
					}

				}

			}
		} else {
			// $last_alert_arr = $this->getLastAlerts(array_column($dataTable, 'client_id'), 'type');
			// $joint_last_alert_arr = $this->getLastAlerts(array_column($dataTable, 'client_joint_id'), 'joint');
			// $last_alert_arr_keys = array_column($last_alert_arr, 'id');
			// $joint_last_alert_arr_keys = array_column($joint_last_alert_arr, 'id');
			foreach ($dataTable as $key => $value) {
				$client_status = $value['client_status'];
				$client_status_options = '';
				foreach ($client_statuses as $kkk => $vvv) {
					if ($vvv['client_status'] != 'Archived') {
						if ($vvv['client_status'] == $client_status) {
							$client_status_options .= '<option selected value="' . $vvv['client_status'] . '">' . $vvv['client_status'] . '</option>';
						} else {
							$client_status_options .= '<option value="' . $vvv['client_status'] . '">' . $vvv['client_status'] . '</option>';
						}
					}

				}
				$select_client_status = "<select client_id=" . $value['client_id'] . " class='selectClientStatus' >
											$client_status_options
										</select>";

				$joint_client_status = $value['joint_client_status'];

				$joint_client_status_options = '';
				foreach ($client_statuses as $kkk => $vvv) {
					if ($vvv['client_status'] != 'Archived') {
						if ($vvv['client_status'] == $joint_client_status) {
							$joint_client_status_options .= '<option selected value="' . $vvv['client_status'] . '">' . $vvv['client_status'] . '</option>';
						} else {
							$joint_client_status_options .= '<option value="' . $vvv['client_status'] . '">' . $vvv['client_status'] . '</option>';
						}
					}

				}
				$joint_client_status_options = "<select client_joint_id=" . $value['client_joint_id'] . " class='selectClientJointStatus' >
													$joint_client_status_options
												</select>";

				// TD
				// NAME
				$name = "<a href='#' class='goToProfile'> <u>" . $value['name'] . "</u> </a>";
				$joint_name = "<a value='" . $value['client_joint_id'] . "' href='#' class='goToJointProfile'><u>" . $value['joint_name'] . "</u></a>";
				// CELL PHONE
				$cell_phone = "<a client_type='client' class='btnCallClientRC' href='#'>" . $value['cell_phone'] . "</a> ";
				$joint_cell_phone = "<a client_type='joint' class='btnCallClientRC' href='#'>" . $value['joint_cell_phone'] . "</a>";
				// EMAIL
				$email_address = $value['email_address'];
				$joint_email_address = $value['joint_email_address'];
				// LAST ALERT
				// $last_alert = '';
				// if (array_search($value['client_id'], $last_alert_arr_keys) != FALSE) {
				// 	$alert_date_ = $last_alert_arr[array_search($value['client_joint_id'], $last_alert_arr_keys)]['alert_date'];
				// 	$last_alert = $alert_date_ ? date('m/d/Y h:i:s a', strtotime($alert_date_)) : '';
				// }
				// $joint_last_alert = '';
				// if (array_search($value['client_joint_id'], $joint_last_alert_arr_keys) != FALSE) {
				// 	$alert_date_ = $joint_last_alert_arr[array_search($value['client_joint_id'], $joint_last_alert_arr_keys)]['alert_date'];
				// 	$joint_last_alert = $alert_date_ ? date('m/d/Y h:i:s a', strtotime($alert_date_)) : '';
				// }

				// PAID
				$client_date_paid = array_search($value['client_id'], array_column($clients_date_paid, 'id')) !== FALSE ? $clients_date_paid[array_search($value['client_id'], array_column($clients_date_paid, 'id'))]['submitted_local'] : '';
				$joint_date_paid = array_search($value['client_joint_id'], array_column($joints_date_paid, 'id')) !== FALSE ? $joints_date_paid[array_search($value['client_joint_id'], array_column($joints_date_paid, 'id'))]['submitted_local'] : '';
				$paid = '<span style="color: transparent;font-size: 0.5px">' . strtotime($client_date_paid) . '</span>' . $client_date_paid;
				// $joint_paid = json_encode($client_joint_ids);
				$joint_paid = '<span style="color: transparent;font-size: 0.5px">' . strtotime($joint_date_paid) . '</span>' . $joint_date_paid;
				// DFR
				$dfr = '<span class="client_dfr">' . $value['dfr'] . '</span>';
				$joint_dfr = '<span class="joint_dfr">' . $value['joint_dfr'] . '</span>';
				// CYCLE
				$cycle = $value['cycle'];
				$joint_cycle = $value['joint_cycle'];

				$broker = $value['broker'];

				$tools = '<div class="btn-group">
		                        <button class="btnClientEmailTab btn btn-secondary btn-sm" type="button" aria-haspopup="true" aria-expanded="false">
		                            <i class="mdi mdi-email"></i> Email
		                        </button>
		                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                            <span class="sr-only">Toggle Dropdown</span>
		                        </button>
		                        <div class="dropdown-menu rowTools">
		                            <a class="dropdown-item" href="#" value="tasks-tab">
		                                <i class="mdi mdi-plus-circle"></i> Add Task</a>
		                            <a class="dropdown-item" href="#" value="notes-tab">
		                                <i class="mdi mdi-plus-circle"></i> Add Note</a>
		                            <a class="dropdown-item" href="#" value="alerts-tab">
		                                <i class="mdi mdi-plus-circle"></i> Add Alert</a>
		                            <a class="dropdown-item" href="#" value="tab_support">
		                                <i class="mdi mdi-help-circle"></i> Support Tickets</a>
		                            <a id="btnArchiveClient" class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete</a>
		                        </div>
		                    </div>';

				$client_id = '<span class="rowClientID hide">' . $value['client_id'] . '</span>';
				$client_status_ = '<span class="rowClientStatus hide">' . $value['client_status'] . '</span>';
				if ($value['client_type'] == 'Joint') {
					if ($value['client_status'] != 'Archived' && $value['joint_client_status'] != 'Archived') {
						$data['data'][] = [
							$client_id . $client_status_ .
							$name . "<br>" . $joint_name,
							$cell_phone . '<br>' . $joint_cell_phone,
							$email_address . '<br>' . $joint_email_address,
							$paid . '<br>' . $joint_paid,
							$cycle . '<br>' . $joint_cycle,
							$select_client_status . '<br>' . $joint_client_status_options,
							$dfr . '<br>' . $joint_dfr,
							$broker,

						];
					} else if ($value['client_status'] != 'Archived' && $value['joint_client_status'] == 'Archived') {
						$data['data'][] = [
							$client_id . $client_status_ .
							$name,
							$cell_phone,
							$email_address,
							$paid,
							$cycle,
							$select_client_status,
							$dfr,
							$broker,

						];
					} else if ($value['client_status'] == 'Archived' && $value['joint_client_status'] != 'Archived') {
						$data['data'][] = [
							$client_id . $client_status_ .
							$joint_name,
							$joint_cell_phone,
							$joint_email_address,
							$joint_paid,
							$joint_cycle,
							$joint_client_status_options,
							$joint_dfr,
							$broker,

						];
					}
				} else {
					if ($value['client_status'] != 'Archived') {
						$data['data'][] = [
							$client_id . $client_status_ .
							$name,
							$cell_phone,
							$email_address,
							$paid,
							$cycle,
							$select_client_status,
							$dfr,
							$broker,

						];
					}
				}
			}

		}

		// $data['data'][] = [$last_query, '', '', '', '', '', '', '', '', ''];
		$data['iTotalRecords'] = count($count_all_data);
		$data['iTotalDisplayRecords'] = count($count_all_data);
		echo json_encode($data);
	}

	function getJointsDatePaid($ids) {
		$ids = implode(',', array_filter($ids, 'strlen'));
		$data = $this->fetchRawData("SELECT
										(SELECT
										date_format( `authorize_transactions`.`submitted_local`, '%m/%d/%Y' ) `submitted_local`
									FROM
										`authorize_transactions`
									WHERE
										`authorize_transactions`.`status` IN ('settledSuccessfully','capturedPendingSettlement')
										AND `authorize_transactions`.`name` = client_joint_billings.card_holder
									ORDER BY
										`authorize_transactions`.`submitted_local` DESC
										LIMIT 1) as `submitted_local` ,
										client_joint_id as id
									FROM
										client_joint_billings
									WHERE
										client_joint_id IN ( $ids )
									GROUP BY
										client_joint_id");
		return $data;
	}
	function getClientsDatePaid($ids) {
		$ids = implode(',', array_filter($ids, 'strlen'));
		$data = $this->fetchRawData("SELECT
										(SELECT
										date_format( `authorize_transactions`.`submitted_local`, '%m/%d/%Y' ) `submitted_local`
									FROM
										`authorize_transactions`
									WHERE
										`authorize_transactions`.`status` IN ('settledSuccessfully','capturedPendingSettlement')
										AND `authorize_transactions`.`name` = client_billings.card_holder
									ORDER BY
										`authorize_transactions`.`submitted_local` DESC
										LIMIT 1) as `submitted_local` ,
										client_id as id
									FROM
										client_billings
									WHERE
										client_id IN ( $ids )
									GROUP BY
										client_id");
		return $data;
	}

	function getLastAlerts($data, $type) {
		$client_ids = implode(', ', array_filter($data, 'strlen'));
		$data_ = $this->fetchRawData("SELECT
										`client_alerts`.`alert_date`,
										`client_alerts`.`client_id` as `id`
									FROM
										`client_alerts`
									WHERE
										( ( `client_alerts`.`client_id` IN  ($client_ids)) AND ( `client_alerts`.`client_type` = '$type' ) )");
		return $data_;
	}

	function test() {
		$data = $this->fetchRawData("SELECT
										*
									FROM
										`view_clients_table`
									WHERE
										`converted` = 1
										AND `client_status` NOT IN ('Archived','Cancelled','Terminated')
									ORDER BY
										`converted_date` DESC
										LIMIT 50");
		$client_ids = array_column($data, 'client_id');
		// $this->pprint($client_ids);
		$last_alert = $this->getLastAlerts(array_column($data, 'client_id'), 'client');
		// $joint_last_alert= $this->getLastAlerts(array_column($data, 'client_joint_id'),'joint');

		$this->pprint($last_alert[array_search(12412412, array_column($last_alert, 'id'))]['alert_date']);

	}

	function validateDate($date, $format = 'm/d/Y') {
		$d = DateTime::createFromFormat($format, $date);
		// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
		return $d && $d->format($format) === $date;
	}

	public function getTemplateRecords() {
		$id = $this->input->post('task_template_id');
		$task_templates = new Model_task_templates();
		$data = $task_templates->search(['task_template_id' => $id], '', '', 1);
		echo json_encode($data);
	}

	function subscription_type() {
		$subscription = new Model_subscription();
		$data = $subscription->get();
		return $data;
	}

	public function AddGameplanTimeline() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$gameplan_timeline = new Model_gameplan_timeline();
		$data = $gameplan_timeline->search(['client_id' => $client_id]);
		$gameplan_timeline = new Model_gameplan_timeline();
		if (count($data) > 0) {
			$timeline_id = $data[0]['timeline_id'];
			$gameplan_timeline->load($timeline_id);
		}
		$gameplan_timeline->enrolled = $this->input->post('Enrolled');
		$gameplan_timeline->dispute = $this->input->post('Dispute');
		$gameplan_timeline->result = $this->input->post('Result');
		$gameplan_timeline->repeat = $this->input->post('Repeat');
		$gameplan_timeline->complete = $this->input->post('Complete');

		$gameplan_timeline->client_id = $client_id;

		$gameplan_timeline->client_type = 'client';
		$gameplan_timeline->save();
	}

	public function saveCreditorsAccountType() {
		$creditors_account_type_id = $this->input->post('creditors_account_type_id');
		$creditors_account_type = $this->input->post('creditors_account_type');

		$creditors_account_type = new Model_creditors_account_type();
		if ($creditors_account_type_id != '') {
			$creditors_account_type->load($creditors_account_type_id);
			$creditors_account_type->creditors_account_type = $this->input->post('creditors_account_type');
		} else {
			$creditors_account_type->creditors_account_type = $this->input->post('creditors_account_type');
		}
		$creditors_account_type->save();

	}

	public function saveCreditorsRec() {
		$creditors_rec_id = $this->input->post('creditors_rec_id');
		$creditors_rec = $this->input->post('creditors_rec');

		$creditors_recommendations = new Model_creditors_recommendations();
		if ($creditors_rec_id != '') {
			$creditors_recommendations->creditors_rec_id = $creditors_rec_id;
		}
		$creditors_recommendations->creditors_rec = $creditors_rec;
		$creditors_recommendations->save();

	}

	public function getCreditorsAccountType() {
		$creditors_account_type = new Model_creditors_account_type();
		$data = $creditors_account_type->get();
		echo json_encode($data);
	}

	public function getTableCreditorsRec() {
		$creditors_recommendations = new Model_creditors_recommendations();
		$data = $creditors_recommendations->get();
		echo json_encode($data);
	}

	public function getGameplanTimeline() {
		$client_id = $this->input->post('client_id');
		$gameplan_timeline = new Model_gameplan_timeline();
		$data = $gameplan_timeline->search(['client_id' => $client_id]);

		echo json_encode($data);
	}

	public function getCreditScore() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_gameplan_points = new Model_client_joint_gameplan_points();
			$data = $client_joint_gameplan_points->search(['client_joint_id' => $client_joint_id]);
		} else {
			$client_gameplan_points = new Model_client_gameplan_points();
			$data = $client_gameplan_points->search(['client_id' => $client_id]);
		}
		echo json_encode($data);
	}

	public function gameplanCreditScore() {
		$equifax_points = $this->input->post('equifax_points');
		$experian_points = $this->input->post('experian_points');
		$transunion_points = $this->input->post('transunion_points');
		$credit_score = $this->input->post('idealCreditScore');
		$eta_date = $this->input->post('eta_date');
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');

		if ($client_type == 'joint') {
			$client_joint_gameplan_points = new Model_client_joint_gameplan_points();
			$data = $client_joint_gameplan_points->search(['client_joint_id' => $client_joint_id]);

			$client_joint_gameplan_points = new Model_client_joint_gameplan_points();

			if (count($data) > 0) {
				$data = reset($data);
				$data = reset($data);
				$client_joint_gameplan_points->load($data);
			}
			$client_joint_gameplan_points->equifax_points = $equifax_points;
			$client_joint_gameplan_points->experian_points = $experian_points;
			$client_joint_gameplan_points->transunion_points = $transunion_points;
			$client_joint_gameplan_points->credit_score = $credit_score;
			$client_joint_gameplan_points->eta_date = $eta_date;
			$client_joint_gameplan_points->client_joint_id = $client_joint_id;
			$client_joint_gameplan_points->save();
		} else {
			$client_gameplan_points = new Model_client_gameplan_points();
			$data = $client_gameplan_points->search(['client_id' => $client_id]);

			$client_gameplan_points = new Model_client_gameplan_points();
			if (count($data) > 0) {
				$data = reset($data);
				$data = reset($data);
				$client_gameplan_points->load($gameplan_id);
			}
			$client_gameplan_points->equifax_points = $equifax_points;
			$client_gameplan_points->experian_points = $experian_points;
			$client_gameplan_points->transunion_points = $transunion_points;
			$client_gameplan_points->credit_score = $credit_score;
			$client_gameplan_points->eta_date = $eta_date;
			$client_gameplan_points->client_id = $client_id;
			$client_gameplan_points->save();
		}

	}

	public function newCreditorsAccount() {
		$id = $this->input->post('id');
		$client_type = $this->input->post('client_type');
		$equifax = $this->input->post('equifax');
		$experian = $this->input->post('experian');
		$transunion = $this->input->post('transunion');
		$account_name = $this->input->post('account_name');
		$account_number = $this->input->post('account_number');
		$account_type = $this->input->post('account_type');
		$recommendation = $this->input->post('recommendation');
		$missing_balance = $this->input->post('missing_balance');
		$credit_limit = $this->input->post('credit_limit');
		$past_due = $this->input->post('past_due');
		if ($equifax == 'true') {
			$creditors_account = new Model_creditors_account();
			$creditors_account->id = $id;
			$creditors_account->client_type = $client_type;
			$creditors_account->bureau = 'Equifax';
			$creditors_account->account_name = $account_name;
			$creditors_account->account_number = $account_number;
			$creditors_account->account_type = $account_type;
			$creditors_account->recommendation = $recommendation;
			$creditors_account->missing_balance = $missing_balance;
			$creditors_account->credit_limit = $credit_limit;
			$creditors_account->past_due = $past_due;
			$creditors_account->date_added = date('Y-m-d H:i:s');
			$creditors_account->save();
		}
		if ($experian == 'true') {
			$creditors_account = new Model_creditors_account();
			$creditors_account->id = $id;
			$creditors_account->client_type = $client_type;
			$creditors_account->bureau = 'Experian';
			$creditors_account->account_name = $account_name;
			$creditors_account->account_number = $account_number;
			$creditors_account->account_type = $account_type;
			$creditors_account->recommendation = $recommendation;
			$creditors_account->missing_balance = $missing_balance;
			$creditors_account->credit_limit = $credit_limit;
			$creditors_account->past_due = $past_due;
			$creditors_account->date_added = date('Y-m-d H:i:s');
			$creditors_account->save();
		}
		if ($transunion == 'true') {
			$creditors_account = new Model_creditors_account();
			$creditors_account->id = $id;
			$creditors_account->client_type = $client_type;
			$creditors_account->bureau = 'TransUnion';
			$creditors_account->account_name = $account_name;
			$creditors_account->account_number = $account_number;
			$creditors_account->account_type = $account_type;
			$creditors_account->recommendation = $recommendation;
			$creditors_account->missing_balance = $missing_balance;
			$creditors_account->credit_limit = $credit_limit;
			$creditors_account->past_due = $past_due;
			$creditors_account->date_added = date('Y-m-d H:i:s');
			$creditors_account->save();
		}
	}

	public function addClientGameplan() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_gameplan = new Model_client_joint_gameplan();
			$data = $client_joint_gameplan->search(['client_joint_id' => $client_joint_id]);
			if (count($data) > 0) {
				$gameplan_id = $data[0]['gameplan_id'];
				$client_joint_gameplan->load($gameplan_id);
			}
			$client_joint_gameplan->broker = $this->input->post('broker_gameplan');
			$client_joint_gameplan->client_joint_id = $this->input->post('client_joint_id');
			$client_joint_gameplan->credit_lynx = $this->input->post('creditlynx_gameplan');
			$client_joint_gameplan->save();
		} else {
			$client_gameplan = new Model_client_gameplan();
			$data = $client_gameplan->search(['client_id' => $client_id]);
			if (count($data) > 0) {
				$gameplan_id = $data[0]['gameplan_id'];
				$client_gameplan->load($gameplan_id);
			}
			$client_gameplan->broker = $this->input->post('broker_gameplan');
			$client_gameplan->client_id = $this->input->post('client_id');
			$client_gameplan->credit_lynx = $this->input->post('creditlynx_gameplan');
			$client_gameplan->save();
		}

	}
	public function getClientGameplan() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_gameplan = new Model_client_joint_gameplan();
			$data = $client_joint_gameplan->search(['client_joint_id' => $client_joint_id]);
		} else {
			$client_gameplan = new Model_client_gameplan();
			$data = $client_gameplan->search(['client_id' => $client_id]);
		}
		echo json_encode($data);
	}

	function getTemplateTitles() {
		$template_header = $this->input->post('template_header');
		$letter_templates = new Model_letter_templates();
		$letter_templates->selects = ['blt_id', 'template_title'];
		if ($template_header != 'Creditor Letters') {
			$this->db->or_like('template_header', 'Misc:');
		}

		$this->db->or_where('template_header', $template_header);
		$data = $letter_templates->searchWithSelect([]);
		echo json_encode($data);
	}

	function getTemplate() {
		$template_title = $this->input->post('template_title');
		$template_header = $this->input->post('template_header');
		$letter_templates = new Model_letter_templates();
		$letter_templates->selects = ['template'];
		$data = $letter_templates->searchWithSelect(['template_title' => $template_title, 'template_header' => $template_header]);
		echo json_encode($data);
	}

	function getAccountTypes() {
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		echo json_encode($data);
	}

	function getAccountStatuses() {
		$clients_account_statuses = new Model_clients_account_statuses();
		$data = $clients_account_statuses->search([], 'account_status', 'ASC');
		echo json_encode($data);
	}

	function getAccountPaymentStatuses() {
		$clients_account_payment_statuses = new Model_clients_account_payment_statuses();
		$data = $clients_account_payment_statuses->search([], 'payment_status', 'ASC');
		echo json_encode($data);
	}

	function getAccountComments() {
		$clients_account_comments = new Model_clients_account_comments();
		$data = $clients_account_comments->search([], 'comment', 'ASC');
		echo json_encode($data);
	}

	function getClientsAccountTypes() {
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		return $data;
	}
	function get_clients_account_types() {
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		echo json_encode($data);
	}

	function getDisputeVerbiagesAccountTypes() {
		$data = $this->Model_Query->getView('view_clients_dispute_verbiages_assigned', [], 'dv_id', 'DESC');
		echo json_encode($data);
	}

	function getDisputeVerbiagesByATandRound() {
		$account_type = $this->input->post('account_type');
		$round = $this->input->post('round');
		$clients_dispute_verbiage_assignments = new Model_clients_dispute_verbiage_assignments();
		$data = $clients_dispute_verbiage_assignments->search(['account_type' => $account_type, 'round' => $round], 'dispute_verbiage', 'asc');
		echo json_encode($data);
	}

	function saveDisputeVerbiagesAssignment() {
		$clients_dispute_verbiage_assignments = new Model_clients_dispute_verbiage_assignments();
		foreach ($this->input->post() as $key => $value) {
			$clients_dispute_verbiage_assignments->$key = $value;
		}
		$clients_dispute_verbiage_assignments->save();
	}

	function deleteDisputeVerbiageAssignment() {
		$cdva_id = $this->input->post('cdva_id');
		$clients_dispute_verbiage_assignments = new Model_clients_dispute_verbiage_assignments();
		$clients_dispute_verbiage_assignments->load($cdva_id);
		$clients_dispute_verbiage_assignments->delete();
	}

	function getDisputeVerbiages() {
		$clients_dispute_verbiages = new Model_clients_dispute_verbiages();
		$data = $clients_dispute_verbiages->search([], 'dispute_verbiage', 'ASC');
		return $data;
	}

	function getLeadStatuses() {
		$lead_statuses = new Model_lead_statuses();
		$data = $lead_statuses->search([], 'ls_id', 'ASC');
		return $data;
	}

	function getClientStatuses() {
		$client_statuses = new Model_client_statuses();
		$data = $client_statuses->search([], 'client_status', 'ASC');
		return $data;
	}

	function getClients($by, $order) {
		$this->Model_Query->selects = ['*', 'IF(STR_TO_DATE(monthly_due, \'%d,%m,%Y\') IS NOT NULL,DAY(STR_TO_DATE(monthly_due, \'%d,%m,%Y\')),monthly_due) as `monthly_due_formated`'];

		$this->db->where('converted', 1);
		$this->db->where('client_status !=', 'Complete');
		$this->db->where('client_status !=', 'Cancelled');
		$this->db->where('client_status !=', 'Archived');
		if ($by != '') {
			$data = $this->Model_Query->getView('view_clients_table', [], $by . ',client_id ' . $order);
		} else {
			$data = $this->Model_Query->getView('view_clients_table', [], 'client_id', 'DESC');
		}

		return $data;
	}

	function getClientsTest() {
		$this->Model_Query->selects = ['*', 'IF(STR_TO_DATE(monthly_due, \'%d,%m,%Y\') IS NOT NULL,DAY(STR_TO_DATE(monthly_due, \'%d,%m,%Y\')),monthly_due) as `monthly_due_formated`'];

		$this->db->where('converted', 1);
		$this->db->where('client_status !=', 'Complete');
		$this->db->where('client_status !=', 'Cancelled');
		$this->db->where('client_status !=', 'Archived');
		$data = $this->Model_Query->getView('view_clients_table', [], 'client_id', 'DESC');
		$this->pprint($data);
	}

	function getAllClients() {
		$this->Model_Query->selects = ['client_id', 'name', 'client_type', 'client_joint_id', 'joint_name', 'cell_phone', 'email_address', 'ss', 'joint_cell_phone', 'joint_email_address', 'joint_ss'];
		$data = $this->Model_Query->getView('view_clients_table', ['converted' => 1], 'client_id', 'DESC');
		return $data;
	}

	function getUserCount() {

		$data_clients = $this->fetchRawData("SELECT * FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$data_client_joints = $this->fetchRawData("SELECT * FROM client_joints WHERE client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_id=client_joints.client_id) AND client_status NOT IN ('Archived','Cancelled','Terminated')");
		$count = count($data_clients) + count($data_client_joints);
		return $count;
	}


	function trim_phone($str) {
		$str = str_replace('(', '', $str);
		$str = str_replace(')', '', $str);
		$str = str_replace(' ', '', $str);
		$str = str_replace('-', '', $str);
		return $str;
	}

	function get_agent_name($agent_id) {
		if ($agent_id != '') {
			$data = $this->fetchRawData("SELECT CONCAT(first_name,' ',last_name) as `agent_name` FROM agents WHERE agent_id=$agent_id");
			if (count($data) > 0) {
				$data = reset($data);
				$data = reset($data);
				return $data;
			} else {
				return '';
			}
		} else {
			return '';
		}
		

	}
	function get_broker_name($broker_id) {
		if ($broker_id != '') {
			$data = $this->fetchRawData("SELECT CONCAT(first_name,' ',last_name) as `broker_name` FROM brokers WHERE broker_id=$broker_id");
			if (count($data) > 0) {
				$data = reset($data);
				$data = reset($data);
				return $data;
			} else {
				return '';
			}

				
			
		} else {
			return '';
		}
		

	}
	function saveDetail() {
		// $this->pprint($this->input->post());
		$converted = $this->input->post('converted');
		$client_id = $this->input->post('client_id');
		$agent_id = $this->input->post('agent_id');
		$broker_id = $this->input->post('broker_id');
		$sale_id = $this->input->post('sale_id');
		$processor_id = $this->input->post('processor_id');

		$client_status = $this->input->post('client_status');
		$lead_status = $this->input->post('lead_status');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state_province = $this->input->post('state_province');
		$zip_postal_code = $this->input->post('zip_postal_code');
		$alt_phone = $this->input->post('alt_phone');
		$cell_phone = $this->input->post('cell_phone');
		$carrier = $this->input->post('carrier');
		$fax = $this->input->post('fax');
		$email_address = $this->input->post('email_address');
		$ss = $this->input->post('ss');
		$date_of_birth = $this->input->post('date_of_birth');
		$date_for_result = $this->input->post('date_for_result');
		$username = $this->input->post('email_address');
		$password = $this->get_default_password($name);;
		$password = password_hash($password, PASSWORD_BCRYPT);

		$joint_name = $this->input->post('joint_name');
		$joint_cell_phone = $this->input->post('joint_cell_phone');
		$joint_email_address = $this->input->post('joint_email_address');
		$joint_ss = $this->input->post('joint_ss');
		$joint_date_of_birth = $this->input->post('joint_date_of_birth');
		$joint_username = $this->input->post('joint_email_address');
		$joint_password = $this->get_default_password($joint_name);
		$joint_password = password_hash($joint_password, PASSWORD_BCRYPT);
		$client_type = 'Single';
		if ($joint_name != '') {
			$client_type = 'Joint';
		}

		$font_array = array('Georgia, serif', '"Palatino Linotype", "Book Antiqua", Palatino, serif', '"Times New Roman", Times, serif', 'Arial, Helvetica, sans-serif', '"Arial Black", Gadget, sans-serif', '"Comic Sans MS", cursive, sans-serif', 'Impact, Charcoal, sans-serif', '"Lucida Sans Unicode", "Lucida Grande", sans-serif', 'Tahoma, Geneva, sans-serif', '"Trebuchet MS", Helvetica, sans-serif', 'Verdana, Geneva, sans-serif', '"Courier New", Courier, monospace', '"Lucida Console", Monaco, monospace');
		$font = $font_array[rand(0, 12)];

		$this->load->model('Model_clients');
		$clients = new Model_clients();
		if ($client_id != '') {
			$clients->client_id = $client_id;
		}
		$clients->client_type = $client_type;
		$clients->client_status = '';
		$clients->lead_status = 'New';
		$clients->name = $name;
		$clients->address = $address;
		$clients->city = $city;
		$clients->state_province = $state_province;
		$clients->zip_postal_code = $zip_postal_code;
		$clients->alt_phone = $alt_phone;
		$clients->cell_phone = $cell_phone;
		$clients->carrier = $carrier;
		$clients->fax = $fax;
		$clients->email_address = $email_address;
		$clients->ss = $ss;
		$clients->date_of_birth = $date_of_birth;
		$clients->date_for_result = $date_for_result;
		$clients->username = $username;
		$clients->password = $password;
		$clients->font = $font;
		$clients->date_created = date('Y-m-d H:i:s');
		$clients->converted = $converted;
		if ($converted == 1) {
			$clients->converted_date = date('Y-m-d H:i:s');
		}

		$clients->save();
		$this->sendRCSms('+18139517686', 'New lead: '.$name.' Phone Number: '.$cell_phone);
		

		if ($client_type == 'Joint') {
			$this->load->model('Model_client_joints');
			$client_joints = new Model_client_joints();

			$client_joints->client_id = $clients->client_id;
			$client_joints->client_status = '';
			$client_joints->name = $joint_name;
			$client_joints->cell_phone = $joint_cell_phone;
			$client_joints->email_address = $joint_email_address;
			$client_joints->ss = $joint_ss;
			$client_joints->date_of_birth = $joint_date_of_birth;
			$client_joints->username = $joint_username;
			$client_joints->password = $joint_password;
			$client_joints->save();
			$this->sendRCSms('+18139517686', 'New lead: '.$joint_name.' Phone Number: '.$joint_cell_phone);
		}

		$client_assignments = new Model_client_assignments();
		$client_assignments->client_id = $clients->client_id;
		$client_assignments->agent_id = $agent_id;
		$client_assignments->broker_id = $broker_id;
		$client_assignments->sale_id = $sale_id;
		$client_assignments->processor_id = $processor_id;
		$client_assignments->save();

		$this->get_next_pool_add_new($sale_id);



		
		$message = 'A new lead just came in <br><br> Name: '.$name.' <br> Phone Number: '.$cell_phone.' <br> Email Address: '.$email_address.' <br> Broker: '.$this->get_broker_name($broker_id).' <br> Agent: '.$this->get_agent_name($agent_id);
		

		$this->send_custom_email('New Lead','',$message,['joshuasaubon@gmail.com','baumab03@dialyourleads.com']);



		$page_title = $this->input->post('page_title');
		if ($client_type == 'Joint') {

			// if ($page_title == 'Leads') {
				redirect(base_url('admin/leads?id=' . $client_joints->client_joint_id . '&type=joint'));
			// } else {
			// 	$this->sendWelcomeEmail($client_joints->client_joint_id,'joint');
			// 	// redirect(base_url('admin/clients?id=' . $client_joints->client_joint_id . '&type=joint'));
			// }
		} else {
			// if ($page_title == 'Leads') {
				redirect(base_url('admin/leads?id=' . $clients->client_id . '&type=client'));
			// } else {
			// 	$this->sendWelcomeEmail($clients->client_id,'client');
			// 	// redirect(base_url('admin/clients?id=' . $clients->client_id . '&type=client'));
			// }
		}

	}


	function get_next_pool_add_new($employee_id) {
		$this->load->model('Model_employees');
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team','status'=>1],'employee_id','ASC');
		// $this->pprint($data);
		$index = array_search($employee_id, array_column($data, 'employee_id'));
		if ($index < (count($data) -1)) {
			$this->set_lead_pool_add_new($data[$index + 1]['employee_id']);
		} else {
			$this->set_lead_pool_add_new($data[0]['employee_id']);
		}
	}

	function set_lead_pool_add_new($employee_id) {
		$pool = 1;
		$this->db->query("UPDATE employees SET pool=0 WHERE type='Sales Team'");

		$this->db->query("UPDATE employees SET pool=$pool WHERE employee_id=$employee_id");
	}

	public function ConvertToJointAccount() {

		# client id data
		$id = $this->input->post('profile_client_id');

		#update client type query
		$update = array('client_type' => 'Joint');
		$this->db->where('client_id', $id);
		$this->db->update('clients', $update);

		# Client joints query

		$this->load->model('Model_client_joints');
		$client_joints = new Model_client_joints();

		$client_joints->client_id = $id;
		$client_joints->name = $this->input->post('joint_name');
		$client_joints->cell_phone = $this->input->post('joint_cell_phone');
		$client_joints->email_address = $this->input->post('joint_email_address');
		$client_joints->ss = $this->input->post('joint_ss');
		$client_joints->date_of_birth = $this->input->post('joint_date_of_birth');
		$client_joints->username = $this->input->post('joint_username');
		$client_joints->password = $this->input->post('joint_password');
		$client_joints->save();
		if ($this->uri->segment(2) == 'leads') {
			redirect(base_url('admin/leads'));
		} else {
			redirect(base_url('admin/clients'));
		}
	}

	function checkIfUsernameExist() {
		$username = $this->input->post('username_val');
		$type = $this->input->post('type');
		if ($type == 'joint') {
			$client_joints = new Model_client_joints();
			$data = $client_joints->search(['username' => $username]);
		} else {

			$clients = new Model_clients();
			$data = $clients->search(['username' => $username]);
		}

		echo count($data);
	}

	function getProfile() {
		$client_id = $this->input->post('client_id');
		$type = $this->input->post('type');
		if ($type == 'client') {
			$profile = new Model_Query();
			$profile->selects = ['*', 'RIGHT(ss,4) as `ss_restricted`'];
			$data['profile'] = $profile->getView('view_clients', ['client_id' => $client_id]);

			$enrollment = new Model_client_enrollments();
			$data['enrollment'] = $enrollment->search(['client_id' => $client_id]);

			$client_billings = new Model_client_billings();
			$data['billing'] = $client_billings->search(['client_id' => $client_id]);
		} else {
			$client_joint_id = $client_id;
			$profile = new Model_Query();
			$profile->selects = ['*', 'RIGHT(ss,4) as `ss_restricted`'];
			$data['profile'] = $profile->getView('view_client_joints', ['client_joint_id' => $client_joint_id]);

			$enrollment = new Model_client_joint_enrollments();
			$data['enrollment'] = $enrollment->search(['client_joint_id' => $client_joint_id]);

			$client_billings = new Model_client_joint_billings();
			$data['billing'] = $client_billings->search(['client_joint_id' => $client_joint_id]);

		}

		// $client_id = $data['profile'][0]['client_id'];

		// $this->updateEmailETextRead($client_id);
		echo json_encode($data);

	}
	function getProfileDetails() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'client') {
			$data = $this->getClientProfileDetails($client_id, $client_joint_id, $client_type);
		} else {
			$data = $this->getJointProfileDetails($client_id, $client_joint_id, $client_type);
		}

		echo json_encode($data);

	}

	private function getClientProfileDetails($client_id, $client_joint_id, $client_type) {
		$profile = new Model_Query();
		$enrollment = new Model_client_enrollments();
		$client_billings = new Model_client_billings();
		$client_account_details = new Model_client_account_details();
		$client_gameplan = new Model_client_gameplan();
		$tasks = new Model_tasks();
		$notes = new Model_notes();
		$client_alerts = new Model_client_alerts();
		$client_gameplan_points = new Model_client_gameplan_points();
		$client_emails = new Model_client_emails();
		$client_etexts = new Model_client_etexts();
		$gameplan_timeline = new Model_gameplan_timeline();
		$creditors_account = new Model_creditors_account();
		$support_tickets = new Model_support_tickets();

		$profile->selects = ['*', 'RIGHT(ss,4) as `ss_restricted`'];
		$data['profile'] = $profile->getView('view_clients', ['client_id' => $client_id]);

		$data['enrollment'] = $enrollment->search(['client_id' => $client_id]);

		$data['billing'] = $client_billings->search(['client_id' => $client_id]);

		$data['documents'] = $this->fetchRawData("SELECT * FROM client_documents WHERE client_id=$client_id ORDER BY date_uploaded DESC");

		$data['results_tracker'] = $client_account_details->search(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');

		$data['gameplan'] = $client_gameplan->search(['client_id' => $client_id]);

		$data['tasks'] = $tasks->search(['task_assigned_id' => $client_id,
			'task_assigned' => 'Clients',
			'task_active <>' => 2,
		], 'task_date', 'DESC');

		$data['notes'] = $notes->search(['note_assigned_id' => $client_id,
			'note_assigned' => 'Clients',
		], 'note_id', 'DESC');

		$data['alerts'] = $this->fetchRawData("SELECT
												client_alerts.*,
											IF
												( HOUR ( alert_date ) = 0, DATE_ADD( alert_date, INTERVAL 20 HOUR ), alert_date ) AS `alert_date_format` ,
												(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM clients WHERE clients.client_id = client_alerts.client_id) as `client_name`
											FROM
												client_alerts 
											WHERE
												client_id = $client_id 
												AND client_type = 'client'
											ORDER BY alert_date_format DESC, alert_id DESC");;

		$data['credit_scores'] = $client_gameplan_points->search(['client_id' => $client_id]);
		$all_emails = $this->getClientAllEmails($client_id,$client_type);
		$all_emails = implode("','", array_column($all_emails, 'email_address'));
		$data['emails'] = [];
		if ($all_emails != '') {
			$data['emails'] = $this->fetchRawData("SELECT * FROM client_emails WHERE `from` IN ('$all_emails') OR `to` IN ('$all_emails') ORDER BY `date` DESC");
		}
		
		// $data['emails'] = $client_emails->search(['client_id' => $client_id, 'client_type' => $client_type], 'email_id', 'DESC');
		foreach ($data['emails'] as $key => $value) {
			$data['emails'][$key]['message'] = strip_tags($value['message']);
		}

		$data['texts'] = $this->get_client_etexts($client_id,$client_type);

		$data['timeline'] = $gameplan_timeline->search(['client_id' => $client_id]);

		$data['creditors_account'] = $creditors_account->search(['client_type' => $client_type, 'id' => $client_joint_id], '', '', 0, 0, 'date_added');

		$data['results_chart'] = $this->getResultsChart($client_id, $client_joint_id, $client_type);
		$client_account_details = new Model_client_account_details();
		$client_account_details->selects = ['account_name',
			'account_number',
			'account_type',
			'GROUP_CONCAT(bureaus) as `bureaus`',
			'GROUP_CONCAT(account_status) as `account_status`',
		];
		$data['disputes_by_account'] = $client_account_details->searchWithSelect(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');
 
		$data['disputes_by_bureau'] = $this->fetchRawData("SELECT 
										(
										CASE 
											WHEN client_account_details.account_status = 'Fixed' OR 
											client_account_details.account_status = 'Deleted' OR 
											client_account_details.account_status = 'Not Reporting' THEN
											 (SELECT 
													CASE 
														WHEN dispute_9_date IS NOT NULL AND dispute_9_date <> '' THEN '9'
														WHEN dispute_8_date IS NOT NULL AND dispute_8_date <> '' THEN '8'
														WHEN dispute_7_date IS NOT NULL AND dispute_7_date <> '' THEN '7'
														WHEN dispute_6_date IS NOT NULL AND dispute_6_date <> '' THEN '6'
														WHEN dispute_5_date IS NOT NULL AND dispute_5_date <> '' THEN '5'
														WHEN dispute_4_date IS NOT NULL AND dispute_4_date <> '' THEN '4'
														WHEN dispute_3_date IS NOT NULL AND dispute_3_date <> '' THEN '3'
														WHEN dispute_2_date IS NOT NULL AND dispute_2_date <> '' THEN '2'
														WHEN dispute_1_date IS NOT NULL AND dispute_1_date <> '' THEN '1' 
														ELSE '0'
													END
												FROM client_rounds WHERE account_id=client_account_details.account_id
											)
											ELSE
												0
										END
										) as `last_round`,
										client_account_details.*


									FROM client_account_details WHERE client_id=$client_id");

		$data['support_tickets'] = $support_tickets->search(['sender' => 'client', 'sender_id' => $client_id], 'support_id', 'DESC');

		$client_account_details = new Model_client_account_details();
		$client_account_details->selects = ['account_name', 'account_number'];
		$this->db->where('account_status !=', 'Fixed');
		$this->db->where('account_status !=', 'Deleted');
		$this->db->where('account_status !=', 'Not Reporting');
		$data['letter_accounts'] = $client_account_details->searchWithSelect(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number');
		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$data['other_cards'] = $client_other_cards->search(['id' => $client_id, 'type' => $client_type],'coc_id','DESC');

		$this->load->model('Model_client_other_emails');
		$client_other_emails = new Model_client_other_emails();
		$data['other_emails'] = $client_other_emails->search(['id' => $client_id, 'type' => $client_type]);

		return $data;
	}

	private function getJointProfileDetails($client_id, $client_joint_id, $client_type) {
		$profile = new Model_Query();
		$enrollment = new Model_client_joint_enrollments();
		$client_billings = new Model_client_joint_billings();
		$client_joint_account_details = new Model_client_joint_account_details();
		$client_joint_gameplan = new Model_client_joint_gameplan();
		$tasks = new Model_tasks();
		$notes = new Model_notes();
		$client_alerts = new Model_client_alerts();
		$client_joint_gameplan_points = new Model_client_joint_gameplan_points();
		$client_emails = new Model_client_emails();
		$client_etexts = new Model_client_etexts();
		$gameplan_timeline = new Model_gameplan_timeline();
		$creditors_account = new Model_creditors_account();
		$support_tickets = new Model_support_tickets();

		$profile->selects = ['*', 'RIGHT(ss,4) as `ss_restricted`'];
		$data['profile'] = $profile->getView('view_client_joints', ['client_joint_id' => $client_joint_id]);

		$data['enrollment'] = $enrollment->search(['client_joint_id' => $client_joint_id]);

		$data['billing'] = $client_billings->search(['client_joint_id' => $client_joint_id]);

		$data['documents'] = $this->fetchRawData("SELECT * FROM client_joint_documents WHERE client_joint_id=$client_joint_id ORDER BY date_uploaded DESC");

		$data['results_tracker'] = $client_joint_account_details->search(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');

		$data['gameplan'] = $client_joint_gameplan->search(['client_joint_id' => $client_joint_id]);

		$data['tasks'] = $tasks->search(['task_assigned_id' => $client_joint_id,
			'task_assigned' => 'Client Joints',
			'task_active <>' => 2,
		], 'task_date', 'DESC');

		$data['notes'] = $notes->search(['note_assigned_id' => $client_joint_id,
			'note_assigned' => 'Client Joints',
		], 'note_id', 'DESC');

		 
		$data['alerts'] = $this->fetchRawData("SELECT
												client_alerts.*,
											IF
												( HOUR ( alert_date ) = 0, DATE_ADD( alert_date, INTERVAL 20 HOUR ), alert_date ) AS `alert_date_format` ,
												(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM client_joints WHERE client_joints.client_joint_id = client_alerts.client_id) as `client_name`
											FROM
												client_alerts 
											WHERE
												client_id = $client_joint_id 
												AND client_type = 'joint'
											ORDER BY alert_date_format DESC, alert_id DESC");;

		$data['credit_scores'] = $client_joint_gameplan_points->search(['client_joint_id' => $client_joint_id]);

		$all_emails = $this->getClientAllEmails($client_joint_id,$client_type);
		$all_emails = implode("','", array_column($all_emails, 'email_address'));
		$data['emails'] = [];
		if ($all_emails != '') {
			$data['emails'] = $this->fetchRawData("SELECT * FROM client_emails WHERE `from` IN ('$all_emails') OR `to` IN ('$all_emails') ORDER BY `date` DESC");
		}
		// $data['emails'] = $client_emails->search(['client_id' => $client_joint_id, 'client_type' => $client_type], 'email_id', 'DESC');
		foreach ($data['emails'] as $key => $value) {
			$data['emails'][$key]['message'] = strip_tags($value['message']);
		}

		$data['texts'] = $this->get_client_etexts($client_joint_id,$client_type);

		$data['timeline'] = $gameplan_timeline->search(['client_id' => $client_id]);

		$data['creditors_account'] = $creditors_account->search(['client_type' => $client_type, 'id' => $client_joint_id], '', '', 0, 0, 'date_added');

		$data['results_chart'] = $this->getResultsChart($client_id, $client_joint_id, $client_type);

		$client_joint_account_details = new Model_client_joint_account_details();
		$client_joint_account_details->selects = ['account_name',
			'account_number',
			'account_type',
			'GROUP_CONCAT(bureaus) as `bureaus`',
			'GROUP_CONCAT(account_status) as `account_status`',
		];
		$data['disputes_by_account'] = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');
 
		$data['disputes_by_bureau'] = $this->fetchRawData("SELECT 
										(
										CASE 
											WHEN client_joint_account_details.account_status = 'Fixed' OR 
											client_joint_account_details.account_status = 'Deleted' OR 
											client_joint_account_details.account_status = 'Not Reporting' THEN
											 (SELECT 
													CASE 
														WHEN dispute_9_date IS NOT NULL AND dispute_9_date <> '' THEN '9'
														WHEN dispute_8_date IS NOT NULL AND dispute_8_date <> '' THEN '8'
														WHEN dispute_7_date IS NOT NULL AND dispute_7_date <> '' THEN '7'
														WHEN dispute_6_date IS NOT NULL AND dispute_6_date <> '' THEN '6'
														WHEN dispute_5_date IS NOT NULL AND dispute_5_date <> '' THEN '5'
														WHEN dispute_4_date IS NOT NULL AND dispute_4_date <> '' THEN '4'
														WHEN dispute_3_date IS NOT NULL AND dispute_3_date <> '' THEN '3'
														WHEN dispute_2_date IS NOT NULL AND dispute_2_date <> '' THEN '2'
														WHEN dispute_1_date IS NOT NULL AND dispute_1_date <> '' THEN '1' 
														ELSE '0'
													END
												FROM client_rounds WHERE account_id=client_joint_account_details.account_id
											)
											ELSE
												0
										END
										) as `last_round`,
										client_joint_account_details.*  
									FROM client_joint_account_details WHERE client_joint_id=$client_joint_id");
		$data['support_tickets'] = $support_tickets->search(['sender' => 'joint', 'sender_id' => $client_joint_id], 'support_id', 'DESC');

		$client_joint_account_details = new Model_client_joint_account_details();
		$client_joint_account_details->selects = ['account_name', 'account_number'];
		$this->db->where('account_status !=', 'Fixed');
		$this->db->where('account_status !=', 'Deleted');
		$this->db->where('account_status !=', 'Not Reporting');
		$data['letter_accounts'] = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number');
		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$data['other_cards'] = $client_other_cards->search(['id' => $client_joint_id, 'type' => $client_type],'coc_id','DESC');

		$this->load->model('Model_client_other_emails');
		$client_other_emails = new Model_client_other_emails();
		$data['other_emails'] = $client_other_emails->search(['id' => $client_joint_id, 'type' => $client_type]);

		return $data;
	}

	function getResultsChart($client_id, $client_joint_id, $client_type) {

		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$total_data = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
			$total_accounts = $total_data[0]['total_accounts'];
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$total_data = $client_account_details->searchWithSelect(['client_id' => $client_id]);
			$total_accounts = $total_data[0]['total_accounts'];
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_account_details->searchWithSelect(['client_id' => $client_id]);
		}

		$data['total_equifax'] = $total_data[0]['total_equifax'];
		$data['total_experian'] = $total_data[0]['total_experian'];
		$data['total_transunion'] = $total_data[0]['total_transunion'];
		$data['total_accounts'] = $total_data[0]['total_accounts'];
		$data['fixed_equifax'] = $count_accounts[0]['fixed_equifax'];
		$data['fixed_experian'] = $count_accounts[0]['fixed_experian'];
		$data['fixed_transunion'] = $count_accounts[0]['fixed_transunion'];
		$data['fixed_accounts'] = $count_accounts[0]['fixed_accounts'];

		return $data;

	}

	function testing123() {
		$client_id = 692;
		$client_type = 'client';
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$total_data = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
			$total_accounts = $total_data[0]['total_accounts'];
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("( account_status <> 'Not Reporting' AND account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$total_data = $client_account_details->searchWithSelect(['client_id' => $client_id]);
			echo $this->db->last_query();
			$total_accounts = $total_data[0]['total_accounts'];
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_account_details->searchWithSelect(['client_id' => $client_id]);
		}

		$data['total_equifax'] = $total_data[0]['total_equifax'];
		$data['total_experian'] = $total_data[0]['total_experian'];
		$data['total_transunion'] = $total_data[0]['total_transunion'];
		$data['total_accounts'] = $total_data[0]['total_accounts'];
		$data['fixed_equifax'] = $count_accounts[0]['fixed_equifax'];
		$data['fixed_experian'] = $count_accounts[0]['fixed_experian'];
		$data['fixed_transunion'] = $count_accounts[0]['fixed_transunion'];
		$data['fixed_accounts'] = $count_accounts[0]['fixed_accounts'];

		$this->pprint($data);

	}

	function updateEmailETextRead($client_id) {
		$client_etexts = new Model_client_etexts();
		$data_etext = $client_etexts->search(['client_id' => $client_id, 'unread' => 1], '', '');
		foreach ($data_etext as $key => $value) {
			$client_etexts = new Model_client_etexts();
			$client_etexts->load($value['text_id']);
			$client_etexts->unread = 0;
			$client_etexts->save();
		}

		$client_emails = new Model_client_emails();
		$data_etext = $client_emails->search(['client_id' => $client_id, 'unread' => 1], '', '');
		foreach ($data_etext as $key => $value) {
			$client_emails = new Model_client_emails();
			$client_emails->load($value['email_id']);
			$client_emails->unread = 0;
			$client_emails->save();
		}
	}

	function saveTask() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
		$tasks = new Model_tasks();

		if ($client_type == 'joint') {
			$tasks->task_assigned = 'Client Joints';
			$tasks->task_assigned_id = $client_joint_id;
		} else {
			$tasks->task_assigned = 'Clients';
			$tasks->task_assigned_id = $client_id;
		}
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$tasks = new Model_tasks();
		if ($client_type == 'joint') {
			$data = $tasks->search(['task_assigned_id' => $client_joint_id,
				'task_assigned' => 'Client Joints',
				'task_active <>' => 2,
			], 'task_date', 'DESC');
		} else {
			$data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$client_id AND task_assigned='Clients' AND task_active <> 2 ORDER BY task_date DESC, task_id DESC");
			$data = $tasks->search(['task_assigned_id' => $client_id,
				'task_assigned' => 'Clients',
				'task_active <>' => 2,
			], 'task_date', 'DESC');
		}

		echo json_encode($data);
	}

	 

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save();
		echo "updated";
	}

	function saveNote() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');
		$note_date = $this->input->post('note_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
		$notes = new Model_notes();

		if ($client_type == 'joint') {
			$notes->note_assigned = 'Client Joints';
			$notes->note_assigned_id = $client_joint_id;
		} else {
			$notes->note_assigned = 'Clients';
			$notes->note_assigned_id = $client_id;
		}
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = $note_date;
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function saveCall() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$call = $this->input->post('call');
		$call_date = $this->input->post('call_date');
		$call_sticky = $this->input->post('call_sticky');
		$call_date = $this->input->post('call_date');
		$call_type = $this->input->post('call_type');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_calls');
		$calls = new Model_calls();

		if ($client_type == 'joint') {
			$calls->call_assigned = 'Client Joints';
			$calls->call_assigned_id = $client_joint_id;
		} else {
			$calls->call_assigned = 'Clients';
			$calls->call_assigned_id = $client_id;
		}
		$calls->sender = $sender;
		$calls->sender_id = $sender_id;
		$calls->sender_photo = $sender_photo;
		$calls->sender_name = $sender_name;
		$calls->call = $call;
		$calls->call_date = $call_date;
		$calls->call_type = $call_type;
		$calls->save();
	}

	function getNotes() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$notes = new Model_notes();
		if ($client_type == 'joint') {
			$data = $notes->search(['note_assigned_id' => $client_joint_id,
				'note_assigned' => 'Client Joints',
			], 'note_id', 'DESC');
		} else {
			$data = $notes->search(['note_assigned_id' => $client_id,
				'note_assigned' => 'Clients',
			], 'note_id', 'DESC');
		}

		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky;
		$notes->save();
		echo "updated";
	}

	function updateFields() {
		$table = $this->input->post('table');
		$client_id = $this->input->post('client_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($field == 'password') {
			$value = password_hash($value, PASSWORD_BCRYPT);
		}

		$table = 'Model_' . $table;
		$model = new $table;

		$table_for_id = 'client_id';
		if (strpos($table, 'joint') !== false) {
			$table_for_id = 'client_joint_id';
		}

		$data_check = $model->search([$table_for_id => $client_id]);
		// echo json_encode($client_id);
		if (count($data_check) > 0) {
			$data_check = reset($data_check);
			$data_check = reset($data_check);
			$model->load($data_check);
		} else {
			$model->$table_for_id = $client_id;
		}

		$model->$field = $value;
		$model->save();

		// echo $model();;
		// $this->pprint($model);
	}

	function getFirstKey() {
		$model = new Model_clients();
		$data_check = $model->search(['client_id' => 33]);
		$data_check = reset($data_check);
		$data_check = reset($data_check);

	}

	function updateFieldsAccounts() {
		$table = $this->input->post('table');
		$account_id = $this->input->post('account_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');
		$table = 'Model_' . $table;
		$model = new $table;
		$data_check = $model->search(['account_id' => $account_id]);

		$data_check = reset($data_check);
		$data_check = reset($data_check);
		$model->load($data_check);
		$model->$field = $value;
		$model->save();
		// $data = $this->db->query("UPDATE $table SET $field='$value' WHERE account_id=$account_id");

		echo "updated!";
		// echo count($data_check);

	}
	function updateFieldsCreditorsAccounts() {
		$table = $this->input->post('table');
		$ca_id = $this->input->post('ca_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');
		$table = 'Model_' . $table;
		$model = new $table;
		$data_check = $model->search(['ca_id' => $ca_id]);

		$data_check = reset($data_check);
		$data_check = reset($data_check);
		$model->load($data_check);
		$model->$field = $value;
		$model->save();
		// $data = $this->db->query("UPDATE $table SET $field='$value' WHERE account_id=$account_id");

		echo "updated!";
		// echo count($data_check);

	}

	function sendClientEmail() {
		$client_id = $this->input->post('client_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes');
		// $client_type = '';
		$client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$from = $this->input->post('from');

		$client_emails = $this->getClientAllEmails($client_id, $client_type);
		// $this->pprint($client_emails);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		foreach ($client_emails as $key => $value) {
			$this->email->initialize($config);
			if ($from == '') {
				$from = 'no-reply@creditlynx.com';
			}
			$this->email->from($from, 'CreditLynx');
			$this->email->to($value['email_address']);
			$this->email->subject($subject);
			$this->email->message($notes);

			if ($this->email->send()) {
				$this->load->model('Model_client_emails');
				$client_emails = new Model_client_emails();
				$client_emails->client_id = $value['id'];
				$client_emails->client_type = $value['type'];
				$client_emails->type = 'to_client';
				$client_emails->subject = $subject;
				$client_emails->message = $notes;
				$client_emails->date = date('Y-m-d H:i:s');
				$client_emails->from = $from;
				$client_emails->to = $value['email_address'];
				$client_emails->save();
			}
		}

		// $this->sendEmail('Client', $type, $client_id, $subject, $notes, date('Y-m-d H:i:s'), $client_type, $from);
	}
	

	

	function sendClientText() {
		$client_id = $this->input->post('client_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes');
		// $client_type = '';
		$client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$from = '';
		$this->sendEmail('Client', $type, $client_id, $subject, $notes, date('Y-m-d H:i:s'), $client_type, $from);
	}

	function saveAlert() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$alert_subject = $this->input->post('alert_subject');
		$alert_date = $this->input->post('alert_date');
		$alert_notes = $this->input->post('alert_notes');
		$client_type = $this->input->post('client_type');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_client_alerts');
		$client_alerts = new Model_client_alerts();

		if ($client_type == 'joint') {
			$client_alerts->client_id = $client_joint_id;
		} else {
			$client_alerts->client_id = $client_id;
		}

		$client_alerts->client_type = $client_type;
		$client_alerts->sender = $sender;
		$client_alerts->sender_id = $sender_id;
		$client_alerts->sender_photo = $sender_photo;
		$client_alerts->sender_name = $sender_name;
		$client_alerts->alert_subject = $alert_subject;
		$client_alerts->alert_notes = $alert_notes;
		$client_alerts->alert_date = date('Y-m-d',strtotime($alert_date));
		$client_alerts->save();

		if ($this->get_auto_alert(	$client_type == 'client' ? 'client_id' : 'client_joint_id', 
									$client_type == 'client' ? $client_id : $client_joint_id, 
									$client_type == 'client' ? 'clients' : 'client_joints') == 1) { 

			$this->send_email_to_client($client_type == 'client' ? $client_id : $client_joint_id,$alert_subject,$alert_notes,$client_type);

			$this->send_text_alert_to_client($client_type == 'client' ? $client_id : $client_joint_id,$client_type,$alert_subject,$alert_notes); 
		}  
	}


 	function test_send() {
 		$this->send_text_alert_to_client(33,'client','testing testing testing sms subject','Testing testing testing testing testing sms notes');
 	}

	function getAlerts() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$client_alerts = new Model_client_alerts();

		if ($client_type == 'joint') {
			// Client ID = Client Joint ID 
			$data = $this->fetchRawData("SELECT
												client_alerts.*,
											IF
												( HOUR ( alert_date ) = 0, DATE_ADD( alert_date, INTERVAL 20 HOUR ), alert_date ) AS `alert_date_format` ,
												(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM client_joints WHERE client_joints.client_joint_id = client_alerts.client_id) as `client_name`
											FROM
												client_alerts 
											WHERE
												client_id = $client_joint_id 
												AND client_type = 'joint'
											ORDER BY alert_date_format DESC, alert_id DESC");
		} else {
			$data = $this->fetchRawData("SELECT
												client_alerts.*,
											IF
												( HOUR ( alert_date ) = 0, DATE_ADD( alert_date, INTERVAL 20 HOUR ), alert_date ) AS `alert_date_format` ,
												(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM clients WHERE clients.client_id = client_alerts.client_id) as `client_name`
											FROM
												client_alerts 
											WHERE
												client_id = $client_id 
												AND client_type = 'client'
											ORDER BY alert_date_format DESC, alert_id DESC");
		}

		// $data = $this->fetchRawData("SELECT *,(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM clients WHERE clients.client_id = client_alerts.client_id) as `client_name` FROM client_alerts WHERE client_id=$client_id ORDER BY alert_id DESC");
		echo json_encode($data);
	}

	function saveTemplate() {
		$alert_template_id = $this->input->post('alert_template_id');
		$template_shortname = $this->input->post('template_shortname');
		$template_subject = $this->input->post('template_subject');
		$template_notes = $this->input->post('template_notes');

		$this->load->model('Model_client_alerts_templates');
		$client_alerts_templates = new Model_client_alerts_templates();

		if ($alert_template_id != '') {
			$client_alerts_templates->alert_template_id = $alert_template_id;
		}
		$client_alerts_templates->template_shortname = $template_shortname;
		$client_alerts_templates->template_subject = $template_subject;
		$client_alerts_templates->template_notes = $template_notes;

		$client_alerts_templates->save();

	}

	function getAlertTemplates() {
		$client_alerts_templates = new Model_client_alerts_templates();
		$data = $client_alerts_templates->search([], 'template_shortname', 'ASC');
		// $data = $this->fetchRawData("SELECT * FROM client_alerts_templates ORDER BY template_shortname ASC");
		echo json_encode($data);
	}

	function saveAccountType() {
		$at_id = $this->input->post('at_id');
		$account_type = $this->input->post('account_type');

		$account_types = new Model_clients_account_types();
		if ($at_id != '') {
			$account_types->at_id = $at_id;
		}

		$account_types->account_type = $account_type;
		$account_types->save();
		echo $account_types->at_id;
	}

	function saveAccountStatus() {
		$as_id = $this->input->post('as_id');
		$account_status = $this->input->post('account_status');

		$account_statuses = new Model_clients_account_statuses();
		if ($as_id != '') {
			$account_statuses->as_id = $as_id;
		}
		$account_statuses->account_status = $account_status;
		$account_statuses->save();
		echo $account_statuses->as_id;
	}
	function saveAccountPaymentStatus() {
		$ps_id = $this->input->post('ps_id');
		$payment_status = $this->input->post('payment_status');

		$payment_statuses = new Model_clients_account_payment_statuses();
		if ($ps_id != '') {
			$payment_statuses->ps_id = $ps_id;
		}
		$payment_statuses->payment_status = $payment_status;
		$payment_statuses->save();
		echo $payment_statuses->ps_id;
	}
	function saveAccountComment() {
		$ac_id = $this->input->post('ac_id');
		$comment = $this->input->post('comment');

		$comments = new Model_clients_account_comments();
		if ($ac_id != '') {
			$comments->ac_id = $ac_id;
		}
		$comments->comment = $comment;
		$comments->save();
		echo $comments->ac_id;
	}
	function deleteAccountType() {
		$at_id = $this->input->post('at_id');

		$account_types = new Model_clients_account_types();
		$account_types->load($at_id);
		$account_types->delete();
	}

	function deleteCreditorsAccountType() {
		$creditors_account_type_id = $this->input->post('creditors_account_type_id');

		$account_types = new Model_creditors_account_type();
		$account_types->load($creditors_account_type_id);
		$account_types->delete();
	}
	function deleteCreditorsAccount() {
		$ca_id = $this->input->post('ca_id');

		$creditors_account = new Model_creditors_account();
		$creditors_account->load($ca_id);
		$creditors_account->delete();
	}
	function deleteCreditorsRec() {
		$creditors_rec_id = $this->input->post('creditors_rec_id');

		$creditors_rec = new Model_creditors_recommendations();
		$creditors_rec->load($creditors_rec_id);
		$creditors_rec->delete();
	}
	function deleteAccountStatus() {
		$as_id = $this->input->post('as_id');

		$account_statuses = new Model_clients_account_statuses();
		$account_statuses->load($as_id);
		$account_statuses->delete();
	}
	function deleteAccountPaymentStatus() {
		$ps_id = $this->input->post('ps_id');

		$account_payment_statuses = new Model_clients_account_payment_statuses();
		$account_payment_statuses->load($ps_id);
		$account_payment_statuses->delete();
	}
	function deleteAccountComment() {
		$ac_id = $this->input->post('ac_id');

		$account_comments = new Model_clients_account_comments();
		$account_comments->load($ac_id);
		$account_comments->delete();
	}

	function saveDisputeVerbiage() {
		$dv_id = $this->input->post('dv_id');
		$dispute_verbiage = $this->input->post('dispute_verbiage');
		$bank_account_type_for_dispute = $this->input->post('bank_account_type_for_dispute');
		// echo json_encode($bank_account_type_for_dispute);

		$dispute_verbiages = new Model_clients_dispute_verbiages();
		if ($dv_id != '') {
			$dispute_verbiages->dv_id = $dv_id;
		}

		$dispute_verbiages->dispute_verbiage = $dispute_verbiage;
		$dispute_verbiages->save();

		$delete_assigned = $this->db->query("DELETE FROM clients_dispute_verbiages_assigned WHERE dv_id=$dv_id");
		$clients_dispute_verbiages_assigned = new Model_clients_dispute_verbiages_assigned();

		$data_assigned = $clients_dispute_verbiages_assigned->search(['dv_id' => $dv_id]);
		foreach ($data_assigned as $key => $value) {
			$clients_dispute_verbiages_assigned = new Model_clients_dispute_verbiages_assigned();
			$clients_dispute_verbiages_assigned->load($value['adv_id']);
			$clients_dispute_verbiages_assigned->delete();
		}
		foreach ($bank_account_type_for_dispute as $key => $value) {
			$clients_dispute_verbiages_assigned = new Model_clients_dispute_verbiages_assigned();
			$clients_dispute_verbiages_assigned->dv_id = $dv_id;
			$clients_dispute_verbiages_assigned->at_id = $value;
			$clients_dispute_verbiages_assigned->save();
		}
	}

	function getDisputeVerbiagesAccountTypesBy_account_type() {
		$account_type = $this->input->post('account_type');
		$round = $this->input->post('round');
		$clients_dispute_verbiage_assignments = new Model_clients_dispute_verbiage_assignments();
		$data = $clients_dispute_verbiage_assignments->search(['account_type' => $account_type, 'round' => $round], 'dispute_verbiage', 'ASC');
		echo json_encode($data);
	}

	function saveNewAccount() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		$equifax = $this->input->post('equifax');
		$experian = $this->input->post('experian');
		$transunion = $this->input->post('transunion');
		$account_type = $this->input->post('account_type');
		$account_status = $this->input->post('account_status');
		$account_name = $this->input->post('account_name');
		$account_number = $this->input->post('account_number');
		$amount = $this->input->post('amount');
		$dispute_verbiage = $this->input->post('dispute_verbiage');
		$bureau_date = $this->input->post('bureau_date');

		$past_due = $this->input->post('past_due');
		$credit_limit = $this->input->post('credit_limit');
		$file_date = $this->input->post('file_date');
		$chapter = $this->input->post('chapter');
		$inquiry_date = $this->input->post('inquiry_date');
		$correct_info = $this->input->post('correct_info');
		$payment_status = $this->input->post('payment_status');
		$comments = $this->input->post('comments');
		$date_opened = $this->input->post('date_opened');
		$original_creditor = $this->input->post('original_creditor');
		$status_date = $this->input->post('status_date');
		$date_added = date('Y-m-d H:i:s');
		if ($client_type == 'joint') {
			$client_joint_id = $client_id;
			if ($equifax == 'true') {
				$this->saveJointNewAccountDetail($client_joint_id, 'Equifax', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
			if ($experian == 'true') {
				$this->saveJointNewAccountDetail($client_joint_id, 'Experian', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
			if ($transunion == 'true') {
				$this->saveJointNewAccountDetail($client_joint_id, 'TransUnion', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
		} else {
			if ($equifax == 'true') {
				$this->saveNewAccountDetail($client_id, 'Equifax', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
			if ($experian == 'true') {
				$this->saveNewAccountDetail($client_id, 'Experian', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
			if ($transunion == 'true') {
				$this->saveNewAccountDetail($client_id, 'TransUnion', $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date);
			}
		}

	}

	function saveNewAccountDetail($client_id, $bureaus, $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date) {
		$this->load->model('Model_client_account_details');
		$client_account_details = new Model_client_account_details();
		$client_account_details->bureaus = $bureaus;
		$client_account_details->client_id = $client_id;
		$client_account_details->account_status = $account_status;
		$client_account_details->account_type = $account_type;
		$client_account_details->account_name = $account_name;
		$client_account_details->account_number = $account_number;
		$client_account_details->amount = $amount;
		$client_account_details->past_due = $past_due;
		$client_account_details->credit_limit = $credit_limit;
		$client_account_details->file_date = $file_date;
		$client_account_details->chapter = $chapter;
		$client_account_details->inquiry_date = $inquiry_date;
		$client_account_details->correct_info = $correct_info;
		$client_account_details->payment_status = $payment_status;
		$client_account_details->comments = $comments;
		$client_account_details->date_opened = $date_opened;
		$client_account_details->original_creditor = $original_creditor;
		$client_account_details->status_date = $status_date;
		$client_account_details->bureau_date = $bureau_date;
		$client_account_details->date_added = $date_added;
		$client_account_details->save();

		$this->load->model('Model_client_rounds');
		$client_rounds = new Model_client_rounds();
		$client_rounds->account_id = $client_account_details->account_id;
		$client_rounds->dispute_1 = $dispute_verbiage;
		$client_rounds->save();

	}
	function saveJointNewAccountDetail($client_joint_id, $bureaus, $account_status, $account_type, $account_name, $account_number, $amount, $dispute_verbiage, $bureau_date, $past_due, $credit_limit, $file_date, $chapter, $correct_info, $inquiry_date, $date_added, $payment_status, $comments, $date_opened, $original_creditor, $status_date) {
		$this->load->model('Model_client_joint_account_details');
		$client_joint_account_details = new Model_client_joint_account_details();
		$client_joint_account_details->bureaus = $bureaus;
		$client_joint_account_details->client_joint_id = $client_joint_id;
		$client_joint_account_details->account_status = $account_status;
		$client_joint_account_details->account_type = $account_type;
		$client_joint_account_details->account_name = $account_name;
		$client_joint_account_details->account_number = $account_number;
		$client_joint_account_details->amount = $amount;
		$client_joint_account_details->past_due = $past_due;
		$client_joint_account_details->credit_limit = $credit_limit;
		$client_joint_account_details->file_date = $file_date;
		$client_joint_account_details->chapter = $chapter;
		$client_joint_account_details->inquiry_date = $inquiry_date;
		$client_joint_account_details->correct_info = $correct_info;
		$client_joint_account_details->payment_status = $payment_status;
		$client_joint_account_details->comments = $comments;
		$client_joint_account_details->date_opened = $date_opened;
		$client_joint_account_details->original_creditor = $original_creditor;
		$client_joint_account_details->status_date = $status_date;
		$client_joint_account_details->bureau_date = $bureau_date;
		$client_joint_account_details->date_added = $date_added;
		$client_joint_account_details->save();

		$this->load->model('Model_client_joint_rounds');
		$client_joint_rounds = new Model_client_joint_rounds();
		$client_joint_rounds->account_id = $client_joint_account_details->account_id;
		$client_joint_rounds->dispute_1 = $dispute_verbiage;
		$client_joint_rounds->save();

	}

	function getAccountTypesForNegativeAccounts() {
		$client_account_types = new Model_clients_account_types();
		$client_account_types->selects = ['account_type'];
		$account_types = $client_account_types->searchWithSelect();
		return array_column($account_types, 'account_type');
	}

	function getResultsTracker() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');

		// $account_types = $this->getAccountTypesForNegativeAccounts();
		if ($client_type == 'joint') {
			$client_joint_id = $client_id;

			$client_joint_account_details = new Model_client_joint_account_details();
			// $this->db->where_in('account_type',$account_types);
			$data = $client_joint_account_details->search(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');

			// $data = $this->fetchRawData("SELECT * FROM client_joint_account_details WHERE client_joint_id=$client_joint_id GROUP BY IF(date_added IS NULL,account_number,date_added) ORDER BY account_id ASC");
		} else {
			$client_account_details = new Model_client_account_details();
			// $this->db->where_in('account_type',$account_types);
			$data = $client_account_details->search(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');
			// $data = $this->fetchRawData("SELECT * FROM client_account_details WHERE client_id=$client_id GROUP BY IF(date_added IS NULL,account_number,date_added) ORDER BY account_id ASC");
		}
		echo json_encode($data);
	}

	function getCreditorsAccount() {
		$id = $this->input->post('id');
		$client_type = $this->input->post('client_type');
		$creditors_account = new Model_creditors_account();
		$data = $creditors_account->search(['client_type' => $client_type, 'id' => $id], '', '', 0, 0, 'date_added');
		echo json_encode($data);
	}

	function getCreditorsAccountByAccount() {
		$account_number = $this->input->post('account_number');
		$date_added = $this->input->post('date_added');
		$creditors_account = new Model_creditors_account();
		$data = $creditors_account->search(['account_number' => $account_number, 'date_added' => $date_added]);
		echo json_encode($data);
	}

	function getAccountDetailsByAccountNumber() {
		$client_type = $this->input->post('client_type');
		$account_number = $this->input->post('account_number');
		$date_added = $this->input->post('date_added');

		if ($client_type == 'joint') {
			if ($date_added != null) {
				$data = $this->Model_Query->getView('view_joint_account_details_by_account_number', ['account_number' => $account_number, 'date_added' => $date_added], 'account_id', 'ASC');
			} else {
				$data = $this->Model_Query->getView('view_joint_account_details_by_account_number', ['account_number' => $account_number, 'date_added IS NULL' => null], 'account_id', 'ASC');
			}

			// $data = $this->fetchRawData("SELECT client_joint_account_details.*,dispute_1,dispute_2,dispute_3,dispute_4,dispute_5,dispute_6,dispute_7,dispute_8,dispute_9,result_1,result_2,result_3,result_4,result_5,result_6,result_7,result_8,result_9,dispute_1_date,dispute_2_date,dispute_3_date,dispute_4_date,dispute_5_date,dispute_6_date,dispute_7_date,dispute_8_date,dispute_9_date FROM client_joint_account_details LEFT JOIN client_joint_rounds ON client_joint_account_details.account_id = client_joint_rounds.account_id WHERE client_joint_account_details.account_number='$account_number' $extra_q  ORDER BY account_id");
		} else {
			if ($date_added != null) {
				$data = $this->Model_Query->getView('view_account_details_by_account_number', ['account_number' => $account_number, 'date_added' => $date_added], 'account_id', 'ASC');
			} else {
				$data = $this->Model_Query->getView('view_account_details_by_account_number', ['account_number' => $account_number, 'date_added IS NULL' => null], 'account_id', 'ASC');
			}

			// $data = $this->fetchRawData("SELECT client_account_details.*,dispute_1,dispute_2,dispute_3,dispute_4,dispute_5,dispute_6,dispute_7,dispute_8,dispute_9,result_1,result_2,result_3,result_4,result_5,result_6,result_7,result_8,result_9,dispute_1_date,dispute_2_date,dispute_3_date,dispute_4_date,dispute_5_date,dispute_6_date,dispute_7_date,dispute_8_date,dispute_9_date FROM client_account_details LEFT JOIN client_rounds ON client_account_details.account_id = client_rounds.account_id WHERE client_account_details.account_number='$account_number' $extra_q  ORDER BY account_id");
		}
		$data_arr['last_query'] = $this->db->last_query();
		$data_arr['data'] = $data;
		$data_arr['clients_account_statuses'] = $this->getClientAccountStatuses();
		$clients_account_types = new Model_clients_account_types();
		$data_arr['account_types'] = $clients_account_types->search([], 'account_type', 'ASC');

		echo json_encode($data_arr);
	}

	function getClientAccountStatuses() {
		$clients_account_statuses = new Model_clients_account_statuses();
		$data = $clients_account_statuses->search([], 'as_id', 'ASC');
		return $data;
	}

	function changeClientAccountStatus() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$account_status = $this->input->post('account_status');
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->load($account_id);
			$client_joint_account_details->account_status = $account_status;
			$client_joint_account_details->save();
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->load($account_id);
			$client_account_details->account_status = $account_status;
			$client_account_details->save();
		}

	}

	function changeClientAccountType() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$account_type = $this->input->post('account_type');
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->load($account_id);
			$client_joint_account_details->account_type = $account_type;
			$client_joint_account_details->save();
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->load($account_id);
			$client_account_details->account_type = $account_type;
			$client_account_details->save();
		}

	}

	function changeClientNote() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$note = $this->input->post('note');

		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->load($account_id);
			$client_joint_account_details->note = $note;
			$client_joint_account_details->save();
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->load($account_id);
			$client_account_details->note = $note;
			$client_account_details->save();
		}

	}

	function updateClientRounds() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($client_type == 'joint') {
			$client_joint_rounds = new Model_client_joint_rounds();
			$exist_data = $client_joint_rounds->search(['account_id' => $account_id]);

			// $exist_data = $this->fetchRawData("SELECT * FROM client_joint_rounds WHERE account_id=$account_id");
			$client_joint_rounds = new Model_client_joint_rounds();
			if (count($exist_data) > 0) {

				$exist_data = reset($exist_data);
				$exist_data = reset($exist_data);
				$client_joint_rounds->load($exist_data);

			}
			$client_joint_rounds->account_id = $account_id;
			$client_joint_rounds->$field = $value;
			$client_joint_rounds->save();
		} else {
			$client_rounds = new Model_client_rounds();
			$exist_data = $client_rounds->search(['account_id' => $account_id]);
			// $exist_data = $this->fetchRawData("SELECT * FROM client_rounds WHERE account_id=$account_id");

			$client_rounds = new Model_client_rounds();
			if (count($exist_data) > 0) {
				$exist_data = reset($exist_data);
				$exist_data = reset($exist_data);
				$client_rounds->load($exist_data);
			}

			$client_rounds->account_id = $account_id;
			$client_rounds->$field = $value;
			$client_rounds->save();
		}

	}

	function removeAccount() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->load($account_id);
			$client_joint_account_details->delete();
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->load($account_id);
			$client_account_details->delete();
		}

		echo "deleted!";
	}

	function addAccount() {
		$client_type = $this->input->post('client_type');
		$account_number = $this->input->post('account_number');
		$date_added = $this->input->post('date_added');
		$bureau = $this->input->post('bureau');
		if ($date_added == 'null' || $date_added == '') {
			$date_added = NULL;
		}
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$data = $client_joint_account_details->search(['account_number' => $account_number, 'date_added' => $date_added], '', '', 1);
			$data = reset($data);
			$account_id = reset($data);

			$client_joint_account_details->load($account_id);
			$client_joint_account_details->bureaus = $bureau;
			$client_joint_account_details->account_status = 'In Process';
			$client_joint_account_details->account_id = '';
			if ($date_added == '') {
				unset($client_joint_account_details->date_added);
			}
			$client_joint_account_details->force_insert();
		} else {
			$client_account_details = new Model_client_account_details();
			$data = $client_account_details->search(['account_number' => $account_number, 'date_added' => $date_added], '', '', 1);
			$data = reset($data);
			$account_id = reset($data);
			// echo $date_added;

			$client_account_details->load($account_id);
			$client_account_details->bureaus = $bureau;
			$client_account_details->account_status = 'In Process';
			$client_account_details->account_id = '';
			if ($date_added == '') {
				unset($client_account_details->date_added);
			}
			$client_account_details->force_insert();
			// echo $this->db->last_query();
			// echo $date_added;

		}

		// echo "added!";
	}
	function addCreditorsAccount() {
		$account_number = $this->input->post('account_number');
		$date_added = $this->input->post('date_added');
		$bureau = $this->input->post('bureau');
		if ($date_added == 'null' || $date_added == '') {
			$date_added = NULL;
		}

		$creditors_account = new Model_creditors_account();
		$data = $creditors_account->search(['account_number' => $account_number, 'date_added' => $date_added], '', '', 1);
		$data = reset($data);
		$ca_id = reset($data);
		// echo $date_added;

		$creditors_account->load($ca_id);
		$creditors_account->bureau = $bureau;
		$creditors_account->ca_id = '';
		if ($date_added == '') {
			unset($creditors_account->date_added);
		}
		$creditors_account->force_insert();

	}

	function changeCardSent() {
		$table = $this->input->post('table');
		$value = $this->input->post('value');
		$where = $this->input->post('where');
		$id = $this->input->post('id');

		$this->db->where($where, $id);
		$this->db->update($table, ['card_sent' => $value]);

	}

	function getFixedDeletedNotReporting() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');

		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$total_data = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
			$total_accounts = $total_data[0]['total_accounts'];
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id]);
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['SUM(IF(bureaus = \'Equifax\',1,0)) as `total_equifax`',
				'SUM(IF(bureaus = \'Experian\',1,0)) as `total_experian`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'SUM(IF(bureaus = \'TransUnion\',1,0)) as `total_transunion`',
				'COUNT(account_id) as `total_accounts`'];
			$this->db->where("( account_status <> 'Not Reporting' OR account_type <> 'Inquiry' OR account_type <> 'New Inquiry' OR account_type <> 'Wrong Name' OR account_type <> 'Wrong Social' OR account_type <> 'Wrong Address' OR account_type <> 'Wrong Employer')");
			$total_data = $client_account_details->searchWithSelect(['client_id' => $client_id]);
			$total_accounts = $total_data[0]['total_accounts'];
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['IF(SUM(IF(bureaus = \'Equifax\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Equifax\',1,0))) as `fixed_equifax`',
				'IF(SUM(IF(bureaus = \'Experian\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'Experian\',1,0))) as `fixed_experian`',
				'IF(SUM(IF(bureaus = \'TransUnion\',1,0)) IS NULL, 0 , SUM(IF(bureaus = \'TransUnion\',1,0))) as `fixed_transunion`',
				'COUNT(account_id) as `fixed_accounts`'];
			$this->db->where("(account_type <> 'Inquiry' AND account_type <> 'New Inquiry' AND account_type <> 'Wrong Name' AND account_type <> 'Wrong Social' AND account_type <> 'Wrong Address' AND account_type <> 'Wrong Employer')");
			$this->db->where("( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting')  ");
			$count_accounts = $client_account_details->searchWithSelect(['client_id' => $client_id]);
		}

		$data['total_equifax'] = $total_data[0]['total_equifax'];
		$data['total_experian'] = $total_data[0]['total_experian'];
		$data['total_transunion'] = $total_data[0]['total_transunion'];
		$data['total_accounts'] = $total_data[0]['total_accounts'];
		$data['fixed_equifax'] = $count_accounts[0]['fixed_equifax'];
		$data['fixed_experian'] = $count_accounts[0]['fixed_experian'];
		$data['fixed_transunion'] = $count_accounts[0]['fixed_transunion'];
		$data['fixed_accounts'] = $count_accounts[0]['fixed_accounts'];

		echo json_encode($data);

	}

	function getTableByAccount() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['account_name',
				'account_number',
				'account_type',
				'GROUP_CONCAT(bureaus) as `bureaus`',
				'GROUP_CONCAT(account_status) as `account_status`',
			];
			$data = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');
		} else {
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['account_name',
				'account_number',
				'account_type',
				'GROUP_CONCAT(bureaus) as `bureaus`',
				'GROUP_CONCAT(account_status) as `account_status`',
			];
			$data = $client_account_details->searchWithSelect(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number,date_added');
		}

		// $data = $this->fetchRawData("SELECT account_name,account_number,account_type,GROUP_CONCAT(bureaus) as `bureaus`,GROUP_CONCAT(account_status) as `account_status` FROM client_account_details WHERE client_id=$client_id GROUP BY account_number ORDER BY account_name");
		echo json_encode($data);
	}

	function getTableByAccountTest() {
		// $client_id = $this->input->post('client_id');
		$client_id = 412;
		$client_account_details = new Model_client_account_details();
		$client_account_details->selects = ['account_name',
			'account_number',
			'account_type',
			'GROUP_CONCAT(bureaus) as `bureaus`',
			'GROUP_CONCAT(account_status) as `account_status`',
		];
		$data = $client_account_details->searchWithSelect(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number');
		// $data = $this->fetchRawData("SELECT account_name,account_number,account_type,GROUP_CONCAT(bureaus) as `bureaus`,GROUP_CONCAT(account_status) as `account_status` FROM client_account_details WHERE client_id=$client_id GROUP BY account_number ORDER BY account_name");
		// echo json_encode($data);
		$this->pprint($data);
	}

	function getTableByBureaus() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$data = $client_joint_account_details->search(['client_joint_id' => $client_joint_id], 'account_name', 'ASC');
		} else {
			$client_account_details = new Model_client_account_details();
			$data = $client_account_details->search(['client_id' => $client_id], 'account_name', 'ASC');
		}

		// $data = $this->fetchRawData("SELECT * FROM client_account_details WHERE client_id=$client_id ORDER BY account_name");
		echo json_encode($data);
	}

	function saveSupportTicket() {
		$sender = $this->input->post('sender');
		$sender_id = $this->input->post('sender_id');
		$assigned_to = $this->input->post('assigned_to');
		$status = 'Pending';
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$support_date = date('Y-m-d H:i:s');

		$this->load->model('Model_support_tickets');
		$support_tickets = new Model_support_tickets();
		$support_tickets->sender = $sender;
		$support_tickets->sender_id = $sender_id;
		$support_tickets->assigned_to = $assigned_to;
		$support_tickets->status = $status;
		$support_tickets->subject = $subject;
		$support_tickets->message = $message;
		$support_tickets->support_date = $support_date;
		$support_tickets->save();

		// redirect(base_url('admin/clients'));
		echo "saved";

	}

	function getTableSupportTickets() {
		$client_id = $this->input->post('client_id');
		$support_tickets = new Model_support_tickets();
		$data = $support_tickets->search(['sender' => 'Client', 'sender_id' => $client_id], 'support_id', 'DESC');
		// $data = $this->fetchRawData("SELECT * FROM support_tickets WHERE sender='Client' AND sender_id=$client_id ORDER BY support_id DESC");
		echo json_encode($data);
	}

	function deleteSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->delete();
		// $data = $this->db->query("DELETE FROM support_tickets WHERE support_id=$support_id");
		echo 'deleted!';
	}

	function getSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$data['support_tickets'] = $support_tickets->search(['support_id' => $support_id]);
		// $data['support_tickets'] = $this->fetchRawData("SELECT * FROM support_tickets WHERE support_id=$support_id");

		$support_ticket_responses = new Model_support_ticket_responses();
		$data['support_ticket_responses'] = $support_ticket_responses->search(['support_id' => $support_id], 'response_id', 'ASC');
		// $data['support_ticket_responses'] = $this->fetchRawData("SELECT * FROM support_ticket_responses WHERE support_id=$support_id ORDER BY response_id ASC");

		echo json_encode($data);
	}

	function updateSupportTicketStatus() {
		$support_id = $this->input->post('support_id');
		$status = $this->input->post('status');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->status = $status;
		$support_tickets->save();
		// $data = $this->db->query("UPDATE support_tickets SET status='$status' WHERE support_id=$support_id");
		echo 'updated!';
	}

	function saveSupportTicketResponse() {
		$userdata = $this->session->userdata('user_data');

		$support_id = $this->input->post('support_id');
		$responder = $userdata['login_type'];
		$login_type = $userdata['login_type'];
		$response_date = date('Y-m-d H:i:s');
		$response = $this->input->post('response');

		$this->load->model('Model_support_ticket_responses');
		$support_ticket_responses = new Model_support_ticket_responses();
		$support_ticket_responses->support_id = $support_id;
		$support_ticket_responses->responder = $responder;
		$support_ticket_responses->response_date = $response_date;
		$support_ticket_responses->response = $response;
		$support_ticket_responses->save();
		echo "saved";

		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		if ($login_type == 'Administrator' || $login_type == 'Employee') {
			$support_tickets->status = 'Responded';
			// $data = $this->db->query("UPDATE support_tickets SET status='Responded' WHERE support_id=$support_id");
		} else {
			$support_tickets->status = 'Pending';
			// $data = $this->db->query("UPDATE support_tickets SET status='Pending' WHERE support_id=$support_id");
		}
		$support_tickets->save();

	}

	function uploadDocumentFile() {
		$category = $this->input->post('category');
		// $file_name = $this->input->post('file_name');
		$client_status = $this->input->post('client_status');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_id = $this->input->post('client_joint_id');
			$folder_name = $this->getClientFolderName($client_joint_id, $client_type);
		} else {
			$client_id = $this->input->post('client_id');
			$folder_name = $this->getClientFolderName($client_id, $client_type);
		}

		$client_name = $folder_name;

		$folder_name = 'assets/documents/' . $folder_name;
		if (!is_dir($folder_name)) {
			mkdir($folder_name, 0777);
		}
		$files = count($_FILES['document']['name']);
		// $this->pprint($_FILES);
		$this->load->model('Model_client_documents');
		$this->load->model('Model_client_joint_documents');
		if ($files > 1) {
			for ($i = 0; $i < count($_FILES['document']['name']); $i++) {
				if ($_FILES['document']['size'][$i] > 0) {
					$document = $_FILES['document']['name'][$i];
					$file_size = $_FILES['document']['size'][$i];

					
					$file_download = date('YmdHis').$document;

					if (!empty($_FILES['document']['name'][$i])) {
						$filetmp = $_FILES["document"]["tmp_name"][$i];
						$filename = $_FILES["document"]["name"][$i];
						$filetype = $_FILES["document"]["type"][$i];
						$filepath = $folder_name . '/' . $file_download;
						if (file_exists($filepath)) {
							$file_download = '_' . $file_download;
							$filepath = $folder_name . '/' . $file_download;
						}

						move_uploaded_file($filetmp, $filepath);
					}
					if ($client_type == 'joint') {
						$client_joint_documents = new Model_client_joint_documents();
						$client_joint_documents->client_joint_id = $client_joint_id;
						$client_joint_documents->category = $category;
						$client_joint_documents->file_download = $file_download;
						$client_joint_documents->file_size = $file_size;
						$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
						$client_joint_documents->save();
						$account_access = 'id=' . $client_joint_id . '&type=joint';

						if ($category == 'Equifax' || $category == 'Experian' || $category == 'TransUnion' || $category == 'Creditor Letters') {
							$this->load->model('Model_client_joints');
							$client_joints = new Model_client_joints();
							$client_joints->load($client_joint_id);
							$client_joints->client_status = 'Review';
							$client_joints->save();
						}
					} else {
						$client_documents = new Model_client_documents();

						$client_documents->client_id = $client_id;
						$client_documents->category = $category;
						$client_documents->file_download = $file_download;
						$client_documents->file_size = $file_size;
						$client_documents->date_uploaded = date('Y-m-d H:i:s');
						$client_documents->save();
						$account_access = 'id=' . $client_id . '&type=client';

						if ($category == 'Equifax' || $category == 'Experian' || $category == 'TransUnion' || $category == 'Creditor Letters') {
							$this->load->model('Model_clients');
							$clients = new Model_clients();
							$clients->load($client_id);
							$clients->client_status = 'Review';
							$clients->save();
						}
					}
					$this->sendUploadedDocuments($client_name, $client_status, $category, $file_size, $filepath, $account_access);

				}

			}
		} else {
			if ($_FILES['document']['size'][0] > 0) {
				$document = $_FILES['document']['name'][0];
				$file_size = $_FILES['document']['size'][0];

				$file_download = date('YmdHis').$document;

				if (!empty($_FILES['document']['name'][0])) {
					$filetmp = $_FILES["document"]["tmp_name"][0];
					$filename = $_FILES["document"]["name"][0];
					$filetype = $_FILES["document"]["type"][0];
					$filepath = $folder_name . '/' . $file_download;
					if (file_exists($filepath)) {
						$file_download = '_' . $file_download;
						$filepath = $folder_name . '/' . $file_download;
					}

					move_uploaded_file($filetmp, $filepath);
				}

				if ($client_type == 'joint') {
					$client_joint_documents = new Model_client_joint_documents();
					$client_joint_documents->client_joint_id = $client_joint_id;
					$client_joint_documents->category = $category;
					$client_joint_documents->file_download = $file_download;
					$client_joint_documents->file_size = $file_size;
					$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
					$client_joint_documents->save();
					$account_access = 'id=' . $client_joint_id . '&type=joint';

					if ($category == 'Equifax' || $category == 'Experian' || $category == 'TransUnion' || $category == 'Creditor Letters') {
						$this->load->model('Model_client_joints');
						$client_joints = new Model_client_joints();
						$client_joints->load($client_joint_id);
						$client_joints->client_status = 'Review';
						$client_joints->save();
					}
				} else {
					$client_documents = new Model_client_documents();

					$client_documents->client_id = $client_id;
					$client_documents->category = $category;
					$client_documents->file_download = $file_download;
					$client_documents->file_size = $file_size;
					$client_documents->date_uploaded = date('Y-m-d H:i:s');
					$client_documents->save();
					$account_access = 'id=' . $client_id . '&type=client';

					if ($category == 'Equifax' || $category == 'Experian' || $category == 'TransUnion' || $category == 'Creditor Letters') {
						$this->load->model('Model_clients');
						$clients = new Model_clients();
						$clients->load($client_id);
						$clients->client_status = 'Review';
						$clients->save();
					}

				}

				$this->sendUploadedDocuments($client_name, $client_status, $category, $file_size, $filepath, $account_access);

			}

		}

	}

	function testSend() {
		$this->sendUploadedDocuments('jo', 'active', 'Credit Report', 100000, base_url('assets/images/LynxLogo.png'));
	}

	function sendUploadedDocuments($client_name, $client_status, $category, $file_size, $attach_url, $account_access) {
		$account_access = 'https://admin.creditlynx.com/admin/clients?' . $account_access;
		$file_size = round((($file_size / 1000) / 1000), 1) . 'MB';
		if ($category == 'Service Agreement' || $category == 'Address (1)' || $category == 'Address (2)' || $category == 'Social') {
			$from = 'docs@creditlynx.com';
		} else {
			$from = 'reports@creditlynx.com';
		}
		$data['client_name'] = $client_name;
		$data['client_status'] = $client_status;
		$data['category'] = $category;
		$data['file_size'] = $file_size;
		$data['account_access'] = $account_access;
		$body = $this->load->view('email_templates/document_template', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from($from, 'Dispute Team');
		$this->email->to('reports@creditlynx.com');
		$this->email->subject($client_name . ' Uploaded ' . $category);
		$this->email->message($body);
		$this->email->attach($attach_url);
		if ($this->email->send()) {
			echo "Emial has been sent";
			$this->email->initialize($config);
			$this->email->from($from, 'Dispute Team');
			$this->email->to('joshuasaubon@gmail.com');
			$this->email->subject($client_name . ' Uploaded ' . $category);
			$this->email->message($body);
			$this->email->attach($attach_url);
			if ($this->email->send()) {
			}
		} else {
			return $this->email->print_debugger();
		}
	}

	function getClientFolderName($id, $type) {
		if ($type == 'joint') {
			$client_joints = new Model_client_joints();
			$client_joints->selects = ['name'];
			$data = $client_joints->search(['client_joint_id' => $id]);
			// $data = $this->fetchRawData("SELECT name FROM client_joints WHERE client_joint_id=$id");
		} else {
			$clients = new Model_clients();
			$clients->selects = ['name'];
			$data = $clients->search(['client_id' => $id]);
			// $data = $this->fetchRawData("SELECT name FROM clients WHERE client_id=$id");
		}

		return $data[0]['name'];
	}

	function getClientDocuments() {

		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_id = $this->input->post('client_id');
			$data = $this->fetchRawData("SELECT * FROM client_joint_documents WHERE client_joint_id=$client_joint_id ORDER BY date_uploaded DESC");
		} else {
			$client_id = $this->input->post('client_id');
			$data = $this->fetchRawData("SELECT * FROM client_documents WHERE client_id=$client_id ORDER BY date_uploaded DESC");
		}

		echo json_encode($data);
	}

	function deleteCLientDocument() {
		$client_type = $this->input->post('client_type');
		$doc_id = $this->input->post('doc_id');
		if ($client_type == 'joint') {
			$client_joint_documents = new Model_client_joint_documents;
			$client_joint_documents->load($doc_id);
			$client_joint_documents->delete();
			// $data_Delete = $this->db->query("DELETE FROM client_joint_documents WHERE doc_id=$doc_id");
		} else {
			$client_documents = new Model_client_documents();
			$client_documents->load($doc_id);
			$client_documents->delete();
			// $data_Delete = $this->db->query("DELETE FROM client_documents WHERE doc_id=$doc_id");
		}

		echo "deleted!";
	}

	function saveBureauLetterTemplate() {
		$template_header = $this->input->post('template_header');
		$template_title = $this->input->post('template_title');
		$template = $this->input->post('template');

		$letter_templates = new Model_letter_templates();
		$data_select = $letter_templates->search(['template_header' => $template_header,
			'template_title' => $template_title]);
		// $data_select = $this->fetchRawData("SELECT * FROM letter_templates WHERE template_header='$template_header' AND template_title='$template_title'");
		$this->load->model('Model_letter_templates');
		$letter_templates = new Model_letter_templates();
		if (count($data_select) > 0) {
			$letter_templates->blt_id = $data_select[0]['blt_id'];
		}

		$letter_templates->template_header = $template_header;
		$letter_templates->template_title = $template_title;
		$letter_templates->template = $template;
		// echo $letter_templates->template;
		$letter_templates->save();
	}

	function fetchDataTemplateList() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$this->db->where('account_status !=', 'Fixed');
		$this->db->where('account_status !=', 'Deleted');
		$this->db->where('account_status !=', 'Not Reporting');
		$this->db->where('account_status !=', 'Hold');
		if ($client_type == 'joint') {
			$data = $this->Model_Query->getView('view_client_joint_rounds', ['client_joint_id' => $client_joint_id], 'account_name', 'ASC');
		} else {
			$data = $this->Model_Query->getView('view_client_rounds', ['client_id' => $client_id], 'account_name', 'ASC');
		}

		// $data = $this->fetchRawData("SELECT client_id,bureaus,account_name,account_number,client_rounds.* FROM client_account_details LEFT JOIN client_rounds ON client_account_details.account_id = client_rounds.account_id WHERE client_account_details.client_id = $client_id AND account_status <> 'Fixed' AND account_status <> 'Deleted' AND account_status <> 'Not Reporting'");

		echo json_encode($data);
		// echo $this->db->last_query();
	}

	function getLetterCreditors() {
		$letter_creditors = new Model_letter_creditors();
		$data = $letter_creditors->search([], 'name', 'ASC');
		// $data = $this->fetchRawData("SELECT * FROM letter_creditors ORDER BY name ASC");
		echo json_encode($data);
	}

	function getClientLetterAccounts() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');

		if ($client_type != 'joint') {
			$client_account_details = new Model_client_account_details();
			$client_account_details->selects = ['account_name', 'account_number'];
			$this->db->where('account_status !=', 'Fixed');
			$this->db->where('account_status !=', 'Deleted');
			$this->db->where('account_status !=', 'Not Reporting');
			$data = $client_account_details->searchWithSelect(['client_id' => $client_id], 'account_name', 'ASC', 0, 0, 'account_number');
		} else {
			$client_joint_account_details = new Model_client_joint_account_details();
			$client_joint_account_details->selects = ['account_name', 'account_number'];
			$this->db->where('account_status !=', 'Fixed');
			$this->db->where('account_status !=', 'Deleted');
			$this->db->where('account_status !=', 'Not Reporting');
			$data = $client_joint_account_details->searchWithSelect(['client_joint_id' => $client_joint_id], 'account_name', 'ASC', 0, 0, 'account_number');
		}

		// $data = $this->fetchRawData("SELECT account_name,account_number FROM client_account_details WHERE client_id=$client_id AND account_status <> 'Fixed' AND account_status <> 'Deleted' AND account_status <> 'Not Reporting' GROUP BY account_number ORDER BY account_name ASC");
		echo json_encode($data);
	}

	function saveLetterCreditor() {
		$creditor_id = $this->input->post('creditor_id');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$city_state_zip = $this->input->post('city_state_zip');

		$this->load->model('Model_letter_creditors');
		$letter_creditors = new Model_letter_creditors();
		if ($creditor_id != '') {
			$letter_creditors->creditor_id = $creditor_id;
		}
		$letter_creditors->name = $name;
		$letter_creditors->address = $address;
		$letter_creditors->city_state_zip = $city_state_zip;
		$letter_creditors->save();
	}

	function deleteLetterCreditor() {
		$creditor_id = $this->input->post('creditor_id');

		$this->load->model('Model_letter_creditors');
		$letter_creditors = new Model_letter_creditors();
		$letter_creditors->load($creditor_id);
		$letter_creditors->delete();
	}

	function get_client_etexts($id,$type) { 
		// echo $type;
		// $client_etexts = new Model_client_etexts();
		// $data = $client_etexts->search(['client_id' => $client_id], 'text_id', 'DESC');
		if ($type == 'client') {
			$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE phone_number LIKE CONCAT('%',(
											SELECT REPLACE
												( REPLACE ( REPLACE ( REPLACE ( `cell_phone`, '-', '' ), '(', '' ), ')', '' ), ' ', '' ) 
											FROM
												clients 
											WHERE
												client_id = $id LIMIT 1
											) ,'%') ORDER BY text_id DESC");
		} else {
			$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE phone_number LIKE CONCAT('%',(
											SELECT REPLACE
												( REPLACE ( REPLACE ( REPLACE ( `cell_phone`, '-', '' ), '(', '' ), ')', '' ), ' ', '' ) 
											FROM
												client_joints 
											WHERE
												client_joint_id = $id LIMIT 1
											) ,'%')  ORDER BY text_id DESC");
		}
		return $data;
	}

	function getClientTexts() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		// echo $type;
		// $client_etexts = new Model_client_etexts();
		// $data = $client_etexts->search(['client_id' => $client_id], 'text_id', 'DESC');
		if ($type == 'client') {
			$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE phone_number LIKE CONCAT('%',(
											SELECT REPLACE
												( REPLACE ( REPLACE ( REPLACE ( `cell_phone`, '-', '' ), '(', '' ), ')', '' ), ' ', '' ) 
											FROM
												clients 
											WHERE
												client_id = $id LIMIT 1
											) ,'%') ORDER BY text_id DESC");
		} else {
			$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE phone_number LIKE CONCAT('%',(
											SELECT REPLACE
												( REPLACE ( REPLACE ( REPLACE ( `cell_phone`, '-', '' ), '(', '' ), ')', '' ), ' ', '' ) 
											FROM
												client_joints 
											WHERE
												client_joint_id = $id LIMIT 1
											) ,'%')  ORDER BY text_id DESC");
		}
		echo json_encode($data);
	}



	function deleteAlertTemplate() {
		$alert_template_id = $this->input->post('alert_template_id');
		$client_alerts_templates = new Model_client_alerts_templates();
		$client_alerts_templates->load($alert_template_id);
		$client_alerts_templates->delete();
		// $data = $this->db->query("DELETE FROM client_alerts_templates WHERE alert_template_id='$alert_template_id'");
		echo "deleted";
	}

	function convertLeadToClient() {
		$client_id = $this->input->post('client_id');
		$converted_date = date('Y-m-d H:i:s');
		$date_of_enrollment = date('m/d/Y');

		$clients = new Model_clients();
		$clients->load($client_id);
		$clients->converted = 1;
		$clients->converted_date = $converted_date;
		$clients->save();
		// $data_update = $this->db->query("UPDATE clients SET converted=1,converted_date='$converted_date' WHERE client_id=$client_id");
		$client_enrollments = new Model_client_enrollments();
		$data_exist = $client_enrollments->search(['client_id' => $client_id]);
		// $data_exist = $this->fetchRawData("SELECT * FROM client_enrollments WHERE client_id=$client_id");

		$client_enrollments = new Model_client_enrollments();

		if (count($data_exist) > 0) {
			$data_exist = reset($data_exist);
			$data_exist = reset($data_exist);
			$client_enrollments->load($data_exist);
			$client_enrollments->date_of_enrollment = $date_of_enrollment;
			// $data_enrollment_update = $this->db->query("UPDATE client_enrollments SET date_of_enrollment='$date_of_enrollment' WHERE client_id=$client_id");
		} else {
			$client_enrollments->client_id = $client_id;
			$client_enrollments->date_of_enrollment = $date_of_enrollment;
			// $data_enrollment_update = $this->db->query("INSERT INTO client_enrollments  (client_id,date_of_enrollment) VALUES ('$client_id','$date_of_enrollment')");
		}
		$client_enrollments->save();
		$this->sendWelcomeEmail($client_id,'client');


		$client_joints = new Model_client_joints();
		$data_joint = $client_joints->search(['client_id' => $client_id]); 
		if (count($data_joint) > 0) {
			$data_joint = reset($data_joint);
			$client_joint_id = reset($data_joint); 
			$client_joint_enrollments = new Model_client_joint_enrollments();
			$data_joint_enrollment_exist = $client_joint_enrollments->search(['client_joint_id' => $client_joint_id]); 
			if (count($data_joint_enrollment_exist) > 0) {
				$data_joint_enrollment_exist = reset($data_joint_enrollment_exist);
				$client_joint_enrollments_id = reset($data_joint_enrollment_exist);
				$client_joint_enrollments = new Model_client_joint_enrollments();
				$client_joint_enrollments->load($client_joint_enrollments_id);
				$client_joint_enrollments->date_of_enrollment = $date_of_enrollment;
				$client_joint_enrollments->save();
			} else {
				$client_joint_enrollments = new Model_client_joint_enrollments();
				$client_joint_enrollments->client_joint_id = $client_joint_id;
				$client_joint_enrollments->date_of_enrollment = $date_of_enrollment;
				$client_joint_enrollments->save();
				
			}

			$this->sendWelcomeEmail($client_joint_id,'joint');
		}

		// $this->sendWelcomeEmail($client_id);
		echo "converted";
			
	}

	function updateCheckboxValue() {

		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$field = $this->input->post('field');
		$value = $this->input->post('value');
		// $this->db->where('client_id', $client_id);
		// $query = $this->db->get('client_gameplan');
		// if ($query->num_rows() > 0) {
		// 	$data = $this->db->query("UPDATE client_gameplan SET $field='$value' WHERE client_id=$client_id");
		// } else {
		// 	$arrayName = array($field  => $value, 'client_id' => $client_id);
		// 	$this->db->INSERT('client_gameplan', $arrayName);
		// }

		if ($client_type == 'joint') {
			$client_joint_gameplan = new Model_client_joint_gameplan();
			$data_exist = $client_joint_gameplan->search(['client_joint_id' => $client_joint_id]);
			if (count($data_exist) > 0) {
				$data_exist = reset($data_exist);
				$data_exist = reset($data_exist);
				$client_joint_gameplan->load($data_exist);
			}

			$client_joint_gameplan->client_joint_id = $client_joint_id;
			$client_joint_gameplan->$field = $value;
			$client_joint_gameplan->save();
		} else {
			$client_gameplan = new Model_client_gameplan();
			$data_exist = $client_gameplan->search(['client_id' => $client_id]);
			if (count($data_exist) > 0) {
				$data_exist = reset($data_exist);
				$data_exist = reset($data_exist);
				$client_gameplan->load($data_exist);
			}

			$client_gameplan->client_id = $client_id;
			$client_gameplan->$field = $value;
			$client_gameplan->save();
		}

		// echo json_encode($data)
		// echo "saved";
	}

	public function getCheckboxValue() {
		$client_id = $this->input->post('client_id');
		$client_gameplan = new Model_client_gameplan();
		$data = $client_gameplan->search(['client_id' => $client_id]);
		echo json_encode($data);
	}

	function changeAlertDate() {
		$alert_id = $this->input->post('alert_id');
		$alert_date = $this->input->post('alert_date');
		$client_alerts = new Model_client_alerts();
		$client_alerts->load($alert_id);
		$client_alerts->alert_date = date('Y-m-d H:i:s', strtotime($alert_date));
		$client_alerts->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_date='$alert_date' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeTaskDate() {
		$task_id = $this->input->post('task_id');
		$task_date = $this->input->post('task_date');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_date = date('Y-m-d H:i:s', strtotime($task_date));
		$tasks->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_date='$alert_date' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeTaskTask() {
		$task_id = $this->input->post('task_id');
		$task = $this->input->post('task');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task = $task;
		$tasks->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_date='$alert_date' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeAlertNotes() {
		$alert_id = $this->input->post('alert_id');
		$alert_notes = addslashes($this->input->post('alert_notes'));
		$client_alerts = new Model_client_alerts();
		$client_alerts->load($alert_id);
		$client_alerts->alert_notes = $alert_notes;
		$client_alerts->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_notes='$alert_notes' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeAlertSubject() {
		$alert_id = $this->input->post('alert_id');
		$alert_subject = addslashes($this->input->post('alert_subject'));
		$client_alerts = new Model_client_alerts();
		$client_alerts->load($alert_id);
		$client_alerts->alert_subject = $alert_subject;
		$client_alerts->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_notes='$alert_notes' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeAlertClientType() {
		$alert_id = $this->input->post('alert_id');
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$client_alerts = new Model_client_alerts();
		$client_alerts->load($alert_id);
		if ($client_type == 'joint') {
			$client_alerts->client_id = $client_id;
			$client_alerts->client_type = 'client';
		} else {
			$client_alerts->client_id = $client_joint_id;
			$client_alerts->client_type = 'joint';
		}
		$client_alerts->save();
		// $data = $this->db->query("UPDATE client_alerts SET alert_notes='$alert_notes' WHERE alert_id=$alert_id");
		echo "saved";
	}

	function deleteAlert() {
		$alert_id = $this->input->post('alert_id');
		$client_alerts = new Model_client_alerts();
		$client_alerts->load($alert_id);
		$client_alerts->delete();
		// $data = $this->db->query("DELETE FROM client_alerts WHERE alert_id=$alert_id");
		echo "saved";
	}

	function changeNoteDate() {
		$note_id = $this->input->post('note_id');
		$note_date = $this->input->post('note_date');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_date = date('Y-m-d H:i:s', strtotime($note_date));
		$notes->save();
		// $data = $this->db->query("UPDATE notes SET note_date='$note_date' WHERE note_id=$note_id");
		echo "saved";
	}

	function changeNoteNote() {
		$note_id = $this->input->post('note_id');
		$note = addslashes($this->input->post('note'));
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note = $note;
		$notes->save();
		// $data = $this->db->query("UPDATE notes SET note='$note' WHERE note_id=$note_id");
		echo "saved";
	}

	function deleteNote() {
		$note_id = $this->input->post('note_id');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->delete();
		// $data = $this->db->query("DELETE FROM notes WHERE note_id=$note_id");
		echo "saved";
	}

	public function sendEmailInvoice() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$to = $this->input->post('email_address');
		$monthly_fee = $this->input->post('monthly_fee');

		if ($client_type == 'joint') {
			$id = $client_joint_id;
		} else {
			$id = $client_id;
		}

		$session = password_hash('creditlynx' . time(), PASSWORD_BCRYPT);
		$session = str_replace('$', 'X1Rs4XsQln', $session);
		$this->db->query("INSERT INTO signup_sessions (session,status,id,type) VALUES ('$session',0,$id,'$client_type')");

		$data['body'] = 'There was an issue with you recent payment.  Please click the link below to review your account information.';
		$data['href'] = 'https://admin.creditlynx.com/emailCampaign/invoice?id=' . $session;
		$data['name'] = $this->input->post('name');
		$data['to'] = $to;
		$month_list = $this->get_month_list($id,$client_type);
		$data['amount'] = count($month_list) > 0 ? (count($month_list) * $monthly_fee) + (count($month_list) * 15) : 0;

		$body = $this->load->view('invoice/email_invoice', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('billing@creditlynx.com', 'Credit Lynx');
		$this->email->to($this->input->post('email_address'));

		$this->email->subject("Opps. There's something wrong with your account...");
		$this->email->message($body);

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}

		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->name1 = $this->input->post('name');
		$template_sent->email1 = $this->input->post('email_address');
		$template_sent->subject = 'Invoice Email Sent';
		$template_sent->message = 'Waiting for response';
		$template_sent->status = 'unseen';
		$template_sent->date_sent = date('Y-m-d');
		$template_sent->save();
	}

	function get_month_list($id,$type) {
 
		$last_trans = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions GROUP BY `name` )  
					ORDER BY
					NAME"
				); 

 		$return = [];
		foreach ($last_trans as $key => $value) {  
			// GET MONTH NAME OF LAST TRANSACTION 
			// CHECK IF STATUS IS DECLINED, THEN PROCEED
			if ($value['status'] == 'declined') { 
				$card_holder = addslashes($value['name']);
				$card_number = $value['account_number'];
				$data = $this->fetchRawData("SELECT IF(type='client',
								(SELECT client_status FROM clients WHERE client_id=id),
								(SELECT client_status FROM client_joints WHERE client_joint_id=id)) as `client_status`,
								IF(type='client',
								(SELECT name FROM clients WHERE client_id=id),
								(SELECT name FROM client_joints WHERE client_joint_id=id)) as `name`,
								id,type
								FROM view_client_cards WHERE RIGHT(card_number,4)=RIGHT('$card_number',4) AND LCASE(name)=LCASE('$card_holder')"
							);
 
				if (count($data) > 0) { 
					$data = reset($data);
					if ($data['id'] == $id && $data['type'] == $type) {  
						// GET Client NAME,TYPE,CLIENT STATUS AND ID  
						$restrictions = array('Cancelled','Terminated','Archived');

						// CHECK IF CLIENT STATUS IS NOT IN RESTRICTIONS
						if (!in_array($data['client_status'], $restrictions)) {
							// CONDITION THAT ONLY EMMA's ACCOUNT WILL GO THROUGH THE PROCESS
							// NOTE THAT EMMA's Client ID is 33 
								// GET LAST SUCCESSFUL TRANSACTION TO GET MONTH NAME OF NEXT DECLINED SERVICE 
								$month_list = $this->get_last_success_transaction($card_number,$card_holder);

		 						$return = count($month_list) > 0 ? $month_list : [] ;
							
						}
					} 
				}
			} 	
		}

		return $return;
			
	}

	function get_last_success_transaction($card_number,$card_holder) {
		$last_success_trans = $this->fetchRawData("SELECT submitted_local FROM authorize_transactions WHERE name='$card_holder' AND RIGHT(account_number,4)=RIGHT('$card_number',4) AND status = 'settledSuccessfully' ORDER BY submitted_local DESC LIMIT 1");
		$last_success_trans = reset($last_success_trans); 
		$month_now = date('F');
		$month_name = date('F',strtotime($last_success_trans['submitted_local']));
		$month_list = array();
		$counter = 1;
		while ($month_now != $month_name) {
			$month_name = date('F',strtotime($last_success_trans['submitted_local'] . " + $counter months"));
			array_push($month_list, $month_name);
			$counter++; 
		} 
		return $month_list;
	}

	public function sendEmailSingUp() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$cell_phone = $this->input->post('cell_phone');
		$to = $this->input->post('email_address');
		$data['name'] = $this->input->post('name');

		if ($client_type == 'joint') {
			$id = $client_joint_id;
		} else {
			$id = $client_id;
		}

		$session = password_hash('creditlynx' . time(), PASSWORD_BCRYPT);
		$session = str_replace('$', 'X1Rs4XsQln', $session);
		$data['session'] = 'signup-invite?id=' . $session;
		$this->db->query("INSERT INTO signup_sessions (session,status,id,type) VALUES ('$session',0,$id,'$client_type')");

		$body = $this->load->view('email_templates/DocuLynx_template_signup_sa', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to($this->input->post('email_address'));

		$this->email->subject('Credit Lynx Sign Up');
		$this->email->message($body);

		if ($this->email->send()) {
			// echo "sent";
			$signup_url = "https://www.creditlynx.com/signup-invite?id=" . $session;
			$signup_url = $this->bitly_short_url($signup_url);
			echo $signup_url;
			$text_message = "Please click the following link to review and complete your service agreement: ".$signup_url;
			$this->sendRCSms('+1'.$this->trim_phone($cell_phone), $text_message);
		} else {
			echo "error";
		}

		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->name1 = $this->input->post('name');
		$template_sent->email1 = $this->input->post('email_address');
		$template_sent->subject = 'Email Sent';
		$template_sent->message = 'Waiting for submission';
		$template_sent->status = 'unseen';
		$template_sent->date_sent = date('Y-m-d');
		$template_sent->save();


	}

	public function sendEmailSingUpRenew() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$to = $this->input->post('email_address');
		$data['name'] = $this->input->post('name');

		if ($client_type == 'joint') {
			$id = $client_joint_id;
		} else {
			$id = $client_id;
		}

		$session = password_hash('creditlynx' . time(), PASSWORD_BCRYPT);
		$session = str_replace('$', 'X1Rs4XsQln', $session);
		$data['session'] = 'signup?id=' . $session;
		$this->db->query("INSERT INTO signup_sessions (session,status,id,type) VALUES ('$session',0,$id,'$client_type')");

		$body = $this->load->view('email_templates/DocuLynx_template_signup', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to($this->input->post('email_address'));

		$this->email->subject('Credit Lynx Sign Up');
		$this->email->message($body);

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}

		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->name1 = $this->input->post('name');
		$template_sent->email1 = $this->input->post('email_address');
		$template_sent->subject = 'Email Sent';
		$template_sent->message = 'Waiting for submission';
		$template_sent->status = 'unseen';
		$template_sent->date_sent = date('Y-m-d');
		$template_sent->save();
	}

	function sendWelcomeEmailToClientByID() {
		$id = $this->input->post('id');
		$type = $this->input->post('type'); 
		$this->sendWelcomeEmail($id,$type);
	}

	function test321321() {
		// $this->Model_Query->selects = ['auto_alert','client_id'];
		// $data = $this->Model_Query->getView('view_clients_table',['auto_alert !='=> NULL]);
		$data = $this->fetchRawData("SELECT auto_alert,client_id FROM view_clients_table WHERE auto_alert IS NOT NULL");
		$this->pprint($data);
	}

	function autoAlertCron() {
		$this->Model_Query->selects = ['auto_alert', 'client_id'];
		$data = $this->Model_Query->getView('view_clients_table', ['auto_alert !=' => NULL, 'converted' => 1]);
		// $data = $this->fetchRawData("SELECT auto_alert,client_id FROM view_clients_table WHERE auto_alert IS NOT NULL AND converted=1");

		foreach ($data as $key => $client) {
			$auto_alert = $client['auto_alert'];
			$client_id = $client['client_id'];
			echo $auto_alert;
			if ($auto_alert == 7 || $auto_alert == 14 || $auto_alert == 21) {
				# SEND Reminder Alert
				$this->load->model('Model_client_alerts');
				$client_alerts = new Model_client_alerts();

				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Friendly Reminder - Please Look For Responses';
				$client_alerts->alert_notes = 'Please upload all bureau reports and/or creditor letters as soon as possible.  They will be in plain, white envelopes which appear as junk mail. ';
				$client_alerts->alert_date = date('Y-m-d H:i:s');
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
			}

			if ($auto_alert == 28 || $auto_alert == 35 || $auto_alert == 42) {
				# SEND Submit Alert
				$this->load->model('Model_client_alerts');
				$client_alerts = new Model_client_alerts();

				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Friendly Reminder - Please Submit Bureau Reports';
				$client_alerts->alert_notes = 'Please upload all bureau reports as soon as possible. If you haven\'t received anything from the credit bureaus please reach out to us immediately.';
				$client_alerts->alert_date = date('Y-m-d H:i:s');
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
			}

			if ($auto_alert == 46 || $auto_alert == 50) {
				# SEND Deadline Alert
				$this->load->model('Model_client_alerts');
				$client_alerts = new Model_client_alerts();

				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Please Submit Bureau Reports - Bureau Deadline Overdue';
				$client_alerts->alert_notes = 'Please upload all bureau reports as soon as possible.  If you haven\'t received anything from the credit bureaus please reach out to us immediately.';
				$client_alerts->alert_date = date('Y-m-d H:i:s');
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
			}
		}

	}

	function getNextPreviousClient() {
		$page = $this->input->post('page');
		$direction = $this->input->post('direction');
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$order = $this->input->post('order');
		$by = $this->input->post('by');
		$filter = $this->input->post('filter');
		$search = $this->input->post('search');

		// COLUMNS
		if ($page == 'Leads') {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'last_contact',
				'paid',
				'broker',
				'agent',
				'lead_status',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'last_contact',
				'joint_paid',
				'broker',
				'agent',
				'lead_status',
				'client_joint_id',
			];
		} else {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'paid',
				'cycle',
				'client_status',
				'dfr',
				'last_alert',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'joint_paid',
				'joint_cycle',
				'joint_client_status',
				'joint_dfr',
				'joint_last_alert',
				'client_joint_id',
			];
		}

		// WHERE LIKE STATEMENTS
		if ($filter != '' && $search != '') {
			$this->db->like($filter, $search);
		}

		if ($filter == '' && $search != '') {
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			}
			foreach ($bColumns as $key => $value) {
				$this->db->or_like($value, $search);
			}
			$this->db->group_end();
		}

		if ($order == 'day_due') {
			$this->db->where('day_due !=', '');
		}

		if ($order == 'dfr') {
			$this->db->where('dfr !=', '');
		}
		if ($order == 'paid') {
			$this->db->where('paid !=', '');
		}
		if ($order != '') {
			$order = $order . ',converted_date';
		} else {
			$order = 'converted_date';
			$by = 'DESC';
		}

		if ($page == 'Clients') {
			$this->db->where('converted', 1);
		} else {
			$this->db->where('converted', 0);
		}
		$this->db->where('client_status !=', 'Archived');
		$this->db->where('client_status !=', 'Cancelled');
		$this->db->where('client_status !=', 'Terminated');
		if ($type == 'joint') {
			$data = $this->Model_Query->getView('view_clients_table', [], $order, $by);
		} else {
			$data = $this->Model_Query->getView('view_clients_table', [], $order, $by);
		}

		foreach ($data as $key => $value) {
			if ($type == 'joint') {
				if ($value['client_joint_id'] == $id) {
					if ($direction == 'next') {
						echo $data[$key + 1]['client_id'] . '_client';
					}
					if ($direction == 'prev') {
						echo $data[$key]['client_id'] . '_client';
					}
				}
			} else {
				if ($value['client_id'] == $id) {
					if ($direction == 'next') {
						if ($value['client_joint_id'] == '' || $value['client_joint_id'] == 'null') {
							echo $data[$key + 1]['client_id'] . '_client';
						} else {
							echo $data[$key]['client_joint_id'] . '_joint_' . $data[$key]['client_id'];
						}
					}
					if ($direction == 'prev') {
						if ($data[$key - 1]['client_joint_id'] == '') {
							echo $data[$key - 1]['client_id'] . '_client';
						} else {
							echo $data[$key - 1]['client_joint_id'] . '_joint_' . $data[$key - 1]['client_id'];
						}
					}
				}
			}
		}

		// $this->pprint($data);

	}

	function getNextPreviousClientTest() {
		//  	$direction = $this->input->post('direction');
		// $id = $this->input->post('id');
		// $type = $this->input->post('type');
		// $order = $this->input->post('order');
		// $by = $this->input->post('by');
		$page = 'Clients';
		$direction = 'next';
		$id = 834;
		$type = 'client';
		$order = '';
		$by = '';
		$filter = '';
		$search = '';

		// COLUMNS
		if ($page == 'Leads') {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'last_contact',
				'paid',
				'broker',
				'agent',
				'lead_status',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'last_contact',
				'joint_paid',
				'broker',
				'agent',
				'lead_status',
				'client_joint_id',
			];
		} else {
			$aColumns = [
				'name',
				'cell_phone',
				'email_address',
				'paid',
				'cycle',
				'client_status',
				'dfr',
				'last_alert',
				'client_id',
			];

			$bColumns = [
				'joint_name',
				'joint_cell_phone',
				'joint_email_address',
				'joint_paid',
				'joint_cycle',
				'joint_client_status',
				'joint_dfr',
				'joint_last_alert',
				'client_joint_id',
			];
		}

		// WHERE LIKE STATEMENTS
		if ($filter != '' && $search != '') {
			$this->db->like($filter, $search);
		}

		if ($filter == '' && $search != '') {
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			}
			foreach ($bColumns as $key => $value) {
				$this->db->or_like($value, $search);
			}
			$this->db->group_end();
		}

		if ($order == 'day_due') {
			$this->db->where('day_due !=', '');
		}

		if ($order == 'dfr') {
			$this->db->where('dfr !=', '');
		}
		if ($order == 'paid') {
			$this->db->where('paid !=', '');
		}
		if ($order != '') {
			$order = $order . ',converted_date';
		} else {
			$order = 'converted_date';
			$by = 'DESC';
		}

		if ($page == 'Clients') {
			$this->db->where('converted', 1);
		} else {
			$this->db->where('converted', 0);
		}
		$this->db->where('client_status !=', 'Archived');
		$this->db->where('client_status !=', 'Cancelled');
		$this->db->where('client_status !=', 'Terminated');
		if ($type == 'joint') {
			$data = $this->Model_Query->getView('view_clients_table', [], $order, $by);
		} else {
			$data = $this->Model_Query->getView('view_clients_table', [], $order, $by);
		}

		// foreach ($data as $key => $value) {
		// 	if ($type == 'joint') {
		// 		if ($value['client_joint_id'] == $id) {
		// 			if ($direction == 'next') {
		// 				echo $data[$key + 1]['client_id'] . '_client';
		// 			}
		// 			if ($direction == 'prev') {
		// 				echo $data[$key]['client_id'] . '_client';
		// 			}
		// 		}
		// 	} else {
		// 		if ($value['client_id'] == $id) {
		// 			if ($direction == 'next') {
		// 				if ($value['client_joint_id'] == '' || $value['client_joint_id'] == 'null') {
		// 					echo $data[$key + 1]['client_id'] . '_client';
		// 				} else {
		// 					echo $data[$key]['client_joint_id'] . '_joint_' . $data[$key]['client_id'];
		// 				}
		// 			}
		// 			if ($direction == 'prev') {
		// 				if ($data[$key - 1]['client_joint_id'] == '') {
		// 					echo $data[$key - 1]['client_id'] . '_client';
		// 				} else {
		// 					echo $data[$key - 1]['client_joint_id'] . '_joint_' . $data[$key - 1]['client_id'];
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// $this->pprint($data);
		echo $this->db->last_query();
		echo "<br>";
		echo "SELECT * FROM `view_clients_table` WHERE `converted` = 1 AND `client_status` != 'Archived' AND `client_status` != 'Cancelled' AND `client_status` != 'Terminated' ORDER BY `converted_date` DESC LIMIT 50";

	}

	function getAccountsByBureau() {
		$bureau = $this->input->post('bureau');
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');

		if ($client_type == 'joint') {
			$data = $this->Model_Query->getView('view_client_joint_account_details_by_bureau',
				[
					'client_joint_id' => $client_joint_id,
					'bureaus' => $bureau,
				]
				, 'account_name', 'ASC');
		} else {
			$data = $this->Model_Query->getView('view_client_account_details_by_bureau',
				[
					'client_id' => $client_id,
					'bureaus' => $bureau,
				],
				'account_name', 'ASC'
			);
		}
		$data_arr['data'] = $data;
		$data_arr['clients_account_statuses'] = $this->getClientAccountStatuses();
		$clients_account_types = new Model_clients_account_types();
		$data_arr['account_types'] = $clients_account_types->search([], 'account_type', 'ASC');
		// echo $client_id;
		echo json_encode($data_arr);
	}

	function updateTableRow() {
		$table = $this->input->post('table');
		$field = $this->input->post('field');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$table = 'Model_' . $table;
		$table = new $table;
		$table_data = $table->search([$field => $from]);
		foreach ($table_data as $key => $value) {
			$id = reset($value);
			$table = new $table;
			$table->load($id);
			$table->$field = $to;
			$table->save();
		}
	}

	function saveAccountBySourceCode() {
		$client_id = $this->input->post('client_id');
		$client_joint_id = $this->input->post('client_joint_id');
		$client_type = $this->input->post('client_type');
		$accounts_to_save = $this->input->post('accounts_to_save');
		$credit_scores_to_save = $this->input->post('credit_scores_to_save');

		// foreach ($credit_scores_to_save as $field => $gameplan_points) {
		if ($client_type == 'joint') {
			$client_joint_gameplan_points = new Model_client_joint_gameplan_points();
			foreach ($credit_scores_to_save as $field => $value) {
				$client_joint_gameplan_points->$field = $value;
			}
			$client_joint_gameplan_points->client_joint_id = $client_joint_id;
			$client_joint_gameplan_points->save();
		} else {
			$client_gameplan_points = new Model_client_gameplan_points();
			foreach ($credit_scores_to_save as $field => $value) {
				$client_gameplan_points->$field = $value;
			}
			$client_gameplan_points->client_id = $client_id;
			$client_gameplan_points->save();
			// $this->pprint($client_gameplan_points);
		}
		// }

		foreach ($accounts_to_save as $key => $accounts) {
			$date_added = $this->addTime(date('Y-m-d H:i:s'), $key);
			foreach ($accounts as $key => $account) {
				if ($account['account_number']) {
					unset($account[0]);

					if ($client_type == 'joint') {
						$client_joint_account_details = new Model_client_joint_account_details();
						foreach ($account as $field => $value) {
							if ($field == 'account_status' && $value == 'Open') {
								$value = 'In Process';
							}
							if ($field == 'account_status' && $value == 'Collection Account') {
								$value = 'Collection';
							}
							if ($field == 'account_status' && $value == 'Installment Account') {
								$value = 'Installment';
							}
							if ($field == 'account_status' && $value == 'Revolving Account') {
								$value = 'Revolving';
							}
							$client_joint_account_details->$field = $this->trim_str($value);
						}
						$client_joint_account_details->client_joint_id = $client_joint_id;
						$client_joint_account_details->date_added = $date_added;
						$client_joint_account_details->save();

						$client_joint_rounds = new Model_client_joint_rounds();
						$client_joint_rounds->account_id = $client_joint_account_details->account_id;
						$client_joint_rounds->dispute_1 = $this->getDisputeVerbiageByAccountType($client_joint_account_details->account_type);
						$client_joint_rounds->save();

					} else {
						$client_account_details = new Model_client_account_details();
						foreach ($account as $field => $value) {
							if ($field == 'account_status' && $value == 'Open') {
								$value = 'In Process';
							}
							if ($field == 'account_status' && $value == 'Collection Account') {
								$value = 'Collection';
							}
							if ($field == 'account_status' && $value == 'Installment Account') {
								$value = 'Installment';
							}
							if ($field == 'account_status' && $value == 'Revolving Account') {
								$value = 'Revolving';
							}
							$client_account_details->$field = $this->trim_str($value);
						}
						$client_account_details->client_id = $client_id;
						$client_account_details->date_added = $date_added;
						$client_account_details->save();

						$client_rounds = new Model_client_rounds();
						$client_rounds->account_id = $client_account_details->account_id;
						$client_rounds->dispute_1 = $this->getDisputeVerbiageByAccountType($client_account_details->account_type);
						$client_rounds->save();
					}

				}
			}
		}
	}

	function getDisputeVerbiageByAccountType($account_type) {
		// $account_type = 'Collection';
		$this->Model_Query->selects = ['dispute_verbiage'];
		$data = $this->Model_Query->getView('view_account_types_verbiages', ['account_type' => $account_type], 'dispute_verbiage', 'ASC', 0, 0, 'dispute_verbiage');
		if (count($data) > 0) {
			return reset($data[rand(0, count($data) - 1)]);
		} else {
			return '';
		}

	}

	function addTime($time, $minute) {
		return date('Y-m-d H:i:s', strtotime($time . "+$minute minutes"));
	}

	function getClientGrade() {
		// $name = 'Darlene Jacobs';
		$name = $this->input->post('card_holder');
		$card_number = $this->input->post('card_number');
		$data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE RIGHT(account_number,4)=RIGHT('$card_number',4) AND name='$name'");
		$declined = 0;
		foreach ($data as $key => $value) {
			if ($value['status'] == 'declined') {
				$declined++;
			}
		}
		if ($declined == 0) {
			$grade = 'A';
		} else if ($declined == 1) {
			$grade = 'B';
		} else if ($declined == 2) {
			$grade = 'C';
		} else if ($declined == 3) {
			$grade = 'D';
		} else {
			$grade = 'F';
		}

		$data = $this->fetchRawData("SELECT client_id FROM client_billings WHERE RIGHT(card_number,4)=RIGHT('$card_number',4) ORDER BY client_id DESC LIMIT 1");

		if (count($data) > 0) {
			$client_id = $data[0]['client_id'];
			$data = $this->fetchRawData("SELECT account_id FROM client_account_details WHERE client_id=$client_id AND account_type IN ('New Collection','New Charge-Off','New 30 Day Late','New 60 Day Late','New 90 Day Late','New 120 Day Late','New 150 Day Late','New 180 Late Payment','New Repossession','New Judgment','New Tax Lien','New Bankruptcy','New Foreclosure','New Inquiry')");
			if (count($data) > 0) {
				$grade .= '1';
			}
		} else {
			$data = $this->fetchRawData("SELECT client_joint_id FROM client_joint_billings WHERE RIGHT(card_number,4)=RIGHT('$card_number',4) ORDER BY client_joint_id DESC LIMIT 1");
			// $this->pprint($data);
			if (count($data) > 0) {
				$client_joint_id = $data[0]['client_joint_id'];
				$data = $this->fetchRawData("SELECT account_id FROM client_joint_account_details WHERE client_joint_id=$client_joint_id AND account_type IN ('New Collection','New Charge-Off','New 30 Day Late','New 60 Day Late','New 90 Day Late','New 120 Day Late','New 150 Day Late','New 180 Late Payment','New Repossession','New Judgment','New Tax Lien','New Bankruptcy','New Foreclosure','New Inquiry')");
				if (count($data) > 0) {
					$grade .= '1';
				}
			}
		}

		echo $grade;
	}

	public function invoiceTemplate() {
		$this->load->view('admin/email_templates/invoice.php');
	}

	function getBrokerDetails() {
		$broker_id = $this->input->post('broker_id');
		$data = $this->fetchRawData("SELECT * FROM brokers WHERE broker_id=$broker_id");
		echo json_encode($data);
	}

	function updateAccountDetails() {
		$date_added = $this->input->post('date_added');
		$account_name = $this->input->post('account_name');
		$account_number = $this->input->post('account_number');
		$id = $this->input->post('id');
		$type = $this->input->post('type');

		if ($type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$data = $client_joint_account_details->search(['client_joint_id' => $id, 'date_added' => $date_added]);
			// echo json_encode($data);
			foreach ($data as $key => $value) {
				$account_id = $value['account_id'];
				$client_joint_account_details = new Model_client_joint_account_details();
				$client_joint_account_details->load($account_id);
				$client_joint_account_details->account_name = $account_name;
				$client_joint_account_details->account_number = $account_number;
				$client_joint_account_details->save();
			}
		} else {
			$client_account_details = new Model_client_account_details();
			$data = $client_account_details->search(['client_id' => $id, 'date_added' => $date_added]);
			// echo json_encode($data);
			foreach ($data as $key => $value) {
				$account_id = $value['account_id'];
				$client_account_details = new Model_client_account_details();
				$client_account_details->load($account_id);
				$client_account_details->account_name = $account_name;
				$client_account_details->account_number = $account_number;
				$client_account_details->save();
			}
		}

	}
	function deleteAccountDetails() {
		$date_added = $this->input->post('date_added');
		$account_name = $this->input->post('account_name');
		$account_number = $this->input->post('account_number');
		$id = $this->input->post('id');
		$type = $this->input->post('type');

		if ($type == 'joint') {
			$client_joint_account_details = new Model_client_joint_account_details();
			$data = $client_joint_account_details->search(['client_joint_id' => $id, 'date_added' => $date_added]);
			// echo json_encode($data);
			foreach ($data as $key => $value) {
				$account_id = $value['account_id'];
				$client_joint_account_details = new Model_client_joint_account_details();
				$client_joint_account_details->load($account_id);
				$client_joint_account_details->delete();
			}
		} else {
			$client_account_details = new Model_client_account_details();
			$data = $client_account_details->search(['client_id' => $id, 'date_added' => $date_added]);
			// echo json_encode($data);
			foreach ($data as $key => $value) {
				$account_id = $value['account_id'];
				$client_account_details = new Model_client_account_details();
				$client_account_details->load($account_id);
				$client_account_details->delete();
			}
		}

	}

	function saveClientOtherCard() {
		$category = $this->input->post('category');
		$coc_id = $this->input->post('coc_id');
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$card_number = $this->input->post('card_number');
		$card_exp = $this->input->post('card_exp');
		$card_cvv = $this->input->post('card_cvv');

		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		if ($coc_id != '') {
			$client_other_cards->coc_id = $coc_id;
		}
		$client_other_cards->id = $id;
		$client_other_cards->type = $type;
		$client_other_cards->card_number = $card_number;
		$client_other_cards->card_exp = $card_exp;
		$client_other_cards->card_cvv = $card_cvv;
		$client_other_cards->save();
		echo $client_other_cards->coc_id;
	}

	function deleteClientOtherCard() {
		$coc_id = $this->input->post('coc_id');
		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$client_other_cards->load($coc_id);
		$client_other_cards->delete();
	}

	function getClientOtherCards() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$data = $client_other_cards->search(['id' => $id, 'type' => $type],'coc_id','DESC');
		echo json_encode($data);
	}

	function saveClientOtherEmail() {
		$coe_id = $this->input->post('coe_id');
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$email_address = $this->input->post('email_address');

		$this->load->model('Model_client_other_emails');
		$client_other_emails = new Model_client_other_emails();
		if ($coe_id != '') {
			$client_other_emails->coe_id = $coe_id;
		}
		$client_other_emails->id = $id;
		$client_other_emails->type = $type;
		$client_other_emails->email_address = $email_address;
		$client_other_emails->save();
		echo $client_other_emails->coe_id;
	}

	function getClientOtherEmails() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$this->load->model('Model_client_other_emails');
		$client_other_emails = new Model_client_other_emails();
		$data = $client_other_emails->search(['id' => $id, 'type' => $type]);
		echo json_encode($data);
	}

	function deleteClientOtherEmail() {
		$coe_id = $this->input->post('coe_id');
		$this->load->model('Model_client_other_emails');
		$client_other_emails = new Model_client_other_emails();
		$client_other_emails->load($coe_id);
		$client_other_emails->delete();
	}

	function email_invoice() {
		$data = [];
		$data['to'] = 'joshuasaubon@gmail.com';
		$data['invoice_number'] = '648912389';
		$data['invoice_date'] = '05/01/2019';
		$data['for_email'] = true;
		$this->load->view('invoice/invoice_template', $data);
	}

	// function delete_lead_client() {
	// 	$client_id = $this->input->post('client_id');
	// 	$clients = new Model_clients();
	// 	$clients->load($client_id);
	// 	if ($clients->client_type == 'Joint') {
	// 		$client_joint_id = $this->fetchRawData("SELECT client_joint_id FROM client_joints WHERE client_id=$client_id LIMIT 1");
	// 		$client_joint_id = reset($client_joint_id);
	// 		$client_joint_id = reset($client_joint_id);
	// 		$client_joints = new Model_client_joints();
	// 		$client_joints->load($client_joint_id);

	// 	}
	// }

	
	function validate_name() {
		$name = $this->input->post('name');
		$data = $this->fetchRawData("SELECT * FROM view_export_list WHERE name='$name'");
		if (count($data) > 0) {
			echo 1;
		} else {
			echo 0;
		}
	}

	function set_to_primary_card() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$coc_id = $this->input->post('coc_id');
		if ($type == 'client') {
			$client = $this->fetchRawData("SELECT * FROM clients WHERE client_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else { 
			$client = $this->fetchRawData("SELECT * FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT * FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing); 
		}

		$this->load->model('Model_client_other_cards');
		$client_other_cards = new Model_client_other_cards();
		$client_other_cards->load($coc_id); 

		// echo $id;
		if ($type == 'client') {
			$this->load->model('Model_client_billings');
			$client_billings = new Model_client_billings();
			$client_billings->load($billing['client_billing_id']);

			$this->load->model('Model_client_other_cards');
			$new_client_other_cards = new Model_client_other_cards();
			$new_client_other_cards->card_holder = $client_billings->card_holder;
			$new_client_other_cards->card_number = $client_billings->card_number;
			$new_client_other_cards->card_exp = $client_billings->card_expiration;
			$new_client_other_cards->card_cvv = $client_billings->cvv_code;
			$new_client_other_cards->shipping_address = $client_billings->address;
			$new_client_other_cards->shipping_city = $client_billings->city;
			$new_client_other_cards->shipping_state = $client_billings->state;
			$new_client_other_cards->shipping_zip = $client_billings->zip;
			$new_client_other_cards->id = $id;
			$new_client_other_cards->type = $type;
			$new_client_other_cards->save();


			$client_billings->card_holder = $client_other_cards->card_holder;
			$client_billings->card_number = $client_other_cards->card_number;
			$client_billings->card_expiration = $client_other_cards->card_exp;
			$client_billings->cvv_code = $client_other_cards->card_cvv;
			$client_billings->address = $client_other_cards->shipping_address;
			$client_billings->city = $client_other_cards->shipping_city;
			$client_billings->state = $client_other_cards->shipping_state;
			$client_billings->zip = $client_other_cards->shipping_zip;
			$client_billings->save(); 

			$client_other_cards->delete();
			// $this->pprint($client_billings);
			
		} else {
			$this->load->model('Model_client_joint_billings');
			$client_joint_billings = new Model_client_joint_billings();
			$client_joint_billings->load($billing['client_billing_id']);


			$this->load->model('Model_client_other_cards');
			$new_client_other_cards = new Model_client_other_cards();
			$new_client_other_cards->card_holder = $client_billings->card_holder;
			$new_client_other_cards->card_number = $client_billings->card_number;
			$new_client_other_cards->card_exp = $client_billings->card_expiration;
			$new_client_other_cards->card_cvv = $client_billings->cvv_code;
			$new_client_other_cards->shipping_address = $client_billings->address;
			$new_client_other_cards->shipping_city = $client_billings->city;
			$new_client_other_cards->shipping_state = $client_billings->state;
			$new_client_other_cards->shipping_zip = $client_billings->zip;
			$new_client_other_cards->id = $id;
			$new_client_other_cards->type = $type;
			$new_client_other_cards->save();



			$client_joint_billings->card_holder = $client_other_cards->card_holder;
			$client_joint_billings->card_number = $client_other_cards->card_number;
			$client_joint_billings->card_expiration = $client_other_cards->card_exp;
			$client_joint_billings->cvv_code = $client_other_cards->card_cvv;
			$client_joint_billings->address = $client_other_cards->shipping_address;
			$client_joint_billings->city = $client_other_cards->shipping_city;
			$client_joint_billings->state = $client_other_cards->shipping_state;
			$client_joint_billings->zip = $client_other_cards->shipping_zip;
			$client_joint_billings->save();

			$client_other_cards->delete();
		}
	}


}
