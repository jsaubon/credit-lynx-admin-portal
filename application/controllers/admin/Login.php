<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

	public function index() {
		// $this->checklog();
		$this->load->view('admin/Login.php');
	}
	function loginUser() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		// $data = $this->fetchRawData("SELECT * FROM admins WHERE username='$username'");
		$this->load->model('Model_admins');
		$admins = new Model_admins();
		$data = $admins->search(['username' => $username], '', '', 1);
		$data = reset($data);
		// $this->pprint($data);

		$password_hashed = $data['password'];
		if (password_verify($password, $password_hashed)) {

			if (!empty($data)) {

				$date_now = date('Y-m-d H:i:s');
				$user_id = $data['admin_id'];
				$admins = new Model_admins();
				$admins->load($user_id);
				$admins->last_login = $date_now;
				$admins->save();
				// $data_update = $this->db->query("UPDATE admins SET last_login='$date_now' WHERE username='$username'");

				$this->load->model('Model_online_users');
				$online_users = new Model_online_users();
				$online_users->user_type = 'Administrator';
				$online_users->user_id = $user_id;
				$online_users->save();
				echo "Valid";
				$data['subscription_type'] = $this->subscription_type();
				$data_session = array(
					'username' => $data['username'],
					'name' => $data['name'],
					'user_id' => $data['admin_id'],
					'photo' => $data['photo'],
					'login_type' => 'Administrator',
					'login_folder' => 'admins',
					'subscription_type' => $this->subscription_type(),
					'lead_statuses' => $this->getLeadStatuses(),
					'client_statuses' => $this->getClientStatuses(),
					'agents' => $this->getAgents(),
					'brokers' => $this->getBrokers(),
					'sales' => $this->getSales(),
					'processors' => $this->getProcessors(),
					'state_provinces' => $this->getStateProvinces(),
					'getNoteTemplatesInit' => $this->getNoteTemplatesInit(),
					'account_types' => $this->getAccountTypesInit()
				);
				$this->session->set_userdata(array('user_data' => $data_session));

			} else {
				echo 0;
			}
		} else {
			echo 0;
		}
	}


	function getLeadStatuses() {
		$this->load->model('Model_lead_statuses');
		$lead_statuses = new Model_lead_statuses();
		$data = $lead_statuses->search([], 'ls_id', 'ASC');
		return $data;
	}

	function getClientStatuses() {
		$this->load->model('Model_client_statuses');
		$client_statuses = new Model_client_statuses();
		$data = $client_statuses->search([], 'client_status', 'ASC');
		return $data;
	}


	function subscription_type() {

		$this->load->model('Model_subscription');
		$subscription = new Model_subscription();
		$data = $subscription->get();
		return $data;
	}


	function logout() {
		// $userdata = $this->session->userdata('user_data');
		// $user_id = $userdata['user_id'];
		// $login_type = $userdata['login_type'];
		// if (isset($login_type)) {
		// $this->load->model('Model_online_users');

		// $online_users = new Model_online_users();
		// if (!empty($user_id)) {
		//   $online_users->load($user_id);
		//   $online_users->delete();
		// }
		session_destroy();

		// }

		redirect(base_url('admin/login'));
	}

}