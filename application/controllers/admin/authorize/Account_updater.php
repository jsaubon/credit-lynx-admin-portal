<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
// date_default_timezone_set(timezone_name_from_abbr("EST"));
date_default_timezone_set('America/Los_Angeles');
// date_default_timezone_set('America/New_York');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');


// define("AUTHORIZENET_LOG_FILE", "phplog"); 
class Account_updater extends MY_Controller {

	public function index()
	{
		
		$this->checklog(); 
		$data['userdata'] = $this->session->userdata('user_data');  
		$this->load->view('admin/authorize/Account_updater',$data);	
	}

	function getAccountUpdaterJobDetails()
	{
	    /* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName(MERCHANT_LOGIN_ID);
	    $merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
	    
	    // Set the request's refId
	    $refId = 'ref' . time();

	    // Set a valid month (and other parameters) for the request
	    // $cur_date = date('Y-m');
	    $month = $this->input->post('year_month');  
	    // $month = '2019-07';
	    $modifedTypeFilter = "all";
	    $paging = new AnetAPI\PagingType;
	    $paging->setLimit("1000");
	    $paging->setOffset("1");

	    // Build tbe request object
	    $request = new AnetAPI\GetAUJobDetailsRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setMonth($month);
	    $request->setModifiedTypeFilter($modifedTypeFilter);
	    $request->setPaging($paging);

	    $controller = new AnetController\GetAUJobDetailsController($request);

	    // Retrieving details for the given month and parameters
	    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
	    $data = array();
	    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
	        //echo "SUCCESS: Get Account Updater Details for Month : " . $month  . "<br><br>";
	        if ($response->getAuDetails() == null) {
	            // echo "No Account Updater Details for this month.<br>";
	            //return ;
	        } else {
	            $details = new AnetAPI\ListOfAUDetailsType;
	            $details = $response->getAuDetails();
	            if (($details->getAuUpdate() == null) && ($details->getAuDelete() == null)) {
	                // echo "No Account Updater Details for this month.<br>";
	                //return ;
	            }
	        }

	        // Displaying the details of each response in the list
	        //echo "Total Num in Result Set : " . $response->getTotalNumInResultSet() . "<br><br>"; 
	        $data = array();
	        $details = new AnetAPI\ListOfAUDetailsType;
	        $details = $response->getAuDetails();
	        //echo "Updates:<br>";
	        foreach ($details->getAuUpdate() as $update) {
	        	$data_ = array();
	        	$data_['reason_code'] = $update->getAuReasonCode();
	        	$data_['reason_description'] = $update->getReasonDescription();
	        	$data_['full_name'] = $update->getFirstName() . ' ' . $update->getLastName();; 
	        	array_push($data, $data_);
	            // echo "		Profile ID / Payment Profile ID	: " . $update->getCustomerProfileID() . " / " . $update->getCustomerPaymentProfileID() . "<br>";
	            // echo "		Update Time (UTC) : " . $update->getUpdateTimeUTC() . "<br>";
	            // echo "		Reason Code	: " . $update->getAuReasonCode() . "<br>";
	            // echo "		Reason Description : " . $update->getReasonDescription() . "<br>";
	            // echo "		First Name : " . $update->getFirstName() . "<br>";
	            // echo "		Last Name : " . $update->getLastName() . "<br>";
	            // echo "<br>";
	        }
	        //echo "<br>Deletes:<br>";
	        foreach ($details->getAuDelete() as $delete) {
	        	$data_ = array();
	        	$data_['reason_code'] = $update->getAuReasonCode();
	        	$data_['reason_description'] = $update->getReasonDescription();
	        	$data_['full_name'] = $update->getFirstName() . ' ' . $update->getLastName();; 

	        	array_push($data, $data_);
	            // echo "		Profile ID / Payment Profile ID	: " . $delete->getCustomerProfileID() . " / " . $delete->getCustomerPaymentProfileID() . "<br>";
	            // echo "		Update Time (UTC) : " . $delete->getUpdateTimeUTC() . "<br>";
	            // echo "		Reason Code	: " . $delete->getAuReasonCode() . "<br>";
	            // echo "		Reason Description : " . $delete->getReasonDescription() . "<br>";
	            // echo "		First Name : " . $update->getFirstName() . "<br>";
	            // echo "		Last Name : " . $update->getLastName() . "<br>";
	            // echo "<br>";
	        }
	    } else {
	        //echo "ERROR :  Invalid response<br>";
	        $errorMessages = $response->getMessages()->getMessage();
	        
	        //echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "<br>";
	        $data['error'] = $errorMessages[0]->getText();
	    }

	    echo json_encode($data);
	}

}

/* End of file Account_updater.php */
/* Location: ./application/controllers/admin/authorize/Account_updater.php */