<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// date_default_timezone_set(timezone_name_from_abbr("EST"));
date_default_timezone_set('America/Los_Angeles');
// date_default_timezone_set('America/New_York');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');

// define("AUTHORIZENET_LOG_FILE", "phplog");

class Transactions extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_authorize_transactions');
		$this->load->model('Model_clients');
		$this->load->model('Model_client_joints');
	}

	public function index() {
		$data['userdata'] = $this->session->userdata('user_data');
		$this->load->view('admin/authorize/Transactions', $data);
	}

	function getTransactionList($batchId) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the request's refId
		$refId = 'ref' . time();

		//Setting a valid batch Id for the Merchant
		$batchId = $batchId;
		$request = new AnetAPI\GetTransactionListRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setBatchId($batchId);

		$controller = new AnetController\GetTransactionListController($request);

		//Retrieving transaction list for the given Batch Id
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			if ($response->getTransactions() == null) {
				echo "No Transaction to display in this Batch.";
				return;
			}
			//Displaying the details of each transaction in the list
			$all_transactions = [];
			foreach ($response->getTransactions() as $transaction) {
				$data_arr = [
					'batch_id' => $batchId,
					'transaction_id' => $transaction->getTransId(),
					'name' => $transaction->getFirstName() . ' ' . $transaction->getLastName(),
					'account_number' => $transaction->getAccountNumber(),
					'status' => $transaction->getTransactionStatus(),
					'amount' => number_format($transaction->getSettleAmount(), 2, '.', ''),
					'submitted_local' => date_format($transaction->getSubmitTimeLocal(), 'Y-m-d H:i:s'),
				];
				array_push($all_transactions, $data_arr);
			}

			return $all_transactions;
		} else {
			echo "ERROR :  Invalid response<br>";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
		}

		return $response;
	}

	function getAllTransactionsFromAuthorize() {
		// for ($i=1; $i <= 12; $i++) {
		$this->getSettledBatchList(date('m'), date('Y'));
		$this->getUnsettledTransactionList();
		// }
	}
	function getSettledBatchList($month, $year) {
		// a date constructed from an ISO8601 format date string
		// $month = '10';
		// $year = '2018';
		$firstSettlementDate = new DateTime("$year-$month-01T06:00:00Z");
		$last_day = new DateTime("$year-$month-01T06:00:00Z");
		$last_day->modify('last day of this month');
		$last_day = $last_day->format('d');
		// a date constructed manually
		$lastSettlementDate = new DateTime();
		$lastSettlementDate->setDate($year, $month, $last_day);
		$lastSettlementDate->setTime(13, 33, 59);
		$lastSettlementDate->setTimezone(new DateTimeZone('UTC'));

		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		$request = new AnetAPI\GetSettledBatchListRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setIncludeStatistics(true);

		// Both the first and last dates must be in the same time zone
		// The time between first and last dates, inclusively, cannot exceed 31 days.
		$request->setFirstSettlementDate($firstSettlementDate);
		$request->setLastSettlementDate($lastSettlementDate);

		$controller = new AnetController\GetSettledBatchListController($request);

		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			foreach ($response->getBatchList() as $batch) {
				$all_transactions = $this->getTransactionList($batch->getBatchId());
				// echo count($all_transactions) . '<br>';
				$this->saveAuthorizeTransactions($all_transactions);
			}
		} else {
			echo "ERROR :  Invalid response<br>";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
		}

		return $response;
	}

	function saveAuthorizeTransactions($all_transactions) {

		foreach ($all_transactions as $key => $transaction) {
			// if ($transaction['name'] == 'Ninete Vermeylen') {
				$authorize_transactions = new Model_authorize_transactions();
				$existing_data = $authorize_transactions->search(['transaction_id' => $transaction['transaction_id']]);

				$authorize_transactions = new Model_authorize_transactions();
				if (count($existing_data) > 0) {
					$existing_data = reset($existing_data);
					$existing_data = reset($existing_data);
					$authorize_transactions->load($existing_data);
				}
				foreach ($transaction as $key => $value) {
					$authorize_transactions->$key = $value;
				}

				$authorize_transactions->save();

			
				// $this->getAllDeclinedTransactions($transaction['name'], $transaction['account_number']);
			// }
			
			// if ($transaction['status'] == 'declined') {
			// 	$this->updateToNSF($transaction['name'], 'NSF');
			// 	// echo $transaction['name'];
			// } else {
			// 	$this->updateToNSF($transaction['name'], 'Active');
			// }

		}
	}

	function cron_send_authorize_alerts_today() {
		$data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local)=CURRENT_DATE ORDER BY submitted_local ASC,status DESC");
		foreach ($data as $key => $transaction) {
			$this->getAllDeclinedTransactions($transaction['name'], $transaction['account_number']);
		}
	}

	function testtest() {
		$this->updateToNSF('Tara Chinn','4599540756207647', 'NSF',2);
	}

	function getIDByBilling($name, $card_number) {
		$response = [];
		if ($card_number != '') {
			$data = $this->fetchRawData("SELECT id,name,type FROM view_client_cards WHERE name=\"$name\" AND RIGHT(card_number,4)=RIGHT('$card_number',4)");
			$response = $data;
		}

		return $response;
	}


	function getNamesByBilling($name, $card_number) {
		$response = [];
		if ($card_number != '') {
			$data = $this->fetchRawData("SELECT id FROM view_client_cards WHERE name=\"$name\" AND RIGHT(card_number,4)=RIGHT('$card_number',4) and type='client'");
			if (count($data) > 0) {
				$id = $data[0]['id']; 
				$response = $this->fetchRawData("SELECT name FROM view_client_cards WHERE id=$id and type='client'");
			} else {
				$data = $this->fetchRawData("SELECT id FROM view_client_cards WHERE name=\"$name\" AND RIGHT(card_number,4)=RIGHT('$card_number',4) and type='joint'");
				if (count($data) > 0) {
					$id = $data[0]['id']; 
					$response = $this->fetchRawData("SELECT name FROM view_client_cards WHERE id=$id and type='joint'");
				}
			}
		}

		return $response;
	}

	function testSendNSF() {
		$id = 78;
		$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1"); 
									if (count($get_enrollment) > 0) {
										$date_of_enrollment = reset($get_enrollment);
										$date_of_enrollment = reset($date_of_enrollment);
										if ($date_of_enrollment != '') {
											$month_now = date('m');
											$year_now = date('Y');
											$month_enrolled = date('m',strtotime($date_of_enrollment));
											$year_enrolled = date('Y',strtotime($date_of_enrollment)); 
											if ($year_now == $year_enrolled) {
												if ($month_enrolled == $month_now) {
													 $this->saveAlertTransaction(0,$id,'joint','Urgent Notice - Setup Fee Declined','Your current payment method has either incorrect cardholder name, incorrect billing address, or has insufficient funds. Your account will be hold until successful payment is made for setup services.');
												} elseif (($month_enrolled + 1) == $month_now) {
													 $this->saveAlertTransaction(0,$id,'joint','1st Late Notice','Please update your card information or contact your bank. Your account is on hold until successful payment is made for previous months services.');
												} 
											} 
										} 
									}
	}

	function updateToNSF($name, $account_number, $client_status) {
		// echo $account_number . '---' . $client_status . '<br>';
		// if ($name == 'Heather Meadows') {
			if ($account_number != '') {
				$data_ = $this->getIDByBilling($name, $account_number);
				$this->pprint($data_);
				if (count($data_) != 0) {
					// $this->pprint($data_);
					
					foreach ($data_ as $key => $value) {
						if ($value['type'] == 'client') {
							$id = $value['id'];
							$this->load->model('Model_clients');
							$clients = new Model_clients();
							$clients->load($id);
							// $this->pprint($clients);
							echo '<br>client status '.$client_status ;
							if ($client_status == 'Active') {
								$no_update = ['AV', 'AV2', 'AVSSN', 'SSN', 'Verifications', 'Cancelled', 'Archived', 'Terminated', 'Review'];
								if (!in_array($clients->client_status, $no_update)) {
									$clients->client_status = $client_status;
									$clients->save();
								}
							} else {
								$no_update = ['NSF', 'NSF2', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];

								if ($client_status == 'NSF2') {
									$no_update = [ 'NSF2', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'NSF3') {
									$no_update = [ 'NSF3', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'Suspended') {
									$no_update = [ 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'Terminated') {
									$no_update = [  'Archived', 'Terminated', 'Cancelled'];
								}
								// echo "no update ".in_array($clients->client_status, $no_update);
								if (!in_array($clients->client_status, $no_update)) {
									$clients->client_status = $client_status;
									$clients->save();
									if ($client_status == 'NSF') {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_enrollments WHERE client_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction($id,0,'client','Urgent Notice - Setup Fee Declined','Your current payment method has either incorrect cardholder name, incorrect billing address, or has insufficient funds. Your account will be hold until successful payment is made for setup services.');
													} else {
														$this->saveAlertTransaction($id,0,'client','1st Late Notice','Please update your card information or contact your bank. Your account is on hold until successful payment is made for previous months services.');
													} 
												} 
											} 
										}
										
									}
									if ($client_status == 'NSF2') {

										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_enrollments WHERE client_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if($month_enrolled == $month_now) {
														$this->saveAlertTransaction($id,0,'client','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
															$this->saveAlertTransaction($id,0,'client','2nd Late Notice','Please update your card information or contact your bank. Your account is on hold until successful payment is made for previous months services.');
														
														
													}
												} 
											} 
										}

										
									}
									if ($client_status == 'NSF3') {

										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_enrollments WHERE client_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if($month_enrolled == $month_now) {
														$this->saveAlertTransaction($id,0,'client','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction($id,0,'client','Final Late Notice','Please update your card information or contact your bank. Your account is suspended until successful payment is made for previous months services.');
														
														
													}
												} 
											} 
										}

										
									}


									if ($client_status == 'Suspended' ) {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_enrollments WHERE client_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction($id,0,'client','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction($id,0,'client','Final Late Notice','Please update your card information or contact your bank. Your account is suspended until successful payment is made for previous months services.');
													} 
												} 
											} 
										}
										
									}

									if ($client_status == 'Terminated' ) {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_enrollments WHERE client_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction($id,0,'client','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction($id,0,'client','Account Terminated','Effective immediately, your account is terminated as a result of non-payment of previous months services. All outstanding charges and/or late fees due will be processed until full successful payment is made.');
													} 
												} 
											} 
										}
										
									}

									
								}
							}
						} else {
							$id = $value['id'];
							$this->load->model('Model_client_joints');
							$client_joints = new Model_client_joints();
							$client_joints->load($id);
							if ($client_status == 'Active') {
								$no_update = ['AV', 'AV2', 'AVSSN', 'SSN', 'Verifications', 'Cancelled', 'Archived', 'Terminated', 'Review'];
								if (!in_array($client_joints->client_status, $no_update)) {
									$client_joints->client_status = $client_status;
									$client_joints->save();
								}
							} else {
								$no_update = ['NSF', 'NSF2', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								if ($client_status == 'NSF2') {
									$no_update = [ 'NSF2', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'NSF3') {
									$no_update = [ 'NSF3', 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'Suspended') {
									$no_update = [ 'Archived', 'Terminated', 'Suspended', 'Cancelled'];
								}
								if ($client_status == 'Terminated') {
									$no_update = [  'Archived', 'Terminated', 'Cancelled'];
								}
								if (!in_array($client_joints->client_status, $no_update)) {
									$client_joints->client_status = $client_status;
									$client_joints->save();
									if ($client_status == 'NSF') {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1"); 
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment)); 
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction(0,$id,'joint','Urgent Notice - Setup Fee Declined','Your current payment method has either incorrect cardholder name, incorrect billing address, or has insufficient funds. Your account will be hold until successful payment is made for setup services.');
													} else {
														$this->saveAlertTransaction(0,$id,'joint','1st Late Notice','Please update your card information or contact your bank. Your account is on hold until successful payment is made for previous months services.');
													} 
												} 
											} 
										}
									}
									if ($client_status == 'NSF2') {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1"); 
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment)); 
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction(0,$id,'joint','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction(0,$id,'joint','2nd Late Notice','Please update your card information or contact your bank. Your account is on hold until successful payment is made for previous months services.');
														
													}
												} 
											} 
										}
										
									}
									if ($client_status == 'NSF3') {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1"); 
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment)); 
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction(0,$id,'joint','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction(0,$id,'joint','Final Late Notice','Please update your card information or contact your bank. Your account is suspended until successful payment is made for previous months services.');
														
													}
												} 
											} 
										}
										
									}
									if ($client_status == 'Suspended' ) {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction(0,$id,'joint','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction(0,$id,'joint','Final Late Notice','Please update your card information or contact your bank. Your account is suspended until successful payment is made for previous months services.');
													} 
												} 
											} 
										}
										
									}

									if ($client_status == 'Terminated' ) {
										$get_enrollment = $this->fetchRawData("SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=$id LIMIT 1");
										if (count($get_enrollment) > 0) {
											$date_of_enrollment = reset($get_enrollment);
											$date_of_enrollment = reset($date_of_enrollment);
											if ($date_of_enrollment != '') {
												$month_now = date('m');
												$year_now = date('Y');
												$month_enrolled = date('m',strtotime($date_of_enrollment));
												$year_enrolled = date('Y',strtotime($date_of_enrollment));
												if ($year_now == $year_enrolled) {
													if ($month_enrolled == $month_now) {
														$this->saveAlertTransaction(0,$id,'joint','New Payment Information Required','Please complete a new Billing Authorization Form to update your card information. Your current payment method has either expired, been reported lost/stolen, or has insufficient funds.');
													} else {
														$this->saveAlertTransaction(0,$id,'joint','Account Terminated','Effective immediately, your account is terminated as a result of non-payment of previous months services. All outstanding charges and/or late fees due will be processed until full successful payment is made.');
													} 
												} 
											} 
										}
										
									}
								}
							}
						}
					}

				} else {
					echo "no data";
				}
			}
		// }
			

	}

	function saveAlertTransaction($client_id,$client_joint_id,$client_type,$alert_subject,$alert_notes) { 

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_client_alerts');
		$client_alerts = new Model_client_alerts();

		if ($client_type == 'joint') {
			$client_alerts->client_id = $client_joint_id;
		} else {
			$client_alerts->client_id = $client_id;
		}

		$client_alerts->client_type = $client_type;
		$client_alerts->sender = $sender;
		$client_alerts->sender_id = $sender_id;
		$client_alerts->sender_photo = $sender_photo;
		$client_alerts->sender_name = $sender_name;
		$client_alerts->alert_subject = $alert_subject;
		$client_alerts->alert_notes = $alert_notes;
		$client_alerts->alert_date = date('Y-m-d H:i:s');
		$client_alerts->save();

		if ($this->get_auto_alert(	$client_type == 'client' ? 'client_id' : 'client_joint_id', 
									$client_type == 'client' ? $client_id : $client_joint_id, 
									$client_type == 'client' ? 'clients' : 'client_joints') == 1) { 
			// $this->sendEmail('Client', 'Email', $client_type == 'client' ? $client_id : $client_joint_id, $alert_subject, $alert_notes, $client_alerts->alert_date, $client_type);
			$this->send_email_to_client($client_type == 'client' ? $client_id : $client_joint_id,$alert_subject,$alert_notes,$client_type);
			$this->sendEmail('Client', 'Phone', $client_type == 'client' ? $client_id : $client_joint_id, $alert_subject, $alert_notes, $client_alerts->alert_date, $client_type);
		} else {
			echo "not send";
		}

			
	}

	function get_auto_alert($pk,$id,$table) {
		$data = $this->fetchRawData("SELECT auto_alert FROM $table WHERE $pk=$id LIMIT 1");
		$data = reset($data);
		$data = reset($data);
		return $data;
	}

	function getAllDeclinedTransactions($name, $account_number) {
		// $account_number = 'Joshua Saubon';
		$data__ = $this->getNamesByBilling($name,$account_number);  
		if (count($data__) > 0) {
			$authorize_transactions = new Model_authorize_transactions();
			$names = array_column($data__, 'name');
			$names = implode("','", $names);
			
			$data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE name IN ('$names') GROUP BY transaction_id  ORDER BY submitted_local ASC,status DESC "); 
			$this->pprint($data);
			$declined = 0;
			$successful = 0;
			$last_transaction = '';
			foreach ($data as $key => $value) {

				if ($value['status'] == 'declined' || $value['status'] == 'generalError') { 
					$declined++;
				} else {
					$declined = 0;
					if ($value['status'] == 'settledSuccessfully' || $value['status'] == 'capturedPendingSettlement') {
						$successful++;
					}
				}
				if ($key == count($data) - 1) {
					$last_transaction = $value['status'];
				}
			}

			// echo $declined;

			if ($successful == 1) {
				$this->update_converted_client($name, $account_number);
				// echo $name.' Convert to client';
				// echo "<br>";
			}

			if ($last_transaction == 'declined' || $last_transaction == 'generalError') {
				if ($declined == 1) {
					$this->updateToNSF($name, $account_number, 'NSF');
				} else if ($declined == 2) {
					$this->updateToNSF($name, $account_number, 'NSF2');
				} else if ($declined == 3) {
					$this->updateToNSF($name, $account_number, 'NSF3');
				} else if ($declined == 4) {
					$this->updateToNSF($name, $account_number, 'Suspended');
				} else {
					$this->updateToNSF($name, $account_number, 'Terminated');
				}
			} else {
				$this->updateToNSF($name, $account_number, 'Active');
			}
		}
			
	}

	function testing_update() {
		$this->update_converted_client('Donny Shepherd', '4030150224127240');
	}

	function update_converted_client($name, $account_number) {
		$data_ = $this->getIDByBilling($name, $account_number);
		if (count($data_) > 0) {
			foreach ($data_ as $key => $value) {
				$type = $value['type']; 
				$id = $value['id'];


				$converted_date = date('Y-m-d H:i:s');
				if ($type == 'client') {
					$this->db->query("UPDATE clients SET converted=1,converted_date='$converted_date',client_status='Review' WHERE client_id=$id AND converted=0 ");
				} else {
					$client_id = $this->fetchRawData("SELECT client_id FROM client_joints WHERE client_joint_id=$id");
					$client_id = reset($client_id);
					$client_id = reset($client_id);
					$this->db->query("UPDATE clients SET converted=1,converted_date='$converted_date',client_status='Review' WHERE client_id=$client_id AND converted=0");
				}
			}
				
		}
	}

	function getTransactions() {

		$order = 'submitted_local';
		$by = 'DESC';

		$limit = 50;
		$offset = 0;
		$aColumns = [
			'transaction_id',
			'name',
			'account_number',
			'status',
			'amount',
			'submitted_local',
		];

		//sort
		if ($this->input->post('iSortCol_0') != null) {
			for ($i = 0; $i < $this->input->post('iSortingCols'); $i++) {
				if ($this->input->post('bSortable_' . $this->input->post('iSortCol_' . $i)) == true) {
					// $order_by = [ $aColumns[$this->input->post('iSortCol_'.$i)] => $this->input->post('sSortDir_'.$i) ];
					$order = $aColumns[$this->input->post('iSortCol_' . $i)];
					$by = $this->input->post('sSortDir_' . $i);
				}
			}
		}

		//limit
		if ($this->input->post('iDisplayLength') != null) {
			$limit = $this->input->post('iDisplayLength');
			$offset = 0;
		}

		//paginate
		if ($this->input->post('iDisplayStart') != 0) {
			// $limit = [$this->input->post('iDisplayLength') => $this->input->post('iDisplayStart')];
			$limit = $this->input->post('iDisplayLength');
			$offset = $this->input->post('iDisplayStart');
		}

		//search
		if ($this->input->post('sSearch') != '') {
			$search = $this->trim_str($this->input->post('sSearch'));
			$this->db->group_start();
			foreach ($aColumns as $key => $value) {
				$this->db->or_like($value, $search);
			}
			$this->db->group_end();

		}

		$authorize_transactions = new Model_authorize_transactions();
		$dataTable = $authorize_transactions->search([], $order, $by, $limit, $offset);
		$data['data'] = [];

		foreach ($dataTable as $key => $value) {
			$data['data'][] = [
				$value['transaction_id'],
				$value['name'],
				$value['account_number'],
				$value['status'],
				'$' . number_format($value['amount'], 2),
				date('m/d/Y h:i:s A', strtotime($value['submitted_local'])),
			];
		}

		echo json_encode($data);
	}

	function getUnsettledTransactionList() {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		$request = new AnetAPI\GetUnsettledTransactionListRequest();
		$request->setMerchantAuthentication($merchantAuthentication);

		$controller = new AnetController\GetUnsettledTransactionListController($request);

		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			if (null != $response->getTransactions()) {
				$all_transactions = [];
				foreach ($response->getTransactions() as $tx) {
					// echo "SUCCESS: TransactionID: " . $tx->getTransId() . "\n";
					$data_arr = [
						'transaction_id' => $tx->getTransId(),
						'name' => $tx->getFirstName() . ' ' . $tx->getLastName(),
						'account_number' => $tx->getAccountNumber(),
						'status' => $tx->getTransactionStatus(),
						'amount' => number_format($tx->getSettleAmount(), 2, '.', ''),
						'submitted_local' => date_format($tx->getSubmitTimeLocal(), 'Y-m-d H:i:s'),
					];
					array_push($all_transactions, $data_arr);
				}
				$this->saveAuthorizeTransactions($all_transactions);
				// $this->pprint($all_transactions);
			} else {
				echo "No unsettled transactions for the merchant." . "\n";
			}
		} else {
			echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
		}

		return $response;
	}


	function getAccountUpdaterJobDetails()
	{
	    /* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName(MERCHANT_LOGIN_ID);
	    $merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
	    
	    // Set the request's refId
	    $refId = 'ref' . time();

	    // Set a valid month (and other parameters) for the request
	    $cur_date = date('Y-m');
	    $month = $cur_date;
	    // $month = '2019-07';
	    $modifedTypeFilter = "all";
	    $paging = new AnetAPI\PagingType;
	    $paging->setLimit("1000");
	    $paging->setOffset("1");

	    // Build tbe request object
	    $request = new AnetAPI\GetAUJobDetailsRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setMonth($month);
	    $request->setModifiedTypeFilter($modifedTypeFilter);
	    $request->setPaging($paging);

	    $controller = new AnetController\GetAUJobDetailsController($request);

	    // Retrieving details for the given month and parameters
	    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
	    
	    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
	        echo "SUCCESS: Get Account Updater Details for Month : " . $month  . "<br><br>";
	        if ($response->getAuDetails() == null) {
	            echo "No Account Updater Details for this month.<br>";
	            return ;
	        } else {
	            $details = new AnetAPI\ListOfAUDetailsType;
	            $details = $response->getAuDetails();
	            if (($details->getAuUpdate() == null) && ($details->getAuDelete() == null)) {
	                echo "No Account Updater Details for this month.<br>";
	                return ;
	            }
	        }

	        // Displaying the details of each response in the list
	        echo "Total Num in Result Set : " . $response->getTotalNumInResultSet() . "<br><br>";
	        $details = new AnetAPI\ListOfAUDetailsType;
	        $details = $response->getAuDetails();
	        echo "Updates:<br>";
	        foreach ($details->getAuUpdate() as $update) {
	            echo "		Profile ID / Payment Profile ID	: " . $update->getCustomerProfileID() . " / " . $update->getCustomerPaymentProfileID() . "<br>";
	            echo "		Update Time (UTC) : " . $update->getUpdateTimeUTC() . "<br>";
	            echo "		Reason Code	: " . $update->getAuReasonCode() . "<br>";
	            echo "		Reason Description : " . $update->getReasonDescription() . "<br>";
	            echo "		First Name : " . $update->getFirstName() . "<br>";
	            echo "		Last Name : " . $update->getLastName() . "<br>";
	            echo "<br>";
	        }
	        echo "<br>Deletes:<br>";
	        foreach ($details->getAuDelete() as $delete) {
	            echo "		Profile ID / Payment Profile ID	: " . $delete->getCustomerProfileID() . " / " . $delete->getCustomerPaymentProfileID() . "<br>";
	            echo "		Update Time (UTC) : " . $delete->getUpdateTimeUTC() . "<br>";
	            echo "		Reason Code	: " . $delete->getAuReasonCode() . "<br>";
	            echo "		Reason Description : " . $delete->getReasonDescription() . "<br>";
	            echo "		First Name : " . $update->getFirstName() . "<br>";
	            echo "		Last Name : " . $update->getLastName() . "<br>";
	            echo "<br>";
	        }
	    } else {
	        echo "ERROR :  Invalid response<br>";
	        $errorMessages = $response->getMessages()->getMessage();
	        echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "<br>";
	    }

	    return $response;
	}


	function voidTransaction()
	{
		$transactionid = $this->input->post('transaction_id');
	    /* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName(MERCHANT_LOGIN_ID);
	    $merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
	    
	    // Set the transaction's refId
	    $refId = 'ref' . time();

	    //create a transaction
	    $transactionRequestType = new AnetAPI\TransactionRequestType();
	    $transactionRequestType->setTransactionType( "voidTransaction"); 
	    $transactionRequestType->setRefTransId($transactionid);

	    $request = new AnetAPI\CreateTransactionRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
		  $request->setRefId($refId);
	    $request->setTransactionRequest( $transactionRequestType);
	    $controller = new AnetController\CreateTransactionController($request);
	    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
	    
	    if ($response != null)
	    {
	      	if($response->getMessages()->getResultCode() == "Ok")
	      	{
	        	$tresponse = $response->getTransactionResponse();
	        
		      	if ($tresponse != null && $tresponse->getMessages() != null)   
		        {
		          	echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
		          	echo " Void transaction SUCCESS AUTH CODE: " . $tresponse->getAuthCode() . "\n";
		          	echo " Void transaction SUCCESS TRANS ID  : " . $tresponse->getTransId() . "\n";
		          	echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
			        echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
		        }
	        	else
		        {
		          	echo "Transaction Failed \n";
		          	if($tresponse->getErrors() != null)
		          	{
		            	echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
		            	echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";            
		          	}
		        }
	      	}
	      	else
	      	{
	        	echo "Transaction Failed \n";
	        	$tresponse = $response->getTransactionResponse();
	        	if($tresponse != null && $tresponse->getErrors() != null)
	        	{
		          echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
		          echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";                      
	        	}
		        else
		        {
		          	echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
		          	echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
		        }
	      	}      
	    }
	    else
	    {
	      	echo  "No response returned \n";
	    }

	    return $response;
  	}

  	function testingVoid() {
  		$this->voidTransaction('41486784307');
  	}

  	function refundTransaction()
	{
		$refTransId = $this->input->post('transaction_id');
		$amount = $this->input->post('amount');

		// $refTransId = '41565553928';
		// $amount = 0.04;
	    /* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName(MERCHANT_LOGIN_ID);
	    $merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
	    
	    // Set the transaction's refId
	    $refId = 'ref' . time();

	    // Create the payment data for a credit card
	    $creditCard = new AnetAPI\CreditCardType();
	    $creditCard->setCardNumber("0015");
	    $creditCard->setExpirationDate("XXXX");
	    $paymentOne = new AnetAPI\PaymentType();
	    $paymentOne->setCreditCard($creditCard);
	    //create a transaction
	    $transactionRequest = new AnetAPI\TransactionRequestType();
	    $transactionRequest->setTransactionType( "refundTransaction"); 
	    $transactionRequest->setAmount($amount);
	    $transactionRequest->setPayment($paymentOne);
	    $transactionRequest->setRefTransId($refTransId);
	 

	    $request = new AnetAPI\CreateTransactionRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setRefId($refId);
	    $request->setTransactionRequest( $transactionRequest);
	    $controller = new AnetController\CreateTransactionController($request);
	    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

	    if ($response != null)
	    {
	      if($response->getMessages()->getResultCode() == "Ok")
	      {
	        $tresponse = $response->getTransactionResponse();
	        
		      if ($tresponse != null && $tresponse->getMessages() != null)   
	        {
	          echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
	          echo "Refund SUCCESS: " . $tresponse->getTransId() . "\n";
	          echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
		        echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
	        }
	        else
	        {
	          echo "Transaction Failed \n";
	          if($tresponse->getErrors() != null)
	          {
	            echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
	            echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";            
	          }
	        }
	      }
	      else
	      {
	        echo "Transaction Failed \n";
	        $tresponse = $response->getTransactionResponse();
	        if($tresponse != null && $tresponse->getErrors() != null)
	        {
	          echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
	          echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";                      
	        }
	        else
	        {
	          echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
	          echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
	        }
	      }      
	    }
	    else
	    {
	      echo  "No response returned \n";
	    }

	    return $response;
	  }

}

?>