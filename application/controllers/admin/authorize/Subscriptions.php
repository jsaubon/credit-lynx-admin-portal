<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// date_default_timezone_set(timezone_name_from_abbr("EST"));
date_default_timezone_set('America/Los_Angeles');
// date_default_timezone_set('America/New_York');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');

define("AUTHORIZENET_LOG_FILE", "phplog");

class Subscriptions extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Model_authorize_subscriptions');
	}
	



	function getCountOfSubscription($searchType) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();
		// $sorting = new AnetAPI\ARBGetSubscriptionListSortingType();
		// $sorting->setOrderBy("id");
		// $sorting->setOrderDescending(false);
		// $paging = new AnetAPI\PagingType();
		// $paging->setLimit("1000");
		// $paging->setOffset("1");
		$request = new AnetAPI\ARBGetSubscriptionListRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSearchType($searchType);
		// $request->setSorting($sorting);
		// $request->setPaging($paging);
		$controller = new AnetController\ARBGetSubscriptionListController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		$arr_response = array();
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			return count($response->getSubscriptionDetails());
		} else {
			echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
		}
		return $response;
	}

	function getTableBySearchType() {
		$searchType = $this->input->post('searchType');
		$this->getListOfSubscriptions($searchType);
	}

	function getListOfSubscriptions($searchType) {
		$authorize_subscriptions = new Model_authorize_subscriptions();
		$data = $authorize_subscriptions->search(['status' => $searchType], 0, 0, 'subscription_id', 'DESC');
		echo json_encode($data);
	}

	function getSubscriptionById($subscriptionId) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Creating the API Request with required parameters
		$request = new AnetAPI\ARBGetSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscriptionId($subscriptionId);
		$request->setIncludeTransactions(true);

		// Controller
		$controller = new AnetController\ARBGetSubscriptionController($request);

		// Getting the response
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			if ($response->getMessages()->getResultCode() == "Ok") {
				if (count($response->getSubscription()->getArbTransactions()) > 0) {
					$last_payment_date = ((array) $response->getSubscription()->getArbTransactions()[0]->getSubmitTimeUTC())['date'];
				} else {
					$last_payment_date = 'no record';
				}
				$new_arr = array(
					'id' => $subscriptionId,
					"name" => $response->getSubscription()->getName(),
					"status" => $response->getSubscription()->getStatus(),
					"monthly_due" => (int) date('d', strtotime(((array) $response->getSubscription()->getPaymentSchedule()->getStartDate())['date'])),
					"amount" => $response->getSubscription()->getAmount(),
					'startDate' => (array) $response->getSubscription()->getPaymentSchedule()->getStartDate(),
					'total_occurrences' => $response->getSubscription()->getPaymentSchedule()->getTotalOccurrences(),
					'completed_payments' => count($response->getSubscription()->getArbTransactions()),
					'cardNumber' => $response->getSubscription()->getProfile()->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber(),
					'expirationDate' => $response->getSubscription()->getProfile()->getPaymentProfile()->getPayment()->getCreditCard()->getExpirationDate(),
					'firstName' => $response->getSubscription()->getProfile()->getPaymentProfile()->getBillTo()->getFirstName(),
					'lastName' => $response->getSubscription()->getProfile()->getPaymentProfile()->getBillTo()->getLastName(),
					'last_payment_date' => $last_payment_date,
				);
				return $new_arr;

			} else {
				// Error
				echo "ERROR :  Invalid response\n";
				$errorMessages = $response->getMessages()->getMessage();
				echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
			}
		} else {
			// Failed to get response
			echo "Null Response Error";
		}

	}

	function getSubscriptionBySId() {
		$subscriptionId = $this->input->post('subscription_id');
		$data = array($this->getSubscriptionById($subscriptionId));
		echo json_encode($data);
	}

	function updateSubscription() {
		$subscriptionId = $this->input->post('subscriptionId');
		$monthlyFee = $this->input->post('monthlyFee');
		$cardNumber = $this->input->post('cardNumber');
		$expirationDate = $this->input->post('expirationDate');
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();
		$subscription = new AnetAPI\ARBSubscriptionType();
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($cardNumber);
		$creditCard->setExpirationDate($expirationDate);
		$payment = new AnetAPI\PaymentType();
		$payment->setCreditCard($creditCard);
		$subscription->setPayment($payment);
		$subscription->setAmount($monthlyFee);

		$request = new AnetAPI\ARBUpdateSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscriptionId($subscriptionId);
		$request->setSubscription($subscription);
		$controller = new AnetController\ARBUpdateSubscriptionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			$errorMessages = $response->getMessages()->getMessage();
			echo "SUCCESS Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";

		} else {
			echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
		}
		return $response;
	}

	function cancelSubscription() {
		$subscriptionId = $this->input->post('subscriptionId');
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();
		$request = new AnetAPI\ARBCancelSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscriptionId($subscriptionId);
		$controller = new AnetController\ARBCancelSubscriptionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			$successMessages = $response->getMessages()->getMessage();
			echo "SUCCESS : " . $successMessages[0]->getCode() . "  " . $successMessages[0]->getText() . "\n";

		} else {
			echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";

		}
		return $response;
	}

	function getSubscriptionDates($subscriptionId) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Creating the API Request with required parameters
		$request = new AnetAPI\ARBGetSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscriptionId($subscriptionId);
		$request->setIncludeTransactions(true);

		// Controller
		$controller = new AnetController\ARBGetSubscriptionController($request);

		// Getting the response
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			if ($response->getMessages()->getResultCode() == "Ok") {
				$data['payment_schedule'] = (((array) $response->getSubscription()->getPaymentSchedule()->getStartDate())['date']);
				// $this->pprint($new_arr);
				if (count($response->getSubscription()->getArbTransactions()) > 0) {
					$data['last_payment_date'] = (((array) $response->getSubscription()->getArbTransactions()[0]->getSubmitTimeUTC())['date']);
				} else {
					$data['last_payment_date'] = 'no record';
				}
				return $data;

			} else {
				// Error
				echo "ERROR :  Invalid response\n";
				$errorMessages = $response->getMessages()->getMessage();
				echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
			}
		} else {
			// Failed to get response
			echo "Null Response Error";
		}

	}

	function getListOfSubscriptionsCron() {
		$this->getListOfSubscriptionsByType('subscriptionInactive',1); 
		$this->getListOfSubscriptionsByType('subscriptionActive');
	}

	function getListOfSubscriptionsByType($searchType,$offset = 1) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();
		$sorting = new AnetAPI\ARBGetSubscriptionListSortingType();
		$sorting->setOrderBy("id");
		$sorting->setOrderDescending(true);
		$paging = new AnetAPI\PagingType();
		$paging->setLimit(30);
		$paging->setOffset($offset);
		$request = new AnetAPI\ARBGetSubscriptionListRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSearchType($searchType);
		$request->setSorting($sorting);
		$request->setPaging($paging);
		$controller = new AnetController\ARBGetSubscriptionListController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		$arr_response = array();
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			foreach ($response->getSubscriptionDetails() as $subscriptionDetails) {
				$subscription_dates = $this->getSubscriptionDates($subscriptionDetails->getId());

				$raw_date_created = (array) $subscriptionDetails->getCreateTimeStampUTC();
				$payment_schedule = $subscription_dates['payment_schedule'];
				$date_created = $this->getEST($raw_date_created['date']);

				$last_payment_date = $subscription_dates['last_payment_date'] == 'no record' ? 'no record' : $this->getEST($subscription_dates['last_payment_date']);
				$new_arr = array(
					"subscription_id" => $subscriptionDetails->getId(),
					"name" => $subscriptionDetails->getName(),
					"status" => $subscriptionDetails->getStatus(),
					"date_created" => $date_created,
					"full_name" => $subscriptionDetails->getFirstName() . ' ' . $subscriptionDetails->getLastName(),
					"total_occurrences" => $subscriptionDetails->getTotalOccurrences(),
					"monthly_due" => $payment_schedule,
					"past_occurrences" => $subscriptionDetails->getPastOccurrences(),
					"payment_method" => $subscriptionDetails->getPaymentMethod(),
					"account_number" => $subscriptionDetails->getAccountNumber(),
					"invoice" => $subscriptionDetails->getInvoice(),
					"amount" => number_format($subscriptionDetails->getAmount(), 2),
					"currency_code" => $subscriptionDetails->getCurrencyCode(),
					'last_payment_date' => $last_payment_date,
				);
				array_push($arr_response, $new_arr);
			}
			$this->saveAuthorizeSubscriptions($arr_response);
			// $this->pprint($arr_response);
		} else {
			echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
		}
		return $response;
	}

	function getEST($utc) {
		$from = 'UTC';
		$to = 'EST';
		$format = 'Y-m-d H:i:s';
		$date = date($utc); // UTC time
		date_default_timezone_set($from);
		$newDatetime = strtotime($date);
		date_default_timezone_set($to);
		$newDatetime = date($format, $newDatetime);
		return $newDatetime;
	}

	function saveAuthorizeSubscriptions($data) {
		$authorize_subscriptions = new Model_authorize_subscriptions();
		foreach ($data as $key => $subscription) {
			$data_exist = $authorize_subscriptions->search(['subscription_id' => $subscription['subscription_id']]);
			$authorize_subscriptions = new Model_authorize_subscriptions();
			if (count($data_exist) > 0) {
				$data_exist = reset($data_exist);
				$data_exist = reset($data_exist);
				$authorize_subscriptions->load($data_exist);
			}
			foreach ($subscription as $field => $value) {
				$authorize_subscriptions->$field = $value;
			}
			$authorize_subscriptions->save();

			$account_number = $subscription['account_number'];
			$name = $subscription['full_name'];
			$status = $subscription['status'];
			// echo $subscription['subscription_id'];
			// echo "<br>";
			// echo $subscription['status'];
			// echo "<br>";
			// if ($status == 'suspended') {
			// 	$this->updateToNSF($name, $account_number, 'NSF');
			// } else if ($status == 'canceled') {
			// 	$this->updateToNSF($name, $account_number, 'Cancelled');
			// } else {
			// 	$this->updateToNSF($name, $account_number, 'Active');
			// }
		}

	}

	function getIDByBilling($name, $card_number) {
		$response = [];
		if ($card_number != '') {
			$data = $this->fetchRawData("SELECT client_id FROM client_billings WHERE card_holder=\"$name\" AND RIGHT(card_number,4)=RIGHT('$card_number',4) ORDER BY client_id DESC");
			if (count($data) > 0) {
				$response['data'] = $data;
				$response['client_type'] = 'client';
			} else {
				$data = $this->fetchRawData("SELECT client_joint_id FROM client_joint_billings WHERE card_holder=\"$name\" AND RIGHT(card_number,4)=RIGHT('$card_number',4) ORDER BY client_joint_id DESC");
				if (count($data) > 0) {
					$response['data'] = $data;
					$response['client_type'] = 'client_joint';
				}
			}
		}

		return $response;
	}

	function updateToNSF($name, $account_number, $client_status) {
		// echo $account_number . '---' . $client_status . '<br>';
		if ($account_number != '') {
			$data_ = $this->getIDByBilling($name, $account_number);
			if (count($data_) != 0) {
				// $this->pprint($data_);
				$client_type = $data_['client_type'];
				foreach ($data_['data'] as $key => $value) {
					if ($client_type == 'client') {
						$id = $value['client_id'];
						$this->load->model('Model_clients');
						$clients = new Model_clients();
						$clients->load($id);
						if ($client_status == 'Active') {
							$no_update = ['AV', 'AV2', 'AVSSN', 'SSN', 'Verifications', 'Cancelled', 'Archived', 'Terminated', 'Review', 'Suspended'];
							if (!in_array($clients->client_status, $no_update)) {
								$clients->client_status = $client_status;
								$clients->save();
							}
						} else {
							if ($id != 1005) {
								$no_update = ['NSF', 'NSF2', 'Archived', 'Terminated', 'Review', 'Suspended', 'Cancelled'];
								if (!in_array($clients->client_status, $no_update)) {
									$clients->client_status = $client_status;
									$clients->save();
								}
							}
						}
					} else {
						$id = $value['client_joint_id'];
						$this->load->model('Model_client_joints');
						$client_joints = new Model_client_joints();
						$client_joints->load($id);
						if ($client_status == 'Active') {
							$no_update = ['AV', 'AV2', 'AVSSN', 'SSN', 'Verifications', 'Cancelled', 'Archived', 'Terminated', 'Review', 'Suspended'];
							if (!in_array($client_joints->client_status, $no_update)) {
								$client_joints->client_status = $client_status;
								$client_joints->save();
							}
						} else {
							$no_update = ['NSF', 'NSF2', 'Archived', 'Terminated', 'Review', 'Suspended', 'Cancelled'];
							if (!in_array($client_joints->client_status, $no_update)) {
								$client_joints->client_status = $client_status;
								$client_joints->save();
							}
						}
					}
				}

			}
		}

	}

	function createSubscription() {
		$plan = $this->input->post('plan');
		$monthly_due = $this->input->post('monthly_due');
		$monthly_fee = $this->input->post('monthly_fee');
		$card_info = $this->input->post('card_info');
		$customer_address = $this->input->post('customer_address');
		$shipping_address = $this->input->post('shipping_address');
		$monthlyFeeDate = new DateTime($monthly_due, new DateTimeZone('EST'));

		$monthlyFeeDate->setTimezone(new DateTimeZone('UTC'));
		// $this->pprint($this->input->post());
		$this->createAuthorizeSubscription($plan, $monthly_fee, $monthlyFeeDate, $card_info, $customer_address, $shipping_address);
	}

	function createAuthorizeSubscription($plan, $monthly_fee, $monthlyFeeDate, $card_info, $customer_address, $shipping_address) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Subscription Type Info
		$subscription = new AnetAPI\ARBSubscriptionType();
		$subscription->setName($plan . " Monthly Fee ($" . $monthly_fee . ")");

		$interval = new AnetAPI\PaymentScheduleType\IntervalAType();
		$interval->setLength(1);
		$interval->setUnit("months");

		$paymentSchedule = new AnetAPI\PaymentScheduleType();
		$paymentSchedule->setInterval($interval);
		$paymentSchedule->setStartDate($monthlyFeeDate);
		$paymentSchedule->setTotalOccurrences("9999");
		$paymentSchedule->setTrialOccurrences("0");

		$subscription->setPaymentSchedule($paymentSchedule);
		$subscription->setAmount($monthly_fee);
		$subscription->setTrialAmount("0.00");

		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);

		$payment = new AnetAPI\PaymentType();
		$payment->setCreditCard($creditCard);
		$subscription->setPayment($payment);

		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . " Monthly Fee For Previous Month Services ($" . $monthly_fee . ")");
		$subscription->setOrder($order);

		$billTo = new AnetAPI\NameAndAddressType();
		$billTo->setFirstName($shipping_address['first_name']);
		$billTo->setLastName($shipping_address['last_name']);

		$subscription->setBillTo($billTo);

		$request = new AnetAPI\ARBCreateSubscriptionRequest();
		$request->setmerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscription($subscription);
		$controller = new AnetController\ARBCreateSubscriptionController($request);

		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

			// $this->getListOfSubscriptionsCron();
			echo "success_" . $response->getSubscriptionId();
		} else {
			echo "error_";
			echo "ERROR :  Invalid response<br>";
			$errorMessages = $response->getMessages()->getMessage();
			echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
		}

		return $response;
	}

	public function getClientAuthorizeSubscriptions() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');

		$cards = $this->get_client_cards($id,$type); 


		if (count($cards) != 0) { 
			$card_holder = array_filter(array_column($cards, 'name'));
			$card_holder = implode('|', $card_holder);

			$card_holder = strtolower($card_holder);


			$card_number = array_filter(array_column($cards, 'card_number'));
			foreach ($card_number as $key => $value) {
				$card_number[$key] = substr($value, -4);
			}
			$card_number = implode(',', $card_number);
 
  
			if ($card_number != '') {  
				$data['transactions'] = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE LCASE(name) REGEXP '$card_holder' AND RIGHT(account_number,4) IN ($card_number) GROUP BY transaction_id ORDER BY submitted_local DESC");
				$data['subscriptions'] = $this->fetchRawData("SELECT * FROM subscription_list WHERE id=$id and type='$type' ORDER BY date_changed DESC,subs_id DESC");;
			} else { 
				$data['transactions'] = [];
				$data['subscriptions'] = [];
				
			}
		} else { 
			$data['transactions'] = [];
			$data['subscriptions'] = [];
		}

		echo json_encode($data);
			
	}

	function chargeCreditCard() {
		$email = $this->input->post('email');
		$plan = $this->input->post('plan');
		$setup_fee = $this->input->post('setup_fee');
		$card_info = $this->input->post('card_info');
		$customer_address = $this->input->post('customer_address');
		$shipping_address = $this->input->post('shipping_address');
		// $this->pprint($this->input->post());
		$this->chargeAuthorizeCreditCard($email, $plan, $setup_fee, $card_info, $customer_address, $shipping_address);
	}
	function chargeCreditCardMonthly() {
		$email = $this->input->post('email');
		$plan = $this->input->post('plan');
		$monthly_fee = $this->input->post('monthly_fee');
		$card_info = $this->input->post('card_info');
		$customer_address = $this->input->post('customer_address');
		$shipping_address = $this->input->post('shipping_address');
		// $this->pprint($this->input->post());
		$this->chargeAuthorizeCreditCardMonthly($email, $plan, $monthly_fee, $card_info, $customer_address, $shipping_address);
	}
	function chargeAuthorizeCreditCard($email, $plan, $setup_fee, $card_info, $customer_address, $shipping_address) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Plan Setup Fee ($' . $setup_fee . ')');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		// $customerAddress->setCompany($shipping_address['company']);
		// $customerAddress->setAddress($shipping_address['address']);
		// $customerAddress->setCity($shipping_address['city']);
		// $customerAddress->setState($shipping_address['state']);
		// $customerAddress->setZip($shipping_address['zip']);
		// $customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($setup_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					echo "success_" . $tresponse->getTransId();
					// echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
				} else {
					echo "error_";
					echo "Transaction Failed <br>";
					if ($tresponse->getErrors() != null) {
						echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
						echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				echo "error_";
				echo "Transaction Failed <br>";
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getErrors() != null) {
					echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				} else {
					echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
					echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				}
			}
		} else {
			echo "No response returned <br>";
		}

		return $response;
	}
	function chargeAuthorizeCreditCardMonthly($email, $plan, $monthly_fee, $card_info, $customer_address, $shipping_address) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Monthly Fee For Overdue Services');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		// $customerAddress->setCompany($shipping_address['company']);
		// $customerAddress->setAddress($shipping_address['address']);
		// $customerAddress->setCity($shipping_address['city']);
		// $customerAddress->setState($shipping_address['state']);
		// $customerAddress->setZip($shipping_address['zip']);
		// $customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($monthly_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					echo "success_" . $tresponse->getTransId();
					// echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
				} else {
					echo "error_";
					echo "Transaction Failed <br>";
					if ($tresponse->getErrors() != null) {
						echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
						echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				echo "error_";
				echo "Transaction Failed <br>";
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getErrors() != null) {
					echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				} else {
					echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
					echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				}
			}
		} else {
			echo "No response returned <br>";
		}

		return $response;
	}

	function get_client_cards($id,$type) {
		$data = $this->fetchRawData("SELECT * FROM view_client_cards WHERE id=$id AND type='$type'");
		return $data;
	}

}
