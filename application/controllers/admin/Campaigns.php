<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checklog();  
	}
	public function index() { 
		$data['userdata'] = $this->session->userdata('user_data');
	 	$data['page_controller'] = 'Campaigns';
	 	$data['client_statuses'] = $this->get_client_statuses();
	 	$data['lead_statuses'] = $this->get_lead_statuses();
		$this->load->view('admin/Campaigns.php', $data);

	} 

	function get_lead_statuses() {
		$data = $this->fetchRawData("SELECT * FROM lead_statuses ORDER BY ls_id ASC");
		return $data;
	}
	function get_client_statuses() {
		$data = $this->fetchRawData("SELECT * FROM client_statuses ORDER BY client_status_id ASC");
		return $data;
	}
 
 
}
 