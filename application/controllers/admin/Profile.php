<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller { 
	
	public function index()
	{
		$this->checklog(); 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers(); 
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Profile';
		$data['page_title_s'] = 'Profile';
		$data['page_controller'] = 'Profile';  
		$data['page_folder'] = 'admins'; 
		$this->load->view('admin/Profile.php',$data); 
	} 

	function getUsers() {
		$data = $this->fetchRawData("SELECT * FROM admins ORDER BY admin_id DESC");
		return $data;
	}

	function getUserCount() {
		$data = $this->fetchRawData("SELECT count(admin_id) as `user_count` FROM admins");
		return $data[0]['user_count'];
	}

	function uploadProfilePicture() {
		$admin_id = $this->input->post('admin_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/admins/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $data = $this->db->query("UPDATE admins SET photo='$photo' WHERE admin_id=$admin_id");
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$admin_id = $this->input->post('admin_id');
		$name = $this->input->post('name'); 
		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		 
    	$photo = $this->input->post('current_photo'); 

	    $this->load->model('Model_admins');
	    $admins = new Model_admins();
	    if ($admin_id != '') {
	    	$admins->admin_id = $admin_id;
	    }
	    $admins->photo = $photo;
	    $admins->name = $name; 
		$admins->username = $username;
		$admins->password = $password;
	    $admins->save();

	    redirect(base_url('admin/profile'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$data = $this->fetchRawData("SELECT * FROM admins WHERE username='$username'");
		echo count($data); 
	}


	function getDetails() {
		$admin_id = $this->input->post('admin_id');
		// $admin_id = '12';
		$userdata = $this->session->userdata('user_data');
		if ($userdata['login_folder'] == 'admins') {
			$data = $this->fetchRawData("SELECT * FROM admins WHERE admin_id = $admin_id");
		} elseif ($this->uri->segment(1) == 'employees') {
			$data = $this->fetchRawData("SELECT * FROM employees WHERE employee_id = $admin_id");
		}
		echo json_encode($data);
	}

	function saveTask() {
		$admin_id = $this->input->post('admin_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Administrators';
		$tasks->task_assigned_id = $admin_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$admin_id = $this->input->post('admin_id');
		$data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$admin_id AND task_assigned='Administrators' AND task_active <> 2 ORDER BY task_id DESC");
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveNote() {
		$admin_id = $this->input->post('admin_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Administrators';
		$notes->note_assigned_id = $admin_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$admin_id = $this->input->post('admin_id');
		$data = $this->fetchRawData("SELECT * FROM notes WHERE note_assigned_id=$admin_id AND note_assigned='Administrators' ORDER BY note_id DESC");
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$data = $this->db->query("UPDATE notes SET note_sticky=$note_sticky WHERE note_id=$note_id");
		echo "updated";
	}
	
}
