<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrators extends MY_Controller { 
	
	public function __construct()
  	{
		parent::__construct(); 
		$this->load->model('Model_admins');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
	    $this->checkLog();
  	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers(); 
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Administrators';
		$data['page_title_s'] = 'Administrator';
		$data['page_controller'] = 'administrators';  
		$data['page_folder'] = 'admins'; 
		$this->load->view('admin/Administrators.php',$data); 
	}   
	
	 

	function getUsers() {  
		$admins = new Model_admins(); 
		$data = $admins->search([],'admin_id','DESC');
		// $data = $this->fetchArray($data);
		return $data;
	}

	function getUserCount() {
		$admins = new Model_admins(); 
		$data = $admins->get();
		return count($data);
	}

	function uploadProfilePicture() {
		$admin_id = $this->input->post('admin_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/admins/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $admins = new Model_admins();
	    $admins->load($admin_id);
	    $admins->photo = $photo;
	    $admins->save();
	    // $data = $this->db->query("UPDATE admins SET photo='$photo' WHERE admin_id=$admin_id");
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$admin_id = $this->input->post('admin_id');
		$name = $this->input->post('name'); 
		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/admins/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }
 
	    $admins = new Model_admins();
	    if ($admin_id != '') {
	    	$admins->admin_id = $admin_id;
	    }
	    $admins->photo = $photo;
	    $admins->name = $name; 
		$admins->username = $username;
		$admins->password = $password;
	    $admins->save();

	    redirect(base_url('admin/administrators'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$admins = new Model_admins();
		$data = $admins->search(['username'=>$username]);
		// $data = $this->fetchArray($data); 
		echo count($data); 
	}


	function getDetails() {
		$admin_id = $this->input->post('admin_id');
		$admins = new Model_admins(); 
		$data = $admins->search(['admin_id'=>$admin_id]);
		// $data = $this->fetchArray($data);

		// $data = $this->fetchRawData("SELECT * FROM admins WHERE admin_id = $admin_id");
		echo json_encode(end($data));
	}
 
	function saveTask() {
		$admin_id = $this->input->post('admin_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Administrators';
		$tasks->task_assigned_id = $admin_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$admin_id = $this->input->post('admin_id'); 
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$admin_id,
										'task_assigned'=>'Administrators',
										'task_active <>'=>'  2'
									],'task_id','DESC'); 
		// $data = $this->fetchArray($data);  
		echo json_encode($data);
	}
 

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');

		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save();
 
		echo "updated";
	}

	function saveNote() {
		$admin_id = $this->input->post('admin_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_name = $userdata['name'];
		$sender_photo = $userdata['photo'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Administrators';
		$notes->note_assigned_id = $admin_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$admin_id = $this->input->post('admin_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$admin_id,
								'note_assigned'=>'Administrators',
							],'note_id','DESC');
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky; 
		$notes->save();
		echo "updated";
	}
	
}
