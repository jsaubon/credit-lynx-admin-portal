<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends MY_Controller { 
	public function __construct() {
		parent::__construct(); 
		$this->load->model('Model_clients');
		$this->load->model('Model_client_joints');
		$this->load->model('Model_client_documents');
		$this->load->model('Model_client_joint_documents');
	}
	public function index()
	{
		$this->checklog(); 
		$data['userdata'] = $this->session->userdata('user_data');

		$data['pageTable'] = $this->getPageTable();
		$data['last4'] = $this->getLast4Uploads();
 
		$this->load->view('admin/Documents.php',$data); 
	} 

	function getLast4Uploads() {
		$client_documents = new Model_client_documents();
		$client_joint_documents = new Model_client_joint_documents();
		$client_documents->selects = ['*',
										'CONCAT( client_id, "_client" ) AS `d_id`',
										'( SELECT NAME FROM clients WHERE clients.client_id = client_documents.client_id ) AS `folder_name`'];
		$data_cd = $client_documents->searchWithSelect([],'date_uploaded','DESC');
		$client_joint_documents->selects = ['*',
										'CONCAT( client_joint_id, "_client" ) AS `d_id`',
										'( SELECT NAME FROM client_joints WHERE client_joints.client_id = client_joint_documents.client_joint_id ) AS `folder_name`'];
		$data_cjd = $client_joint_documents->searchWithSelect([],'date_uploaded','DESC');
		$data_merge = array_merge($data_cd,$data_cjd);
		$data_sliced = array_slice($data_merge, 0, 4);
		// $data = $this->fetchRawData("SELECT
									// 	* 
									// FROM
									// 	(
									// 	SELECT
									// 		*,
									// 		CONCAT( client_id, '_client' ) AS `d_id`,
									// 		( SELECT NAME FROM clients WHERE clients.client_id = client_documents.client_id ) AS `folder_name` 
									// 	FROM
									// 		client_documents UNION ALL
									// 	SELECT
									// 		*,
									// 		CONCAT( client_joint_id, '_joint' ) AS `d_id`,
									// 		( SELECT NAME FROM client_joints WHERE client_joints.client_joint_id = client_joint_documents.client_joint_id ) AS `folder_name` 
									// 	FROM
									// 		client_joint_documents 
									// 	) AS `last5` 
									// ORDER BY
									// 	last5.date_uploaded DESC 
									// 	LIMIT 4");
		return $data_sliced;
		// $this->pprint($data_sliced);
	}


	function getPageTable() {
		$clients = new Model_clients();
		$clients->selects = ['NAME',
										'CONCAT( client_id, "_client" ) AS `d_id`',
										'( SELECT date_uploaded FROM client_documents WHERE client_documents.client_id = clients.client_id ORDER BY doc_id DESC LIMIT 1 ) AS `last_modified`',
										'( SELECT ((SUM(file_size) / 1000) / 1000)  FROM client_documents WHERE client_documents.client_id = clients.client_id ORDER BY doc_id DESC LIMIT 1 ) as `size`'];
		$data_clients = $clients->searchWithSelect([],'last_modified','DESC');

		$client_joints = new Model_client_joints();
		$client_joints->selects = ['NAME',
									'CONCAT( client_joint_id, "_joint" ) AS `d_id`',
									'( SELECT date_uploaded FROM client_joint_documents WHERE client_joint_documents.client_joint_id = client_joints.client_joint_id ORDER BY doc_id DESC LIMIT 1 ) AS `last_modified`',
									'( SELECT ((SUM(file_size) / 1000) / 1000)  FROM client_joint_documents WHERE client_joint_documents.client_joint_id = client_joints.client_joint_id ORDER BY doc_id DESC LIMIT 1 ) AS `size`'];
		$data_client_joints= $client_joints->searchWithSelect([],'last_modified','DESC');
		$data = array_merge($data_clients,$data_client_joints);

		// $data = $this->fetchRawData("SELECT
		// 								* 
		// 							FROM
		// 								(
		// 								SELECT NAME, 
		// 									CONCAT( client_id, '_client' ) AS `d_id`,
		// 									( SELECT date_uploaded FROM client_documents WHERE client_documents.client_id = clients.client_id ORDER BY doc_id DESC LIMIT 1 ) AS `last_modified` ,
		// 									( SELECT ((SUM(file_size) / 1000) / 1000)  FROM client_documents WHERE client_documents.client_id = clients.client_id ORDER BY doc_id DESC LIMIT 1 ) as `size` 
		// 								FROM
		// 									clients UNION ALL
		// 								SELECT NAME,
		// 									CONCAT( client_joint_id, '_joint' ) AS `d_id`,
		// 									( SELECT date_uploaded FROM client_joint_documents WHERE client_joint_documents.client_joint_id = client_joints.client_joint_id ORDER BY doc_id DESC LIMIT 1 ) AS `last_modified` ,
		// 									( SELECT ((SUM(file_size) / 1000) / 1000)  FROM client_joint_documents WHERE client_joint_documents.client_joint_id = client_joints.client_joint_id ORDER BY doc_id DESC LIMIT 1 ) AS `size` 
		// 								FROM
		// 									client_joints 
		// 								) AS `all_clients` 
		// 							ORDER BY
		// 								all_clients.last_modified DESC");
		return $data;
	}

	function getFilesFromFolder() {
		
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
            $client_joint_id = $this->input->post('id');
            $client_joint_documents = new Model_client_joint_documents();
            $data = $client_joint_documents->search(['client_joint_id'=>$client_joint_id],'date_uploaded','DESC');
            // $data = $this->fetchRawData("SELECT * FROM client_joint_documents WHERE client_joint_id=$client_joint_id ORDER BY date_uploaded DESC");
        } else {
            $client_id = $this->input->post('id');
            $client_documents = new Model_client_documents();
            $data = $client_documents->search(['client_id'=>$client_id],'date_uploaded','DESC');
            // $data = $this->fetchRawData("SELECT * FROM client_documents WHERE client_id=$client_id ORDER BY date_uploaded DESC");
        }
		// echo $client_type;
		echo json_encode($data);
	}
	
}
