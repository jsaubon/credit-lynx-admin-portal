<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brokers extends MY_Controller { 
	public function __construct() { 
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_brokers');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
		$this->load->model('Model_support_tickets');
		$this->load->model('Model_support_ticket_responses');
		$this->load->model('Model_broker_emails');
		$this->load->model('Model_broker_etexts'); 	
	}
	public function index()
	{ 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Brokers';
		$data['page_title_s'] = 'Broker';
		$data['page_controller'] = 'brokers';  
		$data['page_folder'] = 'brokers';

		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses(); 
		$this->load->view('admin/Brokers.php',$data); 
	} 

	function getUsers() {
		$brokers = new Model_brokers();
		$data = $brokers->search([],'broker_id','DESC');
		// $data = $this->fetchArray($data);
		// $data = $this->fetchRawData("SELECT * FROM brokers ORDER BY broker_id DESC");
		return $data;
	}

	function sendBrokerText() {
		$broker_id = $this->input->post('broker_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes'); 
		$client_type = '';
		$from = '';
		$type = $this->input->post('type');
		$this->sendEmail('Broker',$type,$broker_id,$subject,$notes,date('Y-m-d H:i:s'),$client_type,$from);
	}

	function getBrokerTexts() {
		$broker_id = $this->input->post('broker_id');
		$broker_etexts = new Model_broker_etexts();
		$data = $broker_etexts->search(['broker_id'=>$broker_id],'text_id','DESC');
		// $data = $this->fetchRawData("SELECT * FROM broker_etexts WHERE broker_id=$broker_id ORDER BY text_id DESC");
		echo json_encode($data);
	}

	function getUserCount() {
		$brokers = new Model_brokers();
		$data = $brokers->get();
		// $data = $this->fetchRawData("SELECT count(broker_id) as `user_count` FROM brokers");
		return count($data);
	}

	function DeleteBroker(){
		$broker_id =  $this->input->post('broker_id');
		$brokers = new Model_brokers();
		$brokers->load($broker_id);
		$brokers->delete();
		// $this->db->WHERE('broker_id', $broker_id);
		// $this->db->delete('brokers');
	}

	function sendBrookerEmail() {
		$client_type ='';
		$broker_id = $this->input->post('broker_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes'); 
		$from = $this->input->post('from'); 
		// $client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$this->sendEmail('Broker',$type,$broker_id,$subject,$notes,date('Y-m-d H:i:s'),$client_type,$from);
	}
	

	function uploadProfilePicture() {
		$broker_id = $this->input->post('broker_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/brokers/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 
	    $brokers = new Model_brokers();
	    $brokers->load($broker_id);
	    $brokers->photo = $photo;
	    $brokers->save();
	    // $data = $this->db->query("UPDATE brokers SET photo='$photo' WHERE broker_id=$broker_id");
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$broker_id = $this->input->post('broker_id');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$company = $this->input->post('company');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state_province = $this->input->post('state_province');
		$zip_postal_code = $this->input->post('zip_postal_code');
		$cell = $this->input->post('cell');
		$NMLS = $this->input->post('NMLS');
		$office = $this->input->post('office');
		$email_address = $this->input->post('email_address');


		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/brokers/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_brokers');
	    $brokers = new Model_brokers();
	    if ($broker_id != '') {
	    	$brokers->broker_id = $broker_id;
	    }
	    $brokers->photo = $photo;
	    $brokers->first_name = $first_name;
		$brokers->last_name = $last_name;
		$brokers->company = $company;
		$brokers->address = $address;
		$brokers->city = $city;
		$brokers->state_province = $state_province;
		$brokers->zip_postal_code = $zip_postal_code;
		$brokers->cell = $cell;
		$brokers->NMLS = $NMLS;
		$brokers->office = $office;
		$brokers->email_address = $email_address;
		$brokers->username = $username;
		$brokers->password = $password;
	    $brokers->save();

	    redirect(base_url('admin/brokers'));

	}

	function updateField() {
		$broker_id = $this->input->post('broker_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($field == 'password') {
			$value = password_hash($value,PASSWORD_BCRYPT);
		}
		$brokers = new Model_brokers();
		$brokers->load($broker_id);
		$brokers->$field = $value;
		$brokers->save();
		// $data_update = $this->db->query("UPDATE brokers SET $field='$value' WHERE broker_id=$broker_id");
		echo "saved";
	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$brokers = new Model_brokers();
		$data = $brokers->search(['username'=>$username]);
		// $data = $this->fetchRawData("SELECT * FROM brokers WHERE username='$username'");
		echo count($data); 
	}


	function getDetails() {
		$broker_id = $this->input->post('broker_id');
		$brokers = new Model_brokers();
		$data = $brokers->search(['broker_id'=>$broker_id]); 
		// $data = $this->fetchRawData("SELECT * FROM brokers WHERE broker_id = $broker_id");
		echo json_encode($data);
	}
	function getBrokerEmails() {
		$broker_id = $this->input->post('broker_id');
		$broker_emails = new Model_broker_emails();
		$data = $broker_emails->search(['broker_id'=>$broker_id],'email_id','DESC');
		// $data = $this->fetchRawData("SELECT * FROM broker_emails WHERE broker_id = $broker_id ORDER by email_id DESC");
		echo json_encode($data);
	}

	function saveTask() {
		$broker_id = $this->input->post('broker_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Brokers';
		$tasks->task_assigned_id = $broker_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$broker_id = $this->input->post('broker_id');
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$broker_id,
									'task_assigned'=>'Brokers',
									'task_active <>'=>2
								],'task_id','DESC');
		// $data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$broker_id AND task_assigned='Brokers' AND task_active <> 2 ORDER BY task_id DESC");
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save();
		// $data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveNote() {
		$broker_id = $this->input->post('broker_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Brokers';
		$notes->note_assigned_id = $broker_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$broker_id = $this->input->post('broker_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$broker_id,
									'note_assigned'=>'Brokers'
								],'note_id','DESC');
		// $data = $this->fetchRawData("SELECT * FROM notes WHERE note_assigned_id=$broker_id AND note_assigned='Brokers' ORDER BY note_id DESC");
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky;
		// $data = $this->db->query("UPDATE notes SET note_sticky=$note_sticky WHERE note_id=$note_id");
		echo "updated";
	}

	function saveSupportTicket() { 
		$sender = $this->input->post('sender');
		$sender_id = $this->input->post('sender_id');
		$assigned_to = $this->input->post('assigned_to');
		$status = 'Pending';
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$support_date = date('Y-m-d H:i:s');

		$this->load->model('Model_support_tickets');
		$support_tickets = new Model_support_tickets(); 
		$support_tickets->sender = $sender;
		$support_tickets->sender_id = $sender_id;
		$support_tickets->assigned_to = $assigned_to;
		$support_tickets->status = $status;
		$support_tickets->subject = $subject;
		$support_tickets->message = $message;
		$support_tickets->support_date = $support_date;
		$support_tickets->save();

		// redirect(base_url('admin/clients'));
		echo "saved";

	}


	function getTableSupportTickets() {
		$broker_id = $this->input->post('broker_id');
		$support_tickets = new Model_support_tickets();
		$data = $support_tickets->search(['sender'=>'broker','sender_id'=>$broker_id],'support_id','DESC');
		// $data = $this->fetchRawData("SELECT * FROM support_tickets WHERE sender='Broker' AND sender_id=$broker_id ORDER BY support_id DESC");
		echo json_encode($data);
	}

	function deleteSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->delete();
		// $data = $this->db->query("DELETE FROM support_tickets WHERE support_id=$support_id");
		echo 'deleted!';
	}

	
	function getSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets(); 
 		$support_ticket_responses = new Model_support_ticket_responses();
 		$data['support_ticket_responses'] = $support_ticket_responses->search(['support_id'=>$support_id],'response_id','ASC'); 
 		$data['support_tickets'] = $support_tickets->search(['support_id',$support_id]);

		echo json_encode($data);
	}

	function updateSupportTicketStatus() {
		$support_id = $this->input->post('support_id');
		$status = $this->input->post('status');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->status = $status;
		$support_tickets->save();
		echo 'updated!';
	}

	
	function saveSupportTicketResponse() { 
		$userdata = $this->session->userdata('user_data');


		$support_id = $this->input->post('support_id');
		$responder = $userdata['login_type'];
		$login_type = $userdata['login_type'];
		$response_date = date('Y-m-d H:i:s');;
		$response = $this->input->post('response');

		$this->load->model('Model_support_ticket_responses');
		$support_ticket_responses = new Model_support_ticket_responses(); 
		$support_ticket_responses->support_id = $support_id;
		$support_ticket_responses->responder = $responder;
		$support_ticket_responses->response_date = $response_date;
		$support_ticket_responses->response = $response; 
		$support_ticket_responses->save();
		echo "saved";

		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		if ($login_type == 'Administrator' || $login_type == 'Employee') {
			$support_tickets->status = 'Responded';
			
		} else {
			$support_tickets->status = 'Pending'; 
		}
		$support_tickets->save();  

	}
	
}
