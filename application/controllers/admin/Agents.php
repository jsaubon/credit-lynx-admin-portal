<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agents extends MY_Controller { 
	public function __construct() { 
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_agents');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
		$this->load->model('Model_support_tickets');
		$this->load->model('Model_support_ticket_responses');
		$this->load->model('Model_agent_emails');
		$this->load->model('Model_agent_etexts');
		$this->load->model('Model_support_ticket_responses');
	}
	public function index()
	{ 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		// $data['all_users'] = $this->getallUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Agents';
		$data['page_title_s'] = 'Agent';
		$data['page_controller'] = 'agents';  
		$data['page_folder'] = 'agents';

		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses(); 
		$this->load->view('admin/Agents.php',$data); 
	} 

	function getAgentEmails() {
		$agent_id = $this->input->post('agent_id');
		$agent_emails = new Model_agent_emails();
		$data = $agent_emails->search(['agent_id'=>$agent_id],'email_id','DESC'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function getAgentEmailsTest() {
		$agent_id = 21;
		$agent_emails = new Model_agent_emails();
		$data = $agent_emails->search(['agent_id'=>$agent_id],'email_id','DESC'); 
		// $data = $this->fetchArray($data);
		// echo json_encode($data);
		$this->pprint($data);
	}

	


	function sendAgentEmail(){
		$client_type ='';
		$agent_id = $this->input->post('agent_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes'); 
		$from = $this->input->post('from'); 
		// $client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$this->sendEmail('Agent',$type,$agent_id,$subject,$notes,date('Y-m-d H:i:s'),$client_type,$from);
	}
	function sendAgentText(){
		$client_type ='';
		$agent_id = $this->input->post('agent_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes'); 
		// $client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$from = '';
		$this->sendEmail('Agent',$type,$agent_id,$subject,$notes,date('Y-m-d H:i:s'),$client_type,$from);
	}

	function getAgentTexts() {
		$agent_id = $this->input->post('agent_id');
		$agent_etexts = new Model_agent_etexts();
		$data = $agent_etexts->search(['agent_id'=>$agent_id],'text_id','DESC'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	public function agentDelete(){
		$agent_id = $this->input->post('agent_id');
		$agents = new Model_agents();
		$agents->load($agent_id);
		$agents->delete();
	}

	function getUsers() {
		$agents = new Model_agents();
		$data = $agents->search([],'agent_id','DESC');
		// $data = $this->fetchArray($data);
		return $data;
	}
	
	// function getallUsers() {
	// 	$data = $this->fetchRawData("SELECT * FROM agents ORDER BY agent_id DESC");
	// 	return $data;
	// }

	function getUserCount() {
		$agents = new Model_agents();
		$data = $agents->get();
		// $data = $this->fetchArray($data);
		return count($data);
	}

	function uploadProfilePicture() {
		$agent_id = $this->input->post('agent_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/agents/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 


	    $agents = new Model_agents();
	    $agents->load($agent_id);
	    $agents->photo = $photo;
	  	$agents->save();
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$agent_id = $this->input->post('agent_id');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$company = $this->input->post('company');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state_province = $this->input->post('state_province');
		$zip_postal_code = $this->input->post('zip_postal_code');
		$cell = $this->input->post('cell');
		$office = $this->input->post('office');
		$email_address = $this->input->post('email_address');


		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/agents/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_agents');
	    $agents = new Model_agents();
	    if ($agent_id != '') {
	    	$agents->agent_id = $agent_id;
	    }
	    $agents->photo = $photo;
	    $agents->first_name = $first_name;
		$agents->last_name = $last_name;
		$agents->company = $company;
		$agents->address = $address;
		$agents->city = $city;
		$agents->state_province = $state_province;
		$agents->zip_postal_code = $zip_postal_code;
		$agents->cell = $cell;
		$agents->office = $office;
		$agents->email_address = $email_address;
		$agents->username = $username;
		$agents->password = $password;
	    $agents->save();

	    redirect(base_url('admin/agents'));

	}

	function updateField() {
		$agent_id = $this->input->post('agent_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($field == 'password') {
			$value = password_hash($value,PASSWORD_BCRYPT);
		}

		$agents = new Model_agents();
		$agents->load($agent_id);
		$agents->$field = $value;
		$agents->save();
  
	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$agents = new Model_agents();
		$data = $agents->search(['username'=>$username]);
		// $data = $this->fetchArray($data); 
		echo count($data); 
	}


	function getDetails() {
		$agent_id = $this->input->post('agent_id');
		$agents = new Model_agents();
		$data = $agents->search(['agent_id'=>$agent_id]);
		// $data = $this->fetchArray($data);
 
		echo json_encode(reset($data));
	}

	function saveTask() {
		$agent_id = $this->input->post('agent_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Agents';
		$tasks->task_assigned_id = $agent_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$agent_id = $this->input->post('agent_id');
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$agent_id,
								'task_assigned'=>'Agents',
								'task_active <>'=>2
							],'task_id','DESC');
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save();
		
		echo "updated";
	}

	function saveNote() {
		$agent_id = $this->input->post('agent_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Agents';
		$notes->note_assigned_id = $agent_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$agent_id = $this->input->post('agent_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$agent_id,
										'note_assigned'=>'Agents'],'note_id','DESC');
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky; 
		$notes->save();
		echo "updated";
	}



	function saveSupportTicket() { 
		$sender = $this->input->post('sender');
		$sender_id = $this->input->post('sender_id');
		$assigned_to = $this->input->post('assigned_to');
		$status = 'Pending';
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$support_date = date('Y-m-d H:i:s');

		$this->load->model('Model_support_tickets');
		$support_tickets = new Model_support_tickets(); 
		$support_tickets->sender = $sender;
		$support_tickets->sender_id = $sender_id;
		$support_tickets->assigned_to = $assigned_to;
		$support_tickets->status = $status;
		$support_tickets->subject = $subject;
		$support_tickets->message = $message;
		$support_tickets->support_date = $support_date;
		$support_tickets->save();
 
		echo "saved";

	}


	function getTableSupportTickets() {
		$agent_id = $this->input->post('agent_id');
		$support_tickets = new Model_support_tickets();
		$data = $support_tickets->search(['sender'=>'agent',
											'sender_id'=>$agent_id
										],'support_id','DESC');
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function deleteSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->save();
		echo 'deleted!';
	}

	
	function getSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets(); 
 		$support_ticket_responses = new Model_support_ticket_responses();
 		$data['support_ticket_responses'] = $support_ticket_responses->search(['support_id'=>$support_id],'response_id','ASC'); 
 		$data['support_tickets'] = $support_tickets->search(['support_id',$support_id]);
 		// $data['support_ticket_responses'] = $this->fetchArray($data['support_ticket_responses']);
 		// $data['support_tickets'] = $this->fetchArray($data['support_tickets']);
 		
		echo json_encode($data);
	}

	function updateSupportTicketStatus() {
		$support_id = $this->input->post('support_id');
		$status = $this->input->post('status');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->status = $status;
		$support_tickets->save(); 
		echo 'updated!';
	}

	
	function saveSupportTicketResponse() { 
		$userdata = $this->session->userdata('user_data'); 
		$support_id = $this->input->post('support_id');
		$responder = $userdata['login_type'];
		$login_type = $userdata['login_type'];
		$response_date = date('Y-m-d H:i:s');;
		$response = $this->input->post('response');

		$this->load->model('Model_support_ticket_responses');
		$support_ticket_responses = new Model_support_ticket_responses(); 
		$support_ticket_responses->support_id = $support_id;
		$support_ticket_responses->responder = $responder;
		$support_ticket_responses->response_date = $response_date;
		$support_ticket_responses->response = $response; 
		$support_ticket_responses->save();
		echo "saved";
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		if ($login_type == 'Administrator' || $login_type == 'Employee') {
			$support_tickets->status = 'Responded';
			
		} else {
			$support_tickets->status = 'Pending'; 
		}
		$support_tickets->save();  

	}

	function get_partner_companies() {
		$partner_type = $this->input->post('partner_type'); 

		$data = $this->fetchRawData("SELECT * FROM partner_companies WHERE partner_type='$partner_type' GROUP BY company_name");
		echo json_encode($data); 
		// $this->pprint($data);
	}
	
}
