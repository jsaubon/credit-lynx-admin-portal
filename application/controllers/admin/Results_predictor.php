<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Results_predictor extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->checklog(); 

	}
	public function index() { 
		$data['userdata'] = $this->session->userdata('user_data');
		 $data['page_controller'] = 'Results_predictor';
		$this->load->view('admin/Results_predictor.php', $data);

	} 

	 function get_account_groups() {
	 	$data = $this->fetchRawData("SELECT * FROM (SELECT account_group FROM client_account_details WHERE account_group <> '' AND account_group IS NOT NULL GROUP BY account_group UNION ALL SELECT account_group FROM client_joint_account_details WHERE account_group <> '' AND account_group IS NOT NULL GROUP BY account_group) as aaa GROUP BY account_group");
	 	echo json_encode($data);
	 }

	 function get_attempts() {
	 	$account_group = $this->input->post('account_group');
	 	$data = $this->fetchRawData("SELECT SUM(count_num) as `count`,account_status FROM (SELECT COUNT(*) as `count_num`,account_status FROM client_account_details WHERE account_group='$account_group' GROUP BY account_status UNION ALL SELECT COUNT(*) as `count_num`,account_status FROM client_account_details WHERE account_group='$account_group' GROUP BY account_status) as aaa GROUP BY account_status");

	 	echo json_encode($data);
	 }
}