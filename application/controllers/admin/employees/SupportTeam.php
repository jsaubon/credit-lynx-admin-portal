<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SupportTeam extends MY_Controller { 
	public function __construct() { 
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_employees');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
	}
	public function index()
	{
		$this->checklog(); 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Support Team';
		$data['page_title_s'] = 'Support Team';
		$data['page_controller'] = 'SupportTeam';  
		$data['page_folder'] = 'team_support'; 
		$this->load->view('admin/employees/SupportTeam.php',$data); 
	} 

	function getUsers() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Support Team'],'employee_id','DESC');
		// $data = $this->fetchArray($data);
		// $data = $this->fetchRawData("SELECT * FROM employees WHERE type='Support Team' ORDER BY employee_id DESC");
		return $data; 
	}

	function getUserCount() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Support Team']);
		return count($data);
	}

	function uploadProfilePicture() {
		$employee_id = $this->input->post('employee_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/team_support/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $employees = new Model_employees();
	    $employees->load($employee_id);
	    $employees->photo = $photo;
	    $employees->save(); 
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$employee_id = $this->input->post('employee_id');
		$name = $this->input->post('name');
		$job_title = $this->input->post('job_title');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$zip_code = $this->input->post('zip_code');
		$date_of_birth = $this->input->post('date_of_birth');
		$email_address = $this->input->post('email_address');
		$phone_number = $this->input->post('phone_number');
		$ss = $this->input->post('ss');
		$salary = $this->input->post('salary');
		$hourly = $this->input->post('hourly');
		$bank_name = $this->input->post('bank_name');
		$bank_address = $this->input->post('bank_address');
		$account_number = $this->input->post('account_number');
		$routing_number = $this->input->post('routing_number');
		$emergency_name = $this->input->post('emergency_name');
		$emergency_phone = $this->input->post('emergency_phone');
		$emergency_address = $this->input->post('emergency_address');
		$emergency_city = $this->input->post('emergency_city');
		$emergency_state = $this->input->post('emergency_state');
		$emergency_zip = $this->input->post('emergency_zip');
		$emergency_relationship = $this->input->post('emergency_relationship');

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = password_hash($password, PASSWORD_BCRYPT); 

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/team_support/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_employees');
	    $team_support = new Model_employees();
	    if ($employee_id != '') {
	    	$team_support->employee_id = $employee_id;
	    }
	    $team_support->type = 'Support Team';
	    $team_support->photo = $photo;
	    $team_support->name = $name;
	    $team_support->job_title = $job_title;
	    $team_support->address = $address;
		$team_support->city = $city;
		$team_support->state = $state;
		$team_support->zip_code = $zip_code;
		$team_support->date_of_birth = $date_of_birth;
		$team_support->email_address = $email_address;
		$team_support->phone_number = $phone_number;
		$team_support->ss = $ss;
		$team_support->salary = $salary;
		$team_support->hourly = $hourly;
		$team_support->bank_name = $bank_name;
		$team_support->bank_address = $bank_address;
		$team_support->account_number = $account_number;
		$team_support->routing_number = $routing_number;
		$team_support->emergency_name = $emergency_name;
		$team_support->emergency_phone = $emergency_phone;
		$team_support->emergency_address = $emergency_address;
		$team_support->emergency_city = $emergency_city;
		$team_support->emergency_state = $emergency_state;
		$team_support->emergency_zip = $emergency_zip;
		$team_support->emergency_relationship = $emergency_relationship;
		$team_support->username = $username;
		$team_support->password = $password;
	    $team_support->save();

	    redirect(base_url('admin/employees/supportTeam'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Support Team','username'=>$username]);
		// $data = $this->fetchArray($data);
		echo count($data); 
	}


	function getDetails() {
		$employee_id = $this->input->post('employee_id');
		$employees = new Model_employees();
		$data = $employees->search(['employee_id'=>$employee_id]); 
		// $data = $this->fetchArray($data);
		echo json_encode(reset($data));
	}

	function saveTask() {
		$employee_id = $this->input->post('employee_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Support Team';
		$tasks->task_assigned_id = $employee_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$employee_id = $this->input->post('employee_id');
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$employee_id,
								'task_assigned'=>'Support Team',
								'task_active <>'=> 2
							],'task_id','DES'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save(); 
		echo "updated";
	}

	function saveNote() {
		$employee_id = $this->input->post('employee_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Support Team';
		$notes->note_assigned_id = $employee_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$employee_id = $this->input->post('employee_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$employee_id,
										'note_assigned'=>'Support Team'
									],'note_id','DESC'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky;
		$notes->save();
		echo "updated";
	}
	
}
