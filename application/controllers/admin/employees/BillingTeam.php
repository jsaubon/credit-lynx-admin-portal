<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BillingTeam extends MY_Controller { 
	public function __construct() { 
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_employees');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
	}
	public function index()
	{ 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Billing Team';
		$data['page_title_s'] = 'Billing Team';
		$data['page_controller'] = 'BillingTeam';  
		$data['page_folder'] = 'team_billing'; 
		$this->load->view('admin/employees/BillingTeam.php',$data); 
	} 

	function getUsers() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Billing Team'],'employee_id','DESC');
		// $data = $this->fetchArray($data);
		// $data = $this->fetchRawData("SELECT * FROM employees WHERE type='Billing Team' ORDER BY employee_id DESC");
		return $data; 
	}

	function getUserCount() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Billing Team']);  
		return count($data);
	}


	function uploadProfilePicture() {
		$employee_id = $this->input->post('employee_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/team_billing/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $employees = new Model_employees();
	    $employees->load($employee_id);
	    $employees->photo = $photo;
	    $employees->save(); 
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$employee_id = $this->input->post('employee_id');
		$name = $this->input->post('name');
		$job_title = $this->input->post('job_title');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$zip_code = $this->input->post('zip_code');
		$date_of_birth = $this->input->post('date_of_birth');
		$email_address = $this->input->post('email_address');
		$phone_number = $this->input->post('phone_number');
		$ss = $this->input->post('ss');
		$salary = $this->input->post('salary');
		$hourly = $this->input->post('hourly');
		$bank_name = $this->input->post('bank_name');
		$bank_address = $this->input->post('bank_address');
		$account_number = $this->input->post('account_number');
		$routing_number = $this->input->post('routing_number');
		$emergency_name = $this->input->post('emergency_name');
		$emergency_phone = $this->input->post('emergency_phone');
		$emergency_address = $this->input->post('emergency_address');
		$emergency_city = $this->input->post('emergency_city');
		$emergency_state = $this->input->post('emergency_state');
		$emergency_zip = $this->input->post('emergency_zip');
		$emergency_relationship = $this->input->post('emergency_relationship');

		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/team_billing/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_employees');
	    $team_billing = new Model_employees();
	    if ($employee_id != '') {
	    	$team_billing->employee_id = $employee_id;
	    }
	    $team_billing->type = 'Billing Team';
	    $team_billing->photo = $photo;
	    $team_billing->name = $name;
	    $team_billing->job_title = $job_title;
	    $team_billing->address = $address;
		$team_billing->city = $city;
		$team_billing->state = $state;
		$team_billing->zip_code = $zip_code;
		$team_billing->date_of_birth = $date_of_birth;
		$team_billing->email_address = $email_address;
		$team_billing->phone_number = $phone_number;
		$team_billing->ss = $ss;
		$team_billing->salary = $salary;
		$team_billing->hourly = $hourly;
		$team_billing->bank_name = $bank_name;
		$team_billing->bank_address = $bank_address;
		$team_billing->account_number = $account_number;
		$team_billing->routing_number = $routing_number;
		$team_billing->emergency_name = $emergency_name;
		$team_billing->emergency_phone = $emergency_phone;
		$team_billing->emergency_address = $emergency_address;
		$team_billing->emergency_city = $emergency_city;
		$team_billing->emergency_state = $emergency_state;
		$team_billing->emergency_zip = $emergency_zip;
		$team_billing->emergency_relationship = $emergency_relationship;
		$team_billing->username = $username;
		$team_billing->password = $password;
	    $team_billing->save();

	    redirect(base_url('admin/employees/billingTeam'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Billing Team','username'=>$username]);
		// $data = $this->fetchArray($data);
		echo count($data); 
	}


	function getDetails() {
		$employee_id = $this->input->post('employee_id');
		$employees = new Model_employees();
		$data = $employees->search(['employee_id'=>$employee_id]); 
		// $data = $this->fetchArray($data);
		echo json_encode(reset($data));
	}

	function saveTask() {
		$employee_id = $this->input->post('employee_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Billing Team';
		$tasks->task_assigned_id = $employee_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$employee_id = $this->input->post('employee_id');
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$employee_id,
								'task_assigned'=>'Billing Team',
								'task_active <>'=> 2
							],'task_id','DES'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save(); 
		echo "updated";
	}

	function saveNote() {
		$employee_id = $this->input->post('employee_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Billing Team';
		$notes->note_assigned_id = $employee_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$employee_id = $this->input->post('employee_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$employee_id,
										'note_assigned'=>'Billing Team'
									],'note_id','DESC'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky;
		$notes->save();
		echo "updated";
	}
	
}
