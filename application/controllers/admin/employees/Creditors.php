<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creditors extends MY_Controller { 
	
	public function index()
	{
		// $this->checklog(); 
		// $userdata['userdata'] = $this->session->userdata('user_data');
		// $data['users'] = $this->getUsers();
		// $data['state_provinces'] = $this->getStateProvinces();
		// $data['user_count'] = $this->getUserCount();
		// $data['page_title'] = 'Sales';
		// $data['page_title_s'] = 'Sales';
		// $data['page_controller'] = 'sales';  
		// $data['page_folder'] = 'sales';
		// $this->load->view('admin/includes/header.php',$userdata);
		// $this->load->view('admin/includes/nav-left.php',$userdata);
		// $this->load->view('admin/includes/nav-top.php',$userdata);
		// $this->load->view('admin/employees/Sales.php',$data);
		// $this->load->view('admin/includes/footer.php',$userdata);
	} 

	function getUsers() {
		$data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' ORDER BY employee_id DESC");
		return $data;
		echo "string";
	}

	function getUserCount() {
		$data = $this->fetchRawData("SELECT count(employee_id) as `user_count` FROM employees WHERE type='Sales Team'");
		return $data[0]['user_count'];
	}

	function uploadProfilePicture() {
		$employee_id = $this->input->post('employee_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/sales/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $data = $this->db->query("UPDATE employees SET photo='$photo' WHERE employee_id=$employee_id");
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$employee_id = $this->input->post('employee_id');
		$name = $this->input->post('name');
		$job_title = $this->input->post('job_title');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$zip_code = $this->input->post('zip_code');
		$date_of_birth = $this->input->post('date_of_birth');
		$email_address = $this->input->post('email_address');
		$phone_number = $this->input->post('phone_number');
		$ss = $this->input->post('ss');
		$salary = $this->input->post('salary');
		$hourly = $this->input->post('hourly');
		$commission = $this->input->post('commission');
		$bank_name = $this->input->post('bank_name');
		$bank_address = $this->input->post('bank_address');
		$account_number = $this->input->post('account_number');
		$routing_number = $this->input->post('routing_number');
		$emergency_name = $this->input->post('emergency_name');
		$emergency_phone = $this->input->post('emergency_phone');
		$emergency_address = $this->input->post('emergency_address');
		$emergency_city = $this->input->post('emergency_city');
		$emergency_state = $this->input->post('emergency_state');
		$emergency_zip = $this->input->post('emergency_zip');
		$emergency_relationship = $this->input->post('emergency_relationship');

		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/sales/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_employees');
	    $sales = new Model_employees();
	    if ($employee_id != '') {
	    	$sales->employee_id = $employee_id;
	    }
	    $sales->type = 'Sales Team';
	    $sales->photo = $photo;
	    $sales->name = $name;
	    $sales->job_title = $job_title;
	    $sales->address = $address;
		$sales->city = $city;
		$sales->state = $state;
		$sales->zip_code = $zip_code;
		$sales->date_of_birth = $date_of_birth;
		$sales->email_address = $email_address;
		$sales->phone_number = $phone_number;
		$sales->ss = $ss;
		$sales->salary = $salary;
		$sales->hourly = $hourly;
		$sales->commission = $commission;
		$sales->bank_name = $bank_name;
		$sales->bank_address = $bank_address;
		$sales->account_number = $account_number;
		$sales->routing_number = $routing_number;
		$sales->emergency_name = $emergency_name;
		$sales->emergency_phone = $emergency_phone;
		$sales->emergency_address = $emergency_address;
		$sales->emergency_city = $emergency_city;
		$sales->emergency_state = $emergency_state;
		$sales->emergency_zip = $emergency_zip;
		$sales->emergency_relationship = $emergency_relationship;
		$sales->username = $username;
		$sales->password = $password;
	    $sales->save();

	    redirect(base_url('admin/employees/sales'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' AND username='$username'");
		echo count($data); 
	}


	function getDetails() {
		$employee_id = $this->input->post('employee_id');
		$data = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM employees WHERE employee_id = $employee_id");
		echo json_encode($data);
	}

	function saveTask() {
		$employee_id = $this->input->post('employee_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Sales Team';
		$tasks->task_assigned_id = $employee_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$employee_id = $this->input->post('employee_id');
		$data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$employee_id AND task_assigned='Sales Team' AND task_active <> 2 ORDER BY task_id DESC");
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveNote() {
		$employee_id = $this->input->post('employee_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Sales Team';
		$notes->note_assigned_id = $employee_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$employee_id = $this->input->post('employee_id');
		$data = $this->fetchRawData("SELECT * FROM notes WHERE note_assigned_id=$employee_id AND note_assigned='Sales Team' ORDER BY note_id DESC");
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$data = $this->db->query("UPDATE notes SET note_sticky=$note_sticky WHERE note_id=$note_id");
		echo "updated";
	}

	
	
}
