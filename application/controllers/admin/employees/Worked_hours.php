<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worked_hours extends MY_Controller { 
	
	public function index()
	{ 
		$this->checklog(); 
		$data['userdata'] = $this->session->userdata('user_data');
		$data['page_title'] = 'Worked Hours';
		$data['page_title_s'] = 'Worked Hours';
		$data['page_controller'] = 'worked_hours';  
		$data['page_folder'] = 'worked_hours'; 
		$this->load->view('admin/employees/Worked_hours.php',$data); 
	} 

	function startWork() {
		$userdata = $this->session->userdata('user_data');
		$worker = $userdata['login_type'];
		$worker_id = $userdata['user_id'];

		$start_time = date('Y-m-d H:i:s');


		$this->load->model('Model_work_hours');
		$work_hours = new Model_work_hours();
		$work_hours->worker = $worker;
		$work_hours->worker_id = $worker_id;
		$work_hours->start_time = $start_time;  
		$work_hours->save();

		echo $work_hours->work_id;
	}

	function getWorkHours() {
		$userdata = $this->session->userdata('user_data');
		$worker = $userdata['login_type'];
		$worker_id = $userdata['user_id'];

		$curdate = date('Y-m-d H:i:s');
		$data = $this->fetchRawData("SELECT max(work_hours.work_id) as `work_id`, HOUR(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `hours` ,  MINUTE(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `minutes`,sum(IF(stop_time IS NULL,1,0)) as `stop`  FROM work_hours WHERE DATE(start_time)=CURDATE() AND worker='$worker' AND worker_id=$worker_id ");
		echo json_encode($data);
	}

	function stopWork() {
		$work_id = $this->input->post('work_id');
		$stop_time = date('Y-m-d H:i:s');
		$data = $this->db->query("UPDATE work_hours SET stop_time='$stop_time' WHERE work_id=$work_id");
		echo "stoped";
	}

	
	function getEmployeesTimeWorked() {
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$from = date('Y-m-d',strtotime($from));
		$to = date('Y-m-d',strtotime($to));
		$curdate = date('Y-m-d H:i:s');
		$data = $this->fetchRawData("SELECT * FROM (
									SELECT hourly,`name`
										,
										photo,
										processors_time_worked.* 
									FROM
										processors
										INNER JOIN ( SELECT * FROM (SELECT HOUR(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `hours` ,  MINUTE(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `minutes`, DATE(start_time) as `work_date`, work_hours.* FROM work_hours WHERE DATE(start_time) BETWEEN '$from' AND '$to' GROUP BY worker,worker_id,DATE(start_time) ) as `view_employees_time_worked` WHERE worker = 'Dispute Team' ) AS processors_time_worked ON processors.processor_id = processors_time_worked.worker_id UNION ALL
									SELECT hourly,`name`
										,
										photo,
										sales_time_worked.* 
									FROM
										sales
										INNER JOIN ( SELECT * FROM (SELECT HOUR(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `hours` ,  MINUTE(SEC_TO_TIME(SUM( TIME_TO_SEC(TIMEDIFF(IF ( stop_time IS NULL, '$curdate', stop_time ), start_time )) ))) as `minutes`, DATE(start_time) as `work_date`, work_hours.* FROM work_hours WHERE DATE(start_time) BETWEEN '$from' AND '$to' GROUP BY worker,worker_id,DATE(start_time) ) as `view_employees_time_worked` WHERE worker = 'Sales Team' ) AS sales_time_worked ON sales.sale_id = sales_time_worked.worker_id) as aaa WHERE work_date BETWEEN '$from' AND '$to' GROUP BY worker,worker_id,DATE(start_time) ORDER BY name");

		  
		echo json_encode($data);
	}
	
}
