<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MY_Controller { 
	
	public function __construct() { 
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_employees');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
	}
	public function index()
	{
		
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		$data['users_active'] = $this->getUsersActive();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Sales';
		$data['page_title_s'] = 'Sales';
		$data['page_controller'] = 'sales';  
		$data['page_folder'] = 'sales'; 
		$data['active_employee_id'] = $this->get_pool_employee_id();
		$this->load->view('admin/employees/Sales.php',$data); 
	} 

	function getUsersActive() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team','status'=>1],'employee_id','ASC');
		// $data = $this->fetchArray($data);
		// $data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' ORDER BY employee_id DESC");
		return $data; 
	}
	function getUsers() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team'],'employee_id','DESC');
		// $data = $this->fetchArray($data);
		// $data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' ORDER BY employee_id DESC");
		return $data; 
	}

	function get_pool_employee_id() {
		$data = $this->fetchRawData("SELECT * FROM employees WHERE type='Sales Team' AND pool=1 AND status=1");
		if (count($data) > 0) {
			$data = reset($data);
			$data = reset($data);
			return $data;
		} else {
			return 0;
		}
	}
	function set_next_pool() {
		$employee_id = $this->input->post('employee_id');
		// $employee_id = 25;
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team','status'=>1],'employee_id','ASC');
		// $this->pprint($data);
		$index = array_search($employee_id, array_column($data, 'employee_id'));
		if ($index < (count($data) -1)) {
			echo $data[$index + 1]['employee_id'];
		} else {
			echo $data[0]['employee_id'];
		}
		


	}

	function set_lead_pool() {
		$employee_id = $this->input->post('employee_id');
		$pool = $this->input->post('pool');
		$this->db->query("UPDATE employees SET pool=0 WHERE type='Sales Team'");

		$this->db->query("UPDATE employees SET pool=$pool WHERE employee_id=$employee_id");


	}

	function getUserCount() {
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team']);
		return count($data);
	}

 

	function uploadProfilePicture() {
		$employee_id = $this->input->post('employee_id');
		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/sales/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    } 

	    $employees = new Model_employees();
	    $employees->load($employee_id);
	    $employees->photo = $photo;
	    $employees->save(); 
	}

	function saveDetail() {
		// $this->pprint($this->input->post());
		$employee_id = $this->input->post('employee_id');
		$name = $this->input->post('name');
		$job_title = $this->input->post('job_title');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$zip_code = $this->input->post('zip_code');
		$date_of_birth = $this->input->post('date_of_birth');
		$email_address = $this->input->post('email_address');
		$phone_number = $this->input->post('phone_number');
		$ss = $this->input->post('ss');
		$salary = $this->input->post('salary');
		$hourly = $this->input->post('hourly');
		$commission = $this->input->post('commission');
		$bank_name = $this->input->post('bank_name');
		$bank_address = $this->input->post('bank_address');
		$account_number = $this->input->post('account_number');
		$routing_number = $this->input->post('routing_number');
		$emergency_name = $this->input->post('emergency_name');
		$emergency_phone = $this->input->post('emergency_phone');
		$emergency_address = $this->input->post('emergency_address');
		$emergency_city = $this->input->post('emergency_city');
		$emergency_state = $this->input->post('emergency_state');
		$emergency_zip = $this->input->post('emergency_zip');
		$emergency_relationship = $this->input->post('emergency_relationship');

		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		

		if($_FILES['user_photo']['size'] > 0)
		{
			 $photo = $_FILES['user_photo']['name'];
			 // echo $photo;
			 if (strpos($photo, 'png') !== false)
			 {
				$filen = date('YmdHis').'.png';
			 }
			 else
			 {
			 	$filen = date('YmdHis').'.jpg';
			 }

	 		$photo = $filen;

			if(!empty($_FILES['user_photo']['name'])){
	            $filetmp = $_FILES["user_photo"]["tmp_name"];
	            $filename = $_FILES["user_photo"]["name"];
	            $filetype = $_FILES["user_photo"]["type"];
	            $filepath = "assets/images/users/sales/".$filen;

	            move_uploaded_file($filetmp, $filepath); 
	    	}
	    }
	    else
	    {
	    	$photo = $this->input->post('current_photo');
	    }

	    $this->load->model('Model_employees');
	    $sales = new Model_employees();
	    if ($employee_id != '') {
	    	$sales->employee_id = $employee_id;
	    }
	    $sales->type = 'Sales Team';
	    $sales->photo = $photo;
	    $sales->name = $name;
	    $sales->job_title = $job_title;
	    $sales->address = $address;
		$sales->city = $city;
		$sales->state = $state;
		$sales->zip_code = $zip_code;
		$sales->date_of_birth = $date_of_birth;
		$sales->email_address = $email_address;
		$sales->phone_number = $phone_number;
		$sales->ss = $ss;
		$sales->salary = $salary;
		$sales->hourly = $hourly;
		$sales->commission = $commission;
		$sales->bank_name = $bank_name;
		$sales->bank_address = $bank_address;
		$sales->account_number = $account_number;
		$sales->routing_number = $routing_number;
		$sales->emergency_name = $emergency_name;
		$sales->emergency_phone = $emergency_phone;
		$sales->emergency_address = $emergency_address;
		$sales->emergency_city = $emergency_city;
		$sales->emergency_state = $emergency_state;
		$sales->emergency_zip = $emergency_zip;
		$sales->emergency_relationship = $emergency_relationship;
		$sales->username = $username;
		$sales->password = $password;
		$sales->status = 1;
		$sales->pool = 0;
	    $sales->save();

	    redirect(base_url('admin/employees/sales'));

	}

	function checkIfExist() {
		$username = $this->input->post('username_val');
		$employees = new Model_employees();
		$data = $employees->search(['type'=>'Sales Team','username'=>$username]);
		// $data = $this->fetchArray($data);
		echo count($data); 
	}


	function getDetails() {
		$employee_id = $this->input->post('employee_id');
		$employees = new Model_employees();
		$data = $employees->search(['employee_id'=>$employee_id]); 
		// $data = $this->fetchArray($data);
		echo json_encode(reset($data));
	}

	function saveTask() {
		$employee_id = $this->input->post('employee_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Sales Team';
		$tasks->task_assigned_id = $employee_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$employee_id = $this->input->post('employee_id');
		$tasks = new Model_tasks();
		$data = $tasks->search(['task_assigned_id'=>$employee_id,
								'task_assigned'=>'Sales Team',
								'task_active <>'=> 2
							],'task_id','DES'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$tasks = new Model_tasks();
		$tasks->load($task_id);
		$tasks->task_active = $task_active;
		$tasks->save(); 
		echo "updated";
	}

	function saveNote() {
		$employee_id = $this->input->post('employee_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Sales Team';
		$notes->note_assigned_id = $employee_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$employee_id = $this->input->post('employee_id');
		$notes = new Model_notes();
		$data = $notes->search(['note_assigned_id'=>$employee_id,
										'note_assigned'=>'Sales Team'
									],'note_id','DESC'); 
		// $data = $this->fetchArray($data);
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$notes = new Model_notes();
		$notes->load($note_id);
		$notes->note_sticky = $note_sticky;
		$notes->save();
		echo "updated";
	}

	function uploadDocumentFile() {   
		$employee_id = $this->input->post('employee_id'); 
		$folder_name = $this->input->post('folder_name');

		$client_name = $folder_name;

		$folder_name = 'assets/documents/' . $folder_name;
		if (!is_dir($folder_name)) {
			mkdir($folder_name, 0777);
		}
		$files = count($_FILES['document']['name']);
		// $this->pprint($_FILES);
		$this->load->model('Model_employee_documents'); 
		if ($files > 1) {
			for ($i = 0; $i < count($_FILES['document']['name']); $i++) {
				if ($_FILES['document']['size'][$i] > 0) {
					$document = $_FILES['document']['name'][$i];
					$file_size = $_FILES['document']['size'][$i];

					$file_download = $document;

					if (!empty($_FILES['document']['name'][$i])) {
						$filetmp = $_FILES["document"]["tmp_name"][$i];
						$filename = $_FILES["document"]["name"][$i];
						$filetype = $_FILES["document"]["type"][$i];
						$filepath = $folder_name . '/' . $file_download;
						if (file_exists($filepath)) {
							$file_download = '_' . $file_download;
							$filepath = $folder_name . '/' . $file_download;
						}

						move_uploaded_file($filetmp, $filepath);
					}
					 
					$employee_documents = new Model_employee_documents();

					$employee_documents->employee_id = $employee_id;
					$employee_documents->category = '';
					$employee_documents->file_name = $file_download;
					$employee_documents->file_download = $filepath;
					$employee_documents->file_size = $file_size;
					$employee_documents->date_uploaded = date('Y-m-d H:i:s');
					$employee_documents->save(); 

				}

			}
		} else {
			if ($_FILES['document']['size'][0] > 0) {
				$document = $_FILES['document']['name'][0];
				$file_size = $_FILES['document']['size'][0];

				$file_download = $document;

				if (!empty($_FILES['document']['name'][0])) {
					$filetmp = $_FILES["document"]["tmp_name"][0];
					$filename = $_FILES["document"]["name"][0];
					$filetype = $_FILES["document"]["type"][0];
					$filepath = $folder_name . '/' . $file_download;
					if (file_exists($filepath)) {
						$file_download = '_' . $file_download;
						$filepath = $folder_name . '/' . $file_download;
					}

					move_uploaded_file($filetmp, $filepath);
				}

				 
				$employee_documents = new Model_employee_documents();

				$employee_documents->employee_id = $employee_id;
				$employee_documents->category = '';
				$employee_documents->file_name = $file_download;
				$employee_documents->file_download = $filepath;
				$employee_documents->file_size = $file_size;
				$employee_documents->date_uploaded = date('Y-m-d H:i:s');
				$employee_documents->save(); 

			  

			}

		}

		echo $filepath;

	}

	
	
}
