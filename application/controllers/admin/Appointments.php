<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointments extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->checklog();
		$this->load->model('Model_appointments');
		$this->load->model('Model_admins');
		$this->load->model('Model_clients');
		$this->load->model('Model_brokers');
		$this->load->model('Model_agents');
		$this->load->model('Model_employees');
		$this->load->model('Model_appointment_notifications');
	}
	public function index() {

		$data['userdata'] = $this->session->userdata('user_data');
		$data['page_title'] = 'Appointments';
		$data['page_title_s'] = 'Appointment';
		$data['page_controller'] = 'appointments';
		$this->load->view('admin/Appointments.php', $data);
	}

	function saveAppointment() {

		$app_from = $this->input->post('app_from');
		$app_from_id = $this->input->post('app_from_id');

		if ($app_from == '') {
			$userdata = $this->session->userdata('user_data');
			$app_from = $userdata['login_type'];
			$app_from_id = $userdata['user_id'];
		}

		$app_id = $this->input->post('app_id');
		$app_to = $this->input->post('app_to');
		$app_to_id = $this->input->post('app_to_id');
		$appointment = $this->input->post('appointment');
		$date_start = $this->input->post('date_start');
		$date_end = $this->input->post('date_end');
		$app_color = $this->input->post('app_color');
		$app_phone = $this->input->post('app_phone');
		$app_location = $this->input->post('app_location');
		$app_email = $this->input->post('app_email');
		$app_note = $this->input->post('app_note');
		$notif_type = $this->input->post('notif_type');
		$notif_time = $this->input->post('notif_time');
		$notif_schedule = $this->input->post('notif_schedule');

		$appointments = new Model_appointments();
		if ($app_id != '') {
			$appointments->app_id = $app_id;
		}

		$appointments->app_from = $app_from;
		$appointments->app_from_id = $app_from_id;
		$appointments->app_to = $app_to;
		$appointments->app_to_id = $app_to_id;
		$appointments->appointment = $appointment;
		$appointments->date_start = $date_start;
		$appointments->date_end = $date_end;
		$appointments->app_color = $app_color;
		$appointments->app_phone = $app_phone;
		$appointments->app_location = $app_location;
		$appointments->app_email = $app_email;
		$appointments->app_note = $app_note;
		$appointments->save();

		if ($app_to == 'Lead') {
			$client_id = $app_to_id;
			$this->db->query("UPDATE clients SET lead_status='Contacted' WHERE client_id=$client_id");
		}

		foreach ($notif_type as $key => $value) {
			$appointment_notifications = new Model_appointment_notifications();
			$appointment_notifications->app_id = $appointments->app_id;
			$appointment_notifications->notif_type = $notif_type[$key];
			$appointment_notifications->notif_time = $notif_time[$key];
			$appointment_notifications->notif_schedule = $notif_schedule[$key];
			$appointment_notifications->save();
		}

	}

	function getAppointments() {
		$userdata = $this->session->userdata('user_data');
		$app_from = $userdata['login_type'];
		$app_from_id = $userdata['user_id'];

		$app_to = $this->input->post('app_to');
		$app_to_id = $this->input->post('app_to_id');

		if ($app_to == 'Administrator') {
			$data = $this->Model_Query->getView('view_appointments', [], 'date_start', 'DESC');
			// $data = $this->fetchRawData("SELECT * FROM view_appointments ORDER BY date_start DESC");
		} else {
			$data = $this->Model_Query->getView('view_appointments', ['app_from' => $app_from,
				'app_from_id' => $app_from_id,
				'app_to' => $app_to,
				'app_to_id' => $app_to_id,
			], 'date_start', 'DESC');
			// $data = $this->fetchRawData("SELECT * FROM view_appointments WHERE app_from='$app_from' AND app_from_id=$app_from_id AND app_to='$app_to' AND app_to_id=$app_to_id ORDER BY date_start DESC");
		}

		echo json_encode($data);
	}

	function deleteAppointment() {
		$app_id = $this->input->post('app_id');
		$appointments = new Model_appointments();
		$appointments->load($app_id);
		$appointments->delete();
		// $delete_data = $this->db->query("DELETE FROM appointments WHERE app_id=$app_id");
		echo "deleted!";
	}

	function getAppToSelect() {
		$table = $this->input->post('table');
		$clients = new Model_clients();
		$agents = new Model_agents();
		$brokers = new Model_brokers();
		$employees = new Model_employees();
		$data = array();
		if ($table == 'Client') {
			$clients = new Model_clients();
			$clients->selects = ['client_id as `id`', 'name'];
			$data = $clients->searchWithSelect(['converted' => 1,
				'client_status <>' => 'Cancelled',
				'client_status <>' => 'Complete',
				'client_status <>' => 'client_status',
			], 'name', 'ASC');
			// $data = $this->fetchRawData("SELECT client_id as `id`,name FROM clients WHERE converted=1 AND client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ORDER BY name ASC");
		}
		if ($table == 'Lead') {
			$clients = new Model_clients();
			$clients->selects = ['client_id as `id`', 'name'];
			$data = $clients->searchWithSelect(['converted' => 0,
				'client_status <>' => 'Cancelled',
				'client_status <>' => 'Complete',
				'client_status <>' => 'client_status',
			], 'name', 'ASC');
			// $data = $this->fetchRawData("SELECT client_id as `id`,name FROM clients WHERE converted=0 AND client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ORDER BY name ASC");
		}
		if ($table == 'Administrator') {
			$admins = new Model_admins();
			$admins->selects = ['admin_id as `id`', 'name'];
			$data = $admins->searchWithSelect([], 'name', 'ASC');
			// $data = $this->fetchRawData("SELECT admin_id as `id`,name FROM admins ORDER BY name ASC");
		}
		if ($table == 'Agent') {
			$agents = new Model_agents();
			$agents->selects = ['agent_id as `id`', 'CONCAT(first_name, " " ,last_name) as `name`'];
			$data = $agents->searchWithSelect([], 'first_name', 'ASC');
			// $data = $this->fetchRawData("SELECT agent_id as `id`,CONCAT(first_name, ' ' ,last_name) as `name` FROM agents ORDER BY first_name ASC");
		}
		if ($table == 'Broker') {
			$brokers = new Model_brokers();
			$brokers->selects = ['broker_id as `id`', 'CONCAT(first_name, " " ,last_name) as `name`'];
			$data = $brokers->searchWithSelect([], 'first_name', 'ASC');
			// $data = $this->fetchRawData("SELECT broker_id as `id`,CONCAT(first_name, ' ' ,last_name) as `name` FROM brokers ORDER BY first_name ASC");
		}
		if ($table == 'Sales Team') {
			$employees = new Model_employees();
			$employees->selects = ['employee_id as `id`', 'name'];
			$data = $employees->searchWithSelect(['type' => 'Sales Team'], 'name', 'ASC');
			// $data = $this->fetchRawData("SELECT sale_id as `id`,name FROM sales ORDER BY name ASC");
		}
		if ($table == 'Dispute Team') {
			$employees = new Model_employees();
			$employees->selects = ['employee_id as `id`', 'name'];
			$data = $employees->searchWithSelect(['type' => 'Dispute Team'], 'name', 'ASC');
			// $data = $this->fetchRawData("SELECT processor_id as `id`,name FROM processors ORDER BY name ASC");
		}

		echo json_encode($data);

	}

	function getAppointmentByAppID() {
		$app_id = $this->input->post('app_id');
		$data = $this->Model_Query->getView('view_appointments', ['app_id' => $app_id]);
		// $data = $this->fetchRawData("SELECT * FROM view_appointments WHERE app_id=$app_id");
		echo json_encode($data);
	}

	function getAppointmentNotificationByAppID() {
		$app_id = $this->input->post('app_id');
		$appointment_notifications = new Model_appointment_notifications();
		$data = $appointment_notifications->search(['app_id' => $app_id]);
		// $data = $this->fetchRawData("SELECT * FROM appointment_notifications WHERE app_id=$app_id");
		echo json_encode($data);
	}

	function deleteAppointmentNotification() {
		$an_id = $this->input->post('an_id');
		$appointment_notifications = new Model_appointment_notifications();
		$appointment_notifications->load($an_id);
		$appointment_notifications->delete();
		// $data = $this->db->query("DELETE FROM appointment_notifications WHERE an_id=$an_id");
		echo "deleted!";
	}

	function get_client_appointments() {
		$app_to = $this->input->post('app_to');
		$app_to_id = $this->input->post('app_to_id');
		$data = $this->fetchRawData("SELECT appointments.*,(SELECT GROUP_CONCAT(CONCAT(notif_type,' (',notif_time,' ',notif_schedule,')') SEPARATOR '<br>') FROM appointment_notifications WHERE app_id=appointments.app_id) as `notification` FROM appointments WHERE app_to='$app_to' AND app_to_id=$app_to_id ORDER BY app_id DESC");

		echo json_encode($data); 
	}

	public function cron_get_appointment_notification()
	{
		$data = $this->fetchRawData("SELECT * FROM appointments WHERE DATE(date_start) = CURRENT_DATE");
		foreach ($data as $key => $value) {
			$app_id = $value['app_id'];
			$notifs = $this->fetchRawData("SELECT * FROM appointment_notifications WHERE app_id=$app_id"); 
			$date_start = ($value['date_start']);
			foreach ($notifs as $k => $v) {
				$notif_time = $v['notif_time'];
				$notif_schedule = $v['notif_schedule']; 
				$notif_actual_time = date('Y-m-d H:i:s',strtotime($date_start. ' - '.$notif_time. ' ' .$notif_schedule  ));
				echo $notif_actual_time;
			} 

		}
			
	}

}
