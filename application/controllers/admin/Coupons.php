<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons extends MY_Controller { 
	public function __construct() {
		parent::__construct();
		$this->checklog(); 
		$this->load->model('Model_coupons');
	}
	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['coupons'] = $this->getCoupons(); 
		$data['coupon_count'] = $this->getUserCount();
		$data['page_title'] = 'Coupons';
		$data['page_title_s'] = 'Coupons';
		$data['page_controller'] = 'Coupons';  
		// $data['page_folder'] = 'admins'; 
		$this->load->view('admin/Coupons.php',$data); 
	} 

	function getCoupons() {
		$coupons = new Model_coupons();
		$data = $coupons->search([],'coupon_id','DESC');
		// -- $data = $this->fetchRawData("SELECT * FROM coupons ORDER BY coupon_id DESC");
		return $data;
	}

	function getUserCount() {
		$coupons = new Model_coupons();
		$data = $coupons->search([],'coupon_id','DESC');
		// $data = $this->fetchRawData("SELECT count(coupon_id) as `coupon_count` FROM coupons");
		return count($data);
	}

	 

	function saveDetail() {
		// $this->pprint($this->input->post());
		$coupon_id = $this->input->post('coupon_id');
		$coupon_code = $this->input->post('coupon_code'); 
		$limit = $this->input->post('limit');
		$amount = $this->input->post('amount');
		$date_start = $this->input->post('date_start');  
		$date_end = $this->input->post('date_end');  


		
	    
	    $coupons = new Model_coupons();
	    if ($coupon_id != '') {
	    	$coupons->coupon_id = $coupon_id;
	    }
	    $coupons->coupon_code = $coupon_code;
	    $coupons->limit = $limit; 
	    $coupons->amount = $amount; 
		$coupons->date_start = date('Y-m-d',strtotime($date_start));
		$coupons->date_end = date('Y-m-d',strtotime($date_end));
	    $coupons->save();

	    redirect(base_url('admin/coupons'));

	} 


	function getDetails() {
		$coupon_id = $this->input->post('coupon_id');
		$coupons = new Model_coupons();
		$data = $coupons->search(['coupon_id'=>$coupon_id],'','',1);
		// $data = $this->fetchRawData("SELECT * FROM coupons WHERE coupon_id = $coupon_id");
		echo json_encode($data);
	}

	function deleteCoupon() {
		$coupon_id = $this->input->post('coupon_id');
		$coupons = new Model_coupons();
		$coupons->load($coupon_id);
		$coupons->delete();
		// $data = $this->db->query("DELETE FROM coupons WHERE coupon_id='$coupon_id'");
		echo "deleted";
	}
 
	
}
