<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alert_schedules extends MY_Controller {

	public function index()
	{ 
		 
		$data['userdata'] = $this->session->userdata('user_data'); 
		$this->load->view('admin/Alert_schedules.php', $data);
	}

	function get_auto_alert_schedule_clients() {
		$client_id = $this->input->post('client_id');
		$restricted_alerts = 'Equifax Reports Received|Experian Reports Received|TransUnion Reports Received|Equifax and TransUnion Reports Received|Equifax and Experian Reports Received|Experian and TransUnion Reports Received|Equifax, Experian, and TransUnion Reports Received|Cancellation Form Received|Hold Request Received|Invoice Sent|New Payment Information Required|Account Terminated|1st Late Notice|2nd Late Notice|Final Late Notice|Account Suspended|Account Terminated';

		$alert_subjects = 'Drafted Twelfth Round Letters|Drafted Eleventh Round Letters|Drafted Tenth Round Letters|Drafted Ninth Round Letters|Drafted Eighth Round Letters|Drafted Seventh Round Letters|Drafted Sixth Round Letters|Drafted Fifth Round Letters|Drafted Fourth Round Letters|Drafted Third Round Letters|Drafted Third Round Letters Without Experian Submission|Drafted Second Round Letters|Drafted First Round Letters Without Full Credit Report Submission|Drafted First Round Letters';
		$alert_missing = 'Missing Equifax Pages|Missing Experian Pages|Missing TransUnion Pages|Credit Report Missing Pages';


		$client_alerts = $this->fetchRawData("
				SELECT * FROM (SELECT
					client_id,
					name,
					client_status,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
					( SELECT alert_notes FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC,alert_id ASC LIMIT 1 ) AS `first_pending_all_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC,alert_id ASC LIMIT 1 ) AS `first_pending_all_alert_subject`
				FROM
					clients 
				WHERE auto_alert=1 AND client_id=$client_id AND 
					client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
					AND client_id IN (SELECT client_id FROM client_alerts WHERE client_type='client' GROUP BY client_id)
					AND converted = 1  ) as `aaa` WHERE aaa.last_alert_date IS NOT NULL
		");
		
		// $this->pprint($client_alerts);
		$date_today = date('m/d/Y');
		// echo "Today: ".$date_today; 
		// echo "<br>";
		$holidays = $this->getHolidays();
		$data_arr = array();
		foreach ($client_alerts as $key => $alert) { 
			$data_ = array();
			$client_id = $alert['client_id']; 
			$data_arr[$key]['id'] = $alert['client_id'];
			$data_arr[$key]['name'] = $alert['name'];
 
			$alert_date = $alert['last_alert_date'];
			$data_arr[$key]['round_alert_date'] = date('m/d/Y',strtotime($alert_date));
			$message = ""; 
				if ($alert['last_restricted_alert_date'] == '') {
					
					if (strtotime($alert_date) < strtotime($alert['first_pending_all_alert_date'])) { 
						$message .= '<br>First Alert Subject Sent: '.$alert['first_pending_all_alert_subject'];
						$message .= $this->get_pending_alert_to_send_today($date_today,$alert['first_pending_all_alert_date'],$alert['first_pending_all_alert_subject'],$holidays);  
					} else { 
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays); 
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						$message .= "First Reminder Date: ".$first_reminder;  
						$message .= "<br>Second Reminder: ".$second_reminder; 
						$message .= "<br>Third Reminder: ".$third_reminder;  
						$message .= "<br>First Submit: ".$first_submit;  
						$message .= "<br>Second Submit: ".$second_submit;
						$message .= "<br>First Deadline: ".$first_deadline;  
						$message .= "<br>Second Deadline: ".$second_deadline;  
						$message .= "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						$message .= "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						
					}
						
				} else { 
					if (strtotime($alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays); 
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						$message .= "First Reminder Date: ".$first_reminder;  
						$message .= "<br>Second Reminder: ".$second_reminder; 
						$message .= "<br>Third Reminder: ".$third_reminder;  
						$message .= "<br>First Submit: ".$first_submit;  
						$message .= "<br>Second Submit: ".$second_submit;
						$message .= "<br>First Deadline: ".$first_deadline;  
						$message .= "<br>Second Deadline: ".$second_deadline;  
						$message .= "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						$message .= "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						
					} else { 
						
						if ($alert['last_restricted_alert_subject'] == 'Equifax, Experian, and TransUnion Reports Received') {
							if ($alert['client_status'] != 'Active' && $alert['client_status'] != 'AV' && $alert['client_status'] != 'AV2' && $alert['client_status'] != 'AVSSN' && $alert['client_status'] != 'SSN') {
								$message .= 'Last Alert Subject Sent: '.$alert['last_restricted_alert_subject'];
								$message .= "<br>change to review";
								$message .= "<br>turn off alert"; 
							}
						} else { 
							$message .= 'Last Alert Subject Sent: '.$alert['last_restricted_alert_subject'];
							$message .= $this->get_pending_alert_to_send_today($date_today,$alert['last_restricted_alert_date'],$alert['last_restricted_alert_subject'],$holidays);  
						} 

						
					}
				}

				if ($alert['last_missing_alert_date'] != '') {   
					$last_missing_alert_date = date('m/d/Y',strtotime($alert['last_missing_alert_date']));

					if (strtotime($last_missing_alert_date) > strtotime($alert['last_restricted_alert_date']) && strtotime($last_missing_alert_date) > strtotime($alert_date)) {
						$next_missing_alert_date = date('m/d/Y',strtotime($last_missing_alert_date. ' + 2 days' ));
						// $alert_template = $this->get_alert_template_by_subject($alert['last_missing_alert_subject']); 
 
						while (strtotime($next_missing_alert_date) < strtotime($date_today)) {
							$next_missing_alert_date = date('m/d/Y',strtotime($next_missing_alert_date. ' + 2 days'));
						}  

						$next_missing_alert_date = $this->skip_weekend_and_holidays($next_missing_alert_date,$holidays);

							$message .= "<br><h3>Missing Alert</h3>";
						$message .= "Next Missing Alert Date: ".$next_missing_alert_date;
						if ($date_today == $next_missing_alert_date) {
							// $this->pprint($alert_template);
							$message .= "<br>Subject: ".$alert['last_missing_alert_subject'];
							$message .= "<br>Note: ".$alert['last_missing_alert_notes']; 
						}
						
					} 	 
						
				}  
 	 
 
			$data_arr[$key]['message'] = $message;
			// array_push($data_arr, $data_); 
			// $this->pprint($data_arr[$key]);

		}
 		echo json_encode($data_arr);

	}

	function get_auto_alert_schedule_joints() { 
		$client_joint_id = $this->input->post('client_joint_id');
		$restricted_alerts = 'Equifax Reports Received|Experian Reports Received|TransUnion Reports Received|Equifax and TransUnion Reports Received|Equifax and Experian Reports Received|Experian and TransUnion Reports Received|Equifax, Experian, and TransUnion Reports Received|Cancellation Form Received|Hold Request Received|Invoice Sent|New Payment Information Required|Account Terminated|1st Late Notice|2nd Late Notice|Final Late Notice|Account Suspended|Account Terminated';

		$alert_subjects = 'Drafted Twelfth Round Letters|Drafted Eleventh Round Letters|Drafted Tenth Round Letters|Drafted Ninth Round Letters|Drafted Eighth Round Letters|Drafted Seventh Round Letters|Drafted Sixth Round Letters|Drafted Fifth Round Letters|Drafted Fourth Round Letters|Drafted Third Round Letters|Drafted Third Round Letters Without Experian Submission|Drafted Second Round Letters|Drafted First Round Letters Without Full Credit Report Submission|Drafted First Round Letters';
		$alert_missing = 'Missing Equifax Pages|Missing Experian Pages|Missing TransUnion Pages|Credit Report Missing Pages';


		$client_alerts = $this->fetchRawData("
				SELECT * FROM (SELECT
					client_joint_id,
					client_status,
					name,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
					( SELECT alert_notes FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_subject`
				FROM
					client_joints 
				WHERE auto_alert=1 AND client_joint_id=$client_joint_id AND 
					client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
					AND client_joint_id IN (SELECT client_id FROM client_alerts WHERE client_type='joint' GROUP BY client_id)
					AND client_id IN (SELECT client_id FROM clients WHERE converted=1)) as `aaa` WHERE aaa.last_alert_subject IS NOT NULL
		");

		// $this->pprint($client_alerts);
		$date_today = date('m/d/Y');
		// echo "Today: ".$date_today; 
		// echo "<br>";
		$holidays = $this->getHolidays();
		$data_arr = array();
		foreach ($client_alerts as $key => $alert) { 
			$data_ = array();
			$client_joint_id = $alert['client_joint_id']; 
			$data_arr[$key]['id'] = $alert['client_joint_id'];
			$data_arr[$key]['name'] = $alert['name'];
 
			$alert_date = $alert['last_alert_date'];
			$data_arr[$key]['round_alert_date'] = date('m/d/Y',strtotime($alert_date));
			$message = ""; 
				if ($alert['last_restricted_alert_date'] == '') {
					
					if (strtotime($alert_date) < strtotime($alert['first_pending_all_alert_date'])) { 
						$message .= '<br>First Alert Subject Sent: '.$alert['first_pending_all_alert_subject'];
						$message .= $this->get_pending_alert_to_send_today($date_today,$alert['first_pending_all_alert_date'],$alert['first_pending_all_alert_subject'],$holidays);  
					} else { 
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays); 
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						$message .= "First Reminder Date: ".$first_reminder;  
						$message .= "<br>Second Reminder: ".$second_reminder; 
						$message .= "<br>Third Reminder: ".$third_reminder;  
						$message .= "<br>First Submit: ".$first_submit;  
						$message .= "<br>Second Submit: ".$second_submit;
						$message .= "<br>First Deadline: ".$first_deadline;  
						$message .= "<br>Second Deadline: ".$second_deadline;  
						$message .= "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						$message .= "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						
					}
						
				} else { 
					if (strtotime($alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays); 
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						$message .= "First Reminder Date: ".$first_reminder;  
						$message .= "<br>Second Reminder: ".$second_reminder; 
						$message .= "<br>Third Reminder: ".$third_reminder;  
						$message .= "<br>First Submit: ".$first_submit;  
						$message .= "<br>Second Submit: ".$second_submit;
						$message .= "<br>First Deadline: ".$first_deadline;  
						$message .= "<br>Second Deadline: ".$second_deadline;  
						$message .= "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						$message .= "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						
					} else { 
						
						if ($alert['last_restricted_alert_subject'] == 'Equifax, Experian, and TransUnion Reports Received') {
							if ($alert['client_status'] != 'Active' && $alert['client_status'] != 'AV' && $alert['client_status'] != 'AV2' && $alert['client_status'] != 'AVSSN' && $alert['client_status'] != 'SSN') {
								$message .= 'Last Alert Subject Sent: '.$alert['last_restricted_alert_subject'];
								$message .= "<br>change to review";
								$message .= "<br>turn off alert"; 
							}
						} else { 
							$message .= 'Last Alert Subject Sent: '.$alert['last_restricted_alert_subject'];
							$message .= $this->get_pending_alert_to_send_today($date_today,$alert['last_restricted_alert_date'],$alert['last_restricted_alert_subject'],$holidays);  
						} 

						
					}
				}

				if ($alert['last_missing_alert_date'] != '') {   
					$last_missing_alert_date = date('m/d/Y',strtotime($alert['last_missing_alert_date']));

					if (strtotime($last_missing_alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$next_missing_alert_date = date('m/d/Y',strtotime($last_missing_alert_date. ' + 2 days' ));
						// $alert_template = $this->get_alert_template_by_subject($alert['last_missing_alert_subject']); 
 
						while (strtotime($next_missing_alert_date) < strtotime($date_today)) {
							$next_missing_alert_date = date('m/d/Y',strtotime($next_missing_alert_date. ' + 2 days'));
						}  

						$next_missing_alert_date = $this->skip_weekend_and_holidays($next_missing_alert_date,$holidays);

							$message .= "<br><h3>Missing Alert</h3>";
						$message .= "Next Missing Alert Date: ".$next_missing_alert_date;
						if ($date_today == $next_missing_alert_date) {
							// $this->pprint($alert_template);
							$message .= "<br>Subject: ".$alert['last_missing_alert_subject'];
							$message .= "<br>Note: ".$alert['last_missing_alert_notes']; 
						}
						
					} 	 
						
				}  
 	 
 
			$data_arr[$key]['message'] = $message;
			// array_push($data_arr, $data_); 
			// $this->pprint($data_arr[$key]);

		}
 		echo json_encode($data_arr);
	}

	function get_pending_alert_to_send_today($date_today,$alert_date,$alert_subject,$holidays) {
		$alert_template = $this->get_alert_shortname($alert_subject); 
		$return = [];
		$message = '';  
		if (count($alert_template) > 0) { 
			$pending_alert_date = date('m/d/Y',strtotime($alert_date . ' + 2 days'));
			$pending_alert_date = $this->skip_weekend_and_holidays($pending_alert_date,$holidays);
			//echo " - ".$pending_alert_date;
			
			if ($pending_alert_date == $date_today) {
				$return = $alert_template;
			} 
			$next_pending_alert_date = date('m/d/Y',strtotime($pending_alert_date . ' + 3 days'));
			$next_pending_alert_date = $this->skip_weekend_and_holidays($next_pending_alert_date,$holidays);
			// echo " - ".$next_pending_alert_date;
			$message .= "<br>Last Alert Date: ". date('m/d/Y',strtotime($alert_date));
			$message .= "<br>Pending Alert Date: ".$pending_alert_date;
			$message .= "<br>Next Pending Alert Date: ";
			if (strtotime($date_today) >= strtotime($pending_alert_date)) { 
				while (strtotime($next_pending_alert_date) < strtotime($date_today)) {
					$next_pending_alert_date = date('m/d/Y',strtotime($next_pending_alert_date . ' + 3 days'));
					$next_pending_alert_date = $this->skip_weekend_and_holidays($next_pending_alert_date,$holidays);
					$message .= "<br>".$next_pending_alert_date;
				} 
				

				$message .= "<br>".$next_pending_alert_date;
				if ($date_today == $next_pending_alert_date) {
					$return = $alert_template;
				}
			}  
			$message .= "<br><br> <h3>Alert to Send:</h3>";
			$message .= "Subject: ".$alert_template['alert_subject'];
			$message .= "<br> Note: ".$alert_template['alert_notes'];
			$message .= "<br>";
		} 

		return $message;
	}

	function skip_weekend_and_holidays($date,$holidays) { 
		while (date('l',strtotime($date)) == 'Saturday' || date('l',strtotime($date)) == 'Sunday' || in_array($date, $holidays)) {
			$date = date('m/d/Y',strtotime($date . ' + 1 days')); 
		} 
		return $date;
	}

	function getHolidays() {
		$json = file_get_contents('https://www.googleapis.com/calendar/v3/calendars/en.usa%23holiday%40group.v.calendar.google.com/events?key=AIzaSyDpvJ8smtYNdm8sO5_xKrznC2b9nn_OrKY');
		$obj = json_decode($json,TRUE); 
		$days = array();
		foreach ($obj['items'] as $key => $holiday) {
			array_push($days, date('m/d/Y',strtotime($holiday['start']['date'])));
		}

		return $days;
	}

	function get_alert_template($shortname) {
		$alert_templates = $this->fetchRawData("SELECT * FROM client_alerts_templates WHERE template_shortname='$shortname'"); 
		$data['alert_subject'] = $alert_templates[0]['template_subject'];
		$data['alert_notes'] = $alert_templates[0]['template_notes']; 
		return $data;
	}

	function get_alert_shortname($alert_subject) { 
		switch ($alert_subject) {
			case strpos($alert_subject, 'Equifax Reports Received'):
				$shortname = 'XP/TU Pending';
				break;
			case strpos($alert_subject, 'Experian Reports Received'):
				$shortname = 'EF/TU Pending';
				break;
			case strpos($alert_subject, 'TransUnion Reports Received'):
				$shortname = 'EF/XP Pending';
				break;
			case strpos($alert_subject, 'Equifax and TransUnion Reports Received'):
				$shortname = 'XP Pending';
				break;
			case strpos($alert_subject, 'Equifax and Experian Reports Received'):
				$shortname = 'TU Pending';
				break;
			case strpos($alert_subject, 'Experian and TransUnion Reports Received'):
				$shortname = 'EF Pending';
				break; 
			case strpos($alert_subject, 'Pending All Verifications'):
				$shortname = 'Verifs';
				break; 
			default:
				$shortname = 'no send';
				break;
		}  
		if ($shortname != 'no send') {
			return $this->get_alert_template($shortname);
		} else { 
			return []; 
		}
	}

}

/* End of file Alert_schedules.php */
/* Location: ./application/controllers/admin/Alert_schedules.php */