<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->checklog(); 

	}
	public function index() { 
		$data['userdata'] = $this->session->userdata('user_data');
		 $data['page_controller'] = 'billing';
		$this->load->view('admin/Billing.php', $data);

	} 

	public function subscriptions() {
		$data['userdata'] = $this->session->userdata('user_data');
		$data['subscriptionCount'] = $this->get_client_count();
		$this->load->view('admin/authorize/Subscriptions.php', $data);
	}

	function get_client_count() { 
		$data = $this->fetchRawData("SELECT * FROM ((SELECT
									'Active' as `status`,
										name as `full_name`,
										client_status,
										(SELECT plan FROM client_billings WHERE client_id = clients.client_id LIMIT 1) as `plan_name`,
										(SELECT card_expiration FROM client_billings WHERE client_id = clients.client_id LIMIT 1) as `card_exp`,
										( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_billings WHERE client_id = clients.client_id LIMIT 1 ) as `payment_method`,
										( SELECT monthly_fee FROM client_billings WHERE client_id = clients.client_id LIMIT 1 ) as `monthly_fee`,
										(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = clients.client_id AND type='client') AND name IN (SELECT name FROM view_client_cards WHERE id = clients.client_id AND type='client')) as `past_occurrences`,
										(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = clients.client_id AND type='client') AND name IN (SELECT name FROM view_client_cards WHERE id = clients.client_id AND type='client') ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
										
									FROM
										clients 
									WHERE
										client_id NOT IN ( SELECT id FROM subscription_list WHERE type = 'client' ) 
										AND converted = 1 
										AND client_status NOT IN ( 'Archived', 'Cancelled', 'Terminated' ) 
										AND client_id NOT IN ( SELECT client_id FROM client_billings WHERE monthly_due = '' ))

									UNION ALL 
									(SELECT
									'Active' as `status`,
										name as `full_name`,
										client_status,
										(SELECT plan FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1) as `plan_name`,
										(SELECT card_expiration FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1) as `card_exp`,
										( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1 ) as `payment_method`,
										( SELECT monthly_fee FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1 ) as `monthly_fee`,
										(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') AND name IN (SELECT name FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint')) as `past_occurrences`,
										(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') AND name IN (SELECT name FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
										
									FROM
										client_joints 
									WHERE
										client_joint_id NOT IN ( SELECT id FROM subscription_list WHERE type = 'joint' ) 
										AND client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated'))
										AND client_status NOT IN ( 'Archived', 'Cancelled', 'Terminated' ) 
										AND client_joint_id NOT IN ( SELECT client_joint_id FROM client_joint_billings WHERE monthly_due = '' ))
										
										
									UNION ALL 


									(SELECT * FROM (SELECT 
										subscription_list.status,
										IF ( type = 'client', 
												( SELECT NAME FROM clients WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT NAME FROM client_joints WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `full_name`  ,
										IF ( type = 'client', 
												( SELECT client_status FROM clients WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT client_status FROM client_joints WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `client_status`  ,
										IF ( type = 'client', 
												( SELECT plan FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT plan FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `plan_name` ,
										IF ( type = 'client', 
												( SELECT card_expiration FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT card_expiration FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `card_exp` ,
										IF ( type = 'client', 
												( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `payment_method`  ,
										IF ( type = 'client', 
												( SELECT monthly_fee FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT monthly_fee FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `monthly_fee` ,
											(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) AND name IN (SELECT name FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type)) as `past_occurrences`,
											(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) AND name IN (SELECT name FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
											
										FROM
											subscription_list 
										WHERE
											subs_id IN ( SELECT MAX( subs_id ) FROM subscription_list GROUP BY id, type ) 
										ORDER BY
											id,
											type) as `subscriptions` WHERE `subscriptions`.client_status NOT IN ('Archived','Cancelled','Terminated'))) as `union_all`");
		$response['active'] = 0;
		$response['cancelled'] = 0;
		$response['expiring'] = 0;
		$response['expiring_now'] = 0;
		$restriction = array('Cancelled','Terminated','Archived');
		foreach ($data as $key => $value) {
			if (!in_array($value['client_status'], $restriction)) {
				if ($value['status'] == 'Active') {
					$response['active']++;
				} 
				if ($value['status'] == 'Cancelled') {
					$response['cancelled']++;
				}


				$month_now = date('my');
				$month_next = date('my',strtotime(date('Y-m-d') . ' + 1 months'));

				if ($value['card_exp'] == $month_now || $value['card_exp'] == $month_next) {
					$response['expiring']++;
				}
				if ($value['card_exp'] == $month_now) {
					$response['expiring_now']++;
				}
			}
				
		}

		return $response;
	}

	function get_table_invoices() {
		$data = $this->fetchRawData("SELECT
										IF
											(
												type = 'client',
												( SELECT NAME FROM clients WHERE client_id = client_invoices.id LIMIT 1 ),
												( SELECT NAME FROM client_joints WHERE client_joint_id = client_invoices.id LIMIT 1 ) 
											) AS `client_name`,
											client_invoices.* 
										FROM
											client_invoices 
											WHERE IF(type='client',id IN (SELECT client_id FROM clients WHERE client_status NOT IN ('Archived','Cancelled','Terminated') AND client_id = client_invoices.id),id IN (SELECT client_joint_id FROM client_joints WHERE client_joint_id=id AND client_status NOT IN ('Archived','Cancelled','Terminated')))
										ORDER BY
											invoice_date DESC");
		echo json_encode($data);
	}

	function get_client_subscriptions() {
		$status = $this->input->post('status');
			
		$query = " status='$status' ";
		if ($status == 'Expiring') {
			$month_now = date('my');
			$month_next = date('my',strtotime(date('Y-m-d') . ' + 1 months'));
			$query = " status='Active' AND card_exp IN ('$month_now','$month_next') ";
		} 
		if ($status == 'Expiring Now') {
			$month_now = date('my');
			$query = " status='Active' AND card_exp = '$month_now' ";
		} 
		$data = $this->fetchRawData("SELECT * FROM ((SELECT
									'Active' as `status`,
										name as `full_name`,
										client_status,
										(SELECT plan FROM client_billings WHERE client_id = clients.client_id LIMIT 1) as `plan_name`,
										(SELECT card_expiration FROM client_billings WHERE client_id = clients.client_id LIMIT 1) as `card_exp`,
										( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_billings WHERE client_id = clients.client_id LIMIT 1 ) as `payment_method`,
										( SELECT monthly_fee FROM client_billings WHERE client_id = clients.client_id LIMIT 1 ) as `monthly_fee`,
										(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = clients.client_id AND type='client') AND name IN (SELECT name FROM view_client_cards WHERE id = clients.client_id AND type='client')) as `past_occurrences`,
										(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = clients.client_id AND type='client') AND name IN (SELECT name FROM view_client_cards WHERE id = clients.client_id AND type='client') ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
										
									FROM
										clients 
									WHERE
										client_id NOT IN ( SELECT id FROM subscription_list WHERE type = 'client' ) 
										AND converted = 1 
										AND client_status NOT IN ( 'Archived', 'Cancelled', 'Terminated' ) 
										AND client_id NOT IN ( SELECT client_id FROM client_billings WHERE monthly_due = '' ))

									UNION ALL 
									(SELECT
									'Active' as `status`,
										name as `full_name`,
										client_status,
										(SELECT plan FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1) as `plan_name`,
										(SELECT card_expiration FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1) as `card_exp`,
										( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1 ) as `payment_method`,
										( SELECT monthly_fee FROM client_joint_billings WHERE client_joint_id = client_joints.client_joint_id LIMIT 1 ) as `monthly_fee`,
										(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') AND name IN (SELECT name FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint')) as `past_occurrences`,
										(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') AND name IN (SELECT name FROM view_client_cards WHERE id = client_joints.client_joint_id AND type='joint') ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
										
									FROM
										client_joints 
									WHERE
										client_joint_id NOT IN ( SELECT id FROM subscription_list WHERE type = 'joint' ) 
										AND client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated'))
										AND client_status NOT IN ( 'Archived', 'Cancelled', 'Terminated' ) 
										AND client_joint_id NOT IN ( SELECT client_joint_id FROM client_joint_billings WHERE monthly_due = '' ))
										
										
									UNION ALL 


									(SELECT * FROM (SELECT 
										subscription_list.status,
										IF ( type = 'client', 
												( SELECT NAME FROM clients WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT NAME FROM client_joints WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `full_name`  ,
										IF ( type = 'client', 
												( SELECT client_status FROM clients WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT client_status FROM client_joints WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `client_status`  ,
										IF ( type = 'client', 
												( SELECT plan FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT plan FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `plan_name` ,
										IF ( type = 'client', 
												( SELECT card_expiration FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT card_expiration FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `card_exp` ,
										IF ( type = 'client', 
												( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT CONCAT(payment_method,' XXXX',RIGHT(card_number,4)) FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `payment_method`  ,
										IF ( type = 'client', 
												( SELECT monthly_fee FROM client_billings WHERE client_id = subscription_list.id LIMIT 1 ),
												( SELECT monthly_fee FROM client_joint_billings WHERE client_joint_id = subscription_list.id LIMIT 1 ) 
											) AS `monthly_fee` ,
											(SELECT count(*) FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) AND name IN (SELECT name FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type)) as `past_occurrences`,
											(SELECT submitted_local FROM authorize_transactions WHERE status IN ('settledSuccessfully','capturedPendingSettlement') AND RIGHT(account_number,4) IN (SELECT RIGHT(card_number,4) FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) AND name IN (SELECT name FROM view_client_cards WHERE id = subscription_list.id AND type=subscription_list.type) ORDER BY submitted_local DESC LIMIT 1) as `last_payment_date`
											
										FROM
											subscription_list 
										WHERE
											subs_id IN ( SELECT MAX( subs_id ) FROM subscription_list GROUP BY id, type ) 
										ORDER BY
											id,
											type) as `subscriptions` WHERE status='Active' AND `subscriptions`.client_status NOT IN ('Archived','Cancelled','Terminated'))) as `union_all` WHERE $query ORDER BY last_payment_date DESC");
		echo json_encode($data);
	}


	
}