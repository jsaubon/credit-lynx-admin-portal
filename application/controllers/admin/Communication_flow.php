<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Communication_flow extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checklog();  
	}
	public function index() { 
		$data['userdata'] = $this->session->userdata('user_data');
	 	$data['page_controller'] = 'communication_flow';
	 	$data['client_statuses'] = $this->get_client_statuses();
	 	$data['lead_statuses'] = $this->get_lead_statuses();
		$this->load->view('admin/Communication_flow.php', $data);

	} 

	function get_lead_statuses() {
		$data = $this->fetchRawData("SELECT * FROM lead_statuses ORDER BY ls_id ASC");
		return $data;
	}
	function get_client_statuses() {
		$data = $this->fetchRawData("SELECT * FROM client_statuses ORDER BY client_status_id ASC");
		return $data;
	}

	function add_default_timeline_to_client() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$status_type = $this->input->post('status_type');
		$this->db->query("DELETE FROM client_status_timelines WHERE id=$id AND type='$type'");
		$this->db->query("INSERT INTO client_status_timelines (id,type,status_type,status,day,subject,note,notif_type) SELECT '$id','$type',status_type,status,day,subject,note,notif_type FROM client_status_timeline_default WHERE status='$status' AND status_type='$status_type'");
	}

	function get_client_status_changes() {
		$data = $this->fetchRawData("SELECT
										id,
										type,
										new_status,
										old_status,
										status_type,
										DATEDIFF( CURRENT_DATE, `date_changed` ) AS `days` ,
										(SELECT subject FROM client_status_timelines WHERE id=client_status_changes.id AND type=client_status_changes.type AND status=client_status_changes.new_status AND status_type=client_status_changes.status_type AND day=DATEDIFF( CURRENT_DATE, client_status_changes.`date_changed` )) as `subject`,
										(SELECT note FROM client_status_timelines WHERE id=client_status_changes.id AND type=client_status_changes.type AND status=client_status_changes.new_status AND status_type=client_status_changes.status_type AND day=DATEDIFF( CURRENT_DATE, client_status_changes.`date_changed` )) as `note`,
										(SELECT notif_type FROM client_status_timelines WHERE id=client_status_changes.id AND type=client_status_changes.type AND status=client_status_changes.new_status AND status_type=client_status_changes.status_type AND day=DATEDIFF( CURRENT_DATE, client_status_changes.`date_changed` )) as `notif_type`
									FROM
										client_status_changes 
									WHERE
										sc_id IN ( SELECT MAX( sc_id ) FROM client_status_changes GROUP BY id, type ) AND new_status NOT IN ('Cancelled','Terminated','Archived')");
		foreach ($data as $key => $value) {
			$id = $value['id'];
			$type = $value['type'];
			$status = $value['new_status']; 
			$subject = $value['subject'];
			$note = $value['note'];
			$notif_type = $value['notif_type'];

			if ($notif_type != '') {
				$this->pprint($data[$key]);
				if ($this->get_auto_alert(	$type == 'client' ? 'client_id' : 'client_joint_id', 
									$id, 
									$type == 'client' ? 'clients' : 'client_joints') == 1) { 
					if ($notif_type == 'email') {
						$this->send_email_to_client($id,$subject,$note,$type);
					}
					if ($notif_type == 'sms') {
						$this->send_text_alert_to_client($id, $type, $subject, $note);
					}

					if ($notif_type == 'both') {
						$this->send_email_to_client($id,$subject,$note,$type);
						$this->send_text_alert_to_client($id, $type, $subject, $note); 
					} 
					
				} else {
					echo "<br>Email/Text not sent (Alert Off) ";
				}
			}

		}
	}


	function get_comm_flow() {
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$type_data = $type == 'client' ? 'Clients' : 'Client Joints';
		$data = $this->fetchRawData("SELECT * FROM (
			SELECT 0 as `id`,type as `from`,'fa-mobile-alt' as `convo_type`,title as `subject`,text_message as `note`,date as `date` FROM client_etexts WHERE client_id=$id AND client_type='$type' AND text_message NOT LIKE '%*Credit Lynx - DO NOT REPLY*%' AND title NOT IN (SELECT template_subject FROM client_alerts_templates)
			UNION ALL
			SELECT 0 as `id`,type as `from`,'fa-envelope' as `convo_type`,subject as `subject`,message as `note`,date as `date` FROM client_emails WHERE client_id=$id AND client_type='$type' AND subject NOT IN (SELECT template_subject FROM client_alerts_templates)
			UNION ALL
			SELECT note_id as `id`,'to_client' as `from`,'fa-sticky-note' as `convo_type`,'' as `subject`,note as `note`,note_date as `date` FROM notes WHERE note_assigned_id=$id AND LCASE(note_assigned)='$type_data'
			
			) as aaa ORDER BY  aaa.date ASC
			");
			
			echo json_encode($data);
			// UNION ALL
			// SELECT IF(call_type = 'inbound','from_client','to_client') as `from`,'fa-phone' as `convo_type`,'' as `subject`,`call` as `note`,call_date as `date` FROM calls WHERE call_assigned_id=$id AND LCASE(call_assigned)='$type_data'
	}
}

/* End of file Communication_flow.php */
/* Location: ./application/controllers/admin/Communication_flow.php */