<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'Fetch/autoload.php';
use Fetch\Message;
use Fetch\Server;

class Email extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function getDocumentTemplate() {
		$data['client_name'] = 'Joshua Saubon';
		$data['client_status'] = 'Active';
		$data['category'] = 'Credit Report';
		$data['file_size'] = '1.0MB';
		$data['account_access'] = '';
		$this->load->view('email_templates/document_template', $data);
	}

	public function sendMail() {

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		// $this->email->priority(3);
		$this->email->from('joshuasaubon@gmail.com', 'CreditLynx');
		$this->email->to('joshuasaubon@gmail.com');
		// $this->email->to('8134409146@vtext.com');
		$this->email->subject('Email Test');
		// $image = base_url('assets/images/LynxLogo.png');
		$this->email->message('Testing the email from. - joshua');
		// $this->email->attach(base_url('assets/images/LynxLogo.png'));
		// echo $this->email->message;

		if ($this->email->send()) {
			echo "Emial has been sent";
		} else {
			$this->email->print_debugger();
		}
	}

	function getEmailMessagesInitial() {
		$server = new Server('imap.gmail.com', 993);
		$server->setAuthentication('Kayci@Creditlynx.com', 'Wrbwrx14!');

		/** @var Message[] $message */
		// $messages = $server->getOrderedMessages(1,1,10);
		$messages = $server->search('ALL', 'desc', 10);
		foreach ($messages as $message) {
			$uid = $message->getUid();
			$from = $message->getAddresses('from');
			$from = $from['address'];
			$subject = $message->getSubject();
			$body = $message->getMessageBody(true);
			// $body = preg_replace('#<no-reply@creditlynx.com (.|\s)+</no-reply@creditlynx.com>#', '', $body);
			$attachments = $message->getAttachments();
			$date = $message->getDate();
			$date = $this->dateParse($date);

			$email_type = 'email';
			if ((strpos($from, '@vzwpix.com') !== false) || (strpos($from, '@vtext.com') !== false) || (strpos($from, '@messaging.sprintpcs.com') !== false) || (strpos($from, '@mymetropcs.com') !== false) || (strpos($from, '@pm.sprint.com') !== false) || (strpos($from, '@tmomail.net') !== false) || (strpos($from, '@sms.myboostmobile.com') !== false) || (strpos($from, '@vtext.com') !== false) || (strpos($from, '@vtext.com') !== false)) {
				// FOR TEXT
				$email_type = 'text';
				$from = explode('@', $from);
				$from = $from[0];
				$client_id = $this->getClientID('cell_phone_trimmed', $from);
			} else {
				// FOR MESSAGE
				$email_type = 'email';
				$client_id = $this->getClientID('email_address', $from);

			}

			// echo $email_type.'<br>'.$client_id.'<br>';

			if ($client_id) {
				if ($email_type == 'email') {
					$uid_exist = $this->checkEmailIfExist($uid);
					$this->load->model('Model_client_emails');
					if (!$uid_exist) {

						echo "<br>Email";
						echo $from;
						echo $body;

						$client_emails = new Model_client_emails();
						$client_emails->client_id = $client_id;
						$client_emails->type = 'from_client';
						$client_emails->subject = $subject;
						$client_emails->message = $body;
						$client_emails->uid = $uid;
						$client_emails->date = $date;
						$client_emails->save();
					}
				} else {
					$uid_exist = $this->checkTextIfExist($uid);
					$this->load->model('Model_client_etexts');
					if (!$uid_exist) {
						if (count($attachments[0]) > 0) {
							foreach ($attachments as $key => $attachment) {
								$filename = $attachment->getFileName();
								if ($filename == 'text_0.txt') {
									$body = $attachment->getData();
								}
							}
						}

						echo "<br>Text";
						echo $from;
						echo $body;

						$client_etexts = new Model_client_etexts();
						$client_etexts->client_id = $client_id;
						$client_etexts->type = 'from_client';
						$client_etexts->title = $subject;
						$client_etexts->text_message = $body;
						$client_etexts->uid = $uid;
						$client_etexts->date = $date;
						$client_etexts->save();
					}
				}

			}

		}
	}

	function getEmailMessages() {
		$server = new Server('imap.gmail.com', 993);
		$server->setAuthentication('Kayci@Creditlynx.com', 'Wrbwrx14!');

		/** @var Message[] $message */
		// $messages = $server->getOrderedMessages(1,1,10);
		$messages = $server->search('ALL', 'desc', 10);

		foreach ($messages as $message) {
			$uid = $message->getUid();
			$from = $message->getAddresses('from');
			$from = $from['address'];
			$subject = $message->getSubject();
			$body = $message->getMessageBody(true);
			// $body = preg_replace('#<no-reply@creditlynx.com (.|\s)+</no-reply@creditlynx.com>#', '', $body);
			$attachments = $message->getAttachments();
			$date = $message->getDate();
			$date = $this->dateParse($date);

			// echo $from . '<br>';
			// if ($from == 'joshuasaubon@gmail.com') {
			$client_id = $this->getClientIDEmail($from);
			if (count($client_id) > 0) {
				foreach ($client_id as $key => $client_) {
					$this->pprint($client_);
					$this->insertUserEmail('client', $uid, $client_['id'], $client_['type'], $subject, $body, $date, $from);
				}
			} else {
				$broker_id = $this->getBrokerIDEmail('email_address', $from);
				if ($broker_id) {
					$this->insertUserEmail('broker', $uid, $broker_id, $subject, $body, $date, $from);
				} else {
					$agent_id = $this->getAgentIDEmail('email_address', $from);
					if ($agent_id) {
						$this->insertUserEmail('agent', $uid, $agent_id, $subject, $body, $date, $from);
					}
				}
			}
			// }

		}
	}

	function insertUserEmail($sender, $uid, $id, $type, $subject, $body, $date, $from) {
		if ($sender == 'client') {
			$uid_exist = $this->checkEmailIfExistClient($uid, $id, $type);
			$this->load->model('Model_client_emails');
			if (!$uid_exist) {

				// echo "<br>Email";
				echo $from;
				// echo $body;

				$client_emails = new Model_client_emails();
				$client_emails->client_id = $id;
				$client_emails->client_type = $type;
				$client_emails->type = 'from_client';
				$client_emails->subject = $subject;
				$client_emails->message = $body;
				$client_emails->uid = $uid;
				$client_emails->date = $date;
				$client_emails->from = $from;
				$client_emails->save();
			}
		} else if ($sender == 'broker') {
			$uid_exist = $this->checkEmailIfExistBroker($uid);
			$this->load->model('Model_broker_emails');
			if (!$uid_exist) {

				echo "<br>Email";
				echo $from;
				echo $body;

				$broker_emails = new Model_broker_emails();
				$broker_emails->broker_id = $id;
				$broker_emails->type = 'from_broker';
				$broker_emails->subject = $subject;
				$broker_emails->message = $body;
				$broker_emails->uid = $uid;
				$broker_emails->date = $date;
				$broker_emails->from = $from;
				$broker_emails->save();
			}
		} else if ($type == 'agent') {
			$agent_id = $this->getAgentIDEmail('email_address', $from);
			if ($agent_id) {
				$uid_exist = $this->checkEmailIfExistAgent($uid);
				$this->load->model('Model_agent_emails');
				if (!$uid_exist) {

					echo "<br>Email";
					echo $from;
					echo $body;

					$agent_emails = new Model_agent_emails();
					$agent_emails->agent_id = $id;
					$agent_emails->type = 'from_agent';
					$agent_emails->subject = $subject;
					$agent_emails->message = $body;
					$agent_emails->uid = $uid;
					$agent_emails->date = $date;
					$agent_emails->from = $from;
					$agent_emails->save();
				}
			}
		}

	}

	function dateParse($date) {
		$date = date('Y-m-d H:i:s', $date);
		return $date;
	}

	function getClientIDEmail($value) {
		$data = $this->fetchRawData("SELECT * FROM view_client_emails WHERE email_address='$value'");
		return $data;
	}

	function getBrokerIDEmail($type, $value) {
		$data = $this->fetchRawData("SELECT broker_id FROM brokers WHERE $type='$value'");
		if (count($data) > 0) {
			return $data[0]['broker_id'];
		} else {
			return false;
		}

	}

	function getAgentIDEmail($type, $value) {
		$data = $this->fetchRawData("SELECT agent_id FROM agents WHERE $type='$value'");
		if (count($data) > 0) {
			return $data[0]['agent_id'];
		} else {
			return false;
		}

	}

	function checkEmailIfExistClient($uid, $id, $type) {
		$data = $this->fetchRawData("SELECT * FROM client_emails WHERE uid=$uid AND client_id='$id' AND client_type='$type'");
		if (count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	function checkEmailIfExistBroker($uid) {
		$data = $this->fetchRawData("SELECT * FROM broker_emails WHERE uid=$uid");
		if (count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	function checkEmailIfExistAgent($uid) {
		$data = $this->fetchRawData("SELECT * FROM agent_emails WHERE uid=$uid");
		if (count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	function checkTextifExist($uid) {
		$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE uid=$uid");
		if (count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	function testEmail() {
		// $data[''] = '';
		// $this->load->view('email_templates/DocuLynx_template');
	}

}
