<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Model_support_tickets');
		$this->load->model('Model_support_ticket_responses');
		$this->checklog(); 
	}

	public function index()
	{
		$data['userdata'] = $this->session->userdata('user_data');
		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['page_title'] = 'Support';
		$data['page_title_s'] = 'Support';
		$data['page_controller'] = 'support';   
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses(); 
		$this->load->view('admin/Support.php',$data); 
	} 

	function saveSupportTicket() { 
		$sender = $this->input->post('sender');
		$sender_id = $this->input->post('sender_id');
		$assigned_to = $this->input->post('assigned_to');
		$status = 'Pending';
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$support_date = date('Y-m-d H:i:s');
 
		$support_tickets = new Model_support_tickets(); 
		$support_tickets->sender = $sender;
		$support_tickets->sender_id = $sender_id;
		$support_tickets->assigned_to = $assigned_to;
		$support_tickets->status = $status;
		$support_tickets->subject = $subject;
		$support_tickets->message = $message;
		$support_tickets->support_date = $support_date;
		$support_tickets->save();

		// redirect(base_url('admin/clients'));
		echo "saved";

	}


	function getTableSupportTickets() {
		// $client_id = $this->input->post('client_id');
		$support_tickets = new Model_support_tickets();
		$support_tickets->selects = ['*',"IF(sender = 'client',(SELECT name FROM clients WHERE client_id=sender_id),IF
	( sender = 'agent', ( SELECT CONCAT(first_name,' ',last_name) FROM agents WHERE agent_id = sender_id ), IF
	( sender = 'broker', ( SELECT CONCAT(first_name,' ',last_name) FROM brokers WHERE broker_id = sender_id ), IF(sender = 'joint',(SELECT name FROM client_joints WHERE client_joint_id=sender_id), '') ) )) as `name`"];
		$data = $support_tickets->searchWithSelect([],'support_id','DESC');
		// $data = $this->fetchArray($data);
		// $this->pprint($data);
	// 	$data = $this->fetchRawData("SELECT *,IF(sender = 'Client',(SELECT name FROM clients WHERE client_id=sender_id),IF
	// ( sender = 'Agent', ( SELECT CONCAT(first_name,' ',last_name) FROM agents WHERE agent_id = sender_id ), IF
	// ( sender = 'Broker', ( SELECT CONCAT(first_name,' ',last_name) FROM brokers WHERE broker_id = sender_id ), '' ) )) as `name` FROM support_tickets ORDER BY support_id DESC");
		echo json_encode($data);
	}

	function deleteSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->delete();
		// $data = $this->db->query("DELETE FROM support_tickets WHERE support_id=$support_id");
		echo 'deleted!';
	}

	
	function getSupportTicket() {
		$support_id = $this->input->post('support_id');
		$support_tickets = new Model_support_tickets();
		$support_ticket_responses = new Model_support_ticket_responses();
		$data['support_tickets'] = $support_tickets->search(['support_id'=>$support_id]);
		$data['support_ticket_responses'] = $support_ticket_responses->search(['support_id'=>$support_id],'response_id','ASC');
		
		// $data['support_tickets'] = $this->fetchRawData("SELECT * FROM support_tickets WHERE support_id=$support_id");
		// $data['support_ticket_responses'] = $this->fetchRawData("SELECT * FROM support_ticket_responses WHERE support_id=$support_id ORDER BY response_id ASC");

		echo json_encode($data);
	}

	function updateSupportTicketStatus() {
		$support_id = $this->input->post('support_id');
		$status = $this->input->post('status');
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->status = $status;
		$support_tickets->save();
		// $data = $this->db->query("UPDATE support_tickets SET status='$status' WHERE support_id=$support_id");
		echo 'updated!';
	}

	
	function saveSupportTicketResponse() { 
		$userdata = $this->session->userdata('user_data');


		$support_id = $this->input->post('support_id');
		$responder = $userdata['login_type'];
		$login_type = $userdata['login_type'];
		$response_date = date('Y-m-d H:i:s');;
		$response = $this->input->post('response');

		$this->load->model('Model_support_ticket_responses');
		$support_ticket_responses = new Model_support_ticket_responses(); 
		$support_ticket_responses->support_id = $support_id;
		$support_ticket_responses->responder = $responder;
		$support_ticket_responses->response_date = $response_date;
		$support_ticket_responses->response = $response; 
		$support_ticket_responses->save();
		echo "saved";
		$support_tickets = new Model_support_tickets();
		$support_tickets->load($support_id);
		$support_tickets->status = $status; 
		 
		if ($login_type == 'Administrator' || $login_type == 'Employee') {
			$support_tickets->status = 'Responded';
			// $data = $this->db->query("UPDATE support_tickets SET status='Responded' WHERE support_id=$support_id");
		} else {
			$support_tickets->status = 'Pending';
			// $data = $this->db->query("UPDATE support_tickets SET status='Pending' WHERE support_id=$support_id");
		}
		$support_tickets->save();

	}


	
	
}
