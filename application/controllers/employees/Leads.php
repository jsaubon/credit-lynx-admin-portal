<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->checklog();
		$this->load->model('Model_task_templates');
		$this->load->model('Model_subscription');
		$this->load->model('Model_gameplan_timeline');
		$this->load->model('Model_creditors_account_type');
		$this->load->model('Model_creditors_recommendations');
		$this->load->model('Model_client_gameplan_points');
		$this->load->model('Model_clients_account_statuses');
		$this->load->model('Model_clients_account_payment_statuses');
		$this->load->model('Model_clients_account_comments');
		$this->load->model('Model_game_plan_accounts');
		$this->load->model('Model_client_gameplan');
		$this->load->model('Model_letter_templates');
		$this->load->model('Model_clients_account_types');
		$this->load->model('Model_clients_account_statuses');
		$this->load->model('Model_client_statuses');
		$this->load->model('Model_clients');
		$this->load->model('Model_client_joints');
		$this->load->model('Model_client_enrollments');
		$this->load->model('Model_client_billings');
		$this->load->model('Model_client_joint_enrollments');
		$this->load->model('Model_client_joint_billings');
		$this->load->model('Model_client_etexts');
		$this->load->model('Model_client_emails');
		$this->load->model('Model_tasks');
		$this->load->model('Model_notes');
		$this->load->model('Model_client_account_details');
		$this->load->model('Model_client_joint_account_details');
		$this->load->model('Model_client_alerts');
		$this->load->model('model_client_alerts_templates');
		$this->load->model('Model_clients_dispute_verbiages_assigned');
		$this->load->model('Model_client_joint_rounds');
		$this->load->model('Model_client_rounds');
		$this->load->model('Model_support_tickets');
		$this->load->model('Model_support_ticket_responses');
		$this->load->model('Model_client_joint_documents');
		$this->load->model('Model_client_documents');
		$this->load->model('Model_letter_creditors');
		$this->load->model('Model_lead_statuses');
		$this->load->model('Model_client_assignments');
		$this->load->model('Model_clients_dispute_verbiages');
		$this->load->model('Model_creditors_account');
		$this->load->model('Model_authorize_transactions');
		$this->load->model('Model_authorize_subscriptions');

	}
	public function index() {
		$get_client_id = $this->input->get('id');
		$get_client_type = $this->input->get('type');
		$data['get_by'] = $this->input->get('by');
		$data['get_order'] = $this->input->get('order');
		$data['go'] = $this->input->get('go');
		$data['filter'] = $this->input->get('filter');
		$data['search'] = $this->input->get('search');
		$userdata['userdata'] = $this->session->userdata('user_data');
		// $data['users'] = $this->getUsers($data['get_by'],$data['get_order']);
		// $data['task_temp'] = $this->TaskTemp();
		$data['userdata'] = $this->session->userdata('user_data');
		$data['all_clients'] = $this->getallUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['subscription_type'] = $this->subscription_type();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Leads';
		$data['page_title_s'] = 'Lead';
		$data['page_controller'] = 'clients';
		$data['page_folder'] = 'clients';
		$get_client_id = $this->input->get('id');
		$get_client_type = $this->input->get('type');
		if ($get_client_type == 'joint') {
			$client_id = $this->getClientId($get_client_id);
			$data['from_email'] = $get_client_id . '_' . $get_client_type . '_' . $client_id;
		} else {
			$data['from_email'] = $get_client_id . '_' . $get_client_type;
		}

		$data['agents'] = $this->getAgents();
		$data['brokers'] = $this->getBrokers();
		$data['sales'] = $this->getSales();
		$data['processors'] = $this->getProcessors();
		$data['clients_account_statuses'] = $this->getAccountStatuses();
		$data['clients_account_types'] = $this->getClientsAccountTypes();
		$data['subscription_type'] = $this->subscription_type();
		$data['note_templates'] = $this->getNoteTemplatesInit();
		$data['task_templates'] = $this->getTaskTemplatesInit();
		$data['alert_templates'] = $this->getAlertTemplatesInit();
		$data['account_types'] = $this->getAccountTypesInit();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['agents'] = $this->getAgents();
		$data['brokers'] = $this->getBrokers();
		$data['sales'] = $this->getSales();
		$data['processors'] = $this->getProcessors();
		$data['lead_statuses'] = $this->getLeadStatuses();
		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses();
		$data['dispute_verbiages'] = $this->getDisputeVerbiages();
		$data['template_headers'] = $this->getTemplateHeaders();

		$this->load->view('admin/Leads.php', $data);

	}
	function getClientsAccountTypes() {
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		return $data;
	}

	function getDisputeVerbiages() {
		$clients_dispute_verbiages = new Model_clients_dispute_verbiages();
		$data = $clients_dispute_verbiages->search([], 'dispute_verbiage', 'ASC');
		return $data;
	}

	function getAccountStatuses() {
		$clients_account_statuses = new Model_clients_account_statuses();
		$data = $clients_account_statuses->search([], 'as_id', 'ASC');
		return $data;
	}

	function testing123() {
		$data['get_by'] = $this->input->get('by');
		$data['get_order'] = $this->input->get('order');
		$userdata['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers($data['get_by'], $data['get_order']);
		// $data['task_temp'] = $this->TaskTemp();
		$data['userdata'] = $this->session->userdata('user_data');
		$data['all_users'] = $this->getallUsers();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['subscription_type'] = $this->subscription_type();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Leads';
		$data['page_title_s'] = 'Lead';
		$data['page_controller'] = 'clients';
		$data['page_folder'] = 'clients';
		$data['agents'] = $this->getAgents();
		$data['brokers'] = $this->getBrokers();
		$data['sales'] = $this->getSales();
		$data['processors'] = $this->getProcessors();
		$data['lead_statuses'] = $this->getLeadStatuses();
		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses();
		$data['template_headers'] = $this->getTemplateHeaders();
		$get_client_id = $this->input->get('id');
		$get_client_type = $this->input->get('type');
		if ($get_client_type == 'joint') {
			$client_id = $this->getClientId($get_client_id);
			$data['from_email'] = $get_client_id . '_' . $get_client_type . '_' . $client_id;
		} else {
			$data['from_email'] = $get_client_id . '_' . $get_client_type;
		}

		$this->load->view('admin/LeadsTest.php', $data);

	}

	function getAccountTypes() {
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		echo json_encode($data);
	}

	public function getTemplateRecords() {
		$id = $this->input->post('task_template_id');
		$task_templates = new Model_task_templates();
		$data = $task_templates->search(['task_template_id' => $id], '', '', 1);
		echo json_encode($data);
	}

	function subscription_type() {
		$subscription = new Model_subscription();
		$data = $subscription->get();
		return $data;
	}

	function getUsers($by, $order) {
		$this->db->where('converted', 0);
		$this->db->where('client_status !=', 'Archived');
		// $data = $this->Model_Query->getView('view_clients_table',[],'client_id','DESC');
		if ($by != '') {
			$data = $this->Model_Query->getView('view_clients_table', [], $by . ',client_id ' . $order);
		} else {
			$data = $this->Model_Query->getView('view_clients_table', [], 'client_id', 'DESC');
		}
		return $data;
	}

	function getallUsers() {
		$this->Model_Query->selects = ['*', 'IF(STR_TO_DATE(monthly_due, \'%d,%m,%Y\') IS NOT NULL,DAY(STR_TO_DATE(monthly_due, \'%d,%m,%Y\')),monthly_due) as `monthly_due_formated`'];
		$data = $this->Model_Query->getView('view_clients_table', ['converted' => 0], 'client_id', 'DESC');
		return $data;
	}

	function getUserCount() {


		$userdata = $this->session->userdata('user_data');
		$login_type = $userdata['login_type'];
		$user_id = $userdata['user_id'];
		$extra_query = '';
		if ($login_type == 'Sales Team') {
			$client_query = " AND client_id IN (SELECT client_id FROM client_assignments WHERE sale_id=$user_id) ";	
			$client_joint_query = " WHERE client_joints.client_id IN (SELECT client_id FROM client_assignments WHERE sale_id=$user_id) ";	
		}

		$data = $this->fetchRawData("SELECT
										sum( user_count ) AS `user_count`
									FROM
										(
									SELECT
										count( client_id ) AS `user_count`
									FROM
										clients
									WHERE
										( ( client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ) OR client_status IS NULL )
										AND converted = 0 $client_query UNION ALL
									SELECT
										count( client_joint_id ) AS `user_count`
									FROM
										client_joints
										INNER JOIN clients ON client_joints.client_id = clients.client_id AND clients.client_type='Joint' AND (clients.client_status <> 'Archived' OR clients.client_status IS NULL) and clients.converted=0 $client_joint_query) AS aaa 
");
		return $data[0]['user_count'];
	}

	function getLeadStatuses() {
		$lead_statuses = new Model_lead_statuses();
		$data = $lead_statuses->search([], 'ls_id', 'ASC');
		// $data = $this->fetchRawData("SELECT * FROM lead_statuses ORDER BY lead_status");
		return $data;
	}
 
}
