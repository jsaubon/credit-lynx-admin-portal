<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {

	public function index()
	{
		$this->checklog();
		$this->load->view('employees/Login.php');
	}
	function loginUser()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = $this->fetchRawData("SELECT * FROM employees WHERE username='$username'");
		$password_hashed = $data[0]['password'];
		$count = count($data);
		if (password_verify($password,$password_hashed)) {
			
			if($count != 0 )
			{
				$date_now = date('Y-m-d H:i:s');
				$user_id = $data[0]['employee_id'];
				$data_update = $this->db->query("UPDATE employees SET last_login='$date_now' WHERE username='$username'");

        		$type = $data[0]['type'];
				$data_insert_online = $this->db->query("INSERT INTO online_users (user_type,user_id) VALUES ('$type',$user_id)");
				echo "Valid";
			 	$data_session=array(
			      'username'=> $data[0]['username'],
			      'name' => $data[0]['name'],
			      'user_id' => $data[0]['employee_id'],
			      'photo' => $data[0]['photo'],
			      'login_type' => $data[0]['type'],
			      'login_folder' => 'employees',

					'subscription_type' => $this->subscription_type(),
					'lead_statuses' => $this->getLeadStatuses(),
					'client_statuses' => $this->getClientStatuses(),
					'agents' => $this->getAgents(),
					'brokers' => $this->getBrokers(),
					'sales' => $this->getSales(),
					'processors' => $this->getProcessors(),
					'state_provinces' => $this->getStateProvinces(),
					'getNoteTemplatesInit' => $this->getNoteTemplatesInit()
		      	);
			      $this->session->set_userdata(array('user_data' => $data_session));
			      
			}
			else
			{
				echo $count;
			}
		} else {
			echo $count;
		}
	}

	
	function getLeadStatuses() {
		$this->load->model('Model_lead_statuses');
		$lead_statuses = new Model_lead_statuses();
		$data = $lead_statuses->search([], 'ls_id', 'ASC');
		return $data;
	}

	function getClientStatuses() {
		$this->load->model('Model_client_statuses');
		$client_statuses = new Model_client_statuses();
		$data = $client_statuses->search([], 'client_status', 'ASC');
		return $data;
	}


	function subscription_type() {

		$this->load->model('Model_subscription');
		$subscription = new Model_subscription();
		$data = $subscription->get();
		return $data;
	}

  	function logout(){
  		$userdata = $this->session->userdata('user_data');
  		$user_id = $userdata['user_id'];
  		$login_type = $userdata['login_type'];
  		if (isset($login_type)) {
  			$data_insert_online = $this->db->query("DELETE FROM online_users WHERE user_id=$user_id AND user_type='$login_type'");
	  		
  		}
  		session_destroy();
    	redirect(base_url('employees/login'));
    	// echo "string";
  	}
 

     
}