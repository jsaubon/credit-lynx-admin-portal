<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller { 
	
	public function index()
	{
		$this->checklog(); 
		$userdata['userdata'] = $this->session->userdata('user_data');
		$data['userdata'] = $this->session->userdata('user_data');
		$data['users'] = $this->getUsers();
		$data['subscription_type'] = $this->subscription_type();
		$data['all_clients'] = $this->getAllClients();
		$data['state_provinces'] = $this->getStateProvinces();
		$data['user_count'] = $this->getUserCount();
		$data['page_title'] = 'Clients';
		$data['page_title_s'] = 'Client';
		$data['page_controller'] = 'clients';  
		$data['page_folder'] = 'clients';
		$data['agents'] = $this->getAgents();
		$data['brokers'] = $this->getBrokers();
		$data['sales'] = $this->getSales();
		$data['processors'] = $this->getProcessors();
		$data['client_statuses'] = $this->getClientStatuses(); 
		// $data['clients_account_types'] = $this->getAccountTypes(); 
		$data['clients_account_statuses'] = $this->getAccountStatuses(); 
		$data['support_responses'] = $this->getSupportTicketResponses();
		$data['dispute_team_responses'] = $this->getDisputeTeamResponses();
		$data['billing_team_responses'] = $this->getBillingTeamResponses();
		$data['support_team_responses'] = $this->getSupportTeamResponses();
		$data['sales_team_responses'] = $this->getSalesTeamResponses();
		// $data['clients_dispute_verbiages'] = $this->getDisputeVerbiages(); 
		$data['template_headers'] = $this->getTemplateHeaders();
		$get_client_id = $this->input->get('id');
		$get_client_type = $this->input->get('type'); 
		if ($get_client_type == 'joint') {
			$client_id = $this->getClientId($get_client_id);
			$data['from_email'] = $get_client_id.'_'.$get_client_type.'_'.$client_id;
		} else {
			$data['from_email'] = $get_client_id.'_'.$get_client_type;
		}
		 
		$this->load->view('admin/Clients.php',$data); 

	}

	function subscription_type(){
		// $data = $this->fetchRawData("SELECT * FROM subscription ORDER BY subscription_id ASC");
		$data = $this->db->get('subscription');
		return $data;
		// print_r($data);
		// if ($data->num_rows() > 0) {
		// 	foreach ($data->result() as $key => $value) {
		// 		echo $value->name;
		// 	}
		// }
	}

	function getAllClients() {
		$data = $this->fetchRawData("SELECT * FROM view_clients_table WHERE converted=1 ORDER BY client_id DESC");
		return $data;
	}

	


	

	function getTemplateTitles() {
		$template_header = $this->input->post('template_header');
		$data = $this->fetchRawData("SELECT template_title FROM letter_templates WHERE template_header='$template_header'");
		echo json_encode($data);
	} 

	function getTemplate() {
		$template_title = $this->input->post('template_title');
		$data = $this->fetchRawData("SELECT template FROM letter_templates WHERE template_title='$template_title'");
		echo json_encode($data);
	} 

	function getAccountTypes() {
		$data = $this->fetchRawData("SELECT * FROM clients_account_types ORDER BY account_type ASC");
		echo json_encode($data);
	} 

	function getAccountStatuses() {
		$data = $this->fetchRawData("SELECT * FROM clients_account_statuses ORDER BY as_id ASC");
		return $data;
	} 

	function getDisputeVerbiagesAccountTypes() {
		$data = $this->fetchRawData("SELECT * FROM view_clients_dispute_verbiages_assigned ORDER BY dv_id DESC");
		echo json_encode($data);
	}


 

	function getClientStatuses() {
		$data = $this->fetchRawData("SELECT * FROM client_statuses WHERE client_status <> 'Archived' ORDER BY client_status ASC");
		return $data;
	}

	function getUsers() {
		$data = $this->fetchRawData("SELECT * FROM view_clients_table WHERE ((client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ) OR  client_status IS NULL) AND converted=1 ORDER BY client_id DESC");
		return $data;
	}

	function getUserCount() {
		$data = $this->fetchRawData("SELECT
										sum( user_count ) AS `user_count` 
									FROM
										(
									SELECT
										count( client_id ) AS `user_count` 
									FROM
										clients 
									WHERE
										( ( client_status <> 'Cancelled' AND client_status <> 'Complete' AND client_status <> 'Archived' ) OR client_status IS NULL ) 
										AND converted = 1 UNION ALL
									SELECT
										count( client_joint_id ) AS `user_count` 
									FROM
										client_joints
										INNER JOIN clients ON client_joints.client_id = clients.client_id AND clients.client_type='Joint' AND (clients.client_status <> 'Archived' OR clients.client_status IS NULL) and clients.converted=1) AS aaa 
									");
		return $data[0]['user_count'];
	}

	

	function saveDetail() {
		// $this->pprint($this->input->post());
		$converted = $this->input->post('converted');
		$client_id = $this->input->post('client_id');
		$agent_id = $this->input->post('agent_id');
		$broker_id = $this->input->post('broker_id');
		$sale_id = $this->input->post('sale_id');
		$processor_id = $this->input->post('processor_id');

		$client_status = $this->input->post('client_status');
		$lead_status = $this->input->post('lead_status');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state_province = $this->input->post('state_province');
		$zip_postal_code = $this->input->post('zip_postal_code');  
		$alt_phone = $this->input->post('alt_phone');
		$cell_phone = $this->input->post('cell_phone');
		$carrier = $this->input->post('carrier');
		$fax = $this->input->post('fax');
		$email_address = $this->input->post('email_address');
		$ss = $this->input->post('ss');
		$date_of_birth = $this->input->post('date_of_birth');
		$date_for_result = $this->input->post('date_for_result');
		$username = $this->input->post('username');
		$password = $this->input->post('password'); 
		$password = password_hash($password, PASSWORD_BCRYPT);

		$joint_name = $this->input->post('joint_name');
		$joint_cell_phone = $this->input->post('joint_cell_phone');
		$joint_email_address = $this->input->post('joint_email_address');
		$joint_ss = $this->input->post('joint_ss');
		$joint_date_of_birth = $this->input->post('joint_date_of_birth');
		$joint_username = $this->input->post('joint_username');
		$joint_password = $this->input->post('joint_password');
		$joint_password = password_hash($joint_password, PASSWORD_BCRYPT);
		$client_type = 'Single';
		if ($joint_name != '') {
			$client_type = 'Joint';
		}

 
		$font_array  = array('Georgia, serif','"Palatino Linotype", "Book Antiqua", Palatino, serif','"Times New Roman", Times, serif','Arial, Helvetica, sans-serif','"Arial Black", Gadget, sans-serif','"Comic Sans MS", cursive, sans-serif','Impact, Charcoal, sans-serif','"Lucida Sans Unicode", "Lucida Grande", sans-serif','Tahoma, Geneva, sans-serif','"Trebuchet MS", Helvetica, sans-serif','Verdana, Geneva, sans-serif','"Courier New", Courier, monospace','"Lucida Console", Monaco, monospace');
		$font = $font_array[rand(0,12)];





 
	    $this->load->model('Model_clients');
	    $clients = new Model_clients();
	    if ($client_id != '') {
	    	$clients->client_id = $client_id;
	    } 
	    $clients->client_type = $client_type; 
		$clients->client_status = 'Hold'; 
		$clients->lead_status = 'Attempting Contact'; 
		$clients->name = $name; 
		$clients->address = $address;
		$clients->city = $city;
		$clients->state_province = $state_province;
		$clients->zip_postal_code = $zip_postal_code;  
		$clients->alt_phone = $alt_phone;
		$clients->cell_phone = $cell_phone;
		$clients->carrier = $carrier;
		$clients->fax = $fax;
		$clients->email_address = $email_address;
		$clients->ss = $ss;
		$clients->date_of_birth = $date_of_birth;
		$clients->date_for_result = $date_for_result;
		$clients->username = $username;
		$clients->password = $password; 
		$clients->font = $font;
		$clients->date_created = date('Y-m-d H:i:s');
		$clients->converted = $converted;
		if ($converted == 1) {
			$clients->converted_date = date('Y-m-d H:i:s');
		}

	    $clients->save();

	    if ($client_type == 'Joint') {
	    	$this->load->model('Model_client_joints');
		    $client_joints = new Model_client_joints();

		    $client_joints->client_id = $clients->client_id;
			$client_joints->name = $joint_name;
			$client_joints->cell_phone = $joint_cell_phone;
			$client_joints->email_address = $joint_email_address;
			$client_joints->ss = $joint_ss;
			$client_joints->date_of_birth = $joint_date_of_birth;
			$client_joints->username = $joint_username;
			$client_joints->password = $joint_password;
			$client_joints->save();
	    }
		    

		$this->load->model('Model_client_assignments');
	    $client_assignments = new Model_client_assignments();
	    $client_assignments->client_id = $clients->client_id;
		$client_assignments->agent_id = $agent_id;
		$client_assignments->broker_id = $broker_id;
		$client_assignments->sale_id = $sale_id;
		$client_assignments->processor_id = $processor_id;
		$client_assignments->save();


		$page_title = $this->input->post('page_title');
		if ($page_title == 'Leads') {
			redirect(base_url('employees/leads'));
		} else {
			redirect(base_url('employees/clients'));	
		}

	    

	}

	public function ConvertToJointAccount(){

		# client id data
		$id = $this->input->post('profile_client_id');

		#update client type query
		$update = array('client_type' => 'Joint');
		$this->db->where('client_id', $id);
		$this->db->update('clients', $update);
		
		# Client joints query


		$this->load->model('Model_client_joints');
	    $client_joints = new Model_client_joints();

	    $client_joints->client_id = $id;
		$client_joints->name = $this->input->post('joint_name');
		$client_joints->cell_phone = $this->input->post('joint_cell_phone');
		$client_joints->email_address = $this->input->post('joint_email_address');
		$client_joints->ss = $this->input->post('joint_ss');
		$client_joints->date_of_birth = $this->input->post('joint_date_of_birth');
		$client_joints->username = $this->input->post('joint_username');
		$client_joints->password = $this->input->post('joint_password');
		$client_joints->save();
		redirect(base_url('employees/clients'));
	}

	function checkIfUsernameExist() {
		$username = $this->input->post('username_val');
		$type = $this->input->post('type');
		if ($type == 'joint') {
			$data = $this->fetchRawData("SELECT * FROM client_joints WHERE username='$username'");
		} else {
			$data = $this->fetchRawData("SELECT * FROM clients WHERE username='$username'");
		}
		
		echo count($data); 
	}


	function getProfile() {
		$client_id = $this->input->post('client_id');
		$type = $this->input->post('type');
		if ($type == 'client') {
			$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_clients WHERE client_id = $client_id");
			$data['enrollment'] = $this->fetchRawData("SELECT * FROM client_enrollments WHERE client_id = $client_id");
			$data['billing'] = $this->fetchRawData("SELECT * FROM client_billings WHERE client_id = $client_id");
		} else {
			$client_joint_id = $client_id;
			$data['profile'] = $this->fetchRawData("SELECT * ,RIGHT(ss,4) as `ss_restricted`  FROM view_client_joints WHERE client_joint_id = $client_id");
			$data['enrollment'] = $this->fetchRawData("SELECT * FROM client_joint_enrollments WHERE client_joint_id = $client_joint_id");
			$data['billing'] = $this->fetchRawData("SELECT * FROM view_client_joint_billings WHERE client_joint_id = $client_id");
		}	

		$client_id = $data['profile'][0]['client_id'];
		
		
		$this->updateEmailETextRead($client_id);
		echo json_encode($data);
		
	} 

	function updateEmailETextRead($client_id) {
		$this->db->query("UPDATE client_etexts SET unread=0 WHERE client_id=$client_id");
		$this->db->query("UPDATE client_emails SET unread=0 WHERE client_id=$client_id");
	}

	function saveTask() {
		$client_id = $this->input->post('client_id');
		$task = $this->input->post('task');
		$task_date = $this->input->post('task_date');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_tasks');
	    $tasks = new Model_tasks();
 
		$tasks->task_assigned = 'Clients';
		$tasks->task_assigned_id = $client_id;
		$tasks->sender = $sender;
		$tasks->sender_id = $sender_id;
		$tasks->sender_photo = $sender_photo;
		$tasks->sender_name = $sender_name;
		$tasks->task = $task;
		$tasks->task_date = $task_date;
		$tasks->task_active = 0;
		$tasks->save();
	}

	function getTasks() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM tasks WHERE task_assigned_id=$client_id AND task_assigned='Clients' AND task_active <> 2 ORDER BY task_id DESC");
		echo json_encode($data);
	}

	function updateTask() {
		$task_id = $this->input->post('task_id');
		$task_active = $this->input->post('task_active');
		$data = $this->db->query("UPDATE tasks SET task_active=$task_active WHERE task_id=$task_id");
		echo "updated";
	}

	function saveNote() {
		$client_id = $this->input->post('client_id');
		$note = $this->input->post('note');
		$note_date = $this->input->post('note_date');
		$note_sticky = $this->input->post('note_sticky');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_notes');
	    $notes = new Model_notes();
 
		$notes->note_assigned = 'Clients';
		$notes->note_assigned_id = $client_id;
		$notes->sender = $sender;
		$notes->sender_id = $sender_id;
		$notes->sender_photo = $sender_photo;
		$notes->sender_name = $sender_name;
		$notes->note = $note;
		$notes->note_date = date('Y-m-d H:i:s');
		$notes->note_sticky = $note_sticky;
		$notes->save();
	}

	function getNotes() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM notes WHERE note_assigned_id=$client_id AND note_assigned='Clients' ORDER BY note_id DESC");
		echo json_encode($data);
	}

	function updateNote() {
		$note_id = $this->input->post('note_id');
		$note_sticky = $this->input->post('note_sticky');
		$data = $this->db->query("UPDATE notes SET note_sticky=$note_sticky WHERE note_id=$note_id");
		echo "updated";
	}

	function updateFields() {
		$table = $this->input->post('table');
		$client_id = $this->input->post('client_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($field == 'password') {
			$value = password_hash($value,PASSWORD_BCRYPT);
		}

		
	  	
	  	if ($table == 'client_joint_billings') {
			$data_check = $this->fetchRawData("SELECT client_joint_id FROM $table WHERE client_joint_id=$client_id");
			if (count($data_check) == 0) {
				$data_insert = $this->db->query("INSERT INTO $table (client_joint_id) VALUES ($client_id)");
			} 
	 

			$data = $this->db->query("UPDATE $table SET $field='$value' WHERE client_joint_id=$client_id");
			echo "UPDATE $table SET $field='$value' WHERE client_joint_id=$client_id";
		} else if ($table == 'client_joint_enrollments') {
			$data_check = $this->fetchRawData("SELECT client_joint_id FROM $table WHERE client_joint_id=$client_id");
			if (count($data_check) == 0) {
				$data_insert = $this->db->query("INSERT INTO $table (client_joint_id) VALUES ($client_id)");
			} 
	 

			$data = $this->db->query("UPDATE $table SET $field='$value' WHERE client_joint_id=$client_id");
			echo "UPDATE $table SET $field='$value' WHERE client_joint_id=$client_id";
		} else if ($table != 'client_joints') {
			$data_check = $this->fetchRawData("SELECT client_id FROM $table WHERE client_id=$client_id");
			if (count($data_check) == 0) {
				$data_insert = $this->db->query("INSERT INTO $table (client_id) VALUES ($client_id)");
			} 
	 

			$data = $this->db->query("UPDATE $table SET $field='$value' WHERE client_id=$client_id");
			echo "UPDATE $table SET $field='$value' WHERE client_id=$client_id";
		} else {
			$data = $this->db->query("UPDATE $table SET $field='$value' WHERE client_joint_id=$client_id");
		}

			
		echo "updated!";
		// echo count($data_check);

	}

	function updateFieldsAccounts() {
		$table = $this->input->post('table');
		$account_id = $this->input->post('account_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		 
		$data = $this->db->query("UPDATE $table SET $field='$value' WHERE account_id=$account_id");
		

			
		echo "updated!";
		// echo count($data_check);

	}

	function sendClientEmail() {
		$client_id = $this->input->post('client_id');
		$subject = $this->input->post('subject');
		$notes = $this->input->post('notes'); 
		$client_type = $this->input->post('client_type');
		$type = $this->input->post('type');
		$this->sendEmail('Client',$type,$client_id,$subject,$notes,date('Y-m-d H:i:s'),$client_type);
	}

	function saveAlert() {
		$client_id = $this->input->post('client_id');
		$alert_subject = $this->input->post('alert_subject');
		$alert_notes = $this->input->post('alert_notes'); 
		$client_type = $this->input->post('client_type');

		$userdata = $this->session->userdata('user_data');
		$sender = $userdata['login_folder'];
		$sender_id = $userdata['user_id'];
		$sender_photo = $userdata['photo'];
		$sender_name = $userdata['name'];
		$this->load->model('Model_client_alerts');
	    $client_alerts = new Model_client_alerts();
  
		$client_alerts->client_id = $client_id;
		$client_alerts->sender = $sender;
		$client_alerts->sender_id = $sender_id;
		$client_alerts->sender_photo = $sender_photo;
		$client_alerts->sender_name = $sender_name;
		$client_alerts->alert_subject = $alert_subject;
		$client_alerts->alert_notes = $alert_notes; 
		$client_alerts->alert_date = date('Y-m-d H:i:s'); 
		$client_alerts->save();

		// $this->sendEmail('Client','Email',$client_id,$alert_subject,$alert_notes,$client_alerts->alert_date,$client_type);
		// $this->sendEmail('Client','Phone',$client_id,$alert_subject,$alert_notes,$client_alerts->alert_date,$client_type);
	}

	function getAlerts() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT *,(SELECT SUBSTRING_INDEX(name, ' ', 1) FROM clients WHERE clients.client_id = client_alerts.client_id) as `client_name` FROM client_alerts WHERE client_id=$client_id ORDER BY alert_id DESC");
		echo json_encode($data);
	}

	function saveTemplate() {
		$alert_template_id = $this->input->post('alert_template_id');
		$template_shortname = $this->input->post('template_shortname');
		$template_subject = $this->input->post('template_subject');
		$template_notes = $this->input->post('template_notes');

		$this->load->model('Model_client_alerts_templates');
	    $client_alerts_templates = new Model_client_alerts_templates();


	    if ($alert_template_id != '') {
	    	$client_alerts_templates->alert_template_id = $alert_template_id;
	    }
	    $client_alerts_templates->template_shortname = $template_shortname;
		$client_alerts_templates->template_subject = $template_subject;
		$client_alerts_templates->template_notes = $template_notes;

		$client_alerts_templates->save();



	}

	function getAlertTemplates() {
		$data = $this->fetchRawData("SELECT * FROM client_alerts_templates ORDER BY template_shortname ASC");
		echo json_encode($data);
	}

	function saveAccountType() {
		$at_id = $this->input->post('at_id');
		$account_type = $this->input->post('account_type');

		$this->load->model('Model_clients_account_types');
		$account_types = new Model_clients_account_types();
		if ($at_id != '') {
			$account_types->at_id = $at_id;
		}

		$account_types->account_type = $account_type;
		$account_types->save();
		echo $account_types->at_id;
	}

	function saveDisputeVerbiage() {
		$dv_id = $this->input->post('dv_id');
		$dispute_verbiage = $this->input->post('dispute_verbiage');
		$bank_account_type_for_dispute = $this->input->post('bank_account_type_for_dispute');
		// echo json_encode($bank_account_type_for_dispute);

		$this->load->model('Model_clients_dispute_verbiages');
		$dispute_verbiages = new Model_clients_dispute_verbiages();
		if ($dv_id != '') {
			$dispute_verbiages->dv_id = $dv_id;
		}

		$dispute_verbiages->dispute_verbiage = $dispute_verbiage;
		$dispute_verbiages->save();

		$delete_assigned = $this->db->query("DELETE FROM clients_dispute_verbiages_assigned WHERE dv_id=$dv_id");
		$this->load->model('Model_clients_dispute_verbiages_assigned');
		foreach ($bank_account_type_for_dispute as $key => $value) {
			$clients_dispute_verbiages_assigned = new Model_clients_dispute_verbiages_assigned();
			$clients_dispute_verbiages_assigned->dv_id = $dv_id;
			$clients_dispute_verbiages_assigned->at_id = $value;
			$clients_dispute_verbiages_assigned->save();
		}
	}


	function getDisputeVerbiagesAccountTypesBy_account_type() {
		$account_type = $this->input->post('account_type');
		$data = $this->fetchRawData("SELECT
										`aaa`.`dv_id` AS `dv_id`,
										`aaa`.`dispute_verbiage` AS `dispute_verbiage`,
										bbb.at_id
									FROM
										(
											( `clients_dispute_verbiages` `aaa` LEFT JOIN `clients_dispute_verbiages_assigned` `bbb` ON ( ( `aaa`.`dv_id` = `bbb`.`dv_id` ) ) )
											LEFT JOIN `clients_account_types` `ccc` ON ( ( `bbb`.`at_id` = `ccc`.`at_id` ) ) 
										)  WHERE ccc.account_type = '$account_type' GROUP BY dispute_verbiage ORDER BY dispute_verbiage ASC");
		echo json_encode($data);
	}

	function saveNewAccount() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		$equifax = $this->input->post('equifax');
		$experian = $this->input->post('experian');
		$transunion = $this->input->post('transunion');
		$account_type = $this->input->post('account_type');
		$account_name = $this->input->post('account_name');
		$account_number = $this->input->post('account_number');
		$amount = $this->input->post('amount'); 
		$dispute_verbiage = $this->input->post('dispute_verbiage');
		$bureau_date = $this->input->post('bureau_date');

		$past_due = $this->input->post('past_due');
		$credit_limit = $this->input->post('credit_limit');
		$file_date = $this->input->post('file_date');
		$chapter = $this->input->post('chapter');
		$correct_info = $this->input->post('correct_info');
		$inquiry_date = $this->input->post('inquiry_date');
		$date_added = date('Y-m-d H:i:s');
		if ($client_type == 'joint') {
			$client_joint_id = $client_id;
			if ($equifax == 'true') { 
				$this->saveJointNewAccountDetail($client_joint_id,'Equifax',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}
			if ($experian == 'true') { 
				$this->saveJointNewAccountDetail($client_joint_id,'Experian',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}
			if ($transunion == 'true') { 
				$this->saveJointNewAccountDetail($client_joint_id,'TransUnion',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}  
		} else {
			if ($equifax == 'true') { 
				$this->saveNewAccountDetail($client_id,'Equifax',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}
			if ($experian == 'true') { 
				$this->saveNewAccountDetail($client_id,'Experian',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}
			if ($transunion == 'true') { 
				$this->saveNewAccountDetail($client_id,'TransUnion',$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$bureau_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added);
			}  
		}
			
	}

	function saveNewAccountDetail($client_id,$bureaus,$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$dispute_1_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added) {
		$this->load->model('Model_client_account_details');
		$client_account_details = new Model_client_account_details();
		$client_account_details->bureaus = $bureaus;
		$client_account_details->client_id = $client_id; 
		$client_account_details->account_type = $account_type;
		$client_account_details->account_name = $account_name;
		$client_account_details->account_number = $account_number;
		$client_account_details->amount = $amount;  
		$client_account_details->past_due = $past_due;
		$client_account_details->credit_limit = $credit_limit;
		$client_account_details->file_date = $file_date;
		$client_account_details->chapter = $chapter;
		$client_account_details->correct_info = $correct_info;
		$client_account_details->inquiry_date = $inquiry_date;
		$client_account_details->date_added = $date_added;
		$client_account_details->save();


		$this->load->model('Model_client_rounds');
		$client_rounds = new Model_client_rounds();
		$client_rounds->account_id = $client_account_details->account_id;
		$client_rounds->dispute_1 = $dispute_verbiage;
		$client_rounds->dispute_1_date = $dispute_1_date;
		$client_rounds->save();

	}
	function saveJointNewAccountDetail($client_joint_id,$bureaus,$account_type,$account_name,$account_number,$amount,$dispute_verbiage,$dispute_1_date,$past_due,$credit_limit,$file_date,$chapter,$correct_info,$inquiry_date,$date_added) {
		$this->load->model('Model_client_joint_account_details');
		$client_joint_account_details = new Model_client_joint_account_details();
		$client_joint_account_details->bureaus = $bureaus;
		$client_joint_account_details->client_joint_id = $client_joint_id; 
		$client_joint_account_details->account_type = $account_type;
		$client_joint_account_details->account_name = $account_name;
		$client_joint_account_details->account_number = $account_number;
		$client_joint_account_details->amount = $amount;  
		$client_joint_account_details->past_due = $past_due;
		$client_joint_account_details->credit_limit = $credit_limit;
		$client_joint_account_details->file_date = $file_date;
		$client_joint_account_details->chapter = $chapter;
		$client_joint_account_details->correct_info = $correct_info;
		$client_joint_account_details->inquiry_date = $inquiry_date;
		$client_joint_account_details->date_added = $date_added;
		$client_joint_account_details->save();


		$this->load->model('Model_client_joint_rounds');
		$client_joint_rounds = new Model_client_joint_rounds();
		$client_joint_rounds->account_id = $client_joint_account_details->account_id;
		$client_joint_rounds->dispute_1 = $dispute_verbiage;
		$client_joint_rounds->dispute_1_date = $dispute_1_date;
		$client_joint_rounds->save();

	}

	function getResulsTracker() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_id = $client_id;
			$data = $this->fetchRawData("SELECT * FROM client_joint_account_details WHERE client_joint_id=$client_joint_id GROUP BY IF(date_added IS NULL,account_number,date_added) ORDER BY account_id ASC");
		} else {
			$data = $this->fetchRawData("SELECT * FROM client_account_details WHERE client_id=$client_id GROUP BY IF(date_added IS NULL,account_number,date_added) ORDER BY account_id ASC");
		}
		


		echo json_encode($data);
	}

	function getAccountDetailsByAccountNumber() {
		$client_type = $this->input->post('client_type');
		$account_number = $this->input->post('account_number');
		$credit_limit = $this->input->post('credit_limit');
		$date_added = $this->input->post('date_added');

		$extra_q = '';
		if ($date_added != 'null') {
			$extra_q = "AND date_added='$date_added'";
		}

		if ($client_type == 'joint') {
			$data = $this->fetchRawData("SELECT client_joint_account_details.*,dispute_1,dispute_2,dispute_3,dispute_4,dispute_5,dispute_6,dispute_7,dispute_8,dispute_9,result_1,result_2,result_3,result_4,result_5,result_6,result_7,result_8,result_9,dispute_1_date,dispute_2_date,dispute_3_date,dispute_4_date,dispute_5_date,dispute_6_date,dispute_7_date,dispute_8_date,dispute_9_date FROM client_joint_account_details LEFT JOIN client_joint_rounds ON client_joint_account_details.account_id = client_joint_rounds.account_id WHERE client_joint_account_details.account_number='$account_number' $extra_q  ORDER BY account_id");
		} else {
			$data = $this->fetchRawData("SELECT client_account_details.*,dispute_1,dispute_2,dispute_3,dispute_4,dispute_5,dispute_6,dispute_7,dispute_8,dispute_9,result_1,result_2,result_3,result_4,result_5,result_6,result_7,result_8,result_9,dispute_1_date,dispute_2_date,dispute_3_date,dispute_4_date,dispute_5_date,dispute_6_date,dispute_7_date,dispute_8_date,dispute_9_date FROM client_account_details LEFT JOIN client_rounds ON client_account_details.account_id = client_rounds.account_id WHERE client_account_details.account_number='$account_number' $extra_q  ORDER BY account_id");
		}
		
 

		$data_arr['data'] = $data;
		$data_arr['clients_account_statuses'] = $this->getClientAccountStatuses();
		$data_arr['account_types'] = $this->fetchRawData("SELECT * FROM clients_account_types ORDER BY account_type ASC");

		echo json_encode($data_arr);
		// $this->pprint($data);
	}
 

	function getClientAccountStatuses() {
		$data = $this->fetchRawData("SELECT * FROM clients_account_statuses ORDER BY as_id ASC");
		return $data;
	}

	function changeClientAccountStatus() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$account_status = $this->input->post('account_status');
		if ($client_type == 'joint') {
			$update = $this->db->query("UPDATE client_joint_account_details SET account_status='$account_status' WHERE account_id='$account_id'" );
		} else {
			$update = $this->db->query("UPDATE client_account_details SET account_status='$account_status' WHERE account_id='$account_id'" );
		}
		

	}

	function changeClientAccountType() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$account_type = $this->input->post('account_type');
		if ($client_type == 'joint') {
			$update = $this->db->query("UPDATE client_joint_account_details SET account_type='$account_type' WHERE account_id='$account_id'" );
		} else {
			$update = $this->db->query("UPDATE client_account_details SET account_type='$account_type' WHERE account_id='$account_id'" );
		}
		

	}

	 
  

	function changeClientNote() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$note = $this->input->post('note');

		if ($client_type == 'joint') {
			$update = $this->db->query("UPDATE client_joint_account_details SET note='$note' WHERE account_id='$account_id'" );
		} else {
			$update = $this->db->query("UPDATE client_account_details SET note='$note' WHERE account_id='$account_id'" );	
		}
		

	}

	
	function updateClientRounds() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($client_type == 'joint') {
			$exist_data = $this->fetchRawData("SELECT * FROM client_joint_rounds WHERE account_id=$account_id");

			if (count($exist_data) > 0) {
				$update = $this->db->query("UPDATE client_joint_rounds SET $field='$value' WHERE account_id='$account_id'" );
			} else {
				$this->load->model('Model_client_joint_rounds');
				$client_joint_rounds = new Model_client_joint_rounds();
				$client_joint_rounds->account_id = $account_id;
				$client_joint_rounds->save();

				$update = $this->db->query("UPDATE client_rounds SET $field='$value' WHERE account_id='$account_id'" );
			}
		} else {
			$exist_data = $this->fetchRawData("SELECT * FROM client_rounds WHERE account_id=$account_id");

			if (count($exist_data) > 0) {
				$update = $this->db->query("UPDATE client_rounds SET $field='$value' WHERE account_id='$account_id'" );
			} else {
				$this->load->model('Model_client_rounds');
				$client_rounds = new Model_client_rounds();
				$client_rounds->account_id = $account_id;
				$client_rounds->save();

				$update = $this->db->query("UPDATE client_rounds SET $field='$value' WHERE account_id='$account_id'" );
			}
		}

			

		

	}

	function removeAccount() {
		$client_type = $this->input->post('client_type');
		$account_id = $this->input->post('account_id');
		if ($client_type == 'joint') {
			$delete_data = $this->db->query("DELETE FROM client_joint_account_details WHERE account_id=$account_id");
		} else {
			$delete_data = $this->db->query("DELETE FROM client_account_details WHERE account_id=$account_id");	
		}
		
		echo "deleted!";
	}


	function changeCardSent() {
		$table = $this->input->post('table'); 
		$value = $this->input->post('value');
		$where = $this->input->post('where');
		$id = $this->input->post('id');

		$update_data = $this->db->query("UPDATE $table SET card_sent='$value' WHERE $where=$id");


	}

	function getFixedDeletedNotReporting() {
		$client_id = $this->input->post('client_id');

		$total_data = $this->fetchRawData("SELECT 
													SUM(IF(bureaus = 'Equifax',1,0)) as `total_equifax`,
													SUM(IF(bureaus = 'Experian',1,0)) as `total_experian`,
													SUM(IF(bureaus = 'TransUnion',1,0)) as `total_transunion`,
													COUNT(account_id) as `total_accounts`
												FROM
													client_account_details 
												WHERE
													client_id = $client_id  ");
		$total_accounts = $total_data[0]['total_accounts'];
		$count_accounts = $this->fetchRawData("SELECT 
												IF(SUM(IF(bureaus = 'Equifax',1,0)) IS NULL, 0 , SUM(IF(bureaus = 'Equifax',1,0))) as `fixed_equifax`,
												IF(SUM(IF(bureaus = 'Experian',1,0)) IS NULL, 0 , SUM(IF(bureaus = 'Experian',1,0))) as `fixed_experian`,
												IF(SUM(IF(bureaus = 'TransUnion',1,0)) IS NULL, 0 , SUM(IF(bureaus = 'TransUnion',1,0))) as `fixed_transunion`,
												COUNT(account_id) as `fixed_accounts`
												FROM
													client_account_details 
												WHERE
													client_id = $client_id 
													AND ( account_status = 'Fixed' OR account_status = 'Deleted' OR account_status = 'Not Reporting' )");
		
		$data['total_equifax'] = $total_data[0]['total_equifax'];
		$data['total_experian'] = $total_data[0]['total_experian'];
		$data['total_transunion'] = $total_data[0]['total_transunion'];
		$data['total_accounts'] = $total_data[0]['total_accounts'];
		$data['fixed_equifax'] = $count_accounts[0]['fixed_equifax'];
		$data['fixed_experian'] = $count_accounts[0]['fixed_experian'];
		$data['fixed_transunion'] = $count_accounts[0]['fixed_transunion'];
		$data['fixed_accounts'] = $count_accounts[0]['fixed_accounts'];

		echo json_encode($data);

	}

	function getTableByAccount() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT account_name,account_number,account_type,GROUP_CONCAT(bureaus) as `bureaus`,GROUP_CONCAT(account_status) as `account_status` FROM client_account_details WHERE client_id=$client_id GROUP BY account_number ORDER BY account_name");
		echo json_encode($data);
	}

	function getTableByBureaus() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM client_account_details WHERE client_id=$client_id ORDER BY account_name");
		echo json_encode($data);
	}

	function saveSupportTicket() { 
		$sender = $this->input->post('sender');
		$sender_id = $this->input->post('sender_id');
		$assigned_to = $this->input->post('assigned_to');
		$status = 'Pending';
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$support_date = date('Y-m-d H:i:s');

		$this->load->model('Model_support_tickets');
		$support_tickets = new Model_support_tickets(); 
		$support_tickets->sender = $sender;
		$support_tickets->sender_id = $sender_id;
		$support_tickets->assigned_to = $assigned_to;
		$support_tickets->status = $status;
		$support_tickets->subject = $subject;
		$support_tickets->message = $message;
		$support_tickets->support_date = $support_date;
		$support_tickets->save();

		// redirect(base_url('employees/clients'));
		echo "saved";

	}


	function getTableSupportTickets() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM support_tickets WHERE sender='Client' AND sender_id=$client_id ORDER BY support_id DESC");
		echo json_encode($data);
	}

	function deleteSupportTicket() {
		$support_id = $this->input->post('support_id');
		$data = $this->db->query("DELETE FROM support_tickets WHERE support_id=$support_id");
		echo 'deleted!';
	}

	
	function getSupportTicket() {
		$support_id = $this->input->post('support_id');
		$data['support_tickets'] = $this->fetchRawData("SELECT * FROM support_tickets WHERE support_id=$support_id");
		$data['support_ticket_responses'] = $this->fetchRawData("SELECT * FROM support_ticket_responses WHERE support_id=$support_id ORDER BY response_id ASC");

		echo json_encode($data);
	}

	function updateSupportTicketStatus() {
		$support_id = $this->input->post('support_id');
		$status = $this->input->post('status');
		$data = $this->db->query("UPDATE support_tickets SET status='$status' WHERE support_id=$support_id");
		echo 'updated!';
	}

	
	function saveSupportTicketResponse() { 
		$userdata = $this->session->userdata('user_data');


		$support_id = $this->input->post('support_id');
		$responder = $userdata['login_type'];
		$login_type = $userdata['login_type'];
		$response_date = date('Y-m-d H:i:s');;
		$response = $this->input->post('response');

		$this->load->model('Model_support_ticket_responses');
		$support_ticket_responses = new Model_support_ticket_responses(); 
		$support_ticket_responses->support_id = $support_id;
		$support_ticket_responses->responder = $responder;
		$support_ticket_responses->response_date = $response_date;
		$support_ticket_responses->response = $response; 
		$support_ticket_responses->save();
		echo "saved";

		if ($login_type != 'Client' || $login_type != 'Agent' || $login_type != 'Broker') {
			$data = $this->db->query("UPDATE support_tickets SET status='Responded' WHERE support_id=$support_id");
		} else {
			$data = $this->db->query("UPDATE support_tickets SET status='Pending' WHERE support_id=$support_id");
		}

	}

	

	function uploadDocumentFile() {
		$category = $this->input->post('category');
		// $file_name = $this->input->post('file_name');
		$client_status = $this->input->post('client_status');
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
			$client_joint_id = $this->input->post('client_joint_id'); 
			$folder_name = $this->getClientFolderName($client_joint_id,$client_type);
		} else {
			$client_id = $this->input->post('client_id');
			$folder_name = $this->getClientFolderName($client_id,$client_type);
		}

		$client_name = $folder_name;

		

		$folder_name = 'assets/documents/'.$folder_name;
		if (!is_dir($folder_name)) {
			mkdir($folder_name,0777);
		}
		$files = count($_FILES['document']['name']); 
		// $this->pprint($_FILES);
		$this->load->model('Model_client_documents');
		$this->load->model('Model_client_joint_documents');
    	if ($files > 1) { 
    		for ($i=0; $i < count($_FILES['document']['name']); $i++) { 
    			if($_FILES['document']['size'][$i] > 0)
				{
				 	$document = $_FILES['document']['name'][$i]; 
				 	$file_size = $_FILES['document']['size'][$i];

			 		$file_download = $document;

					if(!empty($_FILES['document']['name'][$i])){
			            $filetmp = $_FILES["document"]["tmp_name"][$i];
			            $filename = $_FILES["document"]["name"][$i];
			            $filetype = $_FILES["document"]["type"][$i];
			            $filepath = $folder_name.'/'.$file_download;
			            if (file_exists($filepath)) {
			            	$file_download = '_'.$file_download;
		            		$filepath = $folder_name.'/'.$file_download;
			            }

			            move_uploaded_file($filetmp, $filepath); 
			    	}
			    	if ($client_type == 'joint') {
			    		$client_joint_documents = new Model_client_joint_documents(); 
				    	$client_joint_documents->client_joint_id = $client_joint_id;
						$client_joint_documents->category = $category; 
						$client_joint_documents->file_download = $file_download;
						$client_joint_documents->file_size = $file_size;
						$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
						$client_joint_documents->save();
						$account_access = 'id='.$client_joint_id.'&type=joint';
			    	} else {
			    		$client_documents = new Model_client_documents();

				    	$client_documents->client_id = $client_id;
						$client_documents->category = $category; 
						$client_documents->file_download = $file_download;
						$client_documents->file_size = $file_size;
						$client_documents->date_uploaded = date('Y-m-d H:i:s');
						$client_documents->save();
						$account_access = 'id='.$client_id.'&type=client';
			    	}
			    	$this->sendUploadedDocuments($client_name,$client_status,$category,$file_size,base_url($filepath),$account_access);
				    	
			    }  
				    
    		}
    	} else { 
    		if($_FILES['document']['size'] > 0)
			{
			 	$document = $_FILES['document']['name'][0]; 
			 	$file_size = $_FILES['document']['size'][0];

		 		$file_download = $document;

				if(!empty($_FILES['document']['name'])){
		            $filetmp = $_FILES["document"]["tmp_name"][0];
		            $filename = $_FILES["document"]["name"][0];
		            $filetype = $_FILES["document"]["type"][0];
		            $filepath = $folder_name.'/'.$file_download;
		            if (file_exists($filepath)) {
		            	$file_download = '_'.$file_download;
		            	$filepath = $folder_name.'/'.$file_download;
		            }

		            move_uploaded_file($filetmp, $filepath); 
		    	}

		    	if ($client_type == 'joint') {
		    		$client_joint_documents = new Model_client_joint_documents(); 
			    	$client_joint_documents->client_joint_id = $client_joint_id;
					$client_joint_documents->category = $category; 
					$client_joint_documents->file_download = $file_download;
					$client_joint_documents->file_size = $file_size;
					$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
					$client_joint_documents->save(); 
					$account_access = 'id='.$client_joint_id.'&type=joint';
		    	} else {
		    		$client_documents = new Model_client_documents();

			    	$client_documents->client_id = $client_id;
					$client_documents->category = $category; 
					$client_documents->file_download = $file_download;
					$client_documents->file_size = $file_size;
					$client_documents->date_uploaded = date('Y-m-d H:i:s');
					$client_documents->save();
					$account_access = 'id='.$client_id.'&type=client';
					
		    	}

		    	$this->sendUploadedDocuments($client_name,$client_status,$category,$file_size,base_url($filepath),$account_access);


		    }  
			    
    	}


    	
    	
	}

	function testSend() {
		$this->sendUploadedDocuments('jo','active','Credit Report',100000,base_url('assets/images/LynxLogo.png'));
	}
 


	function sendUploadedDocuments($client_name,$client_status,$category,$file_size,$attach_url,$account_access) { 
		$account_access = 'https://admin.creditlynx.com/admin/clients?'.$account_access;
		$file_size = round((($file_size / 1000) / 1000),1).'MB';
		if ($category == 'Service Agreement' || $category == 'Address (1)' || $category == 'Address (2)'|| $category == 'Social')  {
			$from  = 'docs@creditlynx.com';
		} else {
			$from  = 'reports@creditlynx.com';
		}
		$data['client_name'] = $client_name;
        $data['client_status'] = $client_status;
        $data['category'] = $category;
        $data['file_size'] = $file_size;
        $data['account_access'] = $account_access;
        $body = $this->load->view('email_templates/document_template',$data,TRUE);

        $this->load->library('email');
		$config = Array(
				'mailtype' => 'html'
				);

		$this->email->initialize($config); 
		$this->email->from($from,'Dispute Team');
		$this->email->to('info@creditlynx.com'); 
		$this->email->subject($client_name .' Uploaded '.$category); 
		$this->email->message($body);
		$this->email->attach($attach_url); 
		if($this->email->send())
		{
			echo "Emial has been sent";
			// return true;
		}
		else
		{
			return $this->email->print_debugger();
		}
	}

	function getClientFolderName($id,$type) {
		if ($type == 'joint') {
			$data = $this->fetchRawData("SELECT name FROM client_joints WHERE client_joint_id=$id");
		} else {
			$data = $this->fetchRawData("SELECT name FROM clients WHERE client_id=$id");
		}

		return $data[0]['name'];
	}



	function getClientDocuments() {
		
		$client_type = $this->input->post('client_type');
		if ($client_type == 'joint') {
            $client_joint_id = $this->input->post('client_id');
            $data = $this->fetchRawData("SELECT * FROM client_joint_documents WHERE client_joint_id=$client_joint_id ORDER BY date_uploaded DESC");
        } else {
            $client_id = $this->input->post('client_id');
            $data = $this->fetchRawData("SELECT * FROM client_documents WHERE client_id=$client_id ORDER BY date_uploaded DESC");
        }
		
		echo json_encode($data);
	}

	function deleteCLientDocument() { 
		$client_type = $this->input->post('client_type');
		$doc_id = $this->input->post('doc_id');
		if ($client_type == 'joint') {
			$data_Delete = $this->db->query("DELETE FROM client_joint_documents WHERE doc_id=$doc_id");
		} else {
			$data_Delete = $this->db->query("DELETE FROM client_documents WHERE doc_id=$doc_id");
		}
		
		echo "deleted!";
	}


	function saveBureauLetterTemplate() {
		$template_header = $this->input->post('template_header');
		$template_title = $this->input->post('template_title');
		$template = $this->input->post('template');


		$data_select = $this->fetchRawData("SELECT * FROM letter_templates WHERE template_header='$template_header' AND template_title='$template_title'");
		$this->load->model('Model_letter_templates');
		$letter_templates = new Model_letter_templates();
		if (count($data_select) > 0) {
			$letter_templates->blt_id = $data_select[0]['blt_id'];
		}
		 
		$letter_templates->template_header = $template_header;
		$letter_templates->template_title = $template_title;
		$letter_templates->template = $template;
		$letter_templates->save();
	}


	function fetchDataTemplateList() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT client_id,bureaus,account_name,account_number,client_rounds.* FROM client_account_details LEFT JOIN client_rounds ON client_account_details.account_id = client_rounds.account_id WHERE client_account_details.client_id = $client_id AND account_status <> 'Fixed' AND account_status <> 'Deleted' AND account_status <> 'Not Reporting'");

		echo json_encode($data);
	}

	function getLetterCreditors() {
		$data = $this->fetchRawData("SELECT * FROM letter_creditors ORDER BY name ASC");
		echo json_encode($data);
	}

	function getClientLetterAccounts() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT account_name,account_number FROM client_account_details WHERE client_id=$client_id AND account_status <> 'Fixed' AND account_status <> 'Deleted' AND account_status <> 'Not Reporting' GROUP BY account_number ORDER BY account_name ASC");
		echo json_encode($data);
	}

	function saveLetterCreditor() {
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$city_state_zip = $this->input->post('city_state_zip');
 
		$this->load->model('Model_letter_creditors');
		$letter_creditors = new Model_letter_creditors(); 
		 
		$letter_creditors->name = $name;
		$letter_creditors->address = $address;
		$letter_creditors->city_state_zip = $city_state_zip;
		$letter_creditors->save();
	}


	function getClientEmails() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM client_emails WHERE client_id=$client_id ORDER BY email_id DESC");
		echo json_encode($data);
	}

	function getClientTexts() {
		$client_id = $this->input->post('client_id');
		$data = $this->fetchRawData("SELECT * FROM client_etexts WHERE client_id=$client_id ORDER BY text_id DESC");
		echo json_encode($data);
	}

	function deleteAlertTemplate() {
		$alert_template_id = $this->input->post('alert_template_id');
		$data = $this->db->query("DELETE FROM client_alerts_templates WHERE alert_template_id='$alert_template_id'");
		echo "deleted";
	}

	function convertLeadToClient() {
		$client_id = $this->input->post('client_id');
		$converted_date = date('Y-m-d H:i:s');
		$date_of_enrollment = date('m/d/Y');
		$data_update = $this->db->query("UPDATE clients SET converted=1,converted_date='$converted_date' WHERE client_id=$client_id");
		$data_exist = $this->fetchRawData("SELECT * FROM client_enrollments WHERE client_id=$client_id");
		if (count($data_exist) > 0) {
			$data_enrollment_update = $this->db->query("UPDATE client_enrollments SET date_of_enrollment='$date_of_enrollment' WHERE client_id=$client_id");
		} else {
			$data_enrollment_update = $this->db->query("INSERT INTO client_enrollments  (client_id,date_of_enrollment) VALUES ('$client_id','$date_of_enrollment')");
		}
		
		$this->sendWelcomeEmail($client_id);
		echo "converted";
	}

	function changeAlertDate() {
	    $alert_id = $this->input->post('alert_id');
	    $alert_date = $this->input->post('alert_date');
	    $data = $this->db->query("UPDATE client_alerts SET alert_date='$alert_date' WHERE alert_id=$alert_id");
	    echo "saved";
	  }

	 public function sendEmailSingUp() {
	 	$client_id = $this->input->post('client_id'); 
		$to = $this->input->post('email_address'); 
		// $message = 'sample';
		// $this->email->initialize($config);
      
  //     	$this->email->from('no-reply@creditlynx.com', 'CreditLynx'); 
  //     	// $this->email->to($to);
  //     	$this->email->subject($subject); 
  //     	$this->email->message($message);

		// $this->email->send();

		$this->load->library('email');

		$this->email->from('no-reply@creditlynx.com', 'Your Name');
		$this->email->to('joshuasaubon@gmail.com'); 

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
 

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}
 
	}

	  function sendWelcomeEmailToClientByID() {
	    $client_id = $this->input->post('client_id'); 
	    $this->sendWelcomeEmail($client_id); 
	  }

	  function autoAlertCron() {
	  	$data = $this->fetchRawData("SELECT auto_alert,client_id FROM view_clients_table WHERE auto_alert IS NOT NULL AND converted=1");

	  	foreach ($data as $key => $client) {
	  		$auto_alert = $client['auto_alert'];
	  		$client_id = $client['client_id']; 
	  		echo $auto_alert;
	  		if ($auto_alert == 7 || $auto_alert == 14 || $auto_alert == 21) {
	  			# SEND Reminder Alert
	  			$this->load->model('Model_client_alerts');
			    $client_alerts = new Model_client_alerts();
		  
				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Friendly Reminder - Please Look For Responses';
				$client_alerts->alert_notes = 'Please upload all bureau reports and/or creditor letters as soon as possible.  They will be in plain, white envelopes which appear as junk mail. '; 
				$client_alerts->alert_date = date('Y-m-d H:i:s'); 
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
	  		}

	  		if ($auto_alert == 28 || $auto_alert == 35 || $auto_alert == 42) {
	  			# SEND Submit Alert
	  			$this->load->model('Model_client_alerts');
			    $client_alerts = new Model_client_alerts();
		  
				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Friendly Reminder - Please Submit Bureau Reports';
				$client_alerts->alert_notes = 'Please upload all bureau reports as soon as possible. If you haven\'t received anything from the credit bureaus please reach out to us immediately.'; 
				$client_alerts->alert_date = date('Y-m-d H:i:s'); 
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
	  		}

	  		if ($auto_alert == 46 || $auto_alert == 50) {
	  			# SEND Deadline Alert
	  			$this->load->model('Model_client_alerts');
			    $client_alerts = new Model_client_alerts();
		  
				$client_alerts->client_id = $client_id;
				$client_alerts->sender = '';
				$client_alerts->sender_id = 0;
				$client_alerts->sender_photo = '';
				$client_alerts->sender_name = '';
				$client_alerts->alert_subject = 'Please Submit Bureau Reports - Bureau Deadline Overdue';
				$client_alerts->alert_notes = 'Please upload all bureau reports as soon as possible.  If you haven\'t received anything from the credit bureaus please reach out to us immediately.'; 
				$client_alerts->alert_date = date('Y-m-d H:i:s'); 
				$client_alerts->save();

				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Email',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'single');
				// $this->sendEmail('Client','Phone',$client_id,$client_alerts->alert_subject,$client_alerts->alert_notes,$client_alerts->alert_date,'joint');
	  		}
	  	}

	  }

	

	// client id : gaBORr7_Q6GpidE7cpOpOg
	// client secret : pqmGTP9hSsiAxv5knw7g4AEnTQnQ3oRr6Bb6HSKaDa4A
	// server : 	https://platform.devtest.ringcentral.com
	// main : +15134099451
 	// number : +18139517686
	 // sanbox password : Wrbwrx14!
	
	
}
