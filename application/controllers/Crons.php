<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// date_default_timezone_set(timezone_name_from_abbr("EST"));
date_default_timezone_set('America/Los_Angeles');
// date_default_timezone_set('America/New_York');
require 'authorize/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("MERCHANT_LOGIN_ID", '9Ez4A4wqKn');
define("MERCHANT_TRANSACTION_KEY", '4ng82ZbX6D628Bz2');

define("AUTHORIZENET_LOG_FILE", "phplog");

class Crons extends MY_Controller {

	public function index()
	{
		
	}

	function cron_alert_verifs() {
		// ALL FUNCTIONS WILL TRIGGER ONCE EVERY DAY

		// EXCEPTIONS / NOT INCLUDED
		// EXCEPTIONS: Clients before 05/10/2019
		// EXCEPTIONS: Client Statuses 'Cancelled','Terminated','Archived','Suspended','NSF','NSF2' 



		// this function will send SA once in day 1 and then every 2 days until Service Agreement Box is Completed/Inputted.
		$this->cron_send_sa(); 

		// this function will send Credit Report Alert once day 4 and every 2 days until Credit Report is Submitted/Uploaded 
		$this->cron_send_cr_alert();

		// If Address 1 and Address 2 And Social Boxes is empty then this function will send Verification Alert every day until Address 1 and Address 2 and Social is Submitted/Uploaded 
		$this->cron_send_vr_alert();



	}

	function cron_alert_verifs2() {
		// If Address 1 and Address 2 is Uploaded and Social is Empty then this function will send SSN Alert every day until Social is Submitted/Uploaded 
		$this->cron_send_ssn_alert();

		// If Address 1 and Address 2 is Empty and Social is Uploaded then this function will send AV2 Alert every day until Address 1 and Address 2 is Submitted/Uploaded 
		$this->cron_send_av2_alert();

		// If Address 1 is Uploaded and Social is Uploaded And Address 2 is Empty  then this function will send AV Alert every day until Address 1 Submitted/Uploaded 
		$this->cron_send_av_alert();

		// If Address 1 is Uploaded and Social is Empty And Address 2 is Empty  then this function will send AVSSN Alert every day until Address 2 AND Social Submitted/Uploaded 
		$this->cron_send_avssn_alert();
		 
	}

	function cron_alert_verifs3() {
		// IF SA and Credit Report Boxes are Empty then Update Client Status to Review
		$this->cron_status_review();

		// IF Address 1, Address 2 and Social Boxes are Empty then Update Client Status to Verification
		$this->cron_status_verification();

		// IF Address 1, Address 2 are Submitted and Social Box is Empty then Update Client Status to SSN
		$this->cron_status_ssn();

		// IF Address 1, Address 2 are Empty and Social Box is Inputted then Update Client Status to AV2
		$this->cron_status_av2();
	}

	function cron_alert_verifs4() {
		// IF Address 2 is Empty and Address 1,Social Box is Inputted then Update Client Status to AV
		$this->cron_status_av();

		// IF Address 2 and Social Box are Empty and Address 1 is Inputted then Update Client Status to AVSSN
		$this->cron_status_avssn();

		// IF Address 1, Address 2 and Social are Submitted/Inputted Then Update Client Status to Active
		$this->cron_status_active();

		// This Function will update all Auto Alert OFF to Cancelled Clients
		$this->cron_alerts_off();
	}

	function cron_client_alerts1() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_clients(0,20); 
	}
	function cron_client_alerts2() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_clients(20,20); 
	}
	function cron_client_alerts3() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_clients(40,20); 
	}
	function cron_client_alerts4() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_clients(60,20); 
	} 
	function cron_client_alerts5() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_clients(80,20); 
	} 

	function cron_joint_alerts1() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_joints(0,30); 
	}
	function cron_joint_alerts2() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_joints(30,30); 
	}
	function cron_joint_alerts3() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_joints(60,30); 
	}
	function cron_joint_alerts4() {

		// CONDITIONS OF THIS ARE ON THE Docs. 
		$this->cron_alerts_reminder_joints(90,30); 
	} 
 
		
	function cron_send_sa() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND 
											(service_agreement IS NULL OR service_agreement = '')
											AND 
											(date_of_cancellation IS NULL OR date_of_cancellation = '') 
										)");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=2; strtotime(date('m/d/Y')) > strtotime($date); $i+2) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						$this->sendEmailSingUpCron($client['client_id'],'client',$client['email_address'],$client['name']);
					}
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
										client_joint_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
									FROM
										client_joints 
									WHERE
										client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_joint_id IN (
										SELECT
											client_joint_id 
										FROM
											client_joint_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND 
											(service_agreement IS NULL OR service_agreement = '')
											AND 
											(date_of_cancellation IS NULL OR date_of_cancellation = '') 
										)");
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=2; strtotime(date('m/d/Y')) > strtotime($date); $i+2) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						$this->sendEmailSingUpCron($client['client_joint_id'],'joint',$client['email_address'],$client['name']);
					}
				}
			// } 
		}
	}

	function cron_status_review() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									service_agreement IS NULL OR service_agreement = '' OR reports_received IS NULL OR reports_received = ''
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='Review' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2' ) 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE ( service_agreement IS NULL OR service_agreement = '' OR reports_received IS NULL OR reports_received = '' ) )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='Review' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}
	function cron_status_verification() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND  (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND 
									(address_verification_received IS NULL OR address_verification_received = '')
									AND (address_verification_received_2 IS NULL OR address_verification_received_2 = '')
									 AND (ss_proof_received IS NULL OR ss_proof_received = '')
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='Verifications' WHERE client_id IN ($client_ids)");
		}

		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											 
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND  (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND 
											(address_verification_received IS NULL OR address_verification_received = '')
											AND (address_verification_received_2 IS NULL OR address_verification_received_2 = '')
											 AND (ss_proof_received IS NULL OR ss_proof_received = '')
										)
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='Verifications' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}
	function cron_status_ssn() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review','Verifications')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND 
									( address_verification_received <> '' AND address_verification_received_2 <> '' )
									 AND (ss_proof_received IS NULL OR ss_proof_received = '')
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='SSN' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review','Verifications') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND 
											( address_verification_received <> '' AND address_verification_received_2 <> '' )
											 AND (ss_proof_received IS NULL OR ss_proof_received = '') )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='SSN' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}

	function cron_status_av2() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review','Verifications')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND address_verification_received = '' AND address_verification_received_2 = ''
									AND ss_proof_received <> ''
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='AV2' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review','Verifications') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND address_verification_received = '' AND address_verification_received_2 = ''
											AND ss_proof_received <> '' )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='AV2' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}

	function cron_status_av() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review','Verifications')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND address_verification_received <> '' 
									AND address_verification_received_2 = ''
									AND ss_proof_received <> ''
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='AV' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review','Verifications') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND address_verification_received <> '' 
											AND address_verification_received_2 = ''
											AND ss_proof_received <> '' )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='AV' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}
	function cron_status_avssn() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review','Verifications')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND address_verification_received <> '' 
									AND address_verification_received_2 = ''
									AND ss_proof_received = ''
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='AVSSN' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review','Verifications') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND address_verification_received <> '' 
											AND address_verification_received_2 = ''
											AND ss_proof_received = '' )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='AVSSN' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}
	function cron_status_active() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2','Review','Verifications')   AND client_id IN (
								SELECT
									client_id 
								FROM
									client_enrollments 
								WHERE 
									(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
									AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
									AND address_verification_received <> '' 
									AND address_verification_received_2 <> ''
									AND ss_proof_received <> ''
								)
					");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET client_status='Active' WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT
											client_joint_id
										FROM
											client_joints 
										WHERE
											
											client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  ) 
											
											AND client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived','Suspended','NSF','NSF2','Review','Verifications') 
											AND client_joint_id IN ( SELECT client_joint_id FROM client_joint_enrollments WHERE (date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND address_verification_received <> '' 
											AND address_verification_received_2 <> ''
											AND ss_proof_received <> '' )  
										");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET client_status='Active' WHERE client_joint_id IN ($client_joint_ids)");
		}
			
	}

	public function sendEmailSingUpCron($id,$client_type,$email_address,$name) {  
		$data['name'] = $name;
 
		$session = password_hash('creditlynx' . time(), PASSWORD_BCRYPT);
		$session = str_replace('$', 'X1Rs4XsQln', $session);
		$data['session'] = 'signup-invite?id=' . $session;
		$this->db->query("INSERT INTO signup_sessions (session,status,id,type) VALUES ('$session',0,$id,'$client_type')");

		$body = $this->load->view('email_templates/DocuLynx_template_signup_sa', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to($email_address);

		$this->email->subject('Credit Lynx Sign Up');
		$this->email->message($body);

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}

		$this->load->model('Model_template_sent');
		$template_sent = new Model_template_sent();
		$template_sent->name1 = $name;
		$template_sent->email1 = $email_address;
		$template_sent->subject = 'Email Sent';
		$template_sent->message = 'Waiting for submission';
		$template_sent->status = 'unseen';
		$template_sent->date_sent = date('Y-m-d');
		$template_sent->save();
	}

	

	function cron_send_cr_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND 
											(reports_received IS NULL OR reports_received = '')
											AND 
											(date_of_cancellation IS NULL OR date_of_cancellation = '') 
										)");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 4 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 4 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=2; strtotime(date('m/d/Y')) > strtotime($date); $i+2) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						$this->saveAlertCron($client['client_id'],0,'client','Pending Credit Report','Please submit your credit report. This document is required in order to begin rebuilding your credit.');
					}
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
										client_joint_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
									FROM
										client_joints 
									WHERE
										client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_joint_id IN (
										SELECT
											client_joint_id 
										FROM
											client_joint_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND 
											(reports_received IS NULL OR reports_received = '')
											AND 
											(date_of_cancellation IS NULL OR date_of_cancellation = '') 
										)"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 4 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 4 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=2; strtotime(date('m/d/Y')) > strtotime($date); $i+2) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  
 

					if ($date == date('m/d/Y',$date_today)) {
						
						$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending Credit Report','Please submit your credit report. This document is required in order to begin rebuilding your credit.');
					}
				}
			// } 
		}
	}

	function cron_send_vr_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
											AND  (date_of_cancellation IS NULL OR date_of_cancellation = '') 
											AND 
											(address_verification_received IS NULL OR address_verification_received = '')
											AND (address_verification_received_2 IS NULL OR address_verification_received_2 = '')
											 AND (ss_proof_received IS NULL OR ss_proof_received = ''))
										");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							echo "send";
							$this->saveAlertCron($client['client_id'],0,'client','Pending All Verifications','Please upload (2) different address verifications (driver\'s license, utility bill, bank statement, w2, tax return) and (1) social security verification (social security card, tax return)');
						}
					}
						
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
										client_joint_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
									FROM
										client_joints 
									WHERE
										client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_joint_id IN (
										SELECT
											client_joint_id 
										FROM
											client_joint_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
										AND  (date_of_cancellation IS NULL OR date_of_cancellation = '') 
										AND 
										(address_verification_received IS NULL OR address_verification_received = '')
										AND (address_verification_received_2 IS NULL OR address_verification_received_2 = '')
										 AND (ss_proof_received IS NULL OR ss_proof_received = ''))
									"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  
 

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending All Verifications','Please upload (2) different address verifications (driver\'s license, utility bill, bank statement, w2, tax return) and (1) social security verification (social security card, tax return)');
						} 
					}
				}
			// } 
		}
	}

	function cron_send_ssn_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
												AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
												AND 
												( address_verification_received <> '' AND address_verification_received_2 <> '' )
												 AND (ss_proof_received IS NULL OR ss_proof_received = ''))
											");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron($client['client_id'],0,'client','Pending (1) Social Verification','Please upload (1) social security verification (social security card, tax return)');
						}
					}
						
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
												client_joint_id, 
												name,
												email_address,
												(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
											FROM
												client_joints 
											WHERE
												client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
												AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
												AND client_joint_id IN (
												SELECT
													client_joint_id 
												FROM
													client_joint_enrollments 
												WHERE
													(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND 
													( address_verification_received <> ''
													AND address_verification_received_2 <> '')
													 AND (ss_proof_received IS NULL OR ss_proof_received = ''))
												"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" )); 
					}  
  
					if ($date == date('m/d/Y',$date_today)) { 
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending (1) Social Verification','Please upload (1) social security verification (social security card, tax return)');
						} 
					}
				}
			// } 
		}
	}
	function cron_send_av2_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received = '' AND address_verification_received_2 = ''
													AND ss_proof_received <> '')
											");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron($client['client_id'],0,'client','Pending (2) Address Verifications','Please upload (2) different address verifications (driver\'s license, utility bill, bank statement, w2, tax return)');
						}
					}
						
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
												client_joint_id, 
												name,
												email_address,
												(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
											FROM
												client_joints 
											WHERE
												client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
												AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
												AND client_joint_id IN (
												SELECT
													client_joint_id 
												FROM
													client_joint_enrollments 
												WHERE
													(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received = '' AND address_verification_received_2 = ''
													AND ss_proof_received <> '')
												"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" )); 
					}  
  
					if ($date == date('m/d/Y',$date_today)) { 
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending (2) Address Verifications','Please upload (2) different address verifications (driver\'s license, utility bill, bank statement, w2, tax return)');
						} 
					}
				}
			// } 
		}
	}

	function cron_send_av_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received <> '' AND address_verification_received_2 = ''
													AND ss_proof_received <> '')
											");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron($client['client_id'],0,'client','Pending (1) Address Verification','Please upload (1) more address verification (driver\'s license, utility bill, bank statement, w2, tax return)');
						}
					}
						
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
												client_joint_id, 
												name,
												email_address,
												(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
											FROM
												client_joints 
											WHERE
												client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
												AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
												AND client_joint_id IN (
												SELECT
													client_joint_id 
												FROM
													client_joint_enrollments 
												WHERE
													(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received <> '' AND address_verification_received_2 = ''
													AND ss_proof_received <> '')
												"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" )); 
					}  
  
					if ($date == date('m/d/Y',$date_today)) { 
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending (1) Address Verification','Please upload (1) more address verification (driver\'s license, utility bill, bank statement, w2, tax return)');
						} 
					}
				}
			// } 
		}
	}


	function cron_send_avssn_alert() {
		$clients = $this->fetchRawData("SELECT
										client_id, 
										name,
										email_address,
										(SELECT date_of_enrollment FROM client_enrollments WHERE client_id=clients.client_id) as `date_of_enrollment` 
									FROM
										clients 
									WHERE
										converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  
										AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
										AND client_id IN (
										SELECT
											client_id 
										FROM
											client_enrollments 
										WHERE
											(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received <> '' 
													AND address_verification_received_2 = ''
													AND ss_proof_received = '')
											");
		foreach ($clients as $key => $client) { 
			// if ($client['client_id'] == 33) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" ));
					}  

					if ($date == date('m/d/Y',$date_today)) {
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron($client['client_id'],0,'client','Pending (1) Address and (1) Social Verification','Please upload (1) more address verification (driver\'s license, utility bill, bank statement, w2, tax return) and (1) social security verification (social security card, tax return)');
						}
					}
						
				}
			// } 
		}

		$client_joints = $this->fetchRawData("SELECT
												client_joint_id, 
												name,
												email_address,
												(SELECT date_of_enrollment FROM client_joint_enrollments WHERE client_joint_id=client_joints.client_joint_id) as `date_of_enrollment` 
											FROM
												client_joints 
											WHERE
												client_id IN (SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))   ) 
												AND client_status NOT IN  ('Cancelled','Terminated','Archived','Suspended','NSF','NSF2')
												AND client_joint_id IN (
												SELECT
													client_joint_id 
												FROM
													client_joint_enrollments 
												WHERE
													(date_of_enrollment IS NOT NULL  OR date_of_enrollment <> '' )
													AND (date_of_cancellation IS NULL OR date_of_cancellation = '') 
													AND address_verification_received <> '' 
													AND address_verification_received_2 = ''
													AND ss_proof_received = '')
												"); 
		foreach ($client_joints as $key => $client) { 
			// if ($client['client_joint_id'] == 78) {
				$date_today = time();
				$date_of_enrollment = $client['date_of_enrollment']; 
				$date1 = date('m/d/Y',strtotime($date_of_enrollment . ' + 1 days')); 
				if ($date_today > strtotime($date_of_enrollment . ' + 1 days')) {
					$counter = 0;
					$date = $date1;  

					for ($i=1; strtotime(date('m/d/Y')) > strtotime($date); $i+1) { 
						$date = date('m/d/Y',strtotime($date . " + $i days" )); 
					}  
  
					if ($date == date('m/d/Y',$date_today)) { 
						if (date('l',$date_today) != 'Saturday' || date('l',$date_today) != 'Sunday') {
							$this->saveAlertCron(0,$client['client_joint_id'],'joint','Pending (1) Address and (1) Social Verification','Please upload (1) more address verification (driver\'s license, utility bill, bank statement, w2, tax return) and (1) social security verification (social security card, tax return)');
						} 
					}
				}
			// } 
		}
	}

	function saveAlertCron($client_id,$client_joint_id,$client_type,$alert_subject,$alert_notes) {  
		$sender = 'Automated Alert';
		$sender_id = 0;
		$sender_photo = '';
		$sender_name = 'Automated Alert';
		$this->load->model('Model_client_alerts');
		$client_alerts = new Model_client_alerts();

		if ($client_type == 'joint') {
			$client_alerts->client_id = $client_joint_id;
		} else {
			$client_alerts->client_id = $client_id;
		}

		$client_alerts->client_type = $client_type;
		$client_alerts->sender = $sender;
		$client_alerts->sender_id = $sender_id;
		$client_alerts->sender_photo = $sender_photo;
		$client_alerts->sender_name = $sender_name;
		$client_alerts->alert_subject = $alert_subject;
		$client_alerts->alert_notes = $alert_notes;
		$client_alerts->alert_date = date('Y-m-d H:i:s');
		$client_alerts->save(); 

		if ($this->get_auto_alert(	$client_type == 'client' ? 'client_id' : 'client_joint_id', 
									$client_type == 'client' ? $client_id : $client_joint_id, 
									$client_type == 'client' ? 'clients' : 'client_joints') == 1) { 
			// $this->sendEmail('Client', 'Email', $client_type == 'client' ? $client_id : $client_joint_id, $alert_subject, $alert_notes, $client_alerts->alert_date, $client_type);
			$this->send_email_to_client($client_type == 'client' ? $client_id : $client_joint_id,$alert_subject,$alert_notes,$client_type);
			$this->send_text_alert_to_client($client_type == 'client' ? $client_id : $client_joint_id, $client_type, $alert_subject, $alert_notes);
		} else {
			echo "<br>Email/Text not sent (Alert Off) ";
		}

			
	}

	function cron_alerts_off() {
		$clients = $this->fetchRawData("SELECT client_id FROM clients WHERE  converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  AND client_status IN ('Cancelled','Terminated','Archived')");
		if (count($clients) > 0) {
			$client_ids = array_column($clients, 'client_id');
			$client_ids = implode(', ', $client_ids);
			$update = $this->db->query("UPDATE clients SET auto_alert=0 WHERE client_id IN ($client_ids)");
		}


		$client_joints = $this->fetchRawData("SELECT client_joint_id FROM client_joints WHERE
											 client_id IN ( SELECT client_id FROM clients WHERE converted = 1 AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))  )  
											AND client_status IN ('Cancelled','Terminated','Archived')");
		if (count($client_joints) > 0) { 
			$client_joint_ids = array_column($client_joints, 'client_joint_id');
			$client_joint_ids = implode(', ', $client_joint_ids); 
			$update = $this->db->query("UPDATE client_joints SET auto_alert=0 WHERE client_joint_id IN ($client_joint_ids)");
		} 
	}

	function skip_weekend_and_holidays($date,$holidays) {
		while (date('l',strtotime($date)) == 'Saturday' || date('l',strtotime($date)) == 'Sunday' || in_array($date, $holidays)) {
			$date = date('m/d/Y',strtotime($date . ' + 1 days')); 
		} 
		return $date;
	}

	function testing_testing() {
		$restricted_alerts = "
						'Equifax Reports Received',
						'Experian Reports Received',
						'TransUnion Reports Received',
						'Equifax and TransUnion Reports Received',
						'Equifax and Experian Reports Received',
						'Experian and TransUnion Reports Received',
						'Equifax, Experian, and TransUnion Reports Received',
						'Cancellation Form Received',
						'Hold Request Received',
						'Invoice Sent',
						'New Payment Information Required',
						'Account Terminated',
						'1st Late Notice',
						'2nd Late Notice',
						'Final Late Notice',
						'Account Suspended',
						'Account Terminated'";

		$alert_subjects = "
						'Drafted First Round Letters',
						'Drafted Second Round Letters',
						'Drafted Third Round Letters',
						'Drafted Fourth Round Letters',
						'Drafted Fifth Round Letters',
						'Drafted Sixth Round Letters',
						'Drafted Seventh Round Letters',
						'Drafted Eighth Round Letters',
						'Drafted Ninth Round Letters',
						'Drafted Tenth Round Letters',
						'Drafted Eleventh Round Letters',
						'Drafted Twelfth Round Letter',
						'Drafted First Round Letters Without Full Credit Report Submission'";
		$alert_missing = "
						'Missing Equifax Pages',
						'Missing Experian Pages',
						'Missing TransUnion Pages',
						'Credit Report Missing Pages'
						";

		// $client_alerts = $this->fetchRawData("
		// 		SELECT * FROM (SELECT
		// 			client_id,
		// 			client_status,
		// 			name,
		// 			( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_subjects ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
		// 			( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_subjects ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
		// 			( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $restricted_alerts ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
		// 			( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $restricted_alerts ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
		// 			( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
		// 			( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
		// 			( SELECT alert_notes FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
		// 			( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_date`,
		// 			( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_subject`
		// 		FROM
		// 			clients 
		// 		WHERE auto_alert=1 AND
		// 			client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
		// 			AND client_id IN (SELECT client_id FROM client_alerts WHERE client_type='client' GROUP BY client_id)
		// 			AND converted = 1  ) as `aaa` WHERE aaa.last_alert_date IS NOT NULL
		// ");

		echo "SELECT * FROM (SELECT
					client_id,
					client_status,
					name,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_subjects ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_subjects ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $restricted_alerts ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $restricted_alerts ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
					( SELECT alert_notes FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject IN ( $alert_missing ) ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_subject`
				FROM
					clients 
				WHERE auto_alert=1 AND
					client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
					AND client_id IN (SELECT client_id FROM client_alerts WHERE client_type='client' GROUP BY client_id)
					AND converted = 1  ) as `aaa` WHERE aaa.last_alert_date IS NOT NULL ORDER BY client_id ASC OFFSET 0 LIMIT 30";
	}
	 
	function cron_alerts_reminder_clients($offset,$limit) {  
		$restricted_alerts = 'Equifax Reports Received|Experian Reports Received|TransUnion Reports Received|Equifax and TransUnion Reports Received|Equifax and Experian Reports Received|Experian and TransUnion Reports Received|Equifax, Experian, and TransUnion Reports Received|Cancellation Form Received|Hold Request Received|Invoice Sent|New Payment Information Required|Account Terminated|1st Late Notice|2nd Late Notice|Final Late Notice|Account Suspended|Account Terminated';

		$alert_subjects = 'Drafted Twelfth Round Letters|Drafted Eleventh Round Letters|Drafted Tenth Round Letters|Drafted Ninth Round Letters|Drafted Eighth Round Letters|Drafted Seventh Round Letters|Drafted Sixth Round Letters|Drafted Fifth Round Letters|Drafted Fourth Round Letters|Drafted Third Round Letters|Drafted Third Round Letters Without Experian Submission|Drafted Second Round Letters|Drafted First Round Letters Without Full Credit Report Submission|Drafted First Round Letters';
		$alert_missing = 'Missing Equifax Pages|Missing Experian Pages|Missing TransUnion Pages|Credit Report Missing Pages';


		$client_alerts = $this->fetchRawData("
				SELECT * FROM (SELECT
					client_id,
					client_status,
					name,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
					( SELECT alert_notes FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
					( SELECT alert_date FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='client' AND client_id = clients.client_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_subject`
				FROM
					clients 
				WHERE auto_alert=1 AND
					client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
					AND client_id IN (SELECT client_id FROM client_alerts WHERE client_type='client' GROUP BY client_id)
					AND converted = 1  ) as `aaa` WHERE aaa.last_alert_date IS NOT NULL ORDER BY client_id ASC LIMIT $limit OFFSET $offset
		");
		// AND DATE(converted_date) >= DATE(STR_TO_DATE('2019-05-10','%Y-%m-%d'))

		
		$date_today = date('m/d/Y');
		echo "Today: ".$date_today;  

		$holidays = $this->getHolidays();
		foreach ($client_alerts as $key => $alert) { 
			$client_id = $alert['client_id'];
			echo "<br>".$alert['name'];
			// if ($client_id == 1940) {
				$alert_date = $alert['last_alert_date'];
				echo "<br>Round Alert Date: ".date('m/d/Y',strtotime($alert_date));
				echo "<br>";
				echo $alert['last_restricted_alert_date'];
				echo "<br>";
				if ($alert['last_restricted_alert_date'] == '') {
					
					if (strtotime($alert_date) < strtotime($alert['first_pending_all_alert_date'])) {
						echo $alert['first_pending_all_alert_subject'];
						echo '<br>'.$alert_date;
						$alert_template = $this->get_pending_alert_to_send_today($date_today,$alert['first_pending_all_alert_date'],$alert['first_pending_all_alert_subject'],$holidays); 
									$this->pprint($alert_template);
								if (count($alert_template) > 0) {

									$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
									// echo "send";

								} 
					} else { 
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays);
						echo "<br>First Reminder Date: ".$first_reminder;  
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$third_submit = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$third_submit = $this->skip_weekend_and_holidays($third_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						echo "<br>Second Reminder: ".$second_reminder; 
						echo "<br>Third Reminder: ".$third_reminder;  
						echo "<br>First Submit: ".$first_submit;  
						echo "<br>Second Submit: ".$second_submit;
						echo "<br>Third Submit: ".$third_submit;
						echo "<br>First Deadline: ".$first_deadline;  
						echo "<br>Second Deadline: ".$second_deadline;  
						echo "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						echo "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						if ($first_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send First Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						} 
					 	if ($second_reminder == $date_today) { 
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Second Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
					 	if ($third_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Third Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
						if ($first_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send First Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($third_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Third Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($first_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send First Deadline";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send Second Deadline";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
					}
						
				} else { 
					if (strtotime($alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays);
						echo "<br>First Reminder Date: ".$first_reminder;  
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);
						$third_submit = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$third_submit = $this->skip_weekend_and_holidays($third_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						echo "<br>Second Reminder: ".$second_reminder; 
						echo "<br>Third Reminder: ".$third_reminder;  
						echo "<br>First Submit: ".$first_submit;  
						echo "<br>Second Submit: ".$second_submit;
						echo "<br>Third Submit: ".$third_submit;
						echo "<br>First Deadline: ".$first_deadline;  
						echo "<br>Second Deadline: ".$second_deadline;  
						echo "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						echo "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						if ($first_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send First Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						} 
					 	if ($second_reminder == $date_today) { 
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Second Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
					 	if ($third_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Third Reminder";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
						if ($first_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send First Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($third_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($first_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send First Deadline";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send Second Deadline";
							$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
					} else { 
						echo '<br>'.$alert['last_restricted_alert_subject'];
						if ($alert['last_restricted_alert_subject'] == 'Equifax, Experian, and TransUnion Reports Received') {
							if ($alert['client_status'] != 'Active' && $alert['client_status'] != 'AV' && $alert['client_status'] != 'AV2' && $alert['client_status'] != 'AVSSN' && $alert['client_status'] != 'SSN') {
								echo "<br>change to review";
								echo "<br>turn off alert";
								$client_update = $this->db->query("UPDATE clients SET client_status = 'Review', auto_alert=0 WHERE client_id=$client_id");
							}
						} else { 
							$alert_template = $this->get_pending_alert_to_send_today($date_today,$alert['last_restricted_alert_date'],$alert['last_restricted_alert_subject'],$holidays); 
							if (count($alert_template) > 0) {

								$this->pprint($alert_template);
								$this->saveAlertCron($client_id,0,'client',$alert_template['alert_subject'],$alert_template['alert_notes']);
								// $this->pprint($alert_template);

							}  
								
						} 

						
					}
				}

				if ($alert['last_missing_alert_date'] != '') {   
					$last_missing_alert_date = date('m/d/Y',strtotime($alert['last_missing_alert_date']));

					if (strtotime($last_missing_alert_date) > strtotime($alert['last_restricted_alert_date']) && strtotime($last_missing_alert_date) > strtotime($alert_date)) {
						$next_missing_alert_date = date('m/d/Y',strtotime($last_missing_alert_date. ' + 2 days' ));
						// $alert_template = $this->get_alert_template_by_subject($alert['last_missing_alert_subject']); 
 
						while (strtotime($next_missing_alert_date) < strtotime($date_today)) {
							$next_missing_alert_date = date('m/d/Y',strtotime($next_missing_alert_date. ' + 2 days'));
						}  

						$next_missing_alert_date = $this->skip_weekend_and_holidays($next_missing_alert_date,$holidays);

						echo "<br>Next Missing Alert Date".$next_missing_alert_date;
						if ($date_today == $next_missing_alert_date) {
							// $this->pprint($alert_template);
							$alert_subject = $alert['last_missing_alert_subject'];
							$alert_notes = $alert['last_missing_alert_notes'];
							$this->saveAlertCron($client_id,0,'client',$alert_subject,$alert_notes);
						}
						
					} 	 
						
				}  
 	
			// }
		}
	}
	function cron_alerts_reminder_joints($offset,$limit) { 
		$restricted_alerts = 'Equifax Reports Received|Experian Reports Received|TransUnion Reports Received|Equifax and TransUnion Reports Received|Equifax and Experian Reports Received|Experian and TransUnion Reports Received|Equifax, Experian, and TransUnion Reports Received|Cancellation Form Received|Hold Request Received|Invoice Sent|New Payment Information Required|Account Terminated|1st Late Notice|2nd Late Notice|Final Late Notice|Account Suspended|Account Terminated';

		$alert_subjects = 'Drafted Twelfth Round Letters|Drafted Eleventh Round Letters|Drafted Tenth Round Letters|Drafted Ninth Round Letters|Drafted Eighth Round Letters|Drafted Seventh Round Letters|Drafted Sixth Round Letters|Drafted Fifth Round Letters|Drafted Fourth Round Letters|Drafted Third Round Letters|Drafted Third Round Letters Without Experian Submission|Drafted Second Round Letters|Drafted First Round Letters Without Full Credit Report Submission|Drafted First Round Letters';
		$alert_missing = 'Missing Equifax Pages|Missing Experian Pages|Missing TransUnion Pages|Credit Report Missing Pages';


		$client_alerts = $this->fetchRawData("
				SELECT * FROM (SELECT
					client_joint_id,
					client_status,
					name,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_subject` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_subjects' ORDER BY alert_id DESC LIMIT 1 ) AS `last_alert_date` ,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$restricted_alerts' ORDER BY alert_id DESC LIMIT 1 ) AS `last_restricted_alert_subject`,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_subject`,
					( SELECT alert_notes FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject REGEXP '$alert_missing' ORDER BY alert_id DESC LIMIT 1 ) AS `last_missing_alert_notes`,
					( SELECT alert_date FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_date`,
					( SELECT alert_subject FROM client_alerts WHERE client_type='joint' AND client_id = client_joints.client_joint_id AND alert_subject = 'Pending All Verifications' ORDER BY alert_date DESC, alert_id DESC LIMIT 1 ) AS `first_pending_all_alert_subject`
				FROM
					client_joints 
				WHERE auto_alert=1 AND
					client_status NOT IN ( 'Cancelled', 'Terminated', 'Archived', 'Suspended', 'NSF', 'NSF2' ) 
					AND client_joint_id IN (SELECT client_id FROM client_alerts WHERE client_type='joint' GROUP BY client_id)
					AND client_id IN (SELECT client_id FROM clients WHERE converted=1)) as `aaa` WHERE aaa.last_alert_subject IS NOT NULL ORDER BY client_joint_id ASC  LIMIT $limit OFFSET $offset
		");

		$date_today = date('m/d/Y');
		echo "Today: ".$date_today; 
		$holidays = $this->getHolidays();
		foreach ($client_alerts as $key => $alert) { 
			$client_joint_id = $alert['client_joint_id'];
			echo "<br>";
			echo $alert['name'];
			echo "<br>";
			//if ($client_joint_id == 78) {
				$alert_date = $alert['last_alert_date'];
				echo "<br>Round Alert Date: ".date('m/d/Y',strtotime($alert_date));
				 
				echo "<br>";
				if ($alert['last_restricted_alert_date'] == '') {
					if (strtotime($alert_date) < strtotime($alert['first_pending_all_alert_date'])) {
						echo $alert['first_pending_all_alert_subject'];
						echo '<br>'.$alert_date;
						$alert_template = $this->get_pending_alert_to_send_today($date_today,$alert['first_pending_all_alert_date'],$alert['first_pending_all_alert_subject'],$holidays); 
									$this->pprint($alert_template);
								if (count($alert_template) > 0) {

									$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
									// echo "send";

								} 
					} else { 
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays);
						echo "<br>First Reminder Date: ".$first_reminder;  
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);

						$third_submit = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$third_submit = $this->skip_weekend_and_holidays($third_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						echo "<br>Second Reminder: ".$second_reminder; 
						echo "<br>Third Reminder: ".$third_reminder;  
						echo "<br>First Submit: ".$first_submit;  
						echo "<br>Second Submit: ".$second_submit;
						echo "<br>Third Submit: ".$third_submit;
						echo "<br>First Deadline: ".$first_deadline;  
						echo "<br>Second Deadline: ".$second_deadline;  
						echo "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						echo "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						if ($first_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send First Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						} 
					 	if ($second_reminder == $date_today) { 
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Second Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
					 	if ($third_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Third Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
						if ($first_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send First Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($third_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($first_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send First Deadline";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send Second Deadline";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
					}
							
				} else {
					if (strtotime($alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$first_reminder = date('m/d/Y',strtotime($alert_date . ' + 14 days')); 
						$first_reminder = $this->skip_weekend_and_holidays($first_reminder,$holidays);
						echo "<br>First Reminder Date: ".$first_reminder;  
						$second_reminder = date('m/d/Y',strtotime($first_reminder . ' + 7 days'));   
						$second_reminder = $this->skip_weekend_and_holidays($second_reminder,$holidays);
						$third_reminder = date('m/d/Y',strtotime($second_reminder . ' + 7 days'));   
						$third_reminder = $this->skip_weekend_and_holidays($third_reminder,$holidays);
						$first_submit = date('m/d/Y',strtotime($third_reminder . ' + 7 days'));   
						$first_submit = $this->skip_weekend_and_holidays($first_submit,$holidays);
						$second_submit = date('m/d/Y',strtotime($first_submit . ' + 7 days'));   
						$second_submit = $this->skip_weekend_and_holidays($second_submit,$holidays);

						$third_submit = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$third_submit = $this->skip_weekend_and_holidays($third_submit,$holidays);
						$first_deadline = date('m/d/Y',strtotime($second_submit . ' + 7 days'));   
						$first_deadline = $this->skip_weekend_and_holidays($first_deadline,$holidays);
						$second_deadline = date('m/d/Y',strtotime($first_deadline . ' + 7 days'));   
						$second_deadline = $this->skip_weekend_and_holidays($second_deadline,$holidays);
						echo "<br>Second Reminder: ".$second_reminder; 
						echo "<br>Third Reminder: ".$third_reminder;  
						echo "<br>First Submit: ".$first_submit;  
						echo "<br>Second Submit: ".$second_submit;
						echo "<br>Third Submit: ".$third_submit;
						echo "<br>First Deadline: ".$first_deadline;  
						echo "<br>Second Deadline: ".$second_deadline;  
						echo "<br>Last Alert: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date']));
						echo "<br>Restricted: ".date('m/d/Y',strtotime($alert['last_restricted_alert_date'])); 
					 	
						if ($first_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send First Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						} 
					 	if ($second_reminder == $date_today) { 
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Second Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
					 	if ($third_reminder == $date_today) {
							$alert_template = $this->get_alert_template('Reminder');
							echo "<br><br> Send Third Reminder";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
					 	}
						if ($first_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send First Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($third_submit == $date_today) {
							$alert_template = $this->get_alert_template('Submit');
							echo "<br><br> Send Second Submit";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($first_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send First Deadline";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
						if ($second_deadline == $date_today) {
							$alert_template = $this->get_alert_template('Deadline');
							echo "<br><br> Send Second Deadline";
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
						}
					} else { 

						if ($alert['last_restricted_alert_subject'] == 'Equifax, Experian, and TransUnion Reports Received') {
							if ($alert['client_status'] != 'Active' && $alert['client_status'] != 'AV' && $alert['client_status'] != 'AV2' && $alert['client_status'] != 'AVSSN' && $alert['client_status'] != 'SSN') {
								echo "<br>change to review";
								echo "<br>turn off alert";
								$client_update = $this->db->query("UPDATE client_joints SET client_status = 'Review', auto_alert=0 WHERE client_joint_id=$client_joint_id");
							}
						} else { 
							$alert_template = $this->get_pending_alert_to_send_today($date_today,$alert['last_restricted_alert_date'],$alert['last_restricted_alert_subject'],$holidays); 
							if (count($alert_template) > 0) {

								$this->pprint($alert_template);
								$this->saveAlertCron(0,$client_joint_id,'joint',$alert_template['alert_subject'],$alert_template['alert_notes']);
								// $this->pprint($alert_template);

							}  
								
						} 

						
					}
				} 
				if ($alert['last_missing_alert_date'] != '') {   
					$last_missing_alert_date = date('m/d/Y',strtotime($alert['last_missing_alert_date']));

					if (strtotime($last_missing_alert_date) > strtotime($alert['last_restricted_alert_date'])) {
						$next_missing_alert_date = date('m/d/Y',strtotime($last_missing_alert_date. ' + 2 days' ));
						// $alert_template = $this->get_alert_template_by_subject($alert['last_missing_alert_subject']); 
 
						while (strtotime($next_missing_alert_date) < strtotime($date_today)) {
							$next_missing_alert_date = date('m/d/Y',strtotime($next_missing_alert_date. ' + 2 days'));
						}  

						$next_missing_alert_date = $this->skip_weekend_and_holidays($next_missing_alert_date,$holidays);

						echo "<br>Next Missing Alert Date".$next_missing_alert_date;
						if ($date_today == $next_missing_alert_date) {
							// $this->pprint($alert_template);
							$alert_subject = $alert['last_missing_alert_subject'];
							$alert_notes = $alert['last_missing_alert_notes'];
							$this->saveAlertCron(0,$client_joint_id,'joint',$alert_subject,$alert_notes);
						}
						
					} 	 
						
				}  
			//}
		}
	}

	function get_pending_alert_to_send_today($date_today,$alert_date,$alert_subject,$holidays) {
		$alert_template = $this->get_alert_shortname($alert_subject); 
		$return = []; 
		if (count($alert_template) > 0) {
			$pending_alert_date = date('m/d/Y',strtotime($alert_date . ' + 2 days'));
			$pending_alert_date = $this->skip_weekend_and_holidays($pending_alert_date,$holidays);
			
			if ($pending_alert_date == $date_today) {
				$return = $alert_template;
			} 
			$next_pending_alert_date = date('m/d/Y',strtotime($pending_alert_date . ' + 3 days'));
			$next_pending_alert_date = $this->skip_weekend_and_holidays($next_pending_alert_date,$holidays);
			if (strtotime($date_today) >= strtotime($pending_alert_date)) { 
				while (strtotime($next_pending_alert_date) < strtotime($date_today)) {
					$next_pending_alert_date = date('m/d/Y',strtotime($next_pending_alert_date . ' + 3 days'));
					$next_pending_alert_date = $this->skip_weekend_and_holidays($next_pending_alert_date,$holidays);
				} 
				
				
				if ($date_today == $next_pending_alert_date) {
					$return = $alert_template;
				}
			} 
			echo "<br>Last Alert Date: ". date('m/d/Y',strtotime($alert_date));
			echo "<br>Pending Alert Date: ".$pending_alert_date;
			echo "<br>Next Pending Alert Date: ".$next_pending_alert_date; 
		} 

		return $return;
	}

	function get_alert_shortname($alert_subject) { 
		switch ($alert_subject) {
			case strpos($alert_subject, 'Equifax Reports Received'):
				$shortname = 'XP/TU Pending';
				break;
			case strpos($alert_subject, 'Experian Reports Received'):
				$shortname = 'EF/TU Pending';
				break;
			case strpos($alert_subject, 'TransUnion Reports Received'):
				$shortname = 'EF/XP Pending';
				break;
			case strpos($alert_subject, 'Equifax and TransUnion Reports Received'):
				$shortname = 'XP Pending';
				break;
			case strpos($alert_subject, 'Equifax and Experian Reports Received'):
				$shortname = 'TU Pending';
				break;
			case strpos($alert_subject, 'Experian and TransUnion Reports Received'):
				$shortname = 'EF Pending';
				break; 
			case strpos($alert_subject, 'Pending All Verifications'):
				$shortname = 'Verifs';
				break; 
			default:
				$shortname = 'no send';
				break;
		}  
		if ($shortname != 'no send') {
			return $this->get_alert_template($shortname);
		} else { 
			return []; 
		}
	}

	function get_alert_template($shortname) {
		$alert_templates = $this->fetchRawData("SELECT * FROM client_alerts_templates WHERE template_shortname='$shortname'"); 
		$data['alert_subject'] = $alert_templates[0]['template_subject'];
		$data['alert_notes'] = $alert_templates[0]['template_notes']; 
		return $data;
	}
	function get_alert_template_by_subject($subject) {
		$alert_templates = $this->fetchRawData("SELECT * FROM client_alerts_templates");
		$alert_template_index = array_search($subject, array_column($alert_templates, 'template_subject'));
		$data['alert_subject'] = $alert_templates[$alert_template_index]['template_subject'];
		$data['alert_notes'] = $alert_templates[$alert_template_index]['template_notes']; 
		return $data;
	}

	function getHolidays() {
		$json = file_get_contents('https://www.googleapis.com/calendar/v3/calendars/en.usa%23holiday%40group.v.calendar.google.com/events?key=AIzaSyDpvJ8smtYNdm8sO5_xKrznC2b9nn_OrKY');
		$obj = json_decode($json,TRUE); 
		$days = array();
		foreach ($obj['items'] as $key => $holiday) {
			array_push($days, date('m/d/Y',strtotime($holiday['start']['date'])));
		}

		return $days;
	}

	function getHolidaysAll() {
		$json = file_get_contents('https://www.googleapis.com/calendar/v3/calendars/en.usa%23holiday%40group.v.calendar.google.com/events?key=AIzaSyDpvJ8smtYNdm8sO5_xKrznC2b9nn_OrKY');
		$obj = json_decode($json,TRUE); 
		$days = array();
		foreach ($obj['items'] as $key => $holiday) {
			array_push($days, array($holiday['summary'],date('m/d/Y',strtotime($holiday['start']['date']))));
		}

		$this->pprint($days);
	}


	function testing() {
		$date = '01/01/2019';
		$date = $this->skip_weekend_and_holidays($date);
		echo $date;
	}

	 

	function get_client_primary_card_info($id,$type,$month_list_lates) {
		if ($type == 'client') {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM clients WHERE client_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing);
		}


		// RESTRICTIONS
		$restrictions = array('Cancelled','Terminated','Archived');
		if (!in_array($client['client_status'], $restrictions)) {
			// ASSIGN DATA
			$email = $client['email_address'];
			$plan = $billing['plan'];
			$monthly_fee = $billing['monthly_fee'];
			// ADD 15 to monthly fee
            $late_fee = number_format(15 * count($month_list_lates['list']),2); 
            $sub_total = number_format((count($month_list_lates['list']) * $monthly_fee) + $late_fee,2);
			
			$card_info['number'] = $billing['card_number'];
	        $card_info['expiration_date'] = $billing['card_expiration'];
	        $card_info['code']  = $billing['cvv_code'];

	        $full_name = explode(' ', $billing['card_holder']);
	        $last_name = $full_name[count($full_name) -1];
	        unset($full_name[count($full_name)  -1]); 
	        $first_name = implode(' ', $full_name);

			$shipping_address['first_name'] = $first_name;
	        $shipping_address['last_name'] = $last_name;
	        $shipping_address['company'] = '';
	        $shipping_address['address'] = $billing['address'];
	        $shipping_address['city'] = $billing['city'];
	        $shipping_address['state'] = $billing['state'];
	        $shipping_address['zip'] = $billing['zip'];
	        $shipping_address['country'] = 'USA';
	        $shipping_address['phone_number'] = $client['cell_phone'];
	        $shipping_address['fax_number'] = '';
			// $this->pprint($this->input->post());

			$month_names = implode(', ', $month_list_lates['list']);
			$this->pprint($month_names);
			$this->pprint($email);
			$this->pprint($plan);
			$this->pprint($sub_total);
			$this->pprint($card_info);
			$this->pprint($shipping_address);
			echo 'charge this card';
			// CHARGE MONTHLY
			$this->chargeAuthorizeCreditCardMonthly($email, $plan, $sub_total, $card_info, $shipping_address, $month_names);
		}
			
	}

	function get_client_primary_card_info_setup_fee($id,$type) {
		if ($type == 'client') {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM clients WHERE client_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,setup_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,setup_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing);
		}


		// RESTRICTIONS
		$restrictions = array('Cancelled','Terminated','Archived');
		if (!in_array($client['client_status'], $restrictions)) {
			// ASSIGN DATA
			$email = $client['email_address'];
			$plan = $billing['plan'];
			$setup_fee = $billing['setup_fee'];
			// ADD 15 to monthly fee 
            $sub_total = number_format($setup_fee,2);
			
			$card_info['number'] = $billing['card_number'];
	        $card_info['expiration_date'] = $billing['card_expiration'];
	        $card_info['code']  = $billing['cvv_code'];

	        $full_name = explode(' ', $billing['card_holder']);
	        $last_name = $full_name[count($full_name) -1];
	        unset($full_name[count($full_name)  -1]); 
	        $first_name = implode(' ', $full_name);

			$shipping_address['first_name'] = $first_name;
	        $shipping_address['last_name'] = $last_name;
	        $shipping_address['company'] = '';
	        $shipping_address['address'] = $billing['address'];
	        $shipping_address['city'] = $billing['city'];
	        $shipping_address['state'] = $billing['state'];
	        $shipping_address['zip'] = $billing['zip'];
	        $shipping_address['country'] = 'USA';
	        $shipping_address['phone_number'] = $client['cell_phone'];
	        $shipping_address['fax_number'] = '';
			// $this->pprint($this->input->post());

			$month_names = date('F');
			// $this->pprint($month_names);
			// $this->pprint($email);
			// $this->pprint($plan);
			// $this->pprint($sub_total);
			// $this->pprint($card_info);
			// $this->pprint($shipping_address);
			// echo 'charge this card';
			// CHARGE MONTHLY
			$this->chargeAuthorizeCreditCardSetupFee($email, $plan, $sub_total, $card_info, $shipping_address, $month_names,$id,$type);
		}
			
	}


	function chargeAuthorizeCreditCardSetupFee($email, $plan, $setup_fee, $card_info, $shipping_address, $month_name,$id,$type) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Plan Setup Fee ($' . $setup_fee . ')');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		// $customerAddress->setCompany($shipping_address['company']);
		// $customerAddress->setAddress($shipping_address['address']);
		// $customerAddress->setCity($shipping_address['city']);
		// $customerAddress->setState($shipping_address['state']);
		// $customerAddress->setZip($shipping_address['zip']);
		// $customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($setup_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					// echo "success_" . $tresponse->getTransId();
					// // echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					// echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					// echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					// echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					// echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
					if ($type == 'client') {
						$this->load->model('clients');
						$clients = new Model_clients();
						$clients->load($id);
						$clients->converted = 1;
						$clients->converted_date = date('Y-m-d H:i:s');
						$clients->save();
					} else {
						$this->load->model('client_joints');
						$client_joints = new Model_client_joints();
						$client_joints->load($id);

						$this->load->model('clients');
						$clients = new Model_clients();
						$clients->load($client_joints->client_id);
						$clients->converted = 1;
						$clients->converted_date = date('Y-m-d H:i:s');
						$clients->save();
					}
						


					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com');

					$this->email->subject("Transaction Success");
					$this->email->message('Charged Setup Fee for '.$shipping_address['first_name'].' '.$shipping_address['last_name']);

					if ($this->email->send()) { 
					} else { 
					}
				} else {
					// echo "error_";
					// echo "Transaction Failed <br>";
					// if ($tresponse->getErrors() != null) {
					// 	echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					// 	echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					// }

					$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
					$message = "System failed to charge $full_name Setup Fee";
					$this->sendRCSms('+18139517686', $message);
 

					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com');

					$this->email->subject("Transaction Failed");
					$this->email->message($message);

					if ($this->email->send()) { 
					} else { 
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				// echo "error_";
				// echo "Transaction Failed <br>";
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getErrors() != null) {
					// echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					// echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				} else {
					// echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
					// echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				}

				$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
				$message = "System failed to charge $full_name Setup Fee";
				$this->sendRCSms('+18139517686', $message);
 

				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);
				$this->email->initialize($config);
				$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
				$this->email->to('joshuasaubon@gmail.com');

				$this->email->subject("Transaction Failed");
				$this->email->message($message);

				if ($this->email->send()) { 
				} else { 
				}
			}
		} else {
			//echo "No response returned <br>";
		}

		return $response;
	}


	function chargeAuthorizeCreditCardMonthly($email, $plan, $monthly_fee, $card_info, $shipping_address, $month_name) {
		/* Create a merchantAuthenticationType object with authentication details
	       retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_info['number']);
		$creditCard->setExpirationDate($card_info['expiration_date']);
		$creditCard->setCardCode($card_info['code']);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber(time());
		$order->setDescription($plan . ' Monthly Fee For '.$month_name.' Services');

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($shipping_address['first_name']);
		$customerAddress->setLastName($shipping_address['last_name']);
		// $customerAddress->setCompany($shipping_address['company']);
		// $customerAddress->setAddress($shipping_address['address']);
		// $customerAddress->setCity($shipping_address['city']);
		// $customerAddress->setState($shipping_address['state']);
		// $customerAddress->setZip($shipping_address['zip']);
		// $customerAddress->setCountry($shipping_address['country']);

		// // Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId('');
		// $customerData->setEmail($email);

		// // Add values for transaction settings
		// $duplicateWindowSetting = new AnetAPI\SettingType();
		// $duplicateWindowSetting->setSettingName("duplicateWindow");
		// $duplicateWindowSetting->setSettingValue("60");

		// // Add some merchant defined fields. These fields won't be stored with the transaction,
		// // but will be echoed back in the response.
		// $merchantDefinedField1 = new AnetAPI\UserFieldType();
		// $merchantDefinedField1->setName("customerLoyaltyNum");
		// $merchantDefinedField1->setValue("0407");

		// $merchantDefinedField2 = new AnetAPI\UserFieldType();
		// $merchantDefinedField2->setName("favoriteColor");
		// $merchantDefinedField2->setValue("green");

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($monthly_fee);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setCustomer($customerData);
		// $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		// $transactionRequestType->addToUserFields($merchantDefinedField1);
		// $transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					// echo "success_" . $tresponse->getTransId();
					// // echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
					// echo " Transaction Response Code: " . $tresponse->getResponseCode() . "<br>";
					// echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "<br>";
					// echo " Auth Code: " . $tresponse->getAuthCode() . "<br>";
					// echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "<br>";
				} else {
					// echo "error_";
					// echo "Transaction Failed <br>";
					// if ($tresponse->getErrors() != null) {
					// 	echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
					// 	echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
					// }


					$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
					$message = "System failed to charge $full_name for ".date('F')." Service.";
					$this->sendRCSms('+18139517686', $message);
 

					$this->load->library('email');
					$config = Array(
						'mailtype' => 'html',
					);
					$this->email->initialize($config);
					$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com','brent@creditlynx.com');

					$this->email->subject("Transaction Failed");
					$this->email->message($message);

					if ($this->email->send()) { 
					} else { 
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				// echo "error_";
				// echo "Transaction Failed <br>";
				// $tresponse = $response->getTransactionResponse();

				// if ($tresponse != null && $tresponse->getErrors() != null) {
				// 	echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "<br>";
				// 	echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "<br>";
				// } else {
				// 	echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "<br>";
				// 	echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "<br>";
				// }

				$full_name = $shipping_address['first_name'] . ' ' . $shipping_address['last_name'];
				$message = "System failed to charge $full_name for ".date('F')." Service.";
				$this->sendRCSms('+18139517686', $message);
 

				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);
				$this->email->initialize($config);
				$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
					$this->email->to('joshuasaubon@gmail.com','brent@creditlynx.com');

				$this->email->subject("Transaction Failed");
				$this->email->message($message);

				if ($this->email->send()) { 
				} else { 
				}
			}
		} else {
			echo "No response returned <br>";
		}

		return $response;
	}

 	function get_id($card_holder,$card_number) {
 		$response = [];
		if ($card_number != '') {
			$data = $this->fetchRawData("SELECT id,name,type FROM view_client_cards WHERE name=\"$card_holder\" AND RIGHT(card_number,4)=RIGHT('$card_number',4)");
			$response = $data;
		}

		return $response;

 	}


 	


 	public function sendEmailInvoice($id,$type,$month_list_lates) {
 		if ($type == 'client') {
 			$client_data = $this->fetchRawData("SELECT email_address FROM clients WHERE client_id=$id");
 		} else { 
 			$client_data = $this->fetchRawData("SELECT email_address FROM client_joints WHERE client_joint_id=$id");
 		}
		$client_data = reset($client_data);
		$to = reset($client_data);
 

		if ($type == 'client') {
 			$billing_data = $this->fetchRawData("SELECT monthly_fee,card_number,card_holder FROM client_billings WHERE client_id=$id");
 		} else { 
 			$billing_data = $this->fetchRawData("SELECT monthly_fee,card_number,card_holder FROM client_joint_billings WHERE client_joint_id=$id");
 		}
 		$billing_data = reset($billing_data);
 		$monthly_fee = $billing_data['monthly_fee'];
		 

		$session = password_hash('creditlynx' . time(), PASSWORD_BCRYPT);
		$session = str_replace('$', 'X1Rs4XsQln', $session);

		$data['body'] = 'There was an issue with you recent payment.  Please click the link below to review your account information.';
		$data['href'] = 'https://admin.creditlynx.com/emailCampaign/invoice?id=' . $session;
		$data['name'] = $this->input->post('name');
		$data['to'] = $to; 
		if (count($month_list_lates['lates']) > 0) {
            $late_fee = number_format(15 * count($month_list_lates['lates']),2);
        } else {
            $late_fee = 0;
        }
        $sub_total = number_format((count($month_list_lates['list']) * $monthly_fee) + $late_fee,2);
        
		$data['amount'] = $sub_total;
		

		// $this->pprint($month_list);
		$body = $this->load->view('invoice/email_invoice', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('billing@creditlynx.com', 'Credit Lynx');
		$this->email->to($to);

		$this->email->subject("Opps. There's something wrong with your account...");
		$this->email->message($body);

		if ($this->email->send()) {
			echo "invoice sent";
		} else {
			echo "error";
		}

	 

		$this->save_invoice($session,$id,$type,$month_list_lates);


		
	}

	function get_last_invoice_no() {
		$data = $this->fetchRawData("SELECT MAX(invoice_id) as `last_id` FROM client_invoices");
		return count($data) > 0 ? $data[0]['last_id'] : 0;
	}

	function invoice_num ($input, $pad_len = 7, $prefix = null) {
	    if ($pad_len <= strlen($input))
	        trigger_error('<strong>$pad_len</strong> cannot be less than or equal to the length of <strong>$input</strong> to generate invoice number', E_USER_ERROR);

	    if (is_string($prefix))
	        return sprintf("%s%s", $prefix, str_pad($input, $pad_len, "0", STR_PAD_LEFT));

	    return str_pad($input, $pad_len, "0", STR_PAD_LEFT);
	}

	function save_invoice($session,$id,$type,$month_list_lates) {
		// $data['pay_later_date'] = $this->fetchRawData("SELECT pay_date FROM clients_pay_later WHERE id=$id AND type='$type' AND DATE(pay_date) > CURRENT_DATE");
 
		if ($type == 'client') {
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone FROM clients WHERE client_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else { 
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client);  
			$billing = $this->fetchRawData("SELECT plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing); 
		}


		$data['session'] = $session;
		$data['to'] = $client['email_address'];
		$data['invoice_number'] = $this->invoice_num($this->get_last_invoice_no(),7,'C-');;
		$data['invoice_date'] = date('m/d/Y');
		$data['name'] = $client['name'];
		$data['full_address'] = $billing['address']. ', ' . $billing['city']. ', ' . $billing['state']. ', ' . $billing['zip'];
		$data['address'] = $billing['address'];
		$data['city'] = $billing['city'];
		$data['state'] = $billing['state'];
		$data['zip'] = $billing['zip'];
		$data['cell_phone'] = $client['cell_phone'];
		$data['plan'] = $billing['plan'];
		$data['monthly_fee'] = $billing['monthly_fee'];
		$data['card_number'] = substr($billing['card_number'], strlen($billing['card_number']) -4 );

		$data['month_list'] = $month_list_lates['list'];
		$data['month_lates'] = $month_list_lates['lates'];
		$data['no_overdue'] = $month_list_lates['no_overdue'];
		$data['no_overdue_date'] = $month_list_lates['no_overdue_date'];


		// $this->pprint($data);
 	
		$invoice = $this->load->view('invoice/invoice_template', $data, TRUE); 

		$invoice_path = FCPATH.'application/views/invoices/'.$data['invoice_number'].'.php'; 
		
		if ( ! write_file($invoice_path, $invoice))
		{
			 echo 'Unable to write the file'; 
		}
		else
		{
		    echo 'File written!';

			if ($type == 'client') {
				$this->load->model('Model_client_documents');
				$client_documents = new Model_client_documents(); 
				$client_documents->client_id = $id;
				$client_documents->category = 'Invoice '.$data['invoice_number'];
				$client_documents->file_download = $session;
				$client_documents->file_size = '';
				$client_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_documents->save();
			} else {
				$this->load->model('Model_client_joint_documents');
				$client_joint_documents = new Model_client_joint_documents(); 
				$client_joint_documents->client_joint_id = $id;
				$client_joint_documents->category = 'Invoice '.$data['invoice_number'];
				$client_joint_documents->file_download = $session;
				$client_joint_documents->file_size = '';
				$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_joint_documents->save();
			}
			$invoice_no = $data['invoice_number'];
			$invoice_date = date('Y-m-d');
			$this->db->query("INSERT INTO client_invoices (id,type,session_id,invoice_no,invoice_html,invoice_date,status) VALUES ($id,'$type','$session','$invoice_no','','$invoice_date','Unpaid')");
			
		}
			

	}
	 
	function get_client_cards($id,$type) {
		$data = $this->fetchRawData("SELECT GROUP_CONCAT(RIGHT(`card_number`,4) SEPARATOR ',') as `card_numbers`,
											GROUP_CONCAT(`name` SEPARATOR \"','\") as `card_holders`
											FROM view_client_cards WHERE id=$id AND type='$type'");
		return $data;
	}

	 

	function get_item_list_for_receipt_declined($submitted_local,$id,$type) { 
 
		$month_list = [date('F',strtotime($submitted_local))];
		$counter = 1; 

		$client_cards = $this->get_client_cards($id,$type); 
		$client_cards = reset($client_cards);
		$card_numbers = $client_cards['card_numbers'];
		$card_holders = $client_cards['card_holders'];
		$card_holders = strtolower($card_holders);

		$init = $this->fetchRawData("SELECT submitted_local,status FROM authorize_transactions WHERE LCASE(`name`) IN ('$card_holders') AND RIGHT(account_number,4) IN ('$card_numbers')");


		if (count($init) == 0 || count($init) == 1) {
			
		} else { 
			while ($counter < 10) { 
					$date = date('Y-m-d',strtotime($submitted_local. " - $counter months"));
				
					$data = $this->fetchRawData("SELECT submitted_local,status FROM authorize_transactions WHERE `name` IN ('$card_holders') AND RIGHT(account_number,4) IN ('$card_numbers') AND YEAR(submitted_local) = YEAR('$date') AND MONTH(submitted_local) = MONTH('$date') ORDER BY submitted_local DESC");
					
					if (count($data) > 0) {  
						// $this->pprint(array_column($data, 'status'));
						if (!in_array('settledSuccessfully', array_column($data, 'status')) && !in_array('capturedPendingSettlement', array_column($data, 'status'))) {
							array_push($month_list, date('F',strtotime($date))); 
						} 
					} 
				 
				$counter++;   
			}
		}
			

		
		return $month_list;
	}

	function get_receipt($id,$type,$month_list_lates) {
		if ($type == 'client') {
 			$client_data = $this->fetchRawData("SELECT email_address,name FROM clients WHERE client_id=$id");
 		} else { 
 			$client_data = $this->fetchRawData("SELECT email_address,name FROM client_joints WHERE client_joint_id=$id");
 		}
		$client_data = reset($client_data); 
 

		if ($type == 'client') {
 			$billing_data = $this->fetchRawData("SELECT plan,monthly_fee,payment_method,card_number,card_holder FROM client_billings WHERE client_id=$id");
 		} else { 
 			$billing_data = $this->fetchRawData("SELECT plan,monthly_fee,payment_method,card_number,card_holder FROM client_joint_billings WHERE client_joint_id=$id");
 		}
 		$billing_data = reset($billing_data);

		$data['to'] = $client_data['email_address'];
		$data['payment_method'] = $billing_data['payment_method'];
		$data['card_number'] = substr($billing_data['card_number'], strlen($billing_data['card_number']) -4);
		$data['name'] = $client_data['name'];
		$data['month_list'] = $month_list_lates['list'];

		$data['plan'] = $billing_data['plan'];
		$data['monthly_fee'] = $billing_data['monthly_fee']; 
		$data['month_lates'] = $month_list_lates['lates'];
		$data['no_overdue'] = $month_list_lates['no_overdue'];
		$data['no_overdue_date'] = $month_list_lates['no_overdue_date'];
		// $this->load->view('invoice/email_receipt',$data);

		$body = $this->load->view('invoice/email_receipt', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('billing@creditlynx.com', 'Receipt from Credit Lynx');
		$this->email->to($data['to']);

		$this->email->subject("Receipt from Credit Lynx");
		$this->email->message($body);

		// echo "send";

		if ($this->email->send()) {
			echo "receipt sent";
		} else {
			echo "error";
		}

		$receipt_path = FCPATH.'application/views/receipts/'.$data['name'].' '.date('Y-m-d').'.php'; 
		
		if ( ! write_file($receipt_path, $body))
		{
			 // echo 'Unable to write the file'; 
		}
		else
		{
			if ($type == 'client') {
				$this->load->model('Model_client_documents');
				$client_documents = new Model_client_documents(); 
				$client_documents->client_id = $id;
				$client_documents->category = 'Receipt '.$data['name'].' '.date('Y-m-d');
				$client_documents->file_download = $data['name'].' '.date('Y-m-d');
				$client_documents->file_size = '';
				$client_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_documents->save();
			} else {
				$this->load->model('Model_client_joint_documents');
				$client_joint_documents = new Model_client_joint_documents(); 
				$client_joint_documents->client_joint_id = $id;
				$client_joint_documents->category = 'Receipt '.$data['name'].' '.date('Y-m-d');
				$client_joint_documents->file_download = $data['name'].' '.date('Y-m-d');
				$client_joint_documents->file_size = '';
				$client_joint_documents->date_uploaded = date('Y-m-d H:i:s');
				$client_joint_documents->save();
			}
		}

			
	}
 
 

	function get_monthly_due($id,$type) {
 		if ($type == 'client') {
 			$data = $this->fetchRawData("SELECT monthly_due FROM client_billings WHERE client_id=$id");
 		} else {
 			$data = $this->fetchRawData("SELECT monthly_due FROM client_joint_billings WHERE client_joint_id=$id");
 		}
 		
 		$data = reset($data);
 		$data = reset($data); 
 		return $data; 
 	}

 	function get_month_list_lates($monthly_due,$submitted_local,$month_list) {
 		$late_fee_count = date('d',strtotime($monthly_due)) == date('d',strtotime($submitted_local)) ? 0 : 1;
		$late_fee_count = $late_fee_count + (count($month_list) -1); 
		$month_list = array_reverse($month_list);

		if ($late_fee_count == count($month_list)) {
			$month_lates = $month_list;
		} else { 
			$month_lates = $month_list;
			unset($month_lates[count($month_lates) -1]);
		} 
		$month_list_lates['list'] = $month_list;
		$month_list_lates['lates'] = $month_lates; 
		$month_list_lates['no_overdue'] = date('F',strtotime($monthly_due));
		$month_list_lates['no_overdue_date'] = date('d',strtotime($monthly_due));
		return $month_list_lates;
 	}

	function get_last_declined_transaction($id,$type) {
		$client_cards = $this->get_client_cards($id,$type); 
		$client_cards = reset($client_cards);
		$card_numbers = $client_cards['card_numbers'];
		$card_holders = $client_cards['card_holders'];

		$data = $this->fetchRawData("SELECT submitted_local FROM authorize_transactions WHERE `name` IN ('$card_holders') AND RIGHT(account_number,4) IN ('$card_numbers') ORDER BY submitted_local DESC");

		$data = reset($data);
		$data = reset($data);
		return $data;
	}

	// 1 AM
	function recharge_yesterday_declined_transactions() {
 		// GET YESTERDAY DECLINED TRANSACTIONS
 		$data = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions WHERE DATE(submitted_local) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) AND LCASE(`name`) NOT IN (SELECT LCASE(`name`) FROM authorize_transactions WHERE YEAR(submitted_local)=YEAR(DATE_SUB(CURRENT_DATE,INTERVAL 1 DAY)) AND MONTH(submitted_local)=MONTH(DATE_SUB(CURRENT_DATE,INTERVAL 1 DAY)) AND status IN ('settledSuccessfully','capturedPendingSettlement') ) AND LCASE(`name`) IN (SELECT LCASE(view_client_cards.name) FROM view_client_cards WHERE LCASE(view_client_cards.`name`) = LCASE(authorize_transactions.name)) GROUP BY LCASE(`name`) )  
						AND status NOT IN ('settledSuccessfully','capturedPendingSettlement','voided')
					ORDER BY
					NAME");
 		// $data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) AND status NOT IN ('settledSuccessfully','capturedPendingSettlement')");
 		$this->pprint($data);
		$restrictions = array('Cancelled','Terminated','Archived');
		$message = '';
 		foreach ($data as $key => $value) {  
			$data_client = $this->get_id($value['name'],$value['account_number']); 
			// $this->pprint($data_client);
			if (count($data_client) > 0) {
				foreach ($data_client as $k => $val) {

					if (!in_array($this->get_client_status($val['id'],$val['type']), $restrictions)) {
						// 33 is for Emma Only FOR TESTING

						// if ($val['id'] == '33' || $val['type'] == 'client') { 
							$monthly_due = $this->get_monthly_due($val['id'],$val['type']);
							$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$val['id'],$val['type']);
							$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
							// $this->pprint($month_list_lates);
							if ($this->check_valid($val['id'],$val['type'])) {

								// echo "charge";
								$this->get_client_primary_card_info($val['id'],$val['type'],$month_list_lates); 
								$message = "System Recharged Yesterday Declined Transaction to ".$value['name']; 
								$this->sendRCSms('+18139517686', $message);

								$this->load->library('email');
								$config = Array(
									'mailtype' => 'html',
								);
								$this->email->initialize($config);
								$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
								$this->email->to('joshuasaubon@gmail.com');

								$this->email->subject('recharged yesterday declined transactions');
								$this->email->message($message);

								if ($this->email->send()) {
									echo "sent";
								} else {
									echo "error";
								}
								
							}
								
						// }  
					}
				}
					
			}
				
				
 		}


					 		
 	}

 	function send_client_invoice_manual() {
 		$id = $this->input->post('id');
 		$type = $this->input->post('type');
 		$monthly_due = $this->get_monthly_due($id,$type);
		$month_list = $this->get_item_list_for_receipt_declined(date('Y-m-d H:i:s'),$id,$type);
		$month_list_lates = $this->get_month_list_lates($monthly_due,date('Y-m-d H:i:s'),$month_list);
			// $this->pprint($month_list_lates);
		$this->sendEmailInvoice($id,$type,$month_list_lates) ; 
 	}
 
 	// 10 AM
 	function get_today_transactions() {
 		$data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE");
 		$data_success = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND status IN ('settledSuccessfully','capturedPendingSettlement')" );
 		// $this->pprint($data);
 		$not_success = array('settledSuccessfully','capturedPendingSettlement');
 		$invoice_names = array();
 		$receipt_names = array();
 		foreach ($data as $key => $value) { 
 			if (!in_array($value['status'],$not_success)) {
 				if (!in_array($value['name'], array_column($data_success, 'name'))) {
 					$data_client = $this->get_id($value['name'],$value['account_number']); 
 					if (count($data_client) > 0) {
 						// if ($data_client['id'] == '33') { 
	 						$monthly_due = $this->get_monthly_due($data_client['id'],$data_client['type']);
							$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$data_client['id'],$data_client['type']);
							$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
	 						// $this->pprint($month_list_lates);
		 					$this->sendEmailInvoice($data_client['id'],$data_client['type'],$month_list_lates) ; 
		 					array_push($invoice_names, $value['name']);
						// }
 					}
						
 				} 	
 			}  
 			if ($value['status'] == 'settledSuccessfully' || $value['status'] == 'capturedPendingSettlement') {
 				$data_client = $this->get_id($value['name'],$value['account_number']);
 				// $this->pprint($data_client);
 				if (count($data_client) > 0) {
 					// if ($data_client['id'] == '33') {
	 					$monthly_due = $this->get_monthly_due($data_client['id'],$data_client['type']);
		 				$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$data_client['id'],$data_client['type']);
		 				$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
		 				
	 					// $this->pprint($value['name']);
	 					// $this->pprint($month_list_lates);
	 					$this->get_receipt($data_client['id'],$data_client['type'],$month_list_lates);
	 					array_push($receipt_names, $value['name']);
			
					// } 
 				}
	 				
 			}
 		}

		$message_invoice = "System Sent Invoice to ".implode("\n", $invoice_names); 
		$this->sendRCSms('+18139517686', $message_invoice);
		

		$message_receipt = "System Sent Receipt to ".implode("\n", $receipt_names) ;
		$this->sendRCSms('+18139517686', $message_receipt);

		echo $message_invoice;
		echo "<br>";
		echo $message_receipt;

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to('joshuasaubon@gmail.com');

		$this->email->subject('Receipt and Invoice');
		$this->email->message($message_receipt. ' ' . $message_invoice);

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}

		
 	}
 
 	// 1 AM
	function get_pay_date_today() {
		$data = $this->fetchRawData("SELECT * FROM clients_pay_later WHERE DATE(pay_date) = CURRENT_DATE");

		$restrictions = array('Cancelled','Terminated','Archived');
		foreach ($data as $key => $value) { 
			$id = $value['id'];
			$type = $value['type'];
			// if ($id == 33 && $type == 'client') { 
				if (!in_array($this->get_client_status($id,$type), $restrictions)) {
					$monthly_due = $this->get_monthly_due($id,$type);
					$submitted_local = $this->get_last_declined_transaction($id,$type);
					$month_list = $this->get_item_list_for_receipt_declined($submitted_local,$id,$type);
					$month_list_lates = $this->get_month_list_lates($monthly_due,$submitted_local,$month_list);
					$this->pprint($month_list_lates);
					if ($this->check_valid($id,$type)) {
						$this->get_client_primary_card_info($id,$type,$month_list_lates);  
					}
					
				} 

					
			// }		
		}
	}

	function get_client_status($id,$type) {
		if ($type == 'client') {
			$this->load->model('Model_clients');
			$clients = new Model_clients();
			$clients->load($id); 
		} else {
			$this->load->model('Model_client_joints');
			$clients = new Model_client_joints();
			$clients->load($id); 
		}
		return $clients->client_status;
	}

	// 1:30 AM
	function get_today_monthly_due() {
		$data = $this->Model_Query->getView('view_all_payments_today', ['day_due' => date('d')]);
		$this->pprint($data);
		$names_amount = "";
		foreach ($data as $key => $value) {
			if ($value['client_type'] == 'client') {
				if ($this->check_valid($value['client_id'],$value['client_type'])) {  
					$names_amount .= $value['name'].": $".$value['monthly_fee'] . "\n";
					$this->charge_client_monthly_due($value['client_id'],$value['client_type'],date('F'));
				} 
			} else {
				if ($this->check_valid($value['client_joint_id'],$value['client_type'])) { 
					$names_amount .= $value['name'].": $".$value['monthly_fee'] . "\n";
					$this->charge_client_monthly_due($value['client_joint_id'],$value['client_type'],date('F'));
				} 
			} 
		}


		$message = "System Charged \n".$names_amount." for ".date('F')." Service.";
		$this->sendRCSms('+18139517686', $message);
		echo $message;

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);
		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to('joshuasaubon@gmail.com');

		$this->email->subject('Transaction Today');
		$this->email->message($message);

		if ($this->email->send()) {
			echo "sent";
		} else {
			echo "error";
		}

	}
 

	

	function get_today_setup_fee_due() {
		// $data = $this->Model_Query->getView('view_all_payments_today', ['DAY(STR_TO_DATE(`monthly_due`,"%m/%d/%Y"))' => date('d')]);
		$data = $this->fetchRawData("SELECT (SELECT name FROM clients WHERE client_id=client_billings.client_id) as `name`,client_billings.* FROM client_billings WHERE DATE(STR_TO_DATE(`paid`,'%m/%d/%Y'))=CURRENT_DATE AND client_id IN (SELECT client_id FROM clients WHERE client_status NOT IN ('Archived','Terminated','Cancelled'))");
		$names_amount = "";

		foreach ($data as $key => $value) { 
			// if ($value['client_id'] == 33) {
				if ($this->check_valid($value['client_id'],'client')) {  
					$names_amount .= $value['name'].": $".$value['setup_fee'] . "\n"; 

					$this->get_client_primary_card_info_setup_fee($value['client_id'],'client');
				}  
			// }
				
		}

		if ($names_amount != '') {
			$message = "System Charged \n".$names_amount." Setup Fee";
			$this->sendRCSms('+18139517686', $message);
			// echo $message;

			$this->load->library('email');
			$config = Array(
				'mailtype' => 'html',
			);
			$this->email->initialize($config);
			$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
			$this->email->to('joshuasaubon@gmail.com');

			$this->email->subject('Setup Fee Transaction Today');
			$this->email->message($message);

			if ($this->email->send()) {
				echo "sent";
			} else {
				echo "error";
			}
		}


			

	}

	function check_valid($id,$type) {
		$data = $this->fetchRawData("SELECT * FROM subscription_list WHERE id=$id AND type='$type' ORDER BY subs_id DESC LIMIT 1");
		$return = true;
		if (count($data) > 0) {
			foreach ($data as $key => $value) {
				if ($value['status'] == 'Cancelled') {
					$return = false;
				}
			}
		} 

		return $return;
	}

	function testing123() {
		$data = $this->Model_Query->getView('view_all_payments_today', ['DAY(STR_TO_DATE(`monthly_due`,"%m/%d/%Y"))' => date('d')]);
		$this->pprint($data);
	}

	function charge_client_monthly_due($id,$type,$month_name) {
		if ($type == 'client') {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM clients WHERE client_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT plan,monthly_fee,card_holder,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);
		} else {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT plan,monthly_fee,card_holder,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_joint_id=$id");
			$billing = reset($billing);
		}

 
		// ASSIGN DATA
		$email = $client['email_address'];
		$plan = $billing['plan'];
		$monthly_fee = $billing['monthly_fee']; 
		
		$card_info['number'] = $billing['card_number'];
        $card_info['expiration_date'] = $billing['card_expiration'];
        $card_info['code']  = $billing['cvv_code'];

        $full_name = explode(' ', $billing['card_holder']);
        $last_name = $full_name[count($full_name) -1];
        unset($full_name[count($full_name)  -1]); 
        $first_name = implode(' ', $full_name);

		$shipping_address['first_name'] = $first_name;
        $shipping_address['last_name'] = $last_name;
        $shipping_address['company'] = '';
        $shipping_address['address'] = $billing['address'];
        $shipping_address['city'] = $billing['city'];
        $shipping_address['state'] = $billing['state'];
        $shipping_address['zip'] = $billing['zip'];
        $shipping_address['country'] = 'USA';
        $shipping_address['phone_number'] = $client['cell_phone'];
        $shipping_address['fax_number'] = '';
		// $this->pprint($this->input->post());
 
		$this->pprint($month_name);
		$this->pprint($email);
		$this->pprint($plan);
		$this->pprint($monthly_fee);
		$this->pprint($card_info);
		$this->pprint($shipping_address);
		echo 'charge this card';
		// CHARGE MONTHLY
		$this->chargeAuthorizeCreditCardMonthly($email, $plan, $monthly_fee, $card_info, $shipping_address, $month_name);

		
	
	}

	function auto_email_leads() {
		$leads = $this->fetchRawData("SELECT
										cell_phone,
										name,
										email_address,
										'client',
										lead_status,
										date_created

									FROM
										clients 
									WHERE
										converted = 0 
										AND ( client_status <> 'Archived' AND lead_status <> 'Archived' AND lead_status <> ''  AND lead_status <> '0')
										
										UNION ALL 
										
									SELECT
										cell_phone,
										name,
										email_address,
										'joint',
										(SELECT lead_status FROM clients WHERE client_id=client_joints.client_id) as `lead_status`,
										(SELECT date_created FROM clients WHERE client_id=client_joints.client_id) as `date_created`
									FROM
										client_joints 
									WHERE
										client_id IN (SELECT client_id FROM clients WHERE converted=0 AND client_status <> 'Archived' AND lead_status <> 'Archived'  AND lead_status <> ''  AND lead_status <> '0')
										AND  client_status <> 'Archived'  
										");
		foreach ($leads as $key => $lead) {
			echo $lead['name'] . ' ' . $lead['lead_status'] .' ' . $lead['date_created'] . '<br>';
		}
	}

	function auto_email_clients() {
		$clients = $this->fetchRawData("SELECT
										cell_phone,
										name,
										email_address,
										'client',
										client_status,
										date_created 
									FROM
										clients 
									WHERE
										converted = 1 
										AND  client_status NOT IN ('Archived','Cancelled','Terminated')  
										
										UNION ALL 
										
									SELECT
										cell_phone,
										name,
										email_address,
										'joint',
										(SELECT client_status FROM clients WHERE client_id=client_joints.client_id) as `client_status`,
										(SELECT date_created FROM clients WHERE client_id=client_joints.client_id) as `date_created`
									FROM
										client_joints 
									WHERE
										client_id IN (SELECT client_id FROM clients WHERE converted=1 AND client_status NOT IN ('Archived','Cancelled','Terminated')  )
										AND  client_status NOT IN ('Archived','Cancelled','Terminated')  
										");
		foreach ($clients as $key => $client) {
			echo $client['name'] . ' ' . $client['client_status'] .' ' . $client['date_created'] . '<br>';
		}
	}

	function auto_email_brokers() {
		$brokers = $this->fetchRawData("SELECT * FROM brokers");
		foreach ($brokers as $key => $broker) {
			echo $broker['first_name'] . ' ' . $broker['last_name'] . ' #'. $this->phone_trim($broker['cell']) . '<br>';
		}
	}

	function get_clients_converted_today() {
		$data = $this->fetchRawData("SELECT * FROM clients WHERE converted=1 AND DATE(converted_date)=CURRENT_DATE");;
		$client_names = array_column($data, 'name');
		$client_names = implode(',', $client_names);
		// $this->pprint($client_names);
		$message = "Client today: ".$client_names; 
		// echo $message;
		$this->sendRCSms('+18139517686', $message);
	}

	function cron_cmntn_flow() {
		$data = $this->fetchRawData("SELECT * FROM clients WHERE converted=0 AND lead_status='New'");
		$this->pprint($data);
	}


 	function get_client_other_card_info($id,$type,$month_list_lates,$other_index) {
		if ($type == 'client') {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM clients WHERE client_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_billings WHERE client_id=$id");
			$billing = reset($billing);

			// GET CLIENT OTHER BILLING INFO
			$billing_other = $this->fetchRawData("SELECT card_holder,card_number,card_exp as `card_expiration`,card_cvv as `cvv_code`,shipping_city as `city`,shipping_state as `state`,shipping_zip as `zip`,shipping_address as `address` FROM client_other_cards WHERE id=$id AND type='client' ORDER BY coc_id DESC");
			// $billing_other = reset($billing_other);
		} else {
			// GET CLIENT PROFILE INFO
			$client = $this->fetchRawData("SELECT name,email_address,cell_phone,client_status FROM client_joints WHERE client_joint_id=$id");
			$client = reset($client); 

			// GET CLIENT BILLING INFO
			$billing = $this->fetchRawData("SELECT card_holder,plan,monthly_fee,card_number,card_expiration,cvv_code,city,state,zip,address FROM client_joint_billings WHERE client_id=$id");
			$billing = reset($billing);

			// GET CLIENT OTHER BILLING INFO
			$billing_other = $this->fetchRawData("SELECT card_holder,card_number,card_exp as `card_expiration`,card_cvv as `cvv_code`,shipping_city as `city`,shipping_state as `state`,shipping_zip as `zip`,shipping_address as `address` FROM client_other_cards WHERE id=$id AND type='joint' ORDER BY coc_id DESC");
			// $billing_other = reset($billing_other);
			
		}
		// $this->pprint($billing_other);
		if (array_key_exists($other_index -1, $billing_other)) {   
			

			// RESTRICTIONS
			$restrictions = array('Cancelled','Terminated','Archived');
			if (!in_array($client['client_status'], $restrictions)) {
				// ASSIGN DATA
				$email = $client['email_address'];
				$plan = $billing['plan'];
				$monthly_fee = $billing['monthly_fee'];
				// ADD 15 to monthly fee
	            $late_fee = number_format(15 * count($month_list_lates['list']),2); 
	            $sub_total = number_format((count($month_list_lates['list']) * $monthly_fee) + $late_fee,2);
				
				$card_info['number'] = $billing_other[$other_index -1]['card_number'];
		        $card_info['expiration_date'] = $billing_other[$other_index -1]['card_expiration'];
		        $card_info['code']  = $billing_other[$other_index -1]['cvv_code'];

		        $full_name = explode(' ', $billing_other[$other_index -1]['card_holder']);
		        $last_name = $full_name[count($full_name) -1];
		        unset($full_name[count($full_name)  -1]); 
		        $first_name = implode(' ', $full_name);

				$shipping_address['first_name'] = $first_name;
		        $shipping_address['last_name'] = $last_name;
		        $shipping_address['company'] = '';
		        $shipping_address['address'] = $billing_other[$other_index -1]['address'];
		        $shipping_address['city'] = $billing_other[$other_index -1]['city'];
		        $shipping_address['state'] = $billing_other[$other_index -1]['state'];
		        $shipping_address['zip'] = $billing_other[$other_index -1]['zip'];
		        $shipping_address['country'] = 'USA';
		        $shipping_address['phone_number'] = $client['cell_phone'];
		        $shipping_address['fax_number'] = '';
				// $this->pprint($this->input->post());

				$month_names = implode(', ', $month_list_lates['list']);
				$this->pprint($month_names);
				$this->pprint($email);
				$this->pprint($plan);
				$this->pprint($sub_total);
				$this->pprint($card_info);
				$this->pprint($shipping_address);
				echo 'charge this card';
				// CHARGE MONTHLY
				$this->chargeAuthorizeCreditCardMonthly($email, $plan, $sub_total, $card_info, $shipping_address, $month_names);
				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);
				$this->email->initialize($config);
				$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
				$this->email->to('joshuasaubon@gmail.com');

				$this->email->subject('Recharged '.$client['name'].' '.$other_index.' Other Card ');
				$this->email->message('');

				if ($this->email->send()) {
					echo "sent";
				} else {
					echo "error";
				}
			}
		}

			
			
	}


	// 2nd try
	function recharge_today_declined_transactions_2() {
 		// GET YESTERDAY DECLINED TRANSACTIONS
 		$data = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND LCASE(`name`) NOT IN (SELECT LCASE(`name`) FROM authorize_transactions WHERE YEAR(submitted_local)=YEAR(CURRENT_DATE) AND MONTH(submitted_local)=MONTH(CURRENT_DATE) AND status IN ('settledSuccessfully','capturedPendingSettlement')) GROUP BY LCASE(`name`) )  
						AND status NOT IN ('settledSuccessfully','capturedPendingSettlement','voided')
					ORDER BY
					NAME");
 		// $data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND status NOT IN ('settledSuccessfully','capturedPendingSettlement')");
 		// $this->pprint($data);
		$restrictions = array('Cancelled','Terminated','Archived');
 		foreach ($data as $key => $value) {  
			$data_client = $this->get_id($value['name'],$value['account_number']); 
			// $this->pprint($data_client);
			if (count($data_client) > 0) {
				if (!in_array($this->get_client_status($data_client['id'],$data_client['type']), $restrictions)) {
					// 33 is for Emma Only FOR TESTING
					// if ($data_client['id'] == '33' || $data_client['type'] == 'client') { 
						$monthly_due = $this->get_monthly_due($data_client['id'],$data_client['type']);
						$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$data_client['id'],$data_client['type']);
						$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
						// $this->pprint($month_list_lates);
						if ($this->check_valid($data_client['id'],$data_client['type'])) {
							$this->get_client_other_card_info($data_client['id'],$data_client['type'],$month_list_lates,1); 
							// $message = "System Recharged Yesterday Declined Transaction to ".$value['name']; 
							// $this->sendRCSms('+18139517686', $message);
						}
							
					// }  
				}
			}
				
				
 		}
 	}
	
	// 3nd try
	function recharge_today_declined_transactions_3() {
 		// GET YESTERDAY DECLINED TRANSACTIONS
 		$data = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND LCASE(`name`) NOT IN (SELECT LCASE(`name`) FROM authorize_transactions WHERE YEAR(submitted_local)=YEAR(CURRENT_DATE) AND MONTH(submitted_local)=MONTH(CURRENT_DATE) AND status IN ('settledSuccessfully','capturedPendingSettlement')) GROUP BY LCASE(`name`) )  
						AND status NOT IN ('settledSuccessfully','capturedPendingSettlement','voided')
					ORDER BY
					NAME");
 		// $data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND status NOT IN ('settledSuccessfully','capturedPendingSettlement')");
 		// $this->pprint($data);
		$restrictions = array('Cancelled','Terminated','Archived');
 		foreach ($data as $key => $value) {  
			$data_client = $this->get_id($value['name'],$value['account_number']); 
			// $this->pprint($data_client);
			if (count($data_client) > 0) {
				if (!in_array($this->get_client_status($data_client['id'],$data_client['type']), $restrictions)) {
					// 33 is for Emma Only FOR TESTING
					// if ($data_client['id'] == '33' || $data_client['type'] == 'client') { 
						$monthly_due = $this->get_monthly_due($data_client['id'],$data_client['type']);
						$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$data_client['id'],$data_client['type']);
						$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
						// $this->pprint($month_list_lates);
						if ($this->check_valid($data_client['id'],$data_client['type'])) {
							$this->get_client_other_card_info($data_client['id'],$data_client['type'],$month_list_lates,2); 
							// $message = "System Recharged Yesterday Declined Transaction to ".$value['name']; 
							// $this->sendRCSms('+18139517686', $message);
						}
							
					// }  
				}
			}
				
				
 		}
 	}
	// 4nd try
	function recharge_today_declined_transactions_4() {
 		// GET YESTERDAY DECLINED TRANSACTIONS
 		$data = $this->fetchRawData("SELECT 
					*
					FROM
						authorize_transactions 
					WHERE
						t_id IN ( SELECT MAX( t_id ) FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND LCASE(`name`) NOT IN (SELECT LCASE(`name`) FROM authorize_transactions WHERE YEAR(submitted_local)=YEAR(CURRENT_DATE) AND MONTH(submitted_local)=MONTH(CURRENT_DATE) AND status IN ('settledSuccessfully','capturedPendingSettlement')) GROUP BY LCASE(`name`) )  
						AND status NOT IN ('settledSuccessfully','capturedPendingSettlement','voided')
					ORDER BY
					NAME");
 		// $data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE DATE(submitted_local) = CURRENT_DATE AND status NOT IN ('settledSuccessfully','capturedPendingSettlement')");
 		// $this->pprint($data);
		$restrictions = array('Cancelled','Terminated','Archived');
 		foreach ($data as $key => $value) {  
			$data_client = $this->get_id($value['name'],$value['account_number']); 
			// $this->pprint($data_client);
			if (count($data_client) > 0) {
				if (!in_array($this->get_client_status($data_client['id'],$data_client['type']), $restrictions)) {
					// 33 is for Emma Only FOR TESTING
					// if ($data_client['id'] == '33' || $data_client['type'] == 'client') { 
						$monthly_due = $this->get_monthly_due($data_client['id'],$data_client['type']);
						$month_list = $this->get_item_list_for_receipt_declined($value['submitted_local'],$data_client['id'],$data_client['type']);
						$month_list_lates = $this->get_month_list_lates($monthly_due,$value['submitted_local'],$month_list);
						// $this->pprint($month_list_lates);
						if ($this->check_valid($data_client['id'],$data_client['type'])) {
							$this->get_client_other_card_info($data_client['id'],$data_client['type'],$month_list_lates,3); 
							// $message = "System Recharged Yesterday Declined Transaction to ".$value['name']; 
							// $this->sendRCSms('+18139517686', $message);
						}
							
					// }  
				}
			}
				
				
 		}
 	}
	
 

		

}
 