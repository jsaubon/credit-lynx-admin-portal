<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'ocr/vendor/autoload.php';
use thiagoalessio\TesseractOCR\TesseractOCR;

class Testing extends MY_controller {

	function testOcr() {
		echo (new TesseractOCR(base_url('assets/images/cl_logo.jpg')))->run();
	}

	function getModel() {
		$this->load->model('Model_notes');
		$notes = new Model_notes();
		$data = $notes->search(['note_id' => 591]);
		// $this->pprint($notes->getThis());

		$this->pprint($data);

	}

	public function trim_str1($str) {
		$str = trim($str);
		$str = strip_tags($str);
		$str = addslashes($str);
		return $str;
	}

	function uploadCapture() {
		// new filename
		$filename = 'pic_' . date('YmdHis') . '.jpeg';

		$url = '';
		if (move_uploaded_file($_FILES['webcam']['tmp_name'], 'assets/images/camera/' . $filename)) {
			$url = 'http://sandbox.creditlynx.com/assets/images/camera/' . $filename;
			$this->recognize($url);
		}

		// Return image url
		// echo $url;
	}

	function uploadCaptureSPED() {
		// new filename
		$filename = 'pic_' . date('YmdHis') . '.jpeg';

		$url = '';
		if (move_uploaded_file($_FILES['webcam']['tmp_name'], 'assets/images/camera/' . $filename)) {
			$url = 'http://sandbox.creditlynx.com/assets/images/camera/' . $filename;
		}
		echo '-';
		$image_url = $url;
		$api_credentials = array(
			'key' => 'acc_2589ac1571605e2',
			'secret' => 'ccdb4ef695edf915bd76776191ba9153',
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.imagga.com/v2/tags?image_url=' . $image_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_USERPWD, $api_credentials['key'] . ':' . $api_credentials['secret']);

		$response = curl_exec($ch);
		curl_close($ch);

		echo $response;

		// $json_response = json_decode($response);
		// var_dump($json_response);
		// Return image url
		// echo $url;
	}

	function uploadCaptureManual() {
		$url = '';
		file_put_contents(
			$this->input->post('fileName'),
			base64_decode($this->input->post('fileData'))
		);
		$url = 'http://sandbox.creditlynx.com/' . $this->input->post('fileName');

		// Return image url
		echo $url;
	}

	function recognize($url) {
		$APP_ID = 'b911067a';
		$APP_KEY = '66b5528abe5075627cd5046811b7fff4';
		$API_URL = 'https://api.kairos.com';
		$queryUrl = $API_URL . "/recognize";
		$request = curl_init($queryUrl);
		$request_params = array(
			"image" => $url,
			"gallery_name" => 'cswd',
		);
		// set curl options
		curl_setopt($request, CURLOPT_POST, true);
		curl_setopt($request, CURLOPT_POSTFIELDS, json_encode($request_params));
		curl_setopt($request, CURLOPT_HTTPHEADER, array(
			"Content-type: application/json",
			"app_id:" . $APP_ID,
			"app_key:" . $APP_KEY,
		));
		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($request);

		$response = json_decode($response, TRUE);
		if (isset($response['images'][0]['candidates'])) {
			$face_id = $response['images'][0]['candidates'][0]['face_id'];
			header("Access-Control-Allow-Origin: *");
			echo 'naa-' . $face_id . '-' . $url;
		} else {
			// $this->enroll($url);
			echo "wala-" . $url;
		}
		// // close the session
		curl_close($request);
	}

}
