<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "3rd-party/ActiveCampaign/vendor/activecampaign/api-php/includes/ActiveCampaign.class.php";

class LynxCampaign extends MY_Controller {

	function getClientsLeads() {
		$data = $this->fetchRawData("SELECT * FROM view_sync_to_ac WHERE email != '' AND email IS NOT NULL");
		return $data;
	}
	function getClientsLeadsTest() {
		$data = $this->fetchRawData("SELECT * FROM view_sync_to_ac WHERE email != '' AND email IS NOT NULL");
		// return $data;
		$this->pprint($data);
	}

	public function addToList() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");
		/*
			 * ADD OR EDIT CONTACT (TO THE NEW LIST CREATED ABOVE).
		*/
		$data = $this->getClientsLeads();
		$list_id = 4;
		$contact = array(
			"email" => "test131231@example.com",
			"name" => "Test124443",
			"phone" => "(513) 546-5477",
			"field[6]" => 'New',
			"field[7]" => 'Client',
			"p[{$list_id}]" => $list_id,
			"status[{$list_id}]" => 1, // "Active" status
		);
		$contact_sync = $ac->api("contact/sync", $contact);
		if (!(int) $contact_sync->success) {
			// request failed
			echo "<p>Syncing contact failed. Error returned: " . $contact_sync->error . "</p>";
			// exit();
		} else {
			echo "Success";
		}
	}
	public function addFieldToList() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");
		/*
			 * ADD OR EDIT CONTACT (TO THE NEW LIST CREATED ABOVE).
		*/
		$list_id = 4;
		$field = array(
			"title" => "Type",
			"type" => "1",
			"req" => "0",
			"perstag" => "Type",
			"p[{$list_id}]" => $list_id,
		);
		$list_field = $ac->api("list/field_add", $field);
		if (!(int) $list_field->success) {
			// request failed
			echo "<p>Syncing contact failed. Error returned: " . $list_field->error . "</p>";
			// exit();
		} else {
			echo "Success";
		}
	}

	function viewList() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");

		$list_view = $ac->api("list/view?id=4");
		$this->pprint($list_view);
		if (!(array) $list_view->success) {
			// request failed
			echo "<p>Syncing contact failed. Error returned: " . $list_view->error . "</p>";
			// exit();
		} else {
			$this->pprint($list_view);
		}
	}

	function get_automation_list() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");

		$email_address = $this->input->post('email_address');

		$response = $ac->api("automation/list?offset=0&limit=50&api_output=json");

		$automation_list = array();
		$client_automations = array();
		foreach ($response as $key => $automation) {
			if (is_object($automation)) { 
				$automation_list[$automation->id] = $automation->name;
				$result = $this->get_automation_contact_list($email_address,$automation->id);
				if ($result != false) {
					array_push($client_automations, $result);
				}
			}
		}
 
		$data['automation_list'] = $automation_list;
		$data['client_automations'] = $client_automations;
		echo json_encode($data);
	}

	function get_automation_contact_list($email,$automation_id) {
		// $automation_id = 6;
		// $email = 'kayci.baumann@gmail.com';
		// $name = 'Credit Tips';
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");


		$response = $ac->api("automation/contact/list?automation=$automation_id&offset=0&limit=50");

		$matched = false;
		foreach ($response as $key => $contact) {
			if (is_object($contact)) { 
				if ($contact->email == $email && $contact->automation_status == 1) {
					$matched = true;
				}
			}
		}
		return $matched ? $automation_id : false; 
	}

	function get_automation_contact_list_test() {
		$automation_id = 6;
		$email = 'kayci.baumann@gmail.com'; 
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");


		$response = $ac->api("automation/contact/list?automation=$automation_id&offset=0&limit=50");

		$this->pprint($response);
		$matched = false;
		foreach ($response as $key => $contact) {
			if (is_object($contact)) { 
				if ($contact->email == $email) {
					$matched = true;
				}
			}
		}
		return $matched ? $automation_id : false; 
	}

	function add_contact_to_automation() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");

		$email_address = $this->input->post('email_address');
		$automation_id = $this->input->post('automation_id');

		$post_data = array(
		    "contact_email" => $email_address, // include this or contact_id
		    "automation" => $automation_id, // one or more
		);
		$response = $ac->api("automation/contact/add", $post_data);


		echo json_encode($response);
	}


	function remove_contact_to_automation() {
		$ac = new ActiveCampaign("https://creditlynx.api-us1.com", "34dcd9d7a54495ba6063626b838ae4b07200d384039b09c4d775c6780c8f0cca0bd3cc15");

		$email_address = $this->input->post('email_address');
		$automation_id = $this->input->post('automation_id');

		$post_data = array(
		    "contact_email" => $email_address, // include this or contact_id
		    "automation" => $automation_id, // one or more
		);
		$response = $ac->api("automation/contact/remove", $post_data);
 
		echo json_encode($response);
	}

}

/* End of file ActiveCampaign.php */
/* Location: ./application/controllers/ActiveCampaign.php */