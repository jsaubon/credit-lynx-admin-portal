<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require '3rd-party/PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Leadsrain extends MY_controller {
	public function __contruct() {
		parent::__contruct();
		$this->checklog();
	}
	public function index() {
		// redirect(base_url('admin/login'));
		$this->checklog();
	}

	function getClientsNamePhone() {
		$this->load->model('Model_clients');
		$clients = new Model_clients();
		$clients->selects = [
			"'38580' as `username`",
			"'5cf866dc1ce932d915bc59942f48142b6239cf67' as `api_key`",
			'2650425 as `list_id`',
			"'CHECK_DUPLICATE_IN_LIST' as `check_duplicate`",
			'name as `first_name`',
			'cell_phone as `phone_number`',
		];
		$data_clients = $clients->searchWithSelect([]);
		// $this->pprint($data_clients);
		foreach ($data_clients as $key => $post_values) {
			$post_url = 'http://s3.leadsrain.com/leadsrain/api/add_posted_lead_dup.php';
			$response = $this->postData($post_values, $post_url);
			echo $response;
			// $this->passLeadsToLR($value);
		}

		$this->load->model('Model_client_joints');
		$client_joints = new Model_client_joints();
		$client_joints->selects = [
			"'38580' as `username`",
			"'5cf866dc1ce932d915bc59942f48142b6239cf67' as `api_key`",
			'2650425 as `list_id`',
			"'CHECK_DUPLICATE_IN_LIST' as `check_duplicate`",
			'name as `first_name`',
			'cell_phone as `phone_number`',
		];
		$data_client_joints = $client_joints->searchWithSelect([]);
		// $this->pprint($data_client_joints);
		foreach ($data_client_joints as $key => $post_values) {
			$post_url = 'http://s3.leadsrain.com/leadsrain/api/add_posted_lead_dup.php';
			$response = $this->postData($post_values, $post_url);
			echo $response;
			// $this->passLeadsToLR($value);
		}
	}

	function postData($post_values, $post_url) {
		$post_string = "";

		foreach ($post_values as $key => $value) {$post_string .= "$key=" . urlencode($value) . "&";}

		$post_string = rtrim($post_string, "& ");
		//echo $post_url.'?'.$post_string.'<br><br>';
		$request = curl_init($post_url); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.

		$post_response = curl_exec($request); // execute curl post and store results in $post_response
		curl_close($request);
		return $post_response;
	}

	function getClientGrade($name) {
		// $name = 'Darlene Jacobs';
		// $name = 'Autumn Becker';
		// $name = $this->input->post('name');
		$data = $this->fetchRawData("SELECT * FROM authorize_transactions WHERE name='$name'");
		$declined = 0;
		foreach ($data as $key => $value) {
			if ($value['status'] == 'declined') {
				$declined++;
			}
		}
		if ($declined == 0) {
			$grade = 'A';
		} else if ($declined == 1) {
			$grade = 'B';
		} else if ($declined == 2) {
			$grade = 'C';
		} else if ($declined == 3) {
			$grade = 'D';
		} else {
			$grade = 'F';
		}

		$data = $this->fetchRawData("SELECT client_id FROM clients WHERE name='$name' ORDER BY client_id DESC LIMIT 1");
		if (count($data) > 0) {
			$client_id = $data[0]['client_id'];
			$data = $this->fetchRawData("SELECT account_id FROM client_account_details WHERE client_id=$client_id AND account_type IN ('New Collection','New Charge-Off','New 30 Day Late','60 Day Late','90 Day Late','120 Day Late','150 Day Late','180 Late Payment','New Repossession','New Judgment','New Tax Lien','New Bankruptcy','New Foreclosure','New Inquiry')");
			if (count($data) > 0) {
				$grade .= '1';
			}
		} else {
			$data = $this->fetchRawData("SELECT client_joint_id FROM client_joints WHERE name='$name' ORDER BY client_joint_id DESC LIMIT 1");
			// $this->pprint($data);
			if (count($data) > 0) {
				$client_joint_id = $data[0]['client_joint_id'];
				$data = $this->fetchRawData("SELECT account_id FROM client_joint_account_details WHERE client_joint_id=$client_joint_id AND account_type IN ('New Collection','New Charge-Off','New 30 Day Late','60 Day Late','90 Day Late','120 Day Late','150 Day Late','180 Late Payment','New Repossession','New Judgment','New Tax Lien','New Bankruptcy','New Foreclosure','New Inquiry')");
				if (count($data) > 0) {
					$grade .= '1';
				}
			}
		}

		return $grade;
	}

	function generateBrokerCSV() {
		// echo "Csv File will be automatically generated. please wait ....";
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=client_leads_data.csv');
		$output = fopen('php://output', 'w');
		fputcsv($output, array('Name', 'Phone'));

		$result = $this->fetchRawData("SELECT
										CONCAT('first_name',' ','last_name'),
										cell
									FROM brokers ");
		foreach ($result as $key => $value) {
			fputcsv($output, $value);
		}
		fclose($output);
	}

	function generateClientsDataExcel() {
		// // Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// // Set document properties
		$spreadsheet->getProperties()->setCreator('Credit Lynx')
			->setTitle('Credit Lynx Client List')
			->setSubject('Clients Data');

		$sheet = $spreadsheet->getActiveSheet(0);

		// Get Clients data
		$data = $this->getClientList();
		// Get Headers

		// $this->pprint($data);
		// Add Data
		for ($i = 0; $i < count($data) - 1; $i++) {
			// echo $data[$i]['Name'];
			$sheet->setCellValue('A' . $i, $data[$i]['Name']);
			$sheet->setCellValue('B' . $i, $data[$i]['Phone']);
			$sheet->setCellValue('C' . $i, $data[$i]['Email']);
			$sheet->setCellValue('D' . $i, $data[$i]['Status']);
			$sheet->setCellValue('E' . $i, $data[$i]['Grade']);
			$sheet->setCellValue('F' . $i, $data[$i]['Joint Name']);
			$sheet->setCellValue('G' . $i, $data[$i]['Joint Phone']);
			$sheet->setCellValue('H' . $i, $data[$i]['Joint Email']);
			$sheet->setCellValue('I' . $i, $data[$i]['Joint Status']);
			$sheet->setCellValue('J' . $i, $data[$i]['Joint Grade']);
		}
		// // Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Clients');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		$this->downloadExcel($spreadsheet, 'Client List ' . date('Y-m-d'));
		echo "<script>window.close();</script>";
	}

	function generateLeadsDataExcel() {
		// // Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// // Set document properties
		$spreadsheet->getProperties()->setCreator('Credit Lynx')
			->setTitle('Credit Lynx Lead List')
			->setSubject('Leads Data');

		$sheet = $spreadsheet->getActiveSheet(0);

		// Get Clients data
		$data = $this->getLeadList();

		// Add Data
		for ($i = 0; $i < count($data) - 1; $i++) {
			$sheet->setCellValue('A' . $i, $data[$i]['Name']);
			$sheet->setCellValue('B' . $i, $data[$i]['Phone']);
			$sheet->setCellValue('C' . $i, $data[$i]['Email']);
			$sheet->setCellValue('D' . $i, $data[$i]['Joint Name']);
			$sheet->setCellValue('E' . $i, $data[$i]['Joint Phone']);
			$sheet->setCellValue('F' . $i, $data[$i]['Joint Email']);
			$sheet->setCellValue('G' . $i, $data[$i]['Lead Status']);
			$sheet->setCellValue('H' . $i, $data[$i]['Broker']);
		}
		// // Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Leads');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		$this->downloadExcel($spreadsheet, 'Lead List');
		echo "<script>window.close();</script>";
	}

	function getClientList() {
		$result = $this->fetchRawData("SELECT
										name as `Name`,
										cell_phone as `Phone`,
										email_address as `Email`,
										client_status as `Status`,
										'' as `Grade`,
										joint_name as `Joint Name`,
										joint_cell_phone as `Joint Phone`,
										joint_email_address as `Joint Email`,
										joint_client_status as `Joint Status`,
										'' as `Joint Grade`
									FROM view_clients_table WHERE converted=1 ORDER BY converted_date DESC");

		foreach ($result as $key => $value) {
			$grade = $this->getClientGrade($value['Name']);
			$joint_grade = '';
			if ($value['Joint Name'] != '') {
				$joint_grade = $this->getClientGrade($value['Joint Name']);
			}

			foreach ($value as $k => $v) {
				if ($k == 'Grade') {
					$result[$key][$k] = $grade;
				}
				if ($k == 'Joint Grade') {
					$result[$key][$k] = $joint_grade;
				}
			}
		}
		$excel_headers = array('Name' => 'Name1', 'Phone' => 'Phone', 'Email' => 'Email', 'Status' => 'Status', 'Grade' => 'Grade', 'Joint Name' => 'Joint Name', 'Joint Phone' => 'Joint Phone', 'Joint Email' => 'Joint Email', 'Joint Status' => 'Joint Status', 'Joint Grade' => 'Joint Grade');
		array_unshift($result, $excel_headers);

		return $result;
	}

	function getLeadList() {
		$result = $this->fetchRawData("SELECT
		 								name as `Name`,
		 								cell_phone as `Phone`,
		 								email_address as `Email`,
		 								joint_name as `Joint Name`,
		 								joint_cell_phone as `Joint Phone`,
		 								joint_email_address as `Joint Email`,
		 								lead_status as `Lead Status`,
		 								broker as `Broker`
		 							FROM view_clients_table WHERE converted=0 ORDER BY date_created DESC");
		$excel_headers = array('Name' => 'Name', 'Phone' => 'Phone', 'Email' => 'Email', 'Joint Name' => 'Joint Name', 'Joint Phone' => 'Joint Phone', 'Joint Email' => 'Joint Email', 'Lead Status' => 'Lead Status', 'Broker' => 'Broker');
		array_unshift($result, $excel_headers);
		return $result;
	}

	function downloadExcel($spreadsheet, $filename) {
		// // Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// // If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// // If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
	}

}

//  Username - 38580
// API key - 5cf866dc1ce932d915bc59942f48142b6239cf67
