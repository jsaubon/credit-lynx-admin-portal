<?php
require 'ringcentral/vendor/autoload.php';
require '3rd-party/Bitly.php';
use RingCentral\SDK\SDK;

/**
 *
 */
class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set(timezone_name_from_abbr("EST"));
		// $this->checkLog();
	}

	function bitly_short_url($longUrl) {
		$bitly = new Bitly();
		$params = array();
		$params['access_token'] = 'af88eb52a468f85678cc4af16a52e0ff1ba2594b';
		$params['longUrl'] = $longUrl;
		$params['domain'] = 'creditlynx.co';
		$results = $bitly->bitly_get('shorten', $params);
		// $results = bitly_get('link/lookup', $params);
		// var_dump($results);

		return $results['data']['url'];
	}


	function get_encrypted_string($all_url) {
		$this->load->library('encrypt');
		$exploded_url = explode('?', $all_url);
		if (!empty($exploded_url[1])) {
			$exploded_url[1] = $this->encrypt->encode($exploded_url[1]);
			// $this->pprint($exploded_url);
			$all_url = implode('?temp=', $exploded_url);
			
		}


		$all_url = $this->bitly_short_url($all_url);
		return $all_url;
			
	}


	function get_decrypted_string($temp) {
		$temp = str_replace(' ', '+', $temp);
		$this->load->library('encrypt');
		$temp = $this->encrypt->decode($temp);
		return $temp;
	}

	function get_default_password($name) {
		$name = explode(' ', $name);
		$first_name = $name[0];
		// $last_name = $name[count($name) -1];

		// $password = substr($first_name,0,1).$last_name;
		return $first_name;
	}

	function isJson($str) {
	    $json = json_decode($str);
	    return $json && $str != $json;
	}

	public function trim_str($str) {
		$str = trim($str);
		$str = strip_tags($str);
		$str = stripslashes($str);
		$str = str_replace("'", "\'", $str);
		return $str;
	}

	function getPushNotifications() {
		$support_tickets = new Model_support_tickets();
		$support_tickets->selects = ['*', "IF(sender = 'client',(SELECT name FROM clients WHERE client_id=sender_id),IF
  ( sender = 'agent', ( SELECT CONCAT(first_name,' ',last_name) FROM agents WHERE agent_id = sender_id ), IF
  ( sender = 'broker', ( SELECT CONCAT(first_name,' ',last_name) FROM brokers WHERE broker_id = sender_id ), IF(sender = 'joint',(SELECT name FROM client_joints WHERE client_joint_id=sender_id), '') ) )) as `name`", ];
		$data['support_tickets'] = $support_tickets->searchWithSelect(['status' => 'Pending'], 'support_id', 'ASC');
		// $data = $this->fetchArray($data);

		$this->load->model('Model_clients');
		$clients = new Model_clients();
		$this->db->group_start();
		$this->db->or_where('lead_status', '');
		$this->db->or_where('lead_status', 'New');
		$this->db->or_where('lead_status', NULL);
		$this->db->group_end();
		$data['leads'] = $clients->search(['converted' => 0, 'client_status !=' => 'Archived']);
		echo json_encode($data);
	}

	function getCallLogsByClient() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$callLogRecords = $platform->get('/account/~/extension/~/call-log', array(
			'dateFrom' => $credentials['dateFrom']))
			->json();

		$response = json_decode(json_encode($callLogRecords->records), TRUE);
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		$filterBy = $this->getClientPhoneAndEmail($client_id);
		if ($client_type == 'joint') {
			$filterBy = '+1' . $filterBy[0]['joint_cell_phone'];
		} else {
			$filterBy = '+1' . $filterBy[0]['cell_phone'];
		}
		$new_arr = array_filter($response, function ($var) use ($filterBy) {
			if ($var['direction'] == 'Inbound') {
				return ($var['from']['phoneNumber'] == $filterBy);
			} else {
				return ($var['to']['phoneNumber'] == $filterBy);
			}
		});
		echo json_encode($new_arr);
	}

	function getCallLogsByBroker() {
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$callLogRecords = $platform->get('/account/~/extension/~/call-log', array(
			'dateFrom' => $credentials['dateFrom']))
			->json();

		$response = json_decode(json_encode($callLogRecords->records), TRUE);
		$broker_id = $this->input->post('broker_id');
		// $client_type = $this->input->post('client_type');
		$filterBy = $this->getBrokerPhoneAndEmail($broker_id);
		// if ($client_type == 'joint') {
		//     $filterBy = '+1'.$filterBy[0]['joint_cell_phone'];
		// } else {
		//     $filterBy = '+1'.$filterBy[0]['cell_phone'];
		// }
		$new_arr = array_filter($response, function ($var) use ($filterBy) {
			if ($var['direction'] == 'Inbound') {
				return ($var['from']['phoneNumber'] == $filterBy);
			} else {
				return ($var['to']['phoneNumber'] == $filterBy);
			}
		});
		echo json_encode($new_arr);
	}

	function callClient() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		$to_phone = $this->getClientPhoneAndEmail($client_id);
		if ($client_type == 'joint') {
			$to_phone = $to_phone[0]['joint_cell_phone'];
		} else {
			$to_phone = $to_phone[0]['cell_phone'];
		}

		// if ($to_phone != '') {
		$credentials = $this->rcCredentials();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$response = $platform->post('/account/~/extension/~/ringout', array(
			'from' => array('phoneNumber' => '+18134409146'),
			'to' => array('phoneNumber' => '+1' . $to_phone),
		));
		$json = $response->json();
		// }

		// $lastStatus = $json->status->callStatus;
		// // Poll for call status updates
		// while ($lastStatus == 'InProgress') {
		//     $current = $platform->get($json->uri);
		//     $currentJson = $current->json();
		//     $lastStatus = $currentJson->status->callStatus;
		//     echo 'Status: ' . json_encode($currentJson->status) . PHP_EOL;
		//     sleep(2);
		// }
	}

	function rcCredentials() {
		return array(
			'username' => '+13233755956', // your RingCentral account phone number
			'extension' => '101', // or number
			'password' => 'Wrbwrx14!',
			'clientId' => '6GLLDUanQTyV-ViOrmskTQ',
			'clientSecret' => 'g8imdFg5S2q8wE2x2i02_Qt7-MYsKgQmOuBvK-mTjT8w',
			'server' => 'https://platform.devtest.ringcentral.com', // for production - https://platform.ringcentral.com
			'smsNumber' => '+13233755956', // any of SMS-enabled numbers on your RingCentral account
			'mobileNumber' => '8134409146', // your own mobile number to which script will send sms
			'dateFrom' => '2018-11-01', // dateFrom
			'dateTo' => '2018-12-01', // dateTo
			// 8139517686
		);
	}

	function rcCredentialsProduction() {
		return array(
			'username' => '+15138780181', // your RingCentral account phone number
			'extension' => '101', // or number
			'password' => 'Wrbwrx14!',
			'clientId' => 'ifBTHsEBTUKH_aSDYLVHog',
			'clientSecret' => 'Gf4C5kyGQCW76OozbI5iyg4tSA9a3eQNWgc4PFLvyajw',
			'server' => 'https://platform.ringcentral.com', // for production - https://platform.ringcentral.com
			'smsNumber' => '+15138780181', // any of SMS-enabled numbers on your RingCentral account
			'mobileNumber' => '8134409146', // your own mobile number to which script will send sms
			'dateFrom' => '2018-11-01', // dateFrom
			'dateTo' => '2019-12-01', // dateTo
			// 8139517686
		);
	}

	function sendRCSms($to_number, $message) {

		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$response = $platform
			->post('/account/~/extension/~/sms', array(
				'from' => array('phoneNumber' => $credentials['smsNumber']),
				'to' => array(
					array('phoneNumber' => $to_number),
				),
				'text' => $message,
			));

			// $this->pprint($response->json());
	}

	function sendBulkRCSms() { 
		$to_numbers = $this->input->post('to_numbers');
		$message = $this->input->post('message');
		$credentials = $this->rcCredentialsProduction();
		$rcsdk = new SDK($credentials['clientId'], $credentials['clientSecret'], $credentials['server'], 'Credit Lynx SMS/MMS/CALL', '1.0.0');
		$platform = $rcsdk->platform();
		// Authorize
		if (!$rcsdk->platform()->loggedIn()) {
			$platform->login($credentials['username'], $credentials['extension'], $credentials['password']);
		}

		$for_send_numbers = array();
		foreach ($to_numbers as $key => $value) {
			array_push($for_send_numbers, array('phoneNumber'=>$value['phoneNumber']));
		}
		foreach ($for_send_numbers as $key => $to_number) {
			$response = $platform
				->post('/account/~/extension/~/sms', array(
					'from' => array('phoneNumber' => $credentials['smsNumber']),
					'to' => array($to_number),
					'text' => $message,
				)); 
		} 

		$this->load->model('Model_client_etexts');
		foreach ($to_numbers as $key => $value) { 
			$client_etexts = new Model_client_etexts();
			$client_etexts->client_id = $value['id'];
			$client_etexts->client_type = $value['type'];
			$client_etexts->type = 'to_client';
			$client_etexts->title = '';
			$client_etexts->text_message = $message;
			$client_etexts->date = date("Y-m-d H:i:s");
			$client_etexts->phone_number = $value['phoneNumber'];
			$client_etexts->save();
		}
	}


	public function pprint($arr) {
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}

	public function fetchRawData($query) {
		$query = $this->db->query($query);
		return $query->result_array();
	}

	function checkLog() {
		$userdata = $this->session->userdata('user_data');
		$login_folder = $userdata['login_folder'];
		$this->load->library('user_agent');
		if (!empty($userdata) && $this->uri->segment(2) == 'login') {
			if ($login_folder == 'employees') {
				redirect(base_url('employees/profile'));
			} else {
				redirect(base_url('admin/dashboard'));
			}

		} else if (empty($userdata) && $this->uri->segment(2) != 'login') {
			if ($this->uri->segment(1) == 'admin') {
				redirect(base_url('admin/login'));
			} else {
				redirect(base_url('employees/login'));
			}
			// echo "string asdasd";
		}

	}

	function getCountries() {
		$data = array('Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'The Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'People s Republic of China', 'Republic of China', 'Christmas Island', 'Cocos(Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Cook Islands', 'Costa Rica', 'Cote d Ivoire', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Polynesia', 'Gabon', 'The Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea - Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'North Korea', 'South Korea', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Nagorno - Karabakh', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Antilles', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Turkish Republic of Northern Cyprus', 'Northern Mariana', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Islands', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Saint Barthelemy', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre and Miquelon', 'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'Somaliland', 'South Africa', 'South Ossetia', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor - Leste', 'Togo', 'Tokelau', 'Tonga', 'Transnistria Pridnestrovie', 'Trinidad and Tobago', 'Tristan da Cunha', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City', 'Venezuela', 'Vietnam', 'British Virgin Islands', 'Isle of Man', 'US Virgin Islands', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe');
		return $data;
	}

	function getStateProvinces() {
		$data = array('Alaska', 'Alabama', 'Arkansas', 'Arizona', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Iowa', 'Idaho', 'Illinois', 'Indiana', 'Kansas', 'Kentucky', 'Louisiana', 'Massachusetts', 'Maryland', 'Maine', 'Michigan', 'Minnesota', 'Missouri', 'Mississippi', 'Montana', 'North Carolina', 'North Dakota', 'Nebraska', 'New Hampshire', 'New Jersey', 'New Mexico', 'Nevada', 'New York', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Virginia', 'Virgin Islands', 'Vermont', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'DC');
		return $data;
	}
	function getAgents() {
		$this->load->model('Model_agents');
		$agents = new Model_agents();
		$agents->selects = ['agent_id', 'CONCAT(first_name," ",last_name) as `name`'];
		$data = $agents->searchWithSelect([], 'agent_id', 'DESC');
		// $data = $this->fetchRawData("SELECT agent_id,CONCAT(first_name,' ',last_name) as `name` FROM agents ORDER BY agent_id DESC");
		return $data;
	}
	function getBrokers() {
		$this->load->model('Model_brokers');
		$brokers = new Model_brokers();
		$brokers->selects = ['broker_id', 'CONCAT(first_name," ",last_name) as `name`'];
		$data = $brokers->searchWithSelect([], 'broker_id', 'DESC');
		// $data = $this->fetchRawData("SELECT broker_id,CONCAT(first_name,' ',last_name) as `name` FROM brokers ORDER BY broker_id DESC");
		return $data;
	}
	function getSales() {
		$this->load->model('Model_employees');
		$employees = new Model_employees();
		$data = $employees->search(['type' => 'Sales Team','status'=>1], 'employee_id', 'DESC');
		// $data = $this->fetchRawData("SELECT employee_id,name FROM employees WHERE type='Sales Team' ORDER BY employee_id DESC");
		return $data;
	}
	function getProcessors() {
		$this->load->model('Model_employees');
		$employees = new Model_employees();
		$data = $employees->search(['type' => 'Dispute Team'], 'employee_id', 'DESC');
		// $data = $this->fetchRawData("SELECT employee_id,name FROM employees WHERE type='Dispute Team' ORDER BY employee_id DESC");
		return $data;
	}

	function getSupportTicketResponses() {
		$data = array(
			'Address Change' => "In order to change your address, we will need one of the following two things:

- Two proofs of your NEW current mailing address
(Driver's License, Bank Statement, Utility Bill, Cell Phone Bill, Pay Stub, W2)

Please upload them through your online dashboard or email them to Reports@CreditLynx.com requests an address change.

Thank You!",
			'Cancellation' => "To cancel, simply fill out the cancellation form we provided you upon enrollment in your Service Agreement and email it to your Credit Specialist for cancellation or upload it through your online dashboard. Please include your name, signature and reason for cancellation.  Depending on your billing cycle there will be one last charge for the work done in the previous month since we bill arrears.

If you do not have one, please call our office at (877) 820-2425 or email us at:

Support@CreditLynx.com

Thank You!",
			'Mail Results' => "Please send original credit bureau correspondence to the following address:

228 Mill Street Suite 200
Milford, Ohio 45150

Once we receive them, we will update your account and an automatic email and text message alert will be sent to you.  If you would like to make copies for your records, please keep the copies and send us the originals.

Thank You!",
			'NSF' => "We are unable to provide assistance because your account is past due. Please bring your account current to restore all services and support. You can do this by contacting our Billing Team to update your payment information by email or phone.

Thank You!",
			'Due Date' => "Your payment is due on the ##th of the month.  Please make sure that funds are available in order for us to continue working for you.

Thank You!",
			'Score Drop' => "Thank you for your ticket.  To clarify one big important thing -
there is nothing we can do to LOWER a score.  If a score has lowered it can only be from the items below:

1) Paying on old debt and renewing the date of last activity, thus lowering your score
2) Adding new derogatory accounts to your file
3) Increasing the balance on your credit cards
4) New Inquiries

Thank You!",
		);

		return $data;
	}

	function getDisputeTeamResponses() {
		$data = array(
			'Account Update' => '',
			'New Creditor Added' => '',
			'New Inquiry Added' => '',
			'Score Drop' => '',
			'Reports Submitted' => '',
			'Reports Missing' => '',
			'New Address' => '',
		);

		return $data;

	}

	function getBillingTeamResponses() {
		$data = array(
			'Update Card' => '',
			'Original Due Date' => '',
			'Due Date Change' => '',
			'Request Extension' => '',
			'Hold Account' => '',
			'Cancel Account' => '',
		);

		return $data;

	}

	function getSupportTeamResponses() {
		$data = array(
			'Technical Issues' => '',
			'Cannot Upload' => '',
			'Other Sending Methods' => '',
			'Office Address' => '',
			'Office Email' => '',
			'Office Fax' => '',
		);

		return $data;

	}

	function getSalesTeamResponses() {
		$data = array(
			'Client Update' => '',
			'Referral Update' => '',
		);

		return $data;

	}

	public function sendEmail($send_to, $send_type, $to, $subject, $message, $date, $client_type, $from = '') {

		if ($send_to == 'Client') {
			$client_id = $to;

			$phone_email = $this->getClientPhoneAndEmail($to,$client_type);
			if ($client_type == 'joint') {
				if ($send_type == 'Email') {
					$to = $phone_email[0]['joint_email_address'];
				} else {
					$to = $phone_email[0]['joint_cell_phone'];
				}

			} else {
				if ($send_type == 'Email') {
					$to = $phone_email[0]['email_address'];
				} else {
					$to = $phone_email[0]['cell_phone'];
				}
			}

		}


		if ($send_to == 'Broker') {
			$broker_id = $to;
			$phone_email = $this->getBrokerPhoneAndEmail($broker_id);

			if ($send_type == 'Email') {
				$to = $phone_email[0]['email_address'];
			} else {
				$to = $phone_email[0]['cell'];
			}
		}

		if ($send_to == 'Agent') {
			$agent_id = $to;
			$phone_email = $this->getAgentPhoneAndEmail($agent_id);

			if ($send_type == 'Email') {
				$to = $phone_email[0]['email_address'];
			} else {
				$to = $phone_email[0]['cell'];
			}
		}

		if ($send_type == 'Email') {
			if ($to != '') {
				$this->load->library('email');
				$config = Array(
					'mailtype' => 'html',
				);

				$this->email->initialize($config);
				if ($from == '') {
					$from = 'no-reply@creditlynx.com';
				}
				$this->email->from($from, 'CreditLynx');
				$this->email->to($to);
				$this->email->subject($subject);
				$this->email->message($message);

				if ($this->email->send()) {
					// echo $to;
					// echo "sent";
					if ($send_to == 'Client') {
						$this->load->model('Model_client_emails');
						$client_emails = new Model_client_emails();
						$client_emails->client_id = $client_id;
						$client_emails->client_type = $client_type;
						$client_emails->type = 'to_client';
						$client_emails->subject = $subject;
						$client_emails->message = $message;
						$client_emails->date = $date;
						$client_emails->from = $from;
						$client_emails->to = $to;
						$client_emails->save();
					} elseif ($send_to == 'Broker') {
						$this->load->model('Model_broker_emails');
						$broker_emails = new Model_broker_emails();
						$broker_emails->broker_id = $broker_id;
						$broker_emails->type = 'to_broker';
						$broker_emails->subject = $subject;
						$broker_emails->message = $message;
						$broker_emails->date = $date;
						$broker_emails->from = $from;
						$broker_emails->to = $to;
						$broker_emails->save();
						echo $broker_emails->email_id;
					} elseif ($send_to == 'Agent') {
						$this->load->model('Model_agent_emails');
						$agent_emails = new Model_agent_emails();
						$agent_emails->agent_id = $agent_id;
						$agent_emails->type = 'to_agent';
						$agent_emails->subject = $subject;
						$agent_emails->message = $message;
						$agent_emails->date = $date;
						$agent_emails->from = $from;
						$agent_emails->to = $to;
						$agent_emails->save();
						echo $agent_emails->email_id;
					}
				} else {
					$this->email->print_debugger();
				}
			}

		} else {
			if ($to != '') {
				$this->sendRCSms($to, $subject.' '.$message);
				if ($send_to == 'Client') {
					$this->load->model('Model_client_etexts');
					$client_etexts = new Model_client_etexts();
					$client_etexts->client_id = $client_id;
					$client_etexts->client_type = $client_type;
					$client_etexts->type = 'to_client';
					$client_etexts->title = $subject;
					$client_etexts->text_message = $message;
					$client_etexts->date = $date;
					$client_etexts->phone_number = $to;
					$client_etexts->save();
				} elseif ($send_to == 'Broker') {
					$this->load->model('Model_broker_etexts');
					$broker_etexts = new Model_broker_etexts();
					$broker_etexts->broker_id = $broker_id;
					$broker_etexts->type = 'to_broker';
					$broker_etexts->title = $subject;
					$broker_etexts->text_message = $message;
					$broker_etexts->date = $date;
					$broker_etexts->save();

				} elseif ($send_to == 'Agent') {
					$this->load->model('Model_agent_etexts');
					$agent_etexts = new Model_agent_etexts();
					$agent_etexts->agent_id = $agent_id;
					$agent_etexts->type = 'to_agent';
					$agent_etexts->title = $subject;
					$agent_etexts->text_message = $message;
					$agent_etexts->date = $date;
					$agent_etexts->save();

				}
			}

		}

	}

	private function getClientPhoneAndEmail($id,$type) {
		$this->db->select('cell_phone_trimmed as `cell_phone`,email_address,joint_email_address,joint_cell_phone_trimmed as `joint_cell_phone`');
		if ($type == 'client') {
			$data = $this->Model_Query->getView('view_clients_table', ['client_id' => $id]);
		} else {
			$data = $this->Model_Query->getView('view_clients_table', ['client_joint_id' => $id]);
		}
		
		// $data = $this->fetchRawData("SELECT cell_phone_trimmed as `cell_phone`,email_address,joint_email_address,joint_cell_phone_trimmed as `joint_cell_phone` FROM view_clients_table WHERE client_id=$client_id");
		return $data;
	}

	function getBrokerPhoneAndEmail($broker_id) {
		$this->load->model('Model_brokers');
		$brokers = new Model_brokers();
		$data = $brokers->search(['broker_id' => $broker_id]);
		// $data = $this->fetchRawData("SELECT * FROM brokers WHERE broker_id=$broker_id");
		return $data;
	}

	function getAgentPhoneAndEmail($agent_id) {
		$this->load->model('Model_agents');
		$agents = new Model_agents();
		$data = $agents->search(['agent_id' => $agent_id]);
		// $data = $this->fetchRawData("SELECT * FROM agents WHERE agent_id=$agent_id");
		return $data;
	}

	function lookUpCardNumber() {
		$card_number = $this->input->post('card_number');
		// $card_number = 12345112;
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://lookup.binlist.net/' . $card_number,
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// echo $resp;
		echo $resp;
		// Close request to clear up some resources
		curl_close($curl);
	}

	function getChatBoxEmployees() {
		$contact_group = $this->input->post('contact_group');
		$this->db->select('*,(SELECT count(chat_id) FROM chat_employees WHERE chat_employees.from_type = view_chatbox_employees.user_type AND chat_employees.from_id = view_chatbox_employees.user_id AND seen=0) as `unread`');
		if ($contact_group != 'All') {

			$data = $this->Model_Query->getView('view_chatbox_employees', ['user_type' => $contact_group], 'user_name', 'ASC');
			// $data = $this->fetchRawData("SELECT *,(SELECT count(chat_id) FROM chat_employees WHERE chat_employees.from_type = view_chatbox_employees.user_type AND chat_employees.from_id = view_chatbox_employees.user_id AND seen=0) as `unread` FROM view_chatbox_employees WHERE user_type='$contact_group' ORDER BY user_name ASC");
		} else {
			$data = $this->Model_Query->getView('view_chatbox_employees', [], 'user_name', 'ASC');
			// $data = $this->fetchRawData("SELECT *,(SELECT count(chat_id) FROM chat_employees WHERE chat_employees.from_type = view_chatbox_employees.user_type AND chat_employees.from_id = view_chatbox_employees.user_id AND seen=0) as `unread` FROM view_chatbox_employees ORDER BY user_name ASC");
		}

		echo json_encode($data);
	}

	function saveChatEmployees() {
		$userdata = $this->session->userdata('user_data');
		$from_type = $userdata['login_type'];
		$from_name = $userdata['name'];
		$from_photo = base_url('assets/images/users') . '/' . $userdata['login_folder'] . '/' . $userdata['photo'];
		$from_id = $userdata['user_id'];
		$to_type = $this->input->post('to_type');
		$to_name = $this->input->post('to_name');
		$to_photo = $this->input->post('to_photo');
		$to_id = $this->input->post('to_id');
		$message = $this->input->post('message');

		$this->load->model('Model_chat_employees');
		$chat_employees = new Model_chat_employees();
		$chat_employees->from_type = $from_type;
		$chat_employees->from_name = $from_name;
		$chat_employees->from_photo = $from_photo;
		$chat_employees->from_id = $from_id;
		$chat_employees->to_type = $to_type;
		$chat_employees->to_name = $to_name;
		$chat_employees->to_photo = $to_photo;
		$chat_employees->to_id = $to_id;
		$chat_employees->message = $message;
		$chat_employees->message_date = date('Y-m-d H:i:s');
		$chat_employees->save();
	}

	function getChatEmployees() {
		$to_type = $this->input->post('to_type');
		$to_name = $this->input->post('to_name');
		$to_photo = $this->input->post('to_photo');
		$to_id = $this->input->post('to_id');

		$userdata = $this->session->userdata('user_data');

		$from_type = $userdata['login_type'];
		$from_name = $userdata['name'];
		$from_photo = base_url('assets/images/users') . '/' . $userdata['login_folder'] . '/' . $userdata['photo'];
		$from_id = $userdata['user_id'];

		// $this->load->model('Model_chat_employees');
		// $chat_employees = new Model_chat_employees();
		// $this->db->or_where(['to_type'=>$to_type,'to_name'=>$to_name,'to_id'=>$to_id,'from_type'=>$from_type,'from_name'=>$from_name,'from_id'=>$from_id]);
		// $data = $chat_employees->search(['to_type'=>$from_type,'to_name'=>$from_name,'to_id'=>$from_id,'from_type'=>$to_type,'from_name'=>$to_name,'from_id'=>$to_id]);

		$data = $this->fetchRawData("SELECT * FROM chat_employees WHERE (to_type='$to_type' AND to_name='$to_name' AND to_id='$to_id' AND from_type='$from_type' AND from_name='$from_name' AND from_id='$from_id') OR (to_type='$from_type' AND to_name='$from_name' AND to_id='$from_id' AND from_type='$to_type' AND from_name='$to_name' AND from_id='$to_id')");

		// $data = $this->db->query("UPDATE chat_employees SET seen=1 WHERE to_type='$to_type' AND to_name='$to_name' AND to_id='$to_id' AND seen=0");
		echo json_encode($data);
	}

	function getChatEmployeesUnread() {
		$userdata = $this->session->userdata('user_data');
		$to_type = $userdata['login_type'];
		$to_name = $userdata['name'];
		$to_photo = base_url('assets/images/users') . '/' . $userdata['login_folder'] . '/' . $userdata['photo'];
		$to_id = $userdata['user_id'];

		$data = $this->fetchRawData("SELECT * FROM chat_employees WHERE to_type='$to_type' AND to_name='$to_name' AND to_id='$to_id' AND seen=0");

		// $data = $this->db->query("UPDATE chat_employees SET seen=1 WHERE to_type='$to_type' AND to_name='$to_name' AND to_id='$to_id' AND seen=0");
		echo json_encode($data);
	}

	function updateUnread() {
		$to_type = $this->input->post('to_type');
		$to_name = $this->input->post('to_name');
		$to_photo = $this->input->post('to_photo');
		$to_id = $this->input->post('to_id');
		$data = $this->db->query("UPDATE chat_employees SET seen=1 WHERE from_type='$to_type' AND from_name='$to_name' AND from_id='$to_id' AND seen=0");
		echo $data;

	}

	function saveNoteTemplate() {
		$note_template_id = $this->input->post('note_template_id');
		$note_shortname = $this->input->post('note_shortname');
		$note_template = $this->input->post('note_template');

		$this->load->model('Model_note_templates');
		$note_templates = new Model_note_templates();

		if ($note_template_id != '') {
			$note_templates->note_template_id = $note_template_id;
		}
		$note_templates->note_shortname = $note_shortname;
		$note_templates->note_template = $note_template;

		$note_templates->save();

	}
	function saveCallTemplate() {
		$call_template_id = $this->input->post('call_template_id');
		$call_shortname = $this->input->post('call_shortname');
		$call_template = $this->input->post('call_template');
		$call_template_type = $this->input->post('call_template_type');

		$this->load->model('Model_call_templates');
		$call_templates = new Model_call_templates();

		if ($call_template_id != '') {
			$call_templates->call_template_id = $call_template_id;
		}
		$call_templates->call_shortname = $call_shortname;
		$call_templates->call_template = $call_template;
		$call_templates->call_template_type = $call_template_type;

		$call_templates->save();

	}

	function getNoteTemplates() {
		$data = $this->fetchRawData("SELECT * FROM note_templates ORDER BY note_shortname ASC");
		echo json_encode($data);
	}
	function getCallTemplates() {
		$call_template_type = $this->input->post('call_template_type');
		$data = $this->fetchRawData("SELECT * FROM call_templates WHERE call_template_type='$call_template_type' ORDER BY call_shortname ASC");
		echo json_encode($data);
	}
	function getNoteTemplatesInit() {
		$data = $this->fetchRawData("SELECT * FROM note_templates ORDER BY note_shortname ASC");
		return $data;
	}
	function getCallTemplatesInit() {
		$data = $this->fetchRawData("SELECT * FROM call_templates ORDER BY call_shortname ASC");
		return $data;
	}

	function saveTaskTemplate() {
		$task_template_id = $this->input->post('task_template_id');
		$task_shortname = $this->input->post('task_shortname');
		$task_template = $this->input->post('task_template');

		$this->load->model('Model_task_templates');
		$task_templates = new Model_task_templates();

		if ($task_template_id != '') {
			$task_templates->task_template_id = $task_template_id;
		}
		$task_templates->task_shortname = $task_shortname;
		$task_templates->task_template = $task_template;

		$task_templates->save();

	}

	function getTaskTemplates() {
		$data = $this->fetchRawData("SELECT * FROM task_templates ORDER BY task_shortname ASC");
		echo json_encode($data);
	}
	function getTaskTemplatesInit() {
		$data = $this->fetchRawData("SELECT * FROM task_templates ORDER BY task_shortname ASC");
		return $data;
	}

	function getTemplateHeaders() {
		$data = $this->fetchRawData("SELECT template_header FROM letter_templates GROUP BY template_header ORDER BY blt_id ASC");
		return $data;
	}

	function sendWelcomeEmail($id,$type) {

		$c_data = $this->getFirstNameAndEmailAddressByClientID($id,$type);
		$first_name = explode(' ', $c_data[0]['name']);
		$first_name = $first_name[0];
		$data['name'] = $first_name;
		$data['to'] = $c_data[0]['email_address'];

		$data['name'] = $c_data[0]['name'];
		$data['username'] = $c_data[0]['email_address'];
		$data['password'] = $first_name;
		$data['href'] = 'https://www.login.creditlynx.com/login';
		$data['to'] = $c_data[0]['email_address'];


		$body = $this->load->view('autoresponders/WelcomeEmail', $data, TRUE);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		$this->email->initialize($config);
		$this->email->from('no-reply@creditlynx.com', 'Credit Lynx');
		$this->email->to($data['to']);
		$this->email->subject('Welcome to the program!');
		$this->email->message($body);
		if ($this->email->send()) {
			// echo "Emial has been sent";
			// return true;
		} else {

		}
	}

	function getFirstNameAndEmailAddressByClientID($id,$type) {
		if ($type == 'client') {
			$data = $this->fetchRawData("SELECT name,email_address FROM clients WHERE client_id=$id");
		} else {
			$data = $this->fetchRawData("SELECT name,email_address FROM client_joints WHERE client_joint_id=$id");
		}
		
		return $data;
	}

	function updatePasswordToHash() {
		$data = $this->fetchRawData("SELECT agent_id,password FROM agents WHERE password IS NOT NULL");
		foreach ($data as $key => $broker) {
			$agent_id = $broker['agent_id'];
			$password = $broker['password'];
			$password = password_hash($password, PASSWORD_BCRYPT);
			$update = $this->db->query("UPDATE agents SET password='$password' WHERE agent_id=$agent_id");
		}
	}

	function updateFieldEmployee() {
		$employee_id = $this->input->post('employee_id');
		$field = $this->input->post('field');
		$value = $this->input->post('value');

		if ($field == 'password') {
			$value = password_hash($value, PASSWORD_BCRYPT);
		}

		$data_update = $this->db->query("UPDATE employees SET $field='$value' WHERE employee_id=$employee_id");
		echo "saved";
	}

	function getNotifications() {
		$userdata = $this->session->userdata('user_data');
		$login_type = $userdata['login_type'];
		$user_id = $userdata['user_id'];
		if ($login_type == 'Administrator') {
			$data['text_emails'] = $this->fetchRawData("SELECT * FROM (SELECT
                                    client_id,
                                    (SELECT name FROM clients WHERE client_id = client_etexts.client_id) as `client_name`,
                                    (SELECT client_type FROM clients WHERE client_id = client_etexts.client_id) as `client_type`,
                                    (SELECT converted FROM clients WHERE client_id = client_etexts.client_id) as `converted`,
                                    date,
                                    text_message AS `message`,
                                    title AS `subject`  ,
                                    'sms' as `type`
                                  FROM
                                    client_etexts
                                  WHERE
                                    type = 'from_client'
                                    AND client_id != 0  AND unread=1
                                    UNION ALL
                                    SELECT
                                    client_id,
                                    (SELECT name FROM clients WHERE client_id = client_emails.client_id) as `client_name`,
                                    (SELECT client_type FROM clients WHERE client_id = client_emails.client_id) as `client_type`,
                                    (SELECT converted FROM clients WHERE client_id = client_emails.client_id) as `converted`,
                                    date,
                                    message AS `message`,
                                    subject AS `subject` ,
                                    'email' as `type`
                                  FROM
                                    client_emails
                                  WHERE
                                    type = 'from_client'
                                    AND client_id != 0  AND unread=1) as aaa ORDER BY aaa.date DESC LIMIT 10");
		} else {
			$data['text_emails'] = [];
		}
		foreach ($data['text_emails'] as $index => $value) {
			foreach ($value as $key => $value) {
				$data['text_emails'][$index][$key] = $this->trim_str_fetch_data($value);
			}
		}

		$data['docs'] = $this->fetchRawData("SELECT * FROM ((SELECT 'client' as `type`,client_id,(SELECT name FROM clients WHERE client_id=client_documents.client_id) as `name`,category,date_uploaded FROM client_documents ORDER BY date_uploaded DESC LIMIT 30) 
			UNION ALL
			(SELECT 'joint' as `type`,client_joint_id,(SELECT name FROM client_joints WHERE client_joint_id=client_joint_documents.client_joint_id) as `name`,category,date_uploaded FROM client_joint_documents ORDER BY date_uploaded DESC LIMIT 30) ) as `aaa` ORDER BY aaa.date_uploaded DESC LIMIT 30 ");

		$data['docs_sa'] = $this->fetchRawData("SELECT * FROM ((SELECT 'client' as `type`,client_id,(SELECT name FROM clients WHERE client_id=client_documents.client_id) as `name`,(SELECT converted FROM clients WHERE client_id=client_documents.client_id) as `converted`,category,date_uploaded FROM client_documents WHERE category='Service Agreement' ORDER BY date_uploaded DESC LIMIT 30) 
			UNION ALL
			(SELECT 'joint' as `type`,client_joint_id,(SELECT name FROM client_joints WHERE client_joint_id=client_joint_documents.client_joint_id) as `name`,(SELECT converted FROM clients WHERE client_id=(SELECT client_id FROM client_joints WHERE client_joint_id=client_joint_documents.client_joint_id)) as `converted`,category,date_uploaded FROM client_joint_documents WHERE category='Service Agreement' ORDER BY date_uploaded DESC LIMIT 30) ) as `aaa` ORDER BY aaa.date_uploaded DESC LIMIT 30 ");
		echo json_encode($data);
	}

	private function trim_str_fetch_data($str) {
		$str = trim($str);
		// $str = strip_tags($str);
		$str = stripslashes($str);
		$str = str_replace("\\", '', $str);
		return $str;
	}

	function getClientId($client_joint_id) {
		$data = $this->fetchRawData("SELECT client_id FROM view_clients_table WHERE client_joint_id=$client_joint_id");
		return $data[0]['client_id'];
	}

	function getClientJointId($client_id) {
		$data = $this->fetchRawData("SELECT client_joint_id FROM view_clients_table WHERE client_id=$client_id");
		return $data[0]['client_joint_id'];
	}

	function deleteEmployee() {
		$employee_id = $this->input->post('employee_id');
		$data = $this->db->query("DELETE FROM employees WHERE employee_id='$employee_id'");
		echo "deleted";
	}
	function getClientEmails() {
		$client_id = $this->input->post('client_id');
		$client_type = $this->input->post('client_type');
		$all_emails = $this->getClientAllEmails($client_id,$client_type);
		$all_emails = implode("','", array_column($all_emails, 'email_address'));
		$data = [];
		if ($all_emails != '') {
			$data = $this->fetchRawData("SELECT * FROM client_emails WHERE `from` IN ('$all_emails') OR `to` IN ('$all_emails') ORDER BY `date` DESC");
		}
		// echo $this->db->last_query();;
		foreach ($data as $key => $value) {
			$data[$key]['message'] = strip_tags($value['message']);
		}
		// $data = $this->fetchRawData("SELECT * FROM client_emails WHERE client_id=$client_id ORDER BY email_id DESC");
		echo json_encode($data);
	}

	function getAlertTemplatesInit() {
		$this->load->model('Model_client_alerts_templates');
		$client_alerts_templates = new Model_client_alerts_templates();
		$data = $client_alerts_templates->search([], 'template_shortname', 'ASC');
		return $data;
	}
	function getAccountTypesInit() {
		$this->load->model('Model_clients_account_types');
		$clients_account_types = new Model_clients_account_types();
		$data = $clients_account_types->search([], 'account_type', 'ASC');
		return $data;
		// echo json_encode($data);
		// $this->pprint($data);
	}

	function send_email_to_client($client_id,$subject,$notes,$client_type,$from = '') { 

		$client_emails = $this->getClientAllEmails($client_id, $client_type);
		// $this->pprint($client_emails);

		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

		foreach ($client_emails as $key => $value) {
			$this->email->initialize($config);
			if ($from == '') {
				$from = 'no-reply@creditlynx.com';
			}
			$this->email->from($from, 'CreditLynx');
			$this->email->to($value['email_address']);
			$this->email->subject($subject);
			$data_template['to'] = $value['email_address'];
			$data_template['alert_subject'] = $subject;
			$data_template['alert_note'] = $notes;
			$body = $this->load->view('email_templates/alert_email_template', $data_template, TRUE);
			$this->email->message($body);

			if ($this->email->send()) {
				$this->load->model('Model_client_emails');
				$client_emails = new Model_client_emails();
				$client_emails->client_id = $value['id'];
				$client_emails->client_type = $value['type'];
				$client_emails->type = 'to_client';
				$client_emails->subject = $subject;
				$client_emails->message = $notes;
				$client_emails->date = date('Y-m-d H:i:s');
				$client_emails->from = $from;
				$client_emails->to = $value['email_address'];
				$client_emails->save();
			}
		}

		
	}

	function phone_trim($phone) {
		$phone = str_replace('(', '', $phone);
		$phone = str_replace(')', '', $phone);
		$phone = str_replace('-', '', $phone);
		$phone = str_replace(' ', '', $phone);
		return $phone;
	}

	function get_first_name($name) {
		$name = explode(' ', $name);
		$first_name = $name[0];
		return $first_name;
	}

	function send_text_alert_to_client($id,$type,$alert_subject,$alert_notes) {
		if ($type == 'client') {
			$this->load->model('Model_clients');
			$clients = new Model_clients();
			$clients->load($id);
			$to_number = $this->phone_trim($clients->cell_phone);
			$name = $this->get_first_name($clients->name);
 	
		} else {
			$this->load->model('Model_client_joints');
			$client_joints = new Model_client_joints();
			$client_joints->load($id);
			$to_number = $this->phone_trim($client_joints->cell_phone);;
			$name = $this->get_first_name($client_joints->name);
	
		}


		

		$message = '';
		$message .= "*Credit Lynx - DO NOT REPLY*\n\n";
		$message .= "$name: $alert_subject\n\n";
		$message .= "$alert_notes\n\n";
		// $message .= "Access dashboard upload here http://tiny.cc/4zhs7y\n\n";
		$message .= "*Credit Lynx - DO NOT REPLY*";

		if ($to_number != '') {
			$to_number = '+1'.$to_number; 
			$this->sendRCSms($to_number, $message);

			$this->load->model('Model_client_etexts'); 
			$client_etexts = new Model_client_etexts();
			$client_etexts->client_id = $id;
			$client_etexts->client_type = $type;
			$client_etexts->type = 'to_client';
			$client_etexts->title = '';
			$client_etexts->text_message = $message;
			$client_etexts->date = date("Y-m-d H:i:s");
			$client_etexts->phone_number = $to_number;
			$client_etexts->save(); 
			//echo $message;
		}
			
	}

	function getClientAllEmails($id, $type) {
		$data = $this->fetchRawData("SELECT * FROM view_client_emails WHERE id='$id' AND type='$type'");
		return $data;
	}

	function get_auto_alert($pk,$id,$table) {
		$data = $this->fetchRawData("SELECT auto_alert FROM $table WHERE $pk=$id LIMIT 1");
		$data = reset($data);
		$data = reset($data);
		return $data;
	}

	function json_validator($data=NULL) {
	  if (!empty($data)) {
	                @json_decode($data);
	                return (json_last_error() === JSON_ERROR_NONE);
	        }
	        return false;
	}

	function modelTable() {
		$data = $this->input->post(); 

		$this->load->model('Model_' . $data['table']);
		$model = 'Model_' . $data['table'];
		$db_table = new $model;
		if ($data['action'] == 'get') {
			if (isset($data['select'])) {
				$db_table->selects = $data['select'];
			}
			$get_data = $db_table->search(isset($data['where']) ? $data['where'] : [], $data['field'], $data['order'], $data['limit'], $data['offset'], $data['group_by']);
			echo json_encode($get_data);
		} else { 
			if ($this->isJson($data['pk'])) { 
				$where = json_decode($data['pk'],TRUE);
				$data_search = $db_table->search($where);
				// $this->pprint($data);
				if (count($data_search) > 0) {
					foreach ($data_search as $key => $value) {
						$pk = reset($value);
						$db_table = new $model;
						$db_table->load($pk);
						if (isset($data['fields'])) {
							foreach ($data['fields'] as $field => $value) {
								$db_table->$field = $value;
							} 
						}
							
						if ($data['action'] == 'save') {
							$db_table->save();
						}
						if ($data['action'] == 'delete') {
							$db_table->delete();
						}  
					}
				} else { 
					$db_table = new $model; 
					if (isset($data['fields'])) {
						foreach ($data['fields'] as $field => $value) {
							$db_table->$field = $value;
						} 
					}
						
					if ($data['action'] == 'save') {
						$db_table->save();
					}
					if ($data['action'] == 'delete') {
						$db_table->delete();
					}   
				}
					
 
				 
			} else{ 
				if ($data['pk'] != '') {
					$db_table->load($data['pk']);
				}
				
				if (isset($data['fields'])) {
					foreach ($data['fields'] as $field => $value) {
						$db_table->$field = $value;
					} 
				}
				if ($data['action'] == 'save') {
					$db_table->save();
				}
				if ($data['action'] == 'delete') {
					$db_table->delete();
				} 
				echo reset($db_table);
			} 

				
		}

	}


	function send_custom_email($subject,$name,$note,$to) {


		$this->load->library('email');
		$config = Array(
			'mailtype' => 'html',
		);

	
		$this->email->initialize($config);
		

		$from = 'no-reply@creditlynx.com';
		

		$this->email->from($from, 'CreditLynx');
		$this->email->to($to);
		$this->email->subject($subject);

		$data_template['subject'] = $subject;
		$data_template['name'] = $name;
		$data_template['note'] = $note;



		if (is_array($to)) {
			foreach ($to as $key => $to_) {
				$data_template['to'] = $to_;
				// $this->load->view('email_templates/verif_template', $data_template);
				$body = $this->load->view('email_templates/verif_template', $data_template, TRUE);
				$this->email->message($body);

				if ($this->email->send()) {

				}
			}
				
		} else {
			$data_template['to'] = $to;
			// $this->load->view('email_templates/verif_template', $data_template);
			$body = $this->load->view('email_templates/verif_template', $data_template, TRUE);
			$this->email->message($body);

			if ($this->email->send()) {

			}
		}
			
	}

}