<style>
    #sectionProfileInformation .form-group , #sectionClientAssignments .form-group , #sectionBillingInformation .form-group{
        margin-bottom: 5px !important;
    }

    #tableAlertTemplates tr td {
        line-height: 100% !important;
    }

     

</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h5 class="card-title"><?php echo $page_title ?> (<?php echo $user_count ?>)
                                        <a href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" >New <?php echo $page_title_s ?> </a> 
                                    </h5> 
                                    <div class="table-responsive" >
                                        <table id="pageTable" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>  
                                                    <th>Name</th> 
                                                    <th>Email</th>
                                                    <th>Phone</th> 
                                                    <th>Date of Birth</th>
                                                    <th>Date Paid</th>
                                                    <th>Cycle</th>
                                                    <th>Status</th>
                                                    <th>DFR</th>
                                                    <th class="text-center">Tools</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($users as $key => $value): ?>
                                                    <tr id="<?php echo $value['client_id'] ?>" >  
                                                        <td>
                                                            <a href="#" class="goToProfile"><u><?php echo $value['name'] ?></u></a>      
                                                        </td> 
                                                        <td><?php echo $value['email_address'] ?> </td>
                                                        <td><?php echo $value['cell_phone'] ?> </td>  
                                                        <td><?php echo $value['date_of_birth'] ?></td>
                                                        <td><?php echo $value['paid'] ?></td>
                                                        <td><?php echo $value['monthly_due'] ?></td>
                                                        <td><?php echo $value['client_status'] ?></td>
                                                        <td><?php echo $value['dfr'] ?></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button class="btn btn-secondary btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="mdi mdi-email"></i> Email
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="sr-only">Toggle Dropdown</span>
                                                                </button>
                                                                <div class="dropdown-menu rowTools">
                                                                    <a class="dropdown-item" href="#" value="tasks-tab">
                                                                        <i class="mdi mdi-plus-circle"></i> Add Task</a>
                                                                    <a class="dropdown-item" href="#" value="notes-tab">
                                                                        <i class="mdi mdi-plus-circle"></i> Add Note</a>
                                                                    <a class="dropdown-item" href="#" value="alerts-tab">
                                                                        <i class="mdi mdi-plus-circle"></i> Add Alert</a>
                                                                    <a id="btnArchiveClient" class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div> 

                <div id="profileSection" class="row fade"> 
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-8 col-md-8 col-sm-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item"> 
                                    <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                        Profile
                                    </a> 
                                </li>
                                <li class="nav-item"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_enrollment_info" role="tab" aria-selected="false">
                                        Enrollment 
                                    </a> 
                                </li>
                                <li class="nav-item"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                        Billing 
                                    </a> 
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"> 
                                <div class="tab-pane active show" id="tab_profile" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12 text-center b-r" > 
                                                <!-- <form id="uploadProfilePictureForm" enctype="multipart/form-data"> -->
                                                    <!-- <img src="../assets/images/LynxLogo.png" width="120"> -->
                                                    <!-- <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="" data-show-remove="false" data-height="150" /> --> 
                                                <!-- </form>    -->
                                                <br>
                                                <h3 class=" m-b-0"><u class="name"></u></h3> 
                                                <strong>Name</strong><br>
                                                <br>
                                                <div class="text-left">
                                                    <span>Username: </span>
                                                     <h3 class="username m-b-0"></h3>
                                                    <span>Password: </span><input type="text" name="profile_password" id="profile_password" class="form-control"><br>
                                                    <span>Last Login: <span class="last_login"></span></span>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-8" id="sectionClientAssignments">
                                                <h5>Agent / Broker / Sales Person</h5>
                                                <div class="form-group">
                                                    <strong>Agent</strong><br>
                                                    <select class="select2" name="profile_agent_id"  style="width: 100%">
                                                        <option value="0">Select Agent</option>
                                                        <?php foreach ($agents as $key => $value): ?>
                                                            <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <strong>Broker</strong><br>
                                                    <select class="select2" name="profile_broker_id"  style="width: 100%">
                                                        <option value="0">Select Broker</option>
                                                        <?php foreach ($brokers as $key => $value): ?>
                                                            <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>   
                                                <div class="form-group">
                                                    <strong>Sales</strong><br>
                                                    <select class="select2" name="profile_sale_id" id="sales"   style="width: 100%">
                                                        <option value="0">Select Sales</option>
                                                        <?php foreach ($sales as $key => $value): ?>
                                                            <option value="<?php echo $value['sale_id'] ?>"><?php echo $value['name'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>   
                                                <div class="form-group">
                                                    <strong>Processor</strong><br>
                                                    <select class="select2" name="profile_processor_id"  style="width: 100%">
                                                        <option value="0">Select Processor</option>
                                                        <?php foreach ($processors as $key => $value): ?>
                                                            <option value="<?php echo $value['processor_id'] ?>"><?php echo $value['name'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>   
                                                
                                            </div>  
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <strong>Status Line</strong>
                                                    <textarea cols="3" id="profile_status_line" name="profile_status_line" class="form-control" style="width: 100%"></textarea>
                                                </div>
                                            </div> 
                                        </div>
                                        <hr> 
                                        <h3>Personal Information</h3>
                                        <div class="row" id="sectionProfileInformation">
                                            <div class="col-md-6 col-xs-6 b-r"> 
                                                <div class="form-group">
                                                    <h5>Client Status</h5>
                                                    <select class="select2" name="profile_client_status" style="width: 100%">
                                                        <option value="0">Select Status</option>
                                                        <?php foreach ($client_statuses as $key => $value): ?>
                                                            <option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span class="bar"></span>
                                                </div> 
                                                <div class="form-group">
                                                    <label for="name" class="control-label">Name</label>
                                                    <input type="text" name="profile_name" class="form-control" >
                                                </div> 
                                                <div class="form-group">
                                                    <label for="address" class="control-label">Address</label>
                                                    <input type="text" name="profile_address" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="city" class="control-label">City</label>
                                                    <input type="text" name="profile_city" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <h5>State/Province</h5> 
                                                    <select class="select2" name="profile_state_province" style="width: 100%">
                                                        <option value="0">Select State/Province</option>
                                                        <?php foreach ($state_provinces as $key => $value): ?>
                                                            <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="zip_postal_code" class="control-label">Zip/Postal Code</label>
                                                    <input type="text" name="profile_zip_postal_code" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="email_address" class="control-label">Email Address</label>
                                                    <input type="email" name="profile_email_address" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="ss" class="control-label">SS#</label>
                                                    <input type="text" name="profile_ss" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-6 b-r">
                                                <div class="form-group">
                                                    <label for="alt_phone" class="control-label">Alt Phone</label>
                                                    <input type="text" name="profile_alt_phone" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="cell_phone" class="control-label">Cell Phone</label>
                                                    <input type="text" name="profile_cell_phone" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <h5 >Carrier</h5>
                                                        <select class="select2" name="profile_carrier" style="width: 100%">
                                                            <option value="0">Select Carrier</option>
                                                            <option value="ATT">ATT</option>
                                                            <option value="Boost">Boost</option>
                                                            <option value="Cricket">Cricket</option>
                                                            <option value="Metro PCS">Metro PCS</option>
                                                            <option value="Simple">Simple</option>
                                                            <option value="Sprint">Sprint</option>
                                                            <option value="TMobile">TMobile</option>
                                                            <option value="Verizon">Verizon</option>
                                                            <option value="Virgin">Virgin</option>
                                                            <option value="Do Not SMS">Do Not SMS</option>
                                                            <option value="Landline">Landline</option>
                                                        </select> 
                                                </div>
                                                <div class="form-group">
                                                    <label for="fax" class="control-label">Fax</label>
                                                    <input type="text" name="profile_fax" class="form-control" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="date_of_birth" class="control-label">Date of Birth</label>
                                                    <input type="text" name="profile_date_of_birth" class="form-control " >
                                                </div>
                                                <div class="form-group">
                                                    <label for="date_for_result" class="control-label">Date for result</label>
                                                    <input type="text" name="profile_date_for_result" class="form-control " >
                                                </div> 
                                                <h5>DFR <span id="profile_dfr"></span> days</h5>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_enrollment_info" role="tabpanel">
                                    <div class="card-body"> 
                                        <h3>Enrollment Information</h3>
                                        <div class="row" id="sectionEnrollmentInformation">
                                            <div class="col-md-6 col-xs-6 b-r">
                                                <div class="form-group">
                                                    <label for="date_of_enrollment" class="control-label">Date of Enrollment</label>
                                                    <input type="text" name="enrollment_date_of_enrollment" class="form-control ">
                                                </div> 
                                                <div class="form-group">
                                                    <label for="date_of_cancellation" class="control-label">Date of Cancellation</label>
                                                    <input type="text" name="enrollment_date_of_cancellation" class="form-control ">
                                                </div>

                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                 
                                                <div class="form-group">
                                                    <label for="reports_received" class="control-label">Credit Report</label>
                                                    <input type="text" name="enrollment_reports_received" class="form-control ">
                                                </div> 
                                                <div class="form-group">
                                                    <label for="ss_proof_received" class="control-label">Social Security</label>
                                                    <input type="text" name="enrollment_ss_proof_received" class="form-control ">
                                                </div>
                                                <div class="form-group">
                                                    <label for="service_agreement" class="control-label">Service Agreement</label>
                                                    <input type="text" name="enrollment_service_agreement" class="form-control ">
                                                </div>

                                                <div class="form-group">
                                                    <label for="address_verification_received" class="control-label">Address Verification Received</label>
                                                    <input type="text" name="enrollment_address_verification_received" class="form-control ">
                                                    <input type="text" name="enrollment_address_verification_received_2" class="form-control ">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_billing_info" role="tabpanel">
                                    <div class="card-body"> 
                                        <h3>Billing Information</h3>
                                        <div class="row" id="sectionBillingInformation">
                                            <div class="col-md-6 col-xs-6 b-r">  
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="setup_fee" class="control-label">Setup Fee</label>
                                                            <input type="text" name="billing_setup_fee" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="paid" class="control-label">Paid</label>
                                                            <input type="text" name="billing_paid" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="monthly_fee" class="control-label">Monthly Fee</label>
                                                            <input type="text" name="billing_monthly_fee" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="monthly_due" class="control-label">Monthly Due</label>
                                                            <input type="text" name="billing_monthly_due" class="form-control">
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <h5>Payment Method</h5>
                                                    <select class="select2" name="billing_payment_method" style="width: 100%">
                                                        <option value="0">Select Payment Method</option> 
                                                        <option value="American Express">American Express</option>
                                                        <option value="MasterCard">MasterCard</option>
                                                        <option value="Visa">Visa</option>
                                                        <option value="Cash">Cash</option>
                                                        <option value="Prepaid">Prepaid</option>
                                                    </select> 
                                                </div>
                                                <div class="form-group">
                                                    <label for="card_number" class="control-label">Card Number</label>
                                                    <input type="text" name="billing_card_number" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="card_expiration" class="control-label">Expiration</label>
                                                    <input type="text" name="billing_card_expiration" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="cvv_code" class="control-label">CVV Code</label>
                                                    <input type="text" name="billing_cvv_code" class="form-control">
                                                </div> 
                                                        
                                                        
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <div class="form-group">
                                                    <label for="account_holder" class="control-label">Account Holder</label>
                                                    <input readonly="" type="text" name="profile_name" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="address" class="control-label">Address</label>
                                                    <input readonly="" type="text" name="profile_address" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="city" class="control-label">City</label>
                                                    <input readonly="" type="text" name="profile_city" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="state_province" class="control-label">State/Province</label>
                                                    <input readonly="" type="text" name="profile_state_province" class="form-control"> 
                                                </div>
                                                <div class="form-group">
                                                    <label for="zip" class="control-label">Zip/Postal Code</label>
                                                    <input readonly="" type="text" name="profile_zip_postal_code" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xlg-4 col-md-4 col-sm-12">
                        <div class="card">
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li class="nav-item"> 
                                    <a id="tasks-tab-header" class="nav-link active show" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="true">Tasks</a> 
                                </li>
                                <li class="nav-item"> 
                                    <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> 
                                </li>
                                <li class="nav-item"> 
                                    <a id="alerts-tab-header" class="nav-link" data-toggle="tab" href="#alerts-tab" role="tab" aria-selected="false">Alerts</a> 
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                    <div class="card-body">
                                        <h5>Make a task</h5>
                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_task" class="form-control" rows="2"></textarea>   
                                        <div class="input-group">
                                            <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Due Date">
                                            <button id="btnSaveNewTask" style="border-radius: 0;width: 1%;flex: 1 1 auto;" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
                                        </div> 
                                        <div class="message-scroll ">
                                            <div id="task_list" class="profiletimeline m-t-40 ">  
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="notes-tab" role="tabpanel">
                                    <div class="card-body">
                                        <h5>Make a note</h5> 
                                            <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>   
                                            <div class="input-group">  
                                                <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"></i>  
                                                <button style="width: 1%;flex: 1 1 auto;border-radius: 0" id="btnSaveNewNote" class="btn btn-block btn-success waves-effect waves-light" type="button">Save Note</button>
                                                
                                            </div> 
                                        <div class="message-scroll ">
                                            <div id="note_list_sticky" class="profiletimeline m-t-40 ">  
                                            </div> 
                                            <div id="note_list" class="profiletimeline m-t-40 ">  
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="alerts-tab" role="tabpanel">
                                    <div class="card-body">
                                        <h5>Send alert</h5>
                                        <label class="m-b-0">Templates <small>(<a href="#" id="btnOpenAlertTemplatesModal" data-toggle="modal" data-target="#add-edit-alert-templates-modal">manage</a>)(<a id="btnOpenAlertTemplates" href="#">show</a>)</small>
                                        </label> 
                                        <div class="row hide" id="alert_templates_container">
                                            <div class="col-4" id="alert_templates_container1" style="line-height: 70%">
                                                
                                            </div> 
                                            <div class="col-4" id="alert_templates_container2" style="line-height: 70%">
                                                
                                            </div>
                                            <div class="col-4" id="alert_templates_container3" style="line-height: 70%">
                                                
                                            </div>
                                        </div> 

                                        <div class="m-t-20">
                                            <label class="control-label">Subject</label>
                                            <input id="new_alert_subject" type="text" name="" class="form-control new_alert_subject">
                                            <label class="control-label">Note</label>
                                            <textarea id="new_alert_notes" class="form-control new_alert_notes" cols="2"></textarea>
                                            <div>
                                                <button id="btnSaveNewAlert" class="pull-right btn btn-success waves-effect waves-light" type="button">
                                                    <i class="fas fa-paper-plane"></i>
                                                    Send alert
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="message-scroll ">
                                                <div id="alert_list" class="profiletimeline m-t-40 ">  
                                                </div> 
                                            </div>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <?php echo $page_title_s ?> Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body"> 
                <form enctype="multipart/form-data" id="formAddUser" method="post" action="<?php echo $page_controller ?>/saveDetail" class="floating-labels"> 
                    <input type="hidden" name="client_id"> 
                    <div class="row">
                        <div class="col-md-4 col-xs-6 b-r">
                            <h4 style="margin-bottom: 17px">Agent / Broker / Sales </h4>
                            <div class="form-group">
                                <h5>Agent:</h5> 
                                <select class="select2" name="agent_id" id="agent_id" style="width: 100%">
                                    <option value="0">Select Agent</option>
                                    <?php foreach ($agents as $key => $value): ?>
                                        <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div> 
                            <div class="form-group">
                                <h5>Broker:</h5> 
                                <select class="select2" name="broker_id" id="broker_id" style="width: 100%">
                                    <option value="0">Select Broker</option>
                                    <?php foreach ($brokers as $key => $value): ?>
                                        <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <h5>Sales:</h5> 
                                <select class="select2" name="sale_id" id="sale_id" style="width: 100%">
                                    <option value="0">Select Sales</option>
                                    <?php foreach ($sales as $key => $value): ?>
                                        <option value="<?php echo $value['sale_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <h5>Processor:</h5> 
                                <select class="select2" name="processor_id" id="processor_id" style="width: 100%">
                                    <option value="0">Select Processor</option>
                                    <?php foreach ($processors as $key => $value): ?>
                                        <option value="<?php echo $value['processor_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <h4 style="margin-bottom: 17px">User Account Information</h4>
                            <div class="form-group">
                                <label for="username" class="control-label">Username:</label> 
                                <input type="text" name="username" required="" class="form-control" id="username">
                                <span class="bar"></span> 
                                <small id="errorUsernameSingle" class="errorUsername hide form-control-feedback text-danger"> Username already exist!. </small>  
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password:</label> 
                                <input type="password" name="password" required="" class="form-control" id="password">
                                <span class="bar"></span>
                            </div> 
                        </div>  
                        <div class="col-md-4 col-xs-6 b-r">
                            <h4 style="margin-bottom: 17px">Personal Information</h4>
                            <div class="form-group">
                                <h5>Client Status:</h5>
                                <select class="select2" name="client_status" id="client_status" style="width: 100%">
                                    <option value="0">Select Status</option>
                                    <?php foreach ($client_statuses as $key => $value): ?>
                                        <option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="bar"></span>
                            </div> 
                            <div class="form-group">
                                <label for="name" class="control-label">Name:</label>
                                <input type="text" name="name" required="" class="form-control" id="name">
                                <span class="bar"></span>
                            </div> 
                            <div class="form-group">
                                <label for="address" class="control-label">Address:</label>
                                <input type="text" name="address" required="" class="form-control" id="address">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="city" class="control-label">City:</label>
                                <input type="text" name="city" required="" class="form-control" id="city">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <h5>State/Province:</h5> 
                                <select class="select2" name="state_province" id="state_province" style="width: 100%">
                                    <option value="0">Select State/Province</option>
                                    <?php foreach ($state_provinces as $key => $value): ?>
                                        <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="zip_postal_code" class="control-label">Zip/Postal Code:</label>
                                <input type="text" name="zip_postal_code" required="" class="form-control" id="zip_postal_code">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="email_address" class="control-label">Email Address</label>
                                <input type="email" name="email_address" class="form-control" id="email_address">
                            </div>
                            <div class="form-group">
                                <label for="ss" class="control-label">SS#</label>
                                <input type="text" name="ss" class="form-control" id="ss">
                            </div>
                        </div> 
                        <div class="col-md-4 col-xs-6 " style="margin-top: 65px">
                            <div class="form-group">
                                <label for="alt_phone" class="control-label">Alt Phone</label>
                                <input type="text" name="alt_phone" class="form-control" id="alt_phone">
                            </div>
                            <div class="form-group">
                                <label for="cell_phone" class="control-label">Cell Phone</label>
                                <input type="text" name="cell_phone" class="form-control" id="cell_phone">
                            </div>
                            <div class="form-group">
                                <h5 >Carrier</h5>
                                <select class="select2" name="carrier" id="carrier" style="width: 100%">
                                    <option value="0">Select Carrier</option>
                                    <option value="ATT">ATT</option>
                                    <option value="Boost">Boost</option>
                                    <option value="Cricket">Cricket</option>
                                    <option value="Metro PCS">Metro PCS</option>
                                    <option value="Simple">Simple</option>
                                    <option value="Sprint">Sprint</option>
                                    <option value="TMobile">TMobile</option>
                                    <option value="Verizon">Verizon</option>
                                    <option value="Virgin">Virgin</option>
                                    <option value="Do Not SMS">Do Not SMS</option>
                                    <option value="Landline">Landline</option>
                                </select> 

                            </div>
                            <div class="form-group">
                                <label for="fax" class="control-label">Fax</label>
                                <input type="text" name="fax" class="form-control" id="fax">
                            </div>
                            <div class="form-group">
                                <label for="date_of_birth" class="control-label">Date of Birth</label>
                                <input type="text" name="date_of_birth" class="form-control" id="date_of_birth">
                            </div>
                            <div class="form-group">
                                <label for="date_for_result" class="control-label">Date for result</label>
                                <input type="text" name="date_for_result" class="form-control" id="date_for_result">
                            </div> 
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-6 offset-3">
                            Joint Account?
                            <input  type="checkbox" class="js-switch" data-color="#ffbc34" id="toggleJointContent" />
                            <div id="joint_content" class="hide m-t-40">
                                <h4>Spouse Information</h4>
                                <div class="form-group">
                                    <label for="joint_name" class="control-label">Name:</label>
                                    <input type="text" name="joint_name" class="form-control" id="joint_name">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_email_address" class="control-label">Email Address:</label>
                                    <input type="email" name="joint_email_address" class="form-control" id="joint_email_address">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_ss" class="control-label">SS#:</label>
                                    <input type="text" name="joint_ss" class="form-control" id="joint_ss">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_date_fo_birth" class="control-label">Date of Birth:</label>
                                    <input type="text" name="joint_date_fo_birth" class="form-control" id="joint_date_fo_birth">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_username" class="control-label">Username:</label>
                                    <input type="text" name="joint_username" class="form-control" id="joint_username"> 
                                    <span class="bar"></span>
                                    <small id="errorUsernameJoint" class="errorUsername hide form-control-feedback text-danger"> Username already exist!. </small> 
                                </div>
                                <div class="form-group">
                                    <label for="joint_password" class="control-label">password:</label>
                                    <input type="password" name="joint_password" class="form-control" id="joint_password">
                                    <span class="bar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger waves-effect waves-light">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="add-edit-alert-templates-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Alert Templates
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">  
                <div class="row">
                    <div class="col-12">
                        <h4>New template</h4>
                        <div class="input-group">
                            <div class="input-group-append">
                                <input type="hidden" name="" id="new_alert_template_id">
                                <input id="new_template_shortname" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Short Name">
                            </div> 
                                <input id="new_template_subject" style="border-radius: 0" type="text" name="" class="form-control" placeholder="Subject"> 
                            
                        </div>
                        <div class="input-group"> 
                            <textarea id="new_template_notes" style="border-radius: 0" class="form-control" placeholder="Notes" rows="2"></textarea> 
                            <div class="input-group-prepend">
                                <button id="btnSaveNewAlertTemplate" style="width: 1%;flex: 1 1 auto;border-radius: 0" class="btn btn-info waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>
                            
                    </div>
                </div>
                <table id="tableAlertTemplates" class="table table-bordered table-striped">
                    <thead>
                        <th>Short Name</th>
                        <th>Subject</th>
                        <th>Notes</th> 
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
            </div> 
        </div>
    </div>
</div>
 <script>
$(document).ready(function() {
    $('.left_nav_'+'<?php echo $page_controller ?>').addClass('active');
    var newUser = false;
    var client_id = 0; 
    var alert_template_id = '';
    $('#formAddUser').on('submit',function(e){
        var errorUsernameSingle = $('#errorUsernameSingle');  
        var errorUsernameJoint = $('#errorUsernameJoint');  
        if (errorUsernameSingle.hasClass('hide') && errorUsernameJoint.hasClass('hide')) { 
            $('#formAddUser').submit();
        }  else {
            e.preventDefault();
        } 
    });
    var tableAlertTemplates = $('#tableAlertTemplates').DataTable();
    $('#pageTable').DataTable({ 
        'aaSorting': [],  
        dom: 'Bfrtip',
        "pageLength": 100
    }); 
    $('#pageTable').on('click','.goToProfile',function(){
        newUser = false;
        client_id = $(this).closest('tr').attr('id');
        var name = $(this).find('u').html();
        getProfile(client_id);
        getTasks(client_id);
        getNotes(client_id);
        getAlerts(client_id);
        getAlertTemplates();
        $('#profileSection').addClass('show');
        $('#listSection').addClass('hide');
        $('#profile_active').removeClass('hide');
        $('#profile_active').html(name);
        // $('#add-edit-modal').modal('toggle');
    });

    $('.rowTools a').on('click',function(){
        newUser = false;
        client_id = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').find('.goToProfile').find('u').html();
        var tr = $(this).closest('tr');
        if ($(this).find('.fa-trash').length == 0) {
            
            getProfile(client_id);
            getTasks(client_id);
            getNotes(client_id);
            getAlerts(client_id);
            getAlertTemplates();
            $('#profileSection').addClass('show');
            $('#listSection').addClass('hide');
            $('#profile_active').removeClass('hide');
            $('#profile_active').html(name);

            var tab_pane = $(this).attr('value'); 
            $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('active');
            $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('show');
            $('#'+tab_pane).addClass('active');
            $('#'+tab_pane).addClass('show');

            $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('active');
            $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('show');

            $('#'+tab_pane+'-header').addClass('active');
            $('#'+tab_pane+'-header').addClass('show');
        } else {
            swal({   
                title: "Are you sure?",   
                text: 'Client '+name+" will be deleted!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false ,
                preConfirm: function() {
                    updateFields('clients',client_id,'client_status','Archived');
                }
            }).then(function(){
                tr.remove();
                swal("Success!", "Client "+name+' successfully deleted!', "success");
            });
        }
    });
    $('#btnNewUser').on('click',function(){
        newUser = true;
        $('#formAddUser')[0].reset();
        $('input[name=username]').prop('readonly',false);
        $('.dropify-render').find('img').attr('src','../assets/images/users/<?php echo $page_folder ?>/LynxLogo.png');
        var inputs =  $('#formAddUser input'); 
        $.each(inputs,function(key,input){ 
            $(input).closest('.form-group').removeClass('focused');
        }); 
    });
    $('#username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'client');
        }
    });
    $('#joint_username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'joint');
        }
    });
    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,client_id,task,task_date);
            } else {
                task_date.focus();
            }
        } else {
            task.focus();
        }
    });
    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });
    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,client_id,note,note_sticky);
        } else {
            note.focus();
        }
    });
    $('#btnSaveNewAlert').on('click',function(){
        var button = $(this);
        var alert_subject = $('#new_alert_subject');
        var alert_notes = $('#new_alert_notes'); 
        if (alert_subject.val() != '') {
            if (alert_notes.val() != '') {
                saveAlert(button,client_id,alert_subject,alert_notes);
            } else {
                alert_notes.focus();
            }
        } else {
            alert_subject.focus();
        }
    });

    $('#toggleJointContent').on('change',function(){ 
        var joint_content = $('#joint_content');
        if (joint_content.hasClass('hide')) {
            joint_content.removeClass('hide');
        } else {
            joint_content.addClass('hide');
        }
    });

     


    $('#btnSaveNewAlertTemplate').on('click',function(){
        var template_shortname = $('#new_template_shortname');
        var template_subject = $('#new_template_subject');
        var template_notes = $('#new_template_notes');
        if (template_shortname.val() != '') {
            if (template_subject.val() != '') {
                if (template_notes.val() != '') {
                    saveNewTemplate(template_shortname,template_subject,template_notes);
                } else {
                    template_notes.val();
                }
            } else {
                template_subject.focus();
            }
        } else {
            template_shortname.focus();
        }

    });

    $('#btnOpenAlertTemplates').on('click',function(){
        var btnOpenAlertTemplates = $(this);
        var alert_templates_container = $('#alert_templates_container');
        if (alert_templates_container.hasClass('hide')) {
            alert_templates_container.removeClass('hide');
            btnOpenAlertTemplates.html('hide');
        } else {
            alert_templates_container.addClass('hide');
            btnOpenAlertTemplates.html('show');
        }
    });

    function saveNewTemplate(template_shortname,template_subject,template_notes) {
        template_shortname_ = template_shortname;
        template_shortname = template_shortname.val(); 
        template_subject_ = template_subject;
        template_subject = template_subject.val(); 
        template_notes_ = template_notes;
        template_notes = template_notes.val(); 
        var alert_template_id = $('#new_alert_template_id').val(); 
        // console.log(alert_template_id);
        $.post('<?php echo $page_controller ?>'+'/saveTemplate',
            {alert_template_id,template_shortname,template_subject,template_notes},function(){
                template_shortname_.val('');
                template_subject_.val('');
                template_notes_.val('');
                $('#new_alert_template_id').val(''); 
                getAlertTemplates();
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

 
    function getProfile(client_id) {
        $.post('<?php echo $page_controller ?>/getProfile',
            {client_id},function(data){
                data = JSON.parse(data); 
                
                $.each(data['profile'],function(key,value){ 
                    $.each(value,function(k,v){
                        $('input[type=text][name=profile_'+k+']').val(v);
                        $('input[type=email][name=profile_'+k+']').val(v);
                        $('input[type=password][name=profile_'+k+']').val(v);
                        $('input[type=hidden][name=profile_'+k+']').val(v);
                        $('select[name=profile_'+k+']').val(v);
                        $('textarea[name=profile_'+k+']').val(v);
                        $('.'+k).html(v); 
                    })
                }); 
                var dfr = moment(data['profile'][0]['date_for_result']).diff(moment(),'days');
                $('#profile_dfr').html(dfr);

                $.each(data['billing'],function(key,value){ 
                    $.each(value,function(k,v){
                        $('input[type=text][name=billing_'+k+']').val(v);
                        $('input[type=email][name=billing_'+k+']').val(v);
                        $('input[type=password][name=billing_'+k+']').val(v);
                        $('input[type=hidden][name=billing_'+k+']').val(v);
                        $('select[name=billing_'+k+']').val(v);
                        $('.'+k).html(v); 
                    })
                });

                $.each(data['enrollment'],function(key,value){ 
                    $.each(value,function(k,v){
                        $('input[type=text][name=enrollment_'+k+']').val(v);
                        $('input[type=email][name=enrollment_'+k+']').val(v);
                        $('input[type=password][name=enrollment_'+k+']').val(v);
                        $('input[type=hidden][name=enrollment_'+k+']').val(v);
                        $('select[name=enrollment_'+k+']').val(v);
                        $('.'+k).html(v); 
                    })
                });

                var inputs =  $('#formAddUser input'); 
                $.each(inputs,function(key,input){ 
                    $(input).closest('.form-group').addClass('focused');
                });
                $('input[name=profile_username]').prop('readonly',true); 
                $('select').trigger('change');
                functionsForProfileTab();
                functionsForBillingTab();
                functionsForEnrollmentTab();
        });
    }


    function checkIfUsernameExist(username,type) {
        var username_val = username.val();
        $.post('<?php echo $page_controller ?>/checkIfUsernameExist',
            {username_val,type},function(data){
                // console.log(data);
                if (data == '0') {
                    username.removeClass('form-control-danger');
                    username.closest('.form-group').removeClass('has-danger');
                    username.closest('.form-group').removeClass('has-error');
                    username.closest('.form-group').find('.errorUsername').addClass('hide');
                } else { 
                    username.addClass('form-control-danger');
                    username.closest('.form-group').addClass('has-danger');
                    username.closest('.form-group').addClass('has-error');
                    username.closest('.form-group').find('.errorUsername').removeClass('hide');
                }
            });
    }
    function getTasks(client_id) {
        $.post('<?php echo $page_controller ?>/getTasks',
            {client_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-warning waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }
                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a><br><span class="sl-date">'+moment(value.task_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.task+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });
                $('.btnUpdateTaskDone').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,0);
                });
                $('.btnUpdateTaskActive').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,1);
                });
                $('.btnUpdateTaskDelete').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,2);
                });
            });
    }
    function saveTask(button,client_id,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveTask',
            {client_id,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(client_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateTask',
            {task_id,task_active},function(data){
                getTasks(client_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function getNotes(client_id) {
        $.post('<?php echo $page_controller ?>/getNotes',
            {client_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }
                    var newNote = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a><br><span class="sl-date">'+moment(value.note_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.note+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });
                $('.btnUpdateNoteSticky').on('click',function(){
                    var button = $(this);
                    var note_id = $(this).closest('.sl-item').attr('value');
                    if (button.hasClass('text-warning')) {
                        updateNote(button,note_id,0);
                    } else {
                        updateNote(button,note_id,1);
                    }
                });
            });
    }
    function saveNote(button,client_id,note,note_sticky){
        var note_ = note;
        var note = note.val(); 
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveNote',
            {client_id,note,note_sticky},function(data){
                // console.log(data); 
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                getNotes(client_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateNote',
            {note_id,note_sticky},function(data){
                getNotes(client_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getAlerts(client_id) {
        $.post('<?php echo $page_controller ?>/getAlerts',
            {client_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#alert_list').empty();
                $.each(data,function(key,value){  
                    var newAlert = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.alert_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div>\
                                                <a class="link">'+value.sender_name+'</a><br><span class="sl-date">'+moment(value.alert_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote style="line-height: 100%;" class="form-control">\
                                                        Subject: '+value.alert_subject+'<br>\
                                                        <p style="line-height: 100%;font-size:70%;text-align: justify">Notes: '+value.alert_notes+'</p>\
                                                    </blockquote>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#alert_list').append(newAlert);
                }); 
            });
    }
    function saveAlert(button,client_id,alert_subject,alert_notes){
        var alert_subject_ = alert_subject;
        var alert_subject = alert_subject.val();
        var alert_notes_ = alert_notes;
        var alert_notes = alert_notes.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveAlert',
            {client_id,alert_subject,alert_notes},function(data){
                // console.log(data);
                alert_subject_.val('');
                alert_notes_.val('');
                getAlerts(client_id);
                button.prop('disabled',false);

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateFields(table,client_id,field,value,title,selected) {
        if (value != '') {
            $.post('<?php echo $page_controller ?>/updateFields',
                {table,client_id,field,value},function(data){ 
                    if (title != null) {
                        if (selected != null) {
                            value = selected;
                        }
                        $.toast({
                            heading: 'Update Success!',
                            text: title+' successfully updated to '+value,
                            position: 'top-right',
                            loaderBg:'#17a2b8',
                            icon: 'success',
                            hideAfter: 3500, 
                            stack: 6
                          });
                        if (field == 'date_for_result') {
                            var dfr = moment(value).diff(moment(),'days');
                            $('#profile_dfr').html(dfr);
                            $('input[name=profile_date_for_result]').val(moment(value).format('MM/DD/YYYY'));
                        }
                        if (field == 'date_of_birth') {  
                            $('input[name=profile_date_of_birth]').val(moment(value).format('MM/DD/YYYY'));
                        }
                    }
                        
                     
                        
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });
                   
        }
    }

    function functionsForProfileTab() {
        $('#sectionClientAssignments select').on('change',function(){
            var select = $(this); 
            var title = select.closest('.form-group').find('strong').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var selected = select.find('option:selected').text();
            var value = select.val();  
            updateFields('client_assignments',client_id,field,value,title,selected);
        });
        $('#sectionClientAssignments input[type=text]').on('focusout',function(){
            var input = $(this);
            var title = input.closest('.form-group').find('strong').html();
            var field = input.attr('name');
            field = field.replace('profile_','');
            var value = input.val();   
            updateFields('clients',client_id,field,value,title);
        });
        $('#sectionProfileInformation input[type=text]').on('focusout',function(){
            var input = $(this);
            var title = input.closest('.form-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('profile_','');
            var value = input.val();   
            updateFields('clients',client_id,field,value,title);
        });
        
        $('#sectionProfileInformation select').on('change',function(){
            var select = $(this);
            var title = select.closest('.form-group').find('h5').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var value = select.val();   
            updateFields('clients',client_id,field,value,title);
        });
        $('#profile_password').on('focusout',function(){
            var input = $(this); 
            var value = input.val();   
            updateFields('clients',client_id,'password',value,'Password');
        })

        
        $('#profile_status_line').on('focusout',function(){
            var input = $(this); 
            var value = input.val();   
            updateFields('clients',client_id,'status_line',value,'Status Line');
        })
    }

    function functionsForBillingTab() {
        $('#sectionBillingInformation input[type=text]').on('focusout',function(){
            var input = $(this);
            var title = input.closest('.form-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('billing_','');
            var value = input.val();   
            updateFields('client_billings',client_id,field,value,title);
        });
        $('#sectionBillingInformation select').on('change',function(){
            var select = $(this);
            var title = select.closest('.form-group').find('h5').html();
            var field = select.attr('name');
            field = field.replace('billing_','');
            var value = select.val();   
            updateFields('client_billings',client_id,field,value,title);
        });

    }

    function functionsForEnrollmentTab() {
        $('#sectionEnrollmentInformation input[type=text]').on('focusout',function(){
            var input = $(this);
            var title = input.closest('.form-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('enrollment_','');
            var value = input.val();   
            updateFields('client_enrollments',client_id,field,value,title);
        });
    }

    function getAlertTemplates() {
        $.post('<?php echo $page_controller ?>/getAlertTemplates',
            function(data){
                data = JSON.parse(data);
                tableAlertTemplates.clear().draw();
                var alert_templates_container1 = $('#alert_templates_container1');
                var alert_templates_container2 = $('#alert_templates_container2');
                var alert_templates_container3 = $('#alert_templates_container3');
                alert_templates_container1.empty();
                alert_templates_container2.empty();
                alert_templates_container3.empty();
                var template_count = data.length;
                var template_count_divide_3 = Math.round(template_count / 3);
                $.each(data,function(key,value){ 
                    var editButton = '<a href="#">'+value.template_shortname+'</a>';
                    var newRow = tableAlertTemplates.row.add([editButton,value.template_subject,value.template_notes]).node().id = value.alert_template_id;   
                    tableAlertTemplates.draw(false); 
                });

                
                for (var i = 0; i < template_count_divide_3; i++) {
                    var template_subject = '<input type="hidden" class="hidden_template_subject" value="'+data[i]['template_subject']+'"/>';
                    var template_notes = '<input type="hidden" class="hidden_template_notes" value="'+data[i]['template_notes']+'"/>';
                    alert_templates_container1.append('<small style="font-size: 80%"><a href="#" id="'+data[i]['alert_template_id']+'">'+template_subject+template_notes+data[i]['template_shortname']+'</a></small><br>');
                }
                for (var i = template_count_divide_3; i <= template_count - template_count_divide_3; i++) {
                    var template_subject = '<input type="hidden" class="hidden_template_subject" value="'+data[i]['template_subject']+'"/>';
                    var template_notes = '<input type="hidden" class="hidden_template_notes" value="'+data[i]['template_notes']+'"/>';
                    alert_templates_container2.append('<small style="font-size: 80%"><a href="#" id="'+data[i]['alert_template_id']+'">'+template_subject+template_notes+data[i]['template_shortname']+'</a></small><br>');
                }

                for (var i = (template_count_divide_3 + template_count_divide_3 + 2); i < template_count; i++) {
                    var template_subject = '<input type="hidden" class="hidden_template_subject" value="'+data[i]['template_subject']+'"/>';
                    var template_notes = '<input type="hidden" class="hidden_template_notes" value="'+data[i]['template_notes']+'"/>';
                    alert_templates_container3.append('<small style="font-size: 80%"><a href="#" id="'+data[i]['alert_template_id']+'">'+template_subject+template_notes+data[i]['template_shortname']+'</a></small><br>');
                }

                $('#alert_templates_container a').on('click',function(e){ 
                    e.preventDefault();
                    var template_subject = $(this).find('.hidden_template_subject').val();
                    var template_notes = $(this).find('.hidden_template_notes').val();

                    $('.new_alert_subject').val(template_subject);
                    $('.new_alert_notes').val(template_notes); 
                });



                $('#tableAlertTemplates a').on('click',function(){
                    var tr = $(this).closest('tr');
                    var alert_template_id = tr.attr('id');
                    var tdata = tr.find('td');

                    $('#new_alert_template_id').val(alert_template_id); 
                    
                    var template_shortname = $(tdata[0]).find('a').html();
                    var template_subject = $(tdata[1]).html();
                    var template_notes = $(tdata[2]).html();
 
                    $('#new_template_shortname').val(template_shortname);
                    $('#new_template_subject').val(template_subject);
                    $('#new_template_notes').val(template_notes);
                });

            });
    }
 
});
</script>
