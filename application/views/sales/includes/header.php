<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <title>Credit Lynx | Sales Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url() ?>/assets/css/colors/green-dark.css" id="theme" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/plugins/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/plugins/dropify/dist/css/dropify.min.css">  
    <link href="<?php echo base_url() ?>/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo base_url() ?>/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet"> 
    <link href="<?php echo base_url() ?>/assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" /> 
    <link href="<?php echo base_url() ?>/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]--> 
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url() ?>/assets/plugins/popper/popper.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url() ?>/assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url() ?>/assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url() ?>/assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url() ?>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url() ?>/assets/js/custom.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/moment.js"></script>
    <!-- jQuery file upload -->
    <script src="<?php echo base_url() ?>/assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <!-- This is data table -->
    <script src="<?php echo base_url() ?>/assets/plugins/datatables/datatables.min.js"></script>   
    <script src="<?php echo base_url() ?>/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>  
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
    <script src="<?php echo base_url() ?>/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/switchery/dist/switchery.min.js"></script>
    
    <script src="<?php echo base_url() ?>/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- <script src="js/toastr.js"></script> -->
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <!-- <script src="<?php echo base_url() ?>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> --> 
    <style>
        small {
            font-size: 60%;
        } 
        .page-item.active .page-link {
            background-color: #26c6da !important;
            border-color: #26c6da !important;
        }
        .form-group {
            margin-bottom: 20px !important;
        }
        /*.form-control {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }*/
        .control-label {
            margin-bottom: 0px !important;
        }
        .sidebar-nav ul li a:hover {
            color: #00adad !important;
        }
        th {
            font-size: 12px !important;
        }
        .select2-container--default .select2-selection--single {
            border-radius: 0px !important; 
            border: 1px solid #ced4da !important; 
            height: 38px !important; 
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 38px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #67757c !important; 
        }
        .pull-right {
            float: right;
        } 
        .pull-left {
            float: left;
        }
        .form-control {
            border: 1px solid #ced4da !important; 
            padding: .375rem .75rem !important;
            padding-left: 8px !important; 
        }
        .floating-labels label {
            top: 8px !important;
            left: 8px !important;
        }
        .floating-labels .focused label {
            top: -19px !important; 
            left: 0px !important;
        }
        .fa-note {
            cursor: pointer;
            padding: 3px;
            line-height: 140%;
            text-align: center;
            border: 1px solid #ced4da;
        }
        .material_date_time {
            border: 1px solid #ced4da;
        }
        .task_update_button {
            width: 45px !important;
        }
        .dropify-wrapper {
            border: none !important; 
        }
        .dropify-render > img{
            border-radius: 100% !important;
        } 
        li > a:hover,.breadcrumb-item > a:hover,.goToProfile:hover,.paginate_button:hover{
            color: #17a2b8 !important;
        }  

        .paginate_button > a:hover {
            color: white !important; 
        }

        .user-profile .profile-text a:hover {
            color: #17a2b8 !important;
        }

        .jq-icon-success {
            background-color: #17a2b8;
        }

        .dropdown-item:active {
            background-color: #17a2b8 !important;
            color: white !important;
        }
        .user-profile .profile-text a:active {
            color: white !important;
        }


        tr > td {
            line-height: 28px !important;
            font-size: 12px !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
        }

        .sidebar-nav ul li a.active {
          color: #17a2b8;
        }
 
    </style>
</head>