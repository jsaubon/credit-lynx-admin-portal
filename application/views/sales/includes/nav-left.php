<style>
    .hide {
        display: none;
    }
</style>
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url(<?php echo base_url() ?>/assets/images/background/user-info.jpg) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php echo base_url() ?>/assets/images/users/sales/<?php echo $userdata['photo'] ?>" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo $userdata['name'] ?> <span class="caret"></span></a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="<?php echo base_url('sales/profile') ?>" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <!-- <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                            <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a> -->
                            <!-- <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a> -->
                            <div class="dropdown-divider"></div> <a href="<?php echo base_url() ?>sales/login/logout" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav" style="padding-top: 0px">
                    <ul id="sidebarnav"> 
                        <li class="text-center" style="margin-bottom: 0">  
                            <div style="color:  rgba(255, 255, 255, 0.8);">
                                Worked Today: <b  id="worked_today" ></b>
                            </div>
                            <div class="btn-group text-center">
                                <button id="btnStartWork" style="border-radius: 0" class="hide btn btn-outline-success waves-effect waves-light" type="button">
                                    <span class="btn-label"><i class="fas fa-play"></i></span>
                                        Start
                                </button>
                                
                                <button id="btnResumeWork" style="border-radius: 0;" class="hide btn btn-outline-warning waves-effect waves-light" type="button">
                                    <span class="btn-label" style="margin-right: 0"><i class="fas fa-play"></i></span>
                                        Resume
                                </button>
                                <button id="btnStopWork" style="border-radius: 0" class="hide btn btn-outline-danger waves-effect waves-light" type="button">
                                    <span class="btn-label"><i class="fas fa-stop"></i></span>
                                        Stop
                                </button>
                            </div> 
                        </li>     
                        
                        <li class="nav-small-cap" style="text-transform: uppercase;"> 
                            <?php echo $userdata['login_type'] ?> 
                        </li>
                        <li class="left_nav_dashboard">
                            <a href="<?php echo base_url() ?>sales/dashboard" aria-expanded="false">
                                <i class="mdi mdi-gauge"></i>
                                <span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <li class="left_nav_clients">
                            <a href="<?php echo base_url() ?>sales/clients" aria-expanded="false">
                                <i class="fas fa-users"></i>
                                <span class="hide-menu">Clients</span>
                            </a>
                        </li>
                        <li class="left_nav_leads">
                            <a href="<?php echo base_url() ?>sales/leads" aria-expanded="false">
                                <i class="mdi mdi-account"></i>
                                <span class="hide-menu">Leads</span>
                            </a>
                        </li>
                         
                        <li class="left_nav_appointments">
                            <a href="<?php echo base_url() ?>sales/appointments" aria-expanded="false">
                                <i class="mdi mdi-calendar"></i>
                                <span class="hide-menu">Appointments</span>
                            </a>
                        </li> 
                        <li class="left_nav_help">
                            <a href="<?php echo base_url() ?>sales/help" aria-expanded="false">
                                <i class="mdi mdi-help-circle"></i>
                                <span class="hide-menu">Help</span>
                            </a>
                        </li>
                          
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <!-- item-->
                <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <!-- item-->
                <a href="<?php echo base_url() ?>sales/login/logout" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

<script>
    $(document).ready(function(){
        $('#btnStartWork,#btnResumeWork').on('click',function(){
            startWork();
            
        });

        function startWork() {
            $.post('<?php echo base_url('admin/employees/worked_hours/startWork') ?>',
                function(data){ 
                    var work_id = data;
                    $('#worked_today').addClass('work_id_'+work_id);
                    $('#btnStartWork').addClass('hide'); 
                    getWorkHours();
            })
        }

        $('#btnStopWork').on('click',function(){ 
            var work_id = $('#worked_today').attr('class');
            work_id = work_id.replace('work_id_',''); 
            // console.log(work_id);
            $.post('<?php echo base_url('admin/employees/worked_hours/stopWork') ?>',
                {work_id}, function(data){   
                    $('#btnStartWork').addClass('hide');
                    $('#btnStopWork').addClass('hide');
                    $('#btnResumeWork').removeClass('hide');
                    getWorkHours(); 
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });
        }); 


        getWorkHours();
        var get_work_hours_interval = setInterval(function(){
            getWorkHours();
        },60000);
        function getWorkHours() {
            $.post('<?php echo base_url('admin/employees/worked_hours/getWorkHours') ?>',
                function(data){
                    data = JSON.parse(data); 
                        $.each(data,function(key,value){ 
                            if (value.work_id != null) {
                                $('#worked_today').removeClass();
                                $('#worked_today').addClass('work_id_'+value.work_id);
                                var h = value.hours;
                                var m = value.minutes;
                                $('#worked_today').html(h+'h '+m+'m'); 
                                $('#btnResumeWork').addClass('hide');

                                if (value.stop == 1) { 
                                    $('#btnStopWork').removeClass('hide');
                                    $('#btnResumeWork').addClass('hide'); 
                                } else {
                                    $('#btnStopWork').addClass('hide');
                                    $('#btnResumeWork').removeClass('hide'); 
                                }


                            } else {
                                $('#worked_today').html('0h 0m');
                                $('#btnStartWork').removeClass('hide');
                                $('#btnResumeWork').addClass('hide');
                                $('#btnStopWork').addClass('hide');
                            }
                                
                            
                        }); 
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });
                    
        }



    });

</script>