
<div class="row">
    <div class="col-sm-6">
        <label>First Name</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>Email</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>Address</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>State</label> 
        <select name="State" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
            <option value="Alabama">Alabama</option>
            <option value="Alaska">Alaska</option>
            <option value="Arizona">Arizona</option>
            <option value="Arkansas">Arkansas</option>
            <option value="California">California</option>
            <option value="Colorado">Colorado</option>
            <option value="Connecticut">Connecticut</option>
            <option value="Delaware">Delaware</option>
            <option value="Florida">Florida</option>
            <option value="Georgia">Georgia</option>
            <option value="Hawaii">Hawaii</option>
            <option value="Idaho">Idaho</option>
            <option value="Illinois">Illinois</option>
            <option value="Indiana">Indiana</option>
            <option value="Iowa">Iowa</option>
            <option value="Kansas">Kansas</option>
            <option value="Kentucky">Kentucky</option>
            <option value="Louisiana">Louisiana</option>
            <option value="Maine">Maine</option>
            <option value="Maryland">Maryland</option>
            <option value="Massachusetts">Massachusetts</option>
            <option value="Michigan">Michigan</option>
            <option value="Minnesota">Minnesota</option>
            <option value="Mississippi">Mississippi</option>
            <option value="Missouri">Missouri</option>
            <option value="Montana">Montana</option>
            <option value="Nebraska">Nebraska</option>
            <option value="Nevada">Nevada</option>
            <option value="New Hampshire">New Hampshire</option>
            <option value="New Jersey">New Jersey</option>
            <option value="New Mexico">New Mexico</option>
            <option value="New York">New York</option>
            <option value="North Carolina">North Carolina</option>
            <option value="North Dakota">North Dakota</option>
            <option value="Ohio">Ohio</option>
            <option value="Oklahoma">Oklahoma</option>
            <option value="Oregon">Oregon</option>
            <option value="Pennsylvania">Pennsylvania</option>
            <option value="Rhode Island">Rhode Island</option>
            <option value="South Carolina">South Carolina</option>
            <option value="South Dakota">South Dakota</option>
            <option value="Tennessee">Tennessee</option>
            <option value="Texas">Texas</option>
            <option value="Utah">Utah</option>
            <option value="Vermont">Vermont</option>
            <option value="Virginia">Virginia</option>
            <option value="Washington">Washington</option>
            <option value="West Virginia">West Virginia</option>
            <option value="Wisconsin">Wisconsin</option>
            <option value="Wyoming">Wyoming</option>
        </select>
    </div>
    <div class="col-sm-6">
        <label>Last Name</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>Phone</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>City</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
        <label>Zip</label>
        <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" >
    </div>
</div>
<label>Plans</label>

[checkbox* Plan "Premiere Plan"]
   <label>Setup Fee $149</label>
   <label>Monthly $99</label>

[checkbox* Plans "Premiere Plus Plan"]
<label>Premiere Plus</label>
   <label>Setup Fee $219</label>
   <label>Monthly $179</label>

<label>Card Number</label>
[number* CardNumber min:16 max:16]

<label>Expiration</label>
[number* Expiration min:4 max:4]

<label>CVV</label>
[number* CVV min:3 max:4]

<label>Billing Full Name</label>
[text* BillingFullName]

<label>Billing Address</label>
[text* BillingAddress]

<label>Billing City</label>
[text* BillingCity]

<label>Billing State</label>
[text* BillingState][select* State "Alabama" "Alaska" "Arizona" "Arkansas" "California" "Colorado" "Connecticut" "Delaware" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Kansas" "Kentucky" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "North Carolina" "North Dakota" "Ohio" "Oklahoma" "Oregon" "Pennsylvania" "Rhode Island" "South Carolina" "South Dakota" "Tennessee" "Texas" "Utah" "Vermont" "Virginia" "Washington" "West Virginia" "Wisconsin" "Wyoming"]

<label>Billing Zip</label>
[text* BillingZipCode min:5 max:5]

[acceptance Accept] I Accept All Terms and Policies [/acceptance]

[submit "Submit"]