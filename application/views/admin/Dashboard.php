<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<link href="<?php echo base_url('assets/plugins/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chartist-js/dist/chartist-init.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
<!-- <link href="<?php echo base_url() ?>/assets/plugins/css-chart/css-chart.css" rel="stylesheet"> -->


<link href="<?php echo base_url('assets/plugins/gridstack/gridstack.css') ?>" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/lodash.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/gridstack.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/gridstack.jQueryUI.js') ?>"></script>
<style type="text/css">
    .fa-mobile-phone {
        font-size: 2em !important;
    }
    .campaign .ct-series-a .ct-area {
        fill: rgb(116, 223, 232);
    }

    /*.campaign .ct-series-a .ct-bar {
        stroke: #26c6da;
    }*/
    .comment-widgets .comment-row.active, .comment-widgets, .comment-row:hover {
        border-color: #26c6da!important;
    }
    .comment-widgets .comment-row {
        border-left: 5px solid #ffffff;
    }
    .slimbar::-webkit-scrollbar {
      width: 5px;
    }

    /* Track */
    .slimbar::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px grey;
      border-radius: 5px;
    }

    /* Handle */
    .slimbar::-webkit-scrollbar-thumb {
      background: #26c6da!important;
      border-radius: 5px;
    }

    /* Handle on hover */
    .slimbar::-webkit-scrollbar-thumb:hover {
      background: #26c6da!important;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border: 1px solid #cfd4d9!important;
        height: 38px!important;
    }
    .select2-dropdown {
        border: 1px solid #cfd4d9!important;
    }
    .select2-container--default .select2-selection--multiple {
        border: 1px solid #cfd4d9!important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow{
        top: 6px!important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da !important;
        height: 38px !important;
        border-radius: 5px!important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 38px !important;
    }
    .lh{
        line-height: 70%;
    }

    .grid-stack-item-content {
        text-align: left;
    }

    .grid-stack-item-content .card{
        margin-bottom: 0px !important;
    }

    .top_boxes .card{
        height: 100%;
    }

    .top_boxes .card-body{
        padding: 2.25rem 2rem;
    }

    .mid_box .card{
        height: 100%;
    }



    .bot_box .card{
        height: 100%;
    }




</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Dashboard</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="grid-stack" data-gs-width="12" data-gs-animate="yes" >
                            <!-- <div class="grid-stack-item top_boxes" data-gs-x="0" data-gs-y="0" data-gs-width="4" data-gs-height="2" >
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-row">
                                                <div class="round round-lg align-self-center round-info" style="background: linear-gradient(to right, #042768 0%, #194798 100%);">
                                                    <i class="fas fa-user"></i>
                                                </div>
                                                <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0 font-light"><?php echo $total_active_clients ?></h3>
                                                    <h5 class="text-muted m-b-0" style="font-size: 15px;">Active Clients</h5></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="grid-stack-item top_boxes" data-gs-x="0" data-gs-y="0" data-gs-width="6" data-gs-height="2">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-row">
                                                <div class="round round-lg align-self-center round-success" style="background: linear-gradient(to right, #1d4da1 0%, #4581cd 100%);"><i class="fas fa-bullseye" ></i></div>
                                                <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0 font-lgiht"><?php echo ($close_rate * 100) ?>%</h3>
                                                    <h5 class="text-muted m-b-0">Close Rate</h5></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="grid-stack-item top_boxes" data-gs-x="6" data-gs-y="0" data-gs-width="4" data-gs-height="2">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-row">
                                                <div class="round round-lg align-self-center round-danger" style="background: linear-gradient(to right, #589ad7 0%, #8ae0f4 100%);"><i class="fas fa-dollar"></i></div>
                                                <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0 font-lgiht">$<?php echo number_format($proj_sales,2) ?></h3>
                                                    <h5 class="text-muted m-b-0">Projected Sales</h5></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="grid-stack-item top_boxes" data-gs-x="6" data-gs-y="0" data-gs-width="6" data-gs-height="2">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-row">
                                                <div class="round round-lg align-self-center round-primary"style="background: linear-gradient(to right, #8ee5f6 0%, #7fd19b 100%);"><i class="fas fa-chart-line"></i></div>
                                                <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0 font-lgiht">$<?php echo $avg_income_per_client ?></h3>
                                                    <h5 class="text-muted m-b-0">Average Income</h5></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item mid_box" data-gs-x="0" data-gs-y="2" data-gs-width="12" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body p-b-0" >
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="d-flex flex-wrap">
                                                        <div>
                                                            <h3 class="card-title">
                                                                <a class="btnChartPrevMonth" href="#"><i class="fas fa-angle-double-left"></i></a>
                                                                <span class="labelDate hide"><?php echo date('Y-m-d') ?></span>
                                                                <span class="labelMonth"><?php echo date('F') ?></span>
                                                                <span class="labelYear"><?php echo date('Y') ?></span>
                                                                <a class="btnChartNextMonth" href="#"><i class="fas fa-angle-double-right"></i></a>
                                                            </h3>
                                                            <h6 class="card-subtitle">Leads Overview</h6>
                                                        </div>
                                                        <div class="ml-auto">
                                                            <ul class="list-inline">
                                                                <li>
                                                                    <h6 class="text-muted text-success"><i class="fa fa-circle font-10 m-r-10 "></i>Leads</h6>
                                                                </li>
                                                                <li>
                                                                    <h6 class="text-muted  text-info"><i class="fa fa-circle font-10 m-r-10"></i>Clients</h6>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="campaign" style="height: 360px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid-stack-item bot_box" data-gs-x="0" data-gs-y="10" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #000066 0%, #2e6ad8 100%);flex: 0 1 auto;">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Tasks</h4>
                                            <button class="float-right btn btn-sm btn-rounded btn-success" data-toggle="modal" data-target="#myModal" style="margin-top:-27px;">Add Task</button>
                                        </div>
                                        <div class="card-body slimbar" style="padding-top: 0px; overflow-y: scroll; height: 400px;">
                                            <div class="to-do-widget" >
                                                <ul class="list-task todo-list list-group m-b-0" data-role="tasklist" id="all_Task">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item bot_box" data-gs-x="4" data-gs-y="8" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #3572cf 0%, #75e8fb 100%);flex: 0 1 auto">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Appointments</h4>
                                            <!-- <h6 class="card-subtitle text-white m-b-0 op-5">Checkout my contacts here</h6> -->
                                        </div>
                                        <div class="card-body slimbar" id="all_Appoint" style="overflow-y: scroll; height: 400px;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item bot_box" data-gs-x="8" data-gs-y="8" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #8ee5f6 0%, #16d698 100%);flex: 0 1 auto">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Texts</h4>
                                        </div>
                                        <div class=" slimbar" style="overflow-y: scroll; height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget" id="all_Etext">
                                                    <!-- Message -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item bot_box" data-gs-x="0" data-gs-y="14" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #000066 0%, #2e6ad8 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Payments Due Today</h4>
                                        </div>
                                        <div class="slimbar" style=" overflow-y: scroll; height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget " id="all_payments_today">
                                                    <?php foreach ($all_payment_today as $key => $value): ?>
                                                        <?php if ($value['client_type'] == 'client'): ?>
                                                            <a class="link to_client_page" href="#" type="client" id="<?php echo $value['client_id'] ?>">
                                                                <div class="user-img">
                                                                    <span class="round bg-primary">
                                                                        <i class="fa fa-lg fa-dollar"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="mail-desc">Monthly Fee: <?php echo $value['monthly_fee'] ?></span>
                                                                    <span class="time">Monthly Due: <?php echo date('m/d/Y', strtotime(date('Y-m-' . $value['day_due']))) ?></span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                        <?php if ($value['client_type'] == 'joint'): ?>
                                                            <a class="link to_client_page" href="#" type="joint" id="<?php echo $value['client_joint_id'] ?>">
                                                                <div class="user-img">
                                                                    <span class="round bg-primary">
                                                                        <i class="fa fa-lg fa-dollar"></i>
                                                                    </span>
                                                                </div>
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="mail-desc">Monthly Fee: <?php echo $value['monthly_fee'] ?></span>
                                                                    <span class="time">Monthly Due: <?php echo date('m/d/Y', strtotime(date('Y-m-' . date('d', $value['day_due'])))) ?></span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                    <?php endforeach?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item bot_box" data-gs-x="4" data-gs-y="14" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #3572cf 0%, #75e8fb 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Emails</h4>
                                            <!-- <h6 class="card-subtitle text-white m-b-0 op-5">Checkout my contacts here</h6> -->
                                        </div>
                                        <div class="slimbar" style="padding-top: 0px; overflow-y: scroll; height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget " id="all_Emails">
                                                    <!-- Message -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item bot_box" data-gs-x="8" data-gs-y="14" data-gs-width="4" data-gs-height="6">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #8ee5f6 0%, #16d698 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Notifications</h4>
                                        </div>
                                        <div class="slimbar" style="padding-top: 0px; overflow-y: scroll; height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget " id="all_Notif">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Task</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <!-- <form method="POST" action="<?php echo base_url('admin/Dashboard/saveTask') ?>"> -->
            <form method="POST" id="formTask">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <select class="form-white form-control" data-placeholder="" id="account_type">
                                <option value="">choose user</option>
                                <option value="Lead">Lead</option>
                                <option value="Client">Client</option>
                                <option value="Agent">Agent</option>
                                <option value="Broker">Broker</option>
                            </select>
                        </div>
                        <input type="text" id="account_names" class="form-control hide" placeholder="Enter Task Name">
                        <div class="form-group  col-md-6">
                            <select class="select2" style="width: 100%" id="account_name">

                            </select>
                        </div>
                    </div>
                    <div class="form-group row" style="border: 1px solid #6ec4d7!important; padding-top: 9px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 13px; margin-bottom: 7px!important;">
                        <?php if ($task_temp->num_rows() > 0): ?>
                            <?php foreach ($task_temp->result() as $row): ?>
                                <label class="col-md-3 lh" style="color: #6ec4d7; cursor: pointer;" task_temp="<?php echo $row->task_template_id; ?>"><?php echo $row->task_shortname; ?></label>
                            <?php endforeach?>
                        <?php endif?>
                    </div>
                    <div class="form-group">
                        <label>Task name</label>
                        <input required="" type="text" id="task" class="form-control" placeholder="Enter Task Name"> </div>
                    <div class="form-group">
                        <label>Task Date</label>
                        <input required="" type="text" id="task_date" class="form-control date-inputmask">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>



<script src="<?php echo base_url('assets/plugins/chartist-js/dist/chartist.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>


<script>
    $(document).ready(function(){
        $(".select2").select2();
        allEmails();
        allEtext();
        allAppoint();
        allNotif();
        getTasK();
        getLeadsFromMonthYear('init');

        setTimeout(function(){
            $('.top_boxes').find('.ui-resizable-handle').remove();
            $('.mid_box').find('.ui-resizable-handle').remove();
        },1000);
    });


    $(function() {
        $('.grid-stack').gridstack({
            width: 12,
            alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        });

        $('.grid-stack').data('gridstack').resize(
            $('.grid-stack-item')[0],
            $($('.grid-stack-item')[0]).attr('data-gs-width'),
            Math.ceil(($('.grid-stack-item-content')[0].scrollHeight + $('.grid-stack').data('gridstack').opts.verticalMargin) / ($('.grid-stack').data('gridstack').cellHeight() + $('.grid-stack').data('gridstack').opts.verticalMargin))
        );
    });

    $('#formTask').on('submit', function(e){
        e.preventDefault();
        var account_type = $('#account_type').val();
        var account_id = $('#account_name').val();
        var account_names = $('#account_names').val();
        var task = $('#task').val();
        var task_date = $('#task_date').val();
        $('#myModal').modal('hide');
        addformTask(account_type,account_id,task,task_date,account_type,account_names);
    });


    function addformTask(account_type,account_id,task,task_date,account_type,account_names){
        $.post('<?php echo base_url() ?>admin/Dashboard/saveTask', {account_type,account_id,task,task_date,account_type,account_names}, function(data){
            getTasK();
        });
    }

    function getTasK(){
        $.post('<?php echo base_url() ?>admin/Dashboard/getTasksForThisWeek', function(data){
            var data = JSON.parse(data);
            $('#all_Task').empty();
            $.each(data,function(key,value){
                var task_active = value['task_active'];
                var task_id = value['task_id'];
                var task_date = moment(value['task_date']).format('lll');
                var task_name = value['task_name'];
                var task = value['task'];
                var sender = value['sender'];
                var sender_photo = value['sender_photo'];
                var assigned_to = value['assigned_to'];
                if (task_active == '1') {
                    task_active = 'checked';
                    task_active2 = 'task-done';
                } else {
                    task_active = '';
                    task_active2 = '';
                }
                if (task_active != '2') {
                    var body = '\
                        <li task_id="'+task_id+'" class="list-group-item" data-role="task">\
                            <div class="checkbox checkbox-info">\
                                <input '+task_active+' class="task_id" type="checkbox" id="task_'+task_id+'">\
                                <label for="task_'+task_id+'" class="'+task_active2+'"> \
                                    <span>'+task+'</span> \
                                </label>\
                            </div>\
                            <ul class="assignedto">\
                                <li>\
                                    <span style="color: #26c6da;">'+assigned_to+'</span>\
                                </li>\
                            </ul>\
                            <div class="item-date">'+moment(task_date).format('lll');+'</div>\
                        </li>\
                    ';
                    $('#all_Task').append(body);
                }
            });
        });
    }

    $('#account_type').on('change', function(e){
        var account_type = $('#account_type').val();
        console.log(account_type);
        getAccount(account_type);
    });

    function getAccount(account_type) {
        $.post('<?php echo base_url() ?>admin/dashboard/getAccount', {account_type}, function(data) {
            var data = JSON.parse(data);
            // console.log(data);
            $('#account_name').empty();
            $.each(data, function(key, value){
                var id = value['id'];
                var name = value['fullname'];
                $('#account_names').val(name);
                var body = '\
                    <option value="'+id+'">'+name+'</option>\
                ';
                $('#account_name').append(body);
            });
        });
    }

    function allEmails() {
        $.post('<?php echo base_url() ?>admin/dashboard/getAllEmails', function(data){
            var data = JSON.parse(data);
            // console.log(data);
            $.each(data,function(key,value){
                var fullname = value['fullname'];
                var id = value['id'];
                var date = moment(value['date']).format('lll');
                var subject = value['subject'];
                var table = value['account_type'];
                var converted = value['converted'];
                var anchor = '';
                if (table == 'client') {
                    if (converted == 1) {
                        var account_access = 'https://admin.creditlynx.com/admin/clients?id='+id+'&type=client';
                    } else {
                        var account_access = 'https://admin.creditlynx.com/admin/leads?id='+id+'&type=client';
                    }

                    var type = value['client_type'] == 'Single' ? 'client' : 'joint';
                    anchor = '<a href="#" class="to_client_page" type="'+type+'" id="'+id+'">';
                } else if (table == 'broker'){
                    var account_access = 'https://admin.creditlynx.com/admin/brokers';
                    anchor = '<a href="'+account_access+'">';
                } else if (table == 'agent'){
                    var account_access = 'https://admin.creditlynx.com/admin/agents';
                    anchor = '<a href="'+account_access+'">';
                }
                var body = '\
                    '+anchor+'\
                        <div class="user-img">\
                            <span class="round bg-primary">\
                                <i class="fa fa-lg fa-envelope"></i>\
                            </span>\
                        </div>\
                        <div class="mail-contnet">\
                            <h5>'+fullname+'</h5> \
                            <span class="mail-desc">'+ subject +'</span>\
                            <span class="time">'+date+'</span> \
                        </div>\
                    </a>\
                ';
                $('#all_Emails').append(body);
            });
        });
    }

    function allEtext() {
        $.post('<?php echo base_url() ?>admin/dashboard/getAllEtext', function(data){
            var data = JSON.parse(data);
            // console.log(data);
            $.each(data,function(key,value){
                var fullname = value['fullname'];
                var id = value['id'];
                var date = moment(value['date']).format('lll');
                var subject = value['subject'];
                var message = value['message'];
                var table = value['account_type'];
                var converted = value['converted'];
                var anchor = '';
                if (table == 'client') {
                    if (converted == 1) {
                        var account_access = 'https://admin.creditlynx.com/admin/clients?id='+id+'&type=client';
                    } else {
                        var account_access = 'https://admin.creditlynx.com/admin/leads?id='+id+'&type=client';
                    }

                    var type = value['client_type'] == 'Single' ? 'client' : 'joint';
                    anchor = '<a href="#" class="to_client_page" type="'+type+'" id="'+id+'">';
                } else if (table == 'broker'){
                    var account_access = 'https://admin.creditlynx.com/admin/brokers';
                    anchor = '<a href="'+account_access+'">';
                } else if (table == 'agent'){
                    var account_access = 'https://admin.creditlynx.com/admin/agents';
                    anchor = '<a href="'+account_access+'">';
                }
                var body = '\
                    '+anchor+'\
                        <div class="user-img">\
                            <span class="round bg-primary">\
                                <i class="fa fa-lg fa-mobile-phone"></i>\
                            </span>\
                        </div>\
                        <div class="mail-contnet">\
                            <h5>'+fullname+'</h5> \
                            <span class="mail-desc">'+ message +'</span>\
                            <span class="time">'+date+'</span> \
                        </div>\
                    </a>\
                ';
                $('#all_Etext').append(body);
            });
        });
    }

    function allAppoint(){
        $.post('<?php echo base_url() ?>admin/dashboard/getAppointmentsForThisWeek', function(data){
            var data = JSON.parse(data);
            // console.log(data);
            $.each(data,function(key,value){
                var date_start = value.date_start;
                var date = moment(value.date_start).format('LL');
                var title = '\
                    <div class="card-header" style="background: #e3e6e6; border-color: #e3e6e6; color: black;">\
                        '+date+'\
                    </div>\
                ';

                $.post('<?php echo base_url() ?>admin/dashboard/getAppointmentsForThisWeekDay', {date_start}, function(datas){
                    datas = JSON.parse(datas);
                    // console.log(datas);
                    $('#all_Appoint').append(title);
                    $.each(datas, function(keys, values) {
                        var date_starts = moment(values.date_start).format('LL');
                        var time = moment(values.date_start).format('LT');
                        var to_name = values.to_name;
                        var end = moment(values.date_end).format('LT');
                        var appointment = values.appointment;
                        var body = '\
                            <div class="comment-widgets">\
                                <div class="d-flex flex-row comment-row" style="padding: 0px!important;">\
                                    <div class="comment-text w-100">\
                                        <h6 style="text-transform:uppercase; margin-bottom: 0px; font-size: 10px;">\
                                            '+date_starts+'\
                                        </h6>\
                                        <h4 style="    margin-bottom: 0px;font-weight: 500px!important; font-size: 16px!important;">\
                                            '+time+' '+to_name+'</h4>\
                                        </h4>\
                                        <h6 style="text-transform:uppercase; margin-bottom: 5px; font-size: 10px;">\
                                            '+appointment+' from '+time+' - '+end+' <br>\
                                            (813)440-9146\
                                        </h6>\
                                    </div>\
                                </div>\
                            </div>\
                        ';
                        $('#all_Appoint').append(body);
                    });
                });

            });
        });
    }

    function allNotif() {
        $.post('<?php echo base_url() ?>admin/dashboard/getAllNotif', function(data){
            var data = JSON.parse(data);
            console.log(data);
            $.each(data,function(key,value){
                var fullname = value['fullname'];
                var id = value['id'];
                var date = moment(value['date']).format('lll');
                var subject = value['subject'];
                var message = value['message'];
                var table = value['account_type'];
                var notif_type = value['notif_type'];
                var converted = value['converted'];
                var anchor = '<a href="#" class="'+table+'">';
                if (table == 'client') {
                    if (converted == 1) {
                        var account_access = 'https://admin.creditlynx.com/admin/clients?id='+id+'&type=client';
                    } else {
                        var account_access = 'https://admin.creditlynx.com/admin/leads?id='+id+'&type=client';
                    }

                    var type = value['client_type'] == 'Single' ? 'client' : 'joint';
                    anchor = '<a href="#" class="to_client_page" type="'+type+'" id="'+id+'">';
                } else if (table == 'broker'){
                    var account_access = 'https://admin.creditlynx.com/admin/brokers';
                    anchor = '<a href="'+account_access+'">';
                } else if (table == 'agent'){
                    var account_access = 'https://admin.creditlynx.com/admin/agents';
                    anchor = '<a href="'+account_access+'">';
                }
                if (notif_type == 'email') {
                    var icon = '<i class="fa fa-lg fa-envelope"></i>';
                } else {
                    var icon = '<i class="fa fa-lg fa-mobile-phone"></i>';
                }
                var body = '\
                    '+anchor+'\
                        <div class="user-img">\
                            <span class="round bg-primary">\
                                '+icon+'\
                            </span>\
                        </div>\
                        <div class="mail-contnet">\
                            <h5>'+fullname+'</h5> \
                            <span class="mail-desc" style="white-space: pre-wrap;">'+ message +'</span>\
                            <span class="time">'+date+'</span> \
                        </div>\
                    </a>\
                ';
                $('#all_Notif').append(body);
            });
        });
    }

    $('[data-role="tasklist"]').on('click','input[type="checkbox"]',function(){
        var task_id = $(this).closest('li').attr('task_id');
        // alert(task_id);
        var checked = $(this).is(':checked');
        console.log(task_id);
        updateTask(task_id,checked);
        // alert(checked);
        function updateTask(task_id,task_active) {
            $.post('<?php echo base_url('admin/Dashboard') ?>/updateTask',
                {task_id,task_active},function(data){
                }).fail(function(xhr){
                    console.log(xhr.responseText);
            });
        }
    });

    $('.lh').on('click',function(e) {
        var task_template_id = $(this).closest('.lh').attr('task_temp');
        // console.log(task_template_id);
        getTemplate(task_template_id);
    });
    function getTemplate(task_template_id) {
        $.post('<?php echo base_url() ?>admin/dashboard/getTemplate', {task_template_id}, function(data) {
            var data = JSON.parse(data);
            $.each(data,function(key, value){
                var task_template = value['task_template'];
                $('#task').val(task_template);
            });
        })
    }


    $('.btnChartPrevMonth').on('click', function(event) {
        event.preventDefault();
        getLeadsFromMonthYear('prev');
    });

    $('.btnChartNextMonth').on('click', function(event) {
        event.preventDefault();
        getLeadsFromMonthYear('next');
    });


    function getLeadsFromMonthYear(direction) {
        var date = $('.labelDate').html();

        $.post('<?php echo base_url('admin/dashboard/getLeadsFromMonthYear') ?>', {direction,date}, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            $('.campaign').empty();
            data = JSON.parse(data);
            // console.log(data);
            $('.labelMonth').html(moment(data['month']).format('MMMM'));
            $('.labelYear').html(moment(data['year']).format('YYYY'));
            $('.labelDate').html(data['date']);

            var days = [];
            var leads = [];
            var clients = [];
            $.each(data['leads'],function(key,value){
                days.push(key);
                leads.push(value);
            });
            $.each(data['clients'],function(key,value){
                clients.push(value);
            });

            var max = Math.max.apply(Math,leads) ? Math.max.apply(Math,leads) : 15;
            var chart = new Chartist.Line('.campaign', {
                    labels: days,
                    series: [
                            leads
                            , clients
                        ]}, {
                    low: 0,
                    high: max,
                    showArea: true,
                    fullWidth: true,
                    plugins: [
                    Chartist.plugins.tooltip()
                    ],
                        axisY: {
                        onlyInteger: true
                    , scaleMinSpace: 40
                    , offset: 20
                    , labelInterpolationFnc: function (value) {
                        // return (value / 1) + 'k';
                        return value;
                    }
                },
                });
        });
    }

</script>
<?php
$this->load->view('admin/includes/footer.php');
?>

<!--
<script src="//code.tidio.co/znwkebawhxoeqzsan1dg25adcdzy8dfe.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript">
    // Set to false if opt-in required
    var trackByDefault = true;

    function acEnableTracking() {
        var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
        document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
        acTrackVisit();
    }

    function acTrackVisit() {
        var trackcmp_email = '';
        var trackcmp = document.createElement("script");
        trackcmp.async = true;
        trackcmp.type = 'text/javascript';
        trackcmp.src = '//trackcmp.net/visit?actid=223610313&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
        var trackcmp_s = document.getElementsByTagName("script");
        if (trackcmp_s.length) {
            trackcmp_s[0].parentNode.appendChild(trackcmp);
        } else {
            var trackcmp_h = document.getElementsByTagName("head");
            trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
        }
    }

    if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
        acEnableTracking();
    } -->