<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?>  
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h5 class="card-title"><?php echo $page_title ?> (<?php echo $user_count ?>) 
                                    </h5> 
                                    
                                    <div class="table-responsive" >
                                        <a style="margin-top: 14px" href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" >New <?php echo $page_title_s ?> </a>
                                        <table id="pageTableBilling" class="table stylish-table">
                                            <thead>
                                                <tr> 
                                                    <th style="width: 1%"></th>
                                                    <th>Name</th>
                                                    <th>Title</th> 
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Tools</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($users as $key => $value): ?>
                                                    <tr id="<?php echo $value['employee_id'] ?>" > 
                                                        <td style="text-align: center;">
                                                            <img class="img-circle" width="30px" src="<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/<?php echo $value['photo'] ?>" alt="photo"> 
                                                        </td>
                                                        <td>
                                                            
                                                            <a href="#" class="goToProfileBilling"><u><?php echo $value['name'] ?></u></a>      
                                                        </td>
                                                        <td>
                                                            <?php echo $value['job_title'] ?>
                                                        </td>
                                                        <td>
                                                            <span class="phone_format"><?php echo $value['phone_number'] ?></span> 
                                                        </td> 
                                                        <td>
                                                            <?php echo $value['email_address'] ?>
                                                        </td> 
                                                        <td><a href="#" class="text-danger btnDeleteEmployee"><i class="fas fa-trash"></i> Delete</a></td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="profileSection" class="row fade"> 
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"> 
                                <div class="tab-pane active show" id="profile" role="tabpanel">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 text-center b-r" > 
                                                <form id="uploadProfilePictureForm" enctype="multipart/form-data">
                                                    <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="<?php echo base_url() ?>/assets/images/LynxLogo.png" data-show-remove="false" data-height="150" />
                                                    <input type="hidden" name="employee_id">
                                                </form>   
                                                <h3 class="name m-b-0"></h3>
                                            </div>
 
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <strong>Job Title</strong>
                                                <br>
                                                <p class="text-muted job_title"></p>
                                                <strong>Location</strong>
                                                <br>
                                                <p class="text-muted ">
                                                    <span class="address"></span>, 
                                                    <span class="city"></span><br>
                                                    <span class="state_provinces"></span>, 
                                                    <span class="Zip_code"></span>
                                                </p>
                                            </div>
                                            <div class="col-md-5 col-xs-6 "> 
                                                <strong>Email</strong>
                                                <br>
                                                <p class="text-muted email_address"></p>
                                                <strong>Phone</strong>
                                                <br>
                                                <p class="text-muted phone_number phone_format"></p>
                                            </div> 
                                        </div>
                                        <hr> 
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h4>Other Information</h4>
                                                <span>Date of Birth: <span class="date_of_birth"></span></span><br> 
                                                <span>Social: xxx-xx-<span class="ss_restricted"></span></span><br> 
                                                <span>Salary: <span class="salary"></span></span><br>
                                                <span>Hourly: <span class="hourly"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Bank Information</h5>
                                                <span>Bank Name: <span class="bank_name"></span></span><br>
                                                <span>Bank Address: <span class="bank_address"></span></span><br>
                                                <span>Account #: <span class="account_number"></span></span><br>
                                                <span>Routing #: <span class="routing_number"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Emergency Information</h5>
                                                <span>Name: <span class="emergency_name"></span></span><br>
                                                <span>Phone: <span class="emergency_phone"></span></span><br>
                                                <span>Address: <span class="emergency_address"></span></span><br>
                                                <span>City: <span class="emergency_city"></span></span><br>
                                                <span>State: <span class="emergency_state"></span></span><br>
                                                <span>Zip: <span class="emergency_zip"></span></span><br>
                                                <span>Relationship: <span class="emergency_relationship"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>User Account Information</h5>
                                                <span>Username: <span class="username"></span></span><br>
                                                <span>Password: <span>*******</span></span><br>
                                                <span>Last Login: <span class="last_login"></span></span><br>
                                            </div>
                                        </div>


                                        <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                            <li class="nav-item"> <a id="tasks-tab-header" class="nav-link active show" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="true">Tasks</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a task</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_task" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Task Date">
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewTask" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="task_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                        
                                                        
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="notes-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"><br>sticky?</i>
                                                            
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewNote" class="btn btn-success waves-effect waves-light" type="button">Save Note</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="note_list_sticky" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                        <div id="note_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="row">
                                                <div class="col-md-3 col-xs-6">
                                                    <h4 style="margin-bottom: 17px">Basic Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_name" class="control-label">Name:</label>
                                                        <input type="text" name="name" required="" class="form-control" id="profile_name">
                                                        <span class="bar"></span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="profile_address" class="control-label">Address:</label>
                                                        <input type="text" name="address"  class="form-control" id="profile_address">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_city" class="control-label">City:</label>
                                                        <input type="text" name="city"  class="form-control" id="profile_city">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5>State/Province:</h5> 
                                                        <select class="select2" name="state" id="profile_state" style="width: 100%">
                                                            <option value="0">Select State/Province</option>
                                                            <?php foreach ($state_provinces as $key => $value): ?>
                                                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_zip_code" class="control-label">Zip_code:</label>
                                                        <input type="text" name="zip_code"  class="form-control" id="profile_zip_code">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_phone_number" class="control-label">Phone Number:</label>
                                                        <input type="text" name="phone_number"  class="form-control phone-inputmask" id="profile_phone_number">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_email_address" class="control-label">Email Address:</label>
                                                        <input type="email" name="email_address"  class="form-control" id="profile_email_address">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_date_of_birth" class="control-label">Date of Birth:</label>
                                                        <input type="text" name="date_of_birth"  class="form-control date-inputmask" id="profile_date_of_birth">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_ss" class="control-label">Social:</label>
                                                        <input type="text" name="ss"  class="form-control ss-inputmask" id="profile_ss">
                                                        <span class="bar"></span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3 col-xs-6  ">
                                                    
                                                    <h4 style="margin-bottom: 17px">Bank Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_bank_name" class="control-label">Bank Name:</label>
                                                        <input type="text" name="bank_name"  class="form-control" id="profile_bank_name">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_bank_address" class="control-label">Bank Address (address, city, state, zip):</label>
                                                        <input type="text" name="bank_address"  class="form-control" id="profile_bank_address">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_account_number" class="control-label">Account #:</label>
                                                        <input type="text" name="account_number"  class="form-control" id="profile_account_number">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_routing_number" class="control-label">Routing #:</label>
                                                        <input type="text" name="routing_number"  class="form-control" id="profile_routing_number">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <br>
                                                    
                                                    

                                                    
                                                </div>
                                                <div class="col-md-3 col-xs-6">
                                                    <h4 style="margin-bottom: 17px">Emergency Contact Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_name" class="control-label">Name:</label>
                                                        <input type="text" name="emergency_name"  class="form-control" id="profile_emergency_name">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_phone" class="control-label">Phone Number:</label>
                                                        <input type="text" name="emergency_phone"  class="form-control" id="profile_emergency_phone">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_address" class="control-label">Address:</label>
                                                        <input type="text" name="emergency_address"  class="form-control" id="profile_emergency_address">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_city" class="control-label">City:</label>
                                                        <input type="text" name="emergency_city"  class="form-control" id="profile_emergency_city">
                                                    </div>
                                                    <div class="form-group"> 
                                                        <h5>State/Province:</h5> 
                                                        <select class="select2" name="emergency_state" id="profile_emergency_state" style="width: 100%">
                                                            <option value="0">Select State/Province</option>
                                                            <?php foreach ($state_provinces as $key => $value): ?>
                                                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_zip" class="control-label">Zip:</label>
                                                        <input type="text" name="emergency_zip"  class="form-control" id="profile_emergency_zip">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_emergency_relationship" class="control-label">Relationship:</label>
                                                        <input type="text" name="emergency_relationship"  class="form-control" id="profile_emergency_relationship">
                                                    </div>
                                                    <br>
                                                </div>
                                                <div class="col-md-3 col-xs-6">
                                                    <h4 style="margin-bottom: 17px">User Account Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_username" class="control-label">Username:</label>
                                                        <input type="text" name="username" required="" class="form-control" id="profile_username">
                                                        <span class="bar"></span> 
                                                        <small id="profile_errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small>  
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_password" class="control-label">Password:</label>
                                                        <input type="password" name="password" required="" class="form-control" id="profile_password">
                                                        <span class="bar"></span>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label for="profile_job_title" class="control-label">Job Title:</label>
                                                        <input type="text" name="job_title"  class="form-control" id="profile_job_title">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_salary" class="control-label">Salary:</label>
                                                        <input type="text" name="salary"  class="form-control" id="profile_salary">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_hourly" class="control-label">Hourly:</label>
                                                        <input type="text" name="hourly"  class="form-control" id="profile_hourly">
                                                        <span class="bar"></span>
                                                    </div>
                                                    
                                                </div>
                                                
                                                
                                                <!-- <div class="col-12">
                                                    <button id="btnProfileUpdate" type="button" class="btn btn-success pull-right waves-effect waves-light">Update Profile</button>
                                                </div> -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <?php echo $page_title_s ?> Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body"> 
                <form enctype="multipart/form-data" id="formAddUser" method="post" action="<?php echo $page_controller ?>/saveDetail" class="floating-labels"> 
                    <input type="hidden" name="employee_id">
                    <h5 for="input-file-now-custom-1">Profile Picture</h5>
                    <input name="user_photo" type="file" id="input-file-now-custom" class="dropify" data-default-file="<?php echo base_url() ?>/assets/images/LynxLogo.png" data-show-remove="false"/>
                    <input id="current_photo" type="hidden" name="current_photo" value="LynxLogo.png"> 
                    <div class="row">
                        <div class="col-6  m-t-40">
                            <h4 style="margin-bottom: 17px">Basic Information</h4>
                            <div class="form-group">
                                <label for="name" class="control-label">Name:</label>
                                <input name="name" type="text"  class="form-control" id="name">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="job_title" class="control-label">Job Title:</label>
                                <input name="job_title" type="text"  class="form-control" id="job_title">
                                <span class="bar"></span>
                            </div>

                            <div class="form-group">
                                <label for="address" class="control-label">Address:</label>
                                <input type="text" name="address"  class="form-control" id="address">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="city" class="control-label">City:</label>
                                <input type="text" name="city"  class="form-control" id="city">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <h5>State/Province:</h5> 
                                <select class="select2" name="state" id="state" style="width: 100%">
                                    <option value="0">Select State/Province</option>
                                    <?php foreach ($state_provinces as $key => $value): ?>
                                        <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="zip_code" class="control-label">Zip_code:</label>
                                <input type="text" name="zip_code"  class="form-control" id="zip_code">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="date_of_birth" class="control-label">Date of Birth:</label>
                                <input type="text" name="date_of_birth"  class="form-control material_date" id="date_of_birth">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="email_address" class="control-label">Email Address:</label>
                                <input type="email" name="email_address"  class="form-control" id="email_address">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="control-label">Phone Number:</label>
                                <input type="text" name="phone_number"  class="form-control" id="phone_number">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="ss" class="control-label">Social:</label>
                                <input type="text" name="ss"  class="form-control" id="ss">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="salary" class="control-label">Salary:</label>
                                <input type="text" name="salary"  class="form-control" id="salary">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="hourly" class="control-label">Hourly:</label>
                                <input type="text" name="hourly"  class="form-control" id="hourly">
                                <span class="bar"></span>
                            </div>
                        </div>
                        <div class="col-6 m-t-40">
                            
                            <h4 style="margin-bottom: 17px">Bank Information</h4>
                            <div class="form-group">
                                <label for="bank_name" class="control-label">Bank Name:</label>
                                <input type="text" name="bank_name"  class="form-control" id="bank_name">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="bank_address" class="control-label">Bank Address (address, city, state, zip):</label>
                                <input type="text" name="bank_address"  class="form-control" id="bank_address">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="account_number" class="control-label">Account #:</label>
                                <input type="text" name="account_number"  class="form-control" id="account_number">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="routing_number" class="control-label">Routing #:</label>
                                <input type="text" name="routing_number"  class="form-control" id="routing_number">
                                <span class="bar"></span>
                            </div>
                            <br>
                            <h4 style="margin-bottom: 17px">Emergency Contact Information</h4>
                            <div class="form-group">
                                <label for="emergency_name" class="control-label">Name:</label>
                                <input type="text" name="emergency_name"  class="form-control" id="emergency_name">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="emergency_phone" class="control-label">Phone Number:</label>
                                <input type="text" name="emergency_phone"  class="form-control" id="emergency_phone">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="emergency_address" class="control-label">Address:</label>
                                <input type="text" name="emergency_address"  class="form-control" id="emergency_address">
                            </div>
                            <div class="form-group">
                                <label for="emergency_city" class="control-label">City:</label>
                                <input type="text" name="emergency_city"  class="form-control" id="emergency_city">
                            </div>
                            <div class="form-group"> 
                                <h5>State/Province:</h5> 
                                <select class="select2" name="emergency_state" id="emergency_state" style="width: 100%">
                                    <option value="0">Select State/Province</option>
                                    <?php foreach ($state_provinces as $key => $value): ?>
                                        <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="emergency_zip" class="control-label">Zip:</label>
                                <input type="text" name="emergency_zip"  class="form-control" id="emergency_zip">
                            </div>
                            <div class="form-group">
                                <label for="emergency_relationship" class="control-label">Relationship:</label>
                                <input type="text" name="emergency_relationship"  class="form-control" id="emergency_relationship">
                            </div>
                            <br>
                            <h4 style="margin-bottom: 17px">User Account Information</h4>
                            <div class="form-group">
                                <label for="username" class="control-label">Username:</label>
                                <input name="username" type="text"  class="form-control" id="username">
                                <span class="bar"></span> 
                                <small id="errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small>  
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password:</label>
                                <input name="password" type="password"  class="form-control" id="password">
                                <span class="bar"></span>
                            </div>
                        </div>
                    </div>
                        
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
 
 <script>
$(document).ready(function() {
    // $('.left_nav_'+'<?php echo $page_controller ?>').addClass('active');
    var newUser = false;
    var employee_id = 0; 

    $('.btnDeleteEmployee').on('click',function(e){
        e.preventDefault();
        var employee_id = $(this).closest('tr').attr('id');
        var tr = $(this).closest('tr');
        swal({   
            title: "Deleting this Employee?!",   
            text: 'Are you sure you want to delete this employee?',   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes!",   
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo $page_controller ?>/deleteEmployee',{employee_id}, function(data){
                        // data = JSON.parse(data);
                        console.log(data);
                        if (data == 'deleted') {tr.remove();} 
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
            }
        }); 
    });

    $('#formAddUser').on('submit',function(e){
        var errorUsername = $('#errorUsername');  
        if (errorUsername.hasClass('hide')) { 
            $('#formAddUser').submit();
        }  else {
            e.preventDefault();
        }
        
        
    });

    $('#pageTableBilling').DataTable({ 
        'aaSorting': [],
        "aoColumnDefs": [{ 
            'bSortable': false, 
            'aTargets': [0] 
        }],
        dom: 'Bfrtip',
        "pageLength": 100
    }); 
    $('#pageTableBilling').removeClass('dataTable');
    $('#pageTableBilling').on('click','.goToProfileBilling',function(e){
        e.preventDefault();
        newUser = false;
        employee_id = $(this).closest('tr').attr('id');
        var name = $(this).find('u').html();
        getDetails(employee_id);
        getTasks(employee_id);
        getNotes(employee_id);
        $('#profileSection').addClass('show');
        $('#listSection').addClass('hide');
        $('#profile_active').removeClass('hide');
        $('#profile_active').html(name);
        // $('#add-edit-modal').modal('toggle');
    });

    $('#btnNewUser').on('click',function(){
        newUser = true;
        $('#formAddUser')[0].reset();
        $('input[name=username]').prop('readonly',false);
        $('.dropify-render').find('img').attr('src','<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/LynxLogo.png');

        var inputs =  $('#formAddUser input'); 
        $.each(inputs,function(key,input){ 
            $(input).closest('.form-group').removeClass('focused');
        }); 

    });

    $('#username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfExist(username);
        }
        
    });

    

    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,employee_id,task,task_date);
            } else {
                task_date.focus();
            }
            
        } else {
            task.focus();
        }
    });

    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });

    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,employee_id,note,note_sticky);
        } else {
            note.focus();
        }
    });

    $('#uploadProfilePicture').on('change',function(){
        $.ajax({ 
            url: '<?php echo $page_controller ?>/uploadProfilePicture',
            type: 'POST', 
            data: new FormData($('#uploadProfilePictureForm')[0]), 
            cache: false,
            contentType: false,
            processData: false, 
            success: function(data) { 

            }
        });
    });

    function getDetails(employee_id) {
        $.post('<?php echo $page_controller ?>/getDetails',
            {employee_id},function(data){
                data = JSON.parse(data);  
                $('input[name=current_photo]').val(data['photo']);
                $('.dropify-render').find('img').attr('src','<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/'+data['photo']);
                
                $('#profileSection').find('#profile_picture').attr('src','<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/'+data['photo']); 
                $('input[name=date_of_birth]').val(moment(data['date_of_birth']).format('YYYY-MM-DD'));

                 
                $.each(data,function(k,v){
                    $('input[type=text][name='+k+']').val(v);
                    $('input[type=email][name='+k+']').val(v);
                    $('input[type=password][name='+k+']').val(v);
                    $('input[type=hidden][name='+k+']').val(v);
                    $('select[name='+k+']').val(v);

                    $('.'+k).html(v);
                    // console.log(k+ ' ' +v);
                }) ;

                var last_login = data['last_login'];
                if (last_login) {
                    last_login = moment(last_login).format('MM/DD/YYYY h:mm:ss a');
                    $('.last_login').html(last_login);
                }

                $('#profileSection select').trigger('change');

                var inputs =  $('#formAddUser input'); 
                $.each(inputs,function(key,input){ 
                    $(input).closest('.form-group').addClass('focused');
                });
  

                $('#settings').on('change','input,select',function(){
                    var field = $(this).attr('id');
                    field = field.replace('profile_','');
                    var value = $(this).val(); 
                    updateFieldEmployee(field,value);
                    function updateFieldEmployee(field,value) {
                        $.post('<?php echo $page_controller ?>/updateFieldEmployee', {employee_id,field,value} ,function(data){
                                // data = JSON.parse(data); 
                                console.log(data);
                                // getDetails(broker_id);
                                $('.'+field).html(value);
                                $.toast({
                                    heading: 'Update Success!',
                                    text: 'detail successfully updated to '+value,
                                    position: 'top-right',
                                    loaderBg:'#17a2b8',
                                    icon: 'success',
                                    hideAfter: 3500, 
                                    stack: 6
                                  });
                            }).fail(function(xhr){
                                console.log(xhr.responseText);
                            });
                        }
                });
        });
    }

    
    function checkIfExist(username) {
        var username_val = username.val();
        $.post('<?php echo $page_controller ?>/checkIfExist',
            {username_val},function(data){
                // console.log(data);
                if (data == '0') {
                    username.removeClass('form-control-danger');
                    username.closest('.form-group').removeClass('has-danger');
                    username.closest('.form-group').removeClass('has-error');
                    $('#errorUsername').addClass('hide');
                } else { 
                    username.addClass('form-control-danger');
                    username.closest('.form-group').addClass('has-danger');
                    username.closest('.form-group').addClass('has-error');
                    $('#errorUsername').removeClass('hide');
                }
            });
    }

   

    function getTasks(employee_id) {
        $.post('<?php echo $page_controller ?>/getTasks',
            {employee_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-warning waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }

                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="<?php echo base_url() ?>/assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.task_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.task+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });

                $('.btnUpdateTaskDone').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,0);
                });

                $('.btnUpdateTaskActive').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,1);
                });

                $('.btnUpdateTaskDelete').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,2);
                });
            });
    }

    function saveTask(button,employee_id,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveTask',
            {employee_id,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(employee_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateTask',
            {task_id,task_active},function(data){
                getTasks(employee_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getNotes(employee_id) {
        $.post('<?php echo $page_controller ?>/getNotes',
            {employee_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }

                    var newNote = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="<?php echo base_url() ?>/assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.note_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.note+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });

                $('.btnUpdateNoteSticky').on('click',function(){
                    var button = $(this);
                    var note_id = $(this).closest('.sl-item').attr('value');
                    if (button.hasClass('text-warning')) {
                        updateNote(button,note_id,0);
                    } else {
                        updateNote(button,note_id,1);
                    }
                    
                });
 
            });
    }

    function saveNote(button,employee_id,note,note_sticky){
        var note_ = note;
        var note = note.val(); 
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveNote',
            {employee_id,note,note_sticky},function(data){
                // console.log(data); 
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                getNotes(employee_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateNote',
            {note_id,note_sticky},function(data){
                getNotes(employee_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

});
</script>
<?php 
    $this->load->view('admin/includes/footer.php'); 
?> 

