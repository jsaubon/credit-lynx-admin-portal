<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?>  
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row fade show">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h5 class="card-title"> 
                                        <div class="col-12 col-md-6 offset-md-3 text-center">
                                            <label class="control-label">Date Filter (From - To)</label> 
                                            <div class="input-group"> 
                                                <input id="filterFrom" type="text" name="" class="form-control material_date text-center dateFilter" value="<?php echo date('m/d/Y') ?>" >
                                                <input id="filterTo" type="text" name="" class="form-control material_date text-center dateFilter" value="<?php echo date('m/d/Y') ?>" >
                                                <button id="btnDateFilter" style="border-radius: 0" class="btn btn-success waves-effect waves-light"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                            
                                            
                                    </h5> 
                                    
                                    <div class="table-responsive" >
                                        <table id="pageTable" class="table stylish-table">
                                            <thead>
                                                <tr> 
                                                    <th style="width: 1%"></th>
                                                    <th>Name</th>
                                                    <th width="20%">Position</th> 
                                                    <th>Work Date</th>
                                                    <th width="20%">Worked Hours</th>
                                                    <th width="15%">Total Paid</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <!-- <tr> 
                                                        <td style="text-align: center;">
                                                             
                                                        </td>
                                                        <td>
                                                            
                                                            <a href="#" ><u><?php echo $value['name']. ' ' .$value['last_name'] ?></u></a>      
                                                        </td>
                                                        
                                                    </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
 
 
 <script>
$(document).ready(function() { 

    var pageTable = $('#pageTable').DataTable({ 
        'aaSorting': [],
        "aoColumnDefs": [{ 
            'bSortable': false, 
            'aTargets': [0] 
        }],
        dom: 'Bfrtip',
        "pageLength": 100
    });  

    $('#pageTable').removeClass('dataTable');

    getEmployeesTimeWorkedTable('<?php echo date('m/d/Y') ?>','<?php echo date('m/d/Y') ?>');
    $('#btnDateFilter').on('click',function(){
        var from = $('#filterFrom').val();
        var to = $('#filterTo').val();
        getEmployeesTimeWorkedTable(from,to);
    });

    function getEmployeesTimeWorkedTable(from,to) {
        $.post('<?php echo $page_controller ?>/getEmployeesTimeWorked',
            {from,to},function(data){
                
                data = JSON.parse(data);
                console.log(data); 
                pageTable.clear().draw();
                $.each(data,function(key,value){
                    var worker = value.worker;
                    var worker_id = value.worker_id;
                    var photo = value.photo;
                    var name = value.name;
                    var h = value.hours; 
                    var m = value.minutes; 
                    var folder_name = '';
                    if (worker == 'Administrator') {
                        folder_name = 'admins';
                    } 
                    if (worker == 'Dispute Team') {
                        folder_name = 'processors';
                    } 
                    if (worker == 'Sales Team') {
                        folder_name = 'sales';
                    } 

                    var hourly = value.hourly;
                    var total_paid = parseInt(value.hours)*parseInt(hourly);
                    console.log(total_paid);


                    var photo = '<img class="img-circle" width="30px" src="<?php echo base_url() ?>/assets/images/users/'+folder_name+'/'+value.photo+'" alt="photo">';
                    pageTable.row.add([photo,name,worker,value.work_date,h+'h '+m+'m',total_paid]).draw(false);;

                });
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
});
</script>
<?php 
    $this->load->view('admin/includes/footer.php'); 
?> 

