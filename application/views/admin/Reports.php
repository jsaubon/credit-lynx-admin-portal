<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>

			<div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><a href="">Reports</a> </h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Reports</a> </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	<div class="row">
                            		<div class="col-sm-12 col-md-3">
                            			<label for="">Select Type</label>
                            			<select name="" id="selectType" class="form-control">
                            				<option value="">Select Type</option>
                            				<option value="Client">Client</option>
                            				<option value="Lead">Lead</option>
                            			</select>
                            		</div>
                                    <div class="col-sm-12 col-md-3">
                                        <label for="">Select Filter</label>
                                        <select name="" id="selectFilter" class="form-control toExport">
                                            <option value="">Select Filter</option>
                                            <option value="selectClientStatus">Client Status</option>
                                            <option value="selectLeadStatus">Lead Status</option>
                                            <option value="selectBroker">Broker</option>
                                            <option value="selectAgent">Agent</option>
                                            <option value="selectSales">Sales Team</option>
                                            <option value="inputDFRMin_inputDFRMax">DFR</option>
                                        </select>
                                    </div>
                            		<div class="col-sm-12 col-md-5 hide selectExports">
                            			<label for="">Client Status</label>
                            			<select class="form-control toExport" id="selectClientStatus" field="client_status">
                            				<option value="">Select Option</option>
											<?php foreach ($client_statuses as $key => $value): ?>
												<option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
											<?php endforeach?>
                            			</select>
                            		</div>
                            		<div class="col-sm-12 col-md-5 hide selectExports">
                            			<label for="">Lead Status</label>
                            			<select class="form-control toExport" id="selectLeadStatus" field="lead_status">
                            				<option value="">Select Option</option>
											<?php foreach ($lead_statuses as $key => $value): ?>
												<option value="<?php echo $value['lead_status'] ?>"><?php echo $value['lead_status'] ?></option>
											<?php endforeach?>
                            			</select>
                            		</div>
                            		<div class="col-sm-12 col-md-5 hide selectExports">
                            			<label for="">Broker</label>
                            			<select class="form-control toExport" id="selectBroker" field="broker">
                            				<option value="">Select Option</option>
											<?php foreach ($brokers as $key => $value): ?>
												<option value="<?php echo $value['broker'] ?>"><?php echo $value['broker'] ?></option>
											<?php endforeach?>
                            			</select>
                            		</div>
                            		<div class="col-sm-12 col-md-5 hide selectExports">
                            			<label for="">Agent</label>
                            			<select class="form-control toExport" id="selectAgent" field="agent">
                            				<option value="">Select Option</option>
											<?php foreach ($agents as $key => $value): ?>
												<option value="<?php echo $value['agent'] ?>"><?php echo $value['agent'] ?></option>
											<?php endforeach?>
                            			</select>
                            		</div>
                                    <div class="col-sm-12 col-md-5 hide selectExports">
                                        <label for="">Sales Team</label>
                                        <select class="form-control toExport" id="selectSales" field="sale">
                                            <option value="">Select Option</option>
                                            <?php foreach ($sales as $key => $value): ?>
                                                <option value="<?php echo $value['name'] ?>"><?php echo $value['name'] ?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                            		<div class="col-sm-12 col-md-5 hide selectExports">
                            			<label for="">DFR</label>
                            			<div class="input-group">
                            				<input type="number" id="inputDFRMin" class="form-control toExport" placeholder="Min">
                            				<input type="number" id="inputDFRMax" class="form-control toExport" placeholder="Max">
                            			</div>
                            		</div>
                            		<div class="col-sm-12 col-md-4 colButton hide">
                            			<label for="" style="color: transparent;">Export</label>
                            			<button class="btnExport btn btn-success btn-block">Export <i class="mdi mdi-file-excel"></i></button>
                            		</div>
                            	</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
    $('.left_nav_reports').addClass('active');

    $('#selectFilter').on('change', function(event) {
    	event.preventDefault();
    	$('.colButton').removeClass('hide');
    	var select = $(this).val();
    	$('.selectExports').addClass('hide');
    	if (select != 'inputDFRMin_inputDFRMax') {

	    	$('#'+select).closest('div').removeClass('hide');
	    	$('#'+select).closest('div').addClass('show');
    	} else {
    		var inputs = select.split('_');
    		$('#'+inputs[0]).closest('.selectExports').removeClass('hide');
	    	$('#'+inputs[0]).closest('.selectExports').addClass('show');
    	}

    });

    $('.btnExport').on('click', function(event) {
    	event.preventDefault();
    	var filter = $('.selectExports.show').find('.toExport');
    	if (filter.length == 2) {
    		console.log(filter);
    		var min = $(filter[0]).val();
    		var max = $(filter[1]).val();
    		var field = 'dfr';
            var type = $('#selectType').val();
            if (type != '') {
                var url = "<?php echo base_url('') ?>"+'admin/reports/exportFilter?type='+type+'&field='+field+'&min='+min+'&max='+max;
                window.location.href = url;
            }

    	} else {
			var field = filter.attr('field');
	    	var value = filter.val();
            var type = $('#selectType').val();
            if (type != '') {
                var url = "<?php echo base_url('') ?>"+'admin/reports/exportFilter?type='+type+'&field='+field+'&value='+value;
            }
	    	window.location.href = url;
    	}

    });
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>