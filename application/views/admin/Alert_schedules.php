<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    #pageTable tbody tr td:nth-child(8) {
        text-align: center;;
    }

    #pageTable tbody tr td:nth-child(7) {
        text-align: right;;
    }

    .dataTables_filter {
        float: right !important; 
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Alert Schedules</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div id="containerList" class="card-body"> 
                                        
                                
                                <div class="table-responsive"> 
                                    <table class="table stylish-table m-t-20" id="pageTable">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th> 
                                                <th>Round Alert Date</th>
                                                <th>Schedule</th>
                                            </tr>
                                        </thead>
                                        <tbody>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script>
$(document).ready(function(){  
    $('#pageTable').DataTable();
    get_client_alert_schedules();
    function get_client_alert_schedules() {
        $.post('alert_schedules/get_auto_alert_schedule_clients', function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
        });
    }
});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 