<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.js"></script>  

<script type="text/javascript">
    // Set to false if opt-in required
    var trackByDefault = true;

    function acEnableTracking() {
        var expiration = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
        document.cookie = "ac_enable_tracking=1; expires= " + expiration + "; path=/";
        acTrackVisit();
    }

    function acTrackVisit() {
        var trackcmp_email = '';
        var trackcmp = document.createElement("script");
        trackcmp.async = true;
        trackcmp.type = 'text/javascript';
        trackcmp.src = '//trackcmp.net/visit?actid=223610313&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
        var trackcmp_s = document.getElementsByTagName("script");
        if (trackcmp_s.length) {
            trackcmp_s[0].parentNode.appendChild(trackcmp);
        } else {
            var trackcmp_h = document.getElementsByTagName("head");
            trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
        }
    }

    if (trackByDefault || /(^|; )ac_enable_tracking=([^;]+)/.test(document.cookie)) {
        acEnableTracking();
    }

       
    

// STARTER
    
    // From Invite
        function getQueryParams(qs) {
            qs = qs.split("+").join(" ");
            var params = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])]
                    = decodeURIComponent(tokens[2]);
            }

            return params;
        }

        var $_GET = getQueryParams(document.location.search); 
        if (typeof($_GET['cname']) != "undefined" && $_GET['cname'] !== null) { 
            var name = $_GET['cname']; 
            var client_id = $_GET['id'];  

            getProfile(client_id);
            function getProfile(client_id) {
                jQuery.post('../wp-custom-functions/get_client_information.php',{client_id,name}, function(data){
                    // console.log(data);
                        data = JSON.parse(data);
                        var client_type = 'Single';
                        jQuery.each(data['profile'],function(key,value){ 
                            jQuery.each(value,function(k,v){
                                jQuery('[name=profile_'+k+']').val(v);  
                                jQuery('.'+k).html(v); 
                                jQuery('.profile_'+k).html(v);
                                jQuery('.profile_'+k).val(v); 
                            })  

                            client_type = value.client_type;
                            if (client_type == 'Joint') {
                                jQuery('.container_joint_inputs').removeClass('hide');
                            } 
                        }); 
                        jQuery.each(data['billing'],function(key,value){ 
                            jQuery.each(value,function(k,v){
                                jQuery('[name=billing_'+k+']').val(v); 
                                jQuery('.'+k).html(v); 
                                jQuery('.billing_'+k).val(v); 
                            }); 
                            


                            // console.log(value.plan);
                            // console.log(client_type);
                            if (value.plan == 'Monitor' || value.plan == 'Monitor Plus' || value.plan == 'Optimize' || value.plan == 'Optimize Plus' || value.plan == 'Limited' || value.plan == 'Limited Plus') { 
                                jQuery('.extraPricingChart').removeClass('hide');
                                jQuery('.normalPricingChart').remove();
                                jQuery('#coupon_codeDIV').addClass('hide');
                                // console.log('extra');
                                if (client_type == 'Joint') { 
                                    // console.log('joint');
                                    jQuery('#r_yes').attr('checked',true);
                                    jQuery('.container_joint').removeClass('hide');
                                    jQuery('.container_single').addClass('hide'); 
                                    jQuery('[value="submitFormSingle"]').html('Next');
                                } else {
                                    // console.log('not joint');
                                    jQuery('#r_no').attr('checked',true);
                                    jQuery('.container_joint').addClass('hide');
                                    jQuery('.container_single').removeClass('hide'); 
                                }
                                
                            } else {
                                // console.log('not extra');

                                jQuery('.extraPricingChart').remove(); 
                                if (client_type == 'Joint') { 
                                    // console.log('joint');
                                    jQuery('#r_yes').attr('checked',true);
                                    jQuery('.container_joint').removeClass('hide');
                                    jQuery('.container_single').addClass('hide');
                                    jQuery('[value="submitFormSingle"]').html('Next');
                                } else {
                                    // console.log('not joint');
                                    jQuery('.container_joint').addClass('hide');
                                    jQuery('.container_single').removeClass('hide');
                                }
                                    
                            } 

                            var plan_buttons = jQuery('.plan_buttons');
                            jQuery.each(plan_buttons,function(k,button){
                                var plan = jQuery(button).attr('plan'); 
                                plan = plan.replace(/ Plus/g,'');
                                var plan_ = (value.plan).replace(/ Plus/g,''); 
                                // console.log(plan + ' = '+plan_);
                                if (plan != plan_ ) { 
                                    jQuery(button).closest('.removeBr').remove(); 
                                } else { 
                                    jQuery(button).closest('.removeBr').css('float','none');
                                    jQuery(button).closest('.removeBr').css('margin','auto');
                                }
                                
                                
                            });

                        });
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        }  else {
            // console.log('new lead');
        }
    
    // CONSTANTS
        jQuery(".date-inputmask").inputmask("mm/dd/yyyy");
        jQuery(".expiration-inputmask").inputmask("99/99");
        jQuery(".phone-inputmask").inputmask("(999) 999-9999");
        jQuery(".ss-inputmask").inputmask("999-99-9999");
        jQuery(".zip-inputmask").inputmask("99999"); 

        jQuery('.todays_date').html(moment().format('MM/DD/YYYY'));

        jQuery(".my-datepicker" ).datepicker({ dateFormat: "mm/dd/yy" }).val(); 
        jQuery('.material_date').bootstrapMaterialDatePicker({ weekStart: 0, time: false,format: 'MM/DD/YYYY' ,minDate : moment().startOf('day')});
        jQuery('.removeBr').find('br').remove();     
        jQuery('.removeP').find('p').remove();     
        jQuery('#legal_agreements').find('h4').closest('div').find('p').remove();  
        jQuery('polygon').remove(); 
        jQuery('#plan_selection').find('p').remove();
         

    // TABS

        jQuery('#tab-list').on('click','a',function(e){
            e.preventDefault();
            if (e.target !== this)
                return; 
            var href = jQuery(this).attr('href');
            if (href == '#plan_selection') {
                var profile_name = jQuery('[name="profile_name"]'); 
                var profile_email_address = jQuery('[name="profile_email_address"]'); 
                var profile_cell_phone = jQuery('[name="profile_cell_phone"]'); 
                var profile_carrier = jQuery('[name="profile_carrier"]'); 
                var profile_address = jQuery('[name="profile_address"]'); 
                var profile_state_province = jQuery('[name="profile_state_province"]'); 
                var profile_city = jQuery('[name="profile_city"]'); 
                var profile_zip_postal_code = jQuery('[name="profile_zip_postal_code"]'); 
                var profile_joint_name = jQuery('[name="profile_joint_name"]'); 
                var profile_joint_email_address = jQuery('[name="profile_joint_email_address"]'); 
                var profile_joint_cell_phone = jQuery('[name="profile_joint_cell_phone"]'); 
                var profile_joint_carrier = jQuery('[name="profile_joint_carrier"]'); 
                var client_type = jQuery('[name="client_type"]');
                if (profile_name.val() == '' || 
                    profile_email_address.val() == '' || 
                    profile_cell_phone.val() == '' || 
                    profile_address.val() == '' || 
                    profile_state_province.val() == '' || 
                    profile_city.val() == '' || 
                    profile_zip_postal_code.val() == '' || 
                    profile_carrier.val() == '') {
                    swal({
                        title: "Contact Information Is Missing!",
                        text: "click \"Ok\" button to continue",
                        icon: "warning",
                        button: "Ok!",
                    });
                }
                profile_name.css('border','1px solid #e6e6e6');
                profile_email_address.css('border','1px solid #e6e6e6');
                profile_cell_phone.css('border','1px solid #e6e6e6');
                profile_carrier.css('border','1px solid #e6e6e6');
                profile_address.css('border','1px solid #e6e6e6');
                profile_state_province.css('border','1px solid #e6e6e6');
                profile_city.css('border','1px solid #e6e6e6');
                profile_zip_postal_code.css('border','1px solid #e6e6e6');
                profile_joint_name.css('border','1px solid #e6e6e6');
                profile_joint_email_address.css('border','1px solid #e6e6e6');
                profile_joint_cell_phone.css('border','1px solid #e6e6e6');
                profile_joint_carrier.css('border','1px solid #e6e6e6');
                
                    if (profile_name.val() == '') {
                        profile_name.css('border','1px solid red');
                        profile_name.focus();
                        return false;
                    }
                    if (profile_email_address.val() == '' || (profile_email_address.val()).indexOf('@') === -1) {
                        profile_email_address.css('border','1px solid red');
                        profile_email_address.focus();
                        return false;
                    }
                    if (profile_cell_phone.val() == '') {
                        profile_cell_phone.css('border','1px solid red');
                        profile_cell_phone.focus();
                        return false;
                    }
                    if (profile_carrier.val() == '') {
                        profile_carrier.css('border','1px solid red');
                        profile_carrier.focus();
                        return false;
                    }
                    if (profile_address.val() == '') {
                        profile_address.css('border','1px solid red');
                        profile_address.focus();
                        return false;
                    }
                    if (profile_state_province.val() == '') {
                        profile_state_province.css('border','1px solid red');
                        profile_state_province.focus();
                        return false;
                    }
                    if (profile_city.val() == '') {
                        profile_city.css('border','1px solid red');
                        profile_city.focus();
                        return false;
                    }
                    if (profile_zip_postal_code.val() == '') {
                        profile_zip_postal_code.css('border','1px solid red');
                        profile_zip_postal_code.focus();
                        return false;
                    }

                if (!jQuery('#r_yes').is(':checked') && !jQuery('#r_no').is(':checked')) {
                    jQuery('[for="r_yes"]').css('color','red');
                    jQuery('[for="r_no"]').css('color','red');
                    return false;
                }

                if (jQuery('[name="client_type"][value="Joint"]').is(':checked')) {
                    if (profile_joint_name.val() == '' || 
                        profile_joint_email_address.val() == '' || 
                        profile_joint_cell_phone.val() == '' || 
                        profile_joint_carrier.val() == '') {
                        swal({
                            title: "Joint Contact Information Is Missing!",
                            text: "click \"Ok\" button to continue",
                            icon: "warning",
                            button: "Ok!",
                        });
                    }
                    if (profile_joint_name.val() == '') {
                        profile_joint_name.css('border','1px solid red');
                        profile_joint_name.focus();
                        return false;
                    }
                    if (profile_joint_email_address.val() == '' || (profile_joint_email_address.val()).indexOf('@') === -1) {
                        profile_joint_email_address.css('border','1px solid red');
                        profile_joint_email_address.focus();
                        return false;
                    }
                    if (profile_joint_cell_phone.val() == '') {
                        profile_joint_cell_phone.css('border','1px solid red');
                        profile_joint_cell_phone.focus();
                        return false;
                    }
                    if (profile_joint_carrier.val() == '') {
                        profile_joint_carrier.css('border','1px solid red');
                        profile_joint_carrier.focus();
                        return false;
                    }
                }

            }

            if (href == '#legal_agreements') {
                if (jQuery('.plan_selection_inputs').hasClass('hide')) {
                    var profile_name = jQuery('[name="profile_name"]').val();
                    var profile_email_address = jQuery('[name="profile_email_address"]').val();
                    var title = '';
                    if (profile_name == '' && profile_email_address == '') {
                        title = 'Contact Information Is Missing!';
                    } else {
                        title = 'Choose a Plan!';
                    }
                    swal({
                        title: title,
                        text: "click \"Ok\" button to continue",
                        icon: "warning",
                        button: "Ok!",
                    });
                    return false;
                }


                var billing_paid = jQuery('[name="billing_paid"]');
                var billing_monthly_due = jQuery('[name="billing_monthly_due"]');
                var billing_card_number = jQuery('[name="billing_card_number"]');
                var billing_card_expiration = jQuery('[name="billing_card_expiration"]');
                var billing_cvv_code = jQuery('[name="billing_cvv_code"]');
                var billing_card_holder = jQuery('[name="billing_card_holder"]');
                var billing_address = jQuery('[name="billing_address"]');
                var billing_state = jQuery('[name="billing_state"]');
                var billing_city = jQuery('[name="billing_city"]');
                var billing_zip = jQuery('[name="billing_zip"]');
                billing_paid.css('border','1px solid #e6e6e6');
                billing_monthly_due.css('border','1px solid #e6e6e6');
                billing_card_number.css('border','1px solid #e6e6e6');
                billing_card_expiration.css('border','1px solid #e6e6e6');
                billing_cvv_code.css('border','1px solid #e6e6e6');
                billing_card_holder.css('border','1px solid #e6e6e6');
                billing_address.css('border','1px solid #e6e6e6');
                billing_state.css('border','1px solid #e6e6e6');
                billing_city.css('border','1px solid #e6e6e6');
                billing_zip.css('border','1px solid #e6e6e6');
                if (billing_paid.val() == '') {
                    billing_paid.css('border','1px solid red');
                    billing_paid.focus();
                    return false;
                }
                if (billing_monthly_due.val() == '') {
                    billing_monthly_due.css('border','1px solid red');
                    billing_monthly_due.focus();
                    return false;
                }
                if (billing_card_number.val() == '') {
                    billing_card_number.css('border','1px solid red');
                    billing_card_number.focus();
                    return false;
                } else {
                    if ((billing_card_number.val()).length < 16) {
                        billing_card_number.css('border','1px solid red');
                        swal({
                            title: "Validation Alert!",
                            text: "Card Number must be at least 16 digits!",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_card_number.focus(); 
                        return false;
                    } 
                }

                var billing_card_expiration_ = (billing_card_expiration.val()).replace('_','');
                billing_card_expiration_ = billing_card_expiration_.replace('/','');
                if (billing_card_expiration.val() == '') {
                    billing_card_expiration.css('border','1px solid red');
                    billing_card_expiration.focus();
                    return false;
                } else {
                    if ((billing_card_expiration_).toString().length < 4) {
                        billing_card_expiration.css('border','1px solid red');
                        swal({
                            title: "Validation Alert!",
                            text: "Card Expiration must be at least 4 digits!",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_card_expiration.focus();
                        return false; 
                    } 
                }
                if (billing_cvv_code.val() == '') {
                    billing_cvv_code.css('border','1px solid red');
                    billing_cvv_code.focus();
                    return false;
                } else {
                    if ((billing_cvv_code.val()).toString().length < 3 || (billing_cvv_code.val()).toString().length > 4) {
                        billing_cvv_code.css('border','1px solid red');
                        swal({
                            title: "Oops, Try Again!",
                            text: "Security Code must be \n minimum of 3 and maximum of 4 !",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_cvv_code.focus();
                        return false; 
                    } 
                }

                
                
                

                if (jQuery('[name="same_billing_contact"][value="No"]').is(':checked')) {
                    if (billing_card_holder.val() == '') {
                        billing_card_holder.css('border','1px solid red');
                        billing_card_holder.focus();
                        return false;
                    }
                    if (billing_address.val() == '') {
                        billing_address.css('border','1px solid red');
                        billing_address.focus();
                        return false;
                    }
                    if (billing_state.val() == '') {
                        billing_state.css('border','1px solid red');
                        billing_state.focus();
                        return false;
                    }
                    if (billing_city.val() == '') {
                        billing_city.css('border','1px solid red');
                        billing_city.focus();
                        return false;
                    }
                    if (billing_zip.val() == '') {
                        billing_zip.css('border','1px solid red');
                        billing_zip.focus();
                        return false;
                    }
                }

                if (!jQuery('#s_yes').is(':checked') && !jQuery('#s_no').is(':checked')) {
                    jQuery('[for="s_yes"]').css('color','red');
                    jQuery('[for="s_no"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="s_yes"]').css('color','#999');
                    jQuery('[for="s_no"]').css('color','#999');
                } 
                
            }
            jQuery('#tab-list').find('.arrow_box').removeClass('arrow_box');
            jQuery('#tab-list').find('a').css('color','#00adad');
            jQuery(this).closest('div').addClass('arrow_box');
            jQuery(this).closest('div').find('a').css('color','white');
            jQuery(this).closest('div').find('a').css('text-decoration','none');
            
            jQuery('.tab-content').find('.tab-body').addClass('hide');
            jQuery(''+href).removeClass('hide'); 
            jQuery('html,body').animate({scrollTop:500},1000);
        });
         
        jQuery('.tab-body').on('click','.nextButton',function(e){
            e.preventDefault();
            var value = jQuery(this).attr('value'); 
            if (value == 'submitFormSingle') {

                var username = jQuery('[name="username"]');
                var password = jQuery('[name="password"]');
                var retype_password = jQuery('[name="retype_password"]'); 
                var profile_name = jQuery('[name="profile_name"]');
                var profile_signature1 = jQuery('[name="profile_signature1"]');
                var profile_ss = jQuery('[name="profile_ss"]');
                var profile_date_of_birth = jQuery('[name="profile_date_of_birth"]'); 
                if ((username.val()).length < 5) {
                    username.focus();
                    username.css('border','1px solid red');
                    return false;
                } else {
                    username.css('border','1px solid #e6e6e6');
                }
                if ((password.val()).length < 8) {
                    password.focus();
                    password.css('border','1px solid red');
                    return false;
                } else {
                    password.css('border','1px solid #e6e6e6');
                }
                if ((retype_password.val()).length < 8) {
                    retype_password.focus();
                    retype_password.css('border','1px solid red');
                    return false;
                } else {
                    retype_password.css('border','1px solid #e6e6e6');
                }

                if (!jQuery('#obtain_copy_yes').is(':checked')) {
                    jQuery('#obtain_copy_yes').focus();
                    jQuery('[for="obtain_copy_yes"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="obtain_copy_yes"]').css('color','#999');
                }


                var legal_agreements_checkboxes = jQuery('#legal_agreements').find('[type="checkbox"]'); 
                var proceed = true;
                jQuery.each(legal_agreements_checkboxes,function(key,checkbox){
                    if (!jQuery(checkbox).is(':checked')) {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','red');
                        proceed = false;
                    }  else {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','#3cc');
                    }
                });
                if (!proceed) {
                    return false;
                } 


                if ((profile_name.val()).length == 0) {
                    profile_name.focus();
                    profile_name.css('border','1px solid red');
                    return false;
                } else {
                    profile_name.css('border','1px solid #e6e6e6');
                }

                if ((profile_signature1.val()).length == 0) {
                    profile_signature1.focus();
                    profile_signature1.css('border','1px solid red');
                    return false;
                } else {
                    profile_signature1.css('border','1px solid #e6e6e6');
                }
                if ((profile_ss.val()).length == 0) {
                    profile_ss.focus();
                    profile_ss.css('border','1px solid red');
                    return false;
                } else {
                    profile_ss.css('border','1px solid #e6e6e6');
                }
                if ((profile_date_of_birth.val()).length == 0) {
                    profile_date_of_birth.focus();
                    profile_date_of_birth.css('border','1px solid red');
                    return false;
                } else {
                    profile_date_of_birth.css('border','1px solid #e6e6e6');
                }
                
                var joint_ = jQuery('#r_yes').is(':checked'); 
                if (joint_) { 
                    jQuery('[value="submitFormSingle"]').html('Next');
                    jQuery('#containerCredentialsJoint').removeClass('hide');
                    jQuery('#containerCredentialsSingle').addClass('hide');
                    jQuery('#containerElectricSignatureJoint').removeClass('hide');
                    jQuery('#containerElectricSignatureSingle').addClass('hide');
                    
                    jQuery('html,body').animate({scrollTop:500},1000);
                    jQuery('.LAcheckboxes').attr('checked',false);
                    jQuery('#inputSelectAllHidden').attr('checked',false);
                    jQuery('#obtain_copy_yes').attr('checked',false);


                } else {  
                    var form = jQuery('.wpcf7-form'); 
                    form.submit(); 
                }
                 



                
            } else if (value == 'submitFormJoint') {
                var joint_username = jQuery('[name="joint_username"]');
                var joint_password = jQuery('[name="joint_password"]');
                var joint_retype_password = jQuery('[name="joint_retype_password"]');
                var profile_joint_name = jQuery('[name="profile_joint_name"]');
                var profile_signature2 = jQuery('[name="profile_signature2"]');
                var profile_joint_ss = jQuery('[name="profile_joint_ss"]');
                var profile_joint_date_of_birth = jQuery('[name="profile_joint_date_of_birth"]');

                if ((joint_username.val()).length < 5) {
                    joint_username.focus();
                    joint_username.css('border','1px solid red');
                    return false;
                } else {
                    joint_username.css('border','1px solid #e6e6e6');
                }
                if ((joint_password.val()).length < 8) {
                    joint_password.focus();
                    joint_password.css('border','1px solid red');
                    return false;
                } else {
                    joint_password.css('border','1px solid #e6e6e6');
                }
                if ((joint_retype_password.val()).length < 8) {
                    joint_retype_password.focus();
                    joint_retype_password.css('border','1px solid red');
                    return false;
                } else {
                    joint_retype_password.css('border','1px solid #e6e6e6');
                }

                if (!jQuery('#obtain_copy_yes').is(':checked')) {
                    jQuery('#obtain_copy_yes').focus();
                    jQuery('[for="obtain_copy_yes"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="obtain_copy_yes"]').css('color','#999');
                }


                var legal_agreements_checkboxes = jQuery('#legal_agreements').find('[type="checkbox"]'); 
                var proceed = true;
                jQuery.each(legal_agreements_checkboxes,function(key,checkbox){
                    if (!jQuery(checkbox).is(':checked')) {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','red');
                        proceed = false;
                    }  else {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','#3cc');
                    }
                });
                if (!proceed) {
                    return false;
                } 
                if ((profile_joint_name.val()).length == 0) {
                    profile_joint_name.focus();
                    profile_joint_name.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_name.css('border','1px solid #e6e6e6');
                }
                if ((profile_signature2.val()).length == 0) {
                    profile_signature2.focus();
                    profile_signature2.css('border','1px solid red');
                    return false;
                } else {
                    profile_signature2.css('border','1px solid #e6e6e6');
                }
                if ((profile_joint_ss.val()).length == 0) {
                    profile_joint_ss.focus();
                    profile_joint_ss.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_ss.css('border','1px solid #e6e6e6');
                }
                if ((profile_joint_date_of_birth.val()).length == 0) {
                    profile_joint_date_of_birth.focus();
                    profile_joint_date_of_birth.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_date_of_birth.css('border','1px solid #e6e6e6');
                }
                var form = jQuery('.wpcf7-form'); 
                form.submit(); 
            } else {
                jQuery('a[href="'+value+'"]').trigger('click');
            }
            
        });

        jQuery('.plan_buttons').on('click',function(){
            var plan = jQuery(this).attr('plan');
            var setup_fee = jQuery(this).attr('setup_fee');
            var monthly_fee = jQuery(this).attr('monthly_fee');
            jQuery('.plan_selection_inputs').removeClass('hide');
            jQuery('.billing_plan').val(plan);
            jQuery('.billing_setup_fee').html(setup_fee);
            jQuery('.billing_monthly_fee').html(monthly_fee);
            jQuery('.billing_setup_fee').val(setup_fee);
            jQuery('.billing_monthly_fee').val(monthly_fee); 

            jQuery('.SAIframes').addClass('hide');
            if (plan == 'Essentials') {
                jQuery('.Essentials').removeClass('hide');
            } else if (plan == 'Essentials Plus') {
                jQuery('.Essentials_Plus').removeClass('hide');
            } else if (plan == 'Premiere') {
                jQuery('.Premiere').removeClass('hide');
            } else if (plan == 'Premiere Plus') {
                jQuery('.Premiere_Plus').removeClass('hide');
            } else if (plan == 'Exclusive') {
                jQuery('.Exclusive').removeClass('hide');
            } else if (plan == 'Exclusive Plus') {
                jQuery('.Exclusive_Plus').removeClass('hide');
            }
            jQuery('[name="coupon_code"]').focus();

        });
    

    // Contact Information
        jQuery('[name="client_type"]').on('change',function(){
            jQuery('[for="r_yes"]').css('color','#999');
            jQuery('[for="r_no"]').css('color','#999');
            jQuery('.plan_selection_inputs').addClass('hide');
            jQuery('[name="coupon_code"]').prop('disabled',false);
            jQuery('[name="coupon_code"]').val('');
            jQuery('.billing_setup_fee_coupon').addClass('hide');
            jQuery('.billing_setup_fee').css('text-decoration','none');
            jQuery('#coupon_sub').prop('disabled',false);
            var value = jQuery(this).val();
            if (value == 'Joint') {
                jQuery('.container_joint').removeClass('hide'); 
                jQuery('.container_joint_inputs').removeClass('hide'); 
                jQuery('.container_single').addClass('hide'); 
            } else {
                jQuery('.container_joint').addClass('hide');
                jQuery('.container_joint_inputs').addClass('hide');
                jQuery('.container_single').removeClass('hide');  
            }
        });

        jQuery('[name="profile_email_address"],[name="profile_joint_email_address"]').on('focusout', function(event) {
            event.preventDefault();
            /* Act on the event */
            var email_address = jQuery(this).val();
            if (!isValidEmailAddress(email_address)) {
                jQuery(this).val(''); 
                swal('Error','Invalid Email Address!','error');
            }
            function isValidEmailAddress(emailAddress) {
                var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                return pattern.test(emailAddress);
            };
        });

    

    // Plan Selection
        jQuery('[name="same_billing_contact"]').on('change',function(){
            jQuery('[for="s_yes"]').css('color','#999');
            jQuery('[for="s_no"]').css('color','#999');
            var value = jQuery(this).val();
            if (value == 'No') {
                jQuery('.container_billing_info').removeClass('hide');  
                jQuery('#billing_info_container').html('');
                jQuery('[name="billing_card_holder"]').val('');
                jQuery('[name="billing_address"]').val('');
                jQuery('[name="billing_city"]').val('');
                jQuery('[name="billing_state"]').val('');
                jQuery('[name="billing_zip"]').val('');

            } else {
                jQuery('.container_billing_info').addClass('hide');  
                var profile_name = jQuery('input[name="profile_name"]').val();
                var profile_address = jQuery('input[name="profile_address"]').val();
                var profile_city = jQuery('input[name="profile_city"]').val();
                var profile_state_province = jQuery('select[name="profile_state_province"]').val();
                var profile_zip_postal_code = jQuery('input[name="profile_zip_postal_code"]').val();

                var billing_info_container = profile_name + ', ' + profile_address + ', ' + profile_city + ', ' + profile_state_province + ', ' +profile_zip_postal_code;
                jQuery('#billing_info_container').html(billing_info_container);
                jQuery('[name="billing_card_holder"]').val(profile_name);
                jQuery('[name="billing_address"]').val(profile_address);
                jQuery('[name="billing_city"]').val(profile_city);
                jQuery('[name="billing_state"]').val(profile_state_province);
                jQuery('[name="billing_zip"]').val(profile_zip_postal_code);

            }
        });

        jQuery('input[name="billing_paid"]').on('change',function(){
            var value = jQuery(this).val();
            var add_1 = moment(value).add(1,'months').format('MM/DD/YYYY');
            jQuery('input[name="billing_monthly_due"]').val(add_1);
        });

        jQuery('input[name="profile_name"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_card_holder"]').val(value);
            jQuery('.profile_name').html(value);;
            jQuery('.profile_name').val(value);;

        });

        jQuery('input[name="profile_joint_name"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            var profile_name = jQuery('input[name="profile_name"]');
            if (value == profile_name.val()) {
                jQuery(this).val('');
                jQuery('.profile_joint_name').html('');;
                jQuery('.profile_joint_name').val('');;

                swal('Error','Please use different name','error');
            } else {
                jQuery('.profile_joint_name').html(value);;
                jQuery('.profile_joint_name').val(value);;
            }
            

        });
        jQuery('input[name="profile_email_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('.profile_email_address').val(value);;

        });
        jQuery('input[name="profile_joint_email_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            var profile_email_address = jQuery('input[name="profile_email_address"]');
            if (value == profile_email_address.val()) {
                jQuery('.profile_joint_email_address').val('');;
                jQuery(this).val('');
                swal('Error','Please use different email address','error');
            } else {
                jQuery('.profile_joint_email_address').val(value);;   
            }
        });

        jQuery('input[name="profile_joint_ss"]').on('keyup', function(event) {
            var value = jQuery(this).val();
            var profile_ss = jQuery('input[name="profile_ss"]');
            if (value == profile_ss.val()) {
                jQuery(this).val('');
                swal('Error','Please use different ss','error');
            }
        });

        jQuery('input[name="profile_joint_date_of_birth"]').on('keyup', function(event) {
            var value = jQuery(this).val();
            var profile_date_of_birth = jQuery('input[name="profile_date_of_birth"]');
            if (value == profile_date_of_birth.val()) {
                jQuery(this).val('');
                swal('Error','Please use different Date of Birth','error');
            }
        });
        jQuery('input[name="profile_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_address"]').val(value);
        });
        jQuery('input[name="profile_city"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_city"]').val(value);
        });
        jQuery('select[name="profile_state_province"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_state"]').val(value);
        });
        jQuery('input[name="profile_zip_postal_code"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_zip"]').val(value);
        });


    // LEGAL AGREEMENTS
        jQuery('button[value="#plan_selection"]').on('click',function(){ 
            var profile_name = jQuery('input[name="profile_name"]').val();
            var profile_address = jQuery('input[name="profile_address"]').val();
            var profile_city = jQuery('input[name="profile_city"]').val();
            var profile_state_province = jQuery('select[name="profile_state_province"]').val();
            var profile_zip_postal_code = jQuery('input[name="profile_zip_postal_code"]').val();

            var billing_info_container = profile_name + ', ' + profile_address + ', ' + profile_city + ', ' + profile_state_province + ', ' +profile_zip_postal_code;
            jQuery('#billing_info_container').html(billing_info_container);
        });
         

        jQuery('#legal_agreements').on('click','.LA_toggle',function(e){ 
            if (e.target !== this)
                return; 
            var target = jQuery(this).attr('data-target'); 
            var collapse = jQuery(target); 
            collapse.slideToggle('slow');

            var i = jQuery(this).closest('.LA_parent').find('.toggle-icon'); 
            if (i.hasClass('fa-angle-double-right')) {
                i.removeClass('fa-angle-double-right');
                i.addClass('fa-angle-double-down');
            } else {
                i.removeClass('fa-angle-double-down');
                i.addClass('fa-angle-double-right');
            }
        });


        jQuery('#inputSelectAll').on('click',function(){
            var inputSelectAllHidden = jQuery('#inputSelectAllHidden'); 
            console.log(inputSelectAllHidden.is(':checked'));
            if (inputSelectAllHidden.is(':checked')) {
                // inputSelectAllHidden.attr('checked',false);
                jQuery('.LAcheckboxes').attr('checked',false); 
            } else {
                // inputSelectAllHidden.attr('checked',true);
                jQuery('.LAcheckboxes').attr('checked',true);
                
            }
        });

    // BILLING info
        jQuery('[name="billing_card_number"]').on('focusout',function(){
            var val = jQuery(this).val();
            if (val.length < 16) {
                swal({
                    title: "Validation Alert!",
                    text: "Card Number must be at least 16 digits!",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
        jQuery('[name="billing_card_expiration"]').on('focusout',function(){
            var val = jQuery(this).val();
            val = val.replace('_','');
            val = val.replace('/','');  
            if (val.toString().length < 4) {
                swal({
                    title: "Validation Alert!",
                    text: "Card Expiration must be at least 4 digits!",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
        jQuery('[name="billing_cvv_code"]').on('focusout',function(){
            var val = jQuery(this).val(); 
            if (val.toString().length < 3 || val.toString().length > 4 ) {
                swal({
                    title: "Oops, Try Again!",
                    text: "Security Code must be \n minimum of 3 and maximum of 4 !",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
     
    // CREDENTIALS
        jQuery('[name="username"]').on('keyup',function(){
            var username = jQuery(this).val();
            if (username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                // var withNumber = (username.match(/[0-9]/g) || []).length;
                if (username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="username"]').on('focusout',function(){
            var username = jQuery(this).val();
            if (username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
                jQuery(this).focus();
            } else {
                // var withNumber = (username.match(/[0-9]/g) || []).length;
                if (username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                    validateUsername(username);
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
            
            function validateUsername(username) {
                jQuery.post('../wp-custom-functions/validate_client_username.php',{username}, function(data){
                        // data = JSON.parse(data);  
                        if (parseInt(data) == 0) {
                            jQuery('[name="username"]').css('border','1px solid #e6e6e6'); 
                            jQuery('#errorUsername').addClass('hide');
                            // alert('Username already exist!');
                        } else {
                            jQuery('[name="username"]').focus();
                            jQuery('[name="username"]').css('border','1px solid red'); 
                            jQuery('#errorUsername').removeClass('hide');
                            // alert('Username already exist!');
                        }
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        });

        jQuery('[name="password"]').on('keyup',function(){
            var password = jQuery(this).val();
            if (password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (password.match(/[0-9]/g) || []).length;
                var withAZ = (password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="password"]').on('focusout',function(){
            var password = jQuery(this).val();
            if (password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (password.match(/[0-9]/g) || []).length;
                var withAZ = (password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
        });

        jQuery('[name="retype_password"]').on('keyup',function(){
            var retype_password = jQuery(this).val();
            var password = jQuery('[name="password"]').val(); 
            if (password != retype_password) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="retype_password"]').on('focusout',function(){
            var retype_password = jQuery(this).val();
            var password = jQuery('[name="password"]').val();
            if (password != retype_password) {
                // jQuery(this).focus(); 
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="joint_username"]').on('keyup',function(){
            var joint_username = jQuery(this).val();
            if (joint_username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                // var withNumber = (joint_username.match(/[0-9]/g) || []).length;
                if (joint_username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="joint_username"]').on('focusout',function(){
            var joint_username = jQuery(this).val();
            if (joint_username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
                jQuery(this).focus();
            } else {
                // var withNumber = (joint_username.match(/[0-9]/g) || []).length;
                if (joint_username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                    validateUsername(joint_username,'joint');
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
            
            function validateUsername(username,type) {
                jQuery.post('../wp-custom-functions/validate_client_username.php',{username,type}, function(data){
                        // data = JSON.parse(data);  
                        console.log(data);
                        if (parseInt(data) == 0) {
                            jQuery('[name="joint_username"]').css('border','1px solid #e6e6e6'); 
                            jQuery('#errorJointUsername').addClass('hide');
                            // alert('Username already exist!');
                        } else {
                            jQuery('[name="joint_username"]').focus();
                            jQuery('[name="joint_username"]').css('border','1px solid red'); 
                            jQuery('#errorJointUsername').removeClass('hide');
                            // alert('Username already exist!');
                        }
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        });

        jQuery('[name="joint_password"]').on('keyup',function(){
            var joint_password = jQuery(this).val();
            if (joint_password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (joint_password.match(/[0-9]/g) || []).length;
                var withAZ = (joint_password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="joint_password"]').on('focusout',function(){
            var joint_password = jQuery(this).val();
            if (joint_password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (joint_password.match(/[0-9]/g) || []).length;
                var withAZ = (joint_password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
        });

        jQuery('[name="joint_retype_password"]').on('keyup',function(){
            var joint_retype_password = jQuery(this).val();
            var joint_password = jQuery('[name="joint_password"]').val(); 
            if (joint_password != joint_retype_password) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="joint_retype_password"]').on('focusout',function(){
            var joint_retype_password = jQuery(this).val();
            var joint_password = jQuery('[name="joint_password"]').val();
            if (joint_password != joint_retype_password) {
                // jQuery(this).focus(); 
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

    // COUPON
        jQuery('[name="copy_of_cr"]').on('change',function(){
            var value = jQuery(this).val();
            if (value == 'Yes') {
                jQuery('.containerUploadCRBtn').removeClass('hide');
            } else {
                jQuery('.containerUploadCRBtn').addClass('hide');
            }
        });

        jQuery('[name="coupon_code"]').keypress(function(event) {
            var keycode = event.keyCode || event.which;
            if(keycode == '13') {
                jQuery('#coupon_sub').trigger('click'); 
                event.preventDefault();
            }
        });
        jQuery('#coupon_sub').on('click',function(){
            var coupon_code = jQuery('[name="coupon_code"]').val();
            // alert(coupon_code);
            if (coupon_code != '') {
                validateCoupon(coupon_code);
            }  
            
            function validateCoupon(coupon_code) {
                    jQuery.post('../wp-custom-functions/validate_coupon.php',{coupon_code}, function(data){
                            data = JSON.parse(data);  
                            // console.log(data);
                            if (data['status'] == 'success') { 
                                jQuery('input[name="billing_coupon_code"]').val(coupon_code);
                                jQuery('.errorValidateCoupon').addClass('hide'); 
                                var setup_fee = parseInt(jQuery('.billing_setup_fee_display').html()) - data['message']; 

                                jQuery('.billing_setup_fee').css('text-decoration','line-through');
                                jQuery('.billing_setup_fee_coupon').removeClass('hide');
                                jQuery('.billing_setup_fee_coupon').html('$'+setup_fee);
                                // alert(setup_fee);
                                jQuery('.billing_setup_fee').val(setup_fee); 
                                jQuery('[name="coupon_code"]').prop('disabled',true);
                                jQuery('[name="coupon_code"]').val('Coupon Applied');
                                jQuery('#coupon_sub').prop('disabled',true);
                            } else { 
                                jQuery('[name="coupon_code"]').val('');
                                jQuery('.errorValidateCoupon').removeClass('hide');
                                jQuery('.errorValidateCoupon').html(data['message']);
                            }
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                        });
                    }
        });

</script>