
    <input type="hidden" name="profile_client_id">
    <input type="hidden" name="billing_coupon_code">
    <div style="margin-top: 30px" class="row" id="nav-titles-list">
        <div class="col-sm-4 nav-titles nav-title-active nav-title-first text-center" >
            <a class="nav-title">Contact Information</a>
        </div>
        <div class="col-sm-4 nav-titles  text-center" >
            <a class="nav-title">Plan Selection</a>
        </div>
        <div class="col-sm-4 nav-titles last_box text-center" >
            <a class="nav-title">Legal Agreements</a>
        </div>
    </div> 
    <hr>
    <div class="row">
        <div class="tab-content"> 
            <div style="padding-left: 15px;padding-right: 15px" class="tab-body removeBr" id="contact_info"> 
            </div> 

            <div class="tab-body hide" id="plan_selection"> 
            </div>

            <div class="tab-body hide removeBr" id="legal_agreements"> 

            </div> 
        </div>   
    </div> 