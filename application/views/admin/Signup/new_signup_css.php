<meta name="google-site-verification" content="jZV-lx5eQatzkdfndRwHeg2YAN8qIj72-8xLVpL3_40" />
<meta name="google-site-verification" content="R38svinALyPuWs1kFY8UTIcvqLbFdQkM-1yFKvbneYs" />
<script>
  var qsKey = "qs4b840f714123393766f6==";
  setTimeout(function(e,t,n,c,r){
  c=e.createElement(t),c.async=1,
  c.src=n,r=e.getElementsByTagName(t)[0],
  r.parentNode.insertBefore(c, r)},1,document,
  "script","https://cdn.quicksprout.com/qs.js?"+qsKey);
</script> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://www.creditlynx.com/wp_custom_assets/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"> 
<style>   
    .text-right {
        text-align: right;;
    }
    .tableCharts tbody tr td:nth-child(n+2) {
        text-align: center; 
        border-top: 0px;
        border-right: 1px solid #ddd;
        border-left: 1px solid #ddd;
    }
    .tableCharts tbody tr td:nth-child(1) {
        border-left: 1px solid #ddd; 
        color: #3cc;
    }

    .tableCharts thead tr th:nth-child(n+2), .tableCharts tfoot th:nth-child(n+2) {
        text-align: center !important;  
        border: 1px solid #ddd !important;
        width: 150px !important;
        font-weight: 400;
    }


    .tableCharts tbody tr .active { 
        /*border-right: 1px solid #3cc !important;*/
        /*border-left: 1px solid #3cc !important;*/
    }

    .tableCharts thead tr .active {
        background-color: #3cc !important;  
        color: #fff;
    }

    .tableCharts tfoot tr .active {
        background-color: #3cc !important;  
        color: #fff;
    }


    .b-t {
        border-top: 1px solid #ddd;
    }
    .b-l {
        border-left: 1px solid #ddd;
    }
    .b-r {
        border-right: 1px solid #ddd;
    }
    .b-b {
        border-bottom: 1px solid #ddd;
    } 
    .b-t-0 {
        border-top: none;
    }
    .b-l-0 {
        border-left: none;
    }
    .b-r-0 {
        border-right: none;
    }
    .b-b-0 {
        border-bottom: none;
    }
    .b-0 {
        border: none !important;
    }
    .b-a {
        border: 1px solid #ddd !important;
    }
    .rightLabelDiv {
        position: relative;
    }

    .rightLabel {
        position: absolute;
        top: 8px;
        right: 30px;
        font-size: 8px;
    }
    .nextButton {
        border-radius: 25px;background-color: #33cccc;color:white;padding: 10px 120px 10px 120px;border: none;margin-top: 10px; 
    }
    .chartMostPopular {
        font-size: 32px;margin-top: 20px;text-align: center;margin-bottom: 50px;
    }
    .chartPriceSpan {
        font-size: .44em;opacity: .85;letter-spacing: 0px;color: rgb(255, 255, 255);position: relative;bottom: 4px;
    }
    .chartPrice {
        font-size: 32px;font-family: 'Open Sans', sans-serif;font-weight: 300;padding: 13px 0;margin-bottom: 0px;text-align: center;background-color: #33cccc;margin: 0 -15px;color: white;font-size: 34px;letter-spacing: -2px;
    }
    .SAIframes {
        width: 468pt !important;
    }
    .LA_iframeContainer {
        padding: 20px;text-align: center; 
    }
    .LA_toggleIframe {
        position: relative; height: 500px; width: 650pt;;margin: auto;
    }
    .LA_parent {
        cursor: pointer;padding: 15px;background-color: #fbfbfb;margin-bottom: 10px;
    }
    .LA_toggleSpan{
        position: absolute;font-size: 15px;top: 3.6px; 
    }
    .LA_toggleh4 {
        position: relative;margin: 0px;
    }
    #allCheckboxLabel {
        cursor: pointer;padding-right: 15px;margin-bottom: 15px;
    }
    .container_single,.container_joint,.container_single_extra,.container_joint_extra {
        margin: 12px 12px 12px 12px; background: #fff; border: 1px solid #d4d4d4; 
    }
    .chartContainers {
        border: 1px solid #d4d4d4;background: #f2f2f2; 
    }
    .tab-body {
        padding-left: 15px;padding-right: 15px;
    }
    .m-t-30 {
        margin-top: 30px;
    }
    .m-t-20 {
        margin-top: 20px;
    }
    .m-t-10 {
        margin-top: 10px;
    }
    .navTitles {
        margin-bottom: 0px;margin-top:0px;line-height: 47.9px;
    }
    .text-center {
        text-align: center; 
    }
    .chartTitle {
        font-size: 32px;margin-top: 20px;text-align: center;margin-bottom: 20px;
    }
    .swal-icon--warning {
        border: 5px solid red!important;
        display: flex!important;
        -webkit-animation: none!important; 
        animation: none!important; 
    }
    .swal-icon--warning__body, .swal-icon--warning__dot {
        position: absolute!important;
        left: 50%!important;
        background-color: #ff0000!important;
    }
    .swal-text {
        text-align: center;
    }

    .swal-button {
        background: #33cccc;
    }

    .swal-button:not([disabled]):hover {
        background: #33cccc;
        color: white;
        border: none;
    }
    
    .arrow_box {
        position: relative;
        background: #33cccc; 
        
    }
    .arrow_box:after {
        left: 100%;
        top: 63%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(38, 198, 218, 0);
        border-left-color: #33cccc;
        border-width: 23.9px;
        margin-top: -30px;
    }
    .logPass1{
            /*color: white !important;*/
            /*display: none!important;*/
            margin-left: 5px;
    }
    .logPass2{
        /*color: white !important;*/
        /*display: none!important;*/
        margin-left: 5px!;
    }
    @media screen and (max-width: 767px) {
        .logPass1{
            
            margin-left: 0!important;
        }
        .logPass2{
           
            margin-left: none!important;
            margin-top: 9px!important;
        }
    }
    @media screen and (max-width: 1073px) {
        .checkbox_label, .SelectAllLA {
            /*color: white !important;*/
            display: none!important;
        }
    }
    @media screen and (max-width: 425px) {
        .checkbox_label, .SelectAllLA {
            /*color: white !important;*/
            margin-bottom: 37px;
        }
    }

    .last_box:after {
        border-left-color: white !important;
    }

    input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #33cccc;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
 
    .label__checkbox {
      display: none;
    }

    .label__check {
      display: inline-block;
      border-radius: 50%;
      border: 5px solid rgba(0,0,0,0.1);
      background: white;
      vertical-align: middle;
      margin-right: 20px;
      width: 2.5em;
      height: 2.5em;
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: border .3s ease;
      
      i.icon {
        opacity: 0.2;
        font-size: ~'calc(1rem + 1vw)';
        color: transparent;
        transition: opacity .3s .1s ease;
        -webkit-text-stroke: 3px rgba(0,0,0,.5);
      }
      
      &:hover {
        border: 5px solid rgba(0,0,0,0.2);
      }
    }

    .label__checkbox:checked + .label__text .label__check {
      animation: check .5s cubic-bezier(0.895, 0.030, 0.685, 0.220) forwards;
      
      .icon {
        opacity: 1;
        transform: scale(0);
        color: white;
        -webkit-text-stroke: 0;
        animation: icon .3s cubic-bezier(1.000, 0.008, 0.565, 1.650) .1s 1 forwards;
      }
    }

    .my_center {
      position: absolute;
      top: 55%; right: 0;
      transform: translate(50%,-50%);
    }

    @keyframes icon {
      from {
        opacity: 0;
        transform: scale(0.3);
      }
      to {
        opacity: 1;
        transform: scale(1)
      }
    }

    @keyframes check {
      0% {
        width: 1.5em;
        height: 1.5em;
        border-width: 5px;
      }
      10% {
        width: 1.5em;
        height: 1.5em;
        opacity: 0.1;
        background: rgba(0,0,0,0.2);
        border-width: 15px;
      }
      12% {
        width: 1.5em;
        height: 1.5em;
        opacity: 0.4;
        background: rgba(0,0,0,0.1);
        border-width: 0;
      }
      50% {
        width: 2.5em;
        height: 2.5em;
        background: #33cccc;
        border: 0;
        opacity: 0.6;
      }
      100% {
        width: 2.5em;
        height: 2.5em;
        background: #33cccc;
        border: 0;
        opacity: 1;
      }
    }


    .form-control {
        color: #777777 !important;
        font-family: Maven Pro, FontAwesome;
    }

    #sticky_signature { position: absolute; display: block; right: 25px; top: 7.4px; z-index: 9; }

    .dtp .center {
        /*display: none;*/
    }


    .dtp div.dtp-date, .dtp div.dtp-time {
        background: #33cccc;
    }

    .dtp table.dtp-picker-days tr>td>a.selected {
        background: #33cccc;
    }

    .plan_text_slim { font-weight: 300;font-size: 16px;padding: 13px 4px;margin-bottom: 0px;margin-top:0px;color: #999;border-bottom: 1px solid rgba(0, 0, 0, 0.15);}
    .plan_text_bold { font-weight: 500;font-size: 16px;padding: 13px 4px;margin-bottom: 0px;margin-top:0px;color: #999;border-bottom: 1px solid rgba(0, 0, 0, 0.15);}
    .plan_buttons { width: 100%;border-radius: 0px;color: #fff;font-size: 16px;line-height: 18px;padding: 12px 24px;-webkit-border-radius: 0px;background: #3cc;border: 1px solid #27c0c0; }

    .plan_buttons:hover,.plan_buttons:focus,.nextButton:hover {
        border: 1px solid #00a1a1;
        background: #00adad;
        color: #fff;
    }

    .plan_buttons i {margin-left: 8px;margin-right: 0px;font-size: 1.3em}

    .coupon_apply { width: 100%;border-radius: 0px;color: #fff;font-size: 16px;line-height: 18px;padding: 12px 24px;-webkit-border-radius: 0px;background: #3cc;border: 1px solid #27c0c0; }

    .coupon_apply:hover,.coupon_apply:focus {
        border: 1px solid #00a1a1;
        background: #00adad;
        color: #fff;
    }

    .coupon_apply i {margin-left: 8px;margin-right: 0px;font-size: 1.3em}


    .promo:before {
        content: "";
        position: absolute;
        display: block;
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 7px 7px 0;
        border-color: transparent #777777 transparent transparent;
        bottom: -7px;
        left: 0;
    }
    .promo:after {
        content: "";
        position: absolute;
        display: block;
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 7px 7px 0 0;
        border-color: #777777 transparent transparent transparent;
        bottom: -7px;
        right: 0;
    }
    .promo {
        font-size: 12px;
        color: #fff;
        position: absolute;
        top: 85px;
        left: 21px;
        z-index: 1000;
        width: 88.5%;
        margin: 0;
        padding: .625em 17px .75em;
        background: #777777;
        box-shadow: 0 2px 4px rgba(0,0,0,.25);
        border-bottom: 1px solid #777777;
    }   
    @media (max-width: 991px) {
        .leftPC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .middlePC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .rightPC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .leftPC > .chartContainers { width: auto; }
    }

    @media (min-width: 992px) {
        .leftPC{ width:  31.61% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .middlePC{ width:  34.15% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .rightPC{ width:  34.24% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .leftPC > .chartContainers { width: 307px; }
    }

    @media (max-width: 728px) { 
        #allCheckboxLabel { 
            margin-bottom: 40px !important; 
        }
        .arrow_box:after {
            left: 0%;
            top: 63%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-color: rgba(38, 198, 218, 0);
            border-left-color: #33cccc;
            border-width: 23.9px;
            margin-top: -30px;
        }
    }
</style>
<style>
    .nav-titles {
        border: 1px solid #ddd;
        border-left: none;
    }
    .nav-title-active {
        position: relative;
        background: #33cccc;  
        border: 1px solid #3cc;
    } 
    .nav-title-active:after {
        left: 100%;
        top: 61%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(38, 198, 218, 0);
        border-left-color: #33cccc;
        border-width: 24.9px;
        margin-top: -30px;
    }  
    .nav-titles:after {
        left: 100%;
        top: 61%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(38, 198, 218, 0);
        border-left-color: #33cccc;
        border-width: 24.9px;
        margin-top: -30px;
    }  
    .nav-title-last:after { 
        border-top-left-radius: 25px;
        border-bottom-left-radius: 25px;
    }
    .nav-title-first {
        border-top-left-radius: 25px;
        border-bottom-left-radius: 25px;
    } 
    .nav-title {
        margin-bottom: 0px;margin-top:0px;line-height: 47.9px;
    }
    @media (max-width: 728px) {  
        .nav-title-active:after {
            left: 0%;
            top: 63%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-color: rgba(38, 198, 218, 0);
            border-left-color: #33cccc;
            border-width: 23.9px;
            margin-top: -30px;
        }
    }
</style>