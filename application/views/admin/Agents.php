<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css') ?>">
 <script src="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.js') ?>"></script> 
 <style type="text/css">
     .ui-autocomplete {
        z-index: 3000;
     }
 </style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?>
                            <a id="btnSearchArrowLeft" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-left"></i></a>
                            <a id="btnSearchArrowRight" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-right"></i></a>
                        </h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 text-right">
                         
                        <input type="text" name="" class="form-control" placeholder="search agent" id="searchInputAgent" style="width: 70%">
                        <script>
                            
                            getSearchAgent();
                            function getSearchAgent() {
                                var agents = [];
                                var users = '<?php echo json_encode($users) ?>'; 
                                users = JSON.parse(users);
                                $.each(users,function(key,value){
                                    var agent_name = value.first_name + ' ' +value.last_name; 

                                    var newOption = {
                                                    value: value.agent_id,
                                                    label: agent_name,
                                                    desc: value.company+' '+value.cell+ ' ' + value.email_address, 
                                                  };
                                    agents.push(newOption); 
                                }); 
 
  

                                $("#searchInputAgent" ).autocomplete({
                                  minLength: 2,
                                  source: agents,
                                  focus: function( event, ui ) {
                                    $( "#searchInputAgent" ).val( ui.item.label );
                                    return false;
                                  },
                                  select: function( event, ui ) {
                                    // $( "#searchInputAgent" ).val( ui.item.label );  
                                    $('#searchAgent').val(ui.item.value);
                                    $('#searchAgent').trigger('change');
                                    if ($('#profileSection').hasClass('hide')) { 
                                        $('#profileSection').removeClass('hide');
                                        $('#listSection').addClass('hide');
                                        $('#profile_active').removeClass('hide');
                                        $('#profile_active').html(ui.item.label);
                                    }




                                    // $( "#project-id" ).val( ui.item.value );
                                    // $( "#project-description" ).html( ui.item.desc );
                                    // $( "#project-icon" ).attr( "src", "images/" + ui.item.icon );
                             
                                    return false;
                                  }
                                })
                                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                                  return $( "<li>" )
                                    .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
                                    .appendTo( ul );
                                };
                            }
                            
                         
                                
                        </script>
                        <select  id="searchAgent" class="hide"> 
                            <option value="">select agent</option>s
                            <?php foreach ($users as $key => $value): ?>

                                <option value="<?php echo $value['agent_id']?>"><?php echo $value['first_name'].' '.$value['last_name'] ?></option> 
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="listSection" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h5 class="card-title"><?php echo $page_title ?> (<?php echo $user_count ?>)
                                         
                                    </h5> 
                                    
                                    <div class="table-responsive" >
                                        <a style="margin-top: 14px" href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#add-edit-modal" >New <?php echo $page_title_s ?> </a>
                                        <table id="pageTableAgents" class="table stylish-table">
                                            <thead>
                                                <tr> 
                                                    <th style="width: 1%"></th>
                                                    <th>Name</th>
                                                    <th>Company</th> 
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Tools</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($users as $key => $value): ?>
                                                    <tr id="<?php echo $value['agent_id'] ?>" > 
                                                        <td style="text-align: center;">
                                                            <img class="img-circle" width="30px" src="../assets/images/users/<?php echo $page_folder ?>/<?php echo $value['photo'] ?>" alt="photo"> 
                                                        </td>
                                                        <td>
                                                            
                                                            <a href="#" class="goToProfileAgent"><u><?php echo $value['first_name']. ' ' .$value['last_name'] ?></u></a>      
                                                        </td>
                                                        <td>
                                                            <?php echo $value['company'] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $value['cell'] ?>
                                                        </td> 
                                                        <td>
                                                            <?php echo $value['email_address'] ?>
                                                        </td> 
                                                        <td>
                                                            <div class="btn-group">
                                                                <button class="btn btn-secondary btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="mdi mdi-email"></i> Email
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="sr-only">Toggle Dropdown</span>
                                                                </button>
                                                                <div class="dropdown-menu rowTools">
                                                                    <a class="dropdown-item" href="#" value="tasks-tab">
                                                                        <i class="mdi mdi-plus-circle"></i> Add Task</a>
                                                                    <a class="dropdown-item" href="#" value="notes-tab">
                                                                        <i class="mdi mdi-plus-circle"></i> Add Note</a> 
                                                                    <a class="dropdown-item" href="#" value="tab_support">
                                                                        <i class="mdi mdi-help-circle"></i> Support Tickets</a> 
                                                                    <a class="dropdown-item" href="#" id="BtnDeleteAgent" value="tab_support">
                                                                        <i class="mdi mdi-delete-variant "></i> Delete</a> 
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="profileSection" class="row hide"> 
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>
                                <li class="nav-item"> 
                                    <a id="tab_support-header" class="nav-link btnBackToSupportTicketList" data-toggle="tab" href="#tab_support" role="tab" aria-selected="false">
                                        Support <span class="text-danger" id="notif_support"></span>
                                    </a> 
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"> 
                                <div class="tab-pane active show" id="profile" role="tabpanel">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 text-center b-r" > 
                                                <form id="uploadProfilePictureForm" enctype="multipart/form-data">
                                                    <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="../assets/images/LynxLogo.png" data-show-remove="false" data-height="150" />
                                                    <input type="hidden" name="agent_id">
                                                </form>   
                                                <h3 class="m-b-0"><span class="first_name"></span> <span class="last_name"></span></h3>
                                            </div>
 
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <strong>Company</strong>
                                                <br>
                                                <p class="text-muted company"></p>
                                                <strong>Location</strong>
                                                <br>
                                                <p class="text-muted ">
                                                    <span class="address"></span>, 
                                                    <span class="city"></span><br>
                                                    <span class="state_province"></span>, 
                                                    <span class="zip_postal_code"></span>
                                                </p>
                                                <strong>Office</strong>
                                                <br>
                                                <p class="text-muted office"></p>
                                                
                                            </div>
                                            <div class="col-md-5 col-xs-6 "> 
                                                <strong>Cell</strong>
                                                <br>
                                                <p class="text-muted cell"></p>
                                                <strong>Email</strong>
                                                <br>
                                                <p class="text-muted email_address"></p>
                                                
                                            </div> 
                                        </div>
                                        <!-- <hr> 
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h3>Other Information</h3>
                                                <span>Last Login: <span class="last_login"></span></span><br>
                                                <span>Date of Birth: <span class="date_of_birth"></span></span><br>  
                                                <span>SS#: xxx-xx-<span class="ss_restricted"></span></span><br>
                                                <span>Salary: <span class="salary"></span></span><br>
                                                <span>Commission: <span class="commission"></span></span>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Bank Information</h5>
                                                <span>Bank Name: <span class="bank_name"></span></span><br>
                                                <span>Bank Address: <span class="bank_address"></span></span><br>
                                                <span>Account #: <span class="account_number"></span></span><br>
                                                <span>Routing #: <span class="routing_number"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Emergency Information</h5>
                                                <span>Name: <span class="emergency_name"></span></span><br>
                                                <span>Phone: <span class="emergency_phone"></span></span><br>
                                                <span>Address: <span class="emergency_address"></span></span><br>
                                                <span>City: <span class="emergency_city"></span></span><br>
                                                <span>State: <span class="emergency_state"></span></span><br>
                                                <span>Zip: <span class="emergency_zip"></span></span><br>
                                                <span>Relationship: <span class="emergency_relationship"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>User Account Information</h5>
                                                <span>Username: <span class="username"></span></span><br>
                                                <span>Password: <span class="password"></span></span><br>
                                            </div>
                                        </div> -->


                                        <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                            <li class="nav-item"> <a id="tasks-tab-header" class="nav-link active show" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="true">Tasks</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#email-tab" role="tab" aria-selected="false">Emails</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#app-tab" role="tab" aria-selected="false">Appointment</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#text-tab" role="tab" aria-selected="false">Text</a> </li>
                                            <!-- <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#call-tab" role="tab" aria-selected="false">Cal Logs</a> </li> -->
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a task</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_task" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Task Date">
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewTask" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="task_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                        
                                                        
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="notes-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"><br>sticky?</i>
                                                            
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewNote" class="btn btn-success waves-effect waves-light" type="button">Save Note</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="note_list_sticky" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                        <div id="note_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="email-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <div class="row" class="m-b-20">
                                                        <div class="col-12">
                                                            <form method="POST" id="formSendAgentEmail">
                                                                <input id="inputFromClientEmail" required="" type="text" name="" class="form-control" placeholder="From..." value="no-reply@creditlynx.com" style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
                                                                <input id="inputSubjectClientEmail" required="" type="text" name="" class="form-control" placeholder="Subject..."  style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
                                                                <textarea id="inputMessageClientEmail" required="" style="border-top-left-radius: 0px;border-top-right-radius: 0px; " class="form-control" rows="3" placeholder="Message..."></textarea>
                                                                <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Send <i class="fas fa-paper-plane"></i></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    Emails <a class="btnRefreshEmailTab" href="#"><i class="fas fa-refresh"></i></a>
                                                    <div id="containerAgentEmails" class="">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="app-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="text-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <div class="row" class="m-b-20">
                                                        <div class="col-12">
                                                            <form method="POST" id="formSendAgentText">
                                                                <input id="inputTitleClientText" type="text" name="" class="form-control hide" placeholder="Title..." style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
                                                                <textarea id="inputTextMessageClientText" required="" style="border-top-left-radius: 0px;border-top-right-radius: 0px; " class="form-control" rows="3" placeholder="Text Message..."></textarea>
                                                                <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Send <i class="fas fa-paper-plane"></i></button>
                                                            </form>
                                                                
                                                        </div>
                                                            
                                                    </div>
                                                    Text Messages <a class="btnRefreshTextTab" href="#"><i class="fas fa-refresh"></i></a>
                                                    <div id="containerAgentTexts" class="">
                                                        
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="tab-pane" id="call-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                            
                                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="row">
                                                <div class="col-md-3 col-xs-6">
                                                    <h4 style="margin-bottom: 17px">User Account Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_username" class="control-label">Username:</label>
                                                        <input type="text" name="username" required="" class="form-control" id="profile_username">
                                                        <span class="bar"></span> 
                                                        <small id="profile_errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small>  
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_password" class="control-label">Password:</label>
                                                        <input type="password" name="password" required="" class="form-control" id="profile_password">
                                                        <span class="bar"></span>
                                                    </div>
                                                    <br>
                                                    
                                                </div>
                                                <div class="col-md-3 col-xs-6">
                                                    <h4 style="margin-bottom: 17px">Basic Information</h4> 
                                                    <div class="form-group">
                                                        <label for="first_name" class="control-label">First Name:</label>
                                                        <input type="text" name="first_name" required="" class="form-control" id="profile_first_name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="last_name" class="control-label">Last Name:</label>
                                                        <input type="text" name="last_name" required="" class="form-control" id="profile_last_name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="company" class="control-label">Company:</label>
                                                        <input type="text" name="company" required="" class="form-control" id="profile_company">
                                                    </div> 
                                                    <div class="form-group">
                                                        <label for="office" class="control-label">Office:</label>
                                                        <input type="text" name="office" required="" class="form-control" id="profile_office">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="cell" class="control-label">Cell:</label>
                                                        <input type="text" name="cell" required="" class="form-control phone-inputmask" id="profile_cell">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email_address" class="control-label">Email Address:</label>
                                                        <input type="email" name="email_address" required="" class="form-control" id="profile_email_address">
                                                    </div>
                                                    
                                                </div> 
                                                <div class="col-md-3 col-xs-6 m-t-40">
                                                    <div class="form-group">
                                                        <label for="address" class="control-label">Address:</label>
                                                        <input type="text" name="address" required="" class="form-control" id="profile_address">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="city" class="control-label">City:</label>
                                                        <input type="text" name="city" required="" class="form-control" id="profile_city">
                                                    </div> 
                                                    <div class="form-group">
                                                        <h5>State/Province:</h5> 
                                                        <select class="select2" name="state_province" id="profile_state_province" style="width: 100%">
                                                            <option value="0">Select State/Province</option>
                                                            <?php foreach ($state_provinces as $key => $value): ?>
                                                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label for="zip_postal_code" class="control-label">Zip/Postal Code:</label>
                                                        <input type="text" name="zip_postal_code" required="" class="form-control" id="profile_zip_postal_code">
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <!-- <button id="btnProfileUpdate" type="button" class="btn btn-success pull-right waves-effect waves-light">Update Profile</button> -->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_support" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body" id="containerSupportTicketList">
                                                        <h4 class="card-title">Ticket List <a id="btnShowAddNewSupportTicketContainer" href="#" class="pull-right">Start New Ticket</a></h4>
                                                        <h6 class="card-subtitle">List of ticket opend</h6>
                                                        <div class="row m-t-40"> 
                                                            <!-- Column -->
                                                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                                                <div class="card card-danger">
                                                                    <div class="box text-center">
                                                                        <h1 class="font-light text-white" id="label_pending_tickets"></h1>
                                                                        <h6 style="font-size: 70%" class="text-white">Pending</h6>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                                                <div class="card card-primary">
                                                                    <div class="box text-center">
                                                                        <h1 class="font-light text-white" id="label_responded_tickets"></h1>
                                                                        <h6 style="font-size: 70%" class="text-white">Responded</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Column -->
                                                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                                                <div class="card card-success">
                                                                    <div class="box text-center">
                                                                        <h1 class="font-light text-white" id="label_resolved_tickets"></h1>
                                                                        <h6 style="font-size: 70%" class="text-white">Resolved</h6>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                        <div class="table-responsive">
                                                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                                                <thead>
                                                                    <tr> 
                                                                        <th width="50%">Subject</th>
                                                                        <th>Status</th>
                                                                        <th>Assign to</th>
                                                                        <th>Date</th> 
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="tbodySupportTicketList">
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                    <div class="card-body hide" id="containerAddNewSupportTicket">
                                                        <h4 class="card-title">New Ticket <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                                                        <!-- <h6 class="card-subtitle">Log a new help desk ticket</h6> -->
                                                        <div class="row m-t-40"> 
                                                            <div class="col-12">
                                                                <form id="formSupportTicket" method="POST" action="<?php echo $page_controller ?>/saveSupportTicket"> 
                                                                    <div class="demo-radio-button text-center">
                                                                        <input type="hidden" name="sender">
                                                                        <input type="hidden" name="sender_id">
                                                                        <input value="Sales Team" name="assigned_to" type="radio" class="with-gap" id="radio_4" checked="">
                                                                        <label for="radio_4">Sales Team</label> 
                                                                        <input value="Dispute Team" name="assigned_to" type="radio" class="with-gap" id="radio_1" >
                                                                        <label for="radio_1">Dispute Team</label>
                                                                        <input value="Billing Team" name="assigned_to" type="radio" class="with-gap" id="radio_2">
                                                                        <label for="radio_2">Billing Team</label>
                                                                        <input value="Support Team" name="assigned_to" type="radio" class="with-gap" id="radio_3">
                                                                        <label for="radio_3">Support Team</label> 
                                                                    </div>
                                                                    <script>
                                                                        $('#radio_1').on('click',function(){
                                                                            $('.selectResponses').addClass('hide');
                                                                            $('#selectDisputeTeamResponses').removeClass('hide');
                                                                        });
                                                                        $('#radio_2').on('click',function(){
                                                                            $('.selectResponses').addClass('hide');
                                                                            $('#selectBillingTeamResponses').removeClass('hide');
                                                                        });
                                                                        $('#radio_3').on('click',function(){
                                                                            $('.selectResponses').addClass('hide');
                                                                            $('#selectSupportResponses').removeClass('hide');
                                                                        });
                                                                        $('#radio_4').on('click',function(){
                                                                            $('.selectResponses').addClass('hide');
                                                                            $('#selectSalesResponses').removeClass('hide');
                                                                        });
                                                                    </script>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <label class="input-group-text">Templates</label>
                                                                        </div>
                                                                        <select class="form-control selectResponses hide" id="selectDisputeTeamResponses">
                                                                            <option value="">Select Response</option>
                                                                             <?php foreach ($dispute_team_responses as $key => $value): ?>
                                                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                             <?php endforeach ?>
                                                                        </select>
                                                                        <select class="form-control selectResponses hide" id="selectBillingTeamResponses">
                                                                            <option value="">Select Response</option>
                                                                             <?php foreach ($billing_team_responses as $key => $value): ?>
                                                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                             <?php endforeach ?>
                                                                        </select>
                                                                        <select class="form-control selectResponses hide" id="selectSupportResponses">
                                                                            <option value="">Select Response</option>
                                                                             <?php foreach ($support_team_responses as $key => $value): ?>
                                                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                             <?php endforeach ?>
                                                                        </select>
                                                                        <select class="form-control selectResponses" id="selectSalesResponses">
                                                                            <option value="">Select Response</option>
                                                                             <?php foreach ($sales_team_responses as $key => $value): ?>
                                                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                             <?php endforeach ?>
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Subject</label>
                                                                        <input required="" id="new_support_subject" type="text" name="subject" class="form-control">
                                                                        <span class="bar"></span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Message</label>
                                                                        <textarea required="" id="new_support_message" rows="4" class="form-control" name="message"></textarea>
                                                                        <span class="bar"></span>
                                                                    </div>

                                                                    

                                                                    <button type="submit" class="btn btn-success waves-effect waves-light pull-right" >Submit</button>
                                                                </form>
                                                            </div>
                                                                
                                                        </div> 

                                                    </div>
                                                    <div class="card-body hide" id="containerSupportTicket">
                                                        <h4 class="card-title">Ticket Details <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                                                        <!-- <h6 class="card-subtitle">Ticket Details</h6> -->
                                                        <div class="row m-t-40"> 
                                                            <div class="col-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <label class="input-group-text">Status</label>
                                                                    </div>
                                                                    <select class="form-control support_status" id="support_status">
                                                                        <option value="Pending">Pending</option>
                                                                        <option value="Responded">Responded</option>
                                                                        <option value="Resolved">Resolved</option>  
                                                                    </select>
                                                                </div>
                                                            </div> 
                                                            <div class="col-6">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <label class="input-group-text">Subject</label>
                                                                    </div>
                                                                    <input class="form-control support_subject" type="text" disabled="" name="" value="test">
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <label class="input-group-text">Date</label>
                                                                    </div>
                                                                    <input class="form-control support_support_date" type="text" disabled="" name="" value="12-12-12">
                                                                </div>
                                                            </div> 
                                                            <div class="col-12">
                                                                <textarea disabled="" style="font-size: 70%" class="form-control support_message" rows="4"></textarea>
                                                            </div>
                                                            
                                                            <div class="col-12">
                                                                <label><span id="support_assigned_to"></span> Reponses</label>
                                                                <table class="table table-hover no-wrap">
                                                                    <thead class="hide">
                                                                        <tr>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="tbodySupportTicketReponses">
                                                                        <tr>
                                                                            <td style="width: 100%">
                                                                                <label>responder_name</label>
                                                                                <span class="pull-right">datetime</span>
                                                                                <blockquote>asdas</blockquote>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-12">
                                                                 <label>Add a response?</label>
                                                                 <div class="input-group">
                                                                     <div class="input-group-prepend">
                                                                         <label class="input-group-text">Responses</label>
                                                                     </div>
                                                                     <select class="form-control" id="selectAddResponses">
                                                                        <option value="">Select Response</option>
                                                                         <?php foreach ($support_responses as $key => $value): ?>
                                                                             <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                         <?php endforeach ?>
                                                                     </select>
                                                                     <script>
                                                                        $('#selectAddResponses').on('change',function(){
                                                                            var value = $(this).val();
                                                                            $('#support_response').val(value);
                                                                        });
                                                                     </script>
                                                                 </div>
                                                                 <textarea id="support_response" rows="5" style="font-size: 70%" class="form-control"></textarea>
                                                                 <button class="btnSaveSupportTicketResponse btn btn-success waves-effect waves-light">Submit Reponse</button>
                                                            </div>

                                                                
                                                        </div> 

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->





<!-- sample modal content -->
<div id="add-edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <?php echo $page_title_s ?> Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body"> 
                <form enctype="multipart/form-data" id="formAddUser" method="post" action="<?php echo $page_controller ?>/saveDetail" class="floating-labels"> 
                    <input type="hidden" name="agent_id">
                    <h5 for="input-file-now-custom-1">Profile Picture</h5>
                    <input name="user_photo" type="file" id="input-file-now-custom" class="dropify" data-default-file="../assets/images/LynxLogo.png" data-show-remove="false"/>
                    <input id="current_photo" type="hidden" name="current_photo" value="LynxLogo.png"> 
                    <div class="row">
                        <div class="col-6  m-t-40">
                            <h4 style="margin-bottom: 17px">Basic Information</h4> 
                            <div class="form-group">
                                <label for="first_name" class="control-label">First Name:</label>
                                <input type="text" name="first_name" required="" class="form-control" id="first_name">
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="control-label">Last Name:</label>
                                <input type="text" name="last_name" required="" class="form-control" id="last_name">
                            </div>
                            <div class="form-group">
                                <label for="company" class="control-label">Company:</label>
                                <input type="text" name="company"  class="form-control" id="company">
                            </div>
                            <div class="form-group">
                                <label for="address" class="control-label">Address:</label>
                                <input type="text" name="address"  class="form-control" id="address">
                            </div>
                            <div class="form-group">
                                <label for="city" class="control-label">City:</label>
                                <input type="text" name="city"  class="form-control" id="city">
                            </div> 
                            <div class="form-group">
                                <h5>State/Province:</h5> 
                                <select class="select2" name="state_province" id="state_province" style="width: 100%">
                                    <option value="0">Select State/Province</option>
                                    <?php foreach ($state_provinces as $key => $value): ?>
                                        <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="zip_postal_code" class="control-label">Zip/Postal Code:</label>
                                <input type="text" name="zip_postal_code"  class="form-control" id="zip_postal_code">
                            </div>
                            <div class="form-group">
                                <label for="cell" class="control-label">Cell:</label>
                                <input type="text" name="cell"  class="form-control" id="cell">
                            </div>
                            <div class="form-group">
                                <label for="office" class="control-label">Office:</label>
                                <input type="text" name="office"  class="form-control" id="office">
                            </div>
                            <div class="form-group">
                                <label for="email_address" class="control-label">Email Address:</label>
                                <input type="email" name="email_address"  class="form-control" id="email_address">
                            </div> 
                        </div>
                        <div class="col-6 m-t-40"> 
                            <h4 style="margin-bottom: 17px">User Account Information</h4>
                            <div class="form-group">
                                <label for="username" class="control-label">Username:</label>
                                <input name="username" type="text" required="" class="form-control" id="username">
                                <span class="bar"></span> 
                                <small id="errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small>  
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password:</label>
                                <input name="password" type="password" required="" class="form-control" id="password">
                                <span class="bar"></span>
                            </div>
                        </div>
                    </div>
                        
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
 
 <script>
$(document).ready(function() {
    $('.left_nav_'+'<?php echo $page_controller ?>').addClass('active');
    var newUser = false;
    var agent_id = 0; 
    var support_id = 0;

// EVENTS START

   
    $('#formSendAgentText').on('submit',function(e){
        e.preventDefault();
        var title = $('#inputTitleClientText').val();
        var text_message = $('#inputTextMessageClientText').val(); 
        sendAgentText('Text',agent_id,title,text_message);
    });

    function sendAgentText(type,agent_id,subject,notes) {
        $.post('<?php echo $page_controller ?>/sendAgentText',
            {type,agent_id,subject,notes},function(data){ 
                getAgentTexts(agent_id);
                // getClientTexts(client_id);
                $('#inputSubjectClientEmail').val('');
                $('#inputMessageClientEmail').val('');
                $('#inputTitleClientText').val('');
                $('#inputTextMessageClientText').val('');
            });
    }

    function getAgentTexts(agent_id) {
        $.post('<?php echo $page_controller ?>/getAgentTexts',
            {agent_id},function(data){
                data = JSON.parse(data); 
                $('#containerAgentTexts').empty(); 
                var from_phone = 'no-reply@creditlynx.com';
                if (data.length != 0) {

                    $.each(data,function(key,value){ 
                        if (value.type == 'from_agent') { 
                            from_phone = $('input[name="profile_name"]').val();
                        } else { 
                            from_phone = 'CreditLynx';
                        }
                        var text_message = value.text_message != null ? value.text_message : '';
                        text_message = text_message.replace('style="font-family:verdana,sans-serif;color:#3333ff"','');
                        text_message = text_message.replace(/<br>/g,''); 
                        text_message = '<blockquote style="white-space: pre-wrap">'+$.trim(text_message)+'</blockquote>'; 
                        var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                                <div class="card-body emailHeader" style="padding-top:5px;padding-bottom:5px">\
                                                    <h5 class="card-title m-b-0">From: '+from_phone+'<small class="pull-right">'+moment(value.date).format('lll')+'</small> '+text_message+'</h5>\
                                                </div>\
                                            </div>';
                        $('#containerAgentTexts').append(card);
                        
                    });
                } else {
                    $('#containerAgentTexts').append('No Texts Found');
                }


        });
                
    }

    $('#formSendAgentEmail').on('submit',function(e){
        e.preventDefault();
        var from = $('#inputFromClientEmail').val();
        var subject = $('#inputSubjectClientEmail').val();
        var message = $('#inputMessageClientEmail').val();
        sendAgentEmail('Email',agent_id,subject,message,from);
        getEmails(agent_id);
    });



    function sendAgentEmail(type,agent_id,subject,notes,from) {
        $.post('<?php echo $page_controller ?>/sendAgentEmail',
            {type,agent_id,subject,notes,from},function(data){ 
                getEmails(agent_id);
                // getClientTexts(client_id);
                $('#inputFromClientEmail').val('');
                $('#inputSubjectClientEmail').val('');
                $('#inputMessageClientEmail').val('');
                $('#inputTitleClientText').val('');
                $('#inputTextMessageClientText').val('');
            });
    }

    function getEmails(agent_id) {
        $.post('<?php echo $page_controller ?>/getAgentEmails',
            {agent_id},function(data){ 
                data = JSON.parse(data);  
                $('#containerAgentEmails').empty();
                var from = 'CreditLynx';
                var from_email = 'no-reply@creditlynx.com';
                var img = '<div>\
                                <a href="javascript:void(0)"><img src="<?php echo base_url() ?>/assets/images/LynxLogo.png" alt="user" width="40" class="img-circle"></a>\
                            </div>';
                if (data.length != 0) {
                    $.each(data,function(key,value){ 
                        from = 'CreditLynx';
                        from_email = value.from;
                        if (value.type == 'from_agent') {
                            from = $('input[name="first_name"]').val() + ' ' + $('input[name="last_name"]').val();
                            from_email = $('input[name="email_address"]').val();
                        } 
                        var message = value.message != null ? value.message : '';
                        var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                                <div value="'+value.subject+'" from="'+value.from+'" class="card-body emailHeader" style="cursor:pointer;padding-top:5px;padding-bottom:5px">\
                                                    <h5 class="card-title m-b-0">'+value.subject+'<small class="pull-right">'+moment(value.date).format('lll')+'</small></h5>\
                                                </div>\
                                                <div id="email_container_'+value.email_id+'" class="card-body b-t hide animated fadeIn">\
                                                    <div class="d-flex m-b-40">\
                                                        '+img+'\
                                                        <div class="p-l-10">\
                                                            <h4 class="m-b-0">'+from+'</h4>\
                                                            <small class="text-muted">From: '+from_email+'</small>\
                                                        </div>\
                                                    </div>\
                                                    <p>'+value.message+'</p>\
                                                </div>\
                                            </div>';
                        $('#containerAgentEmails').append(card);
                        $('.emailHeader').on('click',function(){
                            var email_container = $(this).closest('.card').find('#email_container_'+value.email_id);
                            var subject = $(this).attr('value');
                            var from = $(this).attr('from');
                            $('#inputSubjectClientEmail').val(subject);
                            $('#inputFromClientEmail').val(from);
                            if (email_container.hasClass('hide')) {
                                email_container.removeClass('hide');
                            } else {
                                email_container.addClass('hide');
                            }
                        });
                    });
                } else {
                    $('#containerAgentEmails').append('No Emails Found');
                }


            });
           
    }



    $('.rowTools a').on('click',function(){
        if ($(this).attr('id') != 'BtnDeleteAgent') {
            newUser = false;
            agent_id = $(this).closest('tr').attr('id');
            var name = $(this).closest('tr').find('.goToProfileAgent').find('u').html();
            var tr = $(this).closest('tr'); 
            $('#searchAgent').val(agent_id); 

            $('#searchAgent').trigger('change');

            

            var tab_pane = $(this).attr('value'); 
            $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('active');
            $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('show');
            $('#'+tab_pane).addClass('active');
            $('#'+tab_pane).addClass('show');

            $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('active');
            $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('show');

            $('#'+tab_pane+'-header').addClass('active');
            $('#'+tab_pane+'-header').addClass('show');
        } else {
            var tr = $(this).closest('tr');
            var agent_id = $(this).closest('tr').attr('id');
            swal({   
                title: "Are you sure?",   
                text: 'Are you sure you want to delete this agent?',   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes!",   
                closeOnConfirm: false ,
                preConfirm: function() {
                    $.post('<?php echo base_url('admin/agents/agentDelete') ?>',{agent_id}, function(data){
                        // data = JSON.parse(data);
                        console.log(data); 
                        tr.remove();
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    }); 
                }
            });
        } 
    });

    $('.selectResponses').on('change',function(){
        var value = $(this).val();
        var text = $(this).find('option:selected').text();
        $('#new_support_subject').val(text);
        $('#new_support_message').val(value); 
    });

    $('.btnSaveSupportTicketResponse').on('click',function(){
        var response = $('#support_response');
        if (response.val() != '') {
            
            response = response.val();
            $('#support_response').val('');
            saveSupportTicketResponse(support_id,response);

        } else {
            response.focus();
        }
    });

    $('#support_status').on('change',function(){
        var status = $(this).val(); 
        updateSupportTicketStatus(support_id,status);
    });

    $('#tbodySupportTicketList').on('click','.supportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('value');
        $('#containerAddNewSupportTicket').addClass('hide');
        $('#containerSupportTicketList').addClass('hide');
        $('#containerSupportTicket').removeClass('hide');
        getSupportTicket(support_id);
    });

    $('#tbodySupportTicketList').on('click','.btnDeleteSupportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('value');
        deleteSupportTicket(support_id);
    });

    $('#formSupportTicket').on('submit',function(e){ 
        e.preventDefault();
        $(this).find('input[name=sender]').val('agent');;
        $(this).find('input[name=sender_id]').val(agent_id);; 

        $.post('<?php echo $page_controller ?>/saveSupportTicket',
            $(this).serialize(),function(data){
                $('#btnShowAddNewSupportTicketContainer').trigger('click');
                getTableSupportTickets(agent_id);
            });
    });

    

    $('.btnBackToSupportTicketList').on('click',function(e){
        e.preventDefault(); 
        $('#containerAddNewSupportTicket').addClass('hide');
        $('#containerSupportTicketList').removeClass('hide');
        $('#containerSupportTicket').addClass('hide'); 
    });

    $('#btnShowAddNewSupportTicketContainer').on('click',function(e){
        e.preventDefault();
        if ($('#containerAddNewSupportTicket').hasClass('hide')) {
            $('#containerAddNewSupportTicket').removeClass('hide');
            $('#containerSupportTicketList').addClass('hide');
            $('#containerSupportTicket').addClass('hide');
        } else {
            $('#containerAddNewSupportTicket').addClass('hide');
            $('#containerSupportTicketList').removeClass('hide');
            $('#containerSupportTicket').addClass('hide');
        }
        
    });
    
    $('#btnSearchArrowLeft').on('click',function(e){
        e.preventDefault();
        var nextElement = $('#searchAgent > option:selected').prev('option');
        if (nextElement.length > 0) {
            $('#searchAgent').val($('#searchAgent option:selected').prev().val());  
        } 
        $('#searchAgent').trigger('change'); 
    });

    $('#btnSearchArrowRight').on('click',function(e){
        e.preventDefault();
        var nextElement = $('#searchAgent > option:selected').next('option');
        if (nextElement.length > 0) { 
            $('#searchAgent').val($('#searchAgent option:selected').next().val());  
        }
        $('#searchAgent').trigger('change'); 
    });

    $('#searchAgent').on('change',function(){ 
        agent_id = $(this).val();
        getDetails(agent_id);
        getTasks(agent_id);
        getNotes(agent_id); 
        getEmails(agent_id);
        getTableSupportTickets(agent_id);

        $('#profileSection').removeClass('hide');
        $('#listSection').addClass('hide');
        $('#profile_active').removeClass('hide'); 
        $('#profile_active').html($('#searchAgent').find('option:selected').text());
        $('#searchInputAgent').val($('#searchAgent').find('option:selected').text());
    });


    $('#formAddUser').on('submit',function(e){
        var errorUsername = $('#errorUsername');  
        if (errorUsername.hasClass('hide')) { 
            $('#formAddUser').submit();
        }  else {
            e.preventDefault();
        }
        
        
    });

    $('#pageTableAgents').DataTable({ 
        'aaSorting': [],
        "aoColumnDefs": [{ 
            'bSortable': false, 
            'aTargets': [0] 
        }],
        dom: 'Bfrtip',
        "pageLength": 100
    }); 

    $('#pageTableAgents').removeClass('dataTable');
    $('#pageTableAgents').on('click','.goToProfileAgent',function(){
        newUser = false;
        agent_id = $(this).closest('tr').attr('id');
        var name = $(this).closest('u').html();
        $('#searchAgent').val(agent_id);
        $('#searchAgent').trigger('change');
    });

    $('#btnNewUser').on('click',function(){
        newUser = true;
        $('#formAddUser')[0].reset();
        $('input[name=username]').prop('readonly',false);
        $('.dropify-render').find('img').attr('src','../assets/images/users/<?php echo $page_folder ?>/LynxLogo.png');

        var inputs =  $('#formAddUser input'); 
        $.each(inputs,function(key,input){ 
            $(input).closest('.form-group').removeClass('focused');
        }); 

    });

    $('#username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfExist(username);
        }
        
    });

     

    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,agent_id,task,task_date);
            } else {
                task_date.focus();
            }
            
        } else {
            task.focus();
        }
    });

    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });

    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,agent_id,note,note_sticky);
        } else {
            note.focus();
        }
    });

    $('#uploadProfilePicture').on('change',function(){
        $.ajax({ 
            url: '<?php echo $page_controller ?>/uploadProfilePicture',
            type: 'POST', 
            data: new FormData($('#uploadProfilePictureForm')[0]), 
            cache: false,
            contentType: false,
            processData: false, 
            success: function(data) { 

            }
        });
    });

// EVENTS END
// FUNCTIONS START
    get_partner_companies();
    function get_partner_companies() { 
        var partner_type = 'Agent';
        $.post('Agents/get_partner_companies', {partner_type}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var companies = [];
            $.each(data,function(key,value){  
                var newOption = {
                                value: value.company_name,
                                label: value.company_name,
                                desc: value.company_name, 
                              };
                companies.push(newOption); 
            }); 
            $('#profile_company' ).autocomplete({
                minLength: 2,
                source: companies,
                focus: function( event, ui ) {
                    $('#profile_company').val(ui.item.label);
                    return false;
                },
                select: function( event, ui ) {
                    // $( "#searchInputAgent" ).val( ui.item.label );  
                    $('#profile_company').val(ui.item.label);
                    return false;
                }
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                .append( "<div>" + item.label +"</div>" )
                .appendTo( ul );
            };

            $('#company' ).autocomplete({
                minLength: 2,
                source: companies,
                focus: function( event, ui ) {
                    $('#company').val(ui.item.label);
                    return false;
                },
                select: function( event, ui ) {
                    // $( "#searchInputAgent" ).val( ui.item.label );  
                    $('#company').val(ui.item.label);
                    return false;
                }
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                .append( "<div>" + item.label +"</div>" )
                .appendTo( ul );
            };
        });
    }



    function saveSupportTicketResponse(support_id,response){
        $.post('<?php echo $page_controller ?>/saveSupportTicketResponse',
            {support_id,response},function(data){ 
                getSupportTicket(support_id);
                getTableSupportTickets(agent_id);
                $('#selectSupportResponses').val('');
            });
    }

    function updateSupportTicketStatus(support_id,status){
        $.post('<?php echo $page_controller ?>/updateSupportTicketStatus',
            {support_id,status},function(data){ 
                getTableSupportTickets(agent_id);
            });
    }

    function getSupportTicket(support_id){
        $.post('<?php echo $page_controller ?>/getSupportTicket',
            {support_id},function(data){
                data = JSON.parse(data);
                var assigned_to = '';
                $.each(data['support_tickets'],function(key,value){ 
                    $.each(value,function(k,v){
                        $('.support_'+k).val(v);
                    });
                    $('.support_support_date').val(moment(value.support_date).format('MM/DD/YYYY h:mm:ss a'));
                    // $('#support_assigned_to').html(value.assigned_to);
                    assigned_to = value.assigned_to;
                });
                var tbodySupportTicketReponses = $('#tbodySupportTicketReponses');
                tbodySupportTicketReponses.empty();
                if ((data['support_ticket_responses']).length != 0) {
                    $.each(data['support_ticket_responses'],function(key,value){ 
                        var newTr = '<tr>\
                                        <td >\
                                            <div>\
                                                <label>'+value.responder+'</label>\
                                                <span class="pull-right">'+moment(value.response_date).format('MM/DD/YYYY h:mm:ss a')+'</span>\
                                                <blockquote style="white-space: pre-wrap !important;line-height: 18px !important">'+value.response+'</blockquote>\
                                            </div>\
                                        </td>\
                                    </tr>';

                        tbodySupportTicketReponses.append(newTr);
                    });
                } else {
                    var newTr = '<tr>\
                                    <td>\
                                        no response found \
                                    </td>\
                                </tr>';

                    tbodySupportTicketReponses.append(newTr);
                }
            });
    }

    function deleteSupportTicket(support_id){
        $.post('<?php echo $page_controller ?>/deleteSupportTicket',
            {support_id},function(data){
                getTableSupportTickets(agent_id);
            });
    }

    function getTableSupportTickets(agent_id) {
        $.post('<?php echo $page_controller ?>/getTableSupportTickets',
            {agent_id},function(data){
                data = JSON.parse(data);   
                var tbodySupportTicketList = $('#tbodySupportTicketList');
                tbodySupportTicketList.empty();
                var total_tickets = 0;
                var total_responded = 0;
                var total_resolved = 0;
                var total_pending = 0;  
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        total_tickets++;
                        var label_color = '';
                        if (value.status == 'Pending') {
                            label_color = 'label-danger';
                            total_pending++;
                        }
                        if (value.status == 'Responded') {
                            total_responded++;
                            label_color = 'label-primary';
                        }
                        if (value.status == 'Resolved') {
                            total_resolved++;
                            label_color = 'label-success';
                        }  

                                            // <td>\
                                            //     <button type="button" class="btnDeleteSupportTicket btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>\
                                            // </td>\
                        var newTicket = '<tr value="'+value.support_id+'">\
                                            <td><a href="#" class="supportTicket">'+value.subject+'</a></td>\
                                            <td><span class="label '+label_color+'">'+value.status+'</span> </td>\
                                            <td>'+value.assigned_to+'</td>\
                                            <td>'+moment(value.support_date).format('MM/DD/YYY h:mm:ss a')+'</td>\
                                        </tr>';
                        tbodySupportTicketList.append(newTicket);
                    });
                } else {
                    var newTicket = '<tr >\
                                        <td colspan="5">No support tickets found</td>\
                                    </tr>';
                        tbodySupportTicketList.append(newTicket);
                }

                if (total_pending != 0) {
                    $('#notif_support').html('( '+total_pending+' )');
                    // $.toast({
                    //     heading: 'Pending Support Ticket!',
                    //     text: 'there is a pending support ticket, check it out on Support tab!',
                    //     position: 'top-right',
                    //     loaderBg:'#ffc107',
                    //     icon: 'warning',
                    //     hideAfter: 3500, 
                    //     stack: 6
                    //   });
                } else {
                    $('#notif_support').html('');
                }


                // $('#label_total_tickets').html(total_tickets);
                $('#label_responded_tickets').html(total_responded);
                $('#label_resolved_tickets').html(total_resolved);
                $('#label_pending_tickets').html(total_pending);  


            });
    }



    function getDetails(agent_id) {
        $.post('<?php echo $page_controller ?>/getDetails',
            {agent_id},function(data){
                data = JSON.parse(data);  
                console.log(data);
                $('input[name=current_photo]').val(data['photo']);
                $('.dropify-render').find('img').attr('src','../assets/images/users/<?php echo $page_folder ?>/'+data['photo']);
                
                $('#profileSection').find('#profile_picture').attr('src','../assets/images/users/<?php echo $page_folder ?>/'+data['photo']); 
                $('input[name=date_of_birth]').val(moment(data['date_of_birth']).format('YYYY-MM-DD')); 
                $.each(data,function(k,v){
                    $('input[type=text][name='+k+']').val(v);
                    $('input[type=email][name='+k+']').val(v);
                    $('input[type=password][name='+k+']').val(v);
                    $('input[type=hidden][name='+k+']').val(v);
                    $('select[name='+k+']').val(v);

                    $('.'+k).html(v);
                    // console.log(k+ ' ' +v);
                });

                $('.tab-pane .select2').trigger('change');

                var inputs =  $('#formAddUser input'); 
                $.each(inputs,function(key,input){ 
                    $(input).closest('.form-group').addClass('focused');
                });
 
                // $('input[name=username]').prop('readonly',true); 

                $('#settings').on('focusout','input,select',function(){
                    var field = $(this).attr('id');
                    field = field.replace('profile_','');
                    var value = $(this).val();  
                    updateField(field,value);
                    function updateField(field,value) { 
                        $.post('<?php echo $page_controller ?>/updateField', {agent_id,field,value} ,function(data){
                                // data = JSON.parse(data); 
                                console.log(data);
                                $.toast({
                                    heading: 'Update Success!',
                                    text: ' successfully updated to '+value,
                                    position: 'top-right',
                                    loaderBg:'#17a2b8',
                                    icon: 'success',
                                    hideAfter: 3500, 
                                    stack: 6
                                });
                                // console.log(data);
                                // getDetails(broker_id);
                                // $('.'+field).html(value);
                            }).fail(function(xhr){
                                console.log(xhr.responseText);
                            });
                        }
                });
        });
    }

    
    function checkIfExist(username) {
        var username_val = username.val();
        $.post('<?php echo $page_controller ?>/checkIfExist',
            {username_val},function(data){
                // console.log(data);
                if (data == '0') {
                    username.removeClass('form-control-danger');
                    username.closest('.form-group').removeClass('has-danger');
                    username.closest('.form-group').removeClass('has-error');
                    $('#errorUsername').addClass('hide');
                } else { 
                    username.addClass('form-control-danger');
                    username.closest('.form-group').addClass('has-danger');
                    username.closest('.form-group').addClass('has-error');
                    $('#errorUsername').removeClass('hide');
                }
            });
    }

    

    function getTasks(agent_id) {
        $.post('<?php echo $page_controller ?>/getTasks',
            {agent_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-warning waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }

                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.task_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.task+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });

                $('.btnUpdateTaskDone').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,0);
                });

                $('.btnUpdateTaskActive').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,1);
                });

                $('.btnUpdateTaskDelete').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,2);
                });
            });
    }

    function saveTask(button,agent_id,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveTask',
            {agent_id,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(agent_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateTask',
            {task_id,task_active},function(data){
                getTasks(agent_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getNotes(agent_id) {
        $.post('<?php echo $page_controller ?>/getNotes',
            {agent_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }

                    var newNote = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.note_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.note+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });

                $('.btnUpdateNoteSticky').on('click',function(){
                    var button = $(this);
                    var note_id = $(this).closest('.sl-item').attr('value');
                    if (button.hasClass('text-warning')) {
                        updateNote(button,note_id,0);
                    } else {
                        updateNote(button,note_id,1);
                    }
                    
                });
 
            });
    }

    function saveNote(button,agent_id,note,note_sticky){
        var note_ = note;
        var note = note.val(); 
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveNote',
            {agent_id,note,note_sticky},function(data){
                // console.log(data); 
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                getNotes(agent_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateNote',
            {note_id,note_sticky},function(data){
                getNotes(agent_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
// FUNCTIONS END 
});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 

