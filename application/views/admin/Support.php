<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Support</h2>
                        <!-- <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Help</li>
                        </ol> -->
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body" id="containerSupportTicketList">
                                                <h4 class="card-title">Ticket List <a id="btnShowAddNewSupportTicketContainer" href="#" class="pull-right">Start New Ticket</a></h4>
                                                <!-- <h6 class="card-subtitle">List of ticket opend</h6> -->
                                                <div class="row m-t-40"> 
                                                    <!-- Column -->
                                                    <div class="col-md-6 col-lg-4 col-xlg-4">
                                                        <div class="card card-danger">
                                                            <div class="box text-center">
                                                                <h1 class="font-light text-white" id="label_pending_tickets"></h1>
                                                                <h6 style="font-size: 70%" class="text-white">Pending</h6>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-6 col-lg-4 col-xlg-4">
                                                        <div class="card card-primary">
                                                            <div class="box text-center">
                                                                <h1 class="font-light text-white" id="label_responded_tickets"></h1>
                                                                <h6 style="font-size: 70%" class="text-white">Responded</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Column -->
                                                    <div class="col-md-6 col-lg-4 col-xlg-4">
                                                        <div class="card card-success">
                                                            <div class="box text-center">
                                                                <h1 class="font-light text-white" id="label_resolved_tickets"></h1>
                                                                <h6 style="font-size: 70%" class="text-white">Resolved</h6>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table m-t-30 table-hover no-wrap ">
                                                        <thead>
                                                            <tr> 
                                                                <th>Sender</th>
                                                                <th>Name</th>
                                                                <th width="50%">Subject</th>
                                                                <th>Status</th>
                                                                <th>Assign to</th>
                                                                <th>Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbodySupportTicketList">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="card-body hide" id="containerAddNewSupportTicket">
                                                <h4 class="card-title">New Ticket <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                                                <!-- <h6 class="card-subtitle">Log a new help desk ticket</h6> -->
                                                <div class="row m-t-40"> 
                                                    <div class="col-12">
                                                        <form id="formSupportTicket" method="POST" action="<?php echo $page_controller ?>/saveSupportTicket"> 
                                                            <div class="demo-radio-button text-center">
                                                                <input type="hidden" name="sender">
                                                                <input type="hidden" name="sender_id">
                                                                <input class="hide" value="Sales Team" name="assigned_to" type="radio" class="with-gap" id="radio_4">
                                                                <label class="hide" for="radio_4">Sales Team</label> 
                                                                <input value="Dispute Team" name="assigned_to" type="radio" class="with-gap" id="radio_1" checked="">
                                                                <label for="radio_1">Dispute Team</label>
                                                                <input value="Billing Team" name="assigned_to" type="radio" class="with-gap" id="radio_2">
                                                                <label for="radio_2">Billing Team</label>
                                                                <input value="Support Team" name="assigned_to" type="radio" class="with-gap" id="radio_3">
                                                                <label for="radio_3">Support Team</label> 
                                                            </div>
                                                            <script>
                                                                $('#radio_1').on('click',function(){
                                                                    $('.selectResponses').addClass('hide');
                                                                    $('#selectDisputeTeamResponses').removeClass('hide');
                                                                });
                                                                $('#radio_2').on('click',function(){
                                                                    $('.selectResponses').addClass('hide');
                                                                    $('#selectBillingTeamResponses').removeClass('hide');
                                                                });
                                                                $('#radio_3').on('click',function(){
                                                                    $('.selectResponses').addClass('hide');
                                                                    $('#selectSupportResponses').removeClass('hide');
                                                                });
                                                                $('#radio_4').on('click',function(){
                                                                    $('.selectResponses').addClass('hide');
                                                                    $('#selectSalesResponses').removeClass('hide');
                                                                });
                                                            </script>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <label class="input-group-text">Templates</label>
                                                                </div>
                                                                <select class="form-control selectResponses" id="selectDisputeTeamResponses">
                                                                    <option value="">Select Response</option>
                                                                     <?php foreach ($dispute_team_responses as $key => $value): ?>
                                                                         <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                     <?php endforeach ?>
                                                                </select>
                                                                <select class="form-control selectResponses hide" id="selectBillingTeamResponses">
                                                                    <option value="">Select Response</option>
                                                                     <?php foreach ($billing_team_responses as $key => $value): ?>
                                                                         <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                     <?php endforeach ?>
                                                                </select>
                                                                <select class="form-control selectResponses hide" id="selectSupportResponses">
                                                                    <option value="">Select Response</option>
                                                                     <?php foreach ($support_team_responses as $key => $value): ?>
                                                                         <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                     <?php endforeach ?>
                                                                </select>
                                                                <select class="form-control selectResponses hide" id="selectSalesResponses">
                                                                    <option value="">Select Response</option>
                                                                     <?php foreach ($sales_team_responses as $key => $value): ?>
                                                                         <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                     <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                            <script>
                                                                $('.selectResponses').on('change',function(){
                                                                    var value = $(this).val();
                                                                    var text = $(this).find('option:selected').text();
                                                                    $('#new_support_subject').val(text);
                                                                    $('#new_support_message').val(value);
                                                                });
                                                            </script>
                                                            <div class="form-group">
                                                                <label>Subject</label>
                                                                <input required="" id="new_support_subject" type="text" name="subject" class="form-control">
                                                                <span class="bar"></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Message</label>
                                                                <textarea required="" id="new_support_message" rows="4" class="form-control" name="message"></textarea>
                                                                <span class="bar"></span>
                                                            </div>

                                                            

                                                            <button type="submit" class="btn btn-success waves-effect waves-light pull-right" >Submit</button>
                                                        </form>
                                                    </div>
                                                        
                                                </div> 

                                            </div>
                                            <div class="card-body hide" id="containerSupportTicket">
                                                <h4 class="card-title"><span class="client_name text-themecolor"></span> <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                                                <!-- <h6 class="card-subtitle">Ticket Details</h6> -->
                                                <div class="row m-t-40"> 
                                                    <div class="col-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <label class="input-group-text">Status</label>
                                                            </div>
                                                            <select class="form-control support_status" id="support_status">
                                                                <option value="Pending">Pending</option>
                                                                <option value="Responded">Responded</option>
                                                                <option value="Resolved">Resolved</option> 
                                                                <option value="Closed">Closed</option>
                                                            </select>
                                                        </div>
                                                    </div> 
                                                    <div class="col-6">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <label class="input-group-text">Subject</label>
                                                            </div>
                                                            <input class="form-control support_subject" type="text" disabled="" name="" value="test">
                                                        </div>
                                                        <textarea disabled="" style="font-size: 70%" class="form-control support_message" rows="4"></textarea>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <label class="input-group-text">Date</label>
                                                            </div>
                                                            <input class="form-control support_support_date" type="text" disabled="" name="" value="12-12-12">
                                                        </div>
                                                    </div> 
                                                    <div class="col-12">
                                                        <label><span id="support_assigned_to"></span> Reponses</label>
                                                        <table class="table table-hover no-wrap">
                                                            <thead class="hide">
                                                                <tr>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbodySupportTicketReponses">
                                                                <tr>
                                                                    <td style="width: 100%">
                                                                        <label>responder_name</label>
                                                                        <span class="pull-right">datetime</span>
                                                                        <blockquote>asdas</blockquote>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-12">
                                                         <label>Add a response?</label>
                                                         <div class="input-group">
                                                             <div class="input-group-prepend">
                                                                 <label class="input-group-text">Responses</label>
                                                             </div> 
                                                             <select class="form-control" id="selectAddResponses">
                                                                <option value="">Select Response</option>
                                                                 <?php foreach ($support_responses as $key => $value): ?>
                                                                     <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                                                 <?php endforeach ?>
                                                             </select>
                                                             <script>
                                                                $('#selectAddResponses').on('change',function(){
                                                                    var value = $(this).val();
                                                                    $('#support_response').val(value);
                                                                });
                                                             </script>
                                                         </div>
                                                         <textarea id="support_response" rows="5" style="font-size: 70%" class="form-control"></textarea>
                                                         <button class="btnSaveSupportTicketResponse btn btn-success waves-effect waves-light">Submit Reponse</button>
                                                    </div>

                                                        
                                                </div> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<script>
$(document).ready(function(){

    var tableSupportTickets = $('#tbodySupportTicketList').closest('table').DataTable({ 
        'aaSorting': [],  
        dom: 'Bfrtip',
        "pageLength": 20,
        'bFilter': false
    });


    $('.btnSaveSupportTicketResponse').on('click',function(){
        var response = $('#support_response');
        if (response.val() != '') {
            
            response = response.val();
            $('#support_response').val('');
            saveSupportTicketResponse(support_id,response);

        } else {
            response.focus();
        }
    });
// EVENTS
    $('#support_status').on('change',function(){
        var status = $(this).val(); 
        updateSupportTicketStatus(support_id,status);
    });

    $('#tbodySupportTicketList').on('click','.supportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('id');
        $('#containerAddNewSupportTicket').addClass('hide');
        $('#containerSupportTicketList').addClass('hide');
        $('#containerSupportTicket').removeClass('hide');
        var client_name = $(this).closest('tr').find('td:nth-child(2)').html();
        $('.client_name').html(client_name);
        getSupportTicket(support_id);
    });

    $('#tbodySupportTicketList').on('click','.btnDeleteSupportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('id');
        deleteSupportTicket(support_id);
    });

    $('#formSupportTicket').on('submit',function(e){ 
        e.preventDefault();
        $(this).find('input[name=sender]').val('Client');;
        $(this).find('input[name=sender_id]').val(client_id);; 

        $.post('<?php echo $page_controller ?>/saveSupportTicket',
            $(this).serialize(),function(data){
                $('#btnShowAddNewSupportTicketContainer').trigger('click');
                getTableSupportTickets(client_id);
            });
    });

    

    $('.btnBackToSupportTicketList').on('click',function(e){
        e.preventDefault(); 
        $('#containerAddNewSupportTicket').addClass('hide');
        $('#containerSupportTicketList').removeClass('hide');
        $('#containerSupportTicket').addClass('hide'); 
    });

    $('#btnShowAddNewSupportTicketContainer').on('click',function(e){
        e.preventDefault();
        if ($('#containerAddNewSupportTicket').hasClass('hide')) {
            $('#containerAddNewSupportTicket').removeClass('hide');
            $('#containerSupportTicketList').addClass('hide');
            $('#containerSupportTicket').addClass('hide');
        } else {
            $('#containerAddNewSupportTicket').addClass('hide');
            $('#containerSupportTicketList').removeClass('hide');
            $('#containerSupportTicket').addClass('hide');
        }
        
    });

// FUNCTIONS
    getTableSupportTickets();;
    function saveSupportTicketResponse(support_id,response){
        $.post('<?php echo $page_controller ?>/saveSupportTicketResponse',
            {support_id,response},function(data){ 
                getSupportTicket(support_id);
                getTableSupportTickets();
                $('#selectSupportResponses').val('');
            });
    }

    function updateSupportTicketStatus(support_id,status){
        $.post('<?php echo $page_controller ?>/updateSupportTicketStatus',
            {support_id,status},function(data){ 
                getTableSupportTickets();
            });
    }

    function getSupportTicket(support_id){
        $.post('<?php echo $page_controller ?>/getSupportTicket',
            {support_id},function(data){
                data = JSON.parse(data);
                var assigned_to = '';
                $.each(data['support_tickets'],function(key,value){ 
                    $.each(value,function(k,v){
                        $('.support_'+k).val(v);
                    });
                    $('.support_support_date').val(moment(value.support_date).format('MM/DD/YYYY h:mm:ss a'));
                    // $('#support_assigned_to').html(value.assigned_to);
                    assigned_to = value.assigned_to;
                });
                var tbodySupportTicketReponses = $('#tbodySupportTicketReponses');
                tbodySupportTicketReponses.empty();
                if ((data['support_ticket_responses']).length != 0) {
                    $.each(data['support_ticket_responses'],function(key,value){ 
                        var responder = value.responder == 'Administrator' ? 'Support Team' : value.responder;
                        var newTr = '<tr>\
                                        <td >\
                                            <div>\
                                                <label>'+responder+'</label>\
                                                <span class="pull-right">'+moment(value.response_date).format('MM/DD/YYYY h:mm:ss a')+'</span>\
                                                <blockquote style="white-space: pre-wrap !important;line-height: 18px !important">'+value.response+'</blockquote>\
                                            </div>\
                                        </td>\
                                    </tr>';

                        tbodySupportTicketReponses.append(newTr);
                    });
                } else {
                    var newTr = '<tr>\
                                    <td>\
                                        no response found \
                                    </td>\
                                </tr>';

                    tbodySupportTicketReponses.append(newTr);
                }
            });
    }

    function deleteSupportTicket(support_id){
        $.post('<?php echo $page_controller ?>/deleteSupportTicket',
            {support_id},function(data){
                getTableSupportTickets();
            });
    }

    function getTableSupportTickets() {
        $.post('<?php echo $page_controller ?>/getTableSupportTickets',
            function(data){
                data = JSON.parse(data);   
                tableSupportTickets.clear().draw();
                var total_tickets = 0;
                var total_responded = 0;
                var total_resolved = 0;
                var total_pending = 0; 
                var total_closed = 0;
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        total_tickets++;
                        var label_color = '';
                        if (value.status == 'Pending') {
                            label_color = 'label-danger';
                            total_pending++;
                        }
                        if (value.status == 'Responded') {
                            total_responded++;
                            label_color = 'label-primary';
                        }
                        if (value.status == 'Resolved') {
                            total_resolved++;
                            label_color = 'label-success';
                        }  
                        var td1 = (value.sender).toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                    return letter.toUpperCase();
                                });; 
                        var td2 = '<a href="#" class="to_client_page" type="'+value.sender+'" id="'+value.sender_id+'">'+value.name+'</a>';
                        var td3 = '<a href="#" class="supportTicket">'+value.subject+'</a>';
                        var td4 = '<span class="label '+label_color+'">'+value.status+'</span>';
                        var td5 = value.assigned_to;
                        var td6 = moment(value.support_date).format('MM/DD/YYY h:mm:ss a');
                        var td7 = '<button type="button" class="btnDeleteSupportTicket btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>';
                        tableSupportTickets.row.add([td1,td2,td3,td4,td5,td6]).node().id = value.support_id;
                        tableSupportTickets.draw(false);
                    });
                } 

                if (total_pending != 0) {
                    $('#notif_support').html('( '+total_pending+' )');
                } else {
                    $('#notif_support').html('');
                }


                // $('#label_total_tickets').html(total_tickets);
                $('#label_responded_tickets').html(total_responded);
                $('#label_resolved_tickets').html(total_resolved);
                $('#label_pending_tickets').html(total_pending); 
                $('#label_closed_tickets').html(total_closed);


            });
    }
});
</script>   
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 