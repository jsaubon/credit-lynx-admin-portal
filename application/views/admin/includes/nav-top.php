

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" style="background: #26c6da;">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url('admin/dashboard') ?>">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img width="50px" src="<?php echo base_url('assets/images/LynxLogo.png') ?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img width="50px" src="<?php echo base_url('assets/images/LynxLogo.png') ?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                            <!-- <span style="color: white"> </span>  --><span style="color: rgba(255, 255, 255, 0.8) !important"> CREDIT LYNX</span>
                         <!-- dark Logo text -->
                         <!-- <img src="<?php echo base_url() ?>/assets/images/logo-text.png" alt="homepage" class="dark-logo" /> -->
                         <!-- Light Logo text -->
                         <!-- <img src="<?php echo base_url() ?>/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /> -->
                        </span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li> 
                    </ul>
                    <!-- ============================================================== -->

                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                    <?php if ($userdata['login_type'] == 'Administrator'): ?>
                        <!-- ============================================================== -->
                            <!-- Comment -->
                            <!-- ============================================================== -->
                            <li id="li_docs_sa" class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-file-outline"></i>
                                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                    <ul>
                                        <li>
                                            <div class="drop-title">Service Agreement Uploaded</div>
                                        </li>
                                        <li>
                                            <div class="message-center notif-top"  >
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                        </li> -->
                                    </ul>
                                </div>
                            </li>
                            <li id="li_docs" class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-file-multiple"></i>
                                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                    <ul>
                                        <li>
                                            <div class="drop-title">Documents Uploaded</div>
                                        </li>
                                        <li>
                                            <div class="message-center notif-top"  >
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                        </li> -->
                                    </ul>
                                </div>
                            </li>
                            <li id="li_notif" class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-bell"></i>
                                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                    <ul>
                                        <li>
                                            <div class="drop-title">Notifications</div>
                                        </li>
                                        <li>
                                            <div class="message-center notif-top"  >
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                        </li> -->
                                    </ul>
                                </div>
                            </li> 
                    <?php endif ?>    

<?php
    if ($userdata['login_type'] == 'Sales Team') {
        $image_folder = 'sales';
    }
    if ($userdata['login_type'] == 'Dispute Team') {
        $image_folder = 'processors';
    }
    if ($userdata['login_type'] == 'Support Team') {
        $image_folder = 'team_support';
    }
    if ($userdata['login_type'] == 'Billing Team') {
        $image_folder = 'team_billing';
    }
    if ($userdata['login_type'] == 'Administrator') {
        $image_folder = 'admins';
    }
?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url('assets/images/users/'.$image_folder.'/' . $userdata['photo']) ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url('assets/images/users/'.$image_folder.'/' . $userdata['photo']) ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4 id="user_name"><?php echo $userdata['name'] ?></h4>
                                                <p id="login_type" class="text-muted"><?php echo $userdata['login_type'] ?></p>
                                                <!-- <?php if ($userdata['login_folder'] == 'employees'): ?>
                                                    <a href="<?php echo base_url('employees/profile') ?>" class="btn btn-rounded btn-success btn-sm">View Profile</a>
                                                <?php endif?> -->
                                                <?php if ($userdata['login_folder'] == 'admins'): ?>
                                                    <a href="<?php echo base_url('admin/profile') ?>" class="btn btn-rounded btn-success btn-sm">View Profile</a>
                                                <?php endif?>
                                            </div>
                                        </div>
                                    </li>

                                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo base_url('admin/profile') ?>"><i class="ti-user"></i> My Profile</a></li>
                                    <?php endif?>
                                    <?php if ($userdata['login_folder'] == 'employees'): ?>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo base_url('employees/profile') ?>"><i class="ti-user"></i> My Profile</a></li>
                                    <?php endif?>
                                    <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                                    <li role="separator" class="divider"></li>
                                    <?php if ($userdata['login_folder'] == 'employees'): ?>
                                        <li><a href="<?php echo base_url('employees/login/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                    <?php endif?>
                                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                                        <li><a href="<?php echo base_url('admin/login/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                    <?php endif?>

                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up"> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->