<style>
    .hide {
        display: none;

    }

</style>
<?php
    if ($userdata['login_type'] == 'Sales Team') {
        $image_folder = 'sales';
    }
    if ($userdata['login_type'] == 'Dispute Team') {
        $image_folder = 'processors';
    }
    if ($userdata['login_type'] == 'Support Team') {
        $image_folder = 'team_support';
    }
    if ($userdata['login_type'] == 'Billing Team') {
        $image_folder = 'team_billing';
    }
    if ($userdata['login_type'] == 'Administrator') {
        $image_folder = 'admins';
    }
?>
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url('<?php echo base_url('assets/images/background/bgsnow.jpg') ?>') no-repeat;background-size: 250px;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php echo base_url("assets/images/users/". $image_folder.'/'. $userdata['photo']) ?>" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo $userdata['name'] ?> <span class="caret"></span></a>
                        <div class="dropdown-menu animated flipInY">
                            <!-- <?php if ($userdata['login_folder'] == 'employees'): ?>
                                <a href="<?php echo base_url('employees/profile') ?>" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <?php endif?> -->
                            <?php if ($userdata['login_folder'] == 'admins'): ?>
                                <a href="<?php echo base_url('admin/profile') ?>" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                                <div class="dropdown-divider">
                                </div>
                            <?php endif?>

                            <!-- <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                            <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a> -->
                            <!-- <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a> -->


                             <a href="<?php echo base_url() ?>admin/login/logout" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <?php if ($userdata['login_folder'] == 'employees'): ?>
                    <nav class="sidebar-nav" style="padding-top: 0px">
                        <ul id="sidebarnav">
                            <li  class="nav-small-cap" style="text-transform: uppercase;">
                                <?php echo $userdata['login_type'] ?>
                            </li>
                            <li class="left_nav_profile">
                                <a href="<?php echo base_url() ?>employees/profile" aria-expanded="false">
                                    <i class="mdi mdi-account-circle"></i>
                                    <span class="hide-menu">My Profile</span>
                                </a>
                            </li>
                            <?php if ($userdata['login_type'] != 'Sales Team'): ?>
                                <li class="left_nav_clients">
                                    <a href="<?php echo base_url() ?>employees/clients" aria-expanded="false">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">Clients</span>
                                    </a>
                                </li>
                            <?php endif ?>
                                
                            <li class="left_nav_leads">
                                <a href="<?php echo base_url() ?>employees/leads" aria-expanded="false">
                                    <i class="mdi mdi-account"></i>
                                    <span class="hide-menu">Leads</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                <?php endif?>
                <?php if ($userdata['login_folder'] == 'admins'): ?>
                    <nav class="sidebar-nav" style="padding-top: 0px">
                        <ul id="sidebarnav"> 
                            <?php if ($userdata['login_type'] != 'Administrator'): ?>
                                <li class="text-center" style="margin-bottom: 0">
                                    <div style="color:  rgba(255, 255, 255, 0.8);">
                                        Worked Today: <b  id="worked_today" ></b>
                                    </div>
                                    <div class="btn-group text-center">
                                        <button id="btnStartWork" style="border-radius: 0" class="hide btn btn-outline-success waves-effect waves-light" type="button">
                                            <span class="btn-label"><i class="fas fa-play"></i></span>
                                                Start
                                        </button>

                                        <button id="btnResumeWork" style="border-radius: 0;" class="hide btn btn-outline-warning waves-effect waves-light" type="button">
                                            <span class="btn-label" style="margin-right: 0"><i class="fas fa-play"></i></span>
                                                Resume
                                        </button>
                                        <button id="btnStopWork" style="border-radius: 0" class="hide btn btn-outline-danger waves-effect waves-light" type="button">
                                            <span class="btn-label"><i class="fas fa-stop"></i></span>
                                                Stop
                                        </button>
                                    </div>
                                </li> 

                            <?php endif ?>
                                

                            <li class="left_nav_dashboard">
                                <a class="has-arrow waves-effect waves-dark" href="#employees" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class="left_nav_sales ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/dashboard">
                                            <span class="hide-menu">Administrator</span>
                                        </a>
                                    </li>
                                    <li class="left_nav_sales ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/dashboard/sales">
                                            <span class="hide-menu">Sales Team</span>
                                        </a>
                                    </li>
                                    <li class="left_nav_processors ms-hover">
                                        <a href="<?php echo base_url() ?>admin/dashboard/processors">
                                            <span class="hide-menu">Dispute Team</span>
                                        </a>
                                    </li> 
                                </ul>
                            </li>
                            <li class="left_nav_clients">
                                <a href="<?php echo base_url() ?>admin/clients" aria-expanded="false">
                                    <i class="fas fa-users"></i>
                                    <span class="hide-menu">Clients</span>
                                </a>
                            </li>
                            <li class="left_nav_leads">
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Leads</span></a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/leads" aria-expanded="false"> 
                                            <span class="hide-menu">Leads List</span>
                                        </a>
                                    </li>
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/leads2" aria-expanded="false"> 
                                            <span class="hide-menu">Leads List Status</span>
                                        </a>
                                    </li> 
                                </ul>
                                        
                            </li>

                            <li class="left_nav_agents">
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-user-secret"></i><span class="hide-menu">Agents</span></a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/agents" aria-expanded="false"> 
                                            <span class="hide-menu">Agents</span>
                                        </a>
                                    </li>
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/partner_companies?partner_type=Agent" aria-expanded="false"> 
                                            <span class="hide-menu">Agents Companies</span>
                                        </a>
                                    </li> 
                                </ul>
                                        
                            </li>
                            

                            <li class="left_nav_brokers">
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fas fa-user-circle"></i><span class="hide-menu">Brokers</span></a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/Brokers" aria-expanded="false"> 
                                            <span class="hide-menu">Brokers</span>
                                        </a>
                                    </li>
                                    <li class="ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/partner_companies?partner_type=Broker" aria-expanded="false"> 
                                            <span class="hide-menu">Brokers Companies</span>
                                        </a>
                                    </li> 
                                </ul>
                                        
                            </li>
                            
                            <li class="left_nav_appointments">
                                <a href="<?php echo base_url() ?>admin/appointments" aria-expanded="false">
                                    <i class="mdi mdi-calendar"></i>
                                    <span class="hide-menu">Appointments</span>
                                </a>
                            </li> 
                            <li class="left_nav_results_predictor">
                                <a href="<?php echo base_url() ?>admin/results_predictor" aria-expanded="false">
                                    <i class="mdi mdi-chart-line"></i>
                                    <span class="hide-menu">Results Predictor</span>
                                </a>
                            </li> 


                            <li class="ms-hover">
                                <a class="has-arrow waves-effect waves-dark" href="#employees" aria-expanded="false"><i class="mdi mdi-account-settings"></i><span class="hide-menu">Employees</span></a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class="left_nav_sales ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/employees/sales">
                                            <span class="hide-menu">Sales Team</span>
                                        </a>
                                    </li>
                                    <li class="left_nav_processors ms-hover">
                                        <a href="<?php echo base_url() ?>admin/employees/processors">
                                            <span class="hide-menu">Dispute Team</span>
                                        </a>
                                    </li>
                                    <!-- <li class="left_nav_team_billing ms-hover">
                                        <a href="<?php echo base_url() ?>admin/employees/creditors">
                                            <span class="hide-menu">Creditors</span>
                                        </a>
                                    </li> -->
                                    <li class="left_nav_team_billing ms-hover">
                                        <a href="<?php echo base_url() ?>admin/employees/billingTeam">
                                            <span class="hide-menu">Billing Team</span>
                                        </a>
                                    </li>
                                    <li class="left_nav_team_support ms-hover">
                                        <a href="<?php echo base_url() ?>admin/employees/supportTeam">
                                            <span class="hide-menu">Support Team</span>
                                        </a>
                                    </li>
                                    <li class="ms-hover"><a href="<?php echo base_url() ?>admin/employees/worked_hours">Payroll</a></li>
                                </ul>
                            </li>
                            <li class="left_nav_support">
                                <a href="<?php echo base_url() ?>admin/support" aria-expanded="false">
                                    <i class="mdi mdi-help-circle"></i>
                                    <span class="hide-menu">Support</span>
                                </a>
                            </li>
                            <li class="ms-hover">
                                <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url() ?>CLSign" aria-expanded="false">
                                    <i class="mdi mdi-grease-pencil"></i>
                                    <span class="hide-menu">DocuLynx</span>
                                </a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class=" ms-hover" >
                                        <a href="<?php echo base_url() ?>DocuLynx">Templates</a>
                                    </li>
                                    <li class=" ms-hover">
                                        <a href="<?php echo base_url() ?>DocuLynx?view=waiting">Waiting</a>
                                    </li>
                                    <li class="ms-hover">
                                        <a href="<?php echo base_url() ?>DocuLynx?view=completed">Completed</a>
                                    </li>
                                    <li class="ms-hover">
                                        <a href="<?php echo base_url() ?>DocuLynx?view=voided">Voided</a>
                                    </li>
                                </ul>
                            </li> 
                            <!-- <li class="">
                                <a href="<?php echo base_url() ?>" aria-expanded="false">
                                    <i class="mdi mdi-account-card-details"></i>
                                    <span class="hide-menu">Subscriptions</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url() ?>admin/subscriptions/subscriptionsType" aria-expanded="false">
                                    <i class="mdi mdi-account-card-details"></i>
                                    <span class="hide-menu">Subscriptions Type</span>
                                </a>
                            </li> -->
                            <li class="hide">
                                <a href="<?php echo base_url() ?>admin/coupons" aria-expanded="false">
                                    <i class="mdi mdi-tag-multiple"></i>
                                    <span class="hide-menu">Coupons</span>
                                </a>
                            </li>
                            <li class="left_nav_documents">
                                <a href="<?php echo base_url() ?>admin/documents" aria-expanded="false">
                                    <i class="mdi mdi-folder-account"></i>
                                    <span class="hide-menu">Documents</span>
                                </a>
                            </li>
                            <li class="ms-hover">
                                <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url() ?>admin/billing" aria-expanded="false">
                                    <i class="mdi mdi-newspaper"></i>
                                    <span class="hide-menu">Billing</span>
                                    
                                </a>
                                <ul aria-expanded="false" class="collapse" style="">
                                    <li class=" ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/billing">Invoices</a>
                                    </li>
                                    <li class=" ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/authorize/transactions">Transactions</a>
                                    </li>
                                    <li class=" ms-hover" >
                                        <a href="<?php echo base_url() ?>admin/billing/subscriptions">Subscriptions</a>
                                    </li>
                                    <li class=" ms-hover">
                                        <a href="<?php echo base_url() ?>admin/authorize/account_updater">Account Updater</a>
                                    </li>

                                </ul>
                            </li>

                            <li class=" ms-hover">
                                <a href="<?php echo base_url() ?>admin/authorize/plans">
                                    <i class="mdi mdi-library-books"></i>
                                    <span class="hide-menu">Plans</span>
                                </a>
                            </li>
                            <li class="left_nav_reports" >
                                <a href="<?php echo base_url() ?>admin/reports" aria-expanded="false">
                                    <i class="mdi mdi-file-excel"></i>
                                    <span class="hide-menu">Reports</span>
                                </a>
                            </li>
                            <li class="left_nav_reports" >
                                <a href="<?php echo base_url() ?>admin/campaigns" aria-expanded="false">
                                    <i class="mdi mdi-road-variant"></i>
                                    <span class="hide-menu">Campaigns</span>
                                </a>
                            </li>
                            <li class="left_nav_administrators" >
                                <a href="<?php echo base_url() ?>admin/administrators" aria-expanded="false">
                                    <i class="mdi mdi-account-key"></i>
                                    <span class="hide-menu">Administrators</span>
                                </a>
                            </li>

                            <!-- <li>
                                <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-map-marker"></i><span class="hide-menu">With Dropdown</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="map-google.html">Google Maps</a></li>
                                    <li><a href="map-vector.html">Vector Maps</a></li>
                                </ul>
                            </li>  -->
                        </ul>
                    </nav>
                <?php endif?>

                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <?php if ($userdata['login_type'] == 'Administrator'): ?>
                    <!-- item-->
                    <a href="https://admin.creditlynx.com/admin/profile" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                    <!-- item-->
                    <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <?php endif ?>
                    
                <!-- item-->
                <?php if ($userdata['login_folder'] == 'employees'): ?>
                    <a href="<?php echo base_url() ?>employees/login/" style="width: 100%;" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                <?php endif?>
                 <?php if ($userdata['login_folder'] == 'admins'): ?>
                    <a href="<?php echo base_url() ?>admin/login/logout" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                <?php endif?>

            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

<script>
    $(document).ready(function(){




    });

</script>