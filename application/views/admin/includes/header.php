<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
    <title>Credit Lynx | Administrator Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url('assets/css/colors/green-dark.css') ?>" id="theme" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/datatables/media/css/dataTables.bootstrap4.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/select2/dist/css/select2.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/emoji_plugin/css/emoji.css') ?>" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/dropify/dist/css/dropify.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-confirm.css') ?>">

    <link href="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css') ?>" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css') ?>">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/plugins/popper/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets/js/waves.js') ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets/js/sidebarmenu.js') ?>"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js') ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets/js/custom.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>
    <!-- jQuery file upload -->
    <script src="<?php echo base_url('assets/plugins/dropify/dist/js/dropify.min.js') ?>"></script>
    <!-- This is data table -->
    <script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/select2/dist/js/select2.full.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sweetalert/sweetalert.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/plugins/toast-master/js/jquery.toast.js') ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js') ?>"></script>
    <script src="<?php echo base_url('assets/emoji_plugin/js/config.js') ?>"></script>
    <script src="<?php echo base_url('assets/emoji_plugin/js/util.js') ?>"></script>
    <script src="<?php echo base_url('assets/emoji_plugin/js/jquery.emojiarea.js') ?>"></script>
    <script src="<?php echo base_url('assets/emoji_plugin/js/emoji-picker.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-confirm.js') ?>"></script>
    <!-- <script src="<?php echo base_url() ?>/assets/js/ColReorderWithResize.js"></script> -->

    <script src="<?php echo base_url('assets/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.js') ?>"></script>

    <!-- <script src="js/toastr.js"></script> -->
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <!-- <script src="<?php echo base_url() ?>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script> -->
    <script>

    </script>
    <style>
        .btn-success:not(:disabled):not(.disabled).active, .btn-success:not(:disabled):not(.disabled):active, .show>.btn-success.dropdown-toggle {
            background-color: #1096a7;
            border-color: #63d6e4;
        }

        .btn-success:not(:disabled):not(.disabled).active:focus, .btn-success:not(:disabled):not(.disabled):active:focus, .show>.btn-success.dropdown-toggle:focus {
            box-shadow: 0 0 0 0.2rem #34d2e685;
        }
        .btn-minimize {
            cursor: pointer;
        }
        .right-sidebar-fullscreen {
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw !important;
        }

        .right-sidebar-loader .circular {
            margin-right: auto;
            margin-left: auto;;
            margin-top: 0px;
            margin-bottom: 0px;
            top: 250px;
        }
        .select2-dropdown {
            z-index: 1225;
        }
        .swal2-container {
            z-index: 1250;
        }
        .modal {
            z-index: 1150;
        }
        .rightLabelDiv {
            position: relative;
        }

        .rightLabel {
            position: absolute;
            top: 8px;
            right: 10px;
            font-size: 10px;
        }
        .mailbox .message-center a {
            padding: 9px 0px;
        }
        .form-control:focus {
            box-shadow: 0 0 0 0.2rem rgba(60, 194, 216, 0.59);
        }

        .topbar ul.dropdown-user li .dw-user-box .u-text .btn:hover {
            background-color: #26c6da;
        }

        .stylish-table tbody tr:hover, .stylish-table tbody tr.active {
            border-left: 4px solid #26c6da !important;
        }

        /*.fa-note {
            line-height: 1.4;
        }*/

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 28px !important;
        }

        .profile-tab li a.nav-link.active, .customtab li a.nav-link.active {
            border-bottom: 2px solid #26c6da !important;
            color: #26c6da !important;
        }

        @font-face {
            font-family: Prestige;
            src: url('<?php echo base_url('assets/fonts/Prestige Signature Script - Demo.ttf') ?>');
        }
        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }
        small {
            font-size: 60%;
        }
        .page-item.active .page-link {
            background-color: #26c6da !important;
            border-color: #26c6da !important;
        }
        .form-group {
            margin-bottom: 20px !important;
        }
        /*.form-control {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }*/
        .control-label {
            margin-bottom: 0px !important;
        }
        .sidebar-nav ul li a:hover {
            color: #00adad !important;
        }
        @media (min-width: 768px) {
            .mini-sidebar .sidebar-nav #sidebarnav>li:hover>a {
                color: #ffffff !important;
            }
        }
        @media (max-width: 768px) {
            .shw-rside {
                width: 100% !important;
            }

            .p-l-0 {
                padding-left: 15px !important;
            }

            .p-r-0 {
                padding-right: 15px !important;
            }
        }

        .shw-rside {
            width: 65vw;
        }
        th {
            font-size: 12px !important;
        }
        .select2-container--default .select2-selection--single {
            border-radius: 0px !important;
            border: 1px solid #ced4da !important;
            /*height: 38px !important; */
        }
       /* .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 38px !important;
        }*/
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #67757c !important;
        }
        .pull-right {
            float: right;
        }
        .pull-left {
            float: left;
        }
        .form-control {
            border: 1px solid #ced4da !important;
            padding: .375rem .75rem !important;
            padding-left: 8px !important;
        }
        .floating-labels label {
            top: 8px !important;
            left: 8px !important;
        }
        .floating-labels .focused label {
            top: -19px !important;
            left: 0px !important;
        }
        .fa-note {
            cursor: pointer;
            padding: 3px;
            line-height: 1.4 !important;
            text-align: center;
            border: 1px solid #ced4da;
        }
        .material_date_time {
            border: 1px solid #ced4da;
        }
        .task_update_button {
            width: 45px !important;
        }
        .dropify-wrapper {
            border: none !important;
        }
        .dropify-render > img{
            border-radius: 100% !important;
        }
        li > a:hover,.breadcrumb-item > a:hover,.goToProfile:hover,.paginate_button:hover{
            color: #17a2b8;
        }

        .paginate_button > a:hover {
            color: white !important;
        }

        .user-profile .profile-text a:hover {
            color: #17a2b8 !important;
        }

        .jq-icon-success {
            background-color: #17a2b8;
        }

        .dropdown-item:active {
            background-color: #17a2b8 !important;
            color: white !important;
        }
        .user-profile .profile-text a:active {
            color: white !important;
        }


        tr > td {
            line-height: 28px !important;
            font-size: 12px !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
        }

        .sidebar-nav ul li a.active {
          color: #17a2b8;
        }

        .b-b {
            border-bottom: 1px solid rgba(120, 130, 140, 0.13);
        }

        .hide {
            display: none !important;
        }

        .dataTables_filter {
            float: left !important;
        }

        .pagination , .dataTables_info , .dataTables_filter{
            font-size: 60%;
        }

        .bg-grey {
            background-color: grey !important;
            border-color: grey !important;
        }

        a.bg-grey:hover {
            background-color: #343a40 !important;
            border-color: #343a40 !important;
        }

        .bg-green {
            background-color: #088080 !important;
            border-color: #088080 !important;
        }

        a.bg-green:hover {
            background-color: #047069 !important;
            border-color: #047069 !important;
        }

        a.bg-success:hover {
            background-color: #21b3c6 !important;
            border-color: #21b3c6 !important;
        }

        .fc-event,.fc-event:hover {
            border-color: white !important;
        }

        .fc-event {
            border-radius: 0 !important;
            padding: 3px !important;
        }

        a:hover {
            color: #21b3c6;
        }

        .ui-menu-item-wrapper:hover {
            background-color: #21b3c6 !important;
            border-color: #21b3c6 !important;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #21b3c6 !important;
            color: white;
        }

        a.bg-success:hover,.btn-success:hover {
            color: white !important;
        }

        .btn-default {
            border: 1px solid #ced4da !important;
            background-color: #ced4da;
        }

        .card-primary,.label-primary {
            background-color: #999999 !important;
        }

        .card-dark,.label-inverse {
            background-color: #088080 !important;
        }

        .jq-icon-warning {
            background-color: #dc3545 !important;
        }

        .emoji-menu {
            bottom: 0;
        }

        .emoji-wysiwyg-editor {
            display: block;
            height: 100% !important;
            line-height: 42px !important;
        }

        .emoji-picker-icon {
            top: 15px !important;
            font-size: 25px !important;
            color: grey !important;
        }

        a.text-success:hover ,a.text-success:focus {
        color: #21b3c6 !important;
        }

        .btn-success:not(:disabled):not(.disabled).active, .btn-success:not(:disabled):not(.disabled):active, .show>.btn-success.dropdown-toggle {
            background-color: #21b3c6;
            border-color: #21b3c6;
        }

        .btn-success:not(:disabled):not(.disabled).active:focus, .btn-success:not(:disabled):not(.disabled):active:focus, .show>.btn-success.dropdown-toggle:focus {
            box-shadow: 0 0 0 0.2rem rgba(106, 228, 223, 0.5);
        }


        .dataTables_filter input:focus {
            background-image: linear-gradient(#98fff1, #4dbdf5) , linear-gradient(#d9d9d9, #d9d9d9) !important;
        }

        .swal2-icon.swal2-warning {
            border-color: #26c6da !important;
            color: #26c6da !important;
        }

        .swal2-popup .swal2-styled.swal2-confirm {
            background-color: #21b3c6 !important;
            border-left-color: #21b3c6 !important;
            border-right-color: #21b3c6 !important
        }

        .notify .point{
           background: #c11010!important;
        }
        /*.notify .heartbit{
           background: #c11010!important;
        }*/

    </style>


    <script>
        getPushNotifications();
        function getPushNotifications() {
            $.post('<?php echo base_url('admin/support/getPushNotifications') ?>',
                function(data){
                    data = JSON.parse(data);
                    if (data['support_tickets'].length != 0) { 
                        $('.left_nav_support').find('span').addClass('text-danger');
                        $('.left_nav_support').find('span').html('Support ('+data['support_tickets'].length+')');
                    }

                    if (data['leads'].length != 0) {
                        // console.log(data['leads']);
                        $.each(data['leads'],function(key,value){ 
                            $('.left_nav_leads').find('.has-arrow').find('span').addClass('text-danger');
                            $('.left_nav_leads').find('.has-arrow').find('span').html('Leads ('+data['leads'].length+')');
                        });
                    }
                });
        }
    </script>
</head>