       <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <!--  <footer class="footer">
                Credit Lynx
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

    <?php $this->load->view('admin/clients/style'); ?> 
        <div class="right-sidebar">
            <div class="slimscrollright">
                <div class="rpanel-title"> <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a> 
                    <b class="profile_name"></b> 
                    Profile
                    <span>
                        <i class="ti-close right-side-toggle"></i>
                    </span>
                </div>
                <div class="r-panel-body">
                    <div class="right-sidebar-loader" style="display: none;" >
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                        </svg>
                    </div>
                    <div id="profileSection" class="row">
                        <!-- Column -->
                        <div class="col-lg-9 col-xlg-9 col-md-9 col-sm-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul id="containerMainTabsClients" class="nav nav-tabs profile-tab" role="tablist">
                                    <li id="profile_tab" class="nav-item" style="margin: auto">
                                        <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                            Profile
                                        </a>
                                    </li> 
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_documents-header" class="nav-link " data-toggle="tab" href="#tab_documents" role="tab" aria-selected="false">
                                            Documents
                                        </a>
                                    </li>
                                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                            Billing
                                        </a>
                                    </li>
                                    <?php endif?>

                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_gameplan" role="tab" aria-selected="false">
                                            GamePlan
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_support-header" class="nav-link btnBackToSupportTicketList" data-toggle="tab" href="#tab_support" role="tab" aria-selected="false">
                                            Support <span class="text-danger" id="notif_support_right"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_results" role="tab" aria-selected="false">
                                            Results
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_disputes" role="tab" aria-selected="false">
                                            Disputes
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_creditors" role="tab" aria-selected="false">
                                            Creditors
                                        </a>
                                    </li>
                                </ul>

                                <ul id="containerMainTabsLeads" class="nav nav-tabs profile-tab hide" role="tablist"> 
                                    <li id="profile_tab" class="nav-item" style="margin: auto">
                                        <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                            Profile
                                        </a>
                                    </li>
                                    <li class="nav-item hide" style="margin: auto">
                                        <a id="tab_gameplan-header" class="nav-link " data-toggle="tab" href="#tab_gameplan" role="tab" aria-selected="false">
                                            GamePlan
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_evaluation-header" class="nav-link " data-toggle="tab" href="#tab_evaluation" role="tab" aria-selected="false">
                                            Evaluation
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if ($userdata['login_type'] == 'Sales Team'): ?>
                                        hide
                                    <?php endif ?>" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                            Billing
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_documents-header" class="nav-link " data-toggle="tab" href="#tab_documents" role="tab" aria-selected="false">
                                            Documents
                                        </a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php $data['subscription_type'] = $userdata['subscription_type']?>
                                    <?php if ($userdata['login_type'] == 'Sales Team') {  
                                        $this->load->view('admin/clients/tab_profile_sales_team');
                                    } else {
                                        $this->load->view('admin/clients/tab_profile');
                                    } ?>
                                    <?php $this->load->view('admin/clients/tab_documents')?>
                                    <?php $this->load->view('admin/clients/tab_billing_info', $data)?>
                                    <?php $this->load->view('admin/clients/tab_gameplan')?>
                                    <?php $this->load->view('admin/clients/tab_evaluation')?>
                                    <?php $this->load->view('admin/clients/tab_support')?>
                                    <?php $this->load->view('admin/clients/tab_results')?>
                                    <?php $this->load->view('admin/clients/tab_disputes')?>
                                    <?php $this->load->view('admin/clients/tab_creditors')?>
                                </div>
                            </div>
                            <div class="card" id="containerEmailTextTabs">
                                    
                                                
                                <?php $this->load->view('admin/clients/tab_conversation_history')?>
                            </div>
                        </div>
                        <?php $this->load->view('admin/clients/RightSideDiv')?>
                        <!-- Column -->

                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('admin/clients/Modals')?>
<?php $this->load->view('admin/clients/script')?>
<script>
    $('a[data-action="expand"]').on('click',function(e){
        e.preventDefault();
        $(this).closest('.right-sidebar').find('[data-action="expand"] i').toggleClass('mdi-arrow-expand mdi-arrow-compress');
        $(this).closest('.right-sidebar').toggleClass('right-sidebar-fullscreen');
    });

    $('.right-side-toggle').on('click', function(event) {
        $('.right-sidebar').removeClass('right-sidebar-fullscreen');
        /* Act on the event */
    });

    $('.toSlimScroll').slimScroll({
        position: 'right'
        , size: "5px"
        , height: '570'
        , color: '#dcdcdc'
    });
    $('.toSlimScrollDisputes').slimScroll({
        position: 'right'
        , size: "5px"
        , height: '80vh'
        , color: '#dcdcdc'
    });
    $(".select2").select2();
    $('.material_date').bootstrapMaterialDatePicker({ weekStart: 0, time: false,format: 'MM/DD/YYYY' });
    $('.material_date_time').bootstrapMaterialDatePicker({ format: 'YYYY-MM-DD HH:mm',shortTime: true });
    $('.dropify').dropify();
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    $('.dropify-filename-inner').html('');
    $('#pageTable').removeClass('dataTable');

    $(".phone_format").text(function(i, text) {
        text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
        return text;
    });
    $(".date-inputmask").inputmask("mm/dd/yyyy");
    $(".monthyear-inputmask").inputmask("mm/yyyy");
    $(".datetime-inputmask").inputmask('datetime', {
        mask: "2/1/y h:s:s t\m",
        alias: "mm/dd/yyyy",
        placeholder: "mm/dd/yyyy hh:mm:ss xm",
        separator: '/',
        hourFormat: "12"
    });
    $(".phone-inputmask").inputmask("(999) 999-9999");
    $(".ss-inputmask").inputmask("999-99-9999");


    $('#li_notif,#li_docs_sa,#li_docs,#all_Etext,#all_Notif,#all_Emails,#tbodySupportTicketList').on('click','.to_client_page', function(event) {
        event.preventDefault();
        var type = $(this).attr('type');
        var id = $(this).attr('id');
        client_id = id;
        client_type = type;
        client_joint_id = id;
        localStorage.stay = 'true';
        localStorage.id = id;
        localStorage.type = type;
        localStorage.joint_id = id;
        getProfileDetails(id,id,type);
    });
    $('.to_client_page').on('click', function(event) {
        event.preventDefault();
        var type = $(this).attr('type');
        var id = $(this).attr('id');
        client_id = id;
        client_type = type;
        client_joint_id = id;
        localStorage.stay = 'true';
        localStorage.id = id;
        localStorage.type = type;
        localStorage.joint_id = id;
        getProfileDetails(id,id,type);
    });
    $(document).ready(function(){
        getNotifications();
        function getNotifications() {
            $.post('<?php echo base_url('admin/clients/getNotifications') ?>', function(data){
                data = JSON.parse(data); 
                    
                if (data['text_emails'].length > 0) {
                    var notif_top = $('#li_notif').find('.message-center');
                    notif_top.empty();
                    $.each(data['text_emails'],function(key,value){
                        var type = value.type;
                        var client_id = value.client_id;
                        var message = value.message;
                        var subject = value.subject;
                        var date = moment(value.date).format('MM/DD/YYYY hh:mm:ss A');
                        // var client_type = (value.client_type).toLowerCase();
                        var converted = value.converted;
                        var _type_ = value.client_type == 'Single' ? 'client' : 'joint';

                        if (type == 'email') {

                            var newNotif = '<a href="#" class="to_client_page '+value.client_type+'" type="'+_type_+'" id="'+client_id+'">\
                                            <div style="width: 60px;text-align: center;" class="pull-left"><i style="font-size: 30px;line-height: 2" class="fa fa-lg fa-envelope"></i></div>\
                                            <div class="mail-contnet">\
                                                <h5>'+value.client_name+'</h5> <span class="mail-desc">'+subject+'</span> <span class="time">'+date+'</span> </div>\
                                        </a> ';
                                        notif_top.append(newNotif);
                        } else {
                            var newNotif = '<a href="#" class="to_client_page '+value.client_type+'" type="'+_type_+'" id="'+client_id+'">\
                                            <div style="width: 60px;text-align: center;" class="pull-left"><i style="font-size: 40px;line-height: 1.5" class="fa fa-lg fa-mobile-phone"></i></div>\
                                            <div class="mail-contnet">\
                                                <h5>'+value.client_name+'</h5> <span class="mail-desc">'+message+'</span> <span class="time">'+date+'</span> </div>\
                                        </a> ';
                                        notif_top.append(newNotif);
                        }

                    });
                } else {
                    var notif_top = $('#li_notif').find('.message-center');
                    notif_top.empty();
                    $('#li_notif').find('.notif-top').remove();
                    notif_top.append('<div class="text-center text-muted">No New Sms/Emails found!</div>');
                } 

                if (data['docs'].length > 0) {
                    var notif_top = $('#li_docs').find('.message-center');
                    notif_top.empty();
                    $.each(data['docs'],function(key,value){
                        var type = value.type;
                        var id = type == 'client' ? value.client_id : value.client_joint_id;
                        var category = value.category; 
                        var date_uploaded = moment(value.date_uploaded).format('MM/DD/YYYY hh:mm:ss A');
                        // var client_type = (value.client_type).toLowerCase();
                        
                        if (type == 'client') {
                            var account_access = 'https://admin.creditlynx.com/admin/clients?id='+id+'&type=client';
                        } else {
                            var account_access = 'https://admin.creditlynx.com/admin/leads?id='+id+'&type=joint';
                        }

                         
                        if (category.indexOf('Receipt') !== -1) {
                            
                        } else if (category.indexOf('Invoice') !== -1) {

                        } else {
                            var newNotif = '<a href="#" class="to_client_page" type="'+value.type+'" id="'+id+'">\
                                            <div style="width: 60px;text-align: center;" class="pull-left"><i style="font-size: 30px;line-height: 2" class="fa fa-lg fa-file"></i></div>\
                                            <div class="mail-contnet">\
                                                <h5>'+value.name+'</h5> <span class="mail-desc">'+category+'</span> <span class="time">'+date_uploaded+'</span> </div>\
                                        </a> ';
                                        notif_top.append(newNotif); 
                        }
                    });
                }

                if (data['docs_sa'].length > 0) {
                    var notif_top = $('#li_docs_sa').find('.message-center');
                    notif_top.empty();
                    $.each(data['docs_sa'],function(key,value){
                        var type = value.type;
                        var id = value.client_id;
                        var category = value.category; 
                        var date_uploaded = moment(value.date_uploaded).format('MM/DD/YYYY hh:mm:ss A');
                        // var client_type = (value.client_type).toLowerCase();
                        var page = value.converted == 1 ? 'clients' : 'leads';
                        if (type == 'client') {
                            var account_access = 'https://admin.creditlynx.com/admin/'+page+'?id='+id+'&type=client';
                        } else {
                            var account_access = 'https://admin.creditlynx.com/admin/'+page+'?id='+id+'&type=joint';
                        }
                        var newNotif = '<a href="#" class="to_client_page" type="'+value.type+'" id="'+value.client_id+'">\
                                        <div style="width: 60px;text-align: center;" class="pull-left"><i style="font-size: 30px;line-height: 2" class="fa fa-lg fa-file"></i></div>\
                                        <div class="mail-contnet">\
                                            <h5>'+value.name+'</h5> <span class="mail-desc">'+category+'</span> <span class="time">'+date_uploaded+'</span> </div>\
                                    </a> ';
                                    notif_top.append(newNotif);  
                            

                    });
                }
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });


        }
    });



</script>


</body>

</html>