
<style>   
    .text-right {
        text-align: right;;
    }
    .tableCharts tbody tr td:nth-child(n+2) {
        text-align: center; 
        border-top: 0px;
        border-right: 1px solid #ddd;
        border-left: 1px solid #ddd;
    }
    .tableCharts tbody tr td:nth-child(1) {
        border-left: 1px solid #ddd; 
        color: #3cc;
    }

    .tableCharts thead tr th:nth-child(n+2), .tableCharts tfoot th:nth-child(n+2) {
        text-align: center !important;  
        border: 1px solid #ddd !important;
        width: 150px !important;
        font-weight: 400;
    }


    .tableCharts tbody tr .active { 
        /*border-right: 1px solid #3cc !important;*/
        /*border-left: 1px solid #3cc !important;*/
    }

    .tableCharts thead tr .active {
        background-color: #3cc !important;  
        color: #fff;
    }

    .tableCharts tfoot tr .active {
        background-color: #3cc !important;  
        color: #fff;
    }


    .b-t {
        border-top: 1px solid #ddd;
    }
    .b-l {
        border-left: 1px solid #ddd;
    }
    .b-r {
        border-right: 1px solid #ddd;
    }
    .b-b {
        border-bottom: 1px solid #ddd;
    } 
    .b-t-0 {
        border-top: none;
    }
    .b-l-0 {
        border-left: none;
    }
    .b-r-0 {
        border-right: none;
    }
    .b-b-0 {
        border-bottom: none;
    }
    .b-0 {
        border: none !important;
    }
    .b-a {
        border: 1px solid #ddd !important;
    }
    .rightLabelDiv {
        position: relative;
    }

    .rightLabel {
        position: absolute;
        top: 8px;
        right: 30px;
        font-size: 8px;
    }
    .nextButton {
        border-radius: 25px;background-color: #33cccc;color:white;padding: 10px 120px 10px 120px;border: none;margin-top: 10px; 
    }
    .chartMostPopular {
        font-size: 32px;margin-top: 20px;text-align: center;margin-bottom: 50px;
    }
    .chartPriceSpan {
        font-size: .44em;opacity: .85;letter-spacing: 0px;color: rgb(255, 255, 255);position: relative;bottom: 4px;
    }
    .chartPrice {
        font-size: 32px;font-family: 'Open Sans', sans-serif;font-weight: 300;padding: 13px 0;margin-bottom: 0px;text-align: center;background-color: #33cccc;margin: 0 -15px;color: white;font-size: 34px;letter-spacing: -2px;
    }
    .SAIframes {
        width: 468pt !important;
    }
    .LA_iframeContainer {
        padding: 20px;text-align: center; 
    }
    .LA_toggleIframe {
        position: relative; height: 500px; width: 650pt;;margin: auto;
    }
    .LA_parent {
        cursor: pointer;padding: 15px;background-color: #fbfbfb;margin-bottom: 10px;
    }
    .LA_toggleSpan{
        position: absolute;font-size: 15px;top: 3.6px; 
    }
    .LA_toggleh4 {
        position: relative;margin: 0px;
    }
    #allCheckboxLabel {
        cursor: pointer;padding-right: 15px;margin-bottom: 15px;
    }
    .container_single,.container_joint,.container_single_extra,.container_joint_extra {
        margin: 12px 12px 12px 12px; background: #fff; border: 1px solid #d4d4d4; 
    }
    .chartContainers {
        border: 1px solid #d4d4d4;background: #f2f2f2; 
    }
    .tab-body {
        padding-left: 15px;padding-right: 15px;
    }
    .m-t-30 {
        margin-top: 30px;
    }
    .m-t-20 {
        margin-top: 20px;
    }
    .m-t-10 {
        margin-top: 10px;
    }
    .navTitles {
        margin-bottom: 0px;margin-top:0px;line-height: 47.9px;
    }
    .text-center {
        text-align: center; 
    }
    .chartTitle {
        font-size: 32px;margin-top: 20px;text-align: center;margin-bottom: 20px;
    }
    .swal-icon--warning {
        border: 5px solid red!important;
        display: flex!important;
        -webkit-animation: none!important; 
        animation: none!important; 
    }
    .swal-icon--warning__body, .swal-icon--warning__dot {
        position: absolute!important;
        left: 50%!important;
        background-color: #ff0000!important;
    }
    .swal-text {
        text-align: center;
    }

    .swal-button {
        background: #33cccc;
    }

    .swal-button:not([disabled]):hover {
        background: #33cccc;
        color: white;
        border: none;
    }
    
    .arrow_box {
        position: relative;
        background: #33cccc; 
        
    }
    .arrow_box:after {
        left: 100%;
        top: 63%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(38, 198, 218, 0);
        border-left-color: #33cccc;
        border-width: 23.9px;
        margin-top: -30px;
    }
    .logPass1{
            /*color: white !important;*/
            /*display: none!important;*/
            margin-left: 5px;
    }
    .logPass2{
        /*color: white !important;*/
        /*display: none!important;*/
        margin-left: 5px!;
    }
    @media screen and (max-width: 767px) {
        .logPass1{
            
            margin-left: 0!important;
        }
        .logPass2{
           
            margin-left: none!important;
            margin-top: 9px!important;
        }
    }
    @media screen and (max-width: 1073px) {
        .checkbox_label, .SelectAllLA {
            /*color: white !important;*/
            display: none!important;
        }
    }
    @media screen and (max-width: 425px) {
        .checkbox_label, .SelectAllLA {
            /*color: white !important;*/
            margin-bottom: 37px;
        }
    }

    .last_box:after {
        border-left-color: white !important;
    }

    input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #33cccc;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
 
    .label__checkbox {
      display: none;
    }

    .label__check {
      display: inline-block;
      border-radius: 50%;
      border: 5px solid rgba(0,0,0,0.1);
      background: white;
      vertical-align: middle;
      margin-right: 20px;
      width: 2.5em;
      height: 2.5em;
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: border .3s ease;
      
      i.icon {
        opacity: 0.2;
        font-size: ~'calc(1rem + 1vw)';
        color: transparent;
        transition: opacity .3s .1s ease;
        -webkit-text-stroke: 3px rgba(0,0,0,.5);
      }
      
      &:hover {
        border: 5px solid rgba(0,0,0,0.2);
      }
    }

    .label__checkbox:checked + .label__text .label__check {
      animation: check .5s cubic-bezier(0.895, 0.030, 0.685, 0.220) forwards;
      
      .icon {
        opacity: 1;
        transform: scale(0);
        color: white;
        -webkit-text-stroke: 0;
        animation: icon .3s cubic-bezier(1.000, 0.008, 0.565, 1.650) .1s 1 forwards;
      }
    }

    .my_center {
      position: absolute;
      top: 55%; right: 0;
      transform: translate(50%,-50%);
    }

    @keyframes icon {
      from {
        opacity: 0;
        transform: scale(0.3);
      }
      to {
        opacity: 1;
        transform: scale(1)
      }
    }

    @keyframes check {
      0% {
        width: 1.5em;
        height: 1.5em;
        border-width: 5px;
      }
      10% {
        width: 1.5em;
        height: 1.5em;
        opacity: 0.1;
        background: rgba(0,0,0,0.2);
        border-width: 15px;
      }
      12% {
        width: 1.5em;
        height: 1.5em;
        opacity: 0.4;
        background: rgba(0,0,0,0.1);
        border-width: 0;
      }
      50% {
        width: 2.5em;
        height: 2.5em;
        background: #33cccc;
        border: 0;
        opacity: 0.6;
      }
      100% {
        width: 2.5em;
        height: 2.5em;
        background: #33cccc;
        border: 0;
        opacity: 1;
      }
    }


    .form-control {
        color: #777777 !important;
        font-family: Maven Pro, FontAwesome;
    }

    #sticky_signature { position: absolute; display: block; right: 25px; top: 7.4px; z-index: 9; }

    .dtp .center {
        /*display: none;*/
    }


    .dtp div.dtp-date, .dtp div.dtp-time {
        background: #33cccc;
    }

    .dtp table.dtp-picker-days tr>td>a.selected {
        background: #33cccc;
    }

    .plan_text_slim { font-weight: 300;font-size: 16px;padding: 13px 4px;margin-bottom: 0px;margin-top:0px;color: #999;border-bottom: 1px solid rgba(0, 0, 0, 0.15);}
    .plan_text_bold { font-weight: 500;font-size: 16px;padding: 13px 4px;margin-bottom: 0px;margin-top:0px;color: #999;border-bottom: 1px solid rgba(0, 0, 0, 0.15);}
    .plan_buttons { width: 100%;border-radius: 0px;color: #fff;font-size: 16px;line-height: 18px;padding: 12px 24px;-webkit-border-radius: 0px;background: #3cc;border: 1px solid #27c0c0; }

    .plan_buttons:hover,.plan_buttons:focus,.nextButton:hover {
        border: 1px solid #00a1a1;
        background: #00adad;
        color: #fff;
    }

    .plan_buttons i {margin-left: 8px;margin-right: 0px;font-size: 1.3em}

    .coupon_apply { width: 100%;border-radius: 0px;color: #fff;font-size: 16px;line-height: 18px;padding: 12px 24px;-webkit-border-radius: 0px;background: #3cc;border: 1px solid #27c0c0; }

    .coupon_apply:hover,.coupon_apply:focus {
        border: 1px solid #00a1a1;
        background: #00adad;
        color: #fff;
    }

    .coupon_apply i {margin-left: 8px;margin-right: 0px;font-size: 1.3em}


    .promo:before {
        content: "";
        position: absolute;
        display: block;
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 7px 7px 0;
        border-color: transparent #777777 transparent transparent;
        bottom: -7px;
        left: 0;
    }
    .promo:after {
        content: "";
        position: absolute;
        display: block;
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 7px 7px 0 0;
        border-color: #777777 transparent transparent transparent;
        bottom: -7px;
        right: 0;
    }
    .promo {
        font-size: 12px;
        color: #fff;
        position: absolute;
        top: 85px;
        left: 21px;
        z-index: 1000;
        width: 88.5%;
        margin: 0;
        padding: .625em 17px .75em;
        background: #777777;
        box-shadow: 0 2px 4px rgba(0,0,0,.25);
        border-bottom: 1px solid #777777;
    }   
    @media (max-width: 991px) {
        .leftPC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .middlePC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .rightPC { width:  100% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .leftPC > .chartContainers { width: auto; }
    }

    @media (min-width: 992px) {
        .leftPC{ width:  31.61% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .middlePC{ width:  34.15% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .rightPC{ width:  34.24% !important;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;}
        .leftPC > .chartContainers { width: 307px; }
    }

    @media (max-width: 728px) { 
        #allCheckboxLabel { 
            margin-bottom: 40px !important; 
        }
        .arrow_box:after {
            left: 0%;
            top: 63%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-color: rgba(38, 198, 218, 0);
            border-left-color: #33cccc;
            border-width: 23.9px;
            margin-top: -30px;
        }
    }
</style> 

 

        <input type="hidden" name="profile_client_id">
        <input type="hidden" name="billing_coupon_code">
        <div style="margin-top: 30px" class="row" id="tab-list">
            <div class="col-sm-4 arrow_box text-center" >
                <a href="#contact_info" class="navTitles" style="color: white;text-decoration: none;">Contact Information</a>
            </div>
            <div class="col-sm-4  text-center" >
                <a href="#plan_selection" id="plan_selectionbtn" class="navTitles">Plan Selection</a>
            </div>
            <div class="col-sm-4 last_box text-center" >
                <a href="#legal_agreements" class="navTitles">Legal Agreements</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="tab-content"> 
                <div style="padding-left: 15px;padding-right: 15px" class="tab-body removeBr" id="contact_info">
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-2">
                            <div class="table-responsive">
                                <table class="table tableCharts ">
                                    <thead>
                                        <tr>
                                            <th>Joshua Saubon <!-- <input type="text" class="material_date" placeholder="Choose Your start Date"> --></th>
                                            <th class="active">Exclusive Plan <input type="checkbox"></th>
                                            <th>Premiere Plan <input type="checkbox"></th>
                                            <th>Essentials Plan <input type="checkbox"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="b-b">
                                        <tr>
                                            <td>Disputes</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>Interventions</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>Credit Analysis</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>Dashboard</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>24/7 Support</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>Email/Text Alerts</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td> 
                                        </tr>
                                        <tr>
                                            <td>Education</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td> </td> 
                                        </tr>
                                        <tr>
                                            <td>Inquiry Disputes</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td> </td> 
                                        </tr>
                                        <tr>
                                            <td>Identity Review</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td><i class="fa fa-check"></i></td>
                                            <td> </td> 
                                        </tr>
                                        <tr>
                                            <td>Improvement Analysis</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td> </td>
                                            <td> </td> 
                                        </tr>
                                        <tr>
                                            <td>Cease/Desist</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td> </td>
                                            <td> </td> 
                                        </tr>
                                        <tr>
                                            <td>Financial Tools</td>
                                            <td class="active"><i class="fa fa-check"></i></td>
                                            <td> </td>
                                            <td> </td> 
                                        </tr>  
                                    </tbody>
                                    <tfoot>
                                        <tr style="background-color: #3cc;color: #fff">
                                            <th style="font-weight: 300;border: 1px solid #ddd;">Monthly Fee Begins 
                                                <span style="font-weight: 500;">03/13/2019</span>
                                            </th>
                                            <th style="vertical-align: middle;font-weight: 300;" class="active">$119.00</th>
                                            <th style="vertical-align: middle;">-</th>
                                            <th style="vertical-align: middle;">-</th>
                                        </tr>
                                        <tr>
                                            <td class="b-0" colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td class="b-l" colspan="3">Setup Fee charged on 
                                                <span style="color: #3cc">02/13/2019</span>
                                            </td>
                                            <td class="b-r text-right" style="color: #3cc">$119.00</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" style="background-color: #3cc;color: #fff;border: 1px solid #3cc">Total Due Today</td>
                                            <td style="background-color: #3cc;color: #fff;border: 1px solid #3cc" class="text-right">$119.00</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>    
                        <div class="col-sm-8 col-md-offset-2">
                            <h3>Contact Information</h3>
                            <label style="font-weight: 300">We will be using your contact information to communicate with you regarding your online dashboard. We respect your privacy and will not spam you or sell your information to anyone.</label>
                        </div>
                        <div class="col-sm-12 col-md-8 col-md-offset-2 rightLabelDiv">
                            <label class="rightLabel">Full Name</label>
                            <input tabindex="1" name="profile_name" type="text" class="form-control" >
                        </div>
                        <div class="rightLabelDiv m-t-10 col-sm-12 col-md-8 col-md-offset-2 removeP"> 
                            <label class="rightLabel">Email</label>
                            <input tabindex="2" name="profile_email_address" type="text" class="form-control">
                           
                        </div>
                        <div class="rightLabelDiv m-t-10 col-sm-12 col-md-5 col-md-offset-2"> 
                            <label class="rightLabel">Phone</label>
                            <input tabindex="3" name="profile_cell_phone" type="text" class="form-control phone-inputmask" > 
                        </div> 
                        <div class="rightLabelDiv m-t-10 col-sm-12 col-md-3"> 
                            <label class="rightLabel">Carrier</label>
                            <!-- <input tabindex="4" name="profile_cell_phone" type="text" class="form-control phone-inputmask" >  -->
                            <select tabindex="4" name="profile_carrier" class="form-control">
                                <option value="0">Select Carrier</option>
                                <option value="ATT">ATT</option>
                                <option value="Boost">Boost</option>
                                <option value="Cricket">Cricket</option>
                                <option value="Metro PCS">Metro PCS</option>
                                <option value="Simple">Simple</option>
                                <option value="Sprint">Sprint</option>
                                <option value="TMobile">TMobile</option>
                                <option value="Verizon">Verizon</option>
                                <option value="Virgin">Virgin</option> 
                                <option value="Landline">Landline</option>
                            </select>
                        </div> 
                         


                        <div class="rightLabelDiv m-t-10 col-sm-5 col-md-offset-2">
                            <label class="rightLabel">Address</label>
                            <input tabindex="4" name="profile_address" type="text" class="form-control" >
                        </div>
                        <div class="rightLabelDiv m-t-10 col-sm-3">
                            <label class="rightLabel">State</label> 
                            <select tabindex="6" name="profile_state_province" class="form-control" aria-required="true" aria-invalid="false">
                                <option value="">Select State</option>
                                <option value="Alabama">Alabama</option>
                                <option value="Alaska">Alaska</option>
                                <option value="Arizona">Arizona</option>
                                <option value="Arkansas">Arkansas</option>
                                <option value="California">California</option>
                                <option value="Colorado">Colorado</option>
                                <option value="Connecticut">Connecticut</option>
                                <option value="Delaware">Delaware</option>
                                <option value="Florida">Florida</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Hawaii">Hawaii</option>
                                <option value="Idaho">Idaho</option>
                                <option value="Illinois">Illinois</option>
                                <option value="Indiana">Indiana</option>
                                <option value="Iowa">Iowa</option>
                                <option value="Kansas">Kansas</option>
                                <option value="Kentucky">Kentucky</option>
                                <option value="Louisiana">Louisiana</option>
                                <option value="Maine">Maine</option>
                                <option value="Maryland">Maryland</option>
                                <option value="Massachusetts">Massachusetts</option>
                                <option value="Michigan">Michigan</option>
                                <option value="Minnesota">Minnesota</option>
                                <option value="Mississippi">Mississippi</option>
                                <option value="Missouri">Missouri</option>
                                <option value="Montana">Montana</option>
                                <option value="Nebraska">Nebraska</option>
                                <option value="Nevada">Nevada</option>
                                <option value="New Hampshire">New Hampshire</option>
                                <option value="New Jersey">New Jersey</option>
                                <option value="New Mexico">New Mexico</option>
                                <option value="New York">New York</option>
                                <option value="North Carolina">North Carolina</option>
                                <option value="North Dakota">North Dakota</option>
                                <option value="Ohio">Ohio</option>
                                <option value="Oklahoma">Oklahoma</option>
                                <option value="Oregon">Oregon</option>
                                <option value="Pennsylvania">Pennsylvania</option>
                                <option value="Rhode Island">Rhode Island</option>
                                <option value="South Carolina">South Carolina</option>
                                <option value="South Dakota">South Dakota</option>
                                <option value="Tennessee">Tennessee</option>
                                <option value="Texas">Texas</option>
                                <option value="Utah">Utah</option>
                                <option value="Vermont">Vermont</option>
                                <option value="Virginia">Virginia</option>
                                <option value="Washington">Washington</option>
                                <option value="West Virginia">West Virginia</option>
                                <option value="Wisconsin">Wisconsin</option>
                                <option value="Wyoming">Wyoming</option>
                            </select>
                        </div>
                        <div class="rightLabelDiv m-t-10 col-sm-5 col-md-offset-2"> 
                            <label class="rightLabel">City</label>
                            <input tabindex="5" name="profile_city" type="text" class="form-control" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
                        </div>
                        <div class="rightLabelDiv m-t-10 col-sm-3">
                            <label class="rightLabel">Zip</label>
                            <input tabindex="7" name="profile_zip_postal_code" type="text"  class="form-control zip-inputmask" >
                        </div>



                        <div class="m-t-30 col-sm-5 col-md-offset-2">
                            <medium>Would you like to <b>save money</b> and take advantage of our couples pricing discount by signing up your partner, friend, or family member?</medium>
                        </div>
            
            
                        <div class="m-t-30 col-sm-2">
                            <div class="removeBr removeP"> 
                                <input id="r_yes" type="radio" name="client_type" value="Joint">
                                <label for="r_yes">Yes</label>
                                <input id="r_no" type="radio" name="client_type" value="Single">
                                <label for="r_no">No</label>
                            </div>
                            
                        </div> 
                        <div class="rightLabelDiv container_joint_inputs m-t-30 hide col-md-8 col-md-8 col-md-offset-2">
                            <label class="rightLabel">Full Name</label>
                            <input tabindex="9" name="profile_joint_name" type="text" class="form-control" >
                        </div>
                        <div class="rightLabelDiv m-t-10 container_joint_inputs hide col-sm-12 col-md-8 col-md-offset-2"> 
                            <label class="rightLabel">Email</label>
                            <input tabindex="10" name="profile_joint_email_address" type="text" class="form-control" >
                             
                        </div>
                        <div class="rightLabelDiv container_joint_inputs hide col-sm-12 col-md-5 col-md-offset-2"> 
                            <label class="rightLabel">Phone</label>
                            <input tabindex="11" name="profile_joint_cell_phone" type="text" class="form-control phone-inputmask" > 
                        </div>  

                        <div class="rightLabelDiv container_joint_inputs hide col-sm-12 col-md-3"> 
                            <label class="rightLabel">Carrier</label>
                            <select tabindex="11" name="profile_joint_carrier" class="form-control">
                                <option value="0">Select Carrier</option>
                                <option value="ATT">ATT</option>
                                <option value="Boost">Boost</option>
                                <option value="Cricket">Cricket</option>
                                <option value="Metro PCS">Metro PCS</option>
                                <option value="Simple">Simple</option>
                                <option value="Sprint">Sprint</option>
                                <option value="TMobile">TMobile</option>
                                <option value="Verizon">Verizon</option>
                                <option value="Virgin">Virgin</option> 
                                <option value="Landline">Landline</option>
                            </select>  
                        </div>  
                             
                        <div class="m-t-30 col-sm-12">
                            <div class="text-center">
                                <button value="#plan_selection" type="button" class="nextButton"><span>Next</span></button>    
                            </div>
                        </div>

                        <div style="line-height: 0.8" class="m-t-20 col-sm-8 col-md-offset-2">
                            <small>By clicking "Next" you agree by electronic signature that you are at least 18 years of age and agree to be contacted about credit repair or credit repair marketing by email, SMS/MMS text and/or contacted at the number(s) provided, whether dialed manually or by autodialer, by a live or pre-recorded Credit Specialist, and agree to the <u>Privacy Policy</u> and <u>Terms and Conditions</u>. To sign up without providing consent to be contacted, call 877-820-2425.</small>
                        </div>
                            
                    </div>
                        
                </div>
                    

                <div class="tab-body hide" id="plan_selection">
                    <div class="row">
                        <div class="normalPricingChart">
                            <div class=" leftPC removeBr m-t-30">
                            
                                <div class="chartContainers " >
                                    <div class="container_single" >
                                        <h2 class="chartTitle">Essentials 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$69 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim">One Time Processing Fee of $149</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" type="button" plan="Essentials" setup_fee="149" monthly_fee="69" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartTitle">Essentials Plus
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$119 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $259</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Essentials Plus" setup_fee="259" monthly_fee="119" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                                
                            </div>
                            <div class=" middlePC removeBr">
                                
                                <div class="chartContainers">
                                    <div class="container_single" >
                                        <h2 class="chartMostPopular">Premiere 
                                            <div class="promo">Our Most Popular Plan</div>
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$99 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_bold" >Unlimited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Credit Education</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $149</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Premiere" setup_fee="149" monthly_fee="99" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartMostPopular">Premiere Plus
                                            <div class="promo">Our Most Popular Plan</div>
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$179 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_bold" >Unlimited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Credit Education</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $259</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Premiere Plus" setup_fee="259" monthly_fee="179" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
 
                                </div>
                                
                            </div>
                            <div class=" rightPC removeBr" style="margin-top: 30px;">
                                
                                <div class="chartContainers">
                                    <div class="container_single" >
                                        <h2 class="chartTitle">Exclusive 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$139 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_bold" >Unlimited Credit Bureau Disputes</div>
                                            <div class="plan_text_bold" >Unlimited Creditor Interventions</div>
                                            <div class="plan_text_bold" >Financial Management Tools</div>
                                            <div class="plan_text_bold" >Credit Improvement Analysis</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Credit Education</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" style="border: none;">One Time Processing Fee of $149</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Exclusive" setup_fee="149" monthly_fee="139" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartTitle">Exclusive Plus 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$249 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_bold" >Unlimited Credit Bureau Disputes</div>
                                            <div class="plan_text_bold" >Unlimited Creditor Interventions</div>
                                            <div class="plan_text_bold" >Financial Management Tools</div>
                                            <div class="plan_text_bold" >Credit Improvement Analysis</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Credit Education</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" style="border: none;">One Time Processing Fee of $259</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Exclusive Plus" setup_fee="259" monthly_fee="249" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div> 
 
                                </div>
                                
                            </div>
                        </div>
                        <div class="extraPricingChart hide">
                            <div class="leftPC removeBr">
                                <div class="chartContainers">
                                    <div class="container_single hide" >
                                        <h2 class="chartTitle">Optimize 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$29 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Personalized Credit Coaching</div>
                                            <div class="plan_text_slim" >Credit Improvement Analysis</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $49</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" type="button" plan="Optimize" setup_fee="49" monthly_fee="29" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartTitle">Optimize Plus 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$59 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Personalized Credit Coaching</div>
                                            <div class="plan_text_slim" >Credit Improvement Analysis</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $99</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" type="button" plan="Optimize Plus" setup_fee="99" monthly_fee="59" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class=" middlePC removeBr">
                                
                                <div class="chartContainers">  
                                    <div class="container_single hide" >
                                        <h2 class="chartMostPopular">Monitor 
                                            <!-- <div class="promo">Our Most Popular Plan</div> -->
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$29 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Profile Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Financial Management Tools</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $49</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Monitor" setup_fee="49" monthly_fee="29" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartTitle">Monitor Plus
                                            <!-- <div class="promo">Our Most Popular Plan</div> -->
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$59 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Profile Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Financial Management Tools</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $99</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Monitor Plus" setup_fee="99" monthly_fee="59" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class=" rightPC removeBr" style="margin-top: 30px;">
                                
                                <div class="chartContainers"> 

                                    <div class="container_single hide" >
                                        <h2 class="chartTitle">Limited 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$39 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim"  >One Time Processing Fee of $69</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Limited" setup_fee="69" monthly_fee="39" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container_joint hide" >
                                        <h2 class="chartTitle">Limited Plus 
                                        </h2>
                                        <hr style="margin: 1px 5px 0px 5px">
                                        <h2 class="chartPrice">$79 
                                            <span class="chartPriceSpan">per month</span>
                                        </h2> 
                                        <div style="padding: 0px; margin: 20px 15px">
                                            <div class="plan_text_slim" >Limited Credit Bureau Disputes</div>
                                            <div class="plan_text_slim" >Limited Creditor Interventions</div>
                                            <div class="plan_text_slim" >Credit Dispute Analysis</div>
                                            <div class="plan_text_slim" >Secure Payment System</div>
                                            <div class="plan_text_slim" >Personal Online Dashboard</div>
                                            <div class="plan_text_slim" >24/7 Access and Team Support</div>
                                            <div class="plan_text_slim" >Email and Text Message Updates</div>
                                            <div class="plan_text_slim" >One Time Processing Fee of $139</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim">&nbsp;</div>
                                            <div class="plan_text_slim" style="border: none;">&nbsp;</div>
                                            <div class="text-center" style="margin-top: 20px">
                                                <button type="button" plan="Limited Plus" setup_fee="139" monthly_fee="79" class="plan_buttons"><span>Let's Start</span> <i class="fa fa-angle-double-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="plan_selection_inputs removeBr hide">
                            <div id="coupon_codeDIV" style="margin-top: 30px;display: flex;" class="col-sm-8 col-md-offset-2">
                                <!-- <label style="margin-right: 5px;line-height: 34px;width: 60%">Have a promo code?</label> 
                                <input  tabindex="2000" name="coupon_code" type="text" class="form-control" placeholder="Apply Code">
                                <input  tabindex="2000" name="coupon_code" type="button" class="plan_buttons" placeholder="Apply" style="height: 34px">  --> 
                                <div class="row" style="width: 100%; padding: 0px; margin: 0px; margin-bottom: -18px;">
                                    <div class="col-md-3" style="padding: 0;line-height: 34px;">
                                        <label style="width: 100%; margin-top: 0px; padding: 0;"> Have a promo code?</label> 
                                    </div>
                                    <div class="col-md-7">
                                        <input style="width: 100%;" name="coupon_code" type="text" class="form-control" placeholder="Apply Code">
                                    </div>
                                    <div class="col-md-2">
                                        <button id="coupon_sub" type="button" class="coupon_apply" style="height: 34px;padding-top: 6px;height: 34px;padding-top: 6px;padding-left: 0px;margin: 0;padding-right: 0;"><span>Apply</span> </button>
                                            

                                    </div>
                                </div> 
                            </div>
                            
                            <div style="margin-top: 15px" class="col-sm-8 col-md-offset-2">
                                <small class="errorValidateCoupon text-danger hide"></small>
                                <input type="hidden" name="billing_plan" class="billing_plan" />
                                <input type="hidden" name="billing_setup_fee" class="billing_setup_fee" />
                                <input type="hidden" name="billing_monthly_fee" class="billing_monthly_fee" />
                            </div>
                            <div class="col-sm-8 col-md-offset-2 offset-sm-2" style="margin-top: 30px">
                                <div style="border: 1px solid #d4d4d4">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 style="margin: 0px;padding: 9.5px">Setup Fee <B>$<span class="billing_setup_fee billing_setup_fee_display"></span> <span class="billing_setup_fee_coupon hide"></span></B></h4>
                                        </div>
                                        <div class="col-sm-6 removeBr" style="padding-top: 5px;padding-right: 20px !important;">
                                            <input type="text"  name="billing_paid" class="form-control material_date" placeholder="Choose Your Start Date">
                                            <!-- onfocus="(this.type='date')" -->
                                        </div>
                                    </div>
                                </div>
                                <div style="border: 1px solid #d4d4d4;border-top: 0px">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <h4 style="margin: 0px;padding: 9.5px;">Monthly Fee <b>$<span class="billing_monthly_fee"></span></b></h4>
                                        </div>
                                        <div class="col-sm-6 removeBr" style="padding-top: 5px;padding-right: 20px !important">
                                            <input readonly type="text"  name="billing_monthly_due" class="form-control " placeholder="Monthly Date Will Populate">
                                            <!-- onfocus="(this.type='date')" -->
                                        </div>
                                    </div>
                                </div>
                                <!-- <div style="border: 0px solid #d4d4d4;border-top: 0px">
                                    <div class="row">
                                        <div class="col-sm-12" style="padding-left: 20px;padding-right: 20px;padding-top: 10px">
                                            <small><b>Your monthly fee is not a prepaid service.</b> Your account is billed in arrears (after services have been performed) similar to how a utility bill is charged which covers for last month's services.</small>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-sm-8 col-md-offset-2"> 
                                <h3 class="">Payment Information</h3>
                                <div class="">
                                    <label style="font-weight: 300;">Pay securely with a Visa, MasterCard, or American Express. Your setup fee covers the creation of your online dashboard and entry of all your credit acconts. <b>Your monthly fee is not a prepaid payment of services.</b> Your account is billed in arrears (after services have been performed) similar to how utility bill is charged.</label>
                                </div>   
                            </div>

                            <div class="m-t-10 col-sm-8 col-md-offset-2">
                                <label>Cardholder Name</label>
                                <input tabindex="31" name="billing_card_holder" type="text" class="form-control" >
                            </div>
                            <div class="m-t-10 col-sm-8 col-md-offset-2">
                                <label>Card Number</label>
                                <input tabindex="20" name="billing_card_number" type="text" class="form-control" >
                            </div>
                            <div class="m-t-10 col-md-offset-2 col-sm-2"> 
                                <label >Expiration</label>
                                <input tabindex="21" maxlength="5" name="billing_card_expiration" type="text" class="form-control expiration-inputmask" placeholder="mm/dd"> 
                                 
                            </div>
                            <div class="m-t-10 col-sm-3"> 
                                <label >Security Code</label>
                                <input tabindex="21" name="billing_cvv_code" type="text" class="form-control" > 
                            </div> 
                            <div class="m-t-10  col-sm-3"> 
                                <small>Is the billing address the same?</small>
                                <div class="removeBr">
                                    <input id="s_yes" type="radio" name="same_billing_contact" value="Yes">
                                    <label for="s_yes">Yes</label>
                                    <input id="s_no" type="radio" name="same_billing_contact" value="No">
                                    <label for="s_no">No</label> 
                                </div> 
                            </div> 
                            <div class="col-sm-8 col-md-offset-2">
                                <div><small><i id="billing_info_container"></i></small></div> 
                            </div>
                            <!-- <div class="container_billing_info removeBr hide col-sm-8 col-md-offset-2" style="padding-top: 10px">
                                <label>Cardholder Name</label>
                                <input tabindex="31" name="billing_card_holder" type="text" class="form-control" >
                            </div> -->
                            <div class="m-t-10 container_billing_info removeBr hide col-sm-5 col-md-offset-2">  
                                <label>Address</label>
                                <input tabindex="32" name="billing_address" type="text" class="form-control" >
                                <label style="margin-top: 10px">State</label> 
                                <select tabindex="34" name="billing_state" class="form-control" aria-required="true" aria-invalid="false">
                                    <option value="Alabama">Alabama</option>
                                    <option value="Alaska">Alaska</option>
                                    <option value="Arizona">Arizona</option>
                                    <option value="Arkansas">Arkansas</option>
                                    <option value="California">California</option>
                                    <option value="Colorado">Colorado</option>
                                    <option value="Connecticut">Connecticut</option>
                                    <option value="Delaware">Delaware</option>
                                    <option value="Florida">Florida</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Hawaii">Hawaii</option>
                                    <option value="Idaho">Idaho</option>
                                    <option value="Illinois">Illinois</option>
                                    <option value="Indiana">Indiana</option>
                                    <option value="Iowa">Iowa</option>
                                    <option value="Kansas">Kansas</option>
                                    <option value="Kentucky">Kentucky</option>
                                    <option value="Louisiana">Louisiana</option>
                                    <option value="Maine">Maine</option>
                                    <option value="Maryland">Maryland</option>
                                    <option value="Massachusetts">Massachusetts</option>
                                    <option value="Michigan">Michigan</option>
                                    <option value="Minnesota">Minnesota</option>
                                    <option value="Mississippi">Mississippi</option>
                                    <option value="Missouri">Missouri</option>
                                    <option value="Montana">Montana</option>
                                    <option value="Nebraska">Nebraska</option>
                                    <option value="Nevada">Nevada</option>
                                    <option value="New Hampshire">New Hampshire</option>
                                    <option value="New Jersey">New Jersey</option>
                                    <option value="New Mexico">New Mexico</option>
                                    <option value="New York">New York</option>
                                    <option value="North Carolina">North Carolina</option>
                                    <option value="North Dakota">North Dakota</option>
                                    <option value="Ohio">Ohio</option>
                                    <option value="Oklahoma">Oklahoma</option>
                                    <option value="Oregon">Oregon</option>
                                    <option value="Pennsylvania">Pennsylvania</option>
                                    <option value="Rhode Island">Rhode Island</option>
                                    <option value="South Carolina">South Carolina</option>
                                    <option value="South Dakota">South Dakota</option>
                                    <option value="Tennessee">Tennessee</option>
                                    <option value="Texas">Texas</option>
                                    <option value="Utah">Utah</option>
                                    <option value="Vermont">Vermont</option>
                                    <option value="Virginia">Virginia</option>
                                    <option value="Washington">Washington</option>
                                    <option value="West Virginia">West Virginia</option>
                                    <option value="Wisconsin">Wisconsin</option>
                                    <option value="Wyoming">Wyoming</option>
                                </select>
                            </div>
                            <div class="m-t-10 container_billing_info removeBr hide col-sm-3">  
                                <label>City</label>
                                <input tabindex="33" name="billing_city" type="text" class="form-control" >
                                <label class="m-t-10">Zip</label>
                                <input tabindex="35" name="billing_zip" type="text" class="form-control" >
                            </div> 

                            
                        </div> 
                        
                    </div> 
                    <div class="row"> 
                        <div class="removeBr">   
                            <div style="line-height: 0.8" class="m-t-20 col-sm-12 col-md-8 col-md-offset-2 text-center">
                                <button value="#legal_agreements" type="button" class="nextButton"><span>Next</span></button> 
                            </div>
                            <div style="line-height: 0.8" class="m-t-20 col-sm-12 col-md-8 col-md-offset-2">
                                <small>By clicking "Next" you agree by electronic signature that the information provided is accurate and understand that it will be used for the Service Agreement. Unless requested otherwise, you understand that the billing information provided will be used by Credit Lynx to charge for Credit Lynx's services only and agree to the <u>Billing Policy</u>, <u>Refund Policy</u>, and <u>Cancellation Policy</u>.</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-body hide removeBr" id="legal_agreements">
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-2">
                            <div id="containerCredentialsSingle" class="removeBr"> 
                                <h3>Login Information for <span class="profile_name"></span></h3>
                                <span style="width: 100%!important;">Please create a Username and Password for your online  dashboard. Passwords must be at least 8 characters and contain one capital letter and one number.</span>
                                <div class="row">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12">
                                        <input type="text" name="username" class="form-control profile_email_address" placeholder="&#xf0e0; Email">
                                        <small id="errorUsername" class=" hide text-danger">Username already exist!</small>
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6 " style="margin-bottom: 9px;">
                                        <input type="password" name="password" class="form-control" placeholder="&#xf084; Password">
                                    </div> 
                                    <div class="col-sm-6 ">
                                        <input type="password" name="retype_password" class="form-control" placeholder="&#xf084; Retype Password">
                                    </div> 
                                </div> 
                            </div>
                            <div id="containerCredentialsJoint" class=" hide">
                                <h3>Login Information for <span class="profile_joint_name"></span></h3> 
                                <span>Please create a Username and Password for your online dashboard. Passwords must be at least 8 characters and contain one capital letter and one number</span>
                                <div class="row">
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12">
                                        <input type="text" name="joint_username" class="form-control profile_joint_email_address" placeholder="&#xf0e0; Email">
                                        <small id="errorJointUsername" class=" hide text-danger">Username already exist!</small>
                                    </div>  
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6" style="margin-bottom: 9px;" >
                                        <input type="password" name="joint_password" class="form-control" placeholder="&#xf084; Password">
                                    </div> 
                                    <div class="col-sm-6" >
                                        <input type="password" name="joint_retype_password" class="form-control" placeholder="&#xf084; Retype Password">
                                    </div> 
                                </div>
                            </div>

                            <div id="containerCredentialsSingle" class="removeBr"> 
                                <h3 class="">Obtaining Your Credit Reports</h3>
                                <div class="">
                                    <label style="font-weight: 300;">We will need a recent copy of your credit reports. If you do not have a copy, we will send you an email with easy instructions on how to obtain your reports from resources that do not affect your scores before getting started.</label>
                                </div>  
                                <div class="row removeBr m-t-10">
                                    
                                    <div class="col-sm-12  m-t-10">
                                        <input id="obtain_copy_yes" type="radio"  value="Yes">
                                        <span for="obtain_copy_yes">Yes</span>
                                        <span>, I understand that I am required to obtain/provide a copy of my credit reports before getting started.</span>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="m-t-20">
                                <h3>Review and Accept Legal Agreements</h3>
                                 
                                <div id="allCheckboxLabel" >
                                    <h4 class="LAcheckBubble LA_toggleh4" > 
                                        <i style="color: transparent;">dummy text</i> 
                                        <span  style="position: absolute;top: 3.6px;left: 25px;color: #999">Check the bubble to the right of each agreement or select all</span> 
                                        <span class="SelectAllLA" style="position: absolute;top: 3.6px;right: 34px">Select All</span>
                                        <span class="pull-right">
                                            <div class="my_center">
                                                  <label class="label">
                                                    <input id="inputSelectAllHidden"  class="label__checkbox sample" type="checkbox" />
                                                    <span class="label__text">
                                                      <span id="inputSelectAll" class=" label__check">
                                                        <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                      </span>
                                                    </span>
                                                  </label>
                                                </div>
                                        </span>
                                    </h4>
                                </div>
                                <div>
                                    <div data-target="#demo3" class="LA_toggle LA_parent" >
                                        <h4 data-target="#demo3"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo3" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo3"  class="LA_toggle LA_toggleSpan" style="left: 25px">ESignature and Record Disclosure</span> 
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px">
                                            Agree and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo3"  class="LA_iframeContainer collapse">
                                        <iframe width="468pt" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" class="LA_toggleIframe";"  src="https://docs.google.com/document/d/e/2PACX-1vSNzpCGHpTlhprTGraYdUHZS_eGLG-qEUvA6VvMnVbUOI6kCnWvn0EbLBDg7h0Uwo6UB4A7s7jSX3B4/pub?embedded=true"></iframe>
                                    </div>
                                </div>
                                <div>
                                    <div data-target="#demo6" class="LA_toggle LA_parent" >
                                        <h4 data-target="#demo6"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo6" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo6"  class="LA_toggle LA_toggleSpan" style="left: 25px">Billing Authorization Form</span> 
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px">
                                            Agree and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo6"  class="LA_iframeContainer collapse">
                                        <iframe width="468pt" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" class="LA_toggleIframe";"  src="https://docs.google.com/document/d/e/2PACX-1vTW5Bb9iXgDJp9eoFxyITkOaZlzhSBCgXlXR_WGxXKwQa-M63FbV2vOPlGOpGRLpuD5al5nWRnMvmIt/pub?embedded=true"></iframe>
                                    </div>
                                </div>
                                
                                <div>
                                    <div data-target="#demo2" class="LA_toggle LA_parent" >
                                        <h4 data-target="#demo2"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo2" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo2"  class="LA_toggle LA_toggleSpan" style="left: 25px">
                                            Consumer Credit Federal Disclosure</span> 
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px">Acknowledge and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo2"  class="LA_iframeContainer collapse">
                                        <iframe width="468pt" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" class="LA_toggleIframe";" src="https://docs.google.com/document/d/e/2PACX-1vTQRlJbejtHsFgRw6rEJxzjHuWV3Mq36Cvd1YEOnfgcRqpSw9tR-TB1dBfmPvUKz_x9WSaxPjX5o8Wx/pub?embedded=true"></iframe>
                                    </div>
                                </div>
                                <div>
                                    <div data-target="#demo4" class="LA_toggle LA_parent hidden" >
                                        <h4 data-target="#demo4"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo4" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo4"  class="LA_toggle LA_toggleSpan" style="left: 25px">
                                            Consumer Credit State Disclosure </span> 
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px">Acknowledge and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo4"  class="LA_iframeContainer collapse">
                                        <iframe width="468pt" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" class="LA_toggleIframe";" src=""></iframe>
                                    </div>
                                </div>
                                <div>
                                    <div data-target="#demo5" class="LA_toggle LA_parent" >
                                        <h4 data-target="#demo5"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo5" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo5"  class="LA_toggle LA_toggleSpan" style="left: 25px">
                                            Service Agreement and Limited Letter of Authorization  </span> 
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px">Agree and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo5"  class="LA_iframeContainer collapse">
                                        <iframe class="LA_toggleIframe SAIframes Exclusive" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vS1259bOXQxNFHnO_f86gBbUZkLRpJaJSO0KIjTNrrEI4AN13k4R4yiDrevHWil-AEJ-ZG3SyQfPEAB/pub?embedded=true"></iframe>
                                        
                                        <iframe class="LA_toggleIframe SAIframes Exclusive_Plus" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vS8SyM7li1DWMOaPMx6qRmf5bK8XP3ge3tKbGEFQp58r-EC4pcxu161TjVXh14axqYM80i9F7CRmbg3/pub?embedded=true"></iframe>
                                        
                                        <iframe class="LA_toggleIframe SAIframes Premiere" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vSrARmJIbp-gXdOyFvmYedxvkR14fgtZzq9K48f3m3GTya_T2d1aPXVkfsmarcr7NOLzgmgXM4zV7gs/pub?embedded=true"></iframe>
                                        
                                        <iframe class="LA_toggleIframe SAIframes Premiere_Plus" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vRdWO1RyYk3yE09NAcCYXlYCAWL-YmkIRrwqCsCJnLhJoDPT_RvER7Vy-Esi47HPVUc1uQXAAO-vjka/pub?embedded=true"></iframe>
                                        
                                        <iframe class="LA_toggleIframe SAIframes Essentials" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vS1259bOXQxNFHnO_f86gBbUZkLRpJaJSO0KIjTNrrEI4AN13k4R4yiDrevHWil-AEJ-ZG3SyQfPEAB/pub?embedded=true"></iframe>
                                        
                                        <iframe class="LA_toggleIframe SAIframes Essentials_Plus" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" src="https://docs.google.com/document/d/e/2PACX-1vQYt73aGcqIPZ3htH_3fwFDe2hRRQP16juwLrSQUpD5jM8ls8gyk4w5qbLUdrsCtpwkOOF7vBWORyVc/pub?embedded=true"></iframe>
                                    </div>
                                    
                                </div>
                                <div>
                                    <div data-target="#demo" class="LA_toggle LA_parent" >
                                        <h4 data-target="#demo"  class="LA_toggle LA_toggleh4" >
                                            <i style="font-size: 28px;" data-target="#demo" class="toggle-icon fa fa-angle-double-right LA_toggle"></i> 
                                            <span data-target="#demo"  class="LA_toggle LA_toggleSpan" style="left: 25px">
                                            Federal Notice of Cancellation</span>
                                            <span class="checkbox_label LA_toggleSpan" style="right: 34px"">Acknowledge and Sign</span>
                                            <span class="pull-right">
                                                <div class="my_center">
                                                      <label class="label">
                                                        <input  class="label__checkbox LAcheckboxes" type="checkbox" />
                                                        <span class="label__text">
                                                          <span class="label__check">
                                                            <i style="font-size: 18px" class="fa fa-check icon"></i>
                                                          </span>
                                                        </span>
                                                      </label>
                                                    </div>
                                            </span>
                                        </h4>
                                    </div>
                                    <div id="demo"  class="LA_iframeContainer collapse">
                                        <iframe width="468pt" marginheight="0" marginwidth="0" allowfullscreen frameborder="0" style="position: relative; height: 500px; width: 650pt;margin: auto;"  src="https://docs.google.com/document/d/e/2PACX-1vSYlVFBvLR0VVTBB3KKdfiqFEdImE9S-jyWuKbPfAAjMaI_mckioZBWF378w09YJOmMzEnQMuXtQmdd/pub?embedded=true"></iframe>
                                    </div> 
                                </div>
                            </div>

                        </div>
                        
                        <div class="col-sm-8 col-md-offset-2">
                            <div id="containerElectricSignatureSingle" style="margin-top: 20px">
                                <h3 style="margin-bottom: 0px">Electronic Signature for <span class="profile_name"></span></h3>
                                <div class="row">
                                    <!-- <div class="col-sm-4 removeBr">
                                        <input type="text" name="" class="profile_name form-control" placeholder="Name">
                                        <small id="sticky_signature">signature</small>
                                    </div> -->
                                    <div class="col-sm-6">
                                        <input type="text" name="" class="profile_name form-control" placeholder="Fullname">
                                    </div>
                                    <div class="col-sm-6" style="line-height: 34px;"> 
                                        <label>Full Name Provided</label>
                                    </div>
                                    <div class="col-sm-6" style="margin-top: 5px">
                                        <input type="text" name="profile_signature1" class="form-control" placeholder="Type Full Name">
                                        <small id="sticky_signature">signature</small>
                                    </div>

                                    <div class="col-sm-6" style="line-height: 34px;margin-top: 5px"> 
                                        <label>Today's Date: <span class="todays_date"></span></label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6" style="margin-top: -5px;">
                                        <input type="text" name="profile_ss" class="ss-inputmask form-control" placeholder="Social Security Number">
                                    </div>
                                    <div class="col-sm-6" style="margin-top: -5px;"> 
                                        <input type="text" name="profile_date_of_birth" class="date-inputmask form-control" placeholder="Date of Birth">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <button value="submitFormSingle" type="button" class="nextButton"><span>Submit</span></button>  
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 20px; line-height: 0.8">
                                        <small>The name you provided is <b class="profile_name"></b>. By clicking this "Submit" button, you are providing your electronic signature and completing the sign up process.</small>
                                        
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="hide m-t-20" id="containerElectricSignatureJoint" >
                                <h3>Electronic Signature for <span class="profile_joint_name"></span></h3>
                                <div class="row m-t-20" >
                                    <div class="col-sm-6 removeBr">
                                        <input type="text" name="" class="profile_joint_name form-control" placeholder="Fullname"> 
                                    </div> 
                                    <div class="col-sm-6" style="line-height: 34px;"> 
                                        <label>Full Name Provided</label>
                                    </div>
                                    <div class="col-sm-6"  style="margin-top: 5px">
                                        <input type="text" name="profile_signature2" class=" form-control" placeholder="Type Full Name">
                                        <small id="sticky_signature">signature</small>
                                    </div>

                                    <div class="col-sm-6"  style="line-height: 34px;margin-top: 5px"> 
                                        <label>Today's Date: <span class="todays_date"></span></label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6"  style="margin-top: -5px;">
                                        <input type="text" name="profile_joint_ss" class="ss-inputmask form-control" placeholder="Social Security Number">
                                    </div>
                                    <div class="col-sm-6"  style="margin-top: -5px;">
                                        <input type="text" name="profile_joint_date_of_birth" class="date-inputmask form-control" placeholder="Date of Birth">
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                        
                                    <div class="text-center">
                                        <button value="submitFormJoint" class="nextButton m-t-20" type="button"><span>Submit</span></button>
                                    </div>  

                                    <div class="col-sm-12 m-t-20" style=" line-height: 0.8">
                                        <small>The name you provided is <b class="profile_joint_name"></b>. By clicking this "Submit" button, you are providing your electronic signature and completing the sign up process</small>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div> 
            </div>   
        </div> 

 
<script>
    

// STARTER
    
    // From Invite
        function getQueryParams(qs) {
            qs = qs.split("+").join(" ");
            var params = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

            while (tokens = re.exec(qs)) {
                params[decodeURIComponent(tokens[1])]
                    = decodeURIComponent(tokens[2]);
            }

            return params;
        }

        var $_GET = getQueryParams(document.location.search); 
        if (typeof($_GET['cname']) != "undefined" && $_GET['cname'] !== null) { 
            var name = $_GET['cname']; 
            var client_id = $_GET['id'];  

            getProfile(client_id);
            function getProfile(client_id) {
                jQuery.post('../wp-custom-functions/get_client_information.php',{client_id,name}, function(data){
                    // console.log(data);
                        data = JSON.parse(data);
                        var client_type = 'Single';
                        jQuery.each(data['profile'],function(key,value){ 
                            jQuery.each(value,function(k,v){
                                jQuery('[name=profile_'+k+']').val(v);  
                                jQuery('.'+k).html(v); 
                                jQuery('.profile_'+k).html(v);
                                jQuery('.profile_'+k).val(v); 
                            })  

                            client_type = value.client_type;
                            if (client_type == 'Joint') {
                                jQuery('.container_joint_inputs').removeClass('hide');
                            } 
                        }); 
                        jQuery.each(data['billing'],function(key,value){ 
                            jQuery.each(value,function(k,v){
                                jQuery('[name=billing_'+k+']').val(v); 
                                jQuery('.'+k).html(v); 
                                jQuery('.billing_'+k).val(v); 
                            }); 
                            


                            // console.log(value.plan);
                            // console.log(client_type);
                            if (value.plan == 'Monitor' || value.plan == 'Monitor Plus' || value.plan == 'Optimize' || value.plan == 'Optimize Plus' || value.plan == 'Limited' || value.plan == 'Limited Plus') { 
                                jQuery('.extraPricingChart').removeClass('hide');
                                jQuery('.normalPricingChart').remove();
                                jQuery('#coupon_codeDIV').addClass('hide');
                                // console.log('extra');
                                if (client_type == 'Joint') { 
                                    // console.log('joint');
                                    jQuery('#r_yes').attr('checked',true);
                                    jQuery('.container_joint').removeClass('hide');
                                    jQuery('.container_single').addClass('hide'); 
                                    jQuery('[value="submitFormSingle"]').html('Next');
                                } else {
                                    // console.log('not joint');
                                    jQuery('#r_no').attr('checked',true);
                                    jQuery('.container_joint').addClass('hide');
                                    jQuery('.container_single').removeClass('hide'); 
                                }
                                
                            } else {
                                // console.log('not extra');

                                jQuery('.extraPricingChart').remove(); 
                                if (client_type == 'Joint') { 
                                    // console.log('joint');
                                    jQuery('#r_yes').attr('checked',true);
                                    jQuery('.container_joint').removeClass('hide');
                                    jQuery('.container_single').addClass('hide');
                                    jQuery('[value="submitFormSingle"]').html('Next');
                                } else {
                                    // console.log('not joint');
                                    jQuery('.container_joint').addClass('hide');
                                    jQuery('.container_single').removeClass('hide');
                                }
                                    
                            } 

                            var plan_buttons = jQuery('.plan_buttons');
                            jQuery.each(plan_buttons,function(k,button){
                                var plan = jQuery(button).attr('plan'); 
                                plan = plan.replace(/ Plus/g,'');
                                var plan_ = (value.plan).replace(/ Plus/g,''); 
                                // console.log(plan + ' = '+plan_);
                                if (plan != plan_ ) { 
                                    jQuery(button).closest('.removeBr').remove(); 
                                } else { 
                                    jQuery(button).closest('.removeBr').css('float','none');
                                    jQuery(button).closest('.removeBr').css('margin','auto');
                                }
                                
                                
                            });

                        });
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        }  else {
            // console.log('new lead');
        }
    
    // CONSTANTS
        jQuery(".date-inputmask").inputmask("mm/dd/yyyy");
        jQuery(".expiration-inputmask").inputmask("99/99");
        jQuery(".phone-inputmask").inputmask("(999) 999-9999");
        jQuery(".ss-inputmask").inputmask("999-99-9999");
        jQuery(".zip-inputmask").inputmask("99999"); 

        jQuery('.todays_date').html(moment().format('MM/DD/YYYY'));

        jQuery(".my-datepicker" ).datepicker({ dateFormat: "mm/dd/yy" }).val(); 
        jQuery('.material_date').bootstrapMaterialDatePicker({ weekStart: 0, time: false,format: 'MM/DD/YYYY' ,minDate : moment().startOf('day')});
        jQuery('.removeBr').find('br').remove();     
        jQuery('.removeP').find('p').remove();     
        jQuery('#legal_agreements').find('h4').closest('div').find('p').remove();  
        jQuery('polygon').remove(); 
        jQuery('#plan_selection').find('p').remove();
         

    // TABS

        jQuery('#tab-list').on('click','a',function(e){
            e.preventDefault();
            if (e.target !== this)
                return; 
            var href = jQuery(this).attr('href');
            if (href == '#plan_selection') {
                var profile_name = jQuery('[name="profile_name"]'); 
                var profile_email_address = jQuery('[name="profile_email_address"]'); 
                var profile_cell_phone = jQuery('[name="profile_cell_phone"]'); 
                var profile_carrier = jQuery('[name="profile_carrier"]'); 
                var profile_address = jQuery('[name="profile_address"]'); 
                var profile_state_province = jQuery('[name="profile_state_province"]'); 
                var profile_city = jQuery('[name="profile_city"]'); 
                var profile_zip_postal_code = jQuery('[name="profile_zip_postal_code"]'); 
                var profile_joint_name = jQuery('[name="profile_joint_name"]'); 
                var profile_joint_email_address = jQuery('[name="profile_joint_email_address"]'); 
                var profile_joint_cell_phone = jQuery('[name="profile_joint_cell_phone"]'); 
                var profile_joint_carrier = jQuery('[name="profile_joint_carrier"]'); 
                var client_type = jQuery('[name="client_type"]');
                if (profile_name.val() == '' || 
                    profile_email_address.val() == '' || 
                    profile_cell_phone.val() == '' || 
                    profile_address.val() == '' || 
                    profile_state_province.val() == '' || 
                    profile_city.val() == '' || 
                    profile_zip_postal_code.val() == '' || 
                    profile_carrier.val() == '') {
                    swal({
                        title: "Contact Information Is Missing!",
                        text: "click \"Ok\" button to continue",
                        icon: "warning",
                        button: "Ok!",
                    });
                }
                profile_name.css('border','1px solid #e6e6e6');
                profile_email_address.css('border','1px solid #e6e6e6');
                profile_cell_phone.css('border','1px solid #e6e6e6');
                profile_carrier.css('border','1px solid #e6e6e6');
                profile_address.css('border','1px solid #e6e6e6');
                profile_state_province.css('border','1px solid #e6e6e6');
                profile_city.css('border','1px solid #e6e6e6');
                profile_zip_postal_code.css('border','1px solid #e6e6e6');
                profile_joint_name.css('border','1px solid #e6e6e6');
                profile_joint_email_address.css('border','1px solid #e6e6e6');
                profile_joint_cell_phone.css('border','1px solid #e6e6e6');
                profile_joint_carrier.css('border','1px solid #e6e6e6');
                
                    if (profile_name.val() == '') {
                        profile_name.css('border','1px solid red');
                        profile_name.focus();
                        return false;
                    }
                    if (profile_email_address.val() == '' || (profile_email_address.val()).indexOf('@') === -1) {
                        profile_email_address.css('border','1px solid red');
                        profile_email_address.focus();
                        return false;
                    }
                    if (profile_cell_phone.val() == '') {
                        profile_cell_phone.css('border','1px solid red');
                        profile_cell_phone.focus();
                        return false;
                    }
                    if (profile_carrier.val() == '') {
                        profile_carrier.css('border','1px solid red');
                        profile_carrier.focus();
                        return false;
                    }
                    if (profile_address.val() == '') {
                        profile_address.css('border','1px solid red');
                        profile_address.focus();
                        return false;
                    }
                    if (profile_state_province.val() == '') {
                        profile_state_province.css('border','1px solid red');
                        profile_state_province.focus();
                        return false;
                    }
                    if (profile_city.val() == '') {
                        profile_city.css('border','1px solid red');
                        profile_city.focus();
                        return false;
                    }
                    if (profile_zip_postal_code.val() == '') {
                        profile_zip_postal_code.css('border','1px solid red');
                        profile_zip_postal_code.focus();
                        return false;
                    }

                if (!jQuery('#r_yes').is(':checked') && !jQuery('#r_no').is(':checked')) {
                    jQuery('[for="r_yes"]').css('color','red');
                    jQuery('[for="r_no"]').css('color','red');
                    return false;
                }

                if (jQuery('[name="client_type"][value="Joint"]').is(':checked')) {
                    if (profile_joint_name.val() == '' || 
                        profile_joint_email_address.val() == '' || 
                        profile_joint_cell_phone.val() == '' || 
                        profile_joint_carrier.val() == '') {
                        swal({
                            title: "Joint Contact Information Is Missing!",
                            text: "click \"Ok\" button to continue",
                            icon: "warning",
                            button: "Ok!",
                        });
                    }
                    if (profile_joint_name.val() == '') {
                        profile_joint_name.css('border','1px solid red');
                        profile_joint_name.focus();
                        return false;
                    }
                    if (profile_joint_email_address.val() == '' || (profile_joint_email_address.val()).indexOf('@') === -1) {
                        profile_joint_email_address.css('border','1px solid red');
                        profile_joint_email_address.focus();
                        return false;
                    }
                    if (profile_joint_cell_phone.val() == '') {
                        profile_joint_cell_phone.css('border','1px solid red');
                        profile_joint_cell_phone.focus();
                        return false;
                    }
                    if (profile_joint_carrier.val() == '') {
                        profile_joint_carrier.css('border','1px solid red');
                        profile_joint_carrier.focus();
                        return false;
                    }
                }

            }

            if (href == '#legal_agreements') {
                if (jQuery('.plan_selection_inputs').hasClass('hide')) {
                    var profile_name = jQuery('[name="profile_name"]').val();
                    var profile_email_address = jQuery('[name="profile_email_address"]').val();
                    var title = '';
                    if (profile_name == '' && profile_email_address == '') {
                        title = 'Contact Information Is Missing!';
                    } else {
                        title = 'Choose a Plan!';
                    }
                    swal({
                        title: title,
                        text: "click \"Ok\" button to continue",
                        icon: "warning",
                        button: "Ok!",
                    });
                    return false;
                }


                var billing_paid = jQuery('[name="billing_paid"]');
                var billing_monthly_due = jQuery('[name="billing_monthly_due"]');
                var billing_card_number = jQuery('[name="billing_card_number"]');
                var billing_card_expiration = jQuery('[name="billing_card_expiration"]');
                var billing_cvv_code = jQuery('[name="billing_cvv_code"]');
                var billing_card_holder = jQuery('[name="billing_card_holder"]');
                var billing_address = jQuery('[name="billing_address"]');
                var billing_state = jQuery('[name="billing_state"]');
                var billing_city = jQuery('[name="billing_city"]');
                var billing_zip = jQuery('[name="billing_zip"]');
                billing_paid.css('border','1px solid #e6e6e6');
                billing_monthly_due.css('border','1px solid #e6e6e6');
                billing_card_number.css('border','1px solid #e6e6e6');
                billing_card_expiration.css('border','1px solid #e6e6e6');
                billing_cvv_code.css('border','1px solid #e6e6e6');
                billing_card_holder.css('border','1px solid #e6e6e6');
                billing_address.css('border','1px solid #e6e6e6');
                billing_state.css('border','1px solid #e6e6e6');
                billing_city.css('border','1px solid #e6e6e6');
                billing_zip.css('border','1px solid #e6e6e6');
                if (billing_paid.val() == '') {
                    billing_paid.css('border','1px solid red');
                    billing_paid.focus();
                    return false;
                }
                if (billing_monthly_due.val() == '') {
                    billing_monthly_due.css('border','1px solid red');
                    billing_monthly_due.focus();
                    return false;
                }
                if (billing_card_number.val() == '') {
                    billing_card_number.css('border','1px solid red');
                    billing_card_number.focus();
                    return false;
                } else {
                    if ((billing_card_number.val()).length < 16) {
                        billing_card_number.css('border','1px solid red');
                        swal({
                            title: "Validation Alert!",
                            text: "Card Number must be at least 16 digits!",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_card_number.focus(); 
                        return false;
                    } 
                }

                var billing_card_expiration_ = (billing_card_expiration.val()).replace('_','');
                billing_card_expiration_ = billing_card_expiration_.replace('/','');
                if (billing_card_expiration.val() == '') {
                    billing_card_expiration.css('border','1px solid red');
                    billing_card_expiration.focus();
                    return false;
                } else {
                    if ((billing_card_expiration_).toString().length < 4) {
                        billing_card_expiration.css('border','1px solid red');
                        swal({
                            title: "Validation Alert!",
                            text: "Card Expiration must be at least 4 digits!",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_card_expiration.focus();
                        return false; 
                    } 
                }
                if (billing_cvv_code.val() == '') {
                    billing_cvv_code.css('border','1px solid red');
                    billing_cvv_code.focus();
                    return false;
                } else {
                    if ((billing_cvv_code.val()).toString().length < 3 || (billing_cvv_code.val()).toString().length > 4) {
                        billing_cvv_code.css('border','1px solid red');
                        swal({
                            title: "Oops, Try Again!",
                            text: "Security Code must be \n minimum of 3 and maximum of 4 !",
                            icon: "warning",
                            button: "Ok!",
                        });
                        // billing_cvv_code.focus();
                        return false; 
                    } 
                }

                
                
                

                if (jQuery('[name="same_billing_contact"][value="No"]').is(':checked')) {
                    if (billing_card_holder.val() == '') {
                        billing_card_holder.css('border','1px solid red');
                        billing_card_holder.focus();
                        return false;
                    }
                    if (billing_address.val() == '') {
                        billing_address.css('border','1px solid red');
                        billing_address.focus();
                        return false;
                    }
                    if (billing_state.val() == '') {
                        billing_state.css('border','1px solid red');
                        billing_state.focus();
                        return false;
                    }
                    if (billing_city.val() == '') {
                        billing_city.css('border','1px solid red');
                        billing_city.focus();
                        return false;
                    }
                    if (billing_zip.val() == '') {
                        billing_zip.css('border','1px solid red');
                        billing_zip.focus();
                        return false;
                    }
                }

                if (!jQuery('#s_yes').is(':checked') && !jQuery('#s_no').is(':checked')) {
                    jQuery('[for="s_yes"]').css('color','red');
                    jQuery('[for="s_no"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="s_yes"]').css('color','#999');
                    jQuery('[for="s_no"]').css('color','#999');
                } 
                
            }
            jQuery('#tab-list').find('.arrow_box').removeClass('arrow_box');
            jQuery('#tab-list').find('a').css('color','#00adad');
            jQuery(this).closest('div').addClass('arrow_box');
            jQuery(this).closest('div').find('a').css('color','white');
            jQuery(this).closest('div').find('a').css('text-decoration','none');
            
            jQuery('.tab-content').find('.tab-body').addClass('hide');
            jQuery(''+href).removeClass('hide'); 
            jQuery('html,body').animate({scrollTop:500},1000);
        });
         
        jQuery('.tab-body').on('click','.nextButton',function(e){
            e.preventDefault();
            var value = jQuery(this).attr('value'); 
            if (value == 'submitFormSingle') {

                var username = jQuery('[name="username"]');
                var password = jQuery('[name="password"]');
                var retype_password = jQuery('[name="retype_password"]'); 
                var profile_name = jQuery('[name="profile_name"]');
                var profile_signature1 = jQuery('[name="profile_signature1"]');
                var profile_ss = jQuery('[name="profile_ss"]');
                var profile_date_of_birth = jQuery('[name="profile_date_of_birth"]'); 
                if ((username.val()).length < 5) {
                    username.focus();
                    username.css('border','1px solid red');
                    return false;
                } else {
                    username.css('border','1px solid #e6e6e6');
                }
                if ((password.val()).length < 8) {
                    password.focus();
                    password.css('border','1px solid red');
                    return false;
                } else {
                    password.css('border','1px solid #e6e6e6');
                }
                if ((retype_password.val()).length < 8) {
                    retype_password.focus();
                    retype_password.css('border','1px solid red');
                    return false;
                } else {
                    retype_password.css('border','1px solid #e6e6e6');
                }

                if (!jQuery('#obtain_copy_yes').is(':checked')) {
                    jQuery('#obtain_copy_yes').focus();
                    jQuery('[for="obtain_copy_yes"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="obtain_copy_yes"]').css('color','#999');
                }


                var legal_agreements_checkboxes = jQuery('#legal_agreements').find('[type="checkbox"]'); 
                var proceed = true;
                jQuery.each(legal_agreements_checkboxes,function(key,checkbox){
                    if (!jQuery(checkbox).is(':checked')) {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','red');
                        proceed = false;
                    }  else {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','#3cc');
                    }
                });
                if (!proceed) {
                    return false;
                } 


                if ((profile_name.val()).length == 0) {
                    profile_name.focus();
                    profile_name.css('border','1px solid red');
                    return false;
                } else {
                    profile_name.css('border','1px solid #e6e6e6');
                }

                if ((profile_signature1.val()).length == 0) {
                    profile_signature1.focus();
                    profile_signature1.css('border','1px solid red');
                    return false;
                } else {
                    profile_signature1.css('border','1px solid #e6e6e6');
                }
                if ((profile_ss.val()).length == 0) {
                    profile_ss.focus();
                    profile_ss.css('border','1px solid red');
                    return false;
                } else {
                    profile_ss.css('border','1px solid #e6e6e6');
                }
                if ((profile_date_of_birth.val()).length == 0) {
                    profile_date_of_birth.focus();
                    profile_date_of_birth.css('border','1px solid red');
                    return false;
                } else {
                    profile_date_of_birth.css('border','1px solid #e6e6e6');
                }
                
                var joint_ = jQuery('#r_yes').is(':checked'); 
                if (joint_) { 
                    jQuery('[value="submitFormSingle"]').html('Next');
                    jQuery('#containerCredentialsJoint').removeClass('hide');
                    jQuery('#containerCredentialsSingle').addClass('hide');
                    jQuery('#containerElectricSignatureJoint').removeClass('hide');
                    jQuery('#containerElectricSignatureSingle').addClass('hide');
                    
                    jQuery('html,body').animate({scrollTop:500},1000);
                    jQuery('.LAcheckboxes').attr('checked',false);
                    jQuery('#inputSelectAllHidden').attr('checked',false);
                    jQuery('#obtain_copy_yes').attr('checked',false);


                } else {  
                    var form = jQuery('.wpcf7-form'); 
                    form.submit(); 
                }
                 



                
            } else if (value == 'submitFormJoint') {
                var joint_username = jQuery('[name="joint_username"]');
                var joint_password = jQuery('[name="joint_password"]');
                var joint_retype_password = jQuery('[name="joint_retype_password"]');
                var profile_joint_name = jQuery('[name="profile_joint_name"]');
                var profile_signature2 = jQuery('[name="profile_signature2"]');
                var profile_joint_ss = jQuery('[name="profile_joint_ss"]');
                var profile_joint_date_of_birth = jQuery('[name="profile_joint_date_of_birth"]');

                if ((joint_username.val()).length < 5) {
                    joint_username.focus();
                    joint_username.css('border','1px solid red');
                    return false;
                } else {
                    joint_username.css('border','1px solid #e6e6e6');
                }
                if ((joint_password.val()).length < 8) {
                    joint_password.focus();
                    joint_password.css('border','1px solid red');
                    return false;
                } else {
                    joint_password.css('border','1px solid #e6e6e6');
                }
                if ((joint_retype_password.val()).length < 8) {
                    joint_retype_password.focus();
                    joint_retype_password.css('border','1px solid red');
                    return false;
                } else {
                    joint_retype_password.css('border','1px solid #e6e6e6');
                }

                if (!jQuery('#obtain_copy_yes').is(':checked')) {
                    jQuery('#obtain_copy_yes').focus();
                    jQuery('[for="obtain_copy_yes"]').css('color','red');
                    return false;
                } else {
                    jQuery('[for="obtain_copy_yes"]').css('color','#999');
                }


                var legal_agreements_checkboxes = jQuery('#legal_agreements').find('[type="checkbox"]'); 
                var proceed = true;
                jQuery.each(legal_agreements_checkboxes,function(key,checkbox){
                    if (!jQuery(checkbox).is(':checked')) {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','red');
                        proceed = false;
                    }  else {
                        jQuery(checkbox).closest('h4').find('.checkbox_label').css('color','#3cc');
                    }
                });
                if (!proceed) {
                    return false;
                } 
                if ((profile_joint_name.val()).length == 0) {
                    profile_joint_name.focus();
                    profile_joint_name.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_name.css('border','1px solid #e6e6e6');
                }
                if ((profile_signature2.val()).length == 0) {
                    profile_signature2.focus();
                    profile_signature2.css('border','1px solid red');
                    return false;
                } else {
                    profile_signature2.css('border','1px solid #e6e6e6');
                }
                if ((profile_joint_ss.val()).length == 0) {
                    profile_joint_ss.focus();
                    profile_joint_ss.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_ss.css('border','1px solid #e6e6e6');
                }
                if ((profile_joint_date_of_birth.val()).length == 0) {
                    profile_joint_date_of_birth.focus();
                    profile_joint_date_of_birth.css('border','1px solid red');
                    return false;
                } else {
                    profile_joint_date_of_birth.css('border','1px solid #e6e6e6');
                }
                var form = jQuery('.wpcf7-form'); 
                form.submit(); 
            } else {
                jQuery('a[href="'+value+'"]').trigger('click');
            }
            
        });

        jQuery('.plan_buttons').on('click',function(){
            var plan = jQuery(this).attr('plan');
            var setup_fee = jQuery(this).attr('setup_fee');
            var monthly_fee = jQuery(this).attr('monthly_fee');
            jQuery('.plan_selection_inputs').removeClass('hide');
            jQuery('.billing_plan').val(plan);
            jQuery('.billing_setup_fee').html(setup_fee);
            jQuery('.billing_monthly_fee').html(monthly_fee);
            jQuery('.billing_setup_fee').val(setup_fee);
            jQuery('.billing_monthly_fee').val(monthly_fee); 

            jQuery('.SAIframes').addClass('hide');
            if (plan == 'Essentials') {
                jQuery('.Essentials').removeClass('hide');
            } else if (plan == 'Essentials Plus') {
                jQuery('.Essentials_Plus').removeClass('hide');
            } else if (plan == 'Premiere') {
                jQuery('.Premiere').removeClass('hide');
            } else if (plan == 'Premiere Plus') {
                jQuery('.Premiere_Plus').removeClass('hide');
            } else if (plan == 'Exclusive') {
                jQuery('.Exclusive').removeClass('hide');
            } else if (plan == 'Exclusive Plus') {
                jQuery('.Exclusive_Plus').removeClass('hide');
            }
            jQuery('[name="coupon_code"]').focus();

        });
    

    // Contact Information
        jQuery('[name="client_type"]').on('change',function(){
            jQuery('[for="r_yes"]').css('color','#999');
            jQuery('[for="r_no"]').css('color','#999');
            jQuery('.plan_selection_inputs').addClass('hide');
            jQuery('[name="coupon_code"]').prop('disabled',false);
            jQuery('[name="coupon_code"]').val('');
            jQuery('.billing_setup_fee_coupon').addClass('hide');
            jQuery('.billing_setup_fee').css('text-decoration','none');
            jQuery('#coupon_sub').prop('disabled',false);
            var value = jQuery(this).val();
            if (value == 'Joint') {
                jQuery('.container_joint').removeClass('hide'); 
                jQuery('.container_joint_inputs').removeClass('hide'); 
                jQuery('.container_single').addClass('hide'); 
            } else {
                jQuery('.container_joint').addClass('hide');
                jQuery('.container_joint_inputs').addClass('hide');
                jQuery('.container_single').removeClass('hide');  
            }
        });

        jQuery('[name="profile_email_address"],[name="profile_joint_email_address"]').on('focusout', function(event) {
            event.preventDefault();
            /* Act on the event */
            var email_address = jQuery(this).val();
            if (!isValidEmailAddress(email_address)) {
                jQuery(this).val(''); 
                swal('Error','Invalid Email Address!','error');
            }
            function isValidEmailAddress(emailAddress) {
                var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                return pattern.test(emailAddress);
            };
        });

    

    // Plan Selection
        jQuery('[name="same_billing_contact"]').on('change',function(){
            jQuery('[for="s_yes"]').css('color','#999');
            jQuery('[for="s_no"]').css('color','#999');
            var value = jQuery(this).val();
            if (value == 'No') {
                jQuery('.container_billing_info').removeClass('hide');  
                jQuery('#billing_info_container').html('');
                jQuery('[name="billing_card_holder"]').val('');
                jQuery('[name="billing_address"]').val('');
                jQuery('[name="billing_city"]').val('');
                jQuery('[name="billing_state"]').val('');
                jQuery('[name="billing_zip"]').val('');

            } else {
                jQuery('.container_billing_info').addClass('hide');  
                var profile_name = jQuery('input[name="profile_name"]').val();
                var profile_address = jQuery('input[name="profile_address"]').val();
                var profile_city = jQuery('input[name="profile_city"]').val();
                var profile_state_province = jQuery('select[name="profile_state_province"]').val();
                var profile_zip_postal_code = jQuery('input[name="profile_zip_postal_code"]').val();

                var billing_info_container = profile_name + ', ' + profile_address + ', ' + profile_city + ', ' + profile_state_province + ', ' +profile_zip_postal_code;
                jQuery('#billing_info_container').html(billing_info_container);
                jQuery('[name="billing_card_holder"]').val(profile_name);
                jQuery('[name="billing_address"]').val(profile_address);
                jQuery('[name="billing_city"]').val(profile_city);
                jQuery('[name="billing_state"]').val(profile_state_province);
                jQuery('[name="billing_zip"]').val(profile_zip_postal_code);

            }
        });

        jQuery('input[name="billing_paid"]').on('change',function(){
            var value = jQuery(this).val();
            var add_1 = moment(value).add(1,'months').format('MM/DD/YYYY');
            jQuery('input[name="billing_monthly_due"]').val(add_1);
        });

        jQuery('input[name="profile_name"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_card_holder"]').val(value);
            jQuery('.profile_name').html(value);;
            jQuery('.profile_name').val(value);;

        });

        jQuery('input[name="profile_joint_name"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            var profile_name = jQuery('input[name="profile_name"]');
            if (value == profile_name.val()) {
                jQuery(this).val('');
                jQuery('.profile_joint_name').html('');;
                jQuery('.profile_joint_name').val('');;

                swal('Error','Please use different name','error');
            } else {
                jQuery('.profile_joint_name').html(value);;
                jQuery('.profile_joint_name').val(value);;
            }
            

        });
        jQuery('input[name="profile_email_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('.profile_email_address').val(value);;

        });
        jQuery('input[name="profile_joint_email_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            var profile_email_address = jQuery('input[name="profile_email_address"]');
            if (value == profile_email_address.val()) {
                jQuery('.profile_joint_email_address').val('');;
                jQuery(this).val('');
                swal('Error','Please use different email address','error');
            } else {
                jQuery('.profile_joint_email_address').val(value);;   
            }
        });

        jQuery('input[name="profile_joint_ss"]').on('keyup', function(event) {
            var value = jQuery(this).val();
            var profile_ss = jQuery('input[name="profile_ss"]');
            if (value == profile_ss.val()) {
                jQuery(this).val('');
                swal('Error','Please use different ss','error');
            }
        });

        jQuery('input[name="profile_joint_date_of_birth"]').on('keyup', function(event) {
            var value = jQuery(this).val();
            var profile_date_of_birth = jQuery('input[name="profile_date_of_birth"]');
            if (value == profile_date_of_birth.val()) {
                jQuery(this).val('');
                swal('Error','Please use different Date of Birth','error');
            }
        });
        jQuery('input[name="profile_address"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_address"]').val(value);
        });
        jQuery('input[name="profile_city"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_city"]').val(value);
        });
        jQuery('select[name="profile_state_province"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_state"]').val(value);
        });
        jQuery('input[name="profile_zip_postal_code"]').on('keyup',function(){
            var value = jQuery(this).val(); 
            jQuery('input[name="billing_zip"]').val(value);
        });


    // LEGAL AGREEMENTS
        jQuery('button[value="#plan_selection"]').on('click',function(){ 
            var profile_name = jQuery('input[name="profile_name"]').val();
            var profile_address = jQuery('input[name="profile_address"]').val();
            var profile_city = jQuery('input[name="profile_city"]').val();
            var profile_state_province = jQuery('select[name="profile_state_province"]').val();
            var profile_zip_postal_code = jQuery('input[name="profile_zip_postal_code"]').val();

            var billing_info_container = profile_name + ', ' + profile_address + ', ' + profile_city + ', ' + profile_state_province + ', ' +profile_zip_postal_code;
            jQuery('#billing_info_container').html(billing_info_container);
        });
         

        jQuery('#legal_agreements').on('click','.LA_toggle',function(e){ 
            if (e.target !== this)
                return; 
            var target = jQuery(this).attr('data-target'); 
            var collapse = jQuery(target); 
            collapse.slideToggle('slow');

            var i = jQuery(this).closest('.LA_parent').find('.toggle-icon'); 
            if (i.hasClass('fa-angle-double-right')) {
                i.removeClass('fa-angle-double-right');
                i.addClass('fa-angle-double-down');
            } else {
                i.removeClass('fa-angle-double-down');
                i.addClass('fa-angle-double-right');
            }
        });


        jQuery('#inputSelectAll').on('click',function(){
            var inputSelectAllHidden = jQuery('#inputSelectAllHidden'); 
            console.log(inputSelectAllHidden.is(':checked'));
            if (inputSelectAllHidden.is(':checked')) {
                // inputSelectAllHidden.attr('checked',false);
                jQuery('.LAcheckboxes').attr('checked',false); 
            } else {
                // inputSelectAllHidden.attr('checked',true);
                jQuery('.LAcheckboxes').attr('checked',true);
                
            }
        });

    // BILLING info
        jQuery('[name="billing_card_number"]').on('focusout',function(){
            var val = jQuery(this).val();
            if (val.length < 16) {
                swal({
                    title: "Validation Alert!",
                    text: "Card Number must be at least 16 digits!",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
        jQuery('[name="billing_card_expiration"]').on('focusout',function(){
            var val = jQuery(this).val();
            val = val.replace('_','');
            val = val.replace('/','');  
            if (val.toString().length < 4) {
                swal({
                    title: "Validation Alert!",
                    text: "Card Expiration must be at least 4 digits!",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
        jQuery('[name="billing_cvv_code"]').on('focusout',function(){
            var val = jQuery(this).val(); 
            if (val.toString().length < 3 || val.toString().length > 4 ) {
                swal({
                    title: "Oops, Try Again!",
                    text: "Security Code must be \n minimum of 3 and maximum of 4 !",
                    icon: "warning",
                    button: "Ok!",
                });
                jQuery(this).css('border','1px solid red');
            } else {
                jQuery(this).css('border','1px solid rgb(230, 230, 230)');
            }
        });
     
    // CREDENTIALS
        jQuery('[name="username"]').on('keyup',function(){
            var username = jQuery(this).val();
            if (username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                // var withNumber = (username.match(/[0-9]/g) || []).length;
                if (username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="username"]').on('focusout',function(){
            var username = jQuery(this).val();
            if (username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
                jQuery(this).focus();
            } else {
                // var withNumber = (username.match(/[0-9]/g) || []).length;
                if (username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                    validateUsername(username);
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
            
            function validateUsername(username) {
                jQuery.post('../wp-custom-functions/validate_client_username.php',{username}, function(data){
                        // data = JSON.parse(data);  
                        if (parseInt(data) == 0) {
                            jQuery('[name="username"]').css('border','1px solid #e6e6e6'); 
                            jQuery('#errorUsername').addClass('hide');
                            // alert('Username already exist!');
                        } else {
                            jQuery('[name="username"]').focus();
                            jQuery('[name="username"]').css('border','1px solid red'); 
                            jQuery('#errorUsername').removeClass('hide');
                            // alert('Username already exist!');
                        }
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        });

        jQuery('[name="password"]').on('keyup',function(){
            var password = jQuery(this).val();
            if (password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (password.match(/[0-9]/g) || []).length;
                var withAZ = (password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="password"]').on('focusout',function(){
            var password = jQuery(this).val();
            if (password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (password.match(/[0-9]/g) || []).length;
                var withAZ = (password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
        });

        jQuery('[name="retype_password"]').on('keyup',function(){
            var retype_password = jQuery(this).val();
            var password = jQuery('[name="password"]').val(); 
            if (password != retype_password) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="retype_password"]').on('focusout',function(){
            var retype_password = jQuery(this).val();
            var password = jQuery('[name="password"]').val();
            if (password != retype_password) {
                // jQuery(this).focus(); 
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="joint_username"]').on('keyup',function(){
            var joint_username = jQuery(this).val();
            if (joint_username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                // var withNumber = (joint_username.match(/[0-9]/g) || []).length;
                if (joint_username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="joint_username"]').on('focusout',function(){
            var joint_username = jQuery(this).val();
            if (joint_username.length < 5 ) {
                jQuery(this).css('border','1px solid red'); 
                jQuery(this).focus();
            } else {
                // var withNumber = (joint_username.match(/[0-9]/g) || []).length;
                if (joint_username.indexOf('@') !== -1) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                    validateUsername(joint_username,'joint');
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
            
            function validateUsername(username,type) {
                jQuery.post('../wp-custom-functions/validate_client_username.php',{username,type}, function(data){
                        // data = JSON.parse(data);  
                        console.log(data);
                        if (parseInt(data) == 0) {
                            jQuery('[name="joint_username"]').css('border','1px solid #e6e6e6'); 
                            jQuery('#errorJointUsername').addClass('hide');
                            // alert('Username already exist!');
                        } else {
                            jQuery('[name="joint_username"]').focus();
                            jQuery('[name="joint_username"]').css('border','1px solid red'); 
                            jQuery('#errorJointUsername').removeClass('hide');
                            // alert('Username already exist!');
                        }
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        });

        jQuery('[name="joint_password"]').on('keyup',function(){
            var joint_password = jQuery(this).val();
            if (joint_password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (joint_password.match(/[0-9]/g) || []).length;
                var withAZ = (joint_password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                }
            }
        });

        jQuery('[name="joint_password"]').on('focusout',function(){
            var joint_password = jQuery(this).val();
            if (joint_password.length < 8 ) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                var withNumber = (joint_password.match(/[0-9]/g) || []).length;
                var withAZ = (joint_password.match(/[A-Z]/g) || []).length;
                if (withNumber > 0 && withAZ > 0) {
                    jQuery(this).css('border','1px solid #e6e6e6'); 
                } else {
                    jQuery(this).css('border','1px solid red'); 
                    jQuery(this).focus();
                }
            }
        });

        jQuery('[name="joint_retype_password"]').on('keyup',function(){
            var joint_retype_password = jQuery(this).val();
            var joint_password = jQuery('[name="joint_password"]').val(); 
            if (joint_password != joint_retype_password) {
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

        jQuery('[name="joint_retype_password"]').on('focusout',function(){
            var joint_retype_password = jQuery(this).val();
            var joint_password = jQuery('[name="joint_password"]').val();
            if (joint_password != joint_retype_password) {
                // jQuery(this).focus(); 
                jQuery(this).css('border','1px solid red'); 
            } else {
                jQuery(this).css('border','1px solid #e6e6e6'); 
            }
        });

    // COUPON
        jQuery('[name="copy_of_cr"]').on('change',function(){
            var value = jQuery(this).val();
            if (value == 'Yes') {
                jQuery('.containerUploadCRBtn').removeClass('hide');
            } else {
                jQuery('.containerUploadCRBtn').addClass('hide');
            }
        });

        jQuery('[name="coupon_code"]').keypress(function(event) {
            var keycode = event.keyCode || event.which;
            if(keycode == '13') {
                jQuery('#coupon_sub').trigger('click'); 
                event.preventDefault();
            }
        });
        jQuery('#coupon_sub').on('click',function(){
            var coupon_code = jQuery('[name="coupon_code"]').val();
            // alert(coupon_code);
            if (coupon_code != '') {
                validateCoupon(coupon_code);
            }  
            
            function validateCoupon(coupon_code) {
                    jQuery.post('../wp-custom-functions/validate_coupon.php',{coupon_code}, function(data){
                            data = JSON.parse(data);  
                            // console.log(data);
                            if (data['status'] == 'success') { 
                                jQuery('input[name="billing_coupon_code"]').val(coupon_code);
                                jQuery('.errorValidateCoupon').addClass('hide'); 
                                var setup_fee = parseInt(jQuery('.billing_setup_fee_display').html()) - data['message']; 

                                jQuery('.billing_setup_fee').css('text-decoration','line-through');
                                jQuery('.billing_setup_fee_coupon').removeClass('hide');
                                jQuery('.billing_setup_fee_coupon').html('$'+setup_fee);
                                // alert(setup_fee);
                                jQuery('.billing_setup_fee').val(setup_fee); 
                                jQuery('[name="coupon_code"]').prop('disabled',true);
                                jQuery('[name="coupon_code"]').val('Coupon Applied');
                                jQuery('#coupon_sub').prop('disabled',true);
                            } else { 
                                jQuery('[name="coupon_code"]').val('');
                                jQuery('.errorValidateCoupon').removeClass('hide');
                                jQuery('.errorValidateCoupon').html(data['message']);
                            }
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                        });
                    }
        });

</script>