<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js" integrity="sha384-THVO/sM0mFD9h7dfSndI6TS0PgAGavwKvB5hAxRRvc0o9cPLohB0wb/PTA7LdUHs" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdn.jsdelivr.net/npm/interactjs@1.3.4/dist/interact.min.js"></script>
<style>
	.dropzone {
		position: relative;
		/*background-image: url('../../assets/service_agreement/Credit Lynx Service Agreement OH-01.jpg');*/
	    background-repeat: no-repeat;
	    /*background-attachment: fixed;*/
	    background-position: center;
	    background-size: 816px 1056px;
	    height: 1056px;
	    width: 816px;
	    margin: auto;
	}
	.page-wrapper {
		margin-left: 0 !important;
	}



	.drop-active {
	  border-color: #aaa;
	}

	.drop-target {
	  background-color: #000;
	  color: #FFF;
	  border-color: #fff;
	  border-style: solid;
	}

	body {
		overflow-y: hidden;
	}

	input {
		border: 1.5px solid #088080;
		font-size: 12px;
		width: 150px;
	}

	button {
		padding: 0px !important;
	}

 	hr {
 		margin: 1px;
 	}
 /*	.pos-absolute {
 		position: absolute !important;
 		top: auto !important;
 	}*/

 	.input_container {
 		border: 1px solid #ccc;padding: 10px;
 		margin-top: 10px;
 	}

 	[type=checkbox]+label:before, [type=checkbox]:not(.filled-in)+label:after {
 		width: 12px;
 		height: 12px;
 	}

 	[type=checkbox]:checked+label:before {
 		border-right: 2px solid black;
 		border-bottom: 2px solid black;
 		left: -1px;
 		width: 6px;
 		height: 13px;
 	}

</style>
<div class="container-fluid  " style="padding-top: 30px">
	<a style="position: absolute;top: 80px" href="<?php echo base_url('DocuLynx') . '?view=' . $view ?>"><i class="fas fa-arrow-left"></i> Back</a>
	<div class="row">
		<div style="position: fixed;top: 115px;left: 30px;width: 215px;background-color: white;height: 80vh;float: left;padding: 10px;overflow-y: auto;">
			<button id="btnSaveTemplate" class="btn btn-success btn-block m-b-10" style="padding: 10px !important">
			<?php if ($action == 'edit'): ?>
				Save Template
			<?php endif?>
			<?php if ($action == 'send'): ?>
				Send <i class="fas fa-send"></i>
			<?php endif?>
			<?php if ($action == 'sent'): ?>
				Save</i>
			<?php endif?>
			</button>
			Standard inputs
			<br>
			<button value="signature1" class="inputs waves-effect waves-light btn btn-block btn-success">Signature 1</button>
			<button value="signature2" class="inputs waves-effect waves-light btn btn-block btn-success">Signature 2</button>
			<button value="input_fullname1" class="inputs waves-effect waves-light btn btn-block btn-success">Ful Name 1</button>
			<button value="input_fullname2" class="inputs waves-effect waves-light btn btn-block btn-success">Ful Name 2</button>
			<button value="date_signed1" class="inputs waves-effect waves-light btn btn-block btn-success">Date Signed 1</button>
			<button value="date_signed2" class="inputs waves-effect waves-light btn btn-block btn-success">Date Signed 2</button>
			<button value="cancel_date" class="inputs waves-effect waves-light btn btn-block btn-success">Cancel Date 1 </button>
			<button value="cancel_date2" class="inputs waves-effect waves-light btn btn-block btn-success">Cancel Date 2</button>

			<div id="inputContainerCLA" class="input_container">
				<b style="font-size: 13px;">Credit Lynx Application </b>
				<hr>
				<button value="input_client_name_1" class="inputs waves-effect waves-light btn btn-block btn-success">Client Name 1</button>
				<button value="input_client_name_2" class="inputs waves-effect waves-light btn btn-block btn-success">Client Name 2</button>
				<button value="input_dob_1" class="inputs waves-effect waves-light btn btn-block btn-success">DOB Client 1</button>
				<button value="input_dob_2" class="inputs waves-effect waves-light btn btn-block btn-success">DOB Client 2</button>
				<button value="input_ssn_1" class="inputs waves-effect waves-light btn btn-block btn-success">SSN Client 1</button>
				<button value="input_ssn_2" class="inputs waves-effect waves-light btn btn-block btn-success">SSN Client 2</button>
				<button value="input_cp_1" class="inputs waves-effect waves-light btn btn-block btn-success">Cell Phone Client 1</button>
				<button value="input_cp_2" class="inputs waves-effect waves-light btn btn-block btn-success">Cell Phone Client 2</button>
				<button value="input_email_1" class="inputs waves-effect waves-light btn btn-block btn-success">Email Client 1</button>
				<button value="input_email_2" class="inputs waves-effect waves-light btn btn-block btn-success">Email Client 2</button>
				<button value="input_appt_suite" class="inputs waves-effect waves-light btn btn-block btn-success">Apt/Suite</button>
				<button value="input_address" class="inputs waves-effect waves-light btn btn-block btn-success">Address</button>
				<button value="input_city" class="inputs waves-effect waves-light btn btn-block btn-success">City</button>
				<button value="input_state" class="inputs waves-effect waves-light btn btn-block btn-success">State</button>
				<button value="input_zip" class="inputs btn btn-block btn-success">Zip</button>
			</div>

		</div>
		<div style="position: fixed;top: 115px;right: 30px;width: 215px;background-color: white;height: 80vh;float: left;padding: 10px;overflow-y: auto;">
			<div id="inputContainerPLANS" class="input_container">
				Plans
				<hr>
				<b style="font-size: 10px;">Individual - Premiere Plan</b>
				<br>
				<button value="ipp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $149 Initial</button>
				<button value="ipp_monthly" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $99 Monthly</button>
				<b style="font-size: 10px;">Couples - Premiere Plus Plan</b>
				<br>
				<button value="cppp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $219 Initial</button>
				<button value="cppp_monthly" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $179 Monthly</button>
				<b style="font-size: 10px;">Individual - Exclusive Plan</b>
				<br>
				<button value="iep_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $149 Initial</button>
				<button value="iep_result" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $50 Per Result</button>
				<b style="font-size: 10px;">Couples - Exclusive Plus Plan</b>
				<br>
				<button value="cepp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $219 Initial</button>
				<button value="cepp_result" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $35 Per Result</button>
			</div>


			<div id="inputContainerPaymentDates" class="input_container">
				<b style="font-size: 10px;">Billing </b>
				<hr>
				<button value="input_setup_fee" class="inputs waves-effect waves-light btn btn-block btn-success">Setup Fee</button>
				<button value="input_monthly_fee" class="inputs waves-effect waves-light btn btn-block btn-success">Monthly Fee</button>
				<button value="input_fee_date" class="inputs waves-effect waves-light btn btn-block btn-success">Fee Date</button>
				<button value="input_due_date" class="inputs waves-effect waves-light btn btn-block btn-success">Due Date</button>

			</div>
			<div id="inputContainerCI" class="input_container">
				<b style="font-size: 10px;">Card Information </b>
				<hr>
				<button value="input_name_on_card" class="inputs waves-effect waves-light btn btn-block btn-success">Name on card</button>
				<button value="input_card_number" class="inputs waves-effect waves-light btn btn-block btn-success">Card Number</button>
				<button value="input_expiration" class="inputs waves-effect waves-light btn btn-block btn-success">Expiration</button>
				<button value="input_cvv" class="inputs waves-effect waves-light btn btn-block btn-success">CVV</button>
				<button value="input_billing_address" class="inputs waves-effect waves-light btn btn-block btn-success">Billing Address</button>
				<button value="input_billing_city" class="inputs waves-effect waves-light btn btn-block btn-success">Billing City</button>
				<button value="input_billing_state" class="inputs waves-effect waves-light btn btn-block btn-success">Billing State</button>
				<button value="input_billing_zip" class="inputs waves-effect waves-light btn btn-block btn-success">Billing Zip</button>

			</div>

		</div>

    	<div id="divDropZone"  class=" col-12 col-md-8 offset-md-2 " style="overflow-y: auto;height: 610px">
    		<?php foreach ($template_images as $key => $template_image): ?>
    		  	<div id="templatePage<?php echo $key + 1 ?>" style="background-image: url('../../assets/template_images/<?php echo $template_image['image_name'] ?>');" class="dropzone "></div>
    		<?php endforeach?>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){


	var drop_container = 'templatePage1';
	$('#divDropZone').scroll(function(){
		var scroll = $('#divDropZone').scrollTop();
		scroll = scroll + 200;
		// console.log(scroll);
		if (scroll > 0 && scroll < 1056) {
			drop_container = 'templatePage1';
		} else if (scroll > 1056 && scroll < 2112) {
			drop_container = 'templatePage2';
		} else if (scroll > 2112 && scroll < 3224) {
			drop_container = 'templatePage3';
		} else if (scroll > 3224 && scroll < 4280) {
			drop_container = 'templatePage4';
		} else if (scroll > 4280 && scroll < 5336) {
			drop_container = 'templatePage5';
		} else if (scroll > 5336 && scroll < 6392) {
			drop_container = 'templatePage6';
		} else if (scroll > 6392 && scroll < 7448) {
			drop_container = 'templatePage7';
		} else if (scroll > 7448 && scroll < 8504) {
			drop_container = 'templatePage8';
		} else if (scroll > 8504 && scroll < 9560) {
			drop_container = 'templatePage9';
		} else if (scroll > 9560 && scroll < 10616) {
			drop_container = 'templatePage10';
		} else if (scroll > 10616 && scroll < 11672) {
			drop_container = 'templatePage11';
		} else if (scroll > 11672 && scroll < 12728) {
			drop_container = 'templatePage12';
		} else if (scroll > 12728 && scroll < 13784) {
			drop_container = 'templatePage13';
		} else if (scroll > 13784 && scroll < 14840) {
			drop_container = 'templatePage14';
		} else if (scroll > 14840 && scroll < 15896) {
			drop_container = 'templatePage15';
		} else if (scroll > 15896 && scroll < 16952) {
			drop_container = 'templatePage16';
		} else if (scroll > 16952 && scroll < 18008) {
			drop_container = 'templatePage17';
		} else if (scroll > 18008 && scroll < 19064) {
			drop_container = 'templatePage18';
		} else if (scroll > 19064 && scroll < 20120) {
			drop_container = 'templatePage19';
		} else if (scroll > 20120 && scroll < 21176) {
			drop_container = 'templatePage20';
		}

	});

	var interactable_count = 0;

	$('#btnSaveTemplate').on('click',function(){
		var interactables = $('#divDropZone .interactable');
		interactable_count = interactables.length;
		$(this).html('Please wait ...');
		interactable_counter = 1;
		$.each(interactables,function(key,interactable){
			var drop_container_ = $(interactable).closest('div').attr('id');
			var field_id = $(interactable).attr('id');
			field_id = field_id.split('-');
			field_id = field_id[0];
			var f_id = $(interactable).attr('f_id');
			// if ($(interactable).is('input')) {
				var style = $(interactable).css('transform');
				var input_container = '';
				if ($(interactable).hasClass('inputContainerCLA')) {
					input_container = 'inputContainerCLA';
				}
				if ($(interactable).hasClass('inputContainerPaymentDates')) {
					input_container = 'inputContainerPaymentDates';
				}
				if ($(interactable).hasClass('inputContainerPLANS')) {
					input_container = 'inputContainerPLANS';
				}
				if ($(interactable).hasClass('inputContainerCI')) {
					input_container = 'inputContainerCI';
				} else {
					input_container = 'inputContainerSI';
				}
				saveSAField(f_id,input_container,drop_container_,field_id,style);
			// }
		});
	});

	$('#divDropZone').on('dblclick','.interactable',function(){

		var f_id = $(this).attr('f_id');
		var id = $(this).attr('id');

		id = id.split('-');
		id = id[0];
		$('[value="'+id+'"]').removeClass('hide');
		deleteSAFieldByID(f_id);
		$(this).remove();
	});

	$('.inputs').on('click',function(){
		var value = $(this).attr('value');
		var input = '';
		if (value == 'signature1') {
			input = $('<a href="#" id="signature1" style="border-radius: 0px" class="interactable btn btn-success">Sign 1<br><i class="fas fa-arrow-down"></i></a>');
			// $(this).remove();
		}
		if (value == 'signature2') {
			input = $('<a href="#" id="signature2" style="border-radius: 0px" class="interactable btn btn-success">Sign 2<br><i class="fas fa-arrow-down"></i></a>');
			// $(this).remove();
		}
		if (value == 'date_signed1') {
			input = $('<input id="date_signed1" class="interactable" disabled="" style="border:none;text-align:center;width: 100px" type="text" name="" placeholder="Date Signed 1">');
			// $(this).remove();
		}
		if (value == 'date_signed2') {
			input = $('<input id="date_signed2" class="interactable" disabled="" style="border:none;text-align:center;width: 100px" type="text" name="" placeholder="Date Signed 2">');
			// $(this).remove();
		}
		if (value == 'cancel_date') {
			input = $('<input id="cancel_date" class="interactable" disabled="" style="border:none;text-align:center;width: 100px" type="text" name="" placeholder="Cancel Date">');
			// $(this).remove();
		}
		if (value == 'cancel_date2') {
			input = $('<input id="cancel_date2" class="interactable" disabled="" style="border:none;text-align:center;width: 100px" type="text" name="" placeholder="Cancel Date">');
			// $(this).remove();
		}
		if (value == 'input_fullname1') {
			input = $('<input id="input_fullname1" class="interactable" type="text" name="" placeholder="Ful Name 1">');
			// $(this).remove();
		}
		if (value == 'input_fullname2') {
			input = $('<input id="input_fullname2" class="interactable" type="text" name="" placeholder="Ful Name 2">');
			// $(this).remove();
		}
		if (value == 'ipp_init') {

			input += '<input id="ipp_init_" type="checkbox" >';
			input += '<label id="ipp_init" class="inputContainerPLANS interactable" for="ipp_init_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'ipp_monthly') {
			input += '<input id="ipp_monthly_" type="checkbox" >';
			input += '<label id="ipp_monthly" class="inputContainerPLANS interactable" for="ipp_monthly_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'cppp_init') {
			input += '<input id="cppp_init_" type="checkbox" >';
			input += '<label id="cppp_init" class="inputContainerPLANS interactable" for="cppp_init_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'cppp_monthly') {
			input += '<input id="cppp_monthly_" type="checkbox" >';
			input += '<label id="cppp_monthly" class="inputContainerPLANS interactable" for="cppp_monthly_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'iep_init') {
			input += '<input id="iep_init_" type="checkbox" >';
			input += '<label id="iep_init" class="inputContainerPLANS interactable" for="iep_init_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'iep_result') {
			input += '<input id="iep_result_" type="checkbox" >';
			input += '<label id="iep_result" class="inputContainerPLANS interactable" for="iep_result_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'cepp_init') {
			input += '<input id="cepp_init_" type="checkbox" >';
			input += '<label id="cepp_init" class="inputContainerPLANS interactable" for="cepp_init_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}
		if (value == 'cepp_result') {
			input += '<input id="cepp_result_" type="checkbox" >';
			input += '<label id="cepp_result" class="inputContainerPLANS interactable" for="cepp_result_"></label>';
			input = $(input);
			$(this).addClass('hide');
		}

		if (value == 'input_client_name_1') {
			input = $('<input class="inputContainerCLA interactable" id="input_client_name_1" type="text" name=""  placeholder="Client Name 1">');
			$(this).addClass('hide');
		}
		if (value == 'input_dob_1') {
			input = $('<input class="inputContainerCLA interactable" id="input_dob_1" type="text" style="width: 100px"  placeholder="DOB Client 1">');
			// $(this).addClass('hide');
		}
		if (value == 'input_ssn_1') {
			input = $('<input class="inputContainerCLA interactable" id="input_ssn_1" type="text" name=""  placeholder="SSN Client 1">');
			// $(this).addClass('hide');
		}
		if (value == 'input_cp_1') {
			input = $('<input class="inputContainerCLA interactable" id="input_cp_1" type="text" name=""  placeholder="Cell Phone Client 1">');
			$(this).addClass('hide');
		}
		if (value == 'input_email_1') {
			input = $('<input class="inputContainerCLA interactable" id="input_email_1" type="text" style="width: 230px"  placeholder="Email Client 1">');
			$(this).addClass('hide');
		}
		if (value == 'input_client_name_2') {
			input = $('<input class="inputContainerCLA interactable" id="input_client_name_2" type="text" name=""  placeholder="Client Name 2">');
			$(this).addClass('hide');
		}
		if (value == 'input_dob_2') {
			input = $('<input class="inputContainerCLA interactable" id="input_dob_2" type="text" style="width: 100px" placeholder="DOB Client 2">');
			// $(this).addClass('hide');
		}
		if (value == 'input_ssn_2') {
			input = $('<input class="inputContainerCLA interactable" id="input_ssn_2" type="text" name="" placeholder="SSN Client 2">');
			// $(this).addClass('hide');
		}
		if (value == 'input_cp_2') {
			input = $('<input class="inputContainerCLA interactable" id="input_cp_2" type="text" name="" placeholder="Cell Phone Client 2">');
			$(this).addClass('hide');
		}
		if (value == 'input_email_2') {
			input = $('<input class="inputContainerCLA interactable" id="input_email_2" type="text" style="width: 230px" placeholder="Email Client 2">');
			$(this).addClass('hide');
		}
		if (value == 'input_appt_suite') {
			input = $('<input class="inputContainerCLA interactable" id="input_appt_suite" type="text" name="" placeholder="Apt/Suite">');
			$(this).addClass('hide');
		}
		if (value == 'input_address') {
			input = $('<input class="inputContainerCLA interactable" id="input_address" type="text" style="width: 250px" placeholder="Address">');
			$(this).addClass('hide');
		}
		if (value == 'input_city') {
			input = $('<input class="inputContainerCLA interactable" id="input_city" type="text" name="" placeholder="City">');
			$(this).addClass('hide');
		}
		if (value == 'input_state') {
			input = $('<input class="inputContainerCLA interactable" id="input_state" type="text" style="width: 100px" placeholder="State">');
			$(this).addClass('hide');
		}
		if (value == 'input_zip') {
			input = $('<input class="inputContainerCLA interactable" id="input_zip" type="text" style="width: 100px"  placeholder="Zip">');
			$(this).addClass('hide');
		}
		if (value == 'input_setup_fee') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_setup_fee" style="width: 80px" type="text" name="" placeholder="Setup Fee">');
			$(this).addClass('hide');
		}
		if (value == 'input_monthly_fee') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_monthly_fee" style="width: 80px" type="text" name="" placeholder="Monthly Fee">');
			$(this).addClass('hide');
		}
		if (value == 'input_fee_date') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_fee_date" type="text" name="" placeholder="Fee Date">');
			$(this).addClass('hide');
		}
		if (value == 'input_due_date') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_due_date" type="text" style="width: 100px" name="" placeholder="Due Date">');
			$(this).addClass('hide');
		}
		if (value == 'input_card_number') {
			input = $('<input class="inputContainerCI interactable" id="input_card_number" type="text" style="width: 230px" placeholder="Card Number">');
			$(this).addClass('hide');
		}
		if (value == 'input_expiration') {
			input = $('<input class="inputContainerCI interactable" id="input_expiration" type="text" style="width: 50px" placeholder="Expiration">');
			$(this).addClass('hide');
		}
		if (value == 'input_cvv') {
			input = $('<input class="inputContainerCI interactable" id="input_cvv" type="text" style="width: 40px" placeholder="CVV">');
			$(this).addClass('hide');
		}
		if (value == 'input_billing_address') {
			input = $('<input class="inputContainerCI interactable" id="input_billing_address" type="text" style="width: 200px" placeholder="Billing Address">');
			$(this).addClass('hide');
		}
		if (value == 'input_billing_city') {
			input = $('<input class="inputContainerCI interactable" id="input_billing_city" type="text" style="width: 130px" placeholder="Billing City">');
			$(this).addClass('hide');
		}
		if (value == 'input_billing_state') {
			input = $('<input class="inputContainerCI interactable" id="input_billing_state" type="text" style="width: 100px" placeholder="Billing State">');
			$(this).addClass('hide');
		}
		if (value == 'input_billing_zip') {
			input = $('<input class="inputContainerCI interactable" id="input_billing_zip" type="text" style="width: 80px" placeholder="Billing Zip">');
			$(this).addClass('hide');
		}
		if (value == 'input_name_on_card') {
			input = $('<input class="inputContainerCI interactable" id="input_name_on_card" type="text" style="width: 200px" placeholder="Name on card">');
			$(this).addClass('hide');
		}

		// var top = $('#'+drop_container).scrollTop() +200 ;
		input = input.css('position','absolute');
		input = input.css('top','400px');
		input = input.css('left','200px');
		$('#'+drop_container).append(input);
		// console.log(drop_container);
	});


	getSAFields();
	function getSAFields() {
		$.get('<?php echo base_url('DocuLynx/getSAFields') . '?template_id=' . $template_id . '&action=' . $action . '&name1=' . $name1 . '&name2=' . $name2 ?>',function(data){
				data = JSON.parse(data);
				$.each(data,function(key,value){
					drop_container = value.drop_container;
					var field_id = value.field_id;
					var style = value.style;
					var arr_style = style.replace(/matrix/,'');
					arr_style = style.replace(/ /g,'');
					arr_style = style.replace(')','');
					arr_style = arr_style.replace('matrix(','');
					arr_style = arr_style.replace(/ /g,'');
					arr_style = arr_style.split(',');
					var x = parseInt(arr_style[4]);
					var y = parseInt(arr_style[5]);

					$('button[value="'+field_id+'"').trigger('click');
					$('#'+field_id).attr('f_id', value.f_id);
					$('#'+field_id).attr('id', field_id+'-'+value.f_id);
					$('[f_id="'+value.f_id+'"]').css('transform','translate('+x+'px,'+y+'px)');
					$('[f_id="'+value.f_id+'"]').attr('data-x', x);
        			$('[f_id="'+value.f_id+'"]').attr('data-y', y);
				});

				// genPDF();
			 //  	function genPDF(){
				//    	html2canvas($('#divDropZone')[0],{
			 //   			onrendered:function(canvas){
				// 		   var img=canvas.toDataURL("image/png");
				// 		   var doc = new jsPDF('p', 'mm');
				// 		   doc.addImage(img,'JPEG',20,20);
				// 		   doc.save('test.pdf');
				// 	   }
				//    	});
			 //  	}

			}).fail(function(xhr){
				console.log(xhr.responseText);
			});
	}

	var interactable_counter = 1;
	function saveSAField(f_id,input_container,drop_container,field_id,style) {
		var template_id = '<?php echo $template_id ?>';
		var action = '<?php echo $action ?>';
		var name1 = '<?php echo $name1 ?>';
		var name2 = '<?php echo $name2 ?>';
		var email1 = '<?php echo $email1 ?>';
		var email2 = '<?php echo $email2 ?>';
		$.post('<?php echo base_url('DocuLynx/saveSAField') ?>',
			{f_id,input_container,drop_container,field_id,style,action,name1,name2,email1,email2,template_id},function(data){
				console.log(data);
				if (data == 'send') {
					// window.location.href = '<?php echo base_url('DocuLynx') ?>';
					if (interactable_counter == interactable_count) {
						var href = window.location.href;
						href = href.replace(/action=send/,'action=sent');
						var subject = '<?php echo $subject ?>';
						var message = '<?php echo $message ?>';
						var id1 = '<?php echo $id1 ?>';
						$('#btnSaveTemplate').html('Sent!');
						templateSent(href,id1,template_id,name1,name2,email1,email2,subject,message) ;
					} else {
						interactable_counter++;
					}
				} else {
					if (interactable_counter == interactable_count) {
						$('#btnSaveTemplate').html('Saved!');
						setTimeout(function(){
							$('#btnSaveTemplate').html('Save');
						},5000);
					} else {
						interactable_counter++;
					}
				}
			});
	}
	function templateSent(href,client_id,template_id,name1,name2,email1,email2,subject,message) {
		$.post('<?php echo base_url('DocuLynx/templateSent') ?>',
			{href,client_id,template_id,name1,name2,email1,email2,subject,message},function(data){
				if (data == 'sent!') {
					window.location.href = '<?php echo base_url('DocuLynx?view=waiting') ?>';
				}
			}).fail(function(xhr){
				console.log(xhr.responseText);
			});
		}
	function deleteSAFieldByInputContainer(input_container) {
		$.post('<?php echo base_url('DocuLynx/deleteSAFieldByInputContainer') ?>',
			{input_container},function(data){
				console.log(data);
			});
	}
	function deleteSAFieldByID(f_id) {
		$.post('<?php echo base_url('DocuLynx/deleteSAFieldByID') ?>',
			{f_id},function(data){
				console.log(data);
			});
	}






interact(".interactable")
    .draggable({
        snap: {
            targets: [
                interact.createSnapGrid({
                    x: 1,
                    y: 1
                })
            ],
            relativePoints: [{x: 0, y: 0}]
        },
        restrict: {
            restriction: 'parent',
            elementRect: {top: 0, left: 0, bottom: 1, right: 1}
        },
    })
    .on('dragmove', function (event) {
        var target = event.target,
                // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px,' + y + 'px)';
                // target.parentNode.appendChild(target);
                console.log($(target).attr('id'));
                // var id = target.attr('id');
                // console.log(id);
        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
    });

});
 </script>