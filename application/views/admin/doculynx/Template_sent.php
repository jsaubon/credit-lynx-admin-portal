<link type="text/css" href="../assets/signature_plugin/css/jquery.signature.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/interactjs@1.3.4/dist/interact.min.js"></script>




<script src="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.js') ?>"></script>
<script type="text/javascript" src="../assets/signature_plugin/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="../assets/signature_plugin/js/jquery.signature.js"></script>
<!-- <script type="text/javascript" src="../assets/js/FileSaver.js"></script>   -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

<meta name="viewport" content="width=1024">
<style>
	/*
	@media only screen and (max-width: 1000px) {
	    .dropzone {
			  	background-repeat: no-repeat !important;
			  	background-attachment: scroll !important;
			  	background-position: 0% 0% !important;
			  	background-size: contain !important;
			  	width: auto !important;
			  	height: auto !important;
				min-height: 100% !important;

		}

	}*/
	@media (min-width: 992px) {
		.modal-lg {
		    max-width: 1200px;
		}
	}


	#consumer-disclosure-content p {
		margin-bottom: 0px;
	}
	#consumer-disclosure-content b {
		margin-top: 10px;
	}

	.dropzone {
		position: relative;
		/*background-image: url('../../assets/service_agreement/Credit Lynx Service Agreement OH-01.jpg');*/
	    background-repeat: no-repeat;
	    /*background-attachment: fixed;*/
	    background-position: center;
	    background-size: 816px 1056px;
	    height: 1056px;
	    width: 816px;
	    margin: auto;
	}
	.page-wrapper {
		margin-left: 0 !important;
	}



	.drop-active {
	  border-color: #aaa;
	}

	.drop-target {
	  background-color: #000;
	  color: #FFF;
	  border-color: #fff;
	  border-style: solid;
	}

	body {
		overflow-y: hidden;
	}

	input {
		border-color: red;
		font-size: 12px;
		width: 150px;
	}

	.dropzone button {
		padding: 0px !important;
	}

 	hr {
 		margin: 1px;
 	}
 /*	.pos-absolute {
 		position: absolute !important;
 		top: auto !important;
 	}*/

 	.input_container {
 		border: 1px solid #ccc;padding: 10px;
 		margin-top: 10px;
 	}

 	[type=checkbox]+label:before, [type=checkbox]:not(.filled-in)+label:after {
 		width: 12px;
 		height: 12px;
 	}

 	[type=checkbox]:checked+label:before {
 		border-right: 2px solid black;
 		border-bottom: 2px solid black;
 		left: -1px;
 		width: 6px;
 		height: 13px;
 	}

 	.dropzone a span{
 		color: black;
 	}


 	#btnStartNext {
 		border-radius: 0px;
 		position: fixed;
 		top: 300px;
 		left: 420px;
 	}
</style>
<div class="container-fluid" style="padding-left: 0px;padding-right: 0px">
	<div class="row" style="background-color: #21b3c6">
		<div class="col-12" style="padding: 10px;padding-left: 50px;padding-right: 50px" >
			<h4 class="pull-left" style="color: white;margin-top: 10px">Select the sign field to create and add your signature.</h4>
			<?php if ($status != 'Completed'): ?>
				<button id="btnFinish" style="padding: 5px !important;border-radius: 3px;padding-left: 20px !important;padding-right: 20px !important " class="btn btn-lg pull-right btn-warning">Finish <i class="fas fa-thumbs-up"></i></button>
				<div class="btn-group pull-right m-r-10" >
	                <button style="border: 1px solid #74cddd;background-color: #21b3c6;color: white;height: 41.99px;padding: 5px !important;border-radius: 0px;padding-left: 20px !important;padding-right: 20px !important "  class="btn btn-lg dropdown-toggle dropdown-toggle-split" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
	                    OTHER ACTIONS
	                </button>
	                <!-- <button style="background-color: #21b3c6;color: white;height: 41.99px;padding: 0px;padding-left: 5px;padding-right: 10px;border-radius: 0px" type="button" class="btn btn-lg dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                    <span class="sr-only">Toggle Dropdown</span>
	                </button> -->
	                <div class="dropdown-menu rowTools">
	                    <a class="dropdown-item" href="#" value="finish_later">
	                        Finish Later</a>
	                    <a class="dropdown-item" href="#" value="decline_to_sign">
	                        Decline to Sign</a>
	                    <a class="dropdown-item" href="#" value="view_electronic_record">
	                        View Electronic Record and<br>Signature Disclosure</a>
	                </div>
	            </div>
			<?php endif?>
			<?php if ($status == 'Completed'): ?>
				<button id="btnDownload" style="padding: 5px !important;border-radius: 3px;padding-left: 20px !important;padding-right: 20px !important " class="btn btn-lg pull-right btn-success">Download <i class="fas fa-download"></i></button>
			<?php endif?>

		</div>
	</div>
</div>
<div class="container-fluid  " style="padding-top: 30px">
	<div class="row">
		<div style="display: none;position: fixed;top: 105px;left: 30px;width: 215px;background-color: white;height: 80vh;float: left;padding: 10px;overflow-y: auto;">
			<button id="btnSaveTemplate" class="btn btn-success btn-block m-b-10" style="padding: 10px !important">
			<?php if ($action == 'edit'): ?>
				Save Template
			<?php endif?>
			<?php if ($action == 'send'): ?>
				Send <i class="fas fa-send"></i>
			<?php endif?>
			<?php if ($action == 'seen'): ?>
				Save <i class="fas fa-thumbs-up"></i>
			<?php endif?>
			</button>
			Standard inputs
			<br>
			<button id="mob_signature1" value="signature1" class="inputs waves-effect waves-light btn btn-block btn-success">Signature 1</button>
			<button id="mob_signature2" value="signature2" class="inputs waves-effect waves-light btn btn-block btn-success">Signature 2</button>
			<button id="mob_input_fullname1" value="input_fullname1" class="inputs waves-effect waves-light btn btn-block btn-success">Ful Name 1</button>
			<button id="mob_input_fullname2" value="input_fullname2" class="inputs waves-effect waves-light btn btn-block btn-success">Ful Name 2</button>
			<button id="mob_date_signed1" value="date_signed1" class="inputs waves-effect waves-light btn btn-block btn-success">Date Signed 1</button>
			<button id="mob_date_signed2" value="date_signed2" class="inputs waves-effect waves-light btn btn-block btn-success">Date Signed 2</button>
			<button id="mob_cancel_date" value="cancel_date" class="inputs waves-effect waves-light btn btn-block btn-success">Cancel Date 1</button> 
			<button id="mob_cancel_date2" value="cancel_date2" class="inputs waves-effect waves-light btn btn-block btn-success">Cancel Date 2</button>

			<div id="inputContainerCLA" class="input_container">
				<b style="font-size: 13px;">Credit Lynx Application <!-- <a id="refreshCreditApplicationInputs" href="#"><i class="fas fa-refresh"></i></a> --></b>
				<hr>
				<button id="mob_input_client_name_1" value="input_client_name_1" class="inputs waves-effect waves-light btn btn-block btn-success">Client Name 1</button>
				<button id="mob_input_client_name_2" value="input_client_name_2" class="inputs waves-effect waves-light btn btn-block btn-success">Client Name 2</button>
				<button id="mob_input_dob_1" value="input_dob_1" class="inputs waves-effect waves-light btn btn-block btn-success">DOB Client 1</button>
				<button id="mob_input_dob_2" value="input_dob_2" class="inputs waves-effect waves-light btn btn-block btn-success">DOB Client 2</button>
				<button id="mob_input_ssn_1" value="input_ssn_1" class="inputs waves-effect waves-light btn btn-block btn-success">SSN Client 1</button>
				<button id="mob_input_ssn_2" value="input_ssn_2" class="inputs waves-effect waves-light btn btn-block btn-success">SSN Client 2</button>
				<button id="mob_input_cp_1" value="input_cp_1" class="inputs waves-effect waves-light btn btn-block btn-success">Cell Phone Client 1</button>
				<button id="mob_input_cp_2" value="input_cp_2" class="inputs waves-effect waves-light btn btn-block btn-success">Cell Phone Client 2</button>
				<button id="mob_input_email_1" value="input_email_1" class="inputs waves-effect waves-light btn btn-block btn-success">Email Client 1</button>
				<button id="mob_input_email_2" value="input_email_2" class="inputs waves-effect waves-light btn btn-block btn-success">Email Client 2</button>
				<button id="mob_input_appt_suite" value="input_appt_suite" class="inputs waves-effect waves-light btn btn-block btn-success">Apt/Suite</button>
				<button id="mob_input_address" value="input_address" class="inputs waves-effect waves-light btn btn-block btn-success">Address</button>
				<button id="mob_input_city" value="input_city" class="inputs waves-effect waves-light btn btn-block btn-success">City</button>
				<button id="mob_input_state" value="input_state" class="inputs waves-effect waves-light btn btn-block btn-success">State</button>
				<button id="mob_input_zip" value="input_zip" class="inputs btn btn-block btn-success">Zip</button>
			</div>

		</div>
		<div style="display: none;position: fixed;top: 105px;right: 30px;width: 215px;background-color: white;height: 80vh;float: left;padding: 10px;overflow-y: auto;">
			<div id="inputContainerPLANS" class="input_container">
				Plans <!-- <a id="refreshPLANSInputs" href="#"><i class="fas fa-refresh"></i></a> -->
				<hr>
				<b style="font-size: 10px;">Individual - Premiere Plan</b>
				<br>
				<button id="mob_ipp_init" value="ipp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $149 Initial</button>
				<button id="mob_ipp_monthly" value="ipp_monthly" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $99 Monthly</button>
				<b style="font-size: 10px;">Couples - Premiere Plus Plan</b>
				<br>
				<button id="mob_cppp_init" value="cppp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $219 Initial</button>
				<button id="mob_cppp_monthly" value="cppp_monthly" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $179 Monthly</button>
				<b style="font-size: 10px;">Individual - Exclusive Plan</b>
				<br>
				<button id="mob_iep_init" value="iep_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $149 Initial</button>
				<button id="mob_iep_result" value="iep_result" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $50 Per Result</button>
				<b style="font-size: 10px;">Couples - Exclusive Plus Plan</b>
				<br>
				<button id="mob_cepp_init" value="cepp_init" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $219 Initial</button>
				<button value="cepp_result" class="inputs waves-effect waves-light btn btn-block btn-success"><i class="fas fa-square"></i> $35 Per Result</button>
			</div>


			<div id="inputContainerPaymentDates" class="input_container">
				<b style="font-size: 10px;">Payment Dates <!-- <a id="refreshPaymentDatesInputs" href="#"><i class="fas fa-refresh"></i></a> --></b>
				<hr>
				<button id="mob_input_setup_fee" value="input_setup_fee" class="inputs waves-effect waves-light btn btn-block btn-success">Setup Fee</button>
				<button id="mob_input_monthly_fee" value="input_monthly_fee" class="inputs waves-effect waves-light btn btn-block btn-success">Monthly Fee</button>
				<button id="mob_input_fee_date" value="input_fee_date" class="inputs waves-effect waves-light btn btn-block btn-success">Fee Date</button>
				<button id="mob_input_due_date" value="input_due_date" class="inputs waves-effect waves-light btn btn-block btn-success">Due Date</button>

			</div>
			<div id="inputContainerCI" class="input_container">
				<b style="font-size: 10px;">Card Information <!-- <a id="refreshCIInputs" href="#"><i class="fas fa-refresh"></i></a> --></b>
				<hr>
				<button id="mob_input_name_on_card" value="input_name_on_card" class="inputs waves-effect waves-light btn btn-block btn-success">Name on card</button>
				<button id="mob_input_card_number" value="input_card_number" class="inputs waves-effect waves-light btn btn-block btn-success">Card Number</button>
				<button id="mob_input_expiration" value="input_expiration" class="inputs waves-effect waves-light btn btn-block btn-success">Expiration</button>
				<button id="mob_input_cvv" value="input_cvv" class="inputs waves-effect waves-light btn btn-block btn-success">CVV</button>
				<button id="mob_input_billing_address" value="input_billing_address" class="inputs waves-effect waves-light btn btn-block btn-success">Billing Address</button>
				<button id="mob_input_billing_city" value="input_billing_city" class="inputs waves-effect waves-light btn btn-block btn-success">Billing City</button>
				<button id="mob_input_billing_state" value="input_billing_state" class="inputs waves-effect waves-light btn btn-block btn-success">Billing State</button>
				<button id="mob_input_billing_zip" value="input_billing_zip" class="inputs waves-effect waves-light btn btn-block btn-success">Billing Zip</button>

			</div>

		</div>

    	<div id="divDropZone"  class=" col-12 col-md-8 offset-md-2 " style="overflow-y: auto;height: 610px">
    		<button id="btnStartNext" class="btn btn-warning" style="">Start</button>
    		<?php foreach ($template_images as $key => $template_image): ?>
    		  	<div id="templatePage<?php echo $key + 1 ?>"  style="background-image: url('../../assets/template_images/<?php echo $template_image['image_name'] ?>');" class="dropzone ">
    		  	</div>
    		<?php endforeach?>
		</div>

	</div>


</div>
<div id="modalER" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 0px">
        	<div class="modal-header" style=";padding-bottom: 10px">
        		<h2 style="margin-bottom: 0px;padding-left: 15px;">Agreement to do business with Credit Lynx</h2>
        		<div>
        			<a href="#" class="pull-right btn-lg" style="padding-top: 0px;padding-bottom: 0px;padding-left: 10px;padding-right: 10px" data-dismiss="modal"><i class="fas fa-times"></i></a>
        			<!-- <a href="#" class="pull-right btn-lg" style="padding-top: 0px;padding-bottom: 0px;padding-left: 10px;padding-right: 10px" onclick="printJS('consumer-disclosure-content', 'html')"><i class="fas fa-print"></i></a>
        			<a href="#" download="ElectronicRecordAndSignatureDisclosure.html" href="" class="pull-right btn-lg" style="padding-top: 0px;padding-bottom: 0px;padding-left: 10px;padding-right: 10px" id="btnDownloadER"><i class="fas fa-download"></i></a> -->


        		</div>
        	</div>
            <div class="modal-body">
                <iframe allowfullscreen  style="position:absolute;top:0;left:0;width:100%;height:85vh;"  src="http://docs.google.com/gview?url=<?php echo base_url('assets/downloadable/ERSD.pdf') ?>&embedded=true"></iframe>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-warning" style="border-radius: 0px"  data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div id="modalSignature1" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card-body text-center">
                    <h1>Signature</h1>
                    <textarea class="hide" id="signatureJSON"></textarea>
                    <div class="card">
                        <div class="card-body">
                            <div id="captureSignature" style="width: 300px;height: 200px">

                            </div>
                             <div>
                                <input value="eSign" type="radio" name="group1" id="eSign" class=" radio-col-teal"><label class="hide" for="eSign" >USE ME!</label><br>
                                 <button id="clear2Button1" class="btn btn-success">Clear <i class="fas fa-refresh"></i></button>


                             </div>
                        </div>

                    </div>
                    <div class="card hide">
                        <div class="card-body">
                            <h1 style="font-family: 'Arizonia',cursive;font-size: 50px"><?php echo $name1 ?></h1>
                            <input value="eSign2" type="radio" name="group1" id="eSign2" class="radio-col-teal"><label for="eSign2" >USE ME!</label>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="#" id="btnStartSigning" class="btn btn-success" >Start Signing!</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="" id="dummyDivToNodeHide">
	_
</div>
<script>
$(document).ready(function(){
	if ('<?php echo $status ?>' == 'Completed') {
    	$('#btnStartNext').addClass('hide');
    }
	if (/Mobi|Android/i.test(navigator.userAgent)) {
	    // mobile!
	    // alert();
	    $('#divDropZone').removeClass('col-md-8');
	    $('#divDropZone').removeClass('offset-md-2');
	    $('#divDropZone').css('height','100vh');
	    // $('.dropzone').addClass('m-0');
	    $('#captureSignature').css('width','90vw');
	    $('#captureSignature').css('height','50vh');

	    $('#btnStartNext').css('left','0px');
	    // $('#btnStartNext').css('background-color','black');

	}

 	$('#btnStartNext').on('click',function(){
 		$('#btnFinish').trigger('click');
 		$(this).html('Next');

 	});

	$('.rowTools a').on('click',function(e){
		e.preventDefault();
		var value = $(this).attr('value');
		if (value == 'view_electronic_record') {
			$('#modalER').modal('show');
		}
		if (value == 'decline_to_sign') {
			var sent_id = '<?php echo $sent_id ?>';
			swal({
		        title: "Are you sure?",
		        text: 'This document will be voided!',
		        type: "warning",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes!",
		        closeOnConfirm: false ,
		        preConfirm: function() {
		        	voidTemplate(sent_id,'Decline to Sign');
		             function voidTemplate(sent_id,reason) {
						$.post('<?php echo base_url('DocuLynx/voidTemplate') ?>',{sent_id,reason}, function(data){
								// data = JSON.parse(data);
								console.log(data);
								if (data == 'voided') {
									window.location.href = '<?php echo base_url('DocuLynx/route?page=decline_to_sign') ?>';
								}
							}).fail(function(xhr){
								console.log(xhr.responseText);
							});
						}
		        }
		    });

		}
		if (value == 'finish_later') {
			window.location.href = '<?php echo base_url('DocuLynx/route?page=finish_later') ?>'
		}
	});
	function replaceElementTag(targetSelector, newTagString) {
		targetSelector.each(function(){
			var newElem = $(newTagString, {html: $(this).html()});
			$.each(this.attributes, function() {
				newElem.attr(this.name, this.value);
			});
			$(this).replaceWith(newElem);
		});
	}




	var all_inputs = 0;

	var interactable_count = 0;

	$('#btnDownload').on('click',function(){



		makePDF();
		// console.log(checkbox);
		$(this).html('please wait ...');

	});

 	$('#btnFinish').on('click',function(){
 		$('#btnStartNext').html('Next');
 		var input_text = $('input[type="text"]');
 		var text_inputted = true;
 		var all_inputted = false;


 		var viewer = '<?php echo $viewer ?>';
 		var signature1 = $('a[field="signature1"]');
 		var signed1 = true;
 		$.each(signature1,function(key,signature){
 			if ($(signature).attr('signed') != 'true') {
 				$(signature).focus();
 				signed1 = false;
 				return false;
 			}
 		});
 		var signature2 = $('a[field="signature2"]');
 		var signed2 = true;
 		$.each(signature2,function(key,signature){
 			if ($(signature).attr('signed') != 'true') {
 				$(signature).focus();
 				signed2 = false;
 				return false;
 			}
 		});
 		if (viewer == 'name1') {
	 		if (!signed1) {
	 			$.each(signature1,function(key,signature){
		 			if ($(signature).attr('signed') != 'true') {
		 				$(signature).focus();
		 				signed1 = false;
		 				var eTop = $(signature).offset().top;
				 		$('#btnStartNext').css('top',eTop +'px');

		 				setTimeout(function(){
			 				var eTop = $(signature).offset().top;
					 		$('#btnStartNext').css('top',eTop +'px');
			 			},500);
			 			return false;
		 			}
		 		});
	 			return false;

	 		}
 		} else {
 			if (!signed2) {
 				$.each(signature2,function(key,signature){
		 			if ($(signature).attr('signed') != 'true') {
		 				$(signature).focus();
		 				signed2 = false;
		 				var eTop = $(signature).offset().top;
				 		$('#btnStartNext').css('top',eTop +'px');
				 		setTimeout(function(){
			 				var eTop = $(signature).offset().top;
					 		$('#btnStartNext').css('top',eTop +'px');
			 			},500);
		 				return false;
		 			}
		 		});
	 			return false;
	 		}
 		}




 		var tabindex = [];
 		$.each(input_text,function(key,input){
 			if (typeof($(input).attr('tabindex')) != "undefined") {
 				tabindex.push(parseInt($(input).attr('tabindex')));
 			}
 		});
 		tabindex = tabindex.sort();
 		$.each(tabindex,function(key,value){
 			var exit = false;
 			$.each(input_text,function(key,input){
 				if (value == $(input).attr('tabindex')) {

 					if ($(input).val() == '') {
 						console.log($(input).attr('id'));
		 				$(input).focus();
		 				text_inputted = false;

				 		var eTop = $(input).offset().top;
				 		$('#btnStartNext').css('top',eTop +'px');
				 		setTimeout(function(){
			 				var eTop = $(input).offset().top;
					 		$('#btnStartNext').css('top',eTop +'px');
			 			},500);
		 				exit = true;
		 				return false;
		 			}
 				}

	 		});

	 		if (exit) {
	 			return false;
	 		}
 		});

 		if (!text_inputted) {
 			// $('#btnFinish').trigger('click');
 			return false;
 		}

 		var input_number = $('input[type="number"]');

 		var input_date = $('input[type="date"]');
 		var date_inputted = true;
 		$.each(input_date,function(key,input){
 			if ($(input).val() == '') {
 				$(input).focus();
 				var eTop = $(input).offset().top;
		 		$('#btnStartNext').css('top',eTop +'px');
 				date_inputted = false;
 				setTimeout(function(){
	 				var eTop = $(input).offset().top;
			 		$('#btnStartNext').css('top',eTop +'px');
	 			},500);
 				return false;
 			}
 		});

 		if (!date_inputted) {
 			// $('#btnFinish').trigger('click');
 			return false;
 		}

 		// var input_checkbox = $('input[type="checkbox"]');
 		// var something_checked = false;
 		// $.each(input_checkbox,function(key,checkbox){
 		// 	if ($(checkbox).is(':checked')) {
 		// 		something_checked = true;
 		// 		all_inputted = true;
 		// 		$(checkbox).val('checked');

 		// 	} else {
 		// 		$(this).focus();
	 	// 		swal('Choose a Plan',"please choose your plan",'info');
	 	// 		var eTop = $(checkbox).offset().top;
		 // 		$('#btnStartNext').css('top',eTop +'px');
		 // 		setTimeout(function(){
	 	// 			var eTop = $(checkbox).offset().top;
			//  		$('#btnStartNext').css('top',eTop +'px');
	 	// 		},500);
 		// 		return false;
 		// 	}
 		// });

 		// if (!something_checked) {
 		// 	return false;
 		// }



 		updated_input_count = 1;
 		if (text_inputted && date_inputted && signed1 && signed2) {
 			// if (!something_checked) {

		 		// $('#btnStartNext').css('top',eTop +'px');
	 		// } else {
	 			$(this).html('please wait ...');
	 			all_inputs = input_date.length + input_text.length +  signature1.length + signature2.length;
	 			console.log(all_inputs);
	 			$.each(input_text,function(key,input){
		 			var f_id = $(input).attr('f_id');
		 			var value = $(input).val();
		 			updateFieldValue(f_id,value);
		 		});

		 		$.each(input_number,function(key,input){
		 			var f_id = $(input).attr('f_id');
		 			var value = $(input).val();
		 			updateFieldValue(f_id,value);
		 		});

		 		$.each(input_date,function(key,input){
		 			var f_id = $(input).attr('f_id');
		 			var value = $(input).val();
		 			updateFieldValue(f_id,value);
		 		});
		 		// $.each(input_checkbox,function(key,checkbox){
		 		// 	var id = $(checkbox).attr('id');
		 		// 	var value = $(checkbox).val();
		 		// 	var f_id = $('[for="'+id+'"]').attr('f_id');

		 		// 	// console.log(f_id);
		 		// 	// console.log(value);
		 		// 	updateFieldValue(f_id,value);
		 		// });

		 		$.each(signature1,function(key,signature){
		 			var f_id = $(signature).attr('f_id');
		 			var value = $(signature).html();
		 			value = value.replace(/'/,"/'");
		 			// console.log(value);
		 			updateFieldValue(f_id,value);
		 		});
		 		$.each(signature2,function(key,signature){
		 			var f_id = $(signature).attr('f_id');
		 			var value = $(signature).html();
		 			value = value.replace(/'/,"/'");
		 			updateFieldValue(f_id,value);
		 		});
	 		// }

 		} else {
 			alert();
 		}
 	});

 	$('.dropzone').on('change','.interactable',function(){
 		console.log('changed');
 		var f_id = $(this).attr('f_id');
		var value = $(this).val();
		updateFieldValue(f_id,value);
 	});

 	$('.dropzone').on('click','.signatures',function(e){

 		e.preventDefault();
 		var status = '<?php echo $status ?>';

 		if (status != 'Completed') {
 			var radio = $('#eSign:checked');
 			// alert(radio.length);
 			var viewer = '<?php echo $viewer ?>';
 			// alert(viewer);
 			if (viewer == 'name1') {
 				if ($(this).attr('field') == 'signature1') {
			 		if (radio.length > 0) {
			 			var name = '<?php echo $name1 ?>';
				        $(this).removeClass('btn');
				        $(this).removeClass('btn-success');
				        var eSign = '';
				        if ($('input[name=group1]:checked').val() == 'eSign') {
				            var src = $('#signatureJSON').val();
				            eSign = '<img width="70px" height="40px" src="'+src+'"/>';
				        } else {
				            eSign = '<span style="font-family: \'Arizonia\', cursive;font-size: 25px">'+name+'</span>';
				        }

				        $(this).html(eSign);
				        $(this).attr('signed','true');

				        var f_id = $(this).attr('f_id');
			 			var value = $(this).html();
			 			value = value.replace(/'/,"/'");
			 			// console.log(value);
			 			updateFieldValue(f_id,value);
			 			// var date_signed1 = $('[field="date_signed1"]');
			 			// $.each(date_signed1,function(key,date_signed){
			 			// 	var f_id = $(date_signed).attr('f_id');
			 			// 	var value = $(date_signed).val();
			 			// 	updateFieldValue(f_id,value);
			 			// });
			 			var date_signed1 = $(this).closest('div').find('[field="date_signed1"]');
			 			var f_id = $(date_signed1).attr('f_id');
		 				var value = moment().format('MM/DD/YYYY hh:mm:ss a');
		 				$(date_signed1).val(value);
		 				$('[field="cancel_date"]').prop('disabled',false);
		 				$('[field="cancel_date2"]').prop('disabled',false);
		 				updateFieldValue(f_id,value);


			 		} else {
			 			// $('#modalSignature1').modal('show');
			 			// $('#modalSignature1').css('z-index',9999999);
			 			$('#modalSignature1').modal({backdrop: false});
			 			$('#captureSignature').signature('clear');
			 			$('#eSign').attr('checked',true);
			 		}
		 		}
 			}
		 	if (viewer == 'name2') {
		 		if ($(this).attr('field') == 'signature2') {
			 		if (radio.length > 0) {
			 			var name = '<?php echo $name2 ?>';
				        $(this).removeClass('btn');
				        $(this).removeClass('btn-success');
				        var eSign = '';
				        if ($('input[name=group1]:checked').val() == 'eSign') {
				            var src = $('#signatureJSON').val();
				            eSign = '<img width="100px" height="50px" src="'+src+'"/>';
				        } else {
				            eSign = '<span style="font-family: \'Arizonia\', cursive;font-size: 25px">'+name+'</span>';
				        }

				        $(this).html(eSign);
				        $(this).attr('signed','true');

				        var f_id = $(this).attr('f_id');
			 			var value = $(this).html();
			 			value = value.replace(/'/,"/'");
			 			// console.log(value);
			 			updateFieldValue(f_id,value);
			 			var date_signed2 = $(this).closest('div').find('[field="date_signed2"]');
			 			var f_id = $(date_signed2).attr('f_id');
		 				var value = moment().format('MM/DD/YYYY hh:mm:ss a');
		 				$(date_signed2).val(value);
		 				updateFieldValue(f_id,value);


			 		} else {
			 			// $('#modalSignature1').modal('show');
			 			$('#modalSignature1').modal({backdrop: false});
			 			$('#captureSignature').signature('clear');
			 			$('#eSign').attr('checked',true);
			 		}
		 		}
		 	}

 		}


 	});

 	$('#btnStartSigning').on('click',function(){
 		// $('#modalSignature1').modal('hide');
 		$("#modalSignature1").modal("hide");
		$("#modalSignature1").hide();
		$('.modal-backdrop').hide();
		$("body").removeClass("modal-open");
		// $('#modalSignature1').trigger('click');

		$('.signatures').trigger('click');

 		// alert('signature box hide');
 		// var radio = $('input[type="radio"]:checked');
 		// if (radio.length > 0) {
 		// 	$('#modalSignature1').modal('hide');
 		// } else {
 		// 	swal('Please select Signature');
 		// }

    });

    $('#captureSignature').signature({syncField: '#signatureJSON'});
    $('#captureSignature').signature('option', 'syncFormat', 'JPEG');



    $('#modalSignature1').on('shown.bs.modal',function(){
    	$('#captureSignature').signature('clear');
    });

    $('#clear2Button1').click(function() {
        $('#captureSignature').signature('clear');
    });


	$('.inputs').on('click',function(){
		var value = $(this).attr('value');
		var input = '';
		if (value == 'signature1') {
			input = $('<a field="signature1" href="#" id="signature1" style="border-radius: 0px" class="signatures interactable btn btn-success">Sign 1<br><i class="fas fa-arrow-down"></i></a>');
			// $(this).remove();
		}
		if (value == 'signature2') {
			input = $('<a field="signature2" href="#" id="signature2" style="border-radius: 0px" class="signatures interactable btn btn-success">Sign 2<br><i class="fas fa-arrow-down"></i></a>');
			// $(this).remove();
		}
		if (value == 'date_signed1') {
			input = $('<input field="date_signed1" id="date_signed1" class="interactable" disabled="" style="border:none;text-align:center;width: 150px" type="text" name="" placeholder="Date Signed 1">');
			// $(this).remove();
		}
		if (value == 'date_signed2') {
			input = $('<input field="date_signed2" id="date_signed2" class="interactable" disabled="" style="border:none;text-align:center;width: 150px" type="text" name="" placeholder="Date Signed 2">');
			// $(this).remove();
		}
		if (value == 'cancel_date') {
			input = $('<input field="cancel_date" id="cancel_date" class="interactable" style="border:none;text-align:center;width: 150px" type="text" name="" placeholder="Cancel Date">');
			// $(this).remove();
		}
		if (value == 'cancel_date2') {
			input = $('<input field="cancel_date2" id="cancel_date2" class="interactable" style="border:none;text-align:center;width: 150px" type="text" name="" placeholder="Cancel Date">');
			// $(this).remove();
		}
		if (value == 'input_fullname1') {
			input = $('<input field="name1" id="input_fullname1" class="interactable" type="text" name="" placeholder="Full Name 1">');
			// $(this).remove();
		}
		if (value == 'input_fullname2') {
			input = $('<input field="name2" id="input_fullname2" class="interactable" type="text" name="" placeholder="Full Name 2">');
			// $(this).remove();
		}
		if (value == 'ipp_init') {

			input += '<input id="ipp_init_" type="checkbox" >';
			input += '<label id="ipp_init" class="inputContainerPLANS interactable" for="ipp_init_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'ipp_monthly') {
			input += '<input id="ipp_monthly_" type="checkbox" >';
			input += '<label id="ipp_monthly" class="inputContainerPLANS interactable" for="ipp_monthly_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'cppp_init') {
			input += '<input id="cppp_init_" type="checkbox" >';
			input += '<label id="cppp_init" class="inputContainerPLANS interactable" for="cppp_init_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'cppp_monthly') {
			input += '<input id="cppp_monthly_" type="checkbox" >';
			input += '<label id="cppp_monthly" class="inputContainerPLANS interactable" for="cppp_monthly_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'iep_init') {
			input += '<input id="iep_init_" type="checkbox" >';
			input += '<label id="iep_init" class="inputContainerPLANS interactable" for="iep_init_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'iep_result') {
			input += '<input id="iep_result_" type="checkbox" >';
			input += '<label id="iep_result" class="inputContainerPLANS interactable" for="iep_result_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'cepp_init') {
			input += '<input id="cepp_init_" type="checkbox" >';
			input += '<label id="cepp_init" class="inputContainerPLANS interactable" for="cepp_init_"></label>';
			input = $(input);
			$(this).remove();
		}
		if (value == 'cepp_result') {
			input += '<input id="cepp_result_" type="checkbox" >';
			input += '<label id="cepp_result" class="inputContainerPLANS interactable" for="cepp_result_"></label>';
			input = $(input);
			$(this).remove();
		}

		if (value == 'input_client_name_1') {
			input = $('<input tabindex="100" class="inputContainerCLA interactable" id="input_client_name_1" type="text" name=""  placeholder="Client Name 1">');
			$(this).remove();
		}
		if (value == 'input_dob_1') {
			input = $('<input tabindex="101" class="inputContainerCLA interactable" id="input_dob_1" type="text" style="width: 100px"  placeholder="DOB Client 1">');
			// $(this).remove();
		}
		if (value == 'input_ssn_1') {
			input = $('<input tabindex="102" class="inputContainerCLA interactable" id="input_ssn_1" type="text" name=""  placeholder="SSN Client 1">');
			// $(this).remove();
		}
		if (value == 'input_cp_1') {
			input = $('<input tabindex="103" class="inputContainerCLA interactable" id="input_cp_1" type="text" name=""  placeholder="Cell Phone Client 1">');
			$(this).remove();
		}
		if (value == 'input_email_1') {
			input = $('<input tabindex="104" class="inputContainerCLA interactable" id="input_email_1" type="text" style="width: 230px"  placeholder="Email Client 1">');
			$(this).remove();
		}
		if (value == 'input_client_name_2') {
			input = $('<input tabindex="105" class="inputContainerCLA interactable" id="input_client_name_2" type="text" name=""  placeholder="Client Name 2">');
			$(this).remove();
		}
		if (value == 'input_dob_2') {
			input = $('<input tabindex="106" class="inputContainerCLA interactable" id="input_dob_2" type="text" style="width: 100px" placeholder="DOB Client 2">');
			// $(this).remove();
		}
		if (value == 'input_ssn_2') {
			input = $('<input tabindex="107" class="inputContainerCLA interactable" id="input_ssn_2" type="text" name="" placeholder="SSN Client 2">');
			// $(this).remove();
		}
		if (value == 'input_cp_2') {
			input = $('<input tabindex="108" class="inputContainerCLA interactable" id="input_cp_2" type="text" name="" placeholder="Cell Phone Client 2">');
			$(this).remove();
		}
		if (value == 'input_email_2') {
			input = $('<input tabindex="109" class="inputContainerCLA interactable" id="input_email_2" type="text" style="width: 230px" placeholder="Email Client 2">');
			$(this).remove();
		}
		if (value == 'input_appt_suite') {
			input = $('<input tabindex="110" class="inputContainerCLA interactable" id="input_appt_suite" type="text" name="" placeholder="Apt/Suite">');
			$(this).remove();
		}
		if (value == 'input_address') {
			input = $('<input tabindex="111" class="inputContainerCLA interactable" id="input_address" type="text" style="width: 250px" placeholder="Address">');
			$(this).remove();
		}
		if (value == 'input_city') {
			input = $('<input tabindex="112" class="inputContainerCLA interactable" id="input_city" type="text" name="" placeholder="City">');
			$(this).remove();
		}
		if (value == 'input_state') {
			input = $('<input tabindex="113" class="inputContainerCLA interactable" id="input_state" type="text" style="width: 100px" placeholder="State">');
			$(this).remove();
		}
		if (value == 'input_zip') {
			input = $('<input tabindex="114" class="inputContainerCLA interactable" id="input_zip" type="text" style="width: 100px"  placeholder="Zip">');
			$(this).remove();
		}
		if (value == 'input_setup_fee') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_setup_fee" style="width: 80px" type="text" name="" placeholder="Setup Fee">');
			$(this).remove();
		}
		if (value == 'input_monthly_fee') {
			input = $('<input class="inputContainerPaymentDates interactable" id="input_monthly_fee" style="width: 80px" type="text" name="" placeholder="Monthly Fee">');
			$(this).remove();
		}
		if (value == 'input_fee_date') {
			input = $('<input tabindex="115" class="inputContainerPaymentDates interactable" id="input_fee_date" type="text" name="" placeholder="Fee Date">');
			$(this).remove();
		}
		if (value == 'input_due_date') {
			input = $('<input tabindex="116" class="inputContainerPaymentDates interactable" id="input_due_date" style="width: 100px" type="text" max="31" min="1" name="" value="1" placeholder="Due Date">');
			$(this).remove();
		}
		if (value == 'input_card_number') {
			input = $('<input tabindex="117" class="inputContainerCI interactable" id="input_card_number" type="text" style="width: 230px" placeholder="Card Number">');
			$(this).remove();
		}
		if (value == 'input_expiration') {
			input = $('<input tabindex="118" class="inputContainerCI interactable" id="input_expiration" type="text" style="width: 50px" placeholder="Expiration">');
			$(this).remove();
		}
		if (value == 'input_cvv') {
			input = $('<input tabindex="119" class="inputContainerCI interactable" id="input_cvv" type="text" style="width: 40px" placeholder="CVV">');
			$(this).remove();
		}
		if (value == 'input_billing_address') {
			input = $('<input tabindex="120" class="inputContainerCI interactable" id="input_billing_address" type="text" style="width: 200px" placeholder="Billing Address">');
			$(this).remove();
		}
		if (value == 'input_billing_city') {
			input = $('<input tabindex="120" class="inputContainerCI interactable" id="input_billing_city" type="text" style="width: 130px" placeholder="Billing City">');
			$(this).remove();
		}
		if (value == 'input_billing_state') {
			input = $('<input tabindex="120" class="inputContainerCI interactable" id="input_billing_state" type="text" style="width: 100px" placeholder="Billing State">');
			$(this).remove();
		}
		if (value == 'input_billing_zip') {
			input = $('<input tabindex="120" class="inputContainerCI interactable" id="input_billing_zip" type="text" style="width: 80px" placeholder="Billing Zip">');
			$(this).remove();
		}
		if (value == 'input_name_on_card') {
			input = $('<input tabindex="121" class="inputContainerCI interactable" id="input_name_on_card" type="text" style="width: 200px" placeholder="Name on card">');
			$(this).remove();
		}

		// var top = $('#'+drop_container).scrollTop() +200 ;
		input = input.css('position','absolute');
		input = input.css('top','400px');
		input = input.css('left','200px');
		$('#'+drop_container).append(input);
		// console.log(drop_container);
	});


	getSAFields();
	function getSAFields() {
		$.get('<?php echo base_url('DocuLynx/getSAFields') . '?template_id=' . $template_id . '&action=' . $action . '&name1=' . $name1 . '&name2=' . $name2 ?>',function(data){
				data = JSON.parse(data);

				$.each(data,function(key,value){
					drop_container = value.drop_container;
					var field_id = value.field_id;

					var style = value.style;
					var arr_style = style.replace(/matrix/,'');
					arr_style = style.replace(/ /g,'');
					arr_style = style.replace(')','');
					arr_style = arr_style.replace('matrix(','');
					arr_style = arr_style.replace(/ /g,'');
					arr_style = arr_style.split(',');
					var x = parseInt(arr_style[4]);
					var y = parseInt(arr_style[5]);

					$('#mob_'+field_id+'').trigger('click');


					$('#'+field_id).attr('f_id', value.f_id);
					$('#'+field_id).attr('id', field_id+'-'+value.f_id);
					$('[f_id="'+value.f_id+'"]').css('transform','translate('+x+'px,'+y+'px)');
					$('[f_id="'+value.f_id+'"]').attr('data-x', x);
        			$('[f_id="'+value.f_id+'"]').attr('data-y', y);
        			if (field_id == 'signature1' || field_id == 'signature2') {
        				if (value.value != '' && value.value != null) {
        					$('[f_id="'+value.f_id+'"]').attr('signed','true');
	        				$('[f_id="'+value.f_id+'"]').removeClass('btn');
			        		$('[f_id="'+value.f_id+'"]').removeClass('btn-success');

			        		var val = $('<h1 style="font-family: \'Prestige\',cursive;font-size: 30px;color: black;">'+value.value+'</h1>');
			        		// console.log(val);
			        		$('[f_id="'+value.f_id+'"]').html(val);
        				}

        			} else {

	        			if ($('[f_id="'+value.f_id+'"]').attr('type') == 'date'){
	        				$('[f_id="'+value.f_id+'"]').val(moment(value.value).format('YYYY-MM-DD'));
		        		}
		        		if ($('[f_id="'+value.f_id+'"]').attr('type') == 'number'){
		        			if ($('[f_id="'+value.f_id+'"]').attr('id') == 'input_due_date-'+value.f_id) {
	        					$('[f_id="'+value.f_id+'"]').val(moment(value.value).format('DD'));
		        			}
		        		}
	        			if ($('[f_id="'+value.f_id+'"]').is(':text')){
	        				$('[f_id="'+value.f_id+'"]').val(value.value);
		        		}
	        			if ($('[f_id="'+value.f_id+'"]').is('label')) {
	        				if (value.value == 'checked') {
		        				// $('[f_id="'+value.f_id+'"]').attr('checked',true);
		        				var id = $('[f_id="'+value.f_id+'"]').attr('for');
		        				$('#'+id).attr('checked',true);
		        				$('#'+id).val('checked');
		        			}
	        			}

        			}



				});

				// genPDF();
			 //  	function genPDF(){
				//    	html2canvas($('#divDropZone')[0],{
			 //   			onrendered:function(canvas){
				// 		   var img=canvas.toDataURL("image/png");
				// 		   var doc = new jsPDF('p', 'mm');
				// 		   doc.addImage(img,'JPEG',20,20);
				// 		   doc.save('test.pdf');
				// 	   }
				//    	});
			 //  	}


				var name1 = '<?php echo $name1 ?>';
				$('[field="name1"]').val(name1);
				var name2 = '<?php echo $name2 ?>';
				$('[field="name2"]').val(name2);

				var viewer = '<?php echo $viewer ?>';
				if (viewer == 'name1') {
					// var date_signed1 = moment().format('MM/DD/YYYY h:mm:ss a');;
					// $('[field="date_signed1"]').val(date_signed1);
					$('[field="name1"]').attr('disabled',true);
					$('[field="name1"]').css('border','0px');
					$('[field="name2"]').css('border','0px');
					$('[field="name2"]').attr('disabled',true);
					$('[field="signature2"]').attr('disabled',true);
				}
				if (viewer == 'name2') {
					// var date_signed2 = moment().format('MM/DD/YYYY h:mm:ss a');;
					// $('[field="date_signed2"]').val(date_signed2);
					$('[field="name1"]').attr('disabled',true);
					$('[field="name1"]').css('border','0px');
					$('[field="name2"]').attr('disabled',true);
					$('[field="name2"]').css('border','0px');
					$('[field="signature1"]').attr('disabled',true);
				}


				var status = '<?php echo $status ?>';
				if (status == 'Completed') {
					$('input').css('border','0');
					$('input').css('background','transparent');
					$('input').attr('readonly',true);
					$('input').attr('onclick','return false;');
				}

				if (status == 'Completed') {
					var labels = $('#divDropZone label');
					$.each(labels,function(key,label){
						var checkbox = $(label).attr('for');
						checkbox = $('#'+checkbox).is(':checked');
						if (checkbox) {
							$(label).removeAttr('for');
							$(label).html('X');
							$(label).css('color','black');
							$(label).css('font-weight','bolder');
							$(label).css('font-size','80%');
							var transform = $(label).css('transform');
							// console.log(transform);
							var transform = transform.replace(/matrix/,'');
								transform = transform.replace(/ /g,'');
								transform = transform.replace(')','');
								transform = transform.replace('matrix(','');
								transform = transform.replace(/ /g,'');
								transform = transform.split(',');
							var x = parseInt(transform[4]) + 1;
							var y = parseInt(transform[5]) - 3;

							$(label).css('transform','translate('+x+'px,'+y+'px)');
							replaceElementTag($(label), '<b></b>');
						} else {
							$(label).remove();
						}
					});
				}




			}).fail(function(xhr){
				console.log(xhr.responseText);
			});
	}

	var updated_input_count = 1;
	function updateFieldValue(f_id,value) {
		$.post('<?php echo base_url('DocuLynx/updateFieldValue') ?>',
			{f_id,value}, function(data){
				console.log('changed');
				console.log(all_inputs+ ' '+ updated_input_count);
			if (all_inputs == updated_input_count) {
				// alert('finished');
				var sent_id = '<?php echo $sent_id ?>';
				updateTemplateSentStatus(sent_id,'Completed');
			} else {
				updated_input_count++;
			}
		}).fail(function(xhr){
			console.log(xhr.responseText);
		});
	}

	function updateTemplateSentStatus(sent_id,status) {
		$.post('<?php echo base_url('DocuLynx/updateTemplateSentStatus') ?>',
			{sent_id,status}, function(data){
				// data = JSON.parse(data);
				console.log(data);
				location.reload();
			}).fail(function(xhr){
				console.log(xhr.responseText);
			});
		}

	function hiddenClone(element){
	  // Create clone of element
	  if (element == null) {
	  	var clone = document.getElementById('dummyDivToNodeHide').cloneNode(true);
	  	var style = clone.style;
		  style.position = 'relative';
		  style.top = window.innerHeight + 'px';
		  style.left = 0;
	  	document.body.appendChild(clone);
	  	return (clone);
	  } else {
	  	var clone = element.cloneNode(true);

		  // Position element relatively within the
		  // body but still out of the viewport
		  var style = clone.style;
		  style.position = 'relative';
		  style.top = window.innerHeight + 'px';
		  style.left = 0;

		  // Append clone to body and return the clone
		  document.body.appendChild(clone);

		  return clone;
	  }

	}
 	function makePDF() {



		var templatePage1 = document.getElementById('templatePage1');
		var templatePage2 = document.getElementById('templatePage2');
		var templatePage3 = document.getElementById('templatePage3');
		var templatePage4 = document.getElementById('templatePage4');
		var templatePage5 = document.getElementById('templatePage5');
		var templatePage6 = document.getElementById('templatePage6');
		var templatePage7 = document.getElementById('templatePage7');
		var templatePage8 = document.getElementById('templatePage8');
		var templatePage9 = document.getElementById('templatePage9');
		var templatePage10 = document.getElementById('templatePage10');
		var templatePage11 = document.getElementById('templatePage11');
		var templatePage12 = document.getElementById('templatePage12');
		var templatePage13 = document.getElementById('templatePage13');
		var templatePage14 = document.getElementById('templatePage14');

		var clone1 = hiddenClone(templatePage1);
		var clone2 = hiddenClone(templatePage2);
		var clone3 = hiddenClone(templatePage3);
		var clone4 = hiddenClone(templatePage4);
		var clone5 = hiddenClone(templatePage5);
		var clone6 = hiddenClone(templatePage6);
		var clone7 = hiddenClone(templatePage7);
		var clone8 = hiddenClone(templatePage8);
		var clone9 = hiddenClone(templatePage9);
		var clone10 = hiddenClone(templatePage10);
		var clone11 = hiddenClone(templatePage11);
		var clone12 = hiddenClone(templatePage12);
		var clone13 = hiddenClone(templatePage13);
		var clone14 = hiddenClone(templatePage14);


		var pdf = new jsPDF('p', 'pt', 'letter');
		var dropzones = $('#divDropZone .dropzone').length;
		// Use clone with htm2canvas and delete clone
html2canvas(clone1, {
    onrendered: function(canvas) {
      document.body.removeChild(clone1);
        var img = canvas.toDataURL('image/png');
        pdf.addImage(img,0,0);
        if (dropzones == 1) {
	        saveFilePdf(pdf);
	        $('#btnDownload').html('Thank you!');
	        setTimeout(function(){
	        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
	        },2000);
	        return false;
        }
        pdf.addPage();
        html2canvas(clone2, {
		    onrendered: function(canvas) {
		      document.body.removeChild(clone2);
		        var img = canvas.toDataURL('image/png');
		        pdf.addImage(img,0,0);
		        if (dropzones == 2) {
			        saveFilePdf(pdf);
			        $('#btnDownload').html('Thank you!');
			        setTimeout(function(){
			        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
			        },2000);
			        return false;
		        }
		        pdf.addPage();
		        html2canvas(clone3, {
				    onrendered: function(canvas) {
				      document.body.removeChild(clone3);
				        var img = canvas.toDataURL('image/png');
				        pdf.addImage(img,0,0);
				        if (dropzones == 3) {
					        saveFilePdf(pdf);
					        $('#btnDownload').html('Thank you!');
					        setTimeout(function(){
					        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
					        },2000);
					        return false;
				        }
				        pdf.addPage();
				        html2canvas(clone4, {
						    onrendered: function(canvas) {
						      document.body.removeChild(clone4);
						        var img = canvas.toDataURL('image/png');
						        pdf.addImage(img,0,0);
						        if (dropzones == 4) {
							        saveFilePdf(pdf);
							        $('#btnDownload').html('Thank you!');
							        setTimeout(function(){
							        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
							        },2000);
							        return false;
						        }
						        pdf.addPage();
						        html2canvas(clone5, {
								    onrendered: function(canvas) {
								      document.body.removeChild(clone5);
								        var img = canvas.toDataURL('image/png');
								        pdf.addImage(img,0,0);
								        if (dropzones == 5) {
									        saveFilePdf(pdf);
									        $('#btnDownload').html('Thank you!');
									        setTimeout(function(){
									        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
									        },2000);
									        return false;
								        }
								        pdf.addPage();
								        html2canvas(clone6, {
										    onrendered: function(canvas) {
										      document.body.removeChild(clone6);
										        var img = canvas.toDataURL('image/png');
										        pdf.addImage(img,0,0);
										        if (dropzones == 6) {
											        saveFilePdf(pdf);
											        $('#btnDownload').html('Thank you!');
											        setTimeout(function(){
											        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
											        },2000);
											        return false;
										        }
										        pdf.addPage();
										        html2canvas(clone7, {
												    onrendered: function(canvas) {
												      document.body.removeChild(clone7);
												        var img = canvas.toDataURL('image/png');
												        pdf.addImage(img,0,0);
												        if (dropzones == 7) {
													        saveFilePdf(pdf);
													        $('#btnDownload').html('Thank you!');
													        setTimeout(function(){
													        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
													        },2000);
													        return false;
												        }
												        pdf.addPage();
												        html2canvas(clone8, {
														    onrendered: function(canvas) {
														      document.body.removeChild(clone8);
														        var img = canvas.toDataURL('image/png');
														        pdf.addImage(img,0,0);
														        if (dropzones == 8) {
															        saveFilePdf(pdf);
															        $('#btnDownload').html('Thank you!');
															        setTimeout(function(){
															        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
															        },2000);
															        return false;
														        }
														        pdf.addPage();
														        html2canvas(clone9, {
																    onrendered: function(canvas) {
																      document.body.removeChild(clone9);
																        var img = canvas.toDataURL('image/png');
																        pdf.addImage(img,0,0);
																        if (dropzones == 9) {
																	        saveFilePdf(pdf);
																	        $('#btnDownload').html('Thank you!');
																	        setTimeout(function(){
																	        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																	        },2000);
																	        return false;
																        }
																        pdf.addPage();
																        html2canvas(clone10, {
																		    onrendered: function(canvas) {
																	      	document.body.removeChild(clone10);
																        	var img = canvas.toDataURL('image/png');
																        	pdf.addImage(img,0,0);
																        	if (dropzones == 10) {
																		        saveFilePdf(pdf);
																		        $('#btnDownload').html('Thank you!');
																		        setTimeout(function(){
																		        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																		        },2000);
																		        return false;
																	        }
																        	pdf.addPage();
																	        html2canvas(clone11, {
																			    onrendered: function(canvas) {
																		      	document.body.removeChild(clone11);
																	        	var img = canvas.toDataURL('image/png');
																	        	pdf.addImage(img,0,0);
																	        	if (dropzones == 11) {
																			        saveFilePdf(pdf);
																			        $('#btnDownload').html('Thank you!');
																			        setTimeout(function(){
																			        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																			        },2000);
																			        return false;
																		        }
																	        	pdf.addPage();
																		        html2canvas(clone12, {
																				    onrendered: function(canvas) {
																			      	document.body.removeChild(clone12);
																		        	var img = canvas.toDataURL('image/png');
																		        	pdf.addImage(img,0,0);
																		        	if (dropzones == 12) {
																				        saveFilePdf(pdf);
																				        $('#btnDownload').html('Thank you!');
																				        setTimeout(function(){
																				        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																				        },2000);
																				        return false;
																			        }
																		        	pdf.addPage();
																			        html2canvas(clone13, {
																					    onrendered: function(canvas) {
																				      	document.body.removeChild(clone13);
																			        	var img = canvas.toDataURL('image/png');
																			        	pdf.addImage(img,0,0);
																			        	if (dropzones == 13) {
																			        		saveFilePdf(pdf);
																					        $('#btnDownload').html('Thank you!');
																					        setTimeout(function(){
																					        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																					        },2000);
																					        return false;
																				        }
																			        	pdf.addPage();
																				        html2canvas(clone14, {
																						    onrendered: function(canvas) {
																					      	document.body.removeChild(clone14);
																				        	var img = canvas.toDataURL('image/png');
																				        	pdf.addImage(img,0,0);
																						        saveFilePdf(pdf);

																						        $('#btnDownload').html('Thank you!');
																						        setTimeout(function(){
																						        	$('#btnDownload').html('Download <i class="fas fa-download"></i>');
																						        },2000);
																						    }
																						});
																					    }
																					});
																				    }
																				});
																			    }
																				});
																		    }
																		});
																    }
																});
														    }
														});
												    }
												});
										    }
										});
								    }
								});
						    }
						});
				    }
				});
		    }
		});
    }
});


	function saveFilePdf(pdf) {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
		{
	     	var subject = '<?php echo $subject ?>';
		    // var datas = new FormData();
		    // datas.append("file" , pdf);
		    saveToServer(pdf.output());
	        function saveToServer(datas) {
	        	$.post('<?php echo base_url('DocuLynx/saveToServer') ?>',{datas}, function(data){
	        			// data = JSON.parse(data);
	        			console.log(data);
	        		}).fail(function(xhr){
	        			console.log(xhr.responseText);
	        		});
	        	}
		}
		else
		{
			var subject = '<?php echo $subject ?>';
		    pdf.save(subject+'.pdf');
		}
	}


		// // pdf.addPage();
		// html2canvas(clone2, {
		//     onrendered: function(canvas) {
		//       document.body.removeChild(clone2);
		//         var img = canvas.toDataURL('image/png');
		//         pdf.addImage(img,0,0);
		//     }
		// });



	}


});
 </script>