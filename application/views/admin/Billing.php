<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style> 
	.dataTables_filter {
		float: right !important;
	}
</style>

<div class="container-fluid p-t-20">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body"> 
					<h2>Invoices</h2>
					<table class="table stylish-table" id="table_invoices">
						<thead>
							<tr>
								<th>Name</th>
								<th>Invoice #</th>
								<th>Invoice Date</th>
								<th>Status</th> 
								<th>Tool</th> 
							</tr>
						</thead>
						<tbody> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="modal_pick_date" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">  
            <div class="modal-body">
            	<h3>Pick a Date</h3>
                 <input type="text" id="data-pay_date" name="" class="material_date form-control"> 
                 <input type="hidden" id="data-session_id" name="" class="form-control">
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-success waves-effect btn_pay_later" data-dismiss="modal">Submit</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script> 


	var table_invoices = $('#table_invoices').DataTable({'aaSorting': [[1, "desc"]],'pageLength': 50});
	get_table_invoices();
	function get_table_invoices() { 
		$.post('billing/get_table_invoices', function(data, textStatus, xhr) {
			data = JSON.parse(data); 
			table_invoices.clear().draw();
			$.each(data, function(index, val) {
				var invoice_url = '<a style="display: block;line-height: 1;" target="_blank" href="https://admin.creditlynx.com/emailCampaign/invoice?id='+val.session_id+'&edit=true">View Invoice</a>';
				var invoice_pay_later = '<a data-id="'+val.id+'" data-type="'+val.type+'" data-session_id="'+val.session_id+'" style="display: block;line-height: 1;" target="_blank" href="#" class="text-danger btn_pay_later_modal" value="">Pay Later</a>';
				table_invoices.row.add([val.client_name,val.invoice_no,moment(val.invoice_date).format('MM/DD/YYYY'),val.status,invoice_url+invoice_pay_later]);
			});
			table_invoices.draw();
		});
	}

	$('#table_invoices').on('click', '.btn_pay_later_modal', function(event) {
		event.preventDefault();
		var id = $(this).attr('data-id');
		var type = $(this).attr('data-type');
		var session_id = $(this).attr('data-session_id');
		$('#modal_pick_date').modal('show'); 
		$('#data-session_id').val(session_id);

	});

	$('.btn_pay_later').on('click', function(event) {
		event.preventDefault();
		var pay_later_date = $('#data-pay_date').val(); 
		var session = $('#data-session_id').val();

		$.post('<?php echo base_url('emailCampaign/pay_later_date') ?>', {session,pay_later_date}, function(data, textStatus, xhr) {
			location.reload();
		});

	});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 