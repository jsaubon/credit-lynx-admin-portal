<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?> 

<link href="<?php echo base_url('assets/plugins/gridstack/gridstack.css') ?>" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/lodash.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/gridstack.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/gridstack/gridstack.jQueryUI.js') ?>"></script>
<style type="text/css"> 
    .grid-stack-item-content {
        text-align: left ;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-8 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Dispute Team Dashboard</h2>
                    </div>
                    <div class="col-12 col-md-4 text-right">
                        <input type="text" name="" class="form-control" placeholder="Global Search" id="searchInputClient" style="width: 70%">

                        <?php $this->load->view('admin/clients/global_search');?>
                        <select  id="searchClient" class="hide">
                            <option value="">select client</option>s
                            <?php foreach ($all_clients as $key => $value): ?>

                                <option value="<?php echo $value['client_id'] . '_client' ?>"><?php echo $value['name'] ?></option>
                                <?php if ($value['client_type'] == 'Joint'): ?>
                                    <option value="<?php echo $value['client_joint_id'] . '_joint_' . $value['client_id'] ?>"><?php echo $value['joint_name'] ?></option>
                                <?php endif?>
                            <?php endforeach?>
                        </select>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-12">
                 
                        <div class="grid-stack" data-gs-width="12" data-gs-animate="yes">
                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="4" data-gs-height="8">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #000066 0%, #2e6ad8 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Need Processed <span class="pull-right"><?php echo count($review_clients['dfr_nulls']) + count($review_clients['social_date_order']) ?></span></h4> 
                                        </div>
                                        <div class="card-body slimbar" style="padding-top: 0px;overflow-y: scroll; min-height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget">
                                                    <?php foreach ($review_clients['dfr_nulls'] as $key => $value): ?>
                                                        <?php if ($value['type'] == 'client'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                        <?php if ($value['type'] == 'joint'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                    <?php endforeach?>
                                                    <?php foreach ($review_clients['social_date_order'] as $key => $value): ?>
                                                        <?php if ($value['type'] == 'client'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                        <?php if ($value['type'] == 'joint'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                    <?php endforeach?>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item" data-gs-x="4" data-gs-y="0" data-gs-width="4" data-gs-height="8">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #3572cf 0%, #75e8fb 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Expring DFR <span class="pull-right"><?php echo count($need_action) ?></span></h4>
                                        </div>
                                        <div class="card-body slimbar" style="padding-top: 0px; overflow-y: scroll; min-height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget">
                                                    <?php foreach ($need_action as $key => $value): ?>
                                                        <?php if ($value['type'] == 'client'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="mail-desc">DFR: <?php echo $value['dfr'] ?></span>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                        <?php if ($value['type'] == 'joint'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="mail-desc">DFR: <?php echo $value['dfr'] ?></span>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                    <?php endforeach?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-stack-item" data-gs-x="8" data-gs-y="0" data-gs-width="4" data-gs-height="8">
                                <div class="grid-stack-item-content">
                                    <div class="card">
                                        <div class="card-body bg-info" style="background: linear-gradient(to right, #8ee5f6 0%, #16d698 100%);">
                                            <h4 class="text-white card-title" style="margin-bottom: 0px;">Pending Docs <span class="pull-right"><?php echo count($verif_clients) ?></span></h4>
                                        </div>
                                        <div class="card-body slimbar" style="padding-top: 0px;overflow-y: scroll; min-height: 400px;">
                                            <div class="message-box">
                                                <div class="message-widget">
                                                    <?php foreach ($verif_clients as $key => $value): ?>
                                                        <?php if ($value['type'] == 'client'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                        <?php if ($value['type'] == 'joint'): ?>
                                                            <a class="link to_client_page" id="<?php echo $value['id'] ?>" type="<?php echo $value['type'] ?>" href="#">
                                                                <div class="mail-contnet">
                                                                    <h5><?php echo $value['name'] ?></h5>
                                                                    <span class="time" style="color: #3cc">View Account</span>
                                                                </div>
                                                            </a>
                                                        <?php endif?>
                                                    <?php endforeach?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script>

    $(function() {
        $('.grid-stack').gridstack({
            width: 12,
            alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            resizable: {
                handles: 'e, se, s, sw, w'
            }
        });

        
    });

</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
 