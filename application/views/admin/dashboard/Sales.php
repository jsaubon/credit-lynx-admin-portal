<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?> 

<style type="text/css"> 
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-8 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Sales Team Dashboard</h2>
                    </div>

                    <div class="col-12 col-md-4 text-right">
                        <input type="text" name="" class="form-control" placeholder="Global Search" id="searchInputClient" style="width: 70%">

                        <?php $this->load->view('admin/clients/global_search');?>
                        <select  id="searchClient" class="hide">
                            <option value="">select client</option>s
                            <?php foreach ($all_clients as $key => $value): ?>

                                <option value="<?php echo $value['client_id'] . '_client' ?>"><?php echo $value['name'] ?></option>
                                <?php if ($value['client_type'] == 'Joint'): ?>
                                    <option value="<?php echo $value['client_joint_id'] . '_joint_' . $value['client_id'] ?>"><?php echo $value['joint_name'] ?></option>
                                <?php endif?>
                            <?php endforeach?>
                        </select>
                    </div>
                </div> 
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script> 
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>
 