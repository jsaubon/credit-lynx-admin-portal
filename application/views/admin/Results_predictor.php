<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
    <style>
        /*svg:not(:root) {
            height: 430px;
        }

        #morris-bar-chart svg {
            height: 700px;
        }*/
    </style>

    <link href="<?php echo base_url('assets/plugins/morrisjs/morris.css') ?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/plugins/raphael/raphael-min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morrisjs/morris.js') ?>"></script>
<style> 
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Results Preditor</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div  class="card-body" style="padding-bottom: 20px;"> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Creditor Search</span>
                                    </div>
                                    <input type="text" class="form-control" id="bank_account_group" aria-describedby="basic-addon3">
                                </div>

                                <h3 class="abel_account_group m-t-20"></h3>

                                <div >
                                    
                                    <div class="row">
                                        <div class="col-12 col-md-4 offset-md-4 card">
                                            <div class="card-body">
                                                <h5 style="text-decoration: underline;">Attempts <span class="attempts_count"></span></h5> 
                                                <div class="row">
                                                    <div class="col-12 col-md-4"> 
                                                        Results:
                                                    </div>
                                                    <div class="col-12 col-md-8 results_count">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                                
                                        </div> 
                                                
                                    </div>
                                </div>
                                <!--         
                                
                                <div class="table-responsive"> 
                                    <table class="table stylish-table m-t-20" id="pageTable">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th> 
                                                <th>Round Alert Date</th>
                                                <th>Schedule</th>
                                            </tr>
                                        </thead>
                                        <tbody>   
                                        </tbody>
                                    </table>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script>
$(document).ready(function(){  
            
    get_account_groups();
    function get_account_groups() {
        $.post('results_predictor/get_account_groups', function(data, textStatus, xhr) { 
            data = JSON.parse(data); 
            var source = [];
            $.each(data, function(index, val) {
                source.push({
                    value: val.account_group,
                    label: val.account_group,
                    desc: ''
                });
            });

            $("#bank_account_group" ).autocomplete({
                minLength: 0,
                source: source,
                focus: function( event, ui ) {
                    $( "#bank_account_group" ).val( ui.item.label );
                    $( ".label_account_group" ).html( ui.item.label );
                    // $( "#bank_account_group" ).trigger('keyup');
                    return false;
                },
                select: function( event, ui ) {
                    $( "#bank_account_group" ).val( ui.item.label );
                    $( ".label_account_group" ).html( ui.item.label );
                    // $( "#bank_account_group" ).trigger('keyup');
                    get_attempts( ui.item.label );
                    return false;
                }
            });
        });
    }


    function get_attempts(account_group){
        $.post('results_predictor/get_attempts', {account_group}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
            var Added = 0;
            var Deleted = 0;
            var Fixed = 0;
            var Hold = 0;
            var In_Process = 0;
            var Intervening = 0;
            var Not_Reporting = 0;
            var Remains = 0;
            var Updated = 0;
            var Verified = 0;
            var attempts = 0;
            $.each(data, function(index, val) {
                attempts += parseInt(val.count);
                Added = val.account_status == 'Added' ? Added + parseInt(val.count) : Added;
                Deleted = val.account_status == 'Deleted' ? Deleted + parseInt(val.count) : Deleted;
                Fixed = val.account_status == 'Fixed' ? Fixed + parseInt(val.count) : Fixed;
                Hold = val.account_status == 'Hold' ? Hold + parseInt(val.count) : Hold;
                In_Process = val.account_status == 'In Process' ? In_Process + parseInt(val.count) : In_Process;
                Intervening = val.account_status == 'Intervening' ? Intervening + parseInt(val.count) : Intervening;
                Not_Reporting = val.account_status == 'Not Reporting' ? Not_Reporting + parseInt(val.count) : Not_Reporting;
                Remains = val.account_status == 'Remains' ? Remains + parseInt(val.count) : Remains;
                Updated = val.account_status == 'Updated' ? Updated + parseInt(val.count) : Updated;
                Verified = val.account_status == 'Verified' ? Verified + parseInt(val.count) : Verified; 
            });

            var Added_percent = ((Added / attempts) * 100).toFixed(2);
            var Deleted_percent = ((Deleted / attempts) * 100).toFixed(2);
            var Fixed_percent = ((Fixed / attempts) * 100).toFixed(2);
            var Hold_percent = ((Hold / attempts) * 100).toFixed(2);
            var In_Process_percent = ((In_Process / attempts) * 100).toFixed(2);
            var Intervening_percent = ((Intervening / attempts) * 100).toFixed(2);
            var Not_Reporting_percent = ((Not_Reporting / attempts) * 100).toFixed(2);
            var Remains_percent = ((Remains / attempts) * 100).toFixed(2);
            var Updated_percent = ((Updated / attempts) * 100).toFixed(2);
            var Verified_percent = ((Verified / attempts) * 100).toFixed(2);
            var attempts_percent = ((attempts / attempts) * 100).toFixed(2);

            var results_count = [];
            if (In_Process != 0) { results_count.push(In_Process+'% In Process') };
            if (Added != 0) { results_count.push(Added+'% Added') };
            if (Deleted != 0) { results_count.push(Deleted+'% Deleted') };
            if (Fixed != 0) { results_count.push(Fixed+'% Fixed') };
            if (Hold != 0) { results_count.push(Hold+'% Hold') };
            if (Intervening != 0) { results_count.push(Intervening+'% Intervening') };
            if (Not_Reporting != 0) { results_count.push(Not_Reporting+'% Not Reporting') };
            if (Remains != 0) { results_count.push(Remains+'% Remains') };
            if (Updated != 0) { results_count.push(Updated+'% Updated') };
            if (Verified != 0) { results_count.push(Verified+'% Verified') };

            results_count = results_count.join('<br>');

            $('.attempts_count').html(attempts);
            $('.results_count').html(results_count);

        });
    }

    // Morris bar chart
    
});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 