<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?>  
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title  ?></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== --> 

                <div id="profileSection" class="row fade show"> 
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"> 
                                <div class="tab-pane active show" id="profile" role="tabpanel">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 text-center b-r" > 
                                                <form id="uploadProfilePictureForm" enctype="multipart/form-data">
                                                    <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="../assets/images/LynxLogo.png" data-show-remove="false" data-height="150" />
                                                    <input type="hidden" name="admin_id">
                                                </form>   
                                                <h3 class="name m-b-0"></h3>
                                            </div> 
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h3>Other Information</h3>
                                                <span>Last Login: <span class="last_login"></span></span><br> 
                                            </div> 
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>User Account Information</h5>
                                                <span>Username: <span class="username"></span></span><br>
                                                <span>Password: <span class="password"></span></span><br>
                                            </div>
                                        </div> 
                                        <hr>


                                        <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                            <li class="nav-item"> <a id="tasks-tab-header" class="nav-link active show" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="true">Tasks</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a task</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_task" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Task Date">
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewTask" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="task_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                        
                                                        
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="notes-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"><br>sticky?</i>
                                                            
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewNote" class="btn btn-success waves-effect waves-light" type="button">Save Note</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="note_list_sticky" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                        <div id="note_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="row"> 
                                                <div class="col-md-12 col-xs-12">
                                                    <input type="hidden" name="current_photo" id="current_photo">
                                                    <h4 style="margin-bottom: 17px">Basic Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_name" class="control-label">Name:</label>
                                                        <input type="text" name="name" required="" class="form-control" id="profile_name">
                                                        <span class="bar"></span>
                                                    </div> 
                                                    <h4 style="margin-bottom: 17px">User Account Information</h4>
                                                    <div class="form-group">
                                                        <label for="profile_username" class="control-label">Username:</label>
                                                        <input type="text" name="username" required="" class="form-control" id="profile_username">
                                                        <span class="bar"></span> 
                                                        <small id="profile_errorUsername" class="hide form-control-feedback text-danger"> Username already exist!. </small>  
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="profile_password" class="control-label">Password:</label>
                                                        <input type="password" name="password" required="" class="form-control" id="profile_password">
                                                        <span class="bar"></span>
                                                    </div>

                                                </div>  
                                                <div class="col-12">
                                                    <button id="btnProfileUpdate" type="button" class="btn btn-success pull-right waves-effect waves-light">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


 
 <script>
$(document).ready(function() {
    $('.left_nav_'+'<?php echo $page_controller ?>').addClass('active');
    var newProcessor = false;
    var admin_id = '<?php echo $userdata['user_id'] ?>'; 

    newProcessor = false; 
    var name = '<?php echo $userdata['name'] ?>'; 
    getDetails(admin_id);
    getTasks(admin_id);
    getNotes(admin_id);
    $('#profileSection').addClass('show');
    $('#listSection').addClass('hide');
    $('#profile_active').removeClass('hide');
    $('#profile_active').html(name);
 

    $('#pageTable').DataTable({ 
        'aaSorting': [],
        "aoColumnDefs": [{ 
            'bSortable': false, 
            'aTargets': [0] 
        }],
        dom: 'Bfrtip',
        "pageLength": 100
    }); 

    $('#pageTable').removeClass('dataTable'); 

    

    $('#username').on('keyup',function(){
        var username = $(this);
        if (newProcessor) {
            checkIfExist(username);
        }
        
    });

    $('#btnProfileUpdate').on('click',function(){
        var button = $(this);
        var name = $('#profile_name').val(); 
        var username = $('#profile_username').val();
        var password = $('#profile_password').val();
        var current_photo = $('#current_photo').val();
        updateProfile(button,admin_id,current_photo,name,username,password);


    });

    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,admin_id,task,task_date);
            } else {
                task_date.focus();
            }
            
        } else {
            task.focus();
        }
    });

    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });

    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,admin_id,note,note_sticky);
        } else {
            note.focus();
        }
    });

    $('#uploadProfilePicture').on('change',function(){
        $.ajax({ 
            url: '<?php echo $page_controller ?>/uploadProfilePicture',
            type: 'POST', 
            data: new FormData($('#uploadProfilePictureForm')[0]), 
            cache: false,
            contentType: false,
            processData: false, 
            success: function(data) { 

            }
        });
    });

    function getDetails(admin_id) {
        $.post('<?php echo $page_controller ?>/getDetails',
            {admin_id},function(data){
                data = JSON.parse(data);  
                $('input[name=current_photo]').val(data[0]['photo']);
                $('.dropify-render').find('img').attr('src','../assets/images/users/<?php echo $page_folder ?>/'+data[0]['photo']);
                
                $('#profileSection').find('#profile_picture').attr('src','../assets/images/users/<?php echo $page_folder ?>/'+data[0]['photo']);
                // console.log(moment(data[0]['date_of_birth']).format('YYYY-MM-DD'));
                $('input[name=date_of_birth]').val(moment(data[0]['date_of_birth']).format('YYYY-MM-DD'));
                $.each(data,function(key,value){ 
                    $.each(value,function(k,v){
                        $('input[type=text][name='+k+']').val(v);
                        $('input[type=email][name='+k+']').val(v);
                        $('input[type=password][name='+k+']').val(v);
                        $('input[type=hidden][name='+k+']').val(v);
                        $('select[name='+k+']').val(v);

                        $('.'+k).html(v);
                        // console.log(k+ ' ' +v);
                    })
                });

                $('select').trigger('change');

                var inputs =  $('#formAddUser input'); 
                $.each(inputs,function(key,input){ 
                    $(input).closest('.form-group').addClass('focused');
                });
 
                $('input[name=username]').prop('readonly',true); 
 
                if (data[0]['last_login'] != null) {
                    $('.last_login').html(moment(data[0]['last_login']).format('lll'));
                }
                

                
        });
    }

    
    function checkIfExist(username) {
        var username_val = username.val();
        $.post('<?php echo $page_controller ?>/checkIfExist',
            {username_val},function(data){
                // console.log(data);
                if (data == '0') {
                    username.removeClass('form-control-danger');
                    username.closest('.form-group').removeClass('has-danger');
                    username.closest('.form-group').removeClass('has-error');
                    $('#errorUsername').addClass('hide');
                } else { 
                    username.addClass('form-control-danger');
                    username.closest('.form-group').addClass('has-danger');
                    username.closest('.form-group').addClass('has-error');
                    $('#errorUsername').removeClass('hide');
                }
            });
    }

    function updateProfile(button,admin_id,current_photo,name,username,password) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveDetail',
            {admin_id,current_photo,name,username,password},function(data){
                // console.log(data);
                getDetails(admin_id);
                button.prop('disabled',false);
                $('.nav-link').removeClass('active');
                $('.nav-link').removeClass('show');
                $('.tab-pane').removeClass('active');
                $('.tab-pane').removeClass('show');
                $('#profile_tab').find('a').addClass('active');
                $('#profile_tab').find('a').addClass('show');
                $('#profile').addClass('active');
                $('#profile').addClass('show');
                
                $('#tasks-tab-header').addClass('active');
                $('#tasks-tab-header').addClass('show');
                $('#tasks-tab').addClass('active');
                $('#tasks-tab').addClass('show');

                swal({   
                    title: "Profile Updated!",   
                    text: "you will be redirected to Profile section, Thank you!",   
                    timer: 2000,   
                    showConfirmButton: false 
                });
            });
    }

    function getTasks(admin_id) {
        $.post('<?php echo $page_controller ?>/getTasks',
            {admin_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-warning waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }

                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.task_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.task+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });

                $('.btnUpdateTaskDone').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,0);
                });

                $('.btnUpdateTaskActive').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,1);
                });

                $('.btnUpdateTaskDelete').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,2);
                });
            });
    }

    function saveTask(button,admin_id,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveTask',
            {admin_id,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(admin_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateTask',
            {task_id,task_active},function(data){
                getTasks(admin_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getNotes(admin_id) {
        $.post('<?php echo $page_controller ?>/getNotes',
            {admin_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }

                    var newNote = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.note_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.note+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });

                $('.btnUpdateNoteSticky').on('click',function(){
                    var button = $(this);
                    var note_id = $(this).closest('.sl-item').attr('value');
                    if (button.hasClass('text-warning')) {
                        updateNote(button,note_id,0);
                    } else {
                        updateNote(button,note_id,1);
                    }
                    
                }); 
            });
    }

    function saveNote(button,admin_id,note,note_sticky){
        var note_ = note;
        var note = note.val(); 
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveNote',
            {admin_id,note,note_sticky},function(data){
                // console.log(data); 
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                getNotes(admin_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateNote',
            {note_id,note_sticky},function(data){
                getNotes(admin_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 

