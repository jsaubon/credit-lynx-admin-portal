<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
 <link href="<?php echo base_url('assets/js/fullcalendar/fullcalendar.css') ?>" rel="stylesheet" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css') ?>">
<style>
    .fc-event {
        cursor: pointer;
    }

    a:hover {
        color: white !important;
    }

    #calendar_app_from_container .select2,#calendar_app_to_container .select2 {
        width: calc(100% - 206px) !important;;
    }

    #otherDetailsContainer .input-group .input-group-text {
        min-width: 42px !important;
    }

    #btnAppMoreOptions:hover,#btnAppMoreNotif:hover  {
        color: #21b3c6 !important;
    }
 
    .ui-autocomplete {
        z-index: 99999999 !important;
    }

    .select2-container--default .select2-selection--single {
        height: 38px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 38px !important;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?></h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 id="calendarWaitingRender" class="text-center">Getting appointments calendar
                                <br>please wait ...</h2>
                                <div class="col-10 offset-1">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div id="modalAppointmentInformation" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Appointment Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div> 
            <form method="post" id="formSaveAppointment">
            <div class="modal-body"> 
                <div class="row">
                    
                    <div class="col-12">  
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="app_id" id="calendar_app_id"> 
                                <div class="input-group" id="calendar_app_to_container">
                                    <div class="input-group-prepend">
                                        <label style="width: 61.23px !important;" class="input-group-text">For</label>
                                    </div>
                                    <div class="input-group-prepend">
                                        <select style="width: 136px !important" class="form-white form-control" data-placeholder="" id="calendar_app_to">
                                            <option value="">choose user</option> 
                                            <option value="Lead">Lead</option> 
                                            <option value="Client">Client</option>
                                            <option value="Agent">Agent</option>
                                            <option value="Broker">Broker</option>  
                                        </select> 
                                    </div>
                                    
                                    <select class="select2 form-white form-control" data-placeholder="" id="calendar_app_to_id"> 
                                         
                                    </select>  
                                    
                                </div> 

                                <div class="input-group" id="calendar_app_from_container">
                                    <div class="input-group-prepend">
                                        <label style="width: 61.24px !important" class="input-group-text">With</label>
                                    </div>
                                    <div class="input-group-prepend">
                                        <select class="form-white form-control" data-placeholder="" id="calendar_app_from">
                                            <option value="">choose user</option> 
                                            <option value="Sales Team">Sales Team</option> 
                                            <option value="Administrator">Administrator</option> 
                                        </select> 
                                    </div>
                                    
                                    <select class="select2 form-white form-control" data-placeholder="" id="calendar_app_from_id"> 
                                         
                                    </select>  
                                    
                                </div>

                                <h4 class="m-t-20" for="calendar_appointment">Appointment</h4>
                                <div class="input-group"> 
                                    <input type="text" required class="form-control bg-success text-white" name="appointment" id="calendar_appointment">
                                    <div class="input-group-append">
                                        <select class="form-control form-white" data-placeholder="" name="app_color" id="calendar_app_color">
                                            <option value="bg-success">Evaluation</option>
                                            <option value="bg-grey">Busy</option>
                                            <option value="bg-green">Meeting</option> 
                                        </select>
                                    </div>
         
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control material_date" name="start" id="calendar_date_start"> 
                                    <select class="form-control" id="calendar_time_start"> 
                                        <option selected="" value="10:00">10:00am</option>
                                        <option value="10:30">10:30am</option>
                                        <option value="11:00">11:00am</option>
                                        <option value="11:30">11:30am</option>
                                        <option value="12:00">12:00pm</option>
                                        <option value="12:30">12:30pm</option> 
                                        <option value="13:00">1:00pm</option>
                                        <option value="13:30">1:30pm</option> 
                                        <option value="14:00">2:00pm</option>
                                        <option value="14:30">2:30pm</option> 
                                        <option value="15:00">3:00pm</option>
                                        <option value="15:30">3:30pm</option> 
                                        <option value="16:00">4:00pm</option>
                                        <option value="16:30">4:30pm</option> 
                                        <option value="17:00">5:00pm</option>   
                                    </select>
                                    <script>
                                        $('#calendar_time_start').on('change',function(){
                                            var index = $(this).find('option:selected').index(); 
                                            $('#calendar_time_end option:eq('+(index+1)+')').attr('selected', 'selected');
                                        });
                                    </script>
                                    <span class="input-group-text bg-success b-0 text-white">TO</span>
                                    
                                    <select class="form-control" id="calendar_time_end">
                                        <option value="10:00">10:00am</option>
                                        <option selected="" value="10:30">10:30am</option>
                                        <option value="11:00">11:00am</option>
                                        <option value="11:30">11:30am</option>
                                        <option value="12:00">12:00pm</option>
                                        <option value="12:30">12:30pm</option> 
                                        <option value="13:00">1:00pm</option>
                                        <option value="13:30">1:30pm</option> 
                                        <option value="14:00">2:00pm</option>
                                        <option value="14:30">2:30pm</option> 
                                        <option value="15:00">3:00pm</option>
                                        <option value="15:30">3:30pm</option> 
                                        <option value="16:00">4:00pm</option>
                                        <option value="16:30">4:30pm</option> 
                                        <option value="17:00">5:00pm</option>  
                                        <option value="17:30">5:30pm</option> 
                                    </select>
                                    <input type="text" class="form-control material_date" name="end" id="calendar_date_end"> 
                                </div>
                                
                            </div> 
                        </div> 
                        <div class="row m-t-10  ">
                            <div class="col-12">
                                <h4>Event Details</h4>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text"><i class="fas fa-phone "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_phone" name="app_phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text"><i class="fas fa-envelope "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_email" name="app_email" class="form-control" placeholder="Email">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label style="width: 42px" class="input-group-text"><i class="fas fa-map-marker "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_location" name="app_location" class="form-control" placeholder="Location">
                                </div>
                                
                                
                                <a href="#" id="btnAppMoreOptions" class="pull-right m-t-10">More Options</a>
                                <script>
                                    $("#btnAppMoreOptions").on('click',function(){
                                        if ($('#otherDetailsContainer').hasClass('hide')) {
                                            $('#otherDetailsContainer').removeClass('hide');
                                        } else {
                                            $('#otherDetailsContainer').addClass('hide');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="row m-t-10 hide animated fadeIn" id="otherDetailsContainer">
                            <div class="col-12">
                                <a href="#" id="btnAppMoreNotif" class="m-t-10">Add Notifications</a>
                                <script>
                                    $("#btnAppMoreNotif").on('click',function(){
                                        var sectionAddNotification = $('#sectionAddNotification');
                                        var newNotif = '<section class="input-group animated fadeIn">\
                                                            <div class="input-group-prepend">\
                                                                <label class="input-group-text"><i class="fas fa-bell"></i></label>\
                                                            </div>\
                                                            <select id="calendar_notif_type" name="notif_type[]" class="form-control calendar_notif_type">\
                                                                <option value="Email">Email</option>\
                                                                <option selected="" value="Notification">Notification</option>\
                                                            </select>\
                                                            <input type="number" id="calendar_notif_time" name="notif_time[]" class="form-control calendar_notif_time" value="30" style="width: 40px">\
                                                            <select id="calendar_notif_schedule" name="notif_schedule[]" class="form-control calendar_notif_schedule">\
                                                                <option selected="" value="minutes">minutes</option>\
                                                                <option value="hours">hours</option>\
                                                                <option value="days">days</option>\
                                                                <option value="weeks">weeks</option>\
                                                            </select>\
                                                            <div class="input-group-append">\
                                                                <label class="input-group-text btnDeleteAppointmentNotification"  style="min-width: 30px !important;cursor:pointer"><i class="fas fa-times"></i></label>\
                                                            </div>\
                                                        </section>';
                                        sectionAddNotification.append(newNotif);
                                        $('.btnDeleteAppointmentNotification').on('click',function(){
                                            var button = $(this);
                                            var section = button.closest('section');
                                            section.remove();
                                        });
                                    });
                                </script>
                                <section id="sectionAddNotification">
                                    
                                </section>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Note</label>
                                    </div>
                                    <textarea id="calendar_app_note" class="form-control app_note" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: block;min-height: 70px">
                <button type="button" id="btnDeleteAppointment" class="btn hide btn-danger waves-effect pull-left" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-danger waves-effect pull-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Save Appointment</button> 
            </div> 
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/plugins/calendar/jquery-ui.min.js') ?>"></script> 

<script src="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.js') ?>"></script> 
<script src="http://code.jquery.com/jquery-migrate-3.0.0.js"></script>
<script src='<?php echo base_url('assets/js/fullcalendar/fullcalendar.min.js') ?>'></script>  
<script src='<?php echo base_url('assets/js/fullcalendar/gcal.min.js') ?>'></script>  

 
<script>

    $("#calendar_appointment" ).autocomplete({
      minLength: 0,
      source: [{
                value: 'Out of Office',
                label: 'Out of Office',
                desc: '', 
              },{
                value: 'In Office',
                label: 'In Office',
                desc: '', 
              },{
                value: 'Over the Phone',
                label: 'Over the Phone',
                desc: '', 
              },{
                value: 'Account Review',
                label: 'Account Review',
                desc: '', 
              }
              ],
      focus: function( event, ui ) {
        $( "#calendar_appointment" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#calendar_appointment" ).val( ui.item.label );  
         
        return false;
      }
    })
// // CALENDAR START
 

    var appointments = [];
    getAppointments('<?php echo $userdata['login_type'] ?>',null);  

    $('#calendar_app_color').on('change',function(){
        $('#calendar_appointment').removeClass();
        $('#calendar_appointment').addClass('form-control');
        $('#calendar_appointment').addClass('text-white');

        var app_color = $(this).val();
        $('#calendar_appointment').addClass(app_color);
    });

    $('#formSaveAppointment').on('submit',function(e){
        e.preventDefault();
        var app_id = $('#calendar_app_id').val();
        var appointment = $('#calendar_appointment').val();
        var date_start = $('#calendar_date_start').val();
        var date_end = $('#calendar_date_end').val();
        var app_color = $('#calendar_app_color').val(); 
        var app_to = $('#calendar_app_to').val();
        var app_to_id = $("#calendar_app_to_id").val();
        var app_from = $('#calendar_app_from').val();
        var app_from_id = $("#calendar_app_from_id").val();
        var app_phone = $("#calendar_app_phone").val();
        var app_location = $("#calendar_app_location").val();
        var app_email = $("#calendar_app_email").val();
        var app_note = $("#calendar_app_note").val();
        var notif_type = $(".calendar_notif_type");
        var notif_time = $(".calendar_notif_time");
        var notif_schedule = $(".calendar_notif_schedule"); 
        var date_start = moment($("#calendar_date_start").val()).format('YYYY-MM-DD');; 
        var time_start = $("#calendar_time_start").val(); 
        var date_end = moment($("#calendar_date_end").val()).format('YYYY-MM-DD');; 
        var time_end = $("#calendar_time_end").val(); 

        date_start = date_start + ' ' +time_start; 
        date_end = date_end + ' ' +time_end; 


        var notif_type_arr = [];
        $.each(notif_type,function(key,value){
            notif_type_arr.push($(value).val());
        });
        var notif_time_arr = [];
        $.each(notif_time,function(key,value){
            notif_time_arr.push($(value).val());
        });
        var notif_schedule_arr = [];
        $.each(notif_schedule,function(key,value){
            notif_schedule_arr.push($(value).val());
        });


        saveAppointment('create',app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type_arr,notif_time_arr,notif_schedule_arr);
        
    });

    $('#btnDeleteAppointment').on('click',function(e){
        var app_id = $('#calendar_app_id').val();
        var appointment = $('#calendar_appointment').val();
        var date_start = moment($('#calendar_date_start').val()).format('lll');
        swal({   
            title: "Are you sure?",   
            text: 'Appointment '+appointment+" that starts at "+date_start+" will be deleted!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAppointment(app_id);
            }
        }).then(function(){ 
            
        });
    });

    $('#calendar_app_to').on('change',function(){
        var table = $(this).val();
        getAppToSelect(table);
        console.log(table);
    });

    $('#calendar_app_from').on('change',function(){
        var table = $(this).val();
        console.log(table);
        getAppFromSelect(table);
    });

    

    function saveAppointment(type,app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type,notif_time,notif_schedule) {
        $.post('<?php echo base_url('admin/appointments/saveAppointment') ?>',
            {app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type,notif_time,notif_schedule},function(data){ 
                $('#formSaveAppointment')[0].reset();
                
                $('#modalAppointmentInformation').modal('hide');
                if (type == 'create') {
                    getAppointments('<?php echo $userdata['login_type'] ?>',null); 
                    swal('Success','Appointment saved!','success');
                }   

                $('#otherDetailsContainer').addClass('hide');
                $('#sectionAddNotification').empty();


            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }   

    function getAppointments(app_to,app_to_id) {
        $.post('<?php echo base_url('admin/appointments/getAppointments') ?>',
            {app_to,app_to_id},function(data){
                data = JSON.parse(data); 
                $('#calendarWaitingRender').addClass('hide');
                appointments = [];
                $.each(data,function(key,value){ 
                    appointments.push({ 
                        id: value.app_id,
                        title: 'For: '+value.to_name+' ('+value.app_to+')<br>With: '+value.from_name+' ('+value.app_from+')<br>Appt: '+value.appointment,
                        description: value.app_from+'_'+value.app_to,
                        start: value.date_start,
                        end: value.date_end,
                        className: value.app_color
                    }); 
                });
                $('#calendar').fullCalendar('destroy');
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    }, 
                    defaultView: 'month',
                    displayEventTime: false,
                    events: appointments,
                    dayClick: function(date, jsEvent, view) { 
                        $('#formSaveAppointment')[0].reset();
                        $('#calendar_app_color').val('bg-success');
                        $('#calendar_date_start').val(date.format('M/D/Y')); 
                        $('#calendar_date_end').val(date.format('M/D/Y')); 
                        $('#modalAppointmentInformation').modal('toggle');
                        $('#calendar_appointment').removeClass();
                        $('#calendar_appointment').addClass('form-control');
                        $('#calendar_appointment').addClass('text-white'); 
                        $('#calendar_appointment').addClass('bg-success');
                        $('#btnDeleteAppointment').addClass('hide');

                        $('#calendar_app_to_id').val('');
                        $('#calendar_app_to_id').empty();
                        $('#calendar_app_from_id').val('');
                        $('#calendar_app_from_id').empty();

                        $('#btnDeleteAppointment').addClass('hide');
                        $('#otherDetailsContainer').addClass('hide');
                        $('#sectionAddNotification').empty();


                    },
                    eventRender: function(event, element) {   
                        element.find('.fc-title').html(element.find('.fc-title').text());
                        element.find('.fc-content').css({'text-align':'left'});
                        // element.find('.fc-content').css({'background-color':'white'});
                        // element.find('.fc-content').css({'color':'black'});
                        // element.find('.fc-content').css({'border-left':'8px solid '+event.eventBackground});
                    },
                    eventClick: function(event){  
                        var app = (event.description).split('_');
                        var app_from = app[0];
                        var app_to = app[1];
                        getAppToSelect(app_to,event.id);
                        getAppFromSelect(app_from,event.id);
                        $('#btnDeleteAppointment').removeClass('hide');

                        $('#modalAppointmentInformation').modal('toggle');
                        $('#calendar_app_id').val(event.id); 
                        $('#calendar_appointment').removeClass();
                        $('#calendar_appointment').addClass('form-control');
                        $('#calendar_appointment').addClass('text-white');  
                        $('#calendar_appointment').addClass((event.className)[0]);
                    },
                    viewRender: function(view) {  
                       // alert(view.name)   
                        // if(view.name == 'agendaWeek')
                        // {
                            // $('#calendar').fullCalendar('option', 'contentHeight', 700);            
                        // }  
                    } 
                     
                }); 
 
            });
    }

    function deleteAppointment(app_id) {
        $.post('<?php echo base_url('admin/appointments/deleteAppointment') ?>',
            {app_id},function(data){
                getAppointments('<?php echo $userdata['login_type'] ?>',null); 
                swal("Success!", "Appointment successfully deleted!", "success");
            });
    }

    function getAppToSelect(table,app_id) {
        $.post('<?php echo $page_controller ?>/getAppToSelect',
            {table},function(data){
                data = JSON.parse(data);
                var calendar_app_to_id = $('#calendar_app_to_id');
                calendar_app_to_id.empty();
                $.each(data,function(key,value){
                    var newOption = '<option value="'+value.id+'">'+value.name+'</option>';
                    calendar_app_to_id.append(newOption);
                });
                
                calendar_app_to_id.select2();

                if (app_id != null) {
                    getAppointmentByAppID(app_id);
                }
            });
    }

    function getAppFromSelect(table,app_id) {
        $.post('<?php echo $page_controller ?>/getAppToSelect',
            {table},function(data){
                data = JSON.parse(data);
                var calendar_app_from_id = $('#calendar_app_from_id');
                calendar_app_from_id.empty();
                $.each(data,function(key,value){
                    var newOption = '<option value="'+value.id+'">'+value.name+'</option>';
                    calendar_app_from_id.append(newOption);
                });
                calendar_app_from_id.select2();

                if (app_id != null) {
                    getAppointmentByAppID(app_id);
                }
            });
    }

    function getAppointmentByAppID(app_id) {
        $.post('<?php echo $page_controller ?>/getAppointmentByAppID',
            {app_id},function(data){
                data = JSON.parse(data); 
                $.each(data,function(key,value){
                    $.each(value,function(k,v){
                        $('#calendar_'+k).val(v);
                    });
                    $('#calendar_app_to_id').trigger('change');
                    $('#calendar_app_from_id').trigger('change');

                    var date_start = moment(value.date_start).format('MM/DD/YYYY');
                    var time_start = moment(value.date_start).format('HH:mm');
                    var date_end = moment(value.date_end).format('MM/DD/YYYY');
                    var time_end = moment(value.date_end).format('HH:mm');
                    
        
 
                    $('#calendar_date_start').val(date_start);
                    $('#calendar_time_start').val(time_start);
                    $('#calendar_date_end').val(date_end);
                    $('#calendar_time_end').val(time_end);

                    getAppointmentNotificationByAppID(app_id);
                });
            });
    }

    function getAppointmentNotificationByAppID(app_id) {
        $.post('<?php echo $page_controller ?>/getAppointmentNotificationByAppID',
            {app_id},function(data){
                data = JSON.parse(data);  
                var sectionAddNotification = $('#sectionAddNotification');
                sectionAddNotification.empty();
                if (data.length != 0) {
                    $('#otherDetailsContainer').removeClass('hide');
                    $.each(data,function(key,value){
                        
                        var newNotif = '<section class="input-group animated fadeIn" id="an_id_'+value.an_id+'">\
                                            <div class="input-group-prepend">\
                                                <label class="input-group-text"><i class="fas fa-bell"></i></label>\
                                            </div>\
                                            <select name="notif_type[]" class="form-control calendar_notif_type_">\
                                                <option value="Email">Email</option>\
                                                <option value="Notification">Notification</option>\
                                            </select>\
                                            <input type="number" name="notif_time[]" class="form-control calendar_notif_time_" value="'+value.notif_time+'" style="width: 40px" >\
                                            <select name="notif_schedule[]" class="form-control calendar_notif_schedule_">\
                                                <option selected="" value="minutes">minutes</option>\
                                                <option value="hours">hours</option>\
                                                <option value="days">days</option>\
                                                <option value="weeks">weeks</option>\
                                            </select>\
                                            <div class="input-group-append">\
                                                <label class="input-group-text btnDeleteAppointmentNotification_"  style="min-width: 30px !important;cursor:pointer"><i class="fas fa-times"></i></label>\
                                            </div>\
                                        </section>';
                        sectionAddNotification.append(newNotif);
                        $('#an_id_'+value.an_id).find('.calendar_notif_type_').val(value.notif_type);
                        $('#an_id_'+value.an_id).find('.calendar_notif_schedule_').val(value.notif_schedule);
                    });


                    $('.btnDeleteAppointmentNotification_').on('click',function(){
                        var button = $(this);
                        var section = button.closest('section');
                        var an_id = section.attr('id');
                        an_id = an_id.replace('an_id_','');
                        deleteAppointmentNotification(an_id,section);
                    });

                    function deleteAppointmentNotification(an_id,section) {
                        $.post('<?php echo $page_controller ?>/deleteAppointmentNotification',
                            {an_id},function(data){
                                section.remove();
                            });
                    }
                } else {
                    $('#otherDetailsContainer').addClass('hide');
                } 
            });
    }







    
// // CALENDAR END

</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 
            