 
<div style="padding-top: 0;min-height: 100px" class="tab-pane b-t " id="tab_appointments" role="tabpanel">
	<div class="card-body"> 
		Appointments <a class="btnRefreshAppointmentsTab" href="#"><i class="fas fa-refresh"></i></a>
		<div id="containerClientAppointments" class="">
			<table class="table stylish-table">
				<thead>
					<tr>
						<th>Appointment</th>
						<th>Date</th> 
						<th>Phone</th>
						<th>Email</th>
						<th>Location</th>
						<th>Note</th>
						<th>Notification</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>



<div id="modalAppointmentInformation" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Appointment Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div> 
            <form method="post" id="formSaveAppointment">
            <div class="modal-body"> 
                <div class="row">
                    
                    <div class="col-12">  
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="app_id" id="calendar_app_id"> 
                                  
                                  
                                <h4 class="m-t-20" for="calendar_appointment">Appointment</h4>
                                <div class="input-group"> 
                                    <input type="text" required class="form-control bg-success text-white" name="appointment" id="calendar_appointment">
                                    <div class="input-group-append">
                                        <select class="form-control form-white" data-placeholder="" name="app_color" id="calendar_app_color" style="height: calc(2.25rem + 2px) !important">
                                            <option value="bg-success">Evaluation</option>
                                            <option value="bg-grey">Busy</option>
                                            <option value="bg-green">Meeting</option> 
                                        </select>
                                    </div>
         
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control date-inputmask" name="start" id="calendar_date_start" > 
                                    <select class="form-control" id="calendar_time_start" style="height: calc(2.25rem + 2px) !important"> 
                                        <option selected="" value="10:00">10:00am</option>
                                        <option value="10:30:00">10:30am</option>
                                        <option value="11:00:00">11:00am</option>
                                        <option value="11:30:00">11:30am</option>
                                        <option value="12:00:00">12:00pm</option>
                                        <option value="12:30:00">12:30pm</option> 
                                        <option value="13:00:00">1:00pm</option>
                                        <option value="13:30:00">1:30pm</option> 
                                        <option value="14:00:00">2:00pm</option>
                                        <option value="14:30:00">2:30pm</option> 
                                        <option value="15:00:00">3:00pm</option>
                                        <option value="15:30:00">3:30pm</option> 
                                        <option value="16:00:00">4:00pm</option>
                                        <option value="16:30:00">4:30pm</option> 
                                        <option value="17:00:00">5:00pm</option>   
                                    </select>
                                    <script>
                                        $('#calendar_time_start').on('change',function(){
                                            var index = $(this).find('option:selected').index(); 
                                            $('#calendar_time_end option:eq('+(index+1)+')').attr('selected', 'selected');
                                        });
                                    </script>
                                    <span class="input-group-text bg-success b-0 text-white">TO</span>
                                    
                                    <select class="form-control" id="calendar_time_end" style="height: calc(2.25rem + 2px) !important">
                                        <option value="10:00:00">10:00am</option>
                                        <option selected="" value="10:30:00">10:30am</option>
                                        <option value="11:00:00">11:00am</option>
                                        <option value="11:30:00">11:30am</option>
                                        <option value="12:00:00">12:00pm</option>
                                        <option value="12:30:00">12:30pm</option> 
                                        <option value="13:00:00">1:00pm</option>
                                        <option value="13:30:00">1:30pm</option> 
                                        <option value="14:00:00">2:00pm</option>
                                        <option value="14:30:00">2:30pm</option> 
                                        <option value="15:00:00">3:00pm</option>
                                        <option value="15:30:00">3:30pm</option> 
                                        <option value="16:00:00">4:00pm</option>
                                        <option value="16:30:00">4:30pm</option> 
                                        <option value="17:00:00">5:00pm</option>  
                                        <option value="17:30:00">5:30pm</option> 
                                    </select>
                                    <input type="text" class="form-control date-inputmask " name="end" id="calendar_date_end" > 
                                </div>
                                
                            </div> 
                        </div> 
                        <div class="row m-t-10  ">
                            <div class="col-12">
                                <h4>Event Details</h4>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text"><i class="fas fa-phone "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_phone" name="app_phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text"><i class="fas fa-envelope "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_email" name="app_email" class="form-control" placeholder="Email">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label style="width: 42px" class="input-group-text"><i class="fas fa-map-marker "></i></label>
                                    </div>
                                    <input type="text" id="calendar_app_location" name="app_location" class="form-control" placeholder="Location">
                                </div>
                                
                                
                                <a href="#" id="btnAppMoreOptions" class="pull-right m-t-10">More Options</a>
                                <script>
                                    $("#btnAppMoreOptions").on('click',function(){
                                        if ($('#otherDetailsContainer').hasClass('hide')) {
                                            $('#otherDetailsContainer').removeClass('hide');
                                        } else {
                                            $('#otherDetailsContainer').addClass('hide');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="row m-t-10 hide animated fadeIn" id="otherDetailsContainer">
                            <div class="col-12">
                                <a href="#" id="btnAppMoreNotif" class="m-t-10">Add Notifications</a>
                                <script>
                                    $("#btnAppMoreNotif").on('click',function(){
                                        var sectionAddNotification = $('#sectionAddNotification');
                                        var newNotif = '<section class="input-group animated fadeIn">\
                                                            <div class="input-group-prepend">\
                                                                <label class="input-group-text"><i class="fas fa-bell"></i></label>\
                                                            </div>\
                                                            <select id="calendar_notif_type" name="notif_type[]" class="form-control calendar_notif_type" style="height: calc(2.25rem + 2px) !important">\
                                                                <option value="Email">Email</option>\
                                                                <option selected="" value="Notification">Notification</option>\
                                                            </select>\
                                                            <input type="number" id="calendar_notif_time" name="notif_time[]" class="form-control calendar_notif_time" value="30" style="width: 40px">\
                                                            <select id="calendar_notif_schedule" name="notif_schedule[]" class="form-control calendar_notif_schedule" style="height: calc(2.25rem + 2px) !important">\
                                                                <option selected="" value="minutes">minutes</option>\
                                                                <option value="hours">hours</option>\
                                                            </select>\
                                                            <div class="input-group-append">\
                                                                <label class="input-group-text btnDeleteAppointmentNotification"  style="min-width: 30px !important;cursor:pointer"><i class="fas fa-times"></i></label>\
                                                            </div>\
                                                        </section>';
                                        sectionAddNotification.append(newNotif);
                                        $('.btnDeleteAppointmentNotification').on('click',function(){
                                            var button = $(this);
                                            var section = button.closest('section');
                                            section.remove();
                                        });
                                    });
                                </script>
                                <section id="sectionAddNotification">
                                    
                                </section>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Note</label>
                                    </div>
                                    <textarea id="calendar_app_note" class="form-control app_note" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: block;min-height: 70px">
                <!-- <button type="button" id="btnDeleteAppointment" class="btn hide btn-danger waves-effect pull-left" data-dismiss="modal">Delete</button> -->
                <button type="button" class="btn btn-danger waves-effect pull-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Save Appointment</button> 
            </div> 
            </form>
        </div>
    </div>
</div>


<script>
	$("#calendar_appointment" ).autocomplete({
      minLength: 0,
      source: [{
                value: 'Out of Office',
                label: 'Out of Office',
                desc: '', 
              },{
                value: 'In Office',
                label: 'In Office',
                desc: '', 
              },{
                value: 'Over the Phone',
                label: 'Over the Phone',
                desc: '', 
              },{
                value: 'Account Review',
                label: 'Account Review',
                desc: '', 
              }
              ],
      focus: function( event, ui ) {
        $( "#calendar_appointment" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#calendar_appointment" ).val( ui.item.label );  
         
        return false;
      }
    });

    $('#calendar_app_color').on('change',function(){
        $('#calendar_appointment').removeClass();
        $('#calendar_appointment').addClass('form-control');
        $('#calendar_appointment').addClass('text-white');

        var app_color = $(this).val();
        $('#calendar_appointment').addClass(app_color);
    });

    
</script>