
                        <!-- Column -->
                        <div class="col-lg-9 col-xlg-9 col-md-9 col-sm-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul id="containerMainTabs" class="nav nav-tabs profile-tab" role="tablist">
                                    <li id="profile_tab" class="nav-item" style="margin: auto">
                                        <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                            Profile
                                        </a>
                                    </li>
                                    <li class="nav-item hide" style="margin: auto">
                                        <a id="tab_gameplan-header" class="nav-link " data-toggle="tab" href="#tab_gameplan" role="tab" aria-selected="false">
                                            GamePlan
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_evaluation-header" class="nav-link " data-toggle="tab" href="#tab_evaluation" role="tab" aria-selected="false">
                                            Evaluation
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if ($userdata['login_type'] == 'Sales Team'): ?>
                                        hide
                                    <?php endif ?>" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                            Billing
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_documents-header" class="nav-link " data-toggle="tab" href="#tab_documents" role="tab" aria-selected="false">
                                            Documents
                                        </a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php $data['subscription_type'] = $subscription_type;?>
                                    <?php if ($userdata['login_type'] == 'Sales Team') {  
                                        $this->load->view('admin/clients/tab_profile_sales_team');
                                    } else {
                                        $this->load->view('admin/clients/tab_profile');
                                    } ?>
                                    <?php $this->load->view('admin/clients/tab_gameplan')?>
                                    <?php $this->load->view('admin/clients/tab_evaluation')?>
                                    <?php $this->load->view('admin/clients/tab_documents')?>
                                    <?php $this->load->view('admin/clients/tab_billing_info', $data)?>
                                </div>
                            </div>
                            <div class="card" id="containerEmailTextTabs">
                                <div class="card-body" style="padding: 0px">
                                    <div class="row">
                                        <div class="col-12"> 
                                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                                <li class="nav-item" style="width: 100%">
                                                    <a class="nav-link active show" data-toggle="tab" href="#tab_conversation_history" role="tab" aria-selected="false">
                                                        <!-- Conversation History -->
                                                    </a>
                                                </li>
                                                <li class="nav-item hide">
                                                    <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link " data-toggle="tab" href="#tab_emails" role="tab" aria-selected="true">
                                                        Emails
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_texts" role="tab" aria-selected="false">
                                                        Text
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_call_logs" role="tab" aria-selected="false">
                                                        Call Logs
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_campaign" role="tab" aria-selected="false">
                                                        Campaign
                                                    </a>
                                                </li>

                                            </ul>
                                            <div class="tab-content">
                                            <?php $this->load->view('admin/clients/tab_conversation_history')?>
                                            <?php $this->load->view('admin/clients/tab_notes')?>
                                            <?php $this->load->view('admin/clients/tab_emails')?>
                                            <?php $this->load->view('admin/clients/tab_texts')?>
                                            <?php $this->load->view('admin/clients/tab_call_logs')?>
                                            <?php $this->load->view('admin/clients/tab_campaign')?>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('admin/clients/RightSideDiv')?>
                        <!-- Column -->

                        
<?php $this->load->view('admin/clients/script')?>