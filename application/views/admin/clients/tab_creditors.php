
<div style="padding-top: 0" class="tab-pane b-t" id="tab_creditors" role="tabpanel">
    <div class="card-body">
        <div class="row">

            <div class="text-center col-12 col-md-4">
                <h3><a href="#" id="btnOpenCreditorLettersModal">Creditor Letters</a></h3>
            </div>
             <div class="text-center col-12 col-md-4">
                <h3><a href="#" id="btnAddCreditorFront">New Creditor</a></h3>
                <!-- <h3><a href="#" id="btnOpenCreditorLettersModal">Creditor Letters</a></h3> -->
            </div>
            <div class="text-center col-12 col-md-4">
                <h3><a id="btnCreditorsNewAccount" href="#">New Account </a></h3>
            </div>
        </div>
        <div class="hide animated fadeIn row" id="containerAddCreditorFront">
            <div class="col-12">
                 <form method="POST" id="formLetterCreditorsFront">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label style="min-width: 0px !important;" class="input-group-text">Name</label>
                        </div>
                        <input required type="text" name="" id="new_creditor_name" class="form-control">
                        <div class="input-group-prepend">
                            <label style="min-width: 0px !important;" class="input-group-text">Address</label>
                        </div>
                        <input required type="text" name="" id="new_creditor_address" class="form-control">
                        <div class="input-group-prepend">
                            <label style="min-width: 0px !important;" class="input-group-text">City, State Zip</label>
                        </div>
                        <input required type="text" name="" id="new_creditor_city_state_zip" class="form-control">
                        <div class="input-group-append">
                            <button  type="submit" class="btn btn-success waves-effect waves-light">Save Creditor</button>
                        </div>
                    </div>
                </form>
                <table class="table stylish-table" id="tableLetterCreditors">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City, State Zip</th>
                            <th>Tools</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>


            <hr>
        </div>
        <div id="tabAddCreditorsPlan" class="hide">
            <form method="post" id="formCreditorsNewAccount">
                <div class="text-center">
                    <!-- <h4 class="m-b-0">Account Type</h4>  -->
                    <span id="span_checkbox_all_g">
                        <input type="checkbox" id="checkbox_all_g" class="chk-col-blue-grey">
                        <label  for="checkbox_all_g">All</label>
                    </span>
                    <br>
                    <span id="span_checkbox_equifax_g">
                        <input type="checkbox" id="checkbox_equifax_g" class="chk-col-red">
                        <label  for="checkbox_equifax_g">Equifax</label>
                    </span>
                    <span id="span_checkbox_experian_g">
                        <input type="checkbox" id="checkbox_experian_g" class="chk-col-indigo">
                        <label for="checkbox_experian_g">Experian</label>
                    </span>
                    <span id="span_checkbox_transunion_g">
                        <input type="checkbox" id="checkbox_transunion_g" class="chk-col-green">
                        <label for="checkbox_transunion_g">TransUnion</label>
                    </span>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Account Name:</label>
                            <input type="text" name="creditors_account_name" required="" class="form-control" id="creditors_account_name">
                            <span class="bar"></span>
                        </div>
                    </div>
                    <div class=" col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Account Number:</label>
                            <input type="text" name="creditors_account_number" required="" class="form-control" id="creditors_account_number">
                            <span class="bar"></span>
                        </div>
                    </div>
                    <div class=" col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">
                                Account Type:
                                <a tabindex="999999" href="#" data-toggle="modal" data-target="#add-edit-creditors-account"><i class="fas fa-plus-circle"></i></a>
                            </label>
                            <!-- <input type="text" name="Education" required="" class="form-control" id="Education"> -->
                            <select class="custom-select col-12" id="select_creditors_account_type">
                                <!-- <option selected="">Choose...</option>
                                <option value="Education">Education</option>
                                <option value="Revolving">Revolving</option>
                                <option value="Installment">Installment</option> -->
                            </select>
                            <span class="bar"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="name" class="control-label">
                                Recommendations:
                                <a tabindex="999999" href="#" data-toggle="modal" data-target="#add-edit-creditors-rec"><i class="fas fa-plus-circle"></i></a>
                            </label>
                            <!-- <input type="text" name="Recommendations" required="" class="form-control" id="Recommendations"> -->
                            <select class="custom-select col-12" id="select_creditors_rec" >
                            </select>
                            <span class="bar"></span>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Balance:</label>
                            <input type="text" name="creditors_missing_balance" required="" class="form-control" id="creditors_missing_balance">
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Credit Limit:</label>
                            <input type="text" name="creditors_credit_limit" required="" class="form-control" id="creditors_credit_limit">
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Past Due:</label>
                            <input type="text" name="creditors_past_due" required="" class="form-control" id="creditors_past_due">
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style=" text-align: right; margin-bottom: 25px;">
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save Account</button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 0px">
                <div class="col-12 m-b-20">
                    <div class="input-group rightLabelDiv">
                        <small class="rightLabel">DFR <u id="profile_dfr_creditors" ></u> days</small>
                        <textarea cols="3" id="profile_status_line_creditors" name="profile_status_line_creditors" class="form-control" ></textarea>
                        <div class="input-group-append" style="width: 110px">
                            <input type="text" id="creditors_date_for_result" name="profile_date_for_result_creditors" class="form-control text-center date-inputmask" placeholder="Date for Results">
                        </div>
                    </div>
                    <script>
                        $("#profile_status_line_creditors" ).autocomplete({
                          minLength: 0,
                          source: [{
                                    value: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    label: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    desc: '',
                                  },{
                                    value: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    label: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    desc: '',
                                  },{
                                    value: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    label: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports. ',
                                    desc: '',
                                  },{
                                    value: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Pending Verifications and Dispute Input.',
                                    label: 'Pending Verifications and Dispute Input.',
                                    desc: '',
                                  },{
                                    value: 'Pending (1) Address Verification and Dispute Input.',
                                    label: 'Pending (1) Address Verification and Dispute Input.',
                                    desc: '',
                                  },{
                                    value: 'Pending (2) Address Verification and Dispute Input.',
                                    label: 'Pending (2) Address Verification and Dispute Input.',
                                    desc: '',
                                  },{
                                    value: 'Pending Social Security Verification and Dispute Input.',
                                    label: 'Pending Social Security Verification and Dispute Input.',
                                    desc: '',
                                  },{
                                    value: 'Pending Dispute Input.',
                                    label: 'Pending Dispute Input.',
                                    desc: '',
                                  },{
                                    value: 'Pending Result Review and Account Update.',
                                    label: 'Pending Result Review and Account Update.',
                                    desc: '',
                                  },{
                                    value: 'Credit Complete.',
                                    label: 'Credit Complete.',
                                    desc: '',
                                  },{
                                    value: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
                                    label: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
                                    desc: '',
                                  }
                                  ],
                          focus: function( event, ui ) {
                            $( "#profile_status_line" ).val( ui.item.label );
                            return false;
                          },
                          select: function( event, ui ) {
                            $( "#profile_status_line" ).val( ui.item.label );

                            return false;
                          }
                        })
                    </script>

                </div>
                <div class="vtabs customvtab customvtabCreditors" style="width: 100%">
                    <div class="toSlimScroll">
                    <ul class="nav nav-tabs tabs-vertical tabs-vertical-creditors " role="tablist" style="width: 20%">
                    </ul>
                    </div>

                    <!-- Tab panes -->
                    <div  class="tab-content" style="padding: 5px;width: 100% !important">
                        <div class="row" id="custom_tab_content_creditors" style="margin-left: 0px;margin-right: 0px">
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-equifax" ><span span-color="text-equifax" bureau="Equifax" class=" spanPointer">Equifax</span></h3>
                                <div  id="section_content_equifax" >
                                </div>
                            </section>
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-experian" ><span span-color="text-experian" bureau="Experian" class=" spanPointer">Experian</span></h3>
                                <div id="section_content_experian" >
                                </div>
                            </section>
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-transunion" ><span span-color="text-transunion" bureau="TransUnion" class=" spanPointer">TransUnion</span></h3>
                                <div id="section_content_transunion" >
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $('#btnCreditorsNewAccount').on('click', function(e){
        e.preventDefault();
        // $('#tabAddCreditorsPlan').removeClass('hide');
        var tabAddCreditorsPlan = $(this).closest('.card-body').find('#tabAddCreditorsPlan');
        if (tabAddCreditorsPlan.hasClass('hide')) {
            tabAddCreditorsPlan.removeClass('hide');
        } else {
            tabAddCreditorsPlan.addClass('hide');
        }
    });

    $('#checkbox_all_g').on('change',function(){
        var checkbox_all_g = $(this);
        if (this.checked) {
            checkbox_all_g.prop('checked',true);
            var checkbox_equifax_g = $('#checkbox_equifax_g').prop('checked',true);
            var checkbox_experian_g = $('#checkbox_experian_g').prop('checked',true);
            var checkbox_transunion_g = $('#checkbox_transunion_g').prop('checked',true);
        } else {
            checkbox_all_g.prop('checked',false);
            var checkbox_equifax_g = $('#checkbox_equifax_g').prop('checked',false);
            var checkbox_experian_g = $('#checkbox_experian_g').prop('checked',false);
            var checkbox_transunion_g = $('#checkbox_transunion_g').prop('checked',false);
        }
    });
</script>
