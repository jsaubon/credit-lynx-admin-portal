<style>
    .badge-summary1 {
        background-color: grey;
        color: white;
    }
    .badge-summary2 {
        background-color: #00adad;
        color: white;
    }
</style>
<div class="col-lg-3 col-xlg-3 col-md-3 col-sm-12">
    <div class="card">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <li class="nav-item" style="margin: auto;">
                <a id="summary-tab-header" class="nav-link active show" data-toggle="tab" href="#summary-tab" role="tab" aria-selected="true">Summary</a>
            </li>

            <li class="nav-item" style="margin: auto;">
                <a id="alerts-tab-header" class="nav-link" data-toggle="tab" href="#alerts-tab" role="tab" aria-selected="false">Alerts</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active show" id="summary-tab" role="tabpanel">
                <div class="card-body">
                    <h4 class="text-center">
                        <a href="#" class="btn-prev-client"><i class="fas fa-angle-double-left fa-lg"></i></a>
                        <span class="name">-</span>
                        <a href="#" class="btn-next-client"><i class="fas fa-angle-double-right fa-lg"></i></a>
                        
                        
                    </h4>
                    <div class="text-center">
                        <span class="profile_client_status badge badge-success summary-client-status">-</span>
                        <span class="client_grade badge badge-summary1">-</span>
                        <span class="billing_plan badge badge-summary2">-</span>
                    </div>
                    <address class="text-center" style="margin-top: 1rem">
                        <span class="profile_address"></span><br>
                        <span class="profile_city"></span>, <span class="profile_state_province"></span>, <span class="profile_zip_postal_code"></span>

                    </address>
                    <div class="text-center">
                        <span>Dispute DFR <span class="profile_dfr"></span></span>
                    </div>
                    <div class="text-center">
                        <span class="profile_date_of_birth badge badge-summary1"></span>
                        <span class="profile_ss badge badge-summary2"></span>

                    </div>
                    <h4 style="margin-top: 1rem" class="text-center">
                        <span class="broker_name"></span>
                        <br>
                        <span class="broker_cell"></span>
                    </h4>
                    <div class="text-center" >
                        <span style="background-color: #dd5970!important" class="span_equifax_points badge badge-success">-</span>
                        <span style="background-color: #6e70e6!important" class="span_experian_points badge badge-success">-</span>
                        <span style="background-color: #6ec4d7!important" class="span_transunion_points badge badge-success">-</span>
                        
                            <br>
                            <span class="show_lead_content">Date Entered: <span class="date_created"></span></span>
                        
                        <br>
                        <small class="joint_container hide">Access
                            <a class="changeToOtherAccount" href="#"></a> account?
                        </small>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="alerts-tab" role="tabpanel">
                <div class="card-body">
                    <h4 class="text-center">
                        <span>Alert: <a href="#" id="btnAlertResponder" class="text-danger">OFF</a></span>
                        <br>
                        <a href="#" id="btn_auto_alert_schedule">Auto Alert Schedule</a>
                    </h4>
                    <h5><a href="#" id="btnShowHideSendAlert" >Send alert <i class="fas fa-angle-right"></i></a>
                        </span>
                    </h5>
                    <div id="containerSendAlert" class="hide">
                        <label class="m-b-0">Templates <small>(<a href="#" id="btnOpenAlertTemplatesModal" data-toggle="modal" data-target="#add-edit-alert-templates-modal">manage</a>)(<a id="btnOpenAlertTemplates" href="#">hide</a>)</small>
                        </label>
                        <div class="row " id="alert_templates_container" style="border: 1px solid #6ec4d7!important; padding-top: 6px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 11px; margin-bottom: 7px!important;line-height: 70%; padding-left: 5px;">
                            <?php foreach ($alert_templates as $key => $alert_template): ?>
                                <label class="col-md-6 alert_notes" style="color: #6ec4d7; cursor: pointer; padding: 0px;" template_subject="<?php echo $alert_template['template_subject'] ?>" alert_temp="<?php echo $alert_template['template_notes'] ?>"><?php echo $alert_template['template_shortname'] ?></label>
                            <?php endforeach?>
                        </div>

                        <div class=" ">
                            <label class="control-label">Date</label>
                            <input style="font-size: 70%;padding-top: 0 !important;padding-bottom: 0 !important" id="new_alert_date" type="text" name="" class="form-control new_alert_date date-inputmask" value="<?php echo date('m/d/Y') ?>">
                            <label class="control-label">Subject</label>
                            <input style="font-size: 70%;padding-top: 0 !important;padding-bottom: 0 !important" id="new_alert_subject" type="text" name="" class="form-control new_alert_subject">
                            <label class="control-label">Note</label>
                            <textarea style="font-size: 70%" id="new_alert_notes" class="form-control new_alert_notes" rows="10"></textarea>
                            <div>
                                <button id="btnSaveNewAlert" class="pull-right btn btn-success waves-effect waves-light" type="button">
                                    <i class="fas fa-paper-plane"></i>
                                    Send alert
                                </button>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>

                    <hr>
                    <div class="message-scroll ">
                        <div id="alert_list" class="profiletimeline   ">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
