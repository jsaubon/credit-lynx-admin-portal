<style>
    .tableByAccount tbody td:nth-child(2),.tableByAccount thead th:nth-child(2),.tableByAccount tbody td:nth-child(3),.tableByAccount thead th:nth-child(3) ,.tableByAccount tbody td:nth-child(4),.tableByAccount thead th:nth-child(4),.tableByAccount tbody td:nth-child(5),.tableByAccount thead th:nth-child(5),.tableByAccount tbody td:nth-child(6),.tableByAccount thead th:nth-child(6){
        white-space: nowrap;
        width: 1px;
    }

    .paginate_button {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        border: none !important;
    }
</style>
<div style="padding-top: 0" class="tab-pane b-t" id="tab_results" role="tabpanel">
    <div class="card-body">
        <h4 class="card-title">Results</h4>
        <div class="row m-t-10">
            <div class="col-12 col-md-4 p-0">
                <h3 class="text-center chartHeaderEquifax hide" style="color: #fc4b6c">Equifax</h3>
                <h5 class="text-center chartLabelEquifax" style="color: #fc4b6c"></h5>
                <h3 class="text-center chartAmountEquifax"></h3>
                <h6 style="font-size: 70%" class="text-center m-b-5 hide">
                    <span style="background-color: grey;color: white;padding: 3px 5px;">Not Fixed/Deleted</span>&nbsp
                    <span style="background-color: #fc4b6c;color: white;padding: 3px 5px;">Fixed/Deleted</span>
                </h6>
                <div class="chartEquifaxPercent hide" style="pointer-events: none;margin-top: 20px;margin-bottom: 20px">
                    <span class="spanEquifaxPercent" style="position: absolute;top: 50%;left: 41%;margin-top: 20px;font-size: 120%"></span>
                    <div id="chartEquifaxPercent" style="width:100%; height:25vh;"></div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-0">
                <h3 class="text-center chartHeaderExperian hide" style="color: #7460ee">Experian</h3>
                <h5 class="text-center chartLabelExperian" style="color: #7460ee"></h5>
                <h3 class="text-center chartAmountExperian"></h3>
                <h6 style="font-size: 70%" class="text-center m-b-5 hide">
                    <span style="background-color: grey;color: white;padding: 3px 5px;">Not Fixed/Deleted</span>&nbsp
                    <span style="background-color: #7460ee;color: white;padding: 3px 5px;">Fixed/Deleted</span>
                </h6>
                <div class="chartExperianPercent hide" style="pointer-events: none;margin-top: 20px;margin-bottom: 20px;">
                    <span class="spanExperianPercent" style="position: absolute;top: 50%;left: 41%;margin-top: 20px;font-size: 120%"></span>
                    <div id="chartExperianPercent" style="width:100%; height:25vh;"></div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-l-0">
                <h3 class="text-center chartHeaderTransUnion hide" style="color: #26c6da">TransUnion</h3>
                <h5 class="text-center chartLabelTransUnion" style="color: #26c6da"></h5>
                <h3 class="text-center chartAmountTransUnion"></h3>
                <h6 style="font-size: 70%" class="text-center m-b-5 hide">
                    <span style="background-color: grey;color: white;padding: 3px 5px;">Not Fixed/Deleted</span>&nbsp
                    <span style="background-color: #26c6da;color: white;padding: 3px 5px;">Fixed/Deleted</span>
                </h6>
                <div class="chartTransUnionPercent hide" style="pointer-events: none;margin-top: 20px;margin-bottom: 20px">
                    <span class="spanTransUnionPercent" style="position: absolute;top: 50%;left: 41%;margin-top: 20px;font-size: 120%"></span>
                    <div id="chartTransUnionPercent" style="width:100%; height:25vh;"></div>
                </div>
            </div>
        </div>

        <ul class="nav nav-tabs profile-tab" role="tablist"> 
            <li id="by_account_tab" class="nav-item"> 
                <a class="nav-link active show" data-toggle="tab" href="#tab_by_account" role="tab" aria-selected="true">
                    Results by Account
                </a> 
            </li>
            <li id="by_bureau_tab" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau" role="tab" aria-selected="true">
                    Results by Bureau
                </a> 
            </li>
            <li id="by_bureau_round_tab_1" bureau_round="1" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_1" role="tab" aria-selected="true">
                    Rnd 1
                </a> 
            </li>
            <li id="by_bureau_round_tab_2" bureau_round="2" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_2" role="tab" aria-selected="true">
                    Rnd 2
                </a> 
            </li>
            <li id="by_bureau_round_tab_3" bureau_round="3" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_3" role="tab" aria-selected="true">
                    Rnd 3
                </a> 
            </li>
            <li id="by_bureau_round_tab_4" bureau_round="4" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_4" role="tab" aria-selected="true">
                    Rnd 4
                </a> 
            </li>
            <li id="by_bureau_round_tab_5" bureau_round="5" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_5" role="tab" aria-selected="true">
                    Rnd 5
                </a> 
            </li>
            <li id="by_bureau_round_tab_6" bureau_round="6" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_6" role="tab" aria-selected="true">
                    Rnd 6
                </a> 
            </li>
            <li id="by_bureau_round_tab_7" bureau_round="7" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_7" role="tab" aria-selected="true">
                    Rnd 7
                </a> 
            </li>
            <li id="by_bureau_round_tab_8" bureau_round="8" class="nav-item"> 
                <a class="nav-link" data-toggle="tab" href="#tab_by_bureau_round_8" role="tab" aria-selected="true">
                    Rnd 8
                </a> 
            </li> 
        </ul>

        <div class="tab-content">
            <div class="tab-pane active show p-t-20" id="tab_by_account" role="tabpanel">
                <h3 class="text-center m-b-0 hide"><span class="improvement_all"></span> Improvement</h3>
                <div class="progress">
                    <div class="progress-bar bg-grey active wow animated progress-animated" id="improvement_all_progressbar" role="progressbar" style="height:15px;" role="progressbar"> 
                        <span class="improvement_all"></span> 
                    </div>
                </div>
                <table class="tableByAccount table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr> 
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th width="93px" class="text-center bg-danger">Equifax</th>
                            <th width="93px" class="text-center bg-purple">Experian</th>
                            <th width="93px" class="text-center bg-success">TransUnion</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau" role="tabpanel">
                <div id="accordion1" role="tablist" aria-multiselectable="true">
                    <div class="card m-b-0">
                        <div class="card-header" role="tab" id="headingOne1">
                            <h5 class="mb-0">
                            <a class="text-danger" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                              Equifax
                            </a>
                          </h5>
                        </div>
                        <div id="collapseOne1" class="collapse" role="tabpanel" aria-labelledby="headingOne1" style="">
                            <div class="card-body">
                                <h3 class="text-center m-b-0"><span class="improvement_equifax"></span> Improvement</h3>
                                <div class="progress">
                                    <div class="progress-bar bg-danger active wow animated progress-animated" id="improvement_equifax_progressbar" role="progressbar" style="height:15px;" role="progressbar"> 
                                        <span class="improvement_equifax"></span> 
                                    </div>
                                </div>
                                <table class="tableByBureauEquifax table color-bordered-table danger-bordered-table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Account Name</th>
                                            <th>Account Number</th>
                                            <th>Account Type</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card m-b-0">
                        <div class="card-header" role="tab" id="headingTwo2">
                            <h5 class="mb-0">
                            <a class="text-purple" class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                              Experian
                            </a>
                          </h5>
                        </div>
                        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" style="">
                            <div class="card-body">
                                <h3 class="text-center m-b-0"><span class="improvement_experian"></span> Improvement</h3>
                                <div class="progress">
                                    <div class="progress-bar bg-purple active wow animated progress-animated" id="improvement_experian_progressbar" role="progressbar" style="height:15px;" role="progressbar"> 
                                        <span class="improvement_experian"></span> 
                                    </div>
                                </div>
                                 <table class="tableByBureauExperian table color-bordered-table purple-bordered-table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Account Name</th>
                                            <th>Account Number</th>
                                            <th>Account Type</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree3">
                            <h5 class="mb-0">
                            <a id="collapseTrasUnion" class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                              TransUnion
                            </a>
                          </h5>
                        </div>
                        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3">
                            <div class="card-body">
                                <h3 class="text-center m-b-0"><span class="improvement_transunion"></span> Improvement</h3>
                                <div class="progress">
                                    <div class="progress-bar bg-success active wow animated progress-animated" id="improvement_transunion_progressbar" role="progressbar" style="height:15px;" role="progressbar"> 
                                        <span class="improvement_transunion"></span> 
                                    </div>
                                </div>
                                 <table class="tableByBureauTransUnion table color-bordered-table success-bordered-table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Account Name</th>
                                            <th>Account Number</th>
                                            <th>Account Type</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_1" role="tabpanel">  
                <table class="tableByAccountRound1 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_2" role="tabpanel">  
                <table class="tableByAccountRound2 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_3" role="tabpanel">  
                <table class="tableByAccountRound3 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_4" role="tabpanel">  
                <table class="tableByAccountRound4 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_5" role="tabpanel">  
                <table class="tableByAccountRound5 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_6" role="tabpanel">  
                <table class="tableByAccountRound6 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_7" role="tabpanel">  
                <table class="tableByAccountRound7 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
            <div class="tab-pane" id="tab_by_bureau_round_8" role="tabpanel">  
                <table class="tableByAccountRound8 table color-bordered-table muted-bordered-table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Bureau</th>
                            <th>Account Name</th>
                            <th>Account Number</th>
                            <th>Account Type</th>
                            <th>Account Status</th>
                        </tr>
                    </thead>
                    <tbody>
                         
                    </tbody>
                </table> 
            </div>
        </div>



       



    </div>
</div>