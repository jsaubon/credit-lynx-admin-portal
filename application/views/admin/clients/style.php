
<style type="text/css">
    .btn-success:not(:disabled):not(.disabled).active:focus, .btn-success:not(:disabled):not(.disabled):active:focus, .show>.btn-success.dropdown-toggle:focus {
        box-shadow: 0 0 0 0.2rem #34d2e685 !important;
    }
    .right-sidebar-loader {
        background-color:rgba(255,255,255,.7);
        z-index: 1200;
        width: 100%;
        height: 100%;
        position: absolute;
        top: 70px;
    }
    .paper_letter {
        margin-left: 30px;
        color: red;
    }
    .wizard-content .wizard>.steps>ul>li.current .step {
        border-color: grey !important;
        color: grey !important;
    }
    .wizard-content .wizard.wizard-circle>.steps>ul>li:after, .wizard-content .wizard.wizard-circle>.steps>ul>li:before {
        background-color: grey !important;
    }

    .spanPointer {
        cursor: pointer;
    }
    .text-equifax {
        color: #fc4b6c;
    }
    .text-experian {
        color: #7460ee;
    }
    .text-transunion {
        color: #26c6da;
    }
    .page-titles .breadcrumb .breadcrumb-item+.breadcrumb-item:before {
        display: none;
    }
    .link {
        cursor: pointer;
    }

    #sectionProfileInformation .form-group , #sectionClientAssignments .form-group , #sectionBillingInformation .form-group{
        margin-bottom: 5px !important;
    }

    #tableAlertTemplates tr td {
        line-height: 100% !important;
    }

    .profiletimeline {
        margin-left: 0px !important;
    }





    .select2-container--default .select2-results__option--highlighted[aria-selected], .ms-container .ms-selectable li.ms-hover, .ms-container .ms-selection li.ms-hover {
        background-color: #21b3c6  !important;
    }

    #containerNewAccount label {
        font-size: 80%;
    }

    #containerNewAccount .form-control,#containerNewAccount .select2 {
        min-height: 28px;
        font-size: 60%;
        border-radius: 0 !important;
        line-height: 100% !important;

    }
    #containerNewAccount .form-group {
        margin-bottom: 0px !important;
    }

  /*  .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 28px !important;
    }*/

   /* .select2-container--default .select2-selection--single {
        height: 28px !important;
    }*/

    .paginate_button > a:hover {
        color: black !important;
    }

    .ms-container {
        width: 100% !important;
    }

    #tableDisputeVerbiages tr {
        cursor: pointer;
    }

    .customvtab .tabs-vertical li .nav-link.active, .customvtab .tabs-vertical li .nav-link:focus, .customvtab .tabs-vertical li .nav-link:hover {
        color: #21b3c6;
        border-right: 2px solid #21b3c6;
    }

    .btn-link {
        color: #21b3c6;
    }


    .rt_rounds {
        color: #21b3c6;
        cursor: pointer;
    }

    .rounds textarea {
        font-size: 60%;
    }

    .rounds select {
        width: 100%;
    }

    .rounds span {
        display: block;
        font-size: 60%;
    }


    .select2-container--default .select2-results>.select2-results__options{
        max-height: 600px;
    }

    .select2-results li:nth-child(2n+1){
        background-color:#efefef;
        color: #67757c;
    }

    #btnNewUser:hover {
        color: #fff !important;
    }

    .input-group .input-group-text, {
        border-radius: 0px;
    }

    .input-group .form-control {
        min-height: 28px !important;
    }

    .input-group label, .input-group input, .input-group select {
        font-size: 70%;
        min-width: 103px !important;
    }

    .sectionEnrollmentInformation .input-group label {
        min-width: 141px !important;
    }

    #sectionBillingInformation .input-group label {
        min-width: 93px;
    }
    #sectionBillingInformation .input-group input, #sectionBillingInformation .input-group select {
        min-width: 0px !important;
    }

    .input-group select.form-control:not([size]):not([multiple]) {
        height: 30px !important;
    }

    .input-group .select2-container--default .select2-selection--single {
        height: 30px ;
        font-size: 70%;
    }
/*
    .input-group .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 30px ;
    }*/

    .input-group .select2 {
        width: calc(100% - 103px) !important;;
    }

    #sectionBillingInformation .input-group .select2 {
        width: calc(100% - 122px) !important;;
    }

    #billing_account_info .input-group .select2 {
        width: calc(100% - 113.22px) !important;;


    }

    .tableByAccount > tbody > tr > td {
        font-size: 60% !important;
    }

    .text-purple:hover {
        color: #7460ee !important;
    }

    .bg-purple {
        background-color: #7460ee !important;
    }

    .bg-grey {
        background-color: #99abb4 !important;
    }

    .border-green {
        border: 2px solid #088080 !important;
        color: #088080 !important;
    }

    .tableByAccount > tbody > tr > td:nth-child(4),.tableByAccount > tbody > tr > td:nth-child(5),.tableByAccount > tbody > tr > td:nth-child(6) {
        text-align: center;
    }

    .note-btn-group button {
        background-color: white ;
    }

    .note-insert {
        display: none;
    }

   /* .modal-backdrop {
        display: none;
    }*/



    .note-editable {
        min-height: 300px;
    }


    .note-misc {
        float: right !important;
     }

    .select2[data-select2-id="42"] {
        width: 1% !important;
        flex: 1 1 auto ;
    }
    .select2[data-select2-id="42"] .select2-selection__rendered, .select2[data-select2-id="42"] .select2-selection,.select2[data-select2-id="42"] .select2-selection__arrow {
        line-height: calc(2.25rem + 2px) ;
        height: calc(2.25rem + 2px) ;
    }

    .swal2-popup .swal2-styled.swal2-confirm {
        background-color: #21b3c6;
        border-left-color: #21b3c6;
        border-right-color: #21b3c6;
    }

    .dataTables_info {
        display: none;
    }

    /*#pageTable thead tr th {
        border-right: 1px solid #dee2e6 !important;
        border-left: 1px solid #dee2e6 !important;
        border-top: 1px solid #dee2e6 !important;
    }*/

    #pageTable thead tr th:nth-child(2) {
        min-width: 110px !important;
    }


</style>