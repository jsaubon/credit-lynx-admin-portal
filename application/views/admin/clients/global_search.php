<script>
    $(document).ready(function() {
        getSearchClient();
        function getSearchClient() {
            var clients = [];
            var users = '<?php echo addslashes(json_encode($all_clients)) ?>';
            users = JSON.parse(users);
            $.each(users,function(key,value){
                var client_name = value.name;
                var client_type = value.client_type;

                var newOption = {
                                value: value.client_id+'_client',
                                label: client_name +'<br>'+value.cell_phone+ ' ' + value.email_address + ' ' +value.ss,
                                desc: value.client_name,
                              };
                clients.push(newOption);

                if (client_type == 'Joint') {
                    var newOption = {
                                value: value.client_joint_id+'_joint_'+value.client_id,
                                label: value.joint_name + '<br>'+value.joint_cell_phone+ ' ' + value.joint_email_address + ' ' +value.joint_ss,
                                desc: value.joint_name,
                              };
                    clients.push(newOption);
                }
            });



            $( "#searchInputClient" ).autocomplete({
              minLength: 2,
              source: clients,
              focus: function( event, ui ) {
                $( "#searchInputClient" ).val( ui.item.desc );
                return false;
              },
              select: function( event, ui ) {
                $( "#searchInputClient" ).val( ui.item.desc );
                $('#searchClient').val(ui.item.value);
                $('#searchClient').trigger('change');
                if ($('#profileSection').hasClass('hide')) {
                    $('#profileSection').removeClass('hide');
                    $('#listSection').addClass('hide');
                    $('#profile_active').removeClass('hide');
                    $('#profile_active').html(ui.item.label);
                }




                // $( "#project-id" ).val( ui.item.value );
                // $( "#project-description" ).html( ui.item.desc );
                // $( "#project-icon" ).attr( "src", "images/" + ui.item.icon );

                return false;
              }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                .append( "<div>" + item.label + "</div>" )
                .appendTo( ul );
            };
        }
    });
</script>