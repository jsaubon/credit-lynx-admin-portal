     
                        <!-- Column -->
                        <div class="col-lg-9 col-xlg-9 col-md-9 col-sm-12">
                            <div class="card">
                                <!-- Nav tabs -->
                                <ul id="containerMainTabs" class="nav nav-tabs profile-tab" role="tablist">
                                    <li id="profile_tab" class="nav-item" style="margin: auto">
                                        <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                            Profile
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_documents-header" class="nav-link " data-toggle="tab" href="#tab_documents" role="tab" aria-selected="false">
                                            Documents
                                        </a>
                                    </li>
                                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                            Billing
                                        </a>
                                    </li>
                                    <?php endif?>

                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_gameplan" role="tab" aria-selected="false">
                                            GamePlan
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a id="tab_support-header" class="nav-link btnBackToSupportTicketList" data-toggle="tab" href="#tab_support" role="tab" aria-selected="false">
                                            Supports <span class="text-danger" id="notif_support_right"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_results" role="tab" aria-selected="false">
                                            Results
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_disputes" role="tab" aria-selected="false">
                                            Disputes
                                        </a>
                                    </li>
                                    <li class="nav-item" style="margin: auto">
                                        <a class="nav-link" data-toggle="tab" href="#tab_creditors" role="tab" aria-selected="false">
                                            Creditors
                                        </a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php $data['subscription_type'] = $subscription_type;?>
                                    <?php $this->load->view('admin/clients/tab_profile')?>
                                    <?php $this->load->view('admin/clients/tab_documents')?>
                                    <?php $this->load->view('admin/clients/tab_billing_info', $data)?>
                                    <?php $this->load->view('admin/clients/tab_gameplan')?>
                                    <?php $this->load->view('admin/clients/tab_support')?>
                                    <?php $this->load->view('admin/clients/tab_results')?>
                                    <?php $this->load->view('admin/clients/tab_disputes')?>
                                    <?php $this->load->view('admin/clients/tab_creditors')?>
                                </div>
                            </div>
                            <div class="card" id="containerEmailTextTabs">
                                <div class="card-body" style="padding: 0px">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                                <li class="nav-item" style="width: 100%">
                                                    <a class="nav-link active show" data-toggle="tab" href="#tab_conversation_history" role="tab" aria-selected="false">
                                                        <!-- Conversation History -->
                                                    </a>
                                                </li>
                                                <li class="nav-item hide">
                                                    <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a>
                                                </li>
                                                <li class="nav-item hide">
                                                    <a id="tasks-tab-header" class="nav-link" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="false">Tasks</a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a id="tab_emails-header" class="nav-link " data-toggle="tab" href="#tab_emails" role="tab" aria-selected="true">
                                                        Emails
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_texts" role="tab" aria-selected="false">
                                                        Text
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_call_logs" role="tab" aria-selected="false">
                                                        Call Logs
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_appointments" role="tab" aria-selected="false">
                                                        Appointments
                                                    </a>
                                                </li>
                                                <li class="nav-item hide" >
                                                    <a class="nav-link" data-toggle="tab" href="#tab_campaign" role="tab" aria-selected="false">
                                                        Campaign
                                                    </a>
                                                </li>

                                            </ul>
                                            <div class="tab-content">
                                                
                                            <?php $this->load->view('admin/clients/tab_conversation_history')?>
                                            <?php $this->load->view('admin/clients/tab_notes')?>
                                            <?php $this->load->view('admin/clients/tab_tasks')?>
                                            <?php $this->load->view('admin/clients/tab_emails')?>
                                            <?php $this->load->view('admin/clients/tab_texts')?>
                                            <?php $this->load->view('admin/clients/tab_call_logs')?>
                                            <?php $this->load->view('admin/clients/tab_appointments')?>
                                            <?php $this->load->view('admin/clients/tab_campaign')?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('admin/clients/RightSideDiv')?>
                        <!-- Column -->

                        
<?php $this->load->view('admin/clients/script')?>