
<link href="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.css') ?>" rel="stylesheet" />
<style>
    .rightLabel {
        font-size: 8px !important;
    }

    #sectionBillingInformation .form-control {
        height: 30px;font-size: 70%;;min-height: 28px !important;
    }
</style>
<div style="padding-top: 0;" class="tab-pane b-t" id="tab_billing_info" role="tabpanel">
    <div class="card-body">
        <!-- <h3>Billing Information</h3> -->
        <div class="row" id="sectionBillingInformation">
            <div class="col-md-6 col-xs-6 b-r">
                <div class="row">
                    <div class="col-12">
                        <!-- <label><?php echo $subscription; ?></label> -->
                        <div class="rightLabelDiv">
                            <label for="billing_plan" class="rightLabel">Plan</label>
                            <select class="form-control" name="billing_plan" >
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="setup_fee" class="rightLabel">Setup Fee</label>
                            <input type="text" name="billing_setup_fee" class="form-control" style="">
                        </div>
                    </div>

                    <div class="col-md-6 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="monthly_fee" class="rightLabel">Monthly Fee</label>
                            <input type="text" name="billing_monthly_fee" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-9 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="card_number" class="rightLabel">Card Number</label>
                            <input type="text" name="billing_card_number" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3 p-l-0">
                        <a href="#" class="btn btn-success btn-block" style="height: 30px;font-size: 70%;" id="btnLookUpCardNumber">Lookup <i class="fas fa-search"></i></a>
                    </div>
                    <div class="col-md-4 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="card_expiration" class="rightLabel">Expiration</label>
                            <input type="text" name="billing_card_expiration" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 p-r-0 p-l-0" >
                        <div class="rightLabelDiv">
                            <label for="cvv_code" class="rightLabel">CVV Code</label>
                            <input type="text" name="billing_cvv_code" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 p-l-0 ">
                        <div class="rightLabelDiv">
                            <label for="billing_payment_method" class="rightLabel">Payment Method</label>
                            <select class="form-control" name="billing_payment_method">
                                <option value="American Express">American Express</option>
                                <option value="MasterCard">MasterCard</option>
                                <option value="Visa">Visa</option>
                                <option value="Cash">Cash</option>
                                <option value="Prepaid">Prepaid</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="account_holder" class="rightLabel">Account Holder</label>
                            <input type="text" name="billing_card_holder" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-8 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="address" class="rightLabel">Address</label>
                            <input type="text" name="billing_address" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="city" class="rightLabel">City</label>
                            <input type="text" name="billing_city" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3 p-r-0 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="state_province" class="rightLabel">State</label>
                            <select class="form-control" name="billing_state" >
                                <?php foreach ($userdata['state_provinces'] as $key => $value): ?>
                                    <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="zip" class="rightLabel">Zip</label>
                            <input type="text" name="billing_zip" class="form-control zip_postal_code">
                        </div>
                    </div>
                    <div class="col-md-12 m-t-20 ">
                        <label style="margin-bottom: 0px;" for="">Other Cards <a href="#" class="btnAddOtherCard"><i class="fas fa-plus"></i></a></label>
                        <section class="containerOtherCards">

                        </section>
                    </div>
                    <script>


                    </script>

                    <div class="col-md-12 m-t-20">
                        <div class="rightLabelDiv">
                            <label for="note" class="rightLabel">Billing Notes</label>
                            <textarea name="billing_note" class="form-control" rows="2" style="height: 100px;"></textarea>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-6 col-xs-6" id="billing_account_info">
                <div class="row">
                    <div class="col-md-12">
                        <div class="rightLabelDiv">
                            <label for="paid" class="rightLabel">Date Paid</label>
                            <input disabled type="text" name="billing_date_paid" class="form-control date-inputmask">
                        </div>
                    </div>
                    <div class="col-md-6 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="paid" class="rightLabel">Setup Date</label>
                            <input type="text" name="billing_paid" class="form-control date-inputmask">
                        </div>
                    </div>
                    <div class="col-md-6 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="monthly_due" class="rightLabel">Monthly Due</label>
                            <input type="text" name="billing_monthly_due" class="form-control date-inputmask">
                        </div>
                    </div>
                    
                    <div style="margin-top: 5px;" class="col-12 col-md-12">
                        <button id="btnChargeSetupFee" style="background-color: #008080;border-color: #008080;color:white" class="btn btn-block b-r-0">Charge Setup fee</button>
                    </div>
                    <!-- <div style="margin-top: 5px;" class="col-12 col-md-12">
                        <button id="btnCreateSubscription" class="btn btn-info btn-block b-r-0">Create Monthly Subscription</button>
                    </div> -->
                    <!-- <div style="margin-top: 5px;" class="col-md-12">
                        <button id="" class="btn btn-warning btn-block b-r-0">ARB Recurring Billing Function</button>
                    </div> -->
                    <?php if ($userdata['login_type'] == 'Administrator'): ?>
                        
                        <div style="margin-top: 5px;" class="col-12 col-md-12">
                            <button id="btnChargeMonthlyFee" class="btn btn-success btn-block b-r-0">Charge Monthly fee</button>
                        </div>
                        <div style="margin-top: 5px;" class="col-md-12">
                            <button id="btnSendInvoice" class="btn btn-primary btn-block b-r-0">Send Invoice</button>
                        </div>
                        <div style="margin-top: 5px;" class="col-md-12">
                            <button type="button" class="btn btn-info waves-effect btn_send_new_payment_form btn-block" >Send New Payment Form</button>
                        </div>
                        
                    <?php endif ?>
                        
                    <!-- <div style="margin-top: 5px;" class="col-md-6 p-l-0">
                        <button id="btnPrintInvoice" class="btn btn-primary btn-block b-r-0">Print Invoice</button>
                    </div> -->
                    <!-- <div style="margin-top: 5px;" class="col-12 ">
                        <button id="btnCancelSubscription" class="btn btn-danger btn-block b-r-0">Cancel Subscription</button>
                    </div> -->
                </div>

            </div> 

            <hr>
            <div class="col-12 m-t-20">
                <?php if ($userdata['login_type'] == 'Administrator'): ?>
                    <div class="text-center m-b-10" style="border: 1px solid rgba(0,0,0,.125);border-radius: 5px;padding: 10px;">
                        <h3>Monthly Subscription</h3> 
                        <!-- <span class="badge  span_monthly_subscription" style="font-size: 17px;margin-bottom: 5px;"> Active</span> -->

                        <input type="checkbox" class="js-switch" id="cb_subscription" data-color="#26c6da" data-secondary-color="#f62d51" />
                        <br>
                        <span class="span_monthly_due"></span>
                    </div>
                    <div id="accordionAuthorize" class="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne" style="border-bottom: 1px solid rgba(0,0,0,.125)">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordionAuthorize" href="#collapseTransactions" aria-expanded="true" aria-controls="collapseTransactions">
                                      Transactions
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseTransactions" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="card-body">
                                    <h3 class="text-center">Transactions</h3>
                                    <div class="table-responsive">
                                        <table class="table stylish-table" id="tbl_authorize_transactions">
                                            <thead>
                                                <tr>
                                                    <th>Trans ID</th>
                                                    <th>Name</th>
                                                    <th>Account #</th>
                                                    <th>Status</th>
                                                    <th>Amount</th>
                                                    <th>Submitted (Local)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class=" m-t-20">
                                        <h5 class="text-right">Total Amount: $<b class="transaction_total_amount"></b></h5>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo" style="border-bottom: 1px solid rgba(0,0,0,.125)">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordionAuthorize" href="#collapseSubscription" aria-expanded="true" aria-controls="collapseSubscription">
                                      Subscriptions
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseSubscription" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="card-body">
                                    <h3 class="text-center">Subscriptions</h3>
                                    <div class="table-responsive">
                                        <table class="table stylish-table" id="tbl_authorize_subscriptions">
                                            <thead>
                                                <tr>
                                                    <th>Account Number</th>
                                                    <th>Status</th>
                                                    <th>Date</th> 
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                <?php endif ?>
                    

            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.js') ?>"></script>
<script>
</script>