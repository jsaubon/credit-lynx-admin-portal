<style>
	.timeline-title input {
		border: none !important;
		font-weight: 400;
		color: inherit;
		font-size: 18px;
		padding: 0px !important;
	}

	.st_note {
		border: none !important; 
		padding: 0px !important;
	}

	hr {
		border: inset !important;
		border-top: 1px solid rgba(0,0,0,.1) !important;

	}
	.btn-success.disabled, .btn-success:disabled {
		background-color: grey;
		border-color: grey;
	}

	.editNoteNote {
		cursor: pointer;
	}
</style> 
	<div class="card-body" id="tab_conversation_history">  
		<div class="text-right">
			<button class="btn waves-effect waves-light btn-success" data-target="#modal_add_sms" data-toggle="modal"><i class="fas fa-mobile"></i> Sms</button>
			<button style="background: #727b84;color: white;box-shadow: 0 2px 2px 0 rgba(118, 232, 32, 0.14), 0 3px 1px -2px rgba(78, 156, 38, 0.2), 0 1px 5px 0 rgba(63, 255, 51, 0.12);border: 1px solid #727b84;transition: 0.2s ease-in; " class="btn waves-effect waves-light" data-target="#modal_add_email" data-toggle="modal"><i class="fas fa-envelope"></i> Email</button>
			<button style="background: #008080;color: white;box-shadow: 0 2px 2px 0 rgba(118, 232, 32, 0.14), 0 3px 1px -2px rgba(78, 156, 38, 0.2), 0 1px 5px 0 rgba(63, 255, 51, 0.12);border: 1px solid #008080;transition: 0.2s ease-in; " class="btn waves-effect waves-light" data-target="#modal_add_note" data-toggle="modal"><i class="fas fa-sticky-note" ></i> Note</button>
			<button class="hide btn waves-effect waves-light btn-primary btn_show_inbound_call_modal"><i class="fas fa-phone" ></i> Inbound Call</button>
			<button class="hide btn waves-effect waves-light btn-secondary btn_show_outbound_call_modal"><i class="fas fa-phone" ></i> Outbound Call</button>
		</div> 
		<h3 class="text-center nothing_found hide">Nothing found</h3> 
		<ul class="timeline " style="margin-top: 10px !important;height: 800px;overflow-y: auto;"> 
             
        </ul> 
	</div>

 
<div id="modal_add_note" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
             	<h5><a href="#" class="btnShowHideMakeNote">Make a note <i class="fas fa-angle-right"></i></a></h5> 
		        <div class="containerMakeNote">
		            <label class="m-b-0">Templates <small>(<a href="#" id="btnOpenNoteTemplatesModal" data-toggle="modal" data-target="#add-edit-note-templates-modal">manage</a>)(<a class="btnOpenNoteTemplates" href="#">hide</a>)</small>
		            </label> 
		            <div class="row note_templates_container" style="border: 1px solid #6ec4d7!important; padding-top: 9px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 13px; margin-bottom: 7px!important;line-height: 70%;">
		                <?php foreach ($note_templates as $key => $value): ?>
		                    <label class="col-md-2 note_template" style="color: #6ec4d7; cursor: pointer;" note_template="<?php echo $value['note_template'] ?>"><?php echo $value['note_shortname'] ?></label>
		                <?php endforeach?>
		            </div>

		            <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>
		            <input type="text" name="" class="form-control material_date_time text-center" id="new_note_date" value="<?php echo date('Y-m-d H:i:s') ?>">
		            <div class="input-group">
		                <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"></i>
		                <button style="width: 1%;flex: 1 1 auto;border-radius: 0" id="btnSaveNewNote" class="btn btn-block btn-success waves-effect waves-light" type="button">Save Note</button>

		            </div>
		        </div>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_add_call" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
             	<h5><a href="#" class="btnShowHideMakeNote"><span class="call_type"></span> Call <i class="fas fa-angle-right"></i></a></h5> 
		        <div class="containerMakeCall">
		            <label class="m-b-0">Templates <small>(<a href="#" id="btnOpenCallTemplatesModal" data-toggle="modal" data-target="#add-edit-call-templates-modal">manage</a>)(<a class="btnOpenCallTemplates" href="#">hide</a>)</small>
		            </label> 
		            <div class="row call_templates_container" style="border: 1px solid #6ec4d7!important; padding-top: 9px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 13px; margin-bottom: 7px!important;line-height: 70%;">
		                <?php foreach ($call_templates as $key => $value): ?>
		                    <label class="col-md-2 call_template" style="color: #6ec4d7; cursor: pointer;" call_template="<?php echo $value['call_template'] ?>"><?php echo $value['call_shortname'] ?></label>
		                <?php endforeach?>
		            </div>

		            <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_call" class="form-control" rows="2"></textarea>
		            <input type="text" name="" class="form-control material_date_time text-center" id="new_call_date" value="<?php echo date('Y-m-d H:i:s') ?>"> 
	                <button style="border-radius: 0" id="btnSaveNewCall" class="btn btn-block btn-success waves-effect waves-light" type="button">Save <span class="call_type"></span> Call</button>
 
		        </div>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_add_email" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
             	<form method="POST" id="formSendClientEmail">
					<label>Sender</label>
					<select class="form-control" id="inputFromClientEmail">
						<option value="no-reply@creditlynx.com">no-reply@creditlynx.com</option>
						<option value="Brent@creditlynx.com">Brent@creditlynx.com</option>
						<option value="Kayci@creditlynx.com">Kayci@creditlynx.com</option>
						<option value="Billing@creditlynx.com">Billing@creditlynx.com</option>
						<option value="Support@creditlynx.com">Support@creditlynx.com</option>
						<option value="Info@creditlynx.com">Info@creditlynx.com</option>
					</select>
					<input id="inputFromClientEmail" required="" type="text" value="no-reply@creditlynx.com" name="" class="form-control" placeholder="From..." style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
					<input id="inputSubjectClientEmail" required="" type="text" name="" class="form-control" placeholder="Subject..." style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
					<textarea id="inputMessageClientEmail" required="" style="border-top-left-radius: 0px;border-top-right-radius: 0px; " class="form-control" rows="3" placeholder="Message..."></textarea>
					<button type="submit" class="btn btn-success waves-effect waves-light pull-right">Send <i class="fas fa-paper-plane"></i></button>
				</form>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_add_sms" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
             	<form method="POST" id="formSendClientText">
					<input id="inputTitleClientText" type="text" name="" class="form-control hide" placeholder="Title..." style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">

					<textarea id="inputTextMessageClientText" required="" style="border-top-left-radius: 0px;border-top-right-radius: 0px; " class="form-control" rows="3" placeholder="Text Message..."></textarea>
					<small>Remaining: <span id="text_sms_remaining_char">160</span></small>
					<button type="submit" class="btn btn-success waves-effect waves-light pull-right">Send <i class="fas fa-paper-plane"></i></button>
				</form>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script> 
$(document).ready(function(){ 
	$('#inputTextMessageClientText').on('keyup', function(event) { 
		var text = $(this).val();
		var text_count = text.length;
		$('#text_sms_remaining_char').html(160 - text_count); 
	});
}) ;    
</script>