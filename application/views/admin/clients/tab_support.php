<div style="padding-top: 0" class="tab-pane b-t" id="tab_support" role="tabpanel">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" id="containerSupportTicketListRight">
                        <h4 class="card-title text-center">
                            <!-- Ticket List  -->
                            <a id="btnShowAddNewSupportTicketContainerRight" href="#">Start New Ticket</a>
                        </h4>
                        <!-- <h6 class="card-subtitle">List of ticket opend</h6> -->
                        <div class="row m-t-40"> 
                            <!-- Column -->
                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                <div class="card card-danger">
                                    <div class="box text-center">
                                        <h1 class="font-light text-white" id="label_pending_tickets_right"></h1>
                                        <h6 style="font-size: 70%" class="text-white">Pending</h6>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                <div class="card card-primary">
                                    <div class="box text-center">
                                        <h1 class="font-light text-white" id="label_responded_tickets_right"></h1>
                                        <h6 style="font-size: 70%" class="text-white">Responded</h6>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-6 col-lg-4 col-xlg-4">
                                <div class="card card-success">
                                    <div class="box text-center">
                                        <h1 class="font-light text-white" id="label_resolved_tickets_right"></h1>
                                        <h6 style="font-size: 70%" class="text-white">Resolved</h6>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="table-responsive">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                <thead>
                                    <tr> 
                                        <th width="50%">Subject</th>
                                        <th>Status</th>
                                        <th>Assign to</th>
                                        <th>Date</th> 
                                    </tr>
                                </thead>
                                <tbody id="tbodySupportTicketListRight">
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-body hide" id="containerAddNewSupportTicketRight">
                        <h4 class="card-title">New Ticket <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                        <!-- <h6 class="card-subtitle">Log a new help desk ticket</h6> -->
                        <div class="row m-t-40"> 
                            <div class="col-12">
                                <form id="formSupportTicket" method="POST" action="clients/saveSupportTicket"> 
                                    <div class="demo-radio-button text-center">
                                        <input type="hidden" name="sender">
                                        <input type="hidden" name="sender_id">
                                        <input class="hide" value="Sales Team" name="assigned_to" type="radio" class="with-gap" id="radio_4">
                                        <label class="hide" for="radio_4">Sales Team</label> 
                                        <input value="Dispute Team" name="assigned_to" type="radio" class="with-gap" id="radio_1" checked="">
                                        <label for="radio_1">Dispute Team</label>
                                        <input value="Billing Team" name="assigned_to" type="radio" class="with-gap" id="radio_2">
                                        <label for="radio_2">Billing Team</label>
                                        <input value="Support Team" name="assigned_to" type="radio" class="with-gap" id="radio_3">
                                        <label for="radio_3">Support Team</label> 
                                    </div>
                                    <script>
                                        $('#radio_1').on('click',function(){
                                            $('.selectResponses').addClass('hide');
                                            $('#selectDisputeTeamResponses').removeClass('hide');
                                        });
                                        $('#radio_2').on('click',function(){
                                            $('.selectResponses').addClass('hide');
                                            $('#selectBillingTeamResponses').removeClass('hide');
                                        });
                                        $('#radio_3').on('click',function(){
                                            $('.selectResponses').addClass('hide');
                                            $('#selectSupportResponses').removeClass('hide');
                                        });
                                        $('#radio_4').on('click',function(){
                                            $('.selectResponses').addClass('hide');
                                            $('#selectSalesResponses').removeClass('hide');
                                        });
                                    </script>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Templates</label>
                                        </div>
                                        <select class="form-control selectResponses" id="selectDisputeTeamResponses">
                                            <option value="">Select Response</option>
                                             <?php foreach ($dispute_team_responses as $key => $value): ?>
                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                             <?php endforeach ?>
                                        </select>
                                        <select class="form-control selectResponses hide" id="selectBillingTeamResponses">
                                            <option value="">Select Response</option>
                                             <?php foreach ($billing_team_responses as $key => $value): ?>
                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                             <?php endforeach ?>
                                        </select>
                                        <select class="form-control selectResponses hide" id="selectSupportResponses">
                                            <option value="">Select Response</option>
                                             <?php foreach ($support_team_responses as $key => $value): ?>
                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                             <?php endforeach ?>
                                        </select>
                                        <select class="form-control selectResponses hide" id="selectSalesResponses">
                                            <option value="">Select Response</option>
                                             <?php foreach ($sales_team_responses as $key => $value): ?>
                                                 <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                             <?php endforeach ?>
                                        </select>
                                    </div>
                                    <script>
                                        $('.selectResponses').on('change',function(){
                                            var value = $(this).val();
                                            var text = $(this).find('option:selected').text();
                                            $('#new_support_subject').val(text);
                                            $('#new_support_message').val(value);
                                        });
                                    </script>
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input required="" id="new_support_subject" type="text" name="subject" class="form-control">
                                        <span class="bar"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea required="" id="new_support_message" rows="4" class="form-control" name="message"></textarea>
                                        <span class="bar"></span>
                                    </div>

                                    

                                    <button type="submit" class="btn btn-success waves-effect waves-light pull-right" >Submit</button>
                                </form>
                            </div>
                                
                        </div> 

                    </div>
                    <div class="card-body hide" id="containerSupportTicketRight">
                        <h4 class="card-title">Ticket Details <a href="#" class="pull-right btnBackToSupportTicketList" id="">Back</a></h4>
                        <!-- <h6 class="card-subtitle">Ticket Details</h6> -->
                        <div class="row m-t-40"> 
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Status</label>
                                    </div>
                                    <select class="form-control support_status" id="support_status">
                                        <option value="Pending">Pending</option>
                                        <option value="Responded">Responded</option>
                                        <option value="Resolved">Resolved</option>  
                                    </select>
                                </div>
                            </div> 
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Subject</label>
                                    </div>
                                    <input class="form-control support_subject" type="text" disabled="" name="" value="test">
                                </div>
                                
                            </div>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Date</label>
                                    </div>
                                    <input class="form-control support_support_date" type="text" disabled="" name="" value="12-12-12">
                                </div>
                            </div> 
                            <div class="col-12">
                                <textarea disabled="" style="font-size: 70%" class="form-control support_message" rows="4"></textarea>
                            </div>
                            
                            <div class="col-12">
                                <label><span id="support_assigned_to"></span> Reponses</label>
                                <div class="table-responsive supportTblScroll">
                                    <table class="table table-hover no-wrap">
                                        <thead class="hide">
                                            <tr>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodySupportTicketReponsesRight">
                                            <tr>
                                                <td style="width: 100%">
                                                    <label>responder_name</label>
                                                    <span class="pull-right">datetime</span>
                                                    <blockquote>asdas</blockquote>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                    
                            </div>
                            <div class="col-12">
                                 <label>Add a response?</label>
                                 <div class="input-group">
                                     <div class="input-group-prepend">
                                         <label class="input-group-text">Responses</label>
                                     </div>
                                     <select class="form-control" id="selectAddResponsesRight">
                                        <option value="">Select Response</option>
                                         <?php foreach ($support_responses as $key => $value): ?>
                                             <option value="<?php echo $value ?>"><?php echo $key ?></option>
                                         <?php endforeach ?>
                                     </select>
                                     <script>
                                        $('#selectAddResponsesRight').on('change',function(){
                                            var value = $(this).val();
                                            $('#support_response_right').val(value);
                                        });
                                     </script>
                                 </div>
                                 <textarea id="support_response_right" rows="5" style="font-size: 70%" class="form-control"></textarea>
                                 <button class="btnSaveSupportTicketResponseRight btn btn-success waves-effect waves-light">Submit Reponse</button>
                            </div>

                                
                        </div> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>