
<style>

    #select_client_accounts_chosen {
        min-height: calc(2.25rem + 2px) !important;
        width: calc(100% - 102.98px) !important;
    }

    #select_client_accounts_chosen .chosen-choices{
        min-height: calc(2.25rem + 2px) !important;
    }
    #select_client_accounts_chosen .chosen-search-input{
        height: 34px !important;
    }
    #select_client_accounts_chosen .search-choice{
        line-height: 21px !important;
    }
    .btnFetchDataTemplateBureau {
        background: transparent;
        border: 0px;
        box-shadow: none;
        cursor: default;
    }

    #modal-BureauAccounts textarea{
        font-size: 12px;
        line-height: 100%;
    }
    #modal-BureauAccounts label{
        font-size: 12px;
        margin-bottom: 0px;
    }
    #modal-BureauAccounts select.form-control:not([size]):not([multiple]){
        height: 28px;
        font-size: 65%;
    }

    #tableCreditorsRec tbody tr td:nth-child(2) , #tableCreditorsAccountType tbody tr td:nth-child(2) , #tableAccountTypes tbody tr td:nth-child(2) , #tableAccountStatuses tbody tr td:nth-child(2) , #tableAccountPaymentStatuses tbody tr td:nth-child(2) , #tableAccountComments tbody tr td:nth-child(2) {
        white-space: pre !important;
    }

    #modal-BureauAccounts .roundDates {
        min-height: 0px !important;
        font-size: 60%;
        margin-top: 6.4px;
        text-align: right;
        padding-right: 5px;
        float: right;
        border: none;
    }

    #modal-BureauAccounts input {
        min-height: 0px;
        font-size:60%;
    }

    #modal-BureauAccounts .rightLabel {
        font-size: 8px;
        right: 8px;
        top: 4px;
    }

    #modal-BureauAccounts select {
        /* for Firefox */
        -moz-appearance: none;
        /* for Chrome */
        -webkit-appearance: none;
        appearance: none;
        min-height: 28px;
        font-size: 65%;
        height: 28px;
    }


    /* For IE10 */
    #modal-BureauAccounts select::-ms-expand {
        display: none;
        min-height: 28px;
        font-size: 65%;
    }

</style>
<div id="modal-BureauAccounts" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="max-width: 95%;">
        <div class="modal-content">
            <div class="modal-header">

                <span><span style="font-size: 18px" id="labelBureau"></span> <small>Status -
                    <span class="text-warning">Deleted</span> |
                    <span class="text-warning">Fixed</span> |
                    <span class="text-warning">Not Reporting</span> |
                    <span class="text-warning">Hold</span>
                     are not included on the list</small>
                 </span>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-t-0 ">
                <div class="row">
                    <div class="col-12 col-md-2 offset-md-5 text-center m-b-10 m-t-20">
                        <h5>Account Type</h5>
                        <select class="selectBureauAccountByType form-control" style="text-align-last: center;">
                            <option value="All Types">All Types</option>
                            <?php foreach ($clients_account_types as $key => $value): ?>
                                <option value="<?php echo $value['account_type'] ?>"><?php echo $value['account_type'] ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="col-12 text-center m-b-10 containerGenerateButtons hide">
                        <h4>Generate Verbiage Rounds</h4>
                        <button type="button" round="1" class="btnGenerateRound btn btn-secondary">Round 1</button>
                        <button type="button" round="2" class="btnGenerateRound btn btn-secondary">Round 2</button>
                        <button type="button" round="3" class="btnGenerateRound btn btn-secondary">Round 3</button>
                        <button type="button" round="4" class="btnGenerateRound btn btn-secondary">Round 4</button>
                        <button type="button" round="5" class="btnGenerateRound btn btn-secondary">Round 5</button>
                        <button type="button" round="6" class="btnGenerateRound btn btn-secondary">Round 6</button>
                        <button type="button" round="7" class="btnGenerateRound btn btn-secondary">Round 7</button>
                        <button type="button" round="8" class="btnGenerateRound btn btn-secondary">Round 8</button>
                        <button type="button" round="9" class="btnGenerateRound btn btn-secondary">Round 9</button>
                    </div>
                </div>
                <div class="row" rounds="1-5">
                    <div class="col-2"></div> 
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="1" copy_to="2">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="2">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="2" copy_to="3">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="3">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="3" copy_to="4">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="4">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="4" copy_to="5">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="5">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div> 
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="5" copy_to="6">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="6">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div> 
                </div>
                <div class="row" rounds="6-9">
                    <div class="col-2"></div> 
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="6" copy_to="7">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="7">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="7" copy_to="8">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="8">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div>
                    <div class="col-2 text-center">
                        <a href="#" class="copy_verbiages" copy_from="8" copy_to="9">Repeat <i class="fa-lg fas fa-angle-double-right"></i></a>
                        <a href="#" class="random_verbiages" random_to="9">Next <i class="fa-lg fas fa-angle-double-right"></i></a>
                    </div> 
                </div>
                <div class="containerBureauAccounts p-10 b-t b-b b-l b-r">

                </div>
                <script type="text/javascript">
                    $(function(){
                        $('#modal-BureauAccounts').on('change','.selectBureauAccountByType',function(e){
                            e.preventDefault();
                            var account_type = $(this).val();
                            var accounts = $('#modal-BureauAccounts .containerBureauAccounts .row').removeClass('hide');
                            if (account_type != 'All Types') {
                                var accounts = $('#modal-BureauAccounts .containerBureauAccounts').find('[field="account_type"]');
                                $.each(accounts,function(key,account){
                                    var type = $(account).val();
                                    if (type != account_type) {
                                        $(account).closest('.row').addClass('hide');
                                    }
                                });
                                var haveRows = 0;
                                $.each(accounts,function(key,account){
                                    if (!$(account).closest('.row').hasClass('hide')) {
                                        haveRows++;
                                    }
                                });

                                $('#modal-BureauAccounts .containerBureauAccounts').find('.noAccountsFound').remove();;
                                if (!haveRows) {
                                    $('#modal-BureauAccounts .containerGenerateButtons').addClass('hide');
                                    $('#modal-BureauAccounts .containerBureauAccounts').append('<h3 class="noAccountsFound text-center">No Accounts Found</h3>');
                                } else {
                                    $('#modal-BureauAccounts .containerGenerateButtons').removeClass('hide');
                                }


                            } else {
                                $('#modal-BureauAccounts .containerGenerateButtons').addClass('hide');
                            }
                        });

                        $('#modal-BureauAccounts').on('click','.btnGenerateRound',function(key,value){
                            var account_type = $('#modal-BureauAccounts .selectBureauAccountByType').val();
                            var round = $(this).attr('round');
                            console.log(account_type);
                            console.log(round);
                            $.post('<?php echo base_url('admin/clients/getDisputeVerbiagesByATandRound') ?>',{account_type,round}, function(data){
                                var round_dv = JSON.parse(data);
                                var round_dv_length = round_dv.length;
                                if (round_dv_length > 0) {
                                    var accounts = $('#modal-BureauAccounts .containerBureauAccounts').find('[field="account_type"]');
                                    $.each(accounts,function(key,account){
                                        var type = $(account).val();
                                        if (type == account_type) {
                                            var row = $(account).closest('.row');
                                            var find_round = row.find('[field="dispute_'+round+'"]');
                                            var random_number = Math.floor(Math.random() * round_dv_length) + 0;
                                            find_round.val(round_dv[random_number]['dispute_verbiage']);
                                            find_round.trigger('change');
                                        };
                                    });
                                } else {

                                }

                            }).fail(function(xhr){
                                console.log(xhr.responseText);
                            });
                        });

                        $('#modal-BureauAccounts').on('click', '.copy_verbiages', function(event) {
                            event.preventDefault();
                            var from = $(this).attr('copy_from');
                            var to = $(this).attr('copy_to');
                            var containerBureauAccounts = $('.containerBureauAccounts').find('.row');
                            $.each(containerBureauAccounts, function(index, col) {
                                var verbiage = $(col).find('[field="dispute_'+from+'"]').val(); 
                                $(col).find('[field="dispute_'+to+'"]').val(verbiage);
                                $(col).find('[field="dispute_'+to+'"]').trigger('change');
                            });
                                
                        });

                        $('#modal-BureauAccounts').on('click', '.random_verbiages', function(event) {
                            event.preventDefault();
                            var round = $(this).attr('random_to');

                            var containerBureauAccounts = $('.containerBureauAccounts').find('.row');
                            $.each(containerBureauAccounts, function(index, col) {
                                var account_type = $(col).find('[field="account_type"]').val(); 
                                var dispute_box = $(col).find('[field="dispute_'+round+'"]');
                                get_random_verbiage(account_type,round,dispute_box);
                            });
                        });

                        function get_random_verbiage(account_type,round,dispute_box) {
                            var data =  {
                                            table: 'clients_dispute_verbiage_assignments',
                                            action: 'get',
                                            select: {
                                                        
                                                    },
                                            where:  {
                                                        account_type: account_type,
                                                        round: round
                                                    },
                                            field: '',
                                            order: '',
                                            limit: 0,
                                            offset: 0,
                                            group_by: ''
                            
                                        };
                            $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
                                data = JSON.parse(data);
                                
                                var max = data.length -1;
                                var random = Math.floor(Math.random() * max);
                                $(dispute_box).val(data[random]['dispute_verbiage']);
                                $(dispute_box).trigger('change');
                            });
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- sample modal content -->
<div id="add-edit-client-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Client Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"   method="post" action="<?php echo base_url('admin/clients/saveDetail') ?>" class="floating-labels formAddUserClient">
                    <input type="hidden" name="converted" value="0">
                    <input type="hidden" name="client_id">
                    <input type="hidden" name="page_title" value="clients">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <select class="select2" name="sale_id" id="sale_id_client" style="width: 100%">
                                            <option value="0">Select Sales</option>
                                            <?php foreach ($userdata['sales'] as $key => $value): ?>
                                                <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <select class="select2" name="broker_id" id="broker_id_client" style="width: 100%">
                                            <option value="0">Select Broker</option>
                                            <?php foreach ($userdata['brokers'] as $key => $value): ?>
                                                <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <select class="select2" name="agent_id" id="agent_id_client" style="width: 100%">
                                            <option value="0">Select Agent</option>
                                            <?php foreach ($userdata['agents'] as $key => $value): ?>
                                                <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 col-xs-6 b-r">
                            <h4 style="margin-bottom: 17px">Personal Information</h4>
                            <div class="form-group">
                                <label for="name" class="control-label">Name:</label>
                                <input type="text" name="name" required="" class="form-control" id="name_client">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="ss" class="control-label">Social</label>
                                <input type="text" name="ss" class="form-control" id="ss_client">
                            </div>
                            <div class="form-group">
                                <label for="date_of_birth" class="control-label">Date of Birth</label>
                                <input type="text" name="date_of_birth" class="form-control" id="date_of_birth_client">
                            </div>


                            <div class="form-group">
                                <label for="email_address" class="control-label">Email</label>
                                <input type="email" name="email_address" class="form-control" id="email_address_client">
                            </div>

                            <div class="form-group">
                                <label for="alt_phone" class="control-label">Phone</label>
                                <input type="text" name="alt_phone" class="form-control phone-inputmask" id="alt_phone_client">
                            </div>

                            <div class="form-group">
                                <label for="cell_phone" class="control-label">Cell Phone</label>
                                <input type="text" name="cell_phone" class="form-control phone-inputmask" id="cell_phone_client">
                            </div>
                            <div class="form-group">
                                <h5 >Carrier</h5>
                                <select class="select2" name="carrier" id="carrier" style="width: 100%">
                                    <option value="0">Select Carrier</option>
                                    <option value="ATT">ATT</option>
                                    <option value="Boost">Boost</option>
                                    <option value="Cricket">Cricket</option>
                                    <option value="Metro PCS">Metro PCS</option>
                                    <option value="Simple">Simple</option>
                                    <option value="Sprint">Sprint</option>
                                    <option value="TMobile">TMobile</option>
                                    <option value="Verizon">Verizon</option>
                                    <option value="Virgin">Virgin</option>
                                    <option value="Do Not SMS">Do Not SMS</option>
                                    <option value="Landline">Landline</option>
                                </select>
                            </div>
                            <h4 style="margin-bottom: 17px">User Account Information</h4>
                            <div class="form-group">
                                <label for="username" class="control-label">Username:</label>
                                <input type="text" name="username" required="" class="form-control" id="username_client">
                                <span class="bar"></span>
                                <small id="errorUsernameSingle" class="errorUsername hide form-control-feedback text-danger"> Username already exist!. </small>
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password:</label>
                                <input type="password" name="password" required="" class="form-control" id="password_client">
                                <span class="bar"></span>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-6 b-r">
                            <h4 style="margin-bottom: 17px">Address Information</h4>
                            <div class="form-group">
                                <label for="address" class="control-label">Address:</label>
                                <input type="text" name="address" required="" class="form-control" id="address_client">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <label for="city" class="control-label">City:</label>
                                <input type="text" name="city" required="" class="form-control" id="city_client">
                                <span class="bar"></span>
                            </div>
                            <div class="form-group">
                                <h5>State/Province:</h5>
                                <select class="select2" name="state_province" id="state_province_client" style="width: 100%">
                                    <option value="0">Select State/Province</option>
                                    <?php foreach ($state_provinces as $key => $value): ?>
                                        <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="zip_postal_code" class="control-label">Zip/Postal Code:</label>
                                <input type="text" name="zip_postal_code" required="" class="form-control" id="zip_postal_code_client">
                                <span class="bar"></span>
                            </div>
                            <div class="text-center">
                                Joint Account? <input  type="checkbox" class="js-switch" data-color="#26c6da" id="toggleJointContentClient" />
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-6 b-r">


                            <div id="joint_content_client" class="hide ">
                                <h4 style="margin-bottom: 17px">Spouse Information</h4>
                                <div class="form-group">
                                    <label for="joint_name" class="control-label">Name:</label>
                                    <input type="text" name="joint_name" class="form-control" id="joint_name_client">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_ss" class="control-label">Social:</label>
                                    <input type="text" name="joint_ss" class="form-control" id="joint_ss_client">
                                    <span class="bar"></span>
                                </div>

                                <div class="form-group">
                                    <label for="joint_date_of_birth" class="control-label">Date of Birth:</label>
                                    <input type="text" name="joint_date_of_birth" class="form-control" id="joint_date_of_birth_client">
                                    <span class="bar"></span>
                                </div>

                                <div class="form-group">
                                    <label for="joint_email_address" class="control-label">Email:</label>
                                    <input type="email" name="joint_email_address" class="form-control" id="joint_email_address_client">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_cell_phone" class="control-label">Cell Phone:</label>
                                    <input type="text" name="joint_cell_phone" class="form-control phone-inputmask" id="joint_cell_phone_client">
                                    <span class="bar"></span>
                                </div>
                                 <div class="form-group">
                                    <label for="joint_username" class="control-label">Username:</label>
                                    <input type="text" name="joint_username" class="form-control" id="joint_username_client">
                                    <span class="bar"></span>
                                    <small id="errorUsernameJoint" class="errorUsername hide form-control-feedback text-danger"> Username already exist!. </small>
                                </div>
                                <div class="form-group">
                                    <label for="joint_password" class="control-label">Password:</label>
                                    <input type="password" name="joint_password" class="form-control" id="joint_password_client">
                                    <span class="bar"></span>
                                </div>


                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button  class="btn btn_formAddUserClient btn-success waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-client-joint-account" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Convert to Joint Account Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"   method="post" action="<?php echo base_url('admin/clients/ConvertToJointAccount') ?>" class="floating-labels formAddUser">
                    <input type="hidden" name="converted" value="0">
                    <input type="hidden" name="profile_client_id">
                    <input type="hidden" name="page_title" value="clients">

                    <div class="row">

                        <div class="col-md-12 col-xs-12">

                            <div id="joint_content_client" >
                                <h4 style="margin-bottom: 17px">Spouse Information</h4>
                                <div class="form-group">
                                    <label for="joint_name" class="control-label">Name:</label>
                                    <input type="text" name="joint_name" class="form-control"  >
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_ss" class="control-label">Social:</label>
                                    <input type="text" name="joint_ss" class="form-control"  >
                                    <span class="bar"></span>
                                </div>

                                <div class="form-group">
                                    <label for="joint_date_of_birth" class="control-label">Date of Birth:</label>
                                    <input type="text" name="joint_date_of_birth" class="form-control"  >
                                    <span class="bar"></span>
                                </div>

                                <div class="form-group">
                                    <label for="joint_email_address" class="control-label">Email:</label>
                                    <input type="email" name="joint_email_address" class="form-control" >
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_cell_phone" class="control-label">Cell Phone:</label>
                                    <input type="text" name="joint_cell_phone" class="form-control phone-inputmask"  >
                                    <span class="bar"></span>
                                </div>
                                 <div class="form-group">
                                    <label for="joint_username" class="control-label">Username:</label>
                                    <input type="text" name="joint_username" class="form-control" >
                                    <span class="bar"></span>
                                    <small id="errorUsernameJoint" class="errorUsername hide form-control-feedback text-danger"> Username already exist!. </small>
                                </div>
                                <div class="form-group">
                                    <label for="joint_password" class="control-label">Password:</label>
                                    <input type="password" name="joint_password" class="form-control"  >
                                    <span class="bar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="add-edit-lead-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Clients Information
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  method="post" action="<?php echo base_url('admin/clients/saveDetail') ?>" class="floating-labels formAddUserLead">
                    <input type="hidden" name="converted" value="0">
                    <input type="hidden" name="client_id">
                    <input type="hidden" name="page_title" value="clients">

                    <div class="row">

                        <div class="col-md-4 col-xs-6 b-r">

                            <div class="form-group">
                                <select class="select2" name="broker_id" id="broker_id" style="width: 100%">
                                    <option value="0">Select Broker</option>
                                    <?php foreach ($userdata['brokers'] as $key => $value): ?>
                                        <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="select2" name="sale_id" id="sale_id" style="width: 100%">
                                    <option value="0">Select Sales</option>
                                    <?php foreach ($userdata['sales'] as $key => $value): ?>
                                        <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="select2" name="agent_id" id="agent_id" style="width: 100%">
                                    <option value="0">Select Agent</option>
                                    <?php foreach ($userdata['agents'] as $key => $value): ?>
                                        <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach?>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-6 b-r">
                            <h4>Personal Information</h4>
                            <div class="form-group">
                                <label for="name" class="control-label">Name:</label>
                                <input type="text" name="name" required="" class="form-control" id="name">
                                <span class="bar"></span>
                            </div>


                            <div class="form-group">
                                <label for="email_address" class="control-label">Email</label>
                                <input type="email" name="email_address" class="form-control" id="email_address">
                            </div>


                            <div class="form-group">
                                <label for="cell_phone" class="control-label">Cell Phone</label>
                                <input type="text" name="cell_phone" class="form-control phone-inputmask" id="cell_phone">
                            </div>
                            <div class="text-center">
                                Joint Account? <input  type="checkbox" class="js-switch" data-color="#26c6da" id="toggleJointContentLead" />
                            </div>

                        </div>
                        <div class="col-md-4 col-xs-6 b-r">
                            <div id="joint_content_lead" class="hide ">
                                <h4>Spouse Information</h4>
                                <div class="form-group">
                                    <label for="joint_name" class="control-label">Name:</label>
                                    <input type="text" name="joint_name" class="form-control" id="joint_name">
                                    <span class="bar"></span>
                                </div>

                                <div class="form-group">
                                    <label for="joint_email_address" class="control-label">Email:</label>
                                    <input type="email" name="joint_email_address" class="form-control" id="joint_email_address">
                                    <span class="bar"></span>
                                </div>
                                <div class="form-group">
                                    <label for="joint_cell_phone" class="control-label">Cell Phones:</label>
                                    <input type="text" name="joint_cell_phone" class="form-control phone-inputmask" id="joint_cell_phone">
                                    <span class="bar"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button  class="btn btn_formAddUserLead btn-success waves-effect waves-light">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-alert-templates-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Alert Templates
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New template</h4>
                        <div class="input-group">
                            <div class="input-group-append">
                                <input type="hidden" name="" id="new_alert_template_id">
                                <input id="new_template_shortname" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Short Name">
                            </div>
                                <input id="new_template_subject" style="border-radius: 0" type="text" name="" class="form-control" placeholder="Subject">

                        </div>
                        <div class="input-group">
                            <textarea id="new_template_notes" style="border-radius: 0;line-height: 100%" class="form-control" placeholder="Notes" rows="3"></textarea>
                            <div class="input-group-prepend">
                                <button id="btnSaveNewAlertTemplate" style="width: 1%;flex: 1 1 auto;border-radius: 0" class="btn btn-info waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
                <table id="tableAlertTemplates" class="table stylish-table">
                    <thead>
                        <th>Short Name</th>
                        <th>Subject</th>
                        <th>Notes</th>
                        <th width="1px"></th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-account-types-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Account Types
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New Account Type</h4>
                        <div class="input-group">
                            <input id="new_account_type" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Account Type">
                            <div class="input-group-append">
                                <button id="btnSaveAccountType" class="btn btn-success waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="tableAccountTypes" class="table stylish-table">
                    <thead>
                        <th>Account Type</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-account-status-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Account Status
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New Account Status</h4>
                        <div class="input-group">
                            <input id="new_account_status" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Account Status">
                            <div class="input-group-append">
                                <button id="btnSaveAccountStatus" class="btn btn-success waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="tableAccountStatuses" class="table stylish-table">
                    <thead>
                        <th>Account Status</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-account-payment-status-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Account Payment Status
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New Account Payment Status</h4>
                        <div class="input-group">
                            <input id="new_payment_status" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Account Payment Status">
                            <div class="input-group-append">
                                <button id="btnSaveAccountPaymentStatus" class="btn btn-success waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="tableAccountPaymentStatuses" class="table stylish-table">
                    <thead>
                        <th>Account Payment Status</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-account-comments-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                Account Comments
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New Account Comments</h4>
                        <div class="input-group">
                            <input id="new_comment" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Account Comments">
                            <div class="input-group-append">
                                <button id="btnSaveAccountComment" class="btn btn-success waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="tableAccountComments" class="table stylish-table">
                    <thead>
                        <th>Account Comments</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-dispute-verbiage-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Dispute Verbiage
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <ol class="breadcrumb" style="background-color: white">
                            <li class="breadcrumb-item"><a href="#" class="btnShowATTable">Account Types</a></li>
                            <li class="breadcrumb-item hide"><a href="#" class="btnShowRoundsTable"></a></li>
                            <li class="breadcrumb-item hide"><a href="#" class="btnShowDVTable"></a></li>
                        </ol>
                    </div>
                    <div class="col-12 hide containerDisputeVerbiages">
                        <label>Dispute Verbiage</label>
                        <select id="selectDisputeVergiageAssignment" class="form-control select2" style="width: 100%">
                            <option>Select Verbiage</option>
                            <?php foreach ($dispute_verbiages as $key => $value): ?>
                                <option value="<?php echo $value['dispute_verbiage'] ?>"><?php echo $value['dispute_verbiage'] ?></option>
                            <?php endforeach?>
                        </select>
                        <textarea name="" id="selectDisputeVergiageAssignmentInput" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                    <div class="col-12 text-right m-t-10 hide containerDisputeVerbiages">
                        <button class="btn btn-success btnSaveDisputeVerbiagesAssignment">Add</button>
                    </div>
                </div>
                <table id="tbl_dva_at" class="table stylish-table">
                    <thead>
                        <th>Account Type</th>
                        <th width="1" class="text-center">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <table id="tbl_dva_rounds" class="table stylish-table hide">
                    <thead>
                        <th>Rounds</th>
                        <th width="1" class="text-center">Tools</th>
                    </thead>
                    <tbody>
                        <tr round="1">
                            <td>Round 1</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="2">
                            <td>Round 2</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="3">
                            <td>Round 3</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="4">
                            <td>Round 4</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="5">
                            <td>Round 5</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="6">
                            <td>Round 6</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="7">
                            <td>Round 7</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="8">
                            <td>Round 8</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                        <tr round="9">
                            <td>Round 9</td>
                            <td><a href="#" class="btnGetTableDPByRounds btn btn-success btn-sm btn-rounded">Verbiages <i class="fa fa-angle-double-right"></i></a></td>
                        </tr>
                    </tbody>
                </table>
                <table id="tbl_dva_dv" class="table stylish-table hide">
                    <thead>
                        <th>Verbiage</th>
                        <th width="1" class="text-center">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-dispute-verbiage-for-results-tracker" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label style="margin-bottom: 0;margin-top: 10px">Select Account Type</label>
                            <select name="select_dv_for_results_tracker" id="select_dv_for_results_tracker" style="width: 100%">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-note-templates-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Note Templates
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New template</h4>
                        <div class="input-group">
                            <div class="input-group-append">
                                <input type="hidden" name="" id="new_note_template_id">
                                <input id="new_note_shortname" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Short Name">
                            </div>

                        </div>
                        <div class="input-group">
                            <textarea id="new_note_template" style="border-radius: 0;line-height: 100%" class="form-control" placeholder="Notes" rows="3"></textarea>
                            <div class="input-group-prepend">
                                <button id="btnSaveNewNoteTemplate" style="width: 1%;flex: 1 1 auto;border-radius: 0" class="btn btn-info waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
                <table id="tableNoteTemplates" class="table stylish-table">
                    <thead>
                        <th>Short Name</th>
                        <th>Notes</th>
                        <th>Action</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="add-edit-credit-report-types-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Credit Report Types
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New Credit Report Type</h4>
                        <div class="input-group">
                            <input id="new_cr_type" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Credit Report Type">
                            <div class="input-group-append">
                                <button id="btn_save_cr_type"  class="btn btn-success waves-effect waves-light " type="button">Save</button>
                            </div>

                        </div>

                    </div>
                </div>
                <table id="tableCreditReportTypes" class="table stylish-table">
                    <thead>
                        <th>Credit Report Type</th> 
                        <th style="width: 1px">Action</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="add-edit-call-templates-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Call Templates
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New template</h4>
                        <div class="input-group">
                            <div class="input-group-append">
                                <input type="hidden" name="" id="new_call_template_id">
                                <input id="new_call_shortname" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Short Name">
                            </div>

                        </div>
                        <div class="input-group">
                            <textarea id="new_call_template" style="border-radius: 0;line-height: 100%" class="form-control" placeholder="Calls" rows="3"></textarea>
                            <div class="input-group-prepend">
                                <button id="btnSaveNewCallTemplate" style="width: 1%;flex: 1 1 auto;border-radius: 0" class="btn btn-info waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
                <table id="tableCallTemplates" class="table stylish-table">
                    <thead>
                        <th>Short Name</th>
                        <th>Notes</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-task-templates-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Task Templates
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h4>New template</h4>
                        <div class="input-group">
                            <div class="input-group-append">
                                <input type="hidden" name="" id="new_task_template_id">
                                <input id="new_task_shortname" style="border-radius: 0"  type="text" name="" class="form-control" placeholder="Short Name">
                            </div>

                        </div>
                        <div class="input-group">
                            <textarea id="new_task_template" style="border-radius: 0;line-height: 100%" class="form-control" placeholder="Task" rows="3"></textarea>
                            <div class="input-group-prepend">
                                <button id="btnSaveNewTaskTemplate" style="width: 1%;flex: 1 1 auto;border-radius: 0" class="btn btn-info waves-effect waves-light " type="button">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
                <table id="tableTaskTemplates" class="table stylish-table">
                    <thead>
                        <th>Short Name</th>
                        <th>Tasks</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="loading-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                Uploading Files
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <label>please wait</label><br>
                        <div class="progress">
                            <div class="progress-bar bg-success wow animated progress-animated loading_percent_div" style="width: 65%; height:6px;" role="progressbar"> <span class="sr-only"><span id="loading_percent"></span> Complete</span> </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="bureau-letters-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;width: 100%">
    <div class="modal-dialog modal-lg" style="max-width: 90% !important">
        <div class="modal-content">
            <div class="modal-header">
                <!-- TITLE -->
                <h3>Bureau Letters</h3>


                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" >

                <div class="row" >
                    <div class="col-12">

                            <div id="containerAddEditTemplates" class="input-group">
                                <div class="input-group-prepend">
                                    <label style="min-width: 0px !important;" class="input-group-text">Header
                                        <i style="cursor: pointer;" class="btnShowBureauLettersTemplateHeader fas fa-plus-circle text-themecolor p-l-10"></i>
                                    </label>
                                </div>
                                <select id="bureau_letters_template_header" class="form-control" style="height: calc(2.25rem + 2px) !important">

                                </select>
                                <div class="input-group-prepend">
                                    <label style="min-width: 0px !important;" class="input-group-text">Title
                                        <i style="cursor: pointer;" class="btnShowBureauLettersTemplateTitle fas fa-plus-circle text-themecolor p-l-10 "></i>
                                    </label>
                                </div>
                                <select id="bureau_letters_template_title" class="form-control" style="height: calc(2.25rem + 2px) !important">

                                </select>
                                <div class="input-group-append">
                                    <button id="" class="btnShowSaveTemplateBureau btn btn-success ">Edit Template</button>
                                    <button id="" class="btnSaveBureauLetterTemplate btn btn-success hide">Save Template</button>
                                </div>
                            </div>

                            <div id="containerBureauLetterTemplateHeaders" class="hide row m-t-10">
                                <div class="col-12 col-md-4 offset-md-4">
                                    <form action="" method="POST" id="formSaveBureauLetterTemplateHeaders">
                                        <div class="input-group">
                                            <input required type="text" class="form-control" placeholder="New Template Header" name="template_header">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-success" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table stylish-table " id="tableBureauLetterTemplateHeaders">
                                        <thead>
                                            <tr>
                                                <th>Template Header</th>
                                                <th class="text-center">Tools</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="containerBureauLetterTemplateTitles" class="hide row m-t-10">
                                <div class="col-12 col-md-4 offset-md-4">
                                    <form action="" method="POST" id="formSaveBureauLetterTemplateTitles">
                                        <div class="input-group">
                                            <input required type="text" class="form-control" placeholder="New Template Title" name="template_title">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-success" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table stylish-table " id="tableBureauLetterTemplateTitles">
                                        <thead>
                                            <tr>
                                                <th>Template Title</th>
                                                <th class="text-center">Tools</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="text-right">
                                <a href="#" class=" m-t-10 btnShowTemplateHelpers" id="">Show/Hide Helpers</a>
                            </div>

                            <div class="hide containerTemplateHelpers" id="">
                                <div class="card">
                                     <div class="card-body">
                                        <h4>Helpers</h4>
                                         <div class="row m-t-20">
                                            <div class="col-6 b-r">
                                                <h5>Profile</h5>
                                                name = {NAME} <br>
                                                address = {ADDRESS} <br>
                                                city = {CITY}<br>
                                                state = {STATE}<br>
                                                zip = {ZIP}<br>
                                                current date = {CURDATE} <br>
                                                current date (mm/dd/yyyy) = {CURRENT_DATE} <br>
                                                social = {SSN} <br>
                                                date of birth = {DOB} <br>
                                                <!-- <br>
                                                Creditor Details = {CREDITOR DETAILS} <br>
                                                Account Number = {ACCOUNT#} <br> -->

                                            </div>
                                            <div class="col-6">
                                                <h5>Account Round List</h5>
                                                round 1 Equifax = {EQUIFAX ACCOUNT LIST RND1} <br>
                                                round 1 Experian = {EXPERIAN ACCOUNT LIST RND1} <br>
                                                round 1 TransUnion = {TRANSUNION ACCOUNT LIST RND1} <br>
                                                round 2 Equifax = {EQUIFAX ACCOUNT LIST RND2} <br>
                                                round 2 Experian = {EXPERIAN ACCOUNT LIST RND2} <br>
                                                round 2 TransUnion = {TRANSUNION ACCOUNT LIST RND2} <br>
                                                etc.
                                            </div>
                                        </div>
                                     </div>
                                </div>


                            </div>
                            <button class="btnFetchDataTemplateBureau btn btn-success m-t-20 hide">Fetch Data <i class="fas fa-database"></i></button>


                        <div class="">
                            <form method="post">
                                <textarea id="bureauMCE" class="mymce" name="area"></textarea>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="creditor-letters-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;width: 100%">
    <div class="modal-dialog modal-lg" style="max-width: 90% !important">
        <div class="modal-content">
            <div class="modal-header">
                <!-- TITLE -->
                <h3>Creditor letters</h3>


                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" >

                <div class="row" >
                    <div class="col-12">

                            <div id="containerAddEditTemplates" class="input-group">
                                <div class="input-group-prepend hide">
                                    <label class="input-group-text">Header</label>
                                </div>
                                <select id="template_header_creditors" class="form-control hide" style="height: calc(2.25rem + 2px) !important">
                                    <option value="">Select Creditor</option>
                                    <option value="Creditor Letters">Creditor Letters</option>
                                </select>
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Title
                                        <a style="margin-left: 5px" href="#" class="btnShowCreditorLettersTemplateTitle"> <i class="fas fa-plus-circle"></i></a>
                                    </label>
                                </div>
                                <select id="template_title_creditor" class="form-control" style="height: calc(2.25rem + 2px) !important">

                                </select>
                                <div class="input-group-append">
                                    <button id="" class="btnShowSaveTemplateCreditor btn btn-success ">Edit Template</button>
                                    <button id="" class="btnSaveCreditorLetterTemplate btn btn-success hide">Save Template</button>
                                </div>
                            </div>
                            <div id="containerCreditorsSelect" class="row">
                                <div class="col-12 col-md-6" style="padding-right: 0">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Creditors <a style="margin-left: 5px" href="#" id="btnAddCreditor"> <i class="fas fa-plus-circle"></i></a> </label>
                                        </div>
                                        <select  id="select_letter_creditors" class="form-control" style="height: calc(2.25rem + 2px) !important">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Account</label>
                                        </div>
                                        <select data-placeholder="Select Account" id="select_client_accounts" class="chosen-select form-control" multiple>
                                            <option value="">Select Account</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="hide animated fadeIn col-12" id="containerAddCreditor">
                                    <form method="POST" id="formLetterCreditors">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">Name</label>
                                            </div>
                                            <input required type="text" name="" id="creditor_name" class="form-control">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">Address</label>
                                            </div>
                                            <input required type="text" name="" id="creditor_address" class="form-control">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">City, State Zip</label>
                                            </div>
                                            <input required type="text" name="" id="creditor_city_state_zip" class="form-control">
                                            <div class="input-group-append">
                                                <button  type="submit" class="btn btn-success waves-effect waves-light">Save Creditor</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="containerCreditorLetterTemplateTitles" class="hide row m-t-10">
                                <div class="col-12 col-md-4 offset-md-4">
                                    <form action="" method="POST" id="formSaveCreditorLetterTemplateTitles">
                                        <div class="input-group">
                                            <input required type="text" class="form-control" placeholder="New Template Title" name="template_title">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-success" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table stylish-table " id="tableCreditorLetterTemplateTitles">
                                        <thead>
                                            <tr>
                                                <th>Template Title</th>
                                                <th class="text-center">Tools</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="text-right">
                                <a href="#" class=" m-t-10 btnShowTemplateHelpers" id="">Show/Hide Helpers</a>
                            </div>

                            <div class="hide containerTemplateHelpers" id="">
                                <div class="card">
                                     <div class="card-body">
                                        <h4>Helpers</h4>
                                         <div class="row m-t-20">
                                            <div class="col-6 b-r">
                                                <h5>Profile</h5>
                                                name = {NAME} <br>
                                                address = {ADDRESS} <br>
                                                city = {CITY}<br>
                                                state = {STATE}<br>
                                                zip = {ZIP}<br>
                                                current date = {CURDATE} <br>
                                                current date (mm/dd/yyyy) = {CURRENT_DATE} <br>
                                                social = {SSN} <br>
                                                date of birth = {DOB} <br>
                                                <br>
                                                Creditor Details = {CREDITOR DETAILS} <br>
                                                Account Number = {ACCOUNT#} <br>
                                                <br>
                                                full social = {FULL_SOCIAL} <br>
                                                phone = {PHONE} <br>

                                            </div>
                                            <div class="col-6">
                                                <h5>Account Round List</h5>
                                                round 1 Equifax = {EQUIFAX ACCOUNT LIST RND1} <br>
                                                round 1 Experian = {EXPERIAN ACCOUNT LIST RND1} <br>
                                                round 1 TransUnion = {TRANSUNION ACCOUNT LIST RND1} <br>
                                                round 2 Equifax = {EQUIFAX ACCOUNT LIST RND2} <br>
                                                round 2 Experian = {EXPERIAN ACCOUNT LIST RND2} <br>
                                                round 2 TransUnion = {TRANSUNION ACCOUNT LIST RND2} <br>
                                                etc.
                                            </div>
                                        </div>
                                     </div>
                                </div>


                            </div>

                            <button class="btnFetchDataTemplateCreditor btn btn-success m-t-20 hide">Fetch Data <i class="fas fa-database"></i></button>


                        <div >
                            <form method="post">
                                <textarea id="creditorMCE" class="mymce" name="area"></textarea>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-creditors-account" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
               Creditors Accounts
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form method="post" id="formCreditorsAccountType">
                    <div class="row">
                        <div class="col-12">
                            <h4>New Creditors Account Type</h4>
                            <div class="input-group m-b-10">
                                <input type="text" class="form-control" name="" id="creditors_account_type">
                                <div class="input-group-append">
                                    <button id="btnSaveCreditorsAccountType" style="width: 100%" class="btn waves-effect waves-light btn-success" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <table id="tableCreditorsAccountType" class="table stylish-table">
                    <thead>
                        <th>Account Type</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-creditors-rec" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
               Creditors Recommendations
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <form method="post" id="formCreditorsRec">
                    <div class="row">
                        <div class="col-12">
                            <h4>New Creditors Recommendation</h4>
                            <div class="input-group m-b-10">
                                <input type="text" class="form-control" name="" id="creditors_rec">
                                <div class="input-group-prepend">
                                    <button id="btnSaveCreditorsRec" style="width: 100%" class="btn waves-effect waves-light btn-success" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <table id="tableCreditorsRec" class="table stylish-table">
                    <thead>
                        <!-- <th style="width: 70% !important">Dispute Verbiage</th>   -->
                        <th > Recommendation</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-account-sourcecode" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
               Credit Report Source Code
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <textarea class="form-control" name="" id="inputAccountSourceCode" rows="30" style="font-size: 60%"></textarea>
            </div>
            <div class="modal-footer">
                <button id="btnSaveAccountBySourceCode" type="button" class="btn btn-success waves-effect">Submit</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="send_invoice_modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
               Send Invoice
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="printableDiv" style="width: 600px;margin: auto;font-size: 14px;padding: 20px">
                    <div class="text-center">
                        <img class="img-responsive" style="width: auto;max-width: 100%" src="<?php echo base_url('assets/images/cl_logo.jpg') ?>"/>
                    </div>
                    <div class="invoiceContent">
                        <div class="row">
                            <div class="col-md-6">
                                From: <br><br>
                                <address>
                                    <strong>Credit Lynx</strong>
                                    <br>228 Mill Street
                                    <br>Suite 200
                                    <br>Milford, Ohio 45150
                                    <br>(877) 820-2425
                                    <br>(513) 878-0181
                                </address>
                            </div>
                            <div class="col-md-6 text-right">
                                Invoice No.<br>
                                <b>1234444</b><br>
                                To:<br>
                                <address>
                                    <strong class="billing_card_holder"></strong>
                                    <br><span class="billing_address"></span>
                                    <br><span class="billing_city"></span>, <span class="billing_state"></span> <span class="billing_zip"></span>
                                    <br><span class="profile_cell_phone"></span>
                                </address>

                                <br>
                                <br><strong>Invoice Date: </strong><span><?php echo date('F j, Y'); ?></span><br><strong>Due Date: </strong><span><?php echo date('F j, Y', strtotime(date('Y-m-d') . ' +4 days')); ?></span>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Item List</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add-edit-disputes-account" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
               Dispute Accounts
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <!-- <h4 class="modal-title">Modal Content is Responsive</h4> -->
            </div>
            <div class="modal-body">
                <table id="tableDisputeAccounts" class="table stylish-table">
                    <thead>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Credit Limit</th>
                        <th>Balance</th>
                        <th>Past Due</th>
                        <th>Date Opened</th>
                        <th width="1">Tools</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="modal_warning_alert" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="modal-body">
                <div class="text-center">
                    <div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"><span class="swal2-icon-text">!</span></div>
                </div>
                <h3 class="text-center"><b>Account Suspended</b></h3>
                <br>
                <p>Hmmm, looks like this account has an overdue balance. No further work can be completed until this account is made current.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect btnSendInvoice">Send Invoice</button>
                <button type="button" class="btn btn-info waves-effect btn_send_new_payment_form">Send New Payment Form</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_send_bulk_sms" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="modal-header" style="display: block;">  
                <div class="text-center">
                    <!-- <div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"><span class="swal2-icon-text">!</span></div> -->
                    <i class="fas fa-envelope fa-xlg text-themecolor" style="font-size: 55px;"></i>
                </div>
                <h3 class="text-center"><b>Send Bulk Sms</b></h3> 
            </div>
            <div class="modal-body">
                <label>To (<span class="to_count">0</span>): </label>
                <select multiple="" class="form-control send_bulk_sms_to" style="height: 400px"> 
                </select>
                <br>
                <label style="margin-top: 20px;">Message Content</label>
                <textarea rows="5" class="form-control send_bulk_sms_message_content"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success waves-effect btn_send_bulk_sms"><i class="fas fa-paper-plane"></i> Send</button> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="modal_alert_schedules" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="modal-header" style="display: block;">  
                <div class="text-center">
                    <!-- <div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"><span class="swal2-icon-text">!</span></div> -->
                    <i class="mdi mdi-calendar-clock fa-xlg text-themecolor" style="font-size: 55px;"></i>
                </div>
                <h3 class="text-center"><b>Alert Schedules</b></h3> 
            </div>
            <div class="modal-body">
                 <h3>Round Date: <span id="as_round_date"></span></h3>  
                 <h4><h3>Schedules:</h3><span id="as_schedules"></span></h4>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>