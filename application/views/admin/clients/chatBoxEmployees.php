<div class="chat-main-box" style="height: 80vh">
    <!-- .chat-left-panel -->
    <div class="chat-left-aside">
        <div class="open-panel"><i class="ti-angle-right"></i></div>
        <div class="chat-left-inner">
            <div class="form-material">
                <!-- <input class="form-control" style="border: none !important" type="text" placeholder="Search Contact"> -->
                <select class="form-control" id="selectContactGroup" style="border: none !important;border-radius: 0px;height: 40px">
                    <option value="All">All</option>
                    <option value="Billing Team">Billing Team</option>
                    <option value="Support Team">Support Team</option>
                    <option value="Sales Team">Sales Team</option>
                    <option value="Dispute Team">Dispute Team</option>
                    <option value="Administrator">Administrator</option>
                </select>
            </div>
            <ul class="chatonline style-none " style="overflow-y: auto;height: calc(80vh - 48px)">
                
                <li class="p-20"></li>
            </ul>
        </div>
    </div>
    <!-- .chat-left-panel -->
    <!-- .chat-right-panel -->
    <div class="chat-right-aside">
        <!-- <div class="chat-main-header">
            <div class="p-20 b-b">
                <h3 class="box-title">Chat Message</h3>
            </div>
        </div> -->
        <div class="chat-rbox">
            <ul class="chat-list p-20" style="padding-top: 0px;overflow-y: auto;height: calc(80vh - 57px)"> 
            </ul>
        </div>
        <div class="card-body b-t p-0">
            <div class="row">
                <div class="col-12">
                    <div class="input-group"> 
                        <div class="emoji-picker-container  " style="width: 1%;flex: auto">
                            <input data-emojiable="true" id="inputChatBoxMessage" placeholder="Type a message..." class="form-control b-0" style="border-radius: 0px;padding-left: 15px !important"> 
                        </div>
                        <div class="input-group-append">
                            <button id="btnSaveChatEmployees" style="border-radius: 0px" type="button" class="btn btn-success waves-effect waves-light btn-lg"><i class="fas fa-paper-plane"></i> </button>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
    </div>
    <!-- .chat-right-panel -->
</div> 
<script>

    window.emojiPicker = new EmojiPicker({
      emojiable_selector: '[data-emojiable=true]',
      assetsPath: '<?php echo base_url() ?>/assets/emoji_plugin/img/',
      popupButtonClasses: 'fa fa-smile-o'
    });
    // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
    // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
    // It can be called as many times as necessary; previously converted input fields will not be converted again
    window.emojiPicker.discover();

    $('#inputChatBoxMessage').on('keypress',function(e){
        if (e.which == 13) {
            $('#btnSaveChatEmployees').trigger('click');
        }
    });

    $('#btnSaveChatEmployees').on('click',function(){
        $('#btnSaveChatEmployees').prop('disabled',true);
        var li = $('.chatonline li a.active');
        li = li.closest('li'); 
        var value = li.attr('value');
        value = value.split('_');
        var to_type = value[0];
        var to_id = value[1];
        var to_name = li.find('.user_name').html();
        var to_photo = li.find('img').attr('src');
        var message = $('#inputChatBoxMessage').val();
        if (message != '') {
            // alert(message);
            saveChatEmployees(to_type,to_name,to_photo,to_id,message);
        } else {
            $('#inputChatBoxMessage').focus();
        } 
    });

    $(".open-panel").on("click", function () {
        $(".chat-left-aside").toggleClass("open-pnl");
        $(".open-panel i").toggleClass("ti-angle-left");
    });


    $('#selectContactGroup').on('change',function(){
        var contact_group = $(this).val();
        getChatBoxEmployees(contact_group);
    });

    var get_chat_interval;

    $('.chatonline').on('click','li',function(){
        $('.chatonline li a').removeClass('active');
        $(this).find('a').addClass('active');

        var value = $(this).attr('value');
        value = value.split('_');
        var to_type = value[0];
        var to_id = value[1];
        var to_name = $(this).find('.user_name').html();
        var to_photo = $(this).find('img').attr('src');
        
        if ($(this).find('.label-danger').length != 0) {
            updateUnread(to_type,to_name,to_photo,to_id);
            $(this).find('.label-danger').remove();
        }
        clearInterval(get_chat_interval);
        getChatEmployees('get',to_type,to_name,to_photo,to_id);
        get_chat_interval = setInterval(function(){
            getChatEmployees('update',to_type,to_name,to_photo,to_id);
        },60000);

    });

    function saveChatEmployees(to_type,to_name,to_photo,to_id,message) {
        $.post('<?php echo base_url('admin/login/saveChatEmployees') ?>',
            {to_type,to_name,to_photo,to_id,message},function(data){
                // console.log(data);
                var li = $('.chatonline li a.active');
                li.trigger('click');
                $('#inputChatBoxMessage').val('');
                $('.emoji-wysiwyg-editor').html('');
                $('#btnSaveChatEmployees').prop('disabled',false);
            });
    }

    getChatBoxEmployees('All');
    function getChatBoxEmployees(contact_group) {
        $.post('<?php echo base_url('admin/login/getChatBoxEmployees') ?>',
            {contact_group},function(data){
                data = JSON.parse(data); 
                var chatonline = $('.chatonline');
                chatonline.empty();
                var user_type = $('#login_type').html();
                var user_name = $('#user_name').html();
                $.each(data,function(key,value){
                    var text_design = 'text-success';
                    if (value.state == 'Offline') {
                        text_design = 'text-muted';
                    }
                    // alert(user_type + ' ' +value.user_type);
                    // alert(user_name + ' ' +value.user_name);
                    if (user_type != value.user_type || user_name != value.user_name) {
                        var unread = '';
                        if (value.unread != 0) {
                            unread = '<span class="label label-rounded label-danger" style="color:white;">'+value.unread+'</span>';
                        }
                        var newLi = '<li value="'+value.user_type+'_'+value.user_id+'">\
                                        <a style="padding: 10px" href="javascript:void(0)"><img src="<?php echo base_url() ?>/'+value.user_photo+'" alt="user-img" class="img-circle"> <span ><span class="user_name">'+value.user_name+'</span> '+unread+'<small class="'+text_design+'"> '+value.state+'</small></span></a>\
                                    </li>';
                        chatonline.append(newLi);
                    }
                        
                });
            });
    }

    function getChatEmployees(method,to_type,to_name,to_photo,to_id) {
        $.post('<?php echo base_url('admin/login/getChatEmployees') ?>',
            {method,to_type,to_name,to_photo,to_id},function(data){
                data = JSON.parse(data);
                $('.chat-list').empty();
                var login_type = $('#login_type').html();
                var user_name = $('#user_name').html();
                // alert(login_type);
                $.each(data,function(key,value){
                    var from_type = value.from_type;
                    var from_name = value.from_name;
                    var li_class = '';
                    var bg_class = 'bg-light-info';
                    var chat_name = value.from_name;
                    var chat_photo = value.from_photo; 
                    // alert(login_type +'=='+ from_type +'&&'+ user_name +'=='+ from_name);
                    if (login_type == from_type && user_name == from_name) { 
                        li_class = 'reverse';
                        bg_class = 'bg-light-inverse';
                        chat_name = value.from_name;
                        chat_photo = value.from_photo;

                        var newLi = '<li class="'+li_class+'">\
                                        <div class="chat-time">'+moment(value.message_date).format('lll')+'</div>\
                                        <div class="chat-content">\
                                            <h5>'+chat_name+'</h5>\
                                            <div class="box '+bg_class+'">'+value.message+'</div>\
                                        </div>\
                                        <div class="chat-img"><img src="'+chat_photo+'" alt="user" /></div>\
                                    </li>';
                        $('.chat-list').append(newLi);
                    } else {
                        // console.log(value.to_name);
                        var newLi = '<li class="'+li_class+'">\
                                        <div class="chat-img"><img src="'+chat_photo+'" alt="user" /></div>\
                                        <div class="chat-content">\
                                            <h5>'+chat_name+'</h5>\
                                            <div class="box '+bg_class+'">'+value.message+'</div>\
                                        </div>\
                                        <div class="chat-time">'+moment(value.message_date).format('lll')+'</div>\
                                    </li>';
                        $('.chat-list').append(newLi);
                    }
                    
                });
                var scroll=$('.chat-list');
                scroll.animate({scrollTop: scroll.prop("scrollHeight")});
                updateUnread(to_type,to_name,to_photo,to_id);
            });
    }
    getChatEmployeesUnread(true);
    setInterval(function(){
        getChatEmployeesUnread(true);
    },60000);
    function getChatEmployeesUnread(showToast) {
        $.post('<?php echo base_url('admin/login/getChatEmployeesUnread') ?>',
            function(data){
                data = JSON.parse(data);
                // console.log(data);

                // $('.chat-list').empty();
                // var login_type = $('#login_type').html();
                // var user_name = $('#user_name').html();
                // // alert(login_type);
                if (data.length != 0) {
                    $('#btnShowChatBoxEmployees').append('<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>');
                    getChatBoxEmployees('All');
                    if (showToast) {
                        $.each(data,function(key,value){
                            // console.log('<img width="20px" src="'+value.from_photo+'"/>');
                            $.toast({
                                heading: 'Unread Message',
                                text: value.from_name+': '+value.message+'<br>'+moment(value.message_date).format('lll'),
                                position: 'bottom-right',
                                loaderBg:'#26c6da', 
                                hideAfter: 10000, 
                                stack: 6
                              });
                        });
                    }
                        
                } else {
                    $('#btnShowChatBoxEmployees.notify').remove();
                }

                    
                // var scroll=$('.chat-list');
                // scroll.animate({scrollTop: scroll.prop("scrollHeight")});
            });
    }

    function updateUnread(to_type,to_name,to_photo,to_id) {
        $.post('<?php echo base_url('admin/login/updateUnread') ?>',
            {to_type,to_name,to_photo,to_id},function(data){
                // console.log(data);
                getChatEmployeesUnread(true);
            });
    }
</script>