<link href="<?php echo base_url()?>assets/plugins/wizard/steps.css" rel="stylesheet" type="text/css">
<!-- <link href="<?php echo base_url()?>material/css/style.css" rel="stylesheet"> -->
<!-- You can change the theme colors from here -->
<!-- <link href="<?php echo base_url()?>material/css/colors/blue.css" id="theme" rel="stylesheet"> -->
<style type="text/css">
    .textCreditScore{
        background: transparent;
        border: none!important;
        text-align: center;
        font-size: 34px;
        padding: 0px!important;
        margin: 0px!important;
        width: 100%!important;
    }
    .wizard-content .wizard>.steps>ul>li.done .step{
        background-color: #6ec4d7!important;
        border-color: #6ec4d7!important;
        color: #fff!important;
    }

</style>

<div style="padding-top: 0" class="tab-pane b-t" id="tab_gameplan" role="tabpanel">
    <div class="card-body"> 
        <form method="post" id="GamePlanForm">
            <div class="row">
                <div class="text-center col-6">
                    <h3 class="font-light m-r-20" style="text-align: left;">Original Credit Scores</h3>
                </div>
                 
            </div>
            <div class="row m-t-10"> 
                <!-- Column -->
                <div class="col-md-6 col-lg-4 col-xlg-4">
                    <div class="card card-danger" style="background: #dd5970!important;">
                        <div class="box text-center">
                            <!-- <h1 class="font-light text-white" id="label_pending_tickets"></h1> -->
                            <input type="text" name="" class="font-light text-white textCreditScore" id="equifax_points">
                            <h6 style="font-size: 100%" class="text-white" style="margin-top: 10px;">Equifax</h6>
                        </div>
                    </div>
                </div> 
                <div class="col-md-6 col-lg-4 col-xlg-4">
                    <div class="card card-primary" style="background: #6e70e6!important;">
                        <div class="box text-center">
                            <input type="text" name="" class="font-light text-white textCreditScore" id="experian_points">
                            <h6 style="font-size: 100%" class="text-white" style="margin-top: 10px;">Experian</h6>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-md-6 col-lg-4 col-xlg-4">
                    <div class="card card-success" style="background: #6ec4d7!important;">
                        <div class="box text-center">
                            <input type="text" name="" class="font-light text-white textCreditScore" id="transunion_points">
                            <h6 style="font-size: 100%" class="text-white" style="margin-top: 10px;">TransUnion</h6>
                        </div>
                    </div>
                </div>  
                <div class=" col-md-6 col-12 text-right ">
                    <div class="form-group ">

                        <label class="p-l-20 p-r-20 control-label" >Estimated Time of Goal</label>
                        <input id="eta_date"  style="text-align: center; width: 103px !important;min-height: 28px !important;font-size: 70%" type="text" class="form-control date-inputmask" > 
                    </div>
                </div>
                <div class="col-md-6 col-12 ">
                    <div class="form-group ">
                        <label class="p-l-20 p-r-20 control-label" >Ideal Credit Score</label>
                        <input id="idealCreditScore"  style="text-align: center; width: 103px !important;min-height: 28px !important;font-size: 70%" type="text" class="form-control">  
                    </div>
                </div>
            </div>
            <!-- <div class="row m-t-10">
                <div class="col-md-6 col-lg-4 col-xlg-4">
                    <div class="form-group row">
                        <label for="exampleInputuname3" class="col-sm-6 control-label">Ideal Credit Score</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control" id="idealCreditScore">
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </form>
        <?php if ($userdata['login_folder'] == 'admins'): ?>
        <form method="post" id="formGamePlanCheckbox">
            <div class="row">
                <div class="col-md-4">
                    <div class="demo-checkbox">
                        <input type="checkbox" id="Credit_Card_Needed" class="filled-in" value="Credit Card Needed" />
                        <label for="Credit_Card_Needed">Credit Card Needed</label>

                        <input type="checkbox" id="Authorized_User" class="filled-in" value="Authorized User" />
                        <label for="Authorized_User">Authorized User</label>

                        <input type="checkbox" id="Recent_Repossession" class="filled-in" value="Recent Repossession" />
                        <label for="Recent_Repossession">Recent Repossession</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="demo-checkbox">
                        <input type="checkbox" id="High_Credit_Utilization" class="filled-in" value="High Credit Utilization" />
                        <label for="High_Credit_Utilization">High Credit Utilization</label>

                        <input type="checkbox" id="Recent_Late" class="filled-in" value="Recent Late Payment" />
                        <label for="Recent_Late">Recent Late Payment</label>

                        <input type="checkbox" id="Recent_Judgment" class="filled-in" value="Recent Judgment/Tax Lien" />
                        <label for="Recent_Judgment">Recent Judgment/Tax Lien</label>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="demo-checkbox">
                        <input type="checkbox" id="New_Inquiries" class="filled-in" value="New Inquiries" />
                        <label for="New_Inquiries">New Inquiries</label>
                        
                        <input type="checkbox" id="Recent_Charge_Off" class="filled-in" value="Recent Charge Off" />
                        <label for="Recent_Charge_Off">Recent Charge Off</label>

                        <input type="checkbox" id="Recent_Moved" class="filled-in" value="Recent Moved" />
                        <label for="Recent_Moved">Recent Moved</label>
                        
                    </div>
                </div>
            </div>   
        </form>    
        <?php endif ?>
            
    </div>
    <div class="card-body"> 
        <form method="post" id="BrokerGameplanForm">
            <div class="form-group"> 
                <label>Broker Game Plan</label>
                <textarea class="form-control" id="broker_gameplan" rows="5" placeholder="Don't have a broker or lender to help you during or after this process? Let us know if you need a list of recommended professionals in your area!"></textarea>
            </div>    
        </form>
    </div>   
    <div class="card-body wizard-content">
        <!-- <h4 class="card-title">Step wizard</h4>
        <h6 class="card-subtitle">You can find the
            <a href="http://www.jquery-steps.com" target="_blank">offical website</a>
        </h6> -->
        <form id="formTimelineContent" method="post" class="tab-wizard wizard-circle">
            <!-- Step 1 -->
            <h6>Enrolled</h6>
            <section id="page1" class="tabs-section1 tabs-section hide ">
                <div class="row">
                    <textarea class="form-control" id="Enrolled" rows="6"></textarea>
                </div>
            </section>
            <!-- Step 2 -->
            <h6>Dispute</h6>
            <section id="page2" class="tabs-section hide ">
                <div class="row">
                    <textarea class="form-control" id="Dispute" rows="6"></textarea>
                </div>
            </section>
            <!-- Step 3 -->
            <h6>Result</h6>
            <section id="page3" class="tabs-section hide ">
                <div class="row">
                    <textarea class="form-control" id="Result" rows="6"></textarea>
                </div>
            </section>
            <!-- Step 4 -->
            <h6>Repeat</h6>
            <section id="page4" class="tabs-section hide ">
                <div class="row"> 
                    <textarea class="form-control" id="Repeat" rows="6"></textarea>
                </div>
            </section>
            <!-- Step 5 -->
            <h6>Complete</h6>
            <section id="page5" class="tabs-section hide ">
                <div class="row"> 
                    <textarea class="form-control" id="Complete" rows="6"></textarea>
                </div>
            </section>
        </form>
    </div>	
    <div class="card-body"> 
        <form method="post" id="CreditLynxGameplanForm">        
            <div class="form-group"> 
                <label>Credit Lynx Game Plan</label>
                <textarea class="form-control" id="creditlynx_gameplan" rows="5" ></textarea>
            </div>    
        </form>
    </div>
</div>
<!-- <div id="game-plan-new-account" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            
        </div>
    </div>
</div> -->

<!-- <script src="<?php echo base_url()?>material/js/custom.min.js"></script> -->
<!-- <script src="<?php echo base_url()?>assets/plugins/moment/moment.js"></script> -->
<script src="<?php echo base_url()?>assets/plugins/wizard/jquery.steps.min.js"></script> 
<script>
    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step" qwerty="#index#"></span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function (event, currentIndex) { 

        }
    });

    $('.tab-wizard').find('[href="#previous"]').hide();
    $('.tab-wizard').find('[href="#next"]').hide();
    $('.tab-wizard').find('[href="#finish"]').hide();
    $('.tab-wizard').find('[role="tab"]').addClass('ms-hover');
    $('.tab-wizard').find('[role="tab"]').removeClass('done');
    $('.tab-wizard').find('[role="tab"]').removeClass('disabled');
    $('.tab-wizard').find('.first').removeClass('current');
    $('.tab-wizard').find('[aria-disabled="true"]').attr('aria-disabled',"false");

    

    $('#steps-uid-0-t-0').on('click', function(e){
        $('.tab-wizard .tabs-section').removeClass('hide');
    });
    $('#formTimelineContent-t-0').on('click', function(e){
        $('.tab-wizard .tabs-section').removeClass('hide');
    });
    $('.tab-wizard').on('click','.ms-hover', function(e){
        $('.tab-wizard .tabs-section').removeClass('hide');
    });
    $('#btnSaveGameplaneAccountType').on('click', function(){

    });



</script>