<div style="padding-top: 0;min-height: 100px" class="tab-pane b-t " id="tab_call_logs" role="tabpanel">
	<div class="card-body">
		<!-- <div class="row" class="m-b-20">
			<div class="col-12">
				<form method="POST" id="formSendClientEmail">
					<input id="inputSubjectClientEmail" required="" type="text" name="" class="form-control" placeholder="Subject..." style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px; ">
					<textarea id="inputMessageClientEmail" required="" style="border-top-left-radius: 0px;border-top-right-radius: 0px; " class="form-control" rows="3" placeholder="Message..."></textarea>
					<button type="submit" class="btn btn-success waves-effect waves-light pull-right">Send <i class="fas fa-paper-plane"></i></button>
				</form>
					
			</div>
				
		</div> -->
		Call Logs <a class="btnRefreshCallLogsTab" href="#"><i class="fas fa-refresh"></i></a>
		<div id="containerClientCallLogs" class="">
			<table class="table stylish-table">
				<thead>
					<tr>
						<th>Type</th>
						<th>Date</th>
						<th>Action</th>
						<th>Result</th>
						<th>Length</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>