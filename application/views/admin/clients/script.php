<script src="<?php echo base_url('assets/plugins/echarts/echarts-all.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/html5-editor/bootstrap-wysihtml5.css') ?>" />
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-editableSelect.css') ?>">
<script src="<?php echo base_url('assets/js/jquery-editableSelect.js') ?>"></script>

<script src="https://momentjs.com/downloads/moment-timezone-with-data-10-year-range.min.js"></script>

<script>




 


// TABLES START

    var tableClientDocuments = $('#tableClientDocuments').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20,
        'bFilter': false
    });

    var tableAlertTemplates = $('#tableAlertTemplates').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20
    });
    var tableNoteTemplates = $('#tableNoteTemplates').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20
    });
    var tableCreditReportTypes = $('#tableCreditReportTypes').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20
    });
    var tableCallTemplates = $('#tableCallTemplates').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20
    });
    var tableTaskTemplates = $('#tableTaskTemplates').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20
    });
    var tableAccountTypes = $('#tableAccountTypes').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
    var tableCreditorsAccountType = $('#tableCreditorsAccountType').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
    var tableCreditorsRec = $('#tableCreditorsRec').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
    var tableAccountStatuses = $('#tableAccountStatuses').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
    var tableAccountPaymentStatuses = $('#tableAccountPaymentStatuses').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
    var tableAccountComments = $('#tableAccountComments').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });

    var tbl_dva_at = $('#tbl_dva_at').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false,
        'paging': false
    });

    var tableByAccount = $('.tableByAccount').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });



    var tableByBureauEquifax = $('.tableByBureauEquifax').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });



    var tableByBureauExperian = $('.tableByBureauExperian').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });



    var tableByBureauTransUnion = $('.tableByBureauTransUnion').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100,
        'bFilter': false
    });
 
    var tbl_authorize_transactions = $('#tbl_authorize_transactions').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20,
        'bFilter': false
    });

    var tbl_authorize_subscriptions = $('#tbl_authorize_subscriptions').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 20,
        'bFilter': false
    });


    $('.tableByAccount').removeClass('dataTable');
    $('.tableByBureauEquifax').removeClass('dataTable');
    $('.tableByBureauExperian').removeClass('dataTable');
    $('.tableByBureauTransUnion').removeClass('dataTable');


    $('#pageTable').removeClass('dataTable');
    $('#tableAlertTemplates').removeClass('dataTable');
    // $('#tableClientDocuments').removeClass('dataTable');

    $('#tableNoteTemplates').removeClass('dataTable');
    $('#tableCreditReportTypes').removeClass('dataTable');
    $('#tableTaskTemplates').removeClass('dataTable');
    $('#tableAccountTypes').removeClass('dataTable');
    $('#tableCreditorsAccountType').removeClass('dataTable');
    $('#tableCreditorsRec').removeClass('dataTable');
    $('#tableAccountStatuses').removeClass('dataTable');
    $('#tableAccountPaymentStatuses').removeClass('dataTable');
    $('#tableAccountComments').removeClass('dataTable');
    $('#tbl_dva_at').removeClass('dataTable');

// TABLES END

// VARIABLES START


    var newUser = false;
    var client_id = 0;
    var client_joint_id = 0;
    var client_type = '';
    var alert_template_id = '';
    var at_id = '';
    var as_id = '';
    var dv_id = '';
    var account_number = '';
    var support_id = 0;
    var credit_limit = '';
    var date_added = '';

    getAccountStatuses();
    getAccountPaymentStatuses();
    getAccountComments();
    
    getTableCreditorsAccountType();
    getTableCreditorsRec();
    getLetterCreditors();
// VARIABLES END



// EVENTS START

    
    $('#tab_evaluation .demo-checkbox').on('change','[type="checkbox"]', function(event) {
       event.preventDefault(); 
       var checkboxes = $(this).closest('.row').find('[type="checkbox"]');
       $.each(checkboxes, function(index, checkbox) {
            $(checkbox).prop('checked',false);
       });
       $(this).prop('checked',true);
    });


    function save_client_evaluation(id,type,field,value) { 
        var where = {id:id,type:type};
        var data =  {
                        table: 'client_evaluations',
                        pk: JSON.stringify(where),
                        action: 'save',
                        fields: {
                                    id:id,
                                    type:type,
                                    [field]: value
                                },
                        return: 'eval_id'
                    };
        
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            set_eval_rec();
        }); 
            
    }

    function get_client_evaluation(id,type) {
        var data =  {
                        table: 'client_evaluations',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    id:id,
                                    type:type
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
                $('.parent_div[question="1"]').find('.sub_div').addClass('hide');
                $('.parent_div[question="2"]').addClass('hide');
                $('.parent_div[question="3"]').addClass('hide');
                $('.parent_div[question="3"]').find('.sub_div_1').addClass('hide');
                $('.parent_div[question="3"]').find('.sub_div_2').addClass('hide');
                $('.parent_div[question="4"]').addClass('hide');
                $('#evaluation_rec').addClass('hide');

                $('#question_1_yes').prop('checked', false);
                $('#question_1_no').prop('checked', false);
                $('#question_1a_yes').prop('checked', false);
                $('#question_1a_no').prop('checked', false);
                $('#question_2_yes').prop('checked', false);
                $('#question_2_no').prop('checked', false);
                $('#question_3_yes').prop('checked', false);
                $('#question_3_no').prop('checked', false);
                $('#question_3a_1to3').prop('checked', false);
                $('#question_3a_4to9').prop('checked', false);
                $('#question_3a_10plus').prop('checked', false);
                $('#question_3b_yes').prop('checked', false);
                $('#question_3b_no').prop('checked', false);
                $('#question_4_yes').prop('checked', false);
                $('#question_4_no').prop('checked', false);
                $('#question_5_yes').prop('checked', false);
                $('#question_5_no').prop('checked', false);
                $('#question_6_yes').prop('checked', false);
                $('#question_6_no').prop('checked', false);
            $.each(data, function(index, value) {

                $('#question_1_yes').prop('checked',value.question_1_yes == '1' ? true : false);  
                $('#question_1_no').prop('checked',value.question_1_no == '1' ? true : false);  

                $('#question_1a_yes').prop('checked',value.question_1a_yes == '1' ? true : false);
                $('#question_1a_no').prop('checked',value.question_1a_no == '1' ? true : false);
                if (value.question_1a_yes == '1' || value.question_1a_no == '1') {
                    $('.parent_div[question="1"]').find('.sub_div').removeClass('hide');
                }
                
                
                $('#question_2_yes').prop('checked',value.question_2_yes == '1' ? true : false);
                $('#question_2_no').prop('checked',value.question_2_no == '1' ? true : false);
                if (value.question_2_yes == '1' || value.question_2_no == '1') {
                    $('.parent_div[question="2"]').removeClass('hide');
                }

                $('#question_3_yes').prop('checked',value.question_3_yes == '1' ? true : false);
                $('#question_3_no').prop('checked',value.question_3_no == '1' ? true : false); 
                if (value.question_2_yes == '1' || value.question_2_no == '1') {
                    $('.parent_div[question="3"]').removeClass('hide');
                }

             

                $('#question_3a_1to3').prop('checked',value.question_3a_1to3 == '1' ? true : false);
                $('#question_3a_4to9').prop('checked',value.question_3a_4to9 == '1' ? true : false); 
                $('#question_3a_10plus').prop('checked',value.question_3a_10plus == '1' ? true : false);
                if (value.question_3a_1to3 == '1' || value.question_3a_4to9 == '1' || value.question_3a_10plus == '1') {
                    $('.parent_div[question="3"]').find('.sub_div_1').removeClass('hide');
                    $('.parent_div[question="4"]').removeClass('hide');
                }

               
                $('#question_3b_yes').prop('checked',value.question_3b_yes == '1' ? true : false);
                $('#question_3b_no').prop('checked',value.question_3b_no == '1' ? true : false);
                if (value.question_3b_yes == '1' || value.question_3b_no == '1') {
                    $('.parent_div[question="3"]').find('.sub_div_2').removeClass('hide');
                    $('.parent_div[question="4"]').removeClass('hide');
                }


                $('#question_4_yes').prop('checked',value.question_4_yes == '1' ? true : false);
                $('#question_4_no').prop('checked',value.question_4_no == '1' ? true : false);
                if (value.question_4_yes == '1' || value.question_4_no == '1') {
                    $('.parent_div[question="4"]').removeClass('hide');
                    $('.parent_div[question="5"]').removeClass('hide');
                }


                $('#question_5_yes').prop('checked',value.question_5_yes == '1' ? true : false);
                $('#question_5_no').prop('checked',value.question_5_no == '1' ? true : false);
                if (value.question_5_yes == '1' || value.question_5_no == '1') {
                    $('.parent_div[question="5"]').removeClass('hide');
                    $('.parent_div[question="6"]').removeClass('hide');
                }

                $('#question_6_yes').prop('checked',value.question_6_yes == '1' ? true : false);
                $('#question_6_no').prop('checked',value.question_6_no == '1' ? true : false);
                if (value.question_6_yes == '1' || value.question_6_no == '1') {
                    $('.parent_div[question="6"]').removeClass('hide');
                    $('#evaluation_rec').removeClass('hide');
                }






            });

            set_eval_rec();

            
        });     
    }

    $('#question_1_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $(this).closest('.parent_div').find('.sub_div').removeClass('hide');
            // $('.parent_div[question="2"]').addClass('hide');
            var checkboxes = $('.parent_div[question="2"]').find('[type="checkbox"]');
            $.each(checkboxes, function(index, checkbox) {
                $(checkbox).prop('checked',false);
            });
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1_no',0);
        }
    });

    $('#question_1_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $(this).closest('.parent_div').find('.sub_div').addClass('hide');
            var checkboxes = $(this).closest('.parent_div').find('.sub_div').find('[type="checkbox"]');
            $.each(checkboxes, function(index, checkbox) {
                $(checkbox).prop('checked',false);
                $(checkbox).trigger('change');
            });

            $('.parent_div[question="2"]').removeClass('hide');


            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1_no',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_no',0);
        }
    });

    $('#question_1a_yes').on('change', function(event) {
        event.preventDefault(); 
        if ($(this).is(':checked')) {
            $('.parent_div[question="2"]').removeClass('hide');

            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_no',0);
        }
    });

    $('#question_1a_no').on('change', function(event) {
        event.preventDefault(); 
        if ($(this).is(':checked')) {
            $('.parent_div[question="2"]').removeClass('hide');

            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_1a_no',1);
        }
    });

    $('#question_2_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="3"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_2_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_2_no',0);
        } 
    });
    $('#question_2_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="3"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_2_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_2_no',1);
        } 
    });

    $('#question_3_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) { 
            $(this).closest('.parent_div').find('.sub_div_1').removeClass('hide');
            // $('.parent_div[question="4"]').addClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3_no',0);
        }
    });


    $('#question_3_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $(this).closest('.parent_div').find('.sub_div_1').addClass('hide');

            $('#question_3a_1to3').prop('checked', false);
            $('#question_3a_4to9').prop('checked', false);
            $('#question_3a_10plus').prop('checked', false);
            $('#question_3b_yes').prop('checked', false);
            $('#question_3b_no').prop('checked', false);
            
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_1to3',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_4to9',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_10plus',0);

            $(this).closest('.parent_div').find('.sub_div_2').addClass('hide'); 
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_no',0);

            $('.parent_div[question="4"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3_no',1);
        }
    });

    $('#question_3a_1to3').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.sub_div_2').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_1to3',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_4to9',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_10plus',0);
        }
    });
    $('#question_3a_4to9').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.sub_div_2').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_1to3',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_4to9',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_10plus',0);
        }
    });
    $('#question_3a_10plus').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.sub_div_2').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_1to3',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_4to9',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3a_10plus',1);
        }
    });

    $('#question_3b_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="4"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_no',0);
        }
    });
    $('#question_3b_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="4"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_3b_no',1);
        }
    });

    $('#question_4_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) { 
            $('.parent_div[question="5"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_4_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_4_no',0);

            set_eval_rec();
        }
    });
    $('#question_4_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="5"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_4_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_4_no',1);

            set_eval_rec();


        }
    });
    $('#question_5_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="6"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_5_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_5_no',0);

            set_eval_rec();
        }
    });
    $('#question_5_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $('.parent_div[question="6"]').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_5_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_5_no',1);

            set_eval_rec(); 
        }
    });


    $('#question_6_yes').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) { 
            $('#evaluation_rec').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_6_yes',1);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_6_no',0);

            set_eval_rec();
        }
    });
    $('#question_6_no').on('change', function(event) {
        event.preventDefault();
        if ($(this).is(':checked')) { 
            $('#evaluation_rec').removeClass('hide');
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_6_yes',0);
            save_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type,'question_6_no',1);

            set_eval_rec(); 
        }
    });

    function set_eval_rec() {
        var question_1_yes = $('#question_1_yes').is(':checked');
        var question_1_no = $('#question_1_no').is(':checked');
        var question_1a_yes = $('#question_1a_yes').is(':checked');
        var question_1a_no = $('#question_1a_no').is(':checked');
        var question_2_yes = $('#question_2_yes').is(':checked');
        var question_2_no = $('#question_2_no').is(':checked');
        var question_3_yes = $('#question_3_yes').is(':checked');
        var question_3_no = $('#question_3_no').is(':checked');
        var question_3a_1to3 = $('#question_3a_1to3').is(':checked');
        var question_3a_4to9 = $('#question_3a_4to9').is(':checked');
        var question_3a_10plus = $('#question_3a_10plus').is(':checked');
        var question_3b_yes = $('#question_3b_yes').is(':checked');
        var question_3b_no = $('#question_3b_no').is(':checked');
        var question_4_yes = $('#question_4_yes').is(':checked');
        var question_4_no = $('#question_4_no').is(':checked');

        var evaluation_rec = '';
        var evaluation_rec_h3 = '';
        if (question_1_no && question_2_no && question_3_no && question_4_no) {
            evaluation_rec_h3 = 'Hmmm';
            evaluation_rec = 'This person may not be a candidate, but probably needs a secured credit card.  <a href="www.creditlynx.com/rebuilding-credit">www.creditlynx.com/rebuilding-credit</a>';
        } 
        if (question_1_no && question_2_yes && question_3_no && question_4_no) {
            evaluation_rec_h3 = 'Hmmm';
            evaluation_rec = 'This person may not be a candidate, but may benefit from a secured credit card.  Feel free to refer them to <a href="www.creditlynx.com/rebuilding-credit">www.creditlynx.com/rebuilding-credit</a>';
        } 


        if (question_1_yes && question_1a_yes && (question_2_yes || question_2_no) && question_3_no &&question_4_no) {
            evaluation_rec_h3 = 'Hmmm';
            evaluation_rec = 'This person may not be a candidate.  It looks like they would benefit by paying down their credit card balances.';
        } 

        if (question_1_no && $question_2_no && question_3_yes && question_3a_4to9 && question_3b_no && question_4_yes) {
            evaluation_rec_h3 = 'Great News!';
            evaluation_rec = 'We recommend the Premiere Plan for this customer.';   
        }




        if (question_3a_1to3) {
            evaluation_rec_h3 = 'Great News!';
            evaluation_rec = 'We recommend the Limited Plan for this customer.';
        } 
        if (question_3a_4to9) {
            evaluation_rec_h3 = 'Great News!';
            evaluation_rec = 'We recommend the Premiere Plan for this customer.';
        } 
        if (question_3a_10plus) {
            evaluation_rec_h3 = 'Great News!';
            evaluation_rec = 'We recommend the Premiere or Exclusive Plan for this customer.';
        }

        $('#evaluation_rec p').html(evaluation_rec);
        $('#evaluation_rec h3').html(evaluation_rec_h3);
    }


    $('.btnShowHideMakeNote').on('click',function(e){
        e.preventDefault();
        if ($('.containerMakeNote').hasClass('hide')) {
            $('.containerMakeNote').removeClass('hide');
        } else {
            $('.containerMakeNote').addClass('hide');
        }
    });
    $('.btnOpenNoteTemplates').on('click',function(e){
        e.preventDefault();
        if ($('.note_templates_container').hasClass('hide')) {
            $('.note_templates_container').removeClass('hide');
        } else {
            $('.note_templates_container').addClass('hide');
        }
    });
    $('.btnShowHideMakeCapp').on('click',function(e){
        e.preventDefault();
        if ($('.containerMakeCapp').hasClass('hide')) {
            $('.containerMakeCapp').removeClass('hide');
        } else {
            $('.containerMakeCapp').addClass('hide');
        }
    });
    $('.btnOpenCappTemplates').on('click',function(e){
        e.preventDefault();
        if ($('.call_templates_container').hasClass('hide')) {
            $('.call_templates_container').removeClass('hide');
        } else {
            $('.call_templates_container').addClass('hide');
        }
    });

    var call_template_type = '';
    $('.btn_show_inbound_call_modal').on('click', function(event) {
        event.preventDefault();
        $('#modal_add_call').modal('show');
        call_template_type = 'inbound';
        $('.call_type').html('Inbound');
        getCallTemplates(call_template_type);
    });
    $('.btn_show_outbound_call_modal').on('click', function(event) {
        event.preventDefault();
        $('#modal_add_call').modal('show');
        call_template_type = 'outbound';
        $('.call_type').html('Outbound');
        getCallTemplates(call_template_type);
    });


    $('#btnOpenCallTemplatesModal').on('click', function(event) {
        getCallTemplates(call_template_type);
    });

    $('.call_templates_container').on('click', '.call_template',function(e){
        e.preventDefault();
        var call_template = $(this).attr('call_template');
        // alert(call_template);
        $('#new_call').val(call_template);
    });


    $('#btnSaveNewCallTemplate').on('click',function(){
        var call_shortname = $('#new_call_shortname');
        var call_template = $('#new_call_template');
        if (call_shortname.val() != '') {
            if (call_template.val() != '') {
                saveNewCallTemplate(call_shortname,call_template,call_template_type);
            } else {
                call_template.focus();
            }
        } else {
            call_shortname.focus();
        } 
    });


    



    $('#tableCallTemplates').on('click','a',function(e){
        e.preventDefault();
        var tr = $(this).closest('tr');
        var call_template_id = tr.attr('id');
        var tdata = tr.find('td');

        $('#new_call_template_id').val(call_template_id);

        var call_shortname = $(tdata[0]).find('a').html();
        var call_template = $(tdata[1]).html();

        $('#new_call_shortname').val(call_shortname);
        $('#new_call_template').val(call_template);
    });

    $('#btn_auto_alert_schedule').on('click', function(event) {
        event.preventDefault();
        $('#modal_alert_schedules').modal('show');
        var type = client_type;
        if (type == 'client') {
            get_auto_alert_schedule_clients(client_id);
        } else {
            get_auto_alert_schedule_joints(client_joint_id);
        }
    });



    $('#tab_campaign').find('.timeline').on('change', '.st_note', function(event) {
        event.preventDefault(); 
        var st_id = $(this).closest('li').attr('st_id');
        var value = $(this).val();
        update_client_status_timelines(st_id,'note',value);
    });
    $('#tab_campaign').find('.timeline').on('change', '.st_subject', function(event) {
        event.preventDefault(); 
        var st_id = $(this).closest('li').attr('st_id');
        var value = $(this).val();
        update_client_status_timelines(st_id,'subject',value);
    });

    $('#tab_campaign').find('.timeline').on('click', '#btn_add_st',function(event) {
        event.preventDefault();
        var day = $('.timeline li').length;
        add_client_status_timelines(day);
    });


    $('#tab_campaign').find('.timeline').on('click', 'button', function(event) {
        event.preventDefault();
        var st_id = $(this).closest('li').attr('st_id');
        var value = $(this).attr('notif_type');
        var buttons = $(this).closest('li').find('button'); 
        if ($(this).hasClass('btn-success')) {  
            $(this).addClass('btn-default');
            $(this).removeClass('btn-success'); 
            $(this).find('.fa-check').remove();


            $(this).closest('.timeline-panel').find('.timeline-heading').addClass('hide');
            $(this).closest('.timeline-panel').find('.timeline-body').addClass('hide');
            update_client_status_timelines(st_id,'notif_type','');
        } else { 
            buttons.removeClass('btn-success');
            buttons.addClass('btn-default');
            buttons.find('.fa-check').remove();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            $(this).append('<i class="fas fa-check"></i>');
            $(this).closest('.timeline-panel').find('.timeline-heading').removeClass('hide');
            $(this).closest('.timeline-panel').find('.timeline-body').removeClass('hide');

            update_client_status_timelines(st_id,'notif_type',value);
        }
        
    });

    $('#formSaveAppointment').on('submit',function(e){
        e.preventDefault();
        var app_id = $('#calendar_app_id').val();
        var appointment = $('#calendar_appointment').val();
        var date_start = $('#calendar_date_start').val();
        var date_end = $('#calendar_date_end').val();
        var app_color = $('#calendar_app_color').val(); 
        var app_to = client_type == 'client' ? 'Client' : 'Joint';
        var app_to_id = client_type == 'client' ? client_id : client_joint_id;
        var app_from = '<?php echo $userdata['login_type'] ?>';
        var app_from_id = '<?php echo $userdata['user_id'] ?>';
        var app_phone = $("#calendar_app_phone").val();
        var app_location = $("#calendar_app_location").val();
        var app_email = $("#calendar_app_email").val();
        var app_note = $("#calendar_app_note").val();
        var notif_type = $(".calendar_notif_type");
        var notif_time = $(".calendar_notif_time");
        var notif_schedule = $(".calendar_notif_schedule"); 
        var date_start = moment($("#calendar_date_start").val()).format('YYYY-MM-DD');; 
        var time_start = $("#calendar_time_start").val(); 
        var date_end = moment($("#calendar_date_end").val()).format('YYYY-MM-DD');; 
        var time_end = $("#calendar_time_end").val(); 

        date_start = date_start + ' ' +time_start; 
        date_end = date_end + ' ' +time_end; 


        var notif_type_arr = [];
        $.each(notif_type,function(key,value){
            notif_type_arr.push($(value).val());
        });
        var notif_time_arr = [];
        $.each(notif_time,function(key,value){
            notif_time_arr.push($(value).val());
        });
        var notif_schedule_arr = [];
        $.each(notif_schedule,function(key,value){
            notif_schedule_arr.push($(value).val());
        });


        saveAppointment('create',app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type_arr,notif_time_arr,notif_schedule_arr);
        
    });

    function saveAppointment(type,app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type,notif_time,notif_schedule) {
        $.post('<?php echo base_url('admin/appointments/saveAppointment') ?>',
            {app_id,app_from,app_from_id,app_to,app_to_id,appointment,date_start,date_end,app_color,app_phone,app_location,app_email,app_note,notif_type,notif_time,notif_schedule},function(data){ 
                $('#formSaveAppointment')[0].reset();
                
                $('#modalAppointmentInformation').modal('hide');
                if (type == 'create') {
                    get_client_appointments();
                    swal('Success','Appointment saved!','success');
                }   

                $('#otherDetailsContainer').addClass('hide');
                $('#sectionAddNotification').empty();


            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }  
    function get_client_appointments() {
        var app_to = client_type == 'client' ? 'Client' : 'Joint';
        var app_to_id = client_type == 'client' ? client_id : client_joint_id;
        $.post('<?php echo base_url('admin/appointments/get_client_appointments') ?>', {app_to,app_to_id}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $('#containerClientAppointments tbody').empty();
            $.each(data, function(index, val) {
                var btn_delete_app = '<button class="btn btn-danger btn_delete_app"><i class="fas fa-trash"></i></button>';
                  
                var tr = '\
                    <tr app_id='+val.app_id+'>\
                        <td><span class="'+val.app_color+' label" style="white-space: nowrap">'+val.appointment+'</span></td>\
                        <td class="text-center">'+moment(val.date_start).format('MM/DD/YYYY - h:mm a') + '<br> to <br>'+moment(val.date_end).format('MM/DD/YYYY - h:mm a')+'</td>\
                        <td>'+val.app_phone+'</td>\
                        <td>'+val.app_email+'</td>\
                        <td>'+val.app_location+'</td>\
                        <td>'+val.app_note+'</td>\
                        <td>'+(val.notification != null ? val.notification : '')+'</td>\
                        <td>'+btn_delete_app+'</td>\
                    </tr>';
                 $('#containerClientAppointments tbody').append(tr);
            });
        });
    }


    $('#containerClientAppointments').on('click', '.btn_delete_app', function(event) {
        event.preventDefault();
        var app_id = $(this).closest('tr').attr('app_id');
        var tr = $(this).closest('tr');
        swal({
                title: "Delete Appointment Confirmation",
                text: 'Are you sure you want to delete this Appointment?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                closeOnConfirm: true ,
                preConfirm: function() { 
                    var data =  {
                                    table: 'appointments',
                                    pk: app_id,
                                    action: 'delete'
                                }; 
                    $.post('appointments/modelTable', data, function(data, textStatus, xhr) { 
                        tr.remove();
                    });
                }
            });
    });

    $('.btn_set_appointment').on('click', function(event) {
        event.preventDefault();
        $('#modalAppointmentInformation').modal('show');    

    });
    
    $("#bank_account_name" ).autocomplete({
        minLength: 0,
        source: 
        [
            {
                value: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted sixth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted sixth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Drafted seventh round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                label: 'Drafted seventh round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                desc: '',
            },{
                value: 'Awaiting Equifax and Experian reports.',
                label: 'Awaiting Equifax and Experian reports.',
                desc: '',
            },{
                value: 'Awaiting Equifax and TransUnion reports.',
                label: 'Awaiting Equifax and TransUnion reports.',
                desc: '',
            },{
                value: 'Awaiting Equifax reports.',
                label: 'Awaiting Equifax reports.',
                desc: '',
            },{
                value: 'Awaiting Experian and TransUnion reports.',
                label: 'Awaiting Experian and TransUnion reports.',
                desc: '',
            },{
                value: 'Awaiting Experian reports.',
                label: 'Awaiting Experian reports.',
                desc: '',
            },{
                value: 'Awaiting TransUnion reports.',
                label: 'Awaiting TransUnion reports.',
                desc: '',
            },{
                value: 'Pending all verifications. Letters are drafted and ready to go.',
                label: 'Pending all verifications. Letters are drafted and ready to go.',
                desc: ''
            },{
                value: 'Pending (1) Address Verification. Letters are drafted and ready to go.',
                label: 'Pending (1) Address Verification. Letters are drafted and ready to go.',
                desc: ''
            },{
                value: 'Pending (1) Address and (1) Social Security Verification. Letters are drafted and ready to go.',
                label: 'Pending (1) Address and (1) Social Security Verification. Letters are drafted and ready to go.',
                desc: ''
            },{
                value: 'Pending (2) Address Verifications. Letters are drafted and ready to go.',
                label: 'Pending (2) Address Verifications. Letters are drafted and ready to go.',
                desc: ''
            },{
                value: 'Pending (1) Social Security Verification. Letters are drafted and ready to go.',
                label: 'Pending (1) Social Security Verification. Letters are drafted and ready to go.',
                desc: ''
            },{
              value: 'Pending Social Security Verification and Dispute Input.',
              label: 'Pending Social Security Verification and Dispute Input.',
              desc: '', 
            },{
              value: 'Pending Dispute Input.',
              label: 'Pending Dispute Input.',
              desc: '', 
            },{
              value: 'Pending Result Review and Account Update.',
              label: 'Pending Result Review and Account Update.',
              desc: '', 
            },{
              value: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
              label: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
              desc: '', 
            },{
              value: 'NSF - Account Terminated due to Non-Payment of Previous Month\'s Service.',
              label: 'NSF - Account Terminated due to Non-Payment of Previous Month\'s Service.',
              desc: '', 
            },{
              value: 'Cancelled',
              label: 'Cancelled',
              desc: '', 
            }
        ],
        focus: function( event, ui ) {
            $( "#bank_account_name" ).val( ui.item.label );
            $( "#bank_account_name" ).trigger('keyup');
            return false;
        },
        select: function( event, ui ) {
            $( "#bank_account_name" ).val( ui.item.label );
            $( "#bank_account_name" ).trigger('keyup');

            return false;
        }
    });

    $('.btn-next-client').on('click', function(event) {
        event.preventDefault(); 
        if (client_type == 'joint') {
            var next_client = $('.goToJointProfile[value="'+client_joint_id+'"]').closest('tr').next(); 
            client_id = $(next_client).attr('id'); 
            client_joint_id = '';
            client_type = 'client';
        } else {  
            if ($('table tr[id="'+client_id+'"]').closest('tr').find('.goToJointProfile').length == 1) {
                var next_joint = $('table tr[id="'+client_id+'"]').closest('tr'); 
                client_id = $(next_joint).attr('id');;
                client_joint_id = $(next_joint).find('.goToJointProfile').attr('value'); 
                client_type = 'joint'; 
            } else {
                var next_client = $('table tr[id="'+client_id+'"]').closest('tr').next(); 
                client_id = $(next_client).attr('id'); 
                client_joint_id = '';
                client_type = 'client';
            } 
        } 

        getProfileDetails(client_id,client_joint_id,client_type,false);
            
    }); 


    $('.btn-prev-client').on('click', function(event) {
        event.preventDefault(); 
        if (client_type == 'client') {
            var prev_client = $('table tr[id="'+client_id+'"]').closest('tr').prev(); 
            if ($(prev_client).find('.goToJointProfile').length == 1) {
                client_id = $(prev_client).attr('id'); 
                client_joint_id = $(prev_client).find('.goToJointProfile').attr('value');;
                client_type = 'joint';
            } else {
                client_id = $(prev_client).attr('id'); 
                client_joint_id = '';
                client_type = 'client';
            }
        } else {  
            var prev_client = $('.goToJointProfile[value="'+client_joint_id+'"]').closest('tr'); 
            client_id = $(prev_client).attr('id'); 
            client_joint_id = '';
            client_type = 'client'; 
        }
 

        getProfileDetails(client_id,client_joint_id,client_type,false);
            
            
    }); 
    

    function update_billing_address() {
        var profile_name = $('input[name="profile_name"]').val();
        var profile_address = $('input[name="profile_address"]').val();
        var profile_city = $('input[name="profile_city"]').val();
        var profile_state_province = $('select[name="profile_state_province"]').val();
        var profile_zip_postal_code = $('input[name="profile_zip_postal_code"]').val();



        $('input[name="billing_card_holder"]').val(profile_name);
        $('input[name="billing_address"]').val(profile_address);
        $('input[name="billing_city"]').val(profile_city);
        $('select[name="billing_state"]').val(profile_state_province);
        $('input[name="billing_zip"]').val(profile_zip_postal_code);

        $('input[name="billing_card_holder"]').trigger('change');
        $('input[name="billing_address"]').trigger('change');
        $('input[name="billing_city"]').trigger('change');
        $('select[name="billing_state"]').trigger('change');
        $('input[name="billing_zip"]').trigger('change');
    }

    $('.send_bulk_sms_to').on('change', function(event) {
        event.preventDefault();
        var selected_options = $(this).find('option:selected');
        $('.to_count').html(selected_options.length);
    });

    $('.btn_send_bulk_sms').on('click', function(event) {
        event.preventDefault(); 

        var message = $('.send_bulk_sms_message_content').val();
        var values = $('.send_bulk_sms_to').val(); 
        var to_numbers = []; 

        var options = $('.send_bulk_sms_to option');
        $.each(options, function(index, option) {
            if ($(option).is(':selected')) {
                to_numbers.push({
                    'id': $(option).attr('id'),
                    'type': $(option).attr('type'),
                    'phoneNumber': $(option).attr('value')
                });
            }
        });

        // console.log(to_numbers);



        if (to_numbers.length < 51) {
            swal({
                title: "SMS Confirmation",
                text: 'Bulk Sms will be sent to '+to_numbers.length+' recipients',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Send!",
                closeOnConfirm: true ,
                preConfirm: function() { 
                    
                    $('.btn_send_bulk_sms').attr('disabled', true);
                    $('.btn_send_bulk_sms').html('<i class="fas fa-spinner fa-spin"></i>');

                    console.log(to_numbers);
                    console.log(message);
                    $.post('clients/sendBulkRCSms', {message,to_numbers} , function(data, textStatus, xhr) { 
                        console.log(data);
                        swal("Bulk SMS Sent!",'','success');
                        $('.btn_send_bulk_sms').attr('disabled', false);
                        $('.btn_send_bulk_sms').html('Send!');
                    });
                }
            });
        } else {
            swal('Error','RingCentral SMS limit per minute is 50 sms, please reduce/remove some recipients');
        }   
    });

    function trim_phone(str) {
        str = str.replace(/ /g,'');
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.replace(/-/g,'');

        return str;
    }

    $('.btn_open_documents').on('click', function(event) {
        event.preventDefault();
        var checkboxes = $('#tableClientDocuments tbody tr td:nth-child(1) ');
        $.each(checkboxes, function(index, checkbox) {
             if ($(checkbox).find('input').prop('checked')) { 
                var href = ($(checkbox).closest('tr').find('td:nth-child(5)').find('a[target="_blank"]')).length > 0 ? $(checkbox).closest('tr').find('td:nth-child(5)').find('a[target="_blank"]').attr('href') : $(checkbox).closest('tr').find('td:nth-child(5)').find('.btnDownloadDocument').attr('href');; 
                window.open(href,'popUpWindow'+index,'height=99999,width=99999,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');; 
             }
        });
    });

    

    $('#pageTable').on('click', '.btn_delete_lead_client', function(event) {
        event.preventDefault();
        var client_id = $(this).attr('client_id');
        var tr = $(this).closest('tr');
        swal({
            title: "Are you sure?",
            text: 'this lead will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                var data =  {
                                table: 'clients',
                                pk: client_id,
                                action: 'save',
                                fields: {
                                            client_status: 'Archived'
                                        },
                                return: ''
                            };
                
                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                    tr.remove();
                });
            }
        });
    });

    $('#pageTable').on('click', '.btn_delete_lead_joint', function(event) {
        event.preventDefault();
        var client_joint_id = $(this).attr('client_joint_id');
        alert(client_joint_id);
    });

    $('#btnCancellClient').on('click', function(event) {
        event.preventDefault(); 

        var name1 = $('input[name="profile_name"]').val();
        var name2 = '';
        var email1 = $('input[name="profile_email_address"]').val();
        var email2 = '';
        var template_id = 51;
        var href = 'https://admin.creditlynx.com/DocuLynx/template?action=send&template_id='+template_id+'&id1=0&id2=&name1='+name1+'&email1='+email1+'&name2=&email2=&subject=Credit+Lynx+Cancellation&message=Please+review+and+sign.';

        href = href.replace(/action=send/,'action=sent');
        var subject = 'Credit Lynx Cancellation';
        var message = 'Please review and sign.'; 
        $.post('<?php echo base_url('DocuLynx/templateSentFromProfile') ?>',
            {href,client_id,template_id,name1,name2,email1,email2,subject,message},function(data){
                swal("Cancellation Sent",'','success');
                var data =  {
                                table: 'notes',
                                pk: '',
                                action: 'save',
                                fields: {
                                            note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                            note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                            sender: '<?php echo $userdata['login_folder'] ?>',
                                            sender_id: '<?php echo $userdata['user_id'] ?>',
                                            sender_photo: '<?php echo $userdata['photo'] ?>',
                                            sender_name: '<?php echo $userdata['name'] ?>',
                                            note: 'Cancellation Sent!',
                                            note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                            note_sticky: 1
                                        },
                                return: ''
                            };
                
                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                    console.log(data);
                    getNotes();
                });

                $('#profile_client_status').val('Cancelling');
                $('#profile_client_status').trigger('change');
                
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
        
    });

    $('.btn_send_new_payment_form').on('click', function(event) {
        event.preventDefault(); 

        var name1 = $('input[name="profile_name"]').val();
        var name2 = '';
        var email1 = $('input[name="profile_email_address"]').val();
        var email2 = '';
        var template_id = 52;
        var href = 'https://admin.creditlynx.com/DocuLynx/template?action=send&template_id='+template_id+'&id1=0&id2=&name1='+name1+'&email1='+email1+'&name2=&email2=&subject=Credit+Lynx+New+Payment+Authorization+Form&message=Please+review+and+sign.';

        href = href.replace(/action=send/,'action=sent');
        var subject = 'Credit Lynx New Payment Authorization Form';
        var message = 'Please review and sign.'; 
        $.post('<?php echo base_url('DocuLynx/templateSentFromProfile') ?>',
            {href,client_id,template_id,name1,name2,email1,email2,subject,message},function(data){
                swal("New Payment Authorization Form Sent!",'','success');
                var data =  {
                                table: 'notes',
                                pk: '',
                                action: 'save',
                                fields: {
                                            note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                            note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                            sender: '<?php echo $userdata['login_folder'] ?>',
                                            sender_id: '<?php echo $userdata['user_id'] ?>',
                                            sender_photo: '<?php echo $userdata['photo'] ?>',
                                            sender_name: '<?php echo $userdata['name'] ?>',
                                            note: 'New Payment Authorization Form Sent!',
                                            note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                            note_sticky: 1
                                        },
                                return: ''
                            };
                
                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                    console.log(data);
                    getNotes();
                }); 
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
        
    });
    
    
    $('.sectionEnrollmentInformation').on('change', 'input[name="enrollment_reports_received"]', function(event) {
        var bureau_date = $(this).val();
        var where = {};
        if (client_type == 'client') {
            where = { client_id : client_id};
        } else { 
            where = { client_joint_id : client_joint_id};
        }
        var data =  {
                        table: client_type == 'client' ? 'client_account_details' : 'client_joint_account_details',
                        pk: JSON.stringify(where),
                        action: 'save',
                        fields: {
                                    bureau_date: bureau_date
                                },
                        return: ''
                    };
        
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            console.log(data);
        });
    });


    $('.sectionEnrollmentInformation').on('click', '.btn_add_a2',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a2',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'a2_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            var container_clients_a2 = $('.container_clients_a2');
            var newInput = '<div class="input-group m-b-5" a2_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label style="min-width: 92px !important;" for="a2" class="input-group-text">Address 2 </label>\
                                </div>\
                                <input type="text" name="clients_a2" class="form-control date-inputmask" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btn_delete_a2 input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            container_clients_a2.append(newInput);
            $(".date-inputmask").inputmask("mm/dd/yyyy");
        });
    });

    $('.container_clients_a2').on('click', '.btn_delete_a2', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a2',
                        pk: $(this).closest('.input-group').attr('a2_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.container_clients_a2').on('change', 'input[name="clients_a2"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a2',
                        pk: $(this).closest('.input-group').attr('a2_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    address2: $(this).val()
                                },
                        return: 'a2_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });

    // A1
    $('.sectionEnrollmentInformation').on('click', '.btn_add_a1',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a1',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'a1_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            var container_clients_a1 = $('.container_clients_a1');
            var newInput = '<div class="input-group m-b-5" a1_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label style="min-width: 92px !important;" for="a1" class="input-group-text">Address 1 </label>\
                                </div>\
                                <input type="text" name="clients_a1" class="form-control date-inputmask" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btn_delete_a1 input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            container_clients_a1.append(newInput);
            $(".date-inputmask").inputmask("mm/dd/yyyy");
        });
    });

    $('.container_clients_a1').on('click', '.btn_delete_a1', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a1',
                        pk: $(this).closest('.input-group').attr('a1_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.container_clients_a1').on('change', 'input[name="clients_a1"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_a1',
                        pk: $(this).closest('.input-group').attr('a1_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    address1: $(this).val()
                                },
                        return: 'a1_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });

    //  CR
    $('.sectionEnrollmentInformation').on('click', '.btn_add_cr',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_cr',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'cr_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            var containerClientsCR = $('.containerClientsCR');
            var newInput = '<div class="input-group m-b-5" cr_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label for="cr" class="input-group-text">Credit Report </label>\
                                </div>\
                                <input type="text" name="clients_cr" class="form-control date-inputmask" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btn_delete_cr input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            containerClientsCR.append(newInput);
            $(".date-inputmask").inputmask("mm/dd/yyyy");
        });
    });

    $('.containerClientsCR').on('click', '.btn_delete_cr', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_cr',
                        pk: $(this).closest('.input-group').attr('cr_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.containerClientsCR').on('change', 'input[name="clients_cr"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_cr',
                        pk: $(this).closest('.input-group').attr('cr_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    credit_report: $(this).val()
                                },
                        return: 'cr_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });

    // SA
    $('.sectionEnrollmentInformation').on('click', '.btn_add_sa',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_sa',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'sa_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            var containerClientsSA = $('.containerClientsSA');
            var newInput = '<div class="input-group m-b-5" sa_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label for="sa" class="input-group-text">Service Agreement </label>\
                                </div>\
                                <input type="text" name="clients_sa" class="form-control date-inputmask" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btn_delete_sa input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            containerClientsSA.append(newInput);
            $(".date-inputmask").inputmask("mm/dd/yyyy");
        });
    });

    $('.containerClientsSA').on('click', '.btn_delete_sa', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_sa',
                        pk: $(this).closest('.input-group').attr('sa_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.containerClientsSA').on('change', 'input[name="clients_sa"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_sa',
                        pk: $(this).closest('.input-group').attr('sa_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    service_agreement: $(this).val()
                                },
                        return: 'sa_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });

    // ENROLLMENT
    $('.sectionEnrollmentInformation').on('click', '.btn_add_enrolled',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_enrolled',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'enrolled_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            var containerClientsEnrolled = $('.containerClientsEnrolled');
            var newInput = '<div class="input-group m-b-5" enrolled_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label for="enrolled" class="input-group-text">Enrolled </label>\
                                </div>\
                                <input type="text" name="clients_enrolled" class="form-control date-inputmask" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btn_delete_enrolled input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            containerClientsEnrolled.append(newInput);
            $(".date-inputmask").inputmask("mm/dd/yyyy");
        });
    });

    $('.containerClientsEnrolled').on('click', '.btn_delete_enrolled', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_enrolled',
                        pk: $(this).closest('.input-group').attr('enrolled_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.containerClientsEnrolled').on('change', 'input[name="clients_enrolled"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_enrolled',
                        pk: $(this).closest('.input-group').attr('enrolled_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    enrolled: $(this).val()
                                },
                        return: 'enrolled_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });
    
    $('.containerClientsFax').on('click', '.btnAddClientsFax',function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_fax',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'fax_id'
                    };

        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) {
            var containerClientsFaxSection = $('.containerClientsFaxSection');
            var newInput = '<div class="input-group m-b-5" fax_id="'+data+'">\
                                <div class="input-group-prepend">\
                                    <label for="fax" class="input-group-text">Fax </label>\
                                </div>\
                                <input type="text" name="clients_fax" class="form-control" >\
                                <div class="input-group-append">\
                                    <a href="#" class="btnDeleteClientsFax input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                </div>\
                            </div>';
            containerClientsFaxSection.append(newInput);
        });
    });

    $('.containerClientsFax').on('click', '.btnDeleteClientsFax', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_fax',
                        pk: $(this).closest('.input-group').attr('fax_id'),
                        action: 'delete'
                    };
        var button = $(this);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            button.closest('.input-group').remove();
        });
    });

    $('.containerClientsFax').on('change', 'input[name="clients_fax"]', function(event) {
        event.preventDefault();
        var data =  {
                        table: 'clients_fax',
                        pk: $(this).closest('.input-group').attr('fax_id'),
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    fax: $(this).val()
                                },
                        return: 'fax_id'
                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            console.log(data);
        });
    });
  

    $('.containerClientOtherEmails').on('change', 'input[type="email"]', function(event) {
        event.preventDefault();
        var coe_id = $(this).closest('.input-group').attr('coe_id');
        var email_address = $(this).closest('.input-group').find('[field="email_address"]').val();;
        saveClientOtherEmail('update',coe_id,client_type == 'client' ? client_id : client_joint_id,client_type,email_address);
    });

    $('.btnAddNewOtherEmail').on('click', function(event) {
        event.preventDefault();
        saveClientOtherEmail('add','',client_type == 'client' ? client_id : client_joint_id,client_type,'');
    });

    $('#sectionProfileInformation').on('click','.btnDeleteClientOtherEmail', function(event) {
        event.preventDefault();
        var coe_id = $(this).closest('.input-group').attr('coe_id');;
        swal({
            title: "Are you sure?",
            text: 'this email address will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                deleteClientOtherEmail(coe_id);
            }
        });
    });

    $('.containerOtherCards').on('change', 'input[type="text"]', function(event) {
        event.preventDefault(); 
        var category = 'edit';

        var data =  {
                        table: 'client_other_cards',
                        pk: $(this).closest('.row').find('[field="coc_id"]').val(),
                        action: 'save',
                        fields: {
                                    coc_id: $(this).closest('.row').find('[field="coc_id"]').val(),
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    card_number: $(this).closest('.row').find('[field="card_number"]').val(),
                                    card_exp: $(this).closest('.row').find('[field="card_exp"]').val(),
                                    card_cvv: $(this).closest('.row').find('[field="card_cvv"]').val(),
                                    card_holder: $(this).closest('.row').find('[field="card_holder"]').val(),
                                    shipping_address: $(this).closest('.row').find('[field="shipping_address"]').val(),
                                    shipping_city: $(this).closest('.row').find('[field="shipping_city"]').val(),
                                    shipping_state: $(this).closest('.row').find('[field="shipping_state"]').val(),
                                    shipping_zip: $(this).closest('.row').find('[field="shipping_zip"]').val(),
                                },
                        return: 'coc_id'
                    };
        
        saveClientOtherCard(category,data);
    });


    $('#sectionBillingInformation').on('click','.btnAddOtherCard', function(event) {
        event.preventDefault(); 
        var category = 'add';
 
        var data =  {
                        table: 'client_other_cards',
                        pk: '',
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        return: 'coc_id'
                    };
        
        saveClientOtherCard(category,data);
    });
    $('#sectionBillingInformation').on('click','.btnDeleteOtherCard', function(event) {
        event.preventDefault();
        var coc_id = $(this).closest('.row').find('[field="coc_id"]').val();
        swal({
            title: "Are you sure?",
            text: 'this card information will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                deleteClientOtherCard(coc_id);
            }
        });

    });
    $('#sectionBillingInformation').on('click','.btn_set_to_primary', function(event) {
        event.preventDefault();
        var coc_id = $(this).closest('.row').find('[field="coc_id"]').val();
        swal({
            title: "Are you sure?",
            text: 'this card information will be set to primary card',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() { 
                set_to_primary_card(client_type == 'client' ? client_id:client_joints,client_type,coc_id);
            }
        });
 
    });

    function set_to_primary_card(id,type,coc_id) {
        $.post('<?php echo base_url('admin/clients/set_to_primary_card') ?>', {id,type,coc_id}, function(data, textStatus, xhr) {
            getProfileDetails(client_id,client_joint_id,client_type,false);
        });
    }



    $('#bank_account_name').on('keyup', function(event) {
        $(this).val($(this).val().toUpperCase());
    });
    $('#creditors_account_name').on('keyup', function(event) {
        $(this).val($(this).val().toUpperCase());
    });



    $('#add-edit-account-types-modal').on('shown.bs.modal', function(event) {
        getAccountTypes();
    });
    $('#add-edit-dispute-verbiage-modal').on('shown.bs.modal', function(event) {
        getAccountTypes();
    });

    $('#btnOpeTaskeTemplatesModal').on('click', function(event) {
        getTaskTemplates();
    });

    $('#task_templates_container').on('click','.lh',function(e){
        var task_template = $(this).attr('task_temp');

        $('#new_task').val(task_template);
    });



    $('#tableTaskTemplates').on('click','a',function(e){
        e.preventDefault();
        var tr = $(this).closest('tr');
        var task_template_id = tr.attr('id');
        var tdata = tr.find('td');

        $('#new_task_template_id').val(task_template_id);

        var task_shortname = $(tdata[0]).find('a').html();
        var task_template = $(tdata[1]).html();

        $('#new_task_shortname').val(task_shortname);
        $('#new_task_template').val(task_template);
    });

    $('#btnOpenNoteTemplatesModal').on('click', function(event) {
        getNoteTemplates();
    });

    $('.note_templates_container').on('click', '.note_template',function(e){
        e.preventDefault();
        var note_template = $(this).attr('note_template'); 
        $('#new_note').val(note_template);
    });



    $('#tableNoteTemplates').on('click','a',function(e){
        e.preventDefault();
        var tr = $(this).closest('tr');
        var note_template_id = tr.attr('id');
        var tdata = tr.find('td');

        $('#new_note_template_id').val(note_template_id);

        var note_shortname = $(tdata[0]).find('a').html();
        var note_template = $(tdata[1]).html();

        $('#new_note_shortname').val(note_shortname);
        $('#new_note_template').val(note_template);
    });

    $('#btnOpenAlertTemplatesModal').on('click', function(event) {
        getAlertTemplates();
    });

    $('#tableLetterCreditors').on('click', '.btnEditLetterCreditors', function(event) {
        event.preventDefault();
        var func = $(this).attr('func');
        if (func == 'edit') {
            $(this).attr('func','save');
            $(this).html('<i class="fas fa-save"></i>');
            var tr = $(this).closest('tr');
            var tds = tr.find('td');
            $.each(tds, function(index, td) {
                if (index != tds.length -1) {
                    var value = $(td).html();
                    var input = '<input type="text" value="'+value+'">';
                    $(td).html(input);
                }
            });
        } else {
            $(this).attr('func','edit');
            $(this).html('<i class="fas fa-spinner fa-spin"></i>');
            var tr = $(this).closest('tr');
            var creditor_id = tr.attr('creditor_id');
            var td1 = tr.find('td:nth-child(1)').find('input').val();
            var td2 = tr.find('td:nth-child(2)').find('input').val();
            var td3 = tr.find('td:nth-child(3)').find('input').val();
            saveLetterCreditor(creditor_id,td1,td2,td3);
        }
    });

    $('#tableLetterCreditors').on('click', '.btnDeleteLetterCreditors', function(event) {
        event.preventDefault();
        var tr = $(this).closest('tr');
        var creditor_id = tr.attr('creditor_id');
        var button = $(this);
        swal({
            title: "Are you sure?",
            text: 'this creditor will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                button.html('<i class="fas fa-spinner fa-spin"></i>');
                $.post('<?php echo base_url('admin/clients/deleteLetterCreditor') ?>', {creditor_id}, function(data, textStatus, xhr) {
                    getLetterCreditors();
                });
            }
        });
    });

    $('#template_title_creditor').on('change', function(event) {
        event.preventDefault();
        var template_title = $(this).val();
        getTemplate('creditor',template_title,'Creditor Letters');
        $('.btnFetchDataTemplateCreditor').removeClass('hide');
        $('#containerTemplateHelpers').addClass('hide');
        $('.btnShowSaveTemplateCreditor').removeClass('hide');
        $('.btnSaveCreditorTemplate').addClass('hide');
    });

    $('#select_letter_creditors').on('change', function(event) {
        event.preventDefault();
        $('#template_title_creditor').trigger('change');
    });

    $('#bureau_letters_template_title').on('change', function(event) {
        event.preventDefault();
        var template_title = $(this).val();
        var template_header = $('#bureau_letters_template_header').val();
        getTemplate('bureau',template_title,template_header);
        $('.btnFetchDataTemplateBureau').removeClass('hide');
        $('#containerTemplateHelpers').addClass('hide');
        $('.btnShowSaveTemplateBureau').removeClass('hide');
        $('.btnSaveBureauLetterTemplate').addClass('hide');
    });

    $('#bureau_letters_template_header').on('change', function(event) {
        event.preventDefault();
        var template_header = $(this).val();
        getTemplateTitles(template_header);
        $('.btnShowBureauLettersTemplateTitle').removeClass('hide');
    });

    $('#tableBureauLetterTemplateHeaders').on('click', '.btnEditBureauLetterTemplateHeader', function(event) {
        event.preventDefault();
        var func = $(this).attr('func');
        if (func == 'edit') {
            $(this).attr('func','save');
            $(this).html('<i class="fas fa-save"></i>');
            var tr = $(this).closest('tr');
            var td1 = tr.find('td:nth-child(1)');
            var template_header = td1.html();
            var input = '<input type="text" value="'+template_header+'">';
            td1.html(input);
        } else {
            $(this).attr('func','edit');
            $(this).html('<i class="fas fa-spinner fa-spin"></i>');
            var tr = $(this).closest('tr');
            var blth_id = tr.attr('blth_id');
            var td1 = tr.find('td:nth-child(1)');
            var template_header = td1.find('input').val();
            saveBureauLettersTemplateHeader(blth_id,template_header);
        }
    });

    $('#tableBureauLetterTemplateHeaders').on('click', '.btnDeleteBureauLetterTemplateHeader', function(event) {
        event.preventDefault();
        var tr = $(this).closest('tr');
        var blth_id = tr.attr('blth_id');
        var button = $(this);
        swal({
            title: "Are you sure?",
            text: 'this template header will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                button.html('<i class="fas fa-spinner fa-spin"></i>');
                $.post('<?php echo base_url('admin/clients/deleteBureauLetterTemplateHeader') ?>', {blth_id}, function(data, textStatus, xhr) {
                    getBureauLetterTemplateHeaders();
                });
            }
        });
    });

    $('#tableBureauLetterTemplateTitles').on('click', '.btnEditBureauLetterTemplateTitle', function(event) {
        event.preventDefault();
        var func = $(this).attr('func');
        if (func == 'edit') {
            $(this).attr('func','save');
            $(this).html('<i class="fas fa-save"></i>');
            var tr = $(this).closest('tr');
            var td1 = tr.find('td:nth-child(1)');
            var template_title = td1.html();
            var input = '<input type="text" value="'+template_title+'">';
            td1.html(input);
        } else {
            $(this).attr('func','edit');
            $(this).html('<i class="fas fa-spinner fa-spin"></i>');
            var tr = $(this).closest('tr');
            var blt_id = tr.attr('blt_id');
            var td1 = tr.find('td:nth-child(1)');
            var template_title = td1.find('input').val();
            var template_header = $('#bureau_letters_template_header').val();
            saveBureauLettersTemplateTitle(blt_id,template_header,template_title);
        }
    });

    $('#tableCreditorLetterTemplateTitles').on('click', '.btnEditCreditorLetterTemplateTitle', function(event) {
        event.preventDefault();
        var func = $(this).attr('func');
        if (func == 'edit') {
            $(this).attr('func','save');
            $(this).html('<i class="fas fa-save"></i>');
            var tr = $(this).closest('tr');
            var td1 = tr.find('td:nth-child(1)');
            var template_title = td1.html();
            var input = '<input type="text" value="'+template_title+'">';
            td1.html(input);
        } else {
            $(this).attr('func','edit');
            $(this).html('<i class="fas fa-spinner fa-spin"></i>');
            var tr = $(this).closest('tr');
            var blt_id = tr.attr('blt_id');
            var td1 = tr.find('td:nth-child(1)');
            var template_title = td1.find('input').val();
            var template_header = 'Creditor Letters';
            saveBureauLettersTemplateTitle(blt_id,template_header,template_title);
        }
    });

    $('#tableBureauLetterTemplateTitles').on('click', '.btnDeleteBureauLetterTemplateTitle', function(event) {
        event.preventDefault();
        var tr = $(this).closest('tr');
        var blt_id = tr.attr('blt_id');
        var button = $(this);
        swal({
            title: "Are you sure?",
            text: 'this template title will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                button.html('<i class="fas fa-spinner fa-spin"></i>');
                $.post('<?php echo base_url('admin/clients/deleteBureauLetterTemplateTitle') ?>', {blt_id}, function(data, textStatus, xhr) {
                    $('#bureau_letters_template_header').trigger('change');
                });
            }
        });
    });


    $('#tableCreditorLetterTemplateTitles').on('click', '.btnDeleteCreditorLetterTemplateTitle', function(event) {
        event.preventDefault();
        var tr = $(this).closest('tr');
        var blt_id = tr.attr('blt_id');
        var button = $(this);
        swal({
            title: "Are you sure?",
            text: 'this template title will be deleted',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes!",
            closeOnConfirm: true ,
            preConfirm: function() {
                button.html('<i class="fas fa-spinner fa-spin"></i>');
                $.post('<?php echo base_url('admin/clients/deleteBureauLetterTemplateTitle') ?>', {blt_id}, function(data, textStatus, xhr) {
                    getTemplateTitles('Creditor Letters');
                });
            }
        });
    });

    $('#formSaveBureauLetterTemplateHeaders').on('submit', function(event) {
        event.preventDefault();
        var template_header = $(this).find('input[name="template_header"]').val();
        saveBureauLettersTemplateHeader('',template_header);

        $('#formSaveBureauLetterTemplateHeaders').find('button').html('<i class="fas fa-spinner fa-spin"></i>');
        $('#formSaveBureauLetterTemplateHeaders').find('button').attr('disabled', true);;
    });
    $('#formSaveBureauLetterTemplateTitles').on('submit', function(event) {
        event.preventDefault();
        var template_title = $(this).find('input[name="template_title"]').val();
        var template_header = $('#bureau_letters_template_header').val();
        saveBureauLettersTemplateTitle('',template_header,template_title);

        $('#formSaveBureauLetterTemplateTitles').find('button').html('<i class="fas fa-spinner fa-spin"></i>');
        $('#formSaveBureauLetterTemplateTitles').find('button').attr('disabled', true);;
    });
    $('#formSaveCreditorLetterTemplateTitles').on('submit', function(event) {
        event.preventDefault();
        var template_title = $(this).find('input[name="template_title"]').val();
        var template_header = $('#bureau_letters_template_header').val();
        saveBureauLettersTemplateTitle('','Creditor Letters',template_title);

        $('#formSaveCreditorLetterTemplateTitles').find('button').html('<i class="fas fa-spinner fa-spin"></i>');
        $('#formSaveCreditorLetterTemplateTitles').find('button').attr('disabled', true);;
    });



    $('.btnShowBureauLettersTemplateHeader').on('click', function(event) {
        event.preventDefault();
        if ($('#containerBureauLetterTemplateHeaders').hasClass('hide')) {
            $('#containerBureauLetterTemplateHeaders').removeClass('hide');
        } else {
            $('#containerBureauLetterTemplateHeaders').addClass('hide');
        }
    });

    $('.btnShowBureauLettersTemplateTitle').on('click', function(event) {
        event.preventDefault();
        if ($('#containerBureauLetterTemplateTitles').hasClass('hide')) {
            $('#containerBureauLetterTemplateTitles').removeClass('hide');
        } else {
            $('#containerBureauLetterTemplateTitles').addClass('hide');
        }
    });
    $('.btnShowCreditorLettersTemplateTitle').on('click', function(event) {
        event.preventDefault();
        if ($('#containerCreditorLetterTemplateTitles').hasClass('hide')) {
            $('#containerCreditorLetterTemplateTitles').removeClass('hide');
        } else {
            $('#containerCreditorLetterTemplateTitles').addClass('hide');
        }
    });


    $('#btnSendInvoice').on('click', function(event) {
        event.preventDefault();
        var email_address = $('[name="profile_email_address"]').val();
        var monthly_fee = $('[name="billing_monthly_fee"]').val(); 
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type;
        $.post("<?php echo base_url('crons/send_client_invoice_manual') ?>", {id,type}, function(data, textStatus, xhr) {
            console.log(data);
            swal("Invoice Sent!",'Invoice Successfully Sent!','success');
            var data =  {
                table: 'notes',
                pk: '',
                action: 'save',
                fields: {
                            note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                            note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                            sender: '<?php echo $userdata['login_folder'] ?>',
                            sender_id: '<?php echo $userdata['user_id'] ?>',
                            sender_photo: '<?php echo $userdata['photo'] ?>',
                            sender_name: '<?php echo $userdata['name'] ?>',
                            note: 'Charged Setup Fee!',
                            note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                            note_sticky: 1
                        },
                return: ''
            };

            $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                console.log(data);
                getNotes();
            });
        });

    });


    $('.btnSendInvoice').on('click', function(event) {
        event.preventDefault();
        var email_address = $('[name="profile_email_address"]').val();
        var monthly_fee = $('[name="billing_monthly_fee"]').val(); 
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type;
        $.post("<?php echo base_url('crons/send_client_invoice_manual') ?>", {id,type}, function(data, textStatus, xhr) {
            console.log(data);
            swal("Invoice Sent!",'Invoice Successfully Sent!','success');
            var data =  {
                table: 'notes',
                pk: '',
                action: 'save',
                fields: {
                            note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                            note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                            sender: '<?php echo $userdata['login_folder'] ?>',
                            sender_id: '<?php echo $userdata['user_id'] ?>',
                            sender_photo: '<?php echo $userdata['photo'] ?>',
                            sender_name: '<?php echo $userdata['name'] ?>',
                            note: 'Charged Setup Fee!',
                            note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                            note_sticky: 1
                        },
                return: ''
            };

            $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                console.log(data);
                getNotes();
            });
        });

    });

    $('#selectDisputeVergiageAssignment').on('change', function(event) {
        event.preventDefault();
        $('#selectDisputeVergiageAssignmentInput').val($(this).val());
    });

    $('#pageTable').on('change', '.selectLeadStatus', function(event) {
        event.preventDefault();
        var lead_status = $(this).val();
        var client_id = $(this).attr('client_id');
        // alert(lead_status);
        updateFields('clients',client_id,'lead_status',lead_status,'','no prompt');
    });
    $('#pageTable').on('change', '.selectClientStatus', function(event) {
        event.preventDefault();
        var client_status = $(this).val();
        var client_id = $(this).attr('client_id');
        // alert(client_status);
        updateFields('clients',client_id,'client_status',client_status,'','no prompt');
    });
    $('#pageTable').on('change', '.selectClientJointStatus', function(event) {
        event.preventDefault();
        var client_status = $(this).val();
        var client_joint_id = $(this).attr('client_joint_id');
        // alert(client_status);
        updateFields('client_joints',client_joint_id,'client_status',client_status,'','no prompt');
    });

    $('#btnChargeSetupFee').on('click',function(){
        var plan = $('select[name="billing_plan"]').val();
        var setup_fee = $('input[name="billing_setup_fee"]').val();
        var card_number = $('input[name="billing_card_number"]').val();
        swal({
            title: "Are you sure you want to charge Setup Fee?",
            text: "Card #"+card_number+' will be charged $'+setup_fee+' now!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $('#btnChargeSetupFee').prop('disabled',true);
                var plan = $('#tab_billing_info select[name="billing_plan"]').val();
                var setup_fee = $('#tab_billing_info input[name="billing_setup_fee"]').val();

                var email = $('#tab_profile input[name="profile_email_address"]').val();


                var bank_information = getBankInformation();
                var card_info = bank_information['card_info'];
                var customer_address = bank_information['customer_address'];
                var shipping_address = bank_information['shipping_address'];

                    $.post('<?php echo base_url('admin/authorize/subscriptions/chargeCreditCard') ?>',
                        {email,plan,setup_fee,card_info,customer_address,shipping_address},
                        function(data){
                            $('#btnChargeSetupFee').prop('disabled',false);
                                // console.log(data);
                                

                            data = data.split('_');
                            if (data[0] == 'success') {
                                swal('Success',card_number+' Successfully Charged!<br>Transaction Information will be available in 5-10 seconds, Thank you<br>Transaction ID: '+data[1],'success');

                                var data =  {
                                    table: 'notes',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                                note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                                sender: '<?php echo $userdata['login_folder'] ?>',
                                                sender_id: '<?php echo $userdata['user_id'] ?>',
                                                sender_photo: '<?php echo $userdata['photo'] ?>',
                                                sender_name: '<?php echo $userdata['name'] ?>',
                                                note: 'Charged Setup Fee!',
                                                note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                                note_sticky: 1
                                            },
                                    return: ''
                                };
                    
                                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                                    console.log(data);
                                    getNotes();
                                });

                                setTimeout(function(){
                                    $.post('<?php echo base_url('admin/authorize/subscriptions/getListOfSubscriptionsCron') ?>',
                                        function(data, textStatus, xhr) {
                                            tbl_authorize_transactions.clear().draw(); 
                                            setTimeout(function(){
                                                getClientSubscriptions();
                                            },3000);
                                            convertLeadToClient(client_id);
                                    });
                                },5000);
                            } else {
                                swal('Error',data[1],'error');
                            }


                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
            }
        });

    });

    $('#btnChargeMonthlyFee').on('click',function(){
        var plan = $('select[name="billing_plan"]').val();
        var monthly_fee = $('input[name="billing_monthly_fee"]').val();
        var card_number = $('input[name="billing_card_number"]').val();
        swal({
            title: "Are you sure you want to charge Monthly Fee?",
            text: "Card #"+card_number+' will be charged $'+monthly_fee+' now!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $('#btnChargeMonthlyFee').prop('disabled',true);
                var plan = $('#tab_billing_info select[name="billing_plan"]').val();
                var monthly_fee = $('#tab_billing_info input[name="billing_monthly_fee"]').val();

                var email = $('#tab_profile input[name="profile_email_address"]').val();


                var bank_information = getBankInformation();
                var card_info = bank_information['card_info'];
                var customer_address = bank_information['customer_address'];
                var shipping_address = bank_information['shipping_address'];

                    $.post('<?php echo base_url('admin/authorize/subscriptions/chargeCreditCardMonthly') ?>',
                        {email,plan,monthly_fee,card_info,customer_address,shipping_address},
                        function(data){
                            $('#btnChargeMonthlyFee').prop('disabled',false);
                                // console.log(data);

                            data = data.split('_');
                            if (data[0] == 'success') {
                                swal('Success',card_number+' Successfully Charged!<br>Transaction Information will be available in 5-10 seconds, Thank you<br>Transaction ID: '+data[1],'success');

                                var data =  {
                                    table: 'notes',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                                note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                                sender: '<?php echo $userdata['login_folder'] ?>',
                                                sender_id: '<?php echo $userdata['user_id'] ?>',
                                                sender_photo: '<?php echo $userdata['photo'] ?>',
                                                sender_name: '<?php echo $userdata['name'] ?>',
                                                note: 'Charged Monthly Fee!',
                                                note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                                note_sticky: 1
                                            },
                                    return: ''
                                };
                    
                                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                                    console.log(data);
                                    getNotes();
                                });

                                setTimeout(function(){
                                    $.post('<?php echo base_url('admin/authorize/subscriptions/getListOfSubscriptionsCron') ?>',
                                        function(data, textStatus, xhr) {
                                            tbl_authorize_transactions.clear().draw(); 
                                            setTimeout(function(){
                                                getClientSubscriptions();
                                            },3000);
                                    });
                                },5000);
                            } else {
                                swal('Error',data[1],'error');
                            }
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
            }
        });

    });

    function getBankInformation(){
        var card_info = {};
            card_info['number'] = $('#tab_billing_info input[name="billing_card_number"]').val();
            card_info['expiration_date'] = $('#tab_billing_info input[name="billing_card_expiration"]').val();
            card_info['code']  = $('#tab_billing_info input[name="billing_cvv_code"]').val();


        var customer_address = {};
            var name = $('#tab_profile input[name="profile_name"]').val();
            name = name.split(' ');
            var first_name = '';
            var last_name = name[name.length -1];
            if (name.length > 2) {
                 name.splice(name.length -1, 1);
                 first_name = name.join(' ');
            } else {
                first_name = name[0];
            }

            customer_address['first_name'] = first_name;
            customer_address['last_name'] = last_name;
            customer_address['company'] = '';
            customer_address['address'] = $('#tab_profile input[name="profile_address"]').val();;
            customer_address['city'] = $('#tab_profile input[name="profile_city"]').val();;
            customer_address['state'] = $('#tab_profile select[name="profile_state_province"]').val();;
            customer_address['zip'] = $('#tab_profile input[name="profile_zip_postal_code"]').val();;
            customer_address['country'] = 'USA';
            customer_address['phone_number'] = $('#tab_profile input[name="profile_cell_phone"]').val();;
            customer_address['fax_number'] = '';


        var shipping_address = {};
            var name = $('#tab_billing_info input[name="billing_card_holder"]').val();
            name = name.split(' ');
            var first_name = '';
            var last_name = name[name.length -1];
            if (name.length > 2) {
                 name.splice(name.length -1, 1);
                 first_name = name.join(' ');
            } else {
                first_name = name[0];
            }

            shipping_address['first_name'] = first_name;
            shipping_address['last_name'] = last_name;
            shipping_address['company'] = '';
            shipping_address['address'] = $('#tab_billing_info input[name="billing_address"]').val();;
            shipping_address['city'] = $('#tab_billing_info input[name="billing_city"]').val();;
            shipping_address['state'] = $('#tab_billing_info select[name="billing_state"]').val();;
            shipping_address['zip'] = $('#tab_billing_info input[name="billing_zip"]').val();;
            shipping_address['country'] = 'USA';
            shipping_address['phone_number'] = $('#tab_profile input[name="profile_cell_phone"]').val();;
            shipping_address['fax_number'] = '';

        var data = {};
        data['card_info'] = card_info;
        data['customer_address'] = customer_address;
        data['shipping_address'] = shipping_address;
        return data;
    }

    

    $('#btnSaveAccountBySourceCode').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var source_code = $('#inputAccountSourceCode').val();
        if (source_code != '') {
            var credit_report = $(source_code);
            var title = credit_report.filter('title').text();
            title = $.trim(title);

            var credit_scores_to_save = {};

            var accounts_to_save = [];
            if (title == 'Credit Report - IdentityIQ') {
                var account_history = credit_report.find('#AccountHistory').closest('.rpt_content_wrapper');
                var accounts = account_history.find('ng-include[src="\'tradeLinePartitionBasic\'"]');

                var bureau_date = credit_report.find('#reportTop').find('td:nth-child(4)').find('ng').html();
                bureau_date = $.trim(bureau_date);

                var credit_score = credit_report.find('#CreditScore').closest('.rpt_content_wrapper');
                var credit_scores = credit_score.find('.rpt_table4column').find('tbody').find('tr:nth-child(2)');

                credit_scores_to_save['transunion_points'] = $.trim((credit_scores.find('td:nth-child(2)').html()).replace(/\n/ig,''));
                credit_scores_to_save['experian_points'] = $.trim((credit_scores.find('td:nth-child(3)').html()).replace(/\n/ig,''));
                credit_scores_to_save['equifax_points'] = $.trim((credit_scores.find('td:nth-child(4)').html()).replace(/\n/ig,''));
                credit_scores_to_save['transunion_points'] = credit_scores_to_save['transunion_points'] == 'Score Not Reported' ? 0 : credit_scores_to_save['transunion_points'];
                credit_scores_to_save['experian_points'] = credit_scores_to_save['experian_points'] == 'Score Not Reported' ? 0 : credit_scores_to_save['experian_points'];
                credit_scores_to_save['equifax_points'] = credit_scores_to_save['equifax_points'] == 'Score Not Reported' ? 0 : credit_scores_to_save['equifax_points'];


                $.each(accounts, function(index, account) {
                    /* iterate through array or object */
                    var account_name = $(account).find('.sub_header');
                    account_name.find('ng').remove();
                    account_name = account_name.html();
                    account_name = removeComments(account_name);
                    account_name = $.trim(account_name);
                    if (account_name == '') {
                        account_name = 'N/A';
                    }
                    var bureaus = $(account).find('tbody');
                    var TransUnion = {};
                    var Experian = {};
                    var Equifax = {};


                    for (var i = 2; i <= 18; i++) {
                        var tr = bureaus.find('tr:nth-child('+i+')');
                        for (var j = 2; j <= 4; j++) {
                            var data = tr.find('td:nth-child('+j+')').find('ng-repeat');

                            if (i == 4) {
                                data = data.find('ng').html();
                            } else if (i == 16) {
                                data = data.find('div').html();
                            } else {
                                data = data.html();
                            }
                            data = $.trim((data));
                            // if (data != '') {

                            var k = 0;
                            (i === 2) ? k = 'account_number' :
                                (i === 3) ? k = 'account_type' :
                                (i === 6) ? k = 'account_status' :
                                (i === 8) ? k = 'date_opened' :
                                (i === 9) ? k = 'amount' :
                                (i === 12) ? k = 'credit_limit' :
                                (i === 13) ? k = 'past_due' :
                                (i === 14) ? k = 'payment_status' :
                                (i === 16) ? k = 'comments' : k = 0;

                                if (j == 2) {
                                    TransUnion[k] = data;
                                }
                                if (j == 3) {
                                    Experian[k] = data;
                                }
                                if (j == 4) {
                                    Equifax[k] = data;
                                }
                            // }

                        }
                    }

                    TransUnion['account_name'] = account_name;
                    TransUnion['bureau_date'] = bureau_date;
                    TransUnion['bureaus'] = 'TransUnion';

                    Experian['account_name'] = account_name;
                    Experian['bureau_date'] = bureau_date;
                    Experian['bureaus'] = 'Experian';

                    Equifax['account_name'] = account_name;
                    Equifax['bureau_date'] = bureau_date;
                    Equifax['bureaus'] = 'Equifax';

                    var source_accounts = {};
                    source_accounts[0] = TransUnion;
                    source_accounts[1] = Experian;
                    source_accounts[2] = Equifax;

                    accounts_to_save.push(source_accounts);

                });
            } else {
                var accounts = credit_report.find('#FtgCrTabContent5');
                var c_header = credit_report.find('#FtgCrHeader');
                $(c_header).find('br').remove();
                $(c_header).find('img').remove();
                var bureau_date = $.trim($(c_header).html());
                bureau_date = bureau_date.split(' ');
                bureau_date = bureau_date[bureau_date.length -2];
                bureau_date = moment(bureau_date).format('MM/DD/YYYY');




                var pgNavBar = credit_report.find('.pgNavBar');
                credit_scores_to_save['transunion_points'] = $(pgNavBar).find('.TransUnion').find('.cxpert').html();
                credit_scores_to_save['experian_points'] = $(pgNavBar).find('.Experian').find('.cxpert').html();
                credit_scores_to_save['equifax_points'] = $(pgNavBar).find('.Equifax').find('.cxpert').html();

                // console.log(bureau_date);
                $.each(accounts, function(index, account) {
                    var account_name = '';
                    var TransUnion = {};
                    var Experian = {};
                    var Equifax = {};




                    var table = $(account).find('.FtgCrDarkTradeHead').closest('table');
                    table.find('.FtgAccountHistory24').remove();
                    var rows = table.find('tr');

                    var account_name_xpn = $.trim($(rows[2]).find('.FtgCrXPNCell').html());
                    var account_name_efc = $.trim($(rows[2]).find('.FtgCrEFXCell').html());
                    var account_name_tuc = $.trim($(rows[2]).find('.FtgCrTUCCell').html());


                    account_name_xpn != '&nbsp;' ? account_name = account_name_xpn :
                    account_name_efc != '&nbsp;' ? account_name = account_name_efc :
                    account_name_tuc != '&nbsp;' ? account_name = account_name_tuc : account_name = 'N/A';

                    account_name = account_name.replace('&nbsp;', '');


                    if (account_name == '') {
                        account_name = 'N/A';
                    }
                    var new_account = [];
                    for (var i = 2; i <= rows.length - 7; i++) {
                        var tr = rows[i];
                        var label = $(tr).find('th').html();
                        label = $.trim(label.replace(':',''));
                        var k = '0';
                        (label == 'Account Name') ? k = 0 :
                        (label == 'Account Number') ? k = 'account_number' :
                        (label == 'Account Type') ? k = 'account_type' :
                        (label == 'Account Status') ? k = 'account_status' :
                        (label == 'Monthly Payment') ? k = 0 :
                        (label == 'Date Opened') ? k = 'date_opened' :
                        (label == 'Balance') ? k = 'amount' :
                        (label == 'Terms') ? k = 0 :
                        (label == 'High Balance') ? k = 0 :
                        (label == 'Limit') ? k = 'credit_limit' :
                        (label == 'Past Due') ? k = 'past_due' :
                        (label == 'Payment Status') ? k = 'payment_status' :
                        (label == 'Comments') ? k = 'comments' : k = 0;
                        var td = $(tr).find('td');
                        $.each(td, function(j, data) {
                            data = $(data).html();
                            data = data.replace('&nbsp;', '');
                            data = $.trim((data));
                            if (j == 0) {
                                Experian[k] = data;
                            }
                            if (j == 1) {
                                Equifax[k] = data;
                            }
                            if (j == 2) {
                                TransUnion[k] = data;
                            }
                        });


                    }

                    TransUnion['account_name'] = account_name;
                    TransUnion['bureau_date'] = bureau_date;
                    TransUnion['bureaus'] = 'TransUnion';

                    Experian['account_name'] = account_name;
                    Experian['bureau_date'] = bureau_date;
                    Experian['bureaus'] = 'Experian';

                    Equifax['account_name'] = account_name;
                    Equifax['bureau_date'] = bureau_date;
                    Equifax['bureaus'] = 'Equifax';

                    var source_accounts = {};
                    source_accounts[0] = TransUnion;
                    source_accounts[1] = Experian;
                    source_accounts[2] = Equifax;

                    accounts_to_save.push(source_accounts);

                });

            }
            // console.log(accounts_to_save);
            // console.log(credit_scores_to_save);
            $.post('<?php echo base_url('admin/clients/saveAccountBySourceCode') ?>',
                {client_id,client_joint_id,client_type,accounts_to_save,credit_scores_to_save},
                function(data) {
                    // console.log(data);
                    swal('Success','Accounts Successfully Saved!','success');
                    $('#modal-account-sourcecode').modal('hide');
                    if (client_type == 'joint') {
                        getResultsTracker(client_joint_id,client_type);
                    } else {
                        getResultsTracker(client_id,client_type);
                    }
                    getFixedDeletedNotReporting(client_id,client_joint_id,client_type);
                    getTableByAccount(client_id,client_joint_id,client_type);
                    getTableByBureaus(client_id,client_joint_id,client_type);
                    getClientCreditScores(client_id,client_joint_id,client_type);
            });

        }


        function removeComments(str) {
            str = str.replace(/<\!--.*?-->/g, "");
            return str;
        }
    });

    $('#btnAddNewAccountWithSC').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('#modal-account-sourcecode').modal('show');
    });

    $('.containerBureauAccounts').on('click','.btnViewRounds',function(){
        var rNumber = $(this).attr('rNumber');
        // alert(rNumber);
        // console.log($('[rounds="1-5"]'));
        $('[rounds="1-5"]').addClass('hide');
        $('[rounds="6-9"]').addClass('hide');
        $('[rounds="'+rNumber+'"]').removeClass('hide');
    });


    $('#custom_tab_content_disputes').on('change','.triggerUpdateAccountDetails',function(e){
        e.preventDefault();
        var account_id = $(this).attr('account_id');
        var field = $(this).attr('field');
        var value = $(this).val();
        var label = $(this).attr('label');
        if (client_type == 'joint') {
            updateFieldsAccounts('by_account_number','client_joint_account_details',account_id,field,value,label);
        } else {
            updateFieldsAccounts('by_account_number','client_account_details',account_id,field,value,label);
        }
    });

    $('#custom_tab_content_disputes').on('change', '.roundDates', function(event) {
        event.preventDefault();
        var value = $(this).val();
        var account_id = $(this).attr('account_id');
        var label = $(this).closest('div').find('a').html();
        var field = $(this).attr('field');
        updateClientRounds(client_type,account_id,field,value,label+ ' Date');
    });

    $('#custom_tab_content_disputes').on('change', '.roundDisputeResults', function(event) {
        event.preventDefault();
        var value = $(this).val();
        var account_id = $(this).attr('account_id');
        var field = $(this).attr('field');
        var label = $(this).closest('section').find('a:nth-child(1)').html();
        updateClientRounds(client_type,account_id,field,value,label);
    });

    $('.containerBureauAccounts').on('change','.triggerUpdateAccountDetails',function(){
        var account_id = $(this).closest('.row').attr('account_id');
        var field = $(this).attr('field');
        var value = $(this).val();
        var label = $(this).attr('label');
        if (client_type == 'joint') {
            updateFieldsAccounts('by_bureau','client_joint_account_details',account_id,field,value,label);
        } else {
            updateFieldsAccounts('by_bureau','client_account_details',account_id,field,value,label);
        }

        if (field == 'account_status') {
            if (value == 'Deleted' || value == 'Fixed' || value == 'Not Reporting' || value == 'Hold') {
                $(this).closest('.row').remove();
            }
        }
    });
    $('.containerBureauAccounts').on('change','.cntrAccountRounds',function(){
        var label = $(this).closest('div').find('label').html();
        if ($(this).hasClass('roundDates')) {
            label +=  ' Date';
        }
        var account_id = $(this).closest('.row').attr('account_id');
        var field = $(this).attr('field');
        var value = $(this).val();
        if (client_type == 'joint') {
            updateFieldsAccounts('by_bureau','client_joint_rounds',account_id,field,value,label);
        } else {
            updateFieldsAccounts('by_bureau','client_rounds',account_id,field,value,label);
        }
    });

    $('.containerBureauAccounts').on('focusout','.cntrAccountRounds',function(){
        var placeholder = $(this).attr('placeholder');
        var field = $(this).attr('field');
        var value = $(this).val();
        if (placeholder == 'mm/dd/yyyy') {
            if (!$('.containerBureauAccounts').find('[field="'+field+'"]').closest('.row').hasClass('hide')) {
                $('.containerBureauAccounts').find('[field="'+field+'"]').val(value);
                $('.containerBureauAccounts').find('[field="'+field+'"]').trigger('change');
            }

        }
    });



    $('.btnGetBureauAccounts').on('click',function(){
        var bureau = $(this).attr('bureau');
        var color = $(this).attr('span-color');
        $('#labelBureau').html(bureau);
        $('#labelBureau').removeClass();
        $('#labelBureau').addClass(color);
        $('.selectBureauAccountByType').val('All Types');
        $('.selectBureauAccountByType').trigger('change');

        $('#modal-BureauAccounts').modal('show');
        var bureauAccountsDiv = $('#modal-BureauAccounts .modal-body .containerBureauAccounts');
        bureauAccountsDiv.html('<h3 class="text-center m-t-20">Fetching Accounts Please Wait ...</h3>');

        getAccountsByBureau(bureau,client_id,client_joint_id,client_type);

    });

    $('#task_list').on('click','.editTaskDate',function(e){
        e.preventDefault();
        var task_id = $(this).closest('.sl-item').attr('value');
        $.confirm({
            title: 'Change task timestamp',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Task Date</label>' +
            '<input type="text" placeholder="Date Time" class="task_date form-control" required />' +
            '</div><script> ' +
                '$(\'.task_date\').inputmask(\'datetime\', { \
                    mask: "1/1/y h:s t\\m",\
                    alias: "mm/dd/yyyy",\
                    placeholder: "mm/dd/yyyy hh:mm aa",\
                    separator: "/",\
                    hourFormat: "12"\
                });' +
            '<\/script>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var task_date = this.$content.find('.task_date').val();
                        if(!task_date){
                            $.alert('provide a valid alert date');
                            return false;
                        }
                        changeTaskDate(task_id,task_date);
                        function changeTaskDate(alert_id,task_date) {
                            $.post('<?php echo base_url('admin/clients/changeTaskDate') ?>',{task_id,task_date}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getTasks(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#task_list').on('click','.editTaskTask',function(e){
        e.preventDefault();
        var task_id = $(this).closest('.sl-item').attr('value');
        var task = $(this).html();
        $.confirm({
            title: 'Change Task',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Task</label>' +
            '<textarea style="fontsize: 12px" rows="5" placeholder="Task" class="task form-control" required >'+task+'</textarea>' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var task = this.$content.find('.task').val();
                        if(!task){
                            $.alert('provide a task');
                            return false;
                        }
                        changeTaskTask(task_id,task);
                        function changeTaskTask(task_id,task) {
                            $.post('<?php echo base_url('admin/clients/changeTaskTask') ?>',{task_id,task}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getTasks(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#btnRefreshCallLogsTab').on('click',function(e){
        e.preventDefault();
        getCallLogsByClient(client_id,client_type);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var target = $(e.target).attr("href") // activated tab
      if (target == '#tab_call_logs') {
        getCallLogsByClient(client_id,client_type);
      }
    });

    $('#btnCallClient').on('click',function(e){
        e.preventDefault();
        var client_name = $('[name="profile_name"]').val();
        var cell_phone = $('[name="profile_cell_phone"]').val();
        // swal({
        //     title: "Call "+client_name+"?",
        //     text: 'Call will be dialed from Company Number to '+cell_phone,
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#DD6B55",
        //     confirmButtonText: "Call Now!",
        //     closeOnConfirm: true ,
        //     preConfirm: function() {
        //         callClient(client_id,client_type);
        //         function callClient(client_id,client_type) {
        //             $.post('<?php echo base_url('admin/clients/callClient') ?>',{client_id,client_type}, function(data){
        //                     console.log(data);
        //                 }).fail(function(xhr){
        //                     console.log(xhr.responseText);
        //                 });
        //             }
        //     }
        // });
        // swal({
        //     title: "Call "+client_name+"?",
        //     text: 'Call will be dialed from Company Number to '+cell_phone,
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#DD6B55",
        //     confirmButtonText: "Call Now!",
        //     closeOnConfirm: true ,
        //     preConfirm: function() {
        //         callClient(client_id,client_type);
        //         function callClient(client_id,client_type) {
        //             $.post('<?php echo base_url('admin/clients/callClient') ?>',{client_id,client_type}, function(data){
        //                     console.log(data);
        //                 }).fail(function(xhr){
        //                     console.log(xhr.responseText);
        //                 });
        //             }
        //     }
        // });
        swal({
            title: "Call "+client_name+"?",
            text: 'Call will be dialed from Company Number to '+cell_phone,
            type: "warning",
            html:
                "<br>" +
                '<button type="button" role="button" tabindex="0" class="SwalCall customSwalBtn btn btn-primary waves-effect waves-light btn-success">' + 'Call' + '</button>' +
                '<button type="button" role="button" tabindex="0" class="SwalText customSwalBtn btn btn-info waves-effect waves-light btn-success m-l-5">' + 'Text' + '</button>' +
                '<button type="button" role="button" tabindex="0" class="SwalVoic customSwalBtn btn btn-warning waves-effect waves-light btn-success m-l-5">' + 'Voicemail' + '</button>' +
                '<button type="button" role="button" tabindex="0" class="SwalCanc customSwalBtn btn btn-danger waves-effect waves-light btn-success m-l-5">' + 'Cancel' + '</button>',
            showCancelButton: false,
            showConfirmButton: false
        });

        $('.SwalCall').on('click',function(e){
            callClient(client_id,client_type);
            function callClient(client_id,client_type) {
                $.post('<?php echo base_url('admin/clients/callClient') ?>',{client_id,client_type}, function(data){
                        // console.log(data);
                    }).fail(function(xhr){
                        console.log(xhr.responseText);
                    });
                }
        });

        $('.SwalText').on('click',function(e){
            // swal.clickCancel();
        });

        $('.SwalVoic').on('click',function(e){
            // swal.clickCancel();
        });
        $('.SwalCanc').on('click',function(e){
            swal.clickCancel();
        });

    });


    var table_by = '<?php echo $this->input->get('by') ?>';
    var table_order = '<?php echo $this->input->get('order') ?>';
    $('#pageTable thead tr').on('click','th',function(e){
        table_order = $(this).attr('order_by');
        if ($(this).attr('aria-sort') == 'ascending') {
            table_by = 'DESC';
        } else {
            table_by = 'ASC';
        }

    });

    $('#pageTable').on('click','.btnCallClientRC',function(e){
        e.preventDefault();
        var client_name_ = $(this).closest('tr').find('.goToProfile').find('u').html();
        var client_id_ = $(this).closest('tr').attr('id');
        var client_type_ = $(this).attr('client_type');
        // console.log(client_id_);
        // console.log(client_type_);
        var cell_phone = $(this).html();
        swal({
            title: "Call "+client_name_+"?",
            text: 'Call will be dialed from Company Number to '+cell_phone,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Call Now!",
            closeOnConfirm: true ,
            preConfirm: function() {
                callClient(client_id_,client_type_);
                function callClient(client_id,client_type) {
                    $.post('<?php echo base_url('admin/clients/callClient') ?>',{client_id,client_type}, function(data){
                            // console.log(data);
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                        });
                    }
            }
        });

    });

    $('#btnSendClientWelcomeEmail').on('click',function(e){
        e.preventDefault();
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type; 
        $.post('<?php echo base_url('admin/clients/sendWelcomeEmailToClientByID') ?>', {id,type} , function(data){
                // data = JSON.parse(data);
                swal("Sent!","Welcome Email Sent!",'success');
            }).fail(function(xhr){
                console.log(xhr.responseText);
            }); 
    });
    $('#btnSendClientVerifEmail').on('click',function(e){
        e.preventDefault();
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type; 
        var name = $('input[name="profile_name"]').val();
        var email_address = $('input[name="profile_email_address"]').val();

        $.post('<?php echo base_url('admin/clients/send_verif_email') ?>', {id,type,name,email_address} , function(data){
                // data = JSON.parse(data);
                swal("Sent!","Verification Email Sent!",'success');
            }).fail(function(xhr){
                console.log(xhr.responseText);
            }); 
    });

    $('#alert_list').on('click','.editAlertDate',function(e){
        e.preventDefault();
        var alert_id = $(this).closest('.sl-item').attr('value');
        $.confirm({
            title: 'Change alert timestamp',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Alert Date</label>' +
            '<input type="text" placeholder="Date Time" class="alert_date form-control" required />' +
            '</div> <script> ' +
                '$(\'.alert_date\').inputmask(\'datetime\', { \
                    mask: "1/1/y",\
                    alias: "mm/dd/yyyy",\
                    placeholder: "mm/dd/yyyy",\
                    separator: "/",\
                    hourFormat: "12"\
                });' +
            '<\/script>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var alert_date = this.$content.find('.alert_date').val();
                        if(!alert_date){
                            $.alert('provide a valid alert date');
                            return false;
                        }
                        changeAlertDate(alert_id,alert_date);
                        function changeAlertDate(alert_id,alert_date) {
                            $.post('<?php echo base_url('admin/clients/changeAlertDate') ?>',{alert_id,alert_date}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getAlerts(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });

        $('.alert_date').inputmask('datetime', {
            mask: "2/1/y h:s:s t\m",
            alias: "mm/dd/yyyy",
            placeholder: "mm/dd/yyyy hh:mm:ss xm",
            separator: '/',
            hourFormat: "12"
        });
    });

    $('#alert_list').on('click','.editAlertNotes',function(e){
        e.preventDefault();
        var alert_id = $(this).closest('.sl-item').attr('value');
        var alert_notes = $(this).html();
        $.confirm({
            title: 'Change alert verbiage',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Verbiage</label>' +
            '<textarea style="font-size: 12px" rows="5" placeholder="Verbiage" class="alert_notes form-control" required >'+alert_notes+'</textarea>' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var alert_notes = this.$content.find('.alert_notes').val();
                        if(!alert_notes){
                            $.alert('provide a alert verbiage');
                            return false;
                        }
                        changeAlertNotes(alert_id,alert_notes);
                        function changeAlertNotes(alert_id,alert_notes) {
                            $.post('<?php echo base_url('admin/clients/changeAlertNotes') ?>',{alert_id,alert_notes}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getAlerts(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#alert_list').on('click','.editAlertSubject',function(e){
        e.preventDefault();
        var alert_id = $(this).closest('.sl-item').attr('value');
        var alert_subject = $(this).html();
        $.confirm({
            title: 'Change alert subject',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Subject</label>' +
            '<textarea style="font-size: 12px" rows="5" placeholder="Subject" class="alert_subject form-control" required >'+alert_subject+'</textarea>' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var alert_subject = this.$content.find('.alert_subject').val();
                        if(!alert_subject){
                            $.alert('provide a alert subject');
                            return false;
                        }
                        changeAlertSubject(alert_id,alert_subject);
                        function changeAlertSubject(alert_id,alert_subject) {
                            $.post('<?php echo base_url('admin/clients/changeAlertSubject') ?>',{alert_id,alert_subject}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getAlerts(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#alert_list').on('click','.deleteAlert',function(e){
        e.preventDefault();
        var alert_id = $(this).closest('.sl-item').attr('value');
        $.confirm({
            title: 'Delete this alert',
            content: '' +
            '<div >' +
            '<label>are you sure you want to delete this alert?</label>' +
            '</div>' ,
            buttons: {
                formSubmit: {
                    text: 'Yes!',
                    btnClass: 'btn-success',
                    action: function () {
                        deleteAlert(alert_id);
                        function deleteAlert(alert_id) {
                            $.post('<?php echo base_url('admin/clients/deleteAlert') ?>',{alert_id}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getAlerts(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#alert_list').on('click','.changeAlertClientType',function(e){
        e.preventDefault();
        var alert_id = $(this).closest('.sl-item').attr('value');
        var name = $(this).html()+"'s";
        $.confirm({
            title: 'Assign this alert',
            content: '' +
            '<div >' +
            '<label>are you sure you want to assign this alert to '+name+' joint account?</label>' +
            '</div>' ,
            buttons: {
                formSubmit: {
                    text: 'Yes!',
                    btnClass: 'btn-success',
                    action: function () {
                        changeAlertClientType(alert_id,client_id,client_joint_id,client_type);
                        function changeAlertClientType(alert_id) {
                            $.post('<?php echo base_url('admin/clients/changeAlertClientType') ?>',{alert_id,client_id,client_joint_id,client_type}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    getAlerts(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#note_list_sticky,#note_list').on('click','.editNoteDate',function(e){
        e.preventDefault();
        var note_id = $(this).closest('.sl-item').attr('value');
        $.confirm({
            title: 'Change note timestamp',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Note Date</label>' +
            '<input type="text" placeholder="Date Time" class="note_date form-control" required />' +
            '</div> <script> ' +
                '$(\'.note_date\').inputmask(\'datetime\', { \
                    mask: "1/1/y h:s t\\m",\
                    alias: "mm/dd/yyyy",\
                    placeholder: "mm/dd/yyyy hh:mm aa",\
                    separator: "/",\
                    hourFormat: "12"\
                });' +
            '<\/script>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var note_date = this.$content.find('.note_date').val();
                        if(!note_date){
                            $.alert('provide a valid alert date');
                            return false;
                        }
                        changeNoteDate(note_id,note_date);
                        function changeNoteDate(note_id,note_date) {
                            $.post('<?php echo base_url('admin/clients/changeNoteDate') ?>',{note_id,note_date}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    //getNotes(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#note_list_sticky,#note_list,#tab_conversation_history').on('click','.editNoteNote',function(e){
        e.preventDefault();
        // var note_id = $(this).closest('.sl-item').attr('value');
        var note_id = $(this).closest('li').attr('id');
        var note = $(this).html();
        $.confirm({
            title: 'Change Note',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Note</label>' +
            '<textarea style="font-size: 12px" rows="5" placeholder="Note" class="note form-control" required >'+note+'</textarea>' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var note = this.$content.find('.note').val();
                        if(!note){
                            $.alert('provide a note');
                            return false;
                        }
                        changeNoteNote(note_id,note);
                        function changeNoteNote(note_id,note) {
                            $.post('<?php echo base_url('admin/clients/changeNoteNote') ?>',{note_id,note}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    //getNotes(client_id,client_joint_id,client_type);
                                    get_comm_flow();
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#note_list_sticky,#note_list').on('click','.deleteNote',function(e){
        e.preventDefault();
        var note_id = $(this).closest('.sl-item').attr('value');
        $.confirm({
            title: 'Delete this note',
            content: '' +
            '<div >' +
            '<label>are you sure you want to delete this note?</label>' +
            '</div>' ,
            buttons: {
                formSubmit: {
                    text: 'Yes!',
                    btnClass: 'btn-success',
                    action: function () {
                        deleteNote(note_id);
                        function deleteNote(note_id) {
                            $.post('<?php echo base_url('admin/clients/deleteNote') ?>',{note_id}, function(data){
                                    // data = JSON.parse(data);
                                    // console.log(data);
                                    //getNotes(client_id,client_joint_id,client_type);
                                }).fail(function(xhr){
                                    console.log(xhr.responseText);
                                });
                            }
                    }
                },
                cancel: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    });

    $('#SendSignUpLink').on('click',function(){
            var email_address = $('[name="profile_email_address"]').val();
            var name = $('[name="profile_name"]').val();
            var cell_phone = $('[name="profile_cell_phone"]').val();
            if (name != '') {
                if (email_address != '') {
                    if ($('select[name="billing_plan"]').val() != '') {
                        $.post('<?php echo base_url('admin/clients/sendEmailSingUp') ?>', {client_id, client_joint_id, client_type, name, email_address,cell_phone} ,function(data){
                            // data = JSON.parse(data);
                            console.log(data);
                            // getDetails(broker_id);
                            $('#profile_lead_status').val('SA Sent');
                            $('#profile_lead_status').trigger('change');
                            swal("Email Sent!",'Signup Email successfully sent to '+email_address,'success');
                            var data =  {
                                    table: 'notes',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                                note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                                sender: '<?php echo $userdata['login_folder'] ?>',
                                                sender_id: '<?php echo $userdata['user_id'] ?>',
                                                sender_photo: '<?php echo $userdata['photo'] ?>',
                                                sender_name: '<?php echo $userdata['name'] ?>',
                                                note: 'Service Agreement Form Sent!',
                                                note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                                note_sticky: 1
                                            },
                                    return: ''
                                };
                    
                                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                                    console.log(data);
                                    getNotes();
                                });
                        }).fail(function(xhr){
                            console.log(xhr.responseText);
                        });
                    } else {
                        swal("Error",'Please Select Plan on Billing Tab','error');
                    }
                } else {
                    swal("Error",'Please Input Email Address','error');
                }
            } else {
                swal("Error",'Please Input Name','error');
            }


    });

    $('#SendSignUpLinkRenew').on('click',function(){
            var email_address = $('[name="profile_email_address"]').val();
            var name = $('[name="profile_name"]').val();
            $.post('<?php echo base_url('admin/clients/sendEmailSingUpRenew') ?>', {client_id, client_joint_id, client_type, name, email_address} ,function(data){
                // data = JSON.parse(data);
                // console.log(data);
                // getDetails(broker_id);
                swal("Email Sent!",'Signup Email successfully sent to '+email_address,'success');
                
                $('#profile_lead_status').val('SA Sent');
                $('#profile_lead_status').trigger('change'); 
                var data =  {
                    table: 'notes',
                    pk: '',
                    action: 'save',
                    fields: {
                                note_assigned: client_type == 'client' ? 'Clients' : 'Client Joints' ,
                                note_assigned_id: client_type == 'client' ? client_id : client_joint_id,
                                sender: '<?php echo $userdata['login_folder'] ?>',
                                sender_id: '<?php echo $userdata['user_id'] ?>',
                                sender_photo: '<?php echo $userdata['photo'] ?>',
                                sender_name: '<?php echo $userdata['name'] ?>',
                                note: 'Signup Form Sent!',
                                note_date: '<?php echo date('Y-m-d H:i:s') ?>',
                                note_sticky: 1
                            },
                    return: ''
                };
    
                $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                    console.log(data);
                    getNotes();
                });
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });

    });




    $('#tab_billing_info').on('change','select[name="billing_plan"]',function(){
        var setup_fee = $(this).find('option:selected').attr('setup_fee');
        $('#tab_billing_info').find('input[name="billing_setup_fee"]').val(setup_fee);
        $('#tab_billing_info').find('input[name="billing_setup_fee"]').trigger('change');

        var monthly_fee = $(this).find('option:selected').attr('monthly_fee');
        $('#tab_billing_info').find('input[name="billing_monthly_fee"]').val(monthly_fee);
        $('#tab_billing_info').find('input[name="billing_monthly_fee"]').trigger('change');
    });



    $('#btnConvertToClient').on('click',function(e){
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: 'this lead will be converted to client!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                convertLeadToClient(client_id);

            }
        });
    });

    $(document).on('shown.bs.tab', '#containerMainTabsClients a[data-toggle="tab"]', function (e) {
        // alert('TAB CHANGED');
        // console.log($(this).text());
        var tab = $(this).text();
        tab = tab.trim();
        if (tab == 'Disputes') {
            $('#containerEmailTextTabs').addClass('hide');
        } else if (tab == 'Evaluation') {
            $('#containerEmailTextTabs').addClass('hide');
        } else if (tab == 'Documents') {
            $('#containerEmailTextTabs').addClass('hide');
        } else {
            $('#containerEmailTextTabs').removeClass('hide');
        }
        // console.log('tab changed');
    });
    $(document).on('shown.bs.tab', '#containerMainTabsLeads a[data-toggle="tab"]', function (e) {
        // alert('TAB CHANGED');
        // console.log($(this).text());
        var tab = $(this).text();
        tab = tab.trim();
        if (tab == 'Disputes') {
            $('#containerEmailTextTabs').addClass('hide');
        } else if (tab == 'Evaluation') {
            $('#containerEmailTextTabs').addClass('hide');
        } else if (tab == 'Documents') {
            $('#containerEmailTextTabs').addClass('hide');
        } else {
            $('#containerEmailTextTabs').removeClass('hide');
        }
        // console.log('tab changed');
    });


    $('.btnRefreshEmailTab,.btnRefreshTextTab').on('click',function(e){
        e.preventDefault();
        //getClientEmails(client_type == 'client' ? client_id : client_joint_id,client_type);
        ////getClientTexts(client_type == 'client' ? client_id : client_joint_id,client_type);
    });





    $('#btnLookUpCardNumber').on('click',function(e){
        e.preventDefault();
        var card_number = $('input[name="billing_card_number"]').val();
        card_number = card_number.replace(/ /g,'');
        card_number = card_number.replace(/-/g,'');
        if (card_number != '') {
            lookUpCardNumber(card_number);
        }
    });

    $('.btnShowTemplateHelpers').on('click',function(e){
        e.preventDefault();
        if ($(this).closest('.row').find('.containerTemplateHelpers').hasClass('hide')) {
            $(this).closest('.row').find('.containerTemplateHelpers').removeClass('hide');
        } else {
            $(this).closest('.row').find('.containerTemplateHelpers').addClass('hide');
        }
    });

    $('#btnAddCreditor').on('click',function(e){
        e.preventDefault();
        $('#formLetterCreditors')[0].reset();
        if ($('#containerAddCreditor').hasClass('hide')) {
            $('#containerAddCreditor').removeClass('hide');
        } else {
            $('#containerAddCreditor').addClass('hide');
        }
    });
    $('#btnAddCreditorFront').on('click',function(e){
        e.preventDefault();
        $('#formLetterCreditorsFront')[0].reset();
        if ($('#containerAddCreditorFront').hasClass('hide')) {
            $('#containerAddCreditorFront').removeClass('hide');
        } else {
            $('#containerAddCreditorFront').addClass('hide');
        }
    });


    $('#formLetterCreditors').on('submit',function(e){
        e.preventDefault();
        var creditor_name = $('#creditor_name').val();
        var creditor_address = $('#creditor_address').val();
        var creditor_city_state_zip = $('#creditor_city_state_zip').val();
        saveLetterCreditor('',creditor_name,creditor_address,creditor_city_state_zip);


        $('#formLetterCreditors').find('button').html('<i class="fas fa-spinner fa-spin"></i>');
        $('#formLetterCreditors').find('button').attr('disabled', true);;
    });
    $('#formLetterCreditorsFront').on('submit',function(e){
        e.preventDefault();
        var creditor_name = $('#new_creditor_name').val();
        var creditor_address = $('#new_creditor_address').val();
        var creditor_city_state_zip = $('#new_creditor_city_state_zip').val();
        saveLetterCreditor('',creditor_name,creditor_address,creditor_city_state_zip);


        $('#formLetterCreditorsFront').find('button').html('<i class="fas fa-spinner fa-spin"></i>');
        $('#formLetterCreditorsFront').find('button').attr('disabled', true);;
    });

    $('#btnOpenBureauLettersModal').on('click',function(){
        $('#bureau-letters-modal').modal('show');
        getBureauLetterTemplateHeaders();
    });

    $('#btnOpenCreditorLettersModal').on('click',function(){
        $('#creditor-letters-modal').modal('show');
        $('#template_header_creditors').val('Creditor Letters');
        $('#template_header_creditors').trigger('change');
        getTemplateTitles('Creditor Letters');
    });

    $('.btnFetchDataTemplateBureau').on('click',function(){
        tinymce.get('bureauMCE').setMode('design');
        var template = tinymce.get('bureauMCE').getContent();
        fetchDataTemplateList(client_id,client_joint_id,client_type,template,'bureau');
    });
    $('.btnFetchDataTemplateCreditor').on('click',function(){
        tinymce.get('creditorMCE').setMode('design');
        var template = tinymce.get('creditorMCE').getContent();
        fetchDataTemplateList(client_id,client_joint_id,client_type,template,'creditor');
    });

    $('.btnShowSaveTemplateBureau').on('click',function(){
        $('#containerTemplateHelpers').addClass('hide');
        $('.btnSaveBureauLetterTemplate').removeClass('hide');
        $(this).addClass('hide');
        tinymce.get('bureauMCE').setMode('design');

        $('.btnFetchDataTemplateBureau').addClass('hide');

        var template_title = $("#bureau_letters_template_title").val();
        var template_header = $('#bureau_letters_template_header').val();
        if (template_title != '') {
            getTemplate('bureau',template_title,template_header);
        }
    });

    $('.btnShowSaveTemplateCreditor').on('click',function(){
        $('#containerTemplateHelpers').addClass('hide');
        $('.btnSaveCreditorLetterTemplate').removeClass('hide');
        $(this).addClass('hide');
        tinymce.get('creditorMCE').setMode('design');

        $('.btnFetchDataTemplateCreditor').addClass('hide');

        var template_title = $("#template_title_creditor").val();
        if (template_title != '') {
            getTemplate('creditor',template_title,'Creditor Letters');
        }
    });

    $('.btnSaveBureauLetterTemplate').on('click',function(){
        $('.btnShowSaveTemplateBureau').removeClass('hide');
        $('#containerTemplateHelpers').addClass('hide');
        $(this).addClass('hide');
        // tinymce.get('bureauMCE').setMode('readonly');
        $('.btnFetchDataTemplateBureau').removeClass('hide');


        var template_header = $('#bureau_letters_template_header').val();
        var template_title = $('#bureau_letters_template_title').val();
        var template = tinymce.get('bureauMCE').getContent();
        if (template_header != '') {
            if (template_title != '') {
                saveBureauLetterTemplate(template_header,template_title,template);
            }
        }
    });

    $('.btnSaveCreditorLetterTemplate').on('click',function(){
        $('.btnShowSaveTemplateCreditor').removeClass('hide');
        $('#containerTemplateHelpers').addClass('hide');
        $(this).addClass('hide');
        // tinymce.get('creditorMCE').setMode('readonly');
        $('.btnFetchDataTemplateCreditor').removeClass('hide');


        var template_title = $('#template_title_creditor').val();
        var template = tinymce.get('creditorMCE').getContent();
        if (template_title != '') {
            saveBureauLetterTemplate('Creditor Letters',template_title,template);
        }
    });

    $('#bureau-letters-modal').on('shown.bs.modal', function() {
        tinymce.get('bureauMCE').setContent('');
        $('#bureau_letters_template_title').empty();
        $('#bureau_letters_template_header').val('');
        $('.btnSaveBureauLetterTemplate').addClass('hide');
        $('#containerBureauLetterTemplateHeaders').addClass('hide');
        $('#containerBureauLetterTemplateTitles').addClass('hide');
        $('.btnShowBureauLettersTemplateTitle').addClass('hide');
        // $('#containerCreditorsSelect').addClass('hide');
    });

    $('#BrokerGameplanForm').on('change','textarea', function(e) {
        formAddGameplan();
    });

    $('#formGamePlanCheckbox').on('change', '[type="checkbox"]', function(e){
        // alert('asdasd');
        var title = '';
        var type = '';
        var Credit_Card_Needed = $('#Credit_Card_Needed');
        var Authorized_User = $('#Authorized_User');
        var Recent_Repossession = $('#Recent_Repossession');
        var High_Credit_Utilization = $('#High_Credit_Utilization');
        var Recent_Late = $('#Recent_Late');
        var Recent_Judgment = $('#Recent_Judgment');
        var New_Inquiries = $('#New_Inquiries');
        var Recent_Charge_Off = $('#Recent_Charge_Off');
        var Recent_Moved = $('#Recent_Moved');
        if (Credit_Card_Needed.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Credit_Card_Needed',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Credit_Card_Needed',0);
        }
        if (Authorized_User.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Authorized_User',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Authorized_User',0);
        }
        if (Recent_Repossession.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Repossession',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Repossession',0);
        }
        if (High_Credit_Utilization.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'High_Credit_Utilization',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'High_Credit_Utilization',0);
        }
        if (Recent_Late.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Late',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Late',0);
        }
        if (Recent_Judgment.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Judgment',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Judgment',0);
        }
        if (New_Inquiries.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'New_Inquiries',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'New_Inquiries',0);
        }
        if (Recent_Charge_Off.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Charge_Off',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Charge_Off',0);
        }
        if (Recent_Moved.is(':checked')) {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Moved',1);
        } else {
            updateCheckboxValue(client_id,client_joint_id,client_type,'Recent_Moved',0);
        }
        $.toast({
            heading: 'Success!',
            text: 'Game Check Boxes Successfully Updated',
            position: 'top-right',
            loaderBg:'#17a2b8',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });

        // InsertClientGameplanForm(Credit_Card_Needed, Authorized_User, Recent_Repossession, High_Credit_Utilization, Recent_Late, Recent_Judgment, New_Inquiries, Recent_Charge_Off, Recent_Moved, client_id);
    });
    function updateCheckboxValue(client_id,client_joint_id,client_type,field,value) {
        $.post('<?php echo base_url('admin/clients/updateCheckboxValue') ?>', {client_id,client_joint_id,client_type,field,value},function(data){
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });

    }



    // function InsertClientGameplanForm(Credit_Card_Needed, Authorized_User, Recent_Repossession, High_Credit_Utilization, Recent_Late, Recent_Judgment, New_Inquiries, Recent_Charge_Off, Recent_Moved, client_id){
    //     $.post('<?php echo base_url() ?>', {Credit_Card_Needed, Authorized_User, Recent_Repossession, High_Credit_Utilization, Recent_Late, Recent_Judgment, New_Inquiries, Recent_Charge_Off, Recent_Moved, client_id}, function(){

    //     });
    // }

    $('#CreditLynxGameplanForm').on('change','textarea', function(e) {
        formAddGameplan();
    });

    function formAddGameplan() {
        var creditlynx_gameplan = $('#creditlynx_gameplan').val();
        var broker_gameplan = $('#broker_gameplan').val();
        addClientGameplan(creditlynx_gameplan,broker_gameplan, client_id,client_joint_id,client_type);
    }

    function addClientGameplan(creditlynx_gameplan,broker_gameplan, client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url() ?>admin/clients/addClientGameplan', {creditlynx_gameplan,broker_gameplan, client_id,client_joint_id,client_type}, function(data) {
            // console.log(data);
            $.toast({
                heading: 'Success!',
                text: 'Game plan is successfully saved!',
                position: 'top-right',
                loaderBg:'#17a2b8',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            // $('#creditlynx_gameplan').val('');
            // $('#broker_gameplan').val('');
            getClientGameplan(client_id,client_joint_id,client_type);
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

    function getClientGameplan(client_id,client_joint_id,client_type){
        $.post('<?php echo base_url() ?>admin/clients/getClientGameplan', {client_id,client_joint_id,client_type}, function(data){
            data = JSON.parse(data);
            // console.log(data);
            $.each(data,function(key,value){
                $('#broker_gameplan').val(value.broker);
                $('#creditlynx_gameplan').val(value.credit_lynx);

            });

            $('#Credit_Card_Needed').attr('checked',false);
            $('#Authorized_User').attr('checked',false);
            $('#Recent_Repossession').attr('checked',false);
            $('#High_Credit_Utilization').attr('checked',false);
            $('#Recent_Late').attr('checked',false);
            $('#Recent_Judgment').attr('checked',false);
            $('#New_Inquiries').attr('checked',false);
            $('#Recent_Charge_Off').attr('checked',false);
            $('#Recent_Moved').attr('checked',false);
            $.each(data, function(key, value){
                if (value.Credit_Card_Needed == '1') {
                    $('#Credit_Card_Needed').attr('checked',(value.Credit_Card_Needed));
                }
                if (value.Authorized_User == '1') {
                    $('#Authorized_User').attr('checked',(value.Authorized_User));
                }
                if (value.Recent_Repossession == '1') {
                    $('#Recent_Repossession').attr('checked',(value.Recent_Repossession));
                }
                if (value.High_Credit_Utilization == '1') {
                    $('#High_Credit_Utilization').attr('checked',(value.High_Credit_Utilization));
                }
                if (value.Recent_Late == '1') {
                    $('#Recent_Late').attr('checked',(value.Recent_Late));
                }
                if (value.Recent_Judgment == '1') {
                    $('#Recent_Judgment').attr('checked',(value.Recent_Judgment));
                }
                if (value.New_Inquiries == '1') {
                    $('#New_Inquiries').attr('checked',(value.New_Inquiries));
                }
                if (value.Recent_Charge_Off == '1') {
                    $('#Recent_Charge_Off').attr('checked',(value.Recent_Charge_Off));
                }
                if (value.Recent_Moved == '1') {
                    $('#Recent_Moved').attr('checked',(value.Recent_Moved));
                }
            });


        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }


    $('#formCreditorsNewAccount').on('submit', function(e){
        e.preventDefault();
        var account_name = $(this).find('#creditors_account_name').val();
        var account_number = $(this).find('#creditors_account_number').val();
        var account_type = $(this).find('#select_creditors_account_type').val();
        var recommendation = $(this).find('#select_creditors_rec').val();
        var missing_balance = $(this).find('#creditors_missing_balance').val();
        var credit_limit = $(this).find('#creditors_credit_limit').val();
        var past_due = $(this).find('#creditors_past_due').val();

        var equifax = $(this).find('#checkbox_equifax_g').is(':checked');
        var experian = $(this).find('#checkbox_experian_g').is(':checked');
        var transunion = $(this).find('#checkbox_transunion_g').is(':checked');

        if (client_type == 'joint') {
            newCreditorsAccount(client_joint_id,client_type,equifax,experian,transunion,account_name,account_number,account_type,recommendation,missing_balance,credit_limit,past_due);
        } else {
            newCreditorsAccount(client_id,client_type,equifax,experian,transunion,account_name,account_number,account_type,recommendation,missing_balance,credit_limit,past_due);
        }


    });
    function newCreditorsAccount(id,client_type,equifax,experian,transunion,account_name,account_number,account_type,recommendation,missing_balance,credit_limit,past_due) {
        $.post('<?php echo base_url() ?>admin/clients/newCreditorsAccount', {id,client_type,equifax,experian,transunion,account_name,account_number,account_type,recommendation,missing_balance,credit_limit,past_due}, function(){
            $.toast({
                heading: 'Success!',
                text: 'Creditor account is successfully saved!',
                position: 'top-right',
                loaderBg:'#17a2b8',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#formCreditorsNewAccount')[0].reset();
            getCreditorsAccount(id,client_type);
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

    $('#tableClientDocuments').on('click','.btnPreview',function(e){
        e.preventDefault('<?php echo base_url() ?>');
        var tr = $(this).closest('tr');
        var doc_id = $(this).closest('tr').attr('id');
        var td = $(this).closest('td');
        var btnDownloadDocument = td.find('.btnDownloadDocument');
        window.open(btnDownloadDocument.attr('href'),'_blank');


    });


    $('#inputFileDocument').on('change',function(){
        $('#formDocumentFileUpload').submit();
    });

    $('#btnDeleteAllChecked').on('click',function(e){
        e.preventDefault();

        var checkboxes = ($('#tableClientDocuments').find('tbody tr td:nth-child(1) input[type=checkbox]:checked'));
        // console.log(checkboxes);
        swal({
            title: "Are you sure?",
            text: 'this files will be deleted permanently!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $.each(checkboxes,function(key,value){
                    var tr = $(value).closest('tr');
                    var doc_id = $(value).closest('tr').attr('id');
                    deleteCLientDocument(client_type,doc_id);
                    tr.remove();
                });

            }
        });

    });

    $('#btnDownloadAllChecked').on('click',function(e){
        e.preventDefault();

        var checkboxes = ($('#tableClientDocuments').find('tbody tr td:nth-child(1) input[type=checkbox]:checked'));
        // console.log(checkboxes);
        $.each(checkboxes,function(key,value){
            var tr = $(value).closest('tr');
            var btnDownloadDocument = tr.find('.btnDownloadDocument');
            btnDownloadDocument[0].click();
        });
    });

    $('#basic_checkbox_header').on('click',function(){
        var checkboxes = ($('#tableClientDocuments').find('tbody tr td:nth-child(1) input[type=checkbox]'));
        if ($(this).is(':checked')) {
            $.each(checkboxes,function(key,value){
                $(value).prop('checked',true);
            });

        } else {
            $.each(checkboxes,function(key,value){
                $(value).prop('checked',false);
            });
        }
    });

    $('#tableClientDocuments').on('click','.btnDeleteClientDocument',function(e){
        e.preventDefault();
        var tr = $(this).closest('tr');
        var doc_id = $(this).closest('tr').attr('id');
        swal({
            title: "Are you sure?",
            text: 'this file will be deleted permanently!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteCLientDocument(client_type,doc_id);
                tr.remove();
            }
        }).then(function(){

        });

    });


    $('.btn_upload_doc').on('click', function(event) {
        event.preventDefault();
        var url_attachment = $('#url_attachment').val();
        if (url_attachment != '') {
            var category = $('#formDocumentFileUpload').find('[name="category"]').val(); 
            if (client_type == 'client') {
                var data =  {
                                table: 'client_documents',
                                pk: '',
                                action: 'save',
                                fields: {
                                            client_id: client_id,
                                            category: category + ' Attachment from Gmail',
                                            file_name: '',
                                            file_download: url_attachment,
                                            file_size: 0,
                                            date_uploaded: moment().format('YYYY-MM-DD HH:mm:ss')
                                        },
                                return: 'doc_id'
                            };
            } else {
                var data =  {
                                table: 'client_joint_documents',
                                pk: '',
                                action: 'save',
                                fields: {
                                            client_joint_id: client_joint_id,
                                            category: category + ' Attachment from Gmail',
                                            file_name: '',
                                            file_download: url_attachment,
                                            file_size: 0,
                                            date_uploaded: moment().format('YYYY-MM-DD HH:mm:ss')
                                        },
                                return: 'doc_id'
                            };
            }
            
            $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                console.log(data);
                $('#url_attachment').val('');
                if (client_type == 'joint') {
                    getClientDocuments(client_joint_id,client_type);
                } else {
                    getClientDocuments(client_id,client_type);
                }
                console.log(category);
                if (category == 'Social') {
                    $('input[name="enrollment_ss_proof_received"]').val(moment().format('MM/DD/YYYY'));
                    $('input[name="enrollment_ss_proof_received"]').trigger('change');
                }
                if (category == 'Address (1)') {
                    $('input[name="enrollment_address_verification_received"]').val(moment().format('MM/DD/YYYY'));
                    $('input[name="enrollment_address_verification_received"]').trigger('change');
                }
                if (category == 'Social (1)') {
                    $('input[name="enrollment_address_verification_received_2"]').val(moment().format('MM/DD/YYYY'));
                    $('input[name="enrollment_address_verification_received_2"]').trigger('change');
                }


            });
        } else {
            var category = $('#formDocumentFileUpload').find('[name="category"]').val(); 
            console.log(category);
            if (category == 'Social') {
                $('input[name="enrollment_ss_proof_received"]').val(moment().format('MM/DD/YYYY'));
                $('input[name="enrollment_ss_proof_received"]').trigger('change');
            }
            if (category == 'Address (1)') {
                $('input[name="enrollment_address_verification_received"]').val(moment().format('MM/DD/YYYY'));
                $('input[name="enrollment_address_verification_received"]').trigger('change');
            }
            if (category == 'Social (1)') {
                $('input[name="enrollment_address_verification_received_2"]').val(moment().format('MM/DD/YYYY'));
                $('input[name="enrollment_address_verification_received_2"]').trigger('change');
            }
            $('#formDocumentFileUpload').submit();
        }
    });
    $('#formDocumentFileUpload').on('submit',function(e){
        e.preventDefault();
        if ($(this).find('input[name=file_name]').val() != '') {

            if ($(this).find('input[type=file]').val() == '') {
                $(this).find('input[type=file]').trigger('click');
            } else {
                var form_data = new FormData($('#formDocumentFileUpload')[0]);
                if (client_type == 'joint') {
                    form_data.append('client_joint_id',client_joint_id);

                } else {
                    form_data.append('client_id',client_id);
                }

                form_data.append('client_type',client_type);
                var client_status = $('#profile_client_status').val();
                form_data.append('client_status',client_status);

                $('#loading-modal').modal({backdrop: 'static',keyboard: true, show: true});

                $('#loading_percent').html(0);
                $('.loading_percent_div').css('width','0%');
                setTimeout(function(){
                    $.ajax({
                        type:'POST',
                        url:'<?php echo base_url('admin/clients/uploadDocumentFile') ?>',
                        processData: false,
                        contentType: false,
                        async: true,
                        cache: false,
                        data : form_data,
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            xhr.upload.addEventListener("progress", function(evt) {
                              if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                console.log(percentComplete);
                                $('#loading_percent').html(percentComplete);
                                $('.loading_percent_div').css('width',percentComplete+'%');
                                if (percentComplete === 100) {

                                }

                              }
                            }, false);

                            return xhr;
                        },
                        success: function(response){
                            // console.log(response);
                            $('#loading-modal').modal('hide');
                            if (client_type == 'joint') {
                                getClientDocuments(client_joint_id,client_type);
                            } else {
                                getClientDocuments(client_id,client_type);
                            }

                            $('#formDocumentFileUpload')[0].reset();
                            $('.dropify-clear').click();
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                        }
                    });
                },1000);

            }

        } else {
            $(this).find('input[name=file_name]').focus();
        }


    });

    $('#btnSaveNewTaskTemplate').on('click',function(){
        var task_shortname = $('#new_task_shortname');
        var task_template = $('#new_task_template');
        if (task_shortname.val() != '') {
            if (task_template.val() != '') {
                saveNewTaskTemplate(task_shortname,task_template);
            } else {
                task_template.focus();
            }
        } else {
            task_shortname.focus();
        }



    });

    $('#btnSaveNewNoteTemplate').on('click',function(){
        var note_shortname = $('#new_note_shortname');
        var note_template = $('#new_note_template');
        if (note_shortname.val() != '') {
            if (note_template.val() != '') {
                saveNewNoteTemplate(note_shortname,note_template);
            } else {
                note_template.focus();
            }
        } else {
            note_shortname.focus();
        } 
    });

    $('.btnSaveSupportTicketResponseRight').on('click',function(){
        var response = $('#support_response_right');
        if (response.val() != '') {

            response = response.val();
            $('#support_response_right').val('');
            saveSupportTicketResponse(support_id,response);

        } else {
            response.focus();
        }
    });

    $('#support_status').on('change',function(){
        var status = $(this).val();
        updateSupportTicketStatus(support_id,status);
    });

    $('#tbodySupportTicketListRight').on('click','.supportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('value');
        $('#containerAddNewSupportTicketRight').addClass('hide');
        $('#containerSupportTicketListRight').addClass('hide');
        $('#containerSupportTicketRight').removeClass('hide');
        getSupportTicket(support_id);
    });

    $('#tbodySupportTicketListRight').on('click','.btnDeleteSupportTicket',function(e){
        e.preventDefault();
        support_id = $(this).closest('tr').attr('value');
        deleteSupportTicket(support_id);
    });

    $('#formSupportTicket').on('submit',function(e){
        e.preventDefault();
        $(this).find('input[name=sender]').val(client_type);;
        if (client_type == 'joint') {
            $(this).find('input[name=sender_id]').val(client_joint_id);;
        } else {
            $(this).find('input[name=sender_id]').val(client_id);;
        }


        $.post('<?php echo base_url('admin/clients/saveSupportTicket') ?>',
            $(this).serialize(),function(data){
                $('#btnShowAddNewSupportTicketContainerRight').trigger('click');
                getTableSupportTickets(client_id);
            });
    });



    $('.btnBackToSupportTicketList').on('click',function(e){
        e.preventDefault();
        $('#containerAddNewSupportTicketRight').addClass('hide');
        $('#containerSupportTicketListRight').removeClass('hide');
        $('#containerSupportTicketRight').addClass('hide');
    });

    $('#btnShowAddNewSupportTicketContainerRight').on('click',function(e){
        e.preventDefault();
        if ($('#containerAddNewSupportTicketRight').hasClass('hide')) {
            $('#containerAddNewSupportTicketRight').removeClass('hide');
            $('#containerSupportTicketListRight').addClass('hide');
            $('#containerSupportTicketRight').addClass('hide');
        } else {
            $('#containerAddNewSupportTicketRight').addClass('hide');
            $('#containerSupportTicketListRight').removeClass('hide');
            $('#containerSupportTicketRight').addClass('hide');
        }

    });

    $('.sectionEnrollmentInformation').on('click','input[type=checkbox]',function(){
        var field = $(this).attr('id');
        var checked = $(this).is(':checked');
        var title = $(this).closest('span').find('label').html();
        if (checked) {
            if (client_type == 'joint') {
                updateFields('client_joint_enrollments',client_joint_id,field,1,title,'no prompt');
            } else {
                updateFields('client_enrollments',client_id,field,1,title,'no prompt');
            }
        } else {
            if (client_type == 'joint') {
                updateFields('client_joint_enrollments',client_joint_id,field,'0',title,'no prompt');
            } else {
                updateFields('client_enrollments',client_id,field,'0',title,'no prompt');
            }
        }
    });

    $('#btnSearchArrowLeft').on('click',function(e){
        e.preventDefault();

        var order = '<?php echo $this->input->get('order') ?>';
        var by = '<?php echo $this->input->get('by') ?>';
        var filter = '<?php echo $this->input->get('filter') ?>';
        var search = '<?php echo $this->input->get('search') ?>';

        if (client_type == 'joint') {
            getNextPreviousClient(global_converted == 1 ? 'Clients' : 'Leads','prev',client_joint_id,client_type,order,by,filter,search);
        } else {
            getNextPreviousClient(global_converted == 1 ? 'Clients' : 'Leads','prev',client_id,client_type,order,by,filter,search);
        }
    });
    $('#btnSearchArrowRight').on('click',function(e){
        e.preventDefault();

        var order = '<?php echo $this->input->get('order') ?>';
        var by = '<?php echo $this->input->get('by') ?>';
        var filter = '<?php echo $this->input->get('filter') ?>';
        var search = '<?php echo $this->input->get('search') ?>';

        if (client_type == 'joint') {
            getNextPreviousClient(global_converted == 1 ? 'Clients' : 'Leads','next',client_joint_id,client_type,order,by,filter,search);
        } else {
            getNextPreviousClient(global_converted == 1 ? 'Clients' : 'Leads','next',client_id,client_type,order,by,filter,search);
        }
    });

    function getNextPreviousClient(page,direction,id,type,order,by,filter,search) {
        $.post('clients/getNextPreviousClient', {page,direction,id,type,order,by,filter,search},function(data){
                // data = JSON.parse(data);
                // console.log(data);
                $('#searchClient').val(data);
                $('#searchClient').trigger('change');
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

    $('.changeToOtherAccount').on('click',function(e){
        e.preventDefault();
        if (client_type == 'client') {
            $('#searchClient').val(client_joint_id+'_joint_'+client_id);
        } else {
            $('#searchClient').val(client_id+'_client');
        }
        $('#searchClient').trigger('change');
    });

    $('#searchClient').on('change',function(e){
        e.preventDefault();
        var searchClient = $(this);
        var value = searchClient.val();
        var __get = '';
        if (value != '') {
            value = value.split('_');

            if (value[1] == 'joint') {
                client_type = 'joint';
                client_joint_id = value[0];
                client_id = value[2];
            } else {
                client_type = 'client';
                client_id = value[0];
            }

            if ($('.right-sidebar').hasClass('shw-rside')) {
                getProfileDetails(client_id,client_joint_id,client_type,false);
            } else {
                getProfileDetails(client_id,client_joint_id,client_type);
            }
            

        }

    });
    $('.btn_formAddUserLead').on('click',function(e){
        var button = $(this);
        if (global_converted == 1) {
            var errorUsernameSingle = $('#errorUsernameSingle');
            var errorUsernameJoint = $('#errorUsernameJoint');
            if (errorUsernameSingle.hasClass('hide') && errorUsernameJoint.hasClass('hide')) {
                var name = $('.formAddUserLead').find('input[name="name"]').val();  
                validate_name(name,'formAddUserLead',button);
            }  else {
                e.preventDefault();
            }
        } else {
            var name = $('.formAddUserLead').find('input[name="name"]').val();  
            validate_name(name,'formAddUserLead',button);
        }

    });
    $('.btn_formAddUserClient').on('click',function(e){
        var button = $(this);
        if (global_converted == 1) {
            var errorUsernameSingle = $('#errorUsernameSingle');
            var errorUsernameJoint = $('#errorUsernameJoint');
            if (errorUsernameSingle.hasClass('hide') && errorUsernameJoint.hasClass('hide')) {
                var name = $('.formAddUserClient').find('input[name="name"]').val(); 
                validate_name(name,'formAddUserClient',button);
            }  else {
                e.preventDefault();
            }
        } else {
            var name = $('.formAddUserClient').find('input[name="name"]').val(); 
            validate_name(name,'formAddUserClient',button);
        }

    });

    function validate_name(name,form,button) { 
        button.html('<i class="fas fa-spinner fa-spin"></i>');
        button.attr('disabled', true);
        $.post('<?php echo base_url('admin') ?>/clients/validate_name', {name}, function(data, textStatus, xhr) {
            console.log(name);
            if (data == 0) {
                $('.'+form).submit(); 
                
            } else {
                swal("Error",name+" is already exist!",'error');
            }
        });
    }

    $('#pageTable').on('click','.goToProfile',function(e){
        e.preventDefault();
        newUser = false;
        client_id = $(this).closest('tr').attr('id');
        client_type = 'client';
        var name = $(this).find('u').html();

        getProfileDetails(client_id,client_joint_id,client_type);

    });

var global_converted = 0;
    function getProfileDetails(client_id,client_joint_id,client_type,slide_down = true) {
        localStorage.stay = 'true';
        localStorage.id = client_id;
        localStorage.type = client_type;
        localStorage.joint_id = client_joint_id;

        // $('#searchClient').val(client_id+'_'+client_type);
        // $('#searchClient').trigger('change');
        // console.log(slide_down);
        if (slide_down == true) {
            $(".right-sidebar").slideDown(50);
            $('.right-sidebar').addClass('shw-rside');
            $(".right-sidebar").focus();
        }

        $('#bank_account_type').val('');
        $('#bank_credit_limit').val('');
        $('#bank_comments').val('');
        $('#bank_payment_status').val('');

        $('#bank_account_name').val('');
        $('#bank_account_number').val('');
        // $('#bank_account_type').val('');
        $('#bank_amount').val('');
        $('#bank_account_type').val('');
        $('#bank_dispute_verbiage').val('');
        $('#bank_past_due').val('');
        $('#bank_date_opened').val('');
        $('#bank_original_creditor').val('');
        $('#bank_status_date').val('');
        $('#bank_comments').val('');
        $('#bank_payment_status').val('');
        $('#global_bureau_date').val('');
        
        $('.right-sidebar-loader').fadeIn(50);
        $('html, body').animate({scrollTop:0}, 'slow');

        avssn_link = '';
        if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            if (slide_down == true) {
                $('.btn-minimize').trigger('click');
            } 
        }

        $.post('<?php echo base_url('admin/clients/getProfileDetails') ?>', {client_id,client_joint_id,client_type}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            // console.log(data);

            global_converted = data['profile'][0]['converted'];
            if (global_converted == 1) {
                $('#containerMainTabsLeads').addClass('hide');
                $('#containerMainTabsClients').removeClass('hide');
                $('.show_lead_content').addClass('hide');
                $('.show_client_content').removeClass('hide');
            } else {
                $('#containerMainTabsLeads').removeClass('hide');
                $('#containerMainTabsClients').addClass('hide');
                $('.show_lead_content').removeClass('hide');
                $('.show_client_content').addClass('hide');
            }

            $('.labelArchived').addClass('hide');

            $('.span_equifax_points').html('-');
            $('.span_experian_points').html('-');
            $('.span_transunion_points').html('-');



            getProfileInit(data,client_type);
            getResultsTrackerInit(data['results_tracker']);
            getCreditorsAccountInit(data['creditors_account']);
            getClientGameplanInit(data['gameplan']);
            getClientDocumentsInit(data['documents']);

            getTasksInit(data['tasks']);
            //getNotesInit(data['notes']);
            getAlertsInit(data['alerts']);
            getClientCreditScoresInit(data['credit_scores']);

            //getClientEmailsInit(data['emails']);
            //getClientTextsInit(data['texts']);
            getGameplanTimelineInit(data['timeline']);
            getClientOtherCardsInit(data['other_cards']);
            getClientOtherEmailsInit(data['other_emails']);

            if (global_converted == 1) {
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_equifax').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_experian').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_transunion').empty();
                $('.customvtabCreditors').find('#custom_tab_content_creditors').find('#section_content_equifax').empty();
                $('.customvtabCreditors').find('#custom_tab_content_creditors').find('#section_content_experian').empty();
                $('.customvtabCreditors').find('#custom_tab_content_creditors').find('#section_content_transunion').empty();
                getFixedDeletedNotReportingInit(data['results_chart']);
                getTableByAccountInit(data['disputes_by_account']);
                getTableByBureausInit(data['disputes_by_bureau']);
                getTableSupportTicketsInit(data['support_tickets']);

                getClientLetterAccountsInit(data['letter_accounts']);
            }

             
            get_subscription_list(client_id,client_joint_id,client_type);

            get_client_appointments();
            get_timeline_day();
            get_client_status_timelines();
            get_comm_flow();
            get_client_evaluation(client_type == 'client' ? client_id: client_joint_id,client_type);
            get_client_crc();
            $('.right-sidebar-loader').fadeOut(50);
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });

    }

    function get_timeline_day() {
        var data =  {
                        table: 'client_status_changes',
                        action: 'get',
                        select: {
                                    0:'DATEDIFF(CURRENT_DATE,`date_changed`) as `days`',
                                    1:'sc_id'
                                },
                        where:  {
                                    id: client_type=='client' ? client_id : client_joint_id,
                                    type: client_type,
                                    status_type: global_converted == 1 ? 'client' : 'lead'
                                },
                        field: 'sc_id',
                        order: 'DESC',
                        limit: 1,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('<?php echo base_url('admin/communication_flow/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data); 
            if (data.length != 0) { 
                $('#timeline_day').html(data[0]['days']);
            } else {
                $('#timeline_day').html(0);
            }
        });
    }

    function get_subscription_list(client_id,client_joint_id,client_type) {
        var data =  {
                        table: 'subscription_list',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'subs_id',
                        order: 'DESC',
                        limit: 1,
                        offset: 0,
                        group_by: ''
        
                    };
        update_toggle_subscription = false;
        var client_status = $('#profile_client_status').val();
        if (client_status != 'Archived' && client_status != 'Cancelled' && client_status != 'Terminated') {
            changeSwitchery($('#cb_subscription'),true, false);  

            $('.span_monthly_due').html('');
            $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
                data = JSON.parse(data);
                // console.log(data);
                if (data.length > 0) { 
                    var client_status = $('#profile_client_status').val(); 
                    var restricted_status = ['Cancelled','Archived','Terminated'];
                    var billing_monthly_due = $('[name="billing_monthly_due"]').val();  
                    $.each(data, function(index, val) {
                        if (val.status == 'Active') {
                            if (billing_monthly_due != '') {
                                if ($.inArray(client_status, restricted_status) !== -1) { 
                                    
                                    changeSwitchery($('#cb_subscription'),false, false); 
                                } else {
                                    changeSwitchery($('#cb_subscription'),true, false);
                                    
                                    var billing_monthly_due = moment($('[name="billing_monthly_due"]').val()).format('Do');
                                    $('.span_monthly_due').html('Payments occur on day '+billing_monthly_due+' of every month.');
                                } 
                            } else {
                                
                                changeSwitchery($('#cb_subscription'),false, false);  
                            }
                            // }
                        } else {
                            // alert();
                            
                            changeSwitchery($('#cb_subscription'),false, false);
                        }
                    });
                } else { 
                    
                    changeSwitchery($('#cb_subscription'),true, false);
                    var billing_monthly_due = moment($('[name="billing_monthly_due"]').val()).format('Do');
                    $('.span_monthly_due').html('Payments occur on day '+billing_monthly_due+' of every month.');
                } 

            });
        } else {
            
            changeSwitchery($('#cb_subscription'),false, false);  
            $('.span_monthly_due').html('');
        }


        // call_subs_toggle_func(); 
            
            
    }


    var update_toggle_subscription = false;
    $('#cb_subscription').on('change', function(event) {
        event.preventDefault();
        var checked = $(this).prop('checked');
        if (update_toggle_subscription) {
            changeSwitchery($('#cb_subscription'),checked, true);      
        } else {
            update_toggle_subscription = true;
        }
        
    });


    function changeSwitchery(element, checked, update) {

        if ( ( element.is(':checked') && checked == false ) || ( !element.is(':checked') && checked == true ) ) {
            element.parent().find('.switchery').trigger('click');
            
        }   
        if (update) {
            var billing_card_number = $('[name="billing_card_number"]').val();
            if (!checked) {
                    var data =  {
                                    table: 'subscription_list',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                id: client_type == 'client' ? client_id : client_joint_id,
                                                type: client_type,
                                                date_changed: moment().format('YYYY-MM-DD HH:mm:ss'),
                                                status: 'Cancelled',
                                                account_number: billing_card_number
                                            },
                                    return: ''
                                };
                    
                    $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                         
                        $('.span_monthly_due').html(''); 
                        getClientSubscriptions();
                    });
                } else { 
                    var data =  {
                                    table: 'subscription_list',
                                    pk: '',
                                    action: 'save',
                                    fields: {
                                                id: client_type == 'client' ? client_id : client_joint_id,
                                                type: client_type,
                                                date_changed: moment().format('YYYY-MM-DD HH:mm:ss'),
                                                status: 'Active',
                                                account_number: billing_card_number
                                            },
                                    return: ''
                                };
                    
                    $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                        var billing_monthly_due = moment($('[name="billing_monthly_due"]').val()).format('Do');
                        $('.span_monthly_due').html('Payments occur on day '+billing_monthly_due+' of every month.'); 
                        getClientSubscriptions();
                    });  

                    
                }
            }
        
        
            
    }

    function getProfileInit(data,type) {
        $('#tab_profile input').val('');
        $('#tab_documents input').val('');
        $('#tab_billing_info input').val('');
        unBindOnChange();
        setPlansSelections(type,
            data['profile'][0]['client_type'],
            data['profile'][0]['joint_client_status'],
            data['profile'][0]['client_name'],
            data['profile'][0]['joint_name']
            );


        setProfileTab(data['profile']);
        setBillingTab(data['billing']);
        setDocumentsTab(data['enrollment']);


        var inputs =  $('.formAddUser input');
        $.each(inputs,function(key,input){
            $(input).closest('.form-group').addClass('focused');
        });

        $('.tab-pane .select2').trigger('change');

        functionsForProfileTab();
        functionsForBillingTab();
        functionsForEnrollmentTab();
        $('.tab-pane .select2').select2();

        if ((data['billing']).length != 0 ) {
            if(data['billing'][0]['address'] == null || data['billing'][0]['address'] == '') {
                $.each(data['profile'],function(key,value){
                    $('input[name=billing_address]').val(value.address);
                    $('input[name=billing_city]').val(value.city);

                    $('select[name=billing_state]').val(value.state_province);
                    $('.zip_postal_code').val(value.zip_postal_code);
                    $('select[name=billing_state],input[name=billing_address],input[name=billing_city],input[name=billing_zip]').trigger('change');
                });
            } else {
                $.each(data['billing'],function(key,value){
                    $('input[name=billing_address]').val(value.address);
                    $('input[name=billing_city]').val(value.city);

                    $('select[name=billing_state]').val(value.state);
                    $('.zip_postal_code').val(value.zip);
                });
            }
        } else {
            $.each(data['profile'],function(key,value){
                $('input[name=billing_address]').val(value.address);
                $('input[name=billing_city]').val(value.city);

                $('select[name=billing_state]').val(value.state_province);

                $('.zip_postal_code').val(value.zip_postal_code);
                $('select[name=billing_state],input[name=billing_address],input[name=billing_city],input[name=billing_zip]').trigger('change');
            });
        }

    }

    function getClientDocumentsInit(data) {
        tableClientDocuments.clear().draw();
        var folder_name = $('#sectionProfileInformation').find('input[name="profile_name"]').val();

        var category_select = '\
                                <option value="Credit Report">Credit Report</option>\
                                <option value="Equifax">Equifax</option>\
                                <option value="Experian">Experian</option>\
                                <option value="TransUnion">TransUnion</option>\
                                <option value="Service Agreement">Service Agreement</option>\
                                <option value="New Payment">New Payment</option>\
                                <option value="Social">Social</option>\
                                <option value="Address (1)">Address (1)</option>\
                                <option value="Address (2)">Address (2)</option>\
                                <option value="AVSSN">AVSSN</option>\
                                <option value="Creditor Letters">Creditor Letters</option>\
                                <option value="Other">Other</option>\
                            ';
        $('#formDocumentFileUpload').find('[name="category"]').html(category_select);
        var hide_from_select_document_category = [];
        $.each(data,function(key,value){
            var checkbox = '<input type="checkbox" id="basic_checkbox_'+value.doc_id+'" class="filled-in chk-col-cyan ">\
                            <label for="basic_checkbox_'+value.doc_id+'"></label>';
            var btnDownload = '<a class="text-success btnDownloadDocument" download href="<?php echo base_url('assets/documents/') ?>'+folder_name+'/'+value.file_download+'"> <i class="fas fa-download  fa-lg"></i> </a>';
            var btnPreview = '<a class="btnPreview text-info" href="#"> <i class="fas  fa-eye fa-lg"></i> </a>';
            var btnPreviewHref = '<a target="_blank" class=" text-info" href="'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
            var btnDelete = '<a class="btnDeleteClientDocument text-danger" href="#"> <i class="fas  fa-trash fa-lg"></i> </a>';
            var file_size = ((value.file_size / 1000) / 1000).toFixed(2);
 

            var Credit_Report = value.category == 'Credit Report' ? 'selected' : '';
            var Equifax = value.category == 'Equifax' ? 'selected' : '';
            var Experian = value.category == 'Experian' ? 'selected' : '';
            var TransUnion = value.category == 'TransUnion' ? 'selected' : '';
            var Service_Agreement = value.category == 'Service Agreement' ? 'selected' : '';
            var New_Payment = value.category == 'New Payment' ? 'selected' : '';
            var Social = value.category == 'Social' ? 'selected' : '';
            var Address_1 = value.category == 'Address (1)' ? 'selected' : '';
            var Address_2 = value.category == 'Address (2)' ? 'selected' : '';
            var AVSSN = value.category == 'AVSSN' ? 'selected' : '';
            var Creditor_Letters = value.category == 'Creditor Letters' ? 'selected' : '';
            var Other = value.category == 'Other' ? 'selected' : '';

            var select_category = ' <select class="select_document_category" name="document_category_'+value.doc_id+'">\
                                        <option '+Credit_Report+' value="Credit Report">Credit Report</option>\
                                        <option '+Equifax+' value="Equifax">Equifax</option>\
                                        <option '+Experian+' value="Experian">Experian</option>\
                                        <option '+TransUnion+' value="TransUnion">TransUnion</option>\
                                        <option '+Service_Agreement+' value="Service Agreement">Service Agreement</option>\
                                        <option '+New_Payment+' value="New Payment">New Payment</option>\
                                        <option '+Social+' value="Social">Social</option>\
                                        <option '+Address_1+' value="Address (1)">Address (1)</option>\
                                        <option '+Address_2+' value="Address (2)">Address (2)</option>\
                                        <option '+AVSSN+' value="AVSSN">AVSSN</option>\
                                        <option '+Creditor_Letters+' value="Creditor Letters">Creditor Letters</option>\
                                        <option '+Other+' value="Other">Other</option>\
                                    </select>';

            if (value.category == 'New Payment Authorization Form' || value.category == 'OH Premiere' || value.category == 'Service Agreement' || value.category == 'Federal Disclosure' || value.category == 'Esignature Disclosure' || value.category == 'Billing Authorization Form' || value.category == 'Cancellation' || (value.category).indexOf("Attachment from Gmail") !== -1) { 
                var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id; 

            } else if ((value.category).indexOf('Invoice') !== -1) { 
                var btnPreviewHref = '<a target="_blank" class=" text-info" href="<?php echo base_url('emailCampaign/invoice?id=') ?>'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
                var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id;    
            } else if ((value.category).indexOf('Receipt') !== -1) { 
                var btnPreviewHref = '<a target="_blank" class=" text-info" href="<?php echo base_url('emailCampaign/receipt?id=') ?>'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
                var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id;    
            } else {
                var newRow = tableClientDocuments.row.add([checkbox,select_category,file_size+'MB',moment(value.date_uploaded).format('lll'),btnPreview+' '+btnDownload+' '+btnDelete ]).node().id = value.doc_id;
            }

            // if (value.category == 'Social' || value.category == 'Address (1)' || value.category == 'Address (2)') {
            //     hide_from_select_document_category.push(value.category);
            // }
            if (value.category == 'AVSSN') {
                avssn_link = '<?php echo base_url('assets/documents/') ?>'+folder_name+'/'+value.file_download;
            }

            tableClientDocuments.draw(false); 

             
        });
         
        $.each(hide_from_select_document_category, function(index, val) {
             $('.select_document_category').find('option[value="'+val+'"]').addClass('hide');
             $('#formDocumentFileUpload').find('[name="category"]').find('option[value="'+val+'"]').addClass('hide');
        });
        
    }

    $('#tableClientDocuments').on('change', '.select_document_category', function(event) {
        event.preventDefault();
        var doc_id = $(this).closest('tr').attr('id');
        console.log(data);
        var category = $(this).val();
        var data =  {
                        table: client_type == 'client' ? 'client_documents' : 'client_joint_documents',
                        pk: doc_id,
                        action: 'save',
                        fields: {
                                    category: category
                                },
                        return: 'doc_id'
                    };
        
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            console.log(data);
        });
    });



    function getResultsTrackerInit(data) {
        var customvtabDisputes = $('.customvtabDisputes');
        var tabs_vertical = customvtabDisputes.find('.tabs-vertical-disputes');
        tabs_vertical.empty();

        var tableDisputeAccounts = $('#tableDisputeAccounts tbody');
        tableDisputeAccounts.empty();


        $.each(data,function(key,value){
            var newLi = '<li  class="nav-item" style="line-height: 100%">\
                            <a class="nav-link customNavLink" data-toggle="tab" credit_limit="'+value.credit_limit+'" date_added="'+value.date_added+'" href="#an_'+value.account_number+'" role="tab">\
                                <span class="hidden-sm-up">\
                                    <i class="mdi mdi-account-card-details"></i>\
                                </span> \
                                <span class="hidden-xs-down" >\
                                    <span field="account_name" style="font-size: 60%">'+value.account_name+'</span>\
                                    <br>\
                                    <span field="account_number" style="font-size: 80%">'+value.account_number+'</span>\
                                </span>\
                            </a>\
                        </li>';
            tabs_vertical.append(newLi);


            var account_name = '<input readonly type="text" class="b-0" field="account_name" value="'+value.account_name+'" style="width: 100%"/>';
            var account_number = '<input readonly type="text" class="b-0" field="account_number" value="'+value.account_number+'" style="width: 100%"/>';
            var editButton = '<div class="btn-group"><button class="btn btn-sm btn-success btnEditDisputesAccount"><i class="fas fa-edit"></i></button>';
            var deleteButton = '<button class="btn btn-sm btn-danger btnDeleteDisputesAccount"><i class="fas fa-trash"></i></button></div>';
            var newTr = '\
                        <tr date_added="'+value.date_added+'">\
                            <td>'+account_name+'</td>\
                            <td>'+account_number+'</td>\
                            <td>'+value.credit_limit+'</td>\
                            <td>'+value.amount+'</td>\
                            <td>'+value.past_due+'</td>\
                            <td>'+value.date_opened+'</td>\
                            <td>'+editButton+deleteButton+'</td>\
                        </tr>';
            tableDisputeAccounts.append(newTr);
        });
    }

    $('#tableDisputeAccounts').on('click', '.btnEditDisputesAccount', function(event) {
        event.preventDefault();
        var button = $(this);
        var date_added = $(this).closest('tr').attr('date_added');
        var account_name = $(this).closest('tr').find('[field="account_name"]');
        var account_number = $(this).closest('tr').find('[field="account_number"]');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            account_name.removeClass('b-0');
            account_number.removeClass('b-0');
            account_name.attr('readonly',false);
            account_number.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            account_name.addClass('b-0');
            account_name.attr('readonly',true);
            account_number.addClass('b-0');
            account_number.attr('readonly',true);
            updateAccountDetails(client_type == 'client' ? client_id : client_joint_id,client_type,date_added,account_name.val(),account_number.val());
        }
    });
    $('#tableDisputeAccounts').on('click', '.btnDeleteDisputesAccount', function(event) {
        event.preventDefault();
        var button = $(this);
        var date_added = $(this).closest('tr').attr('date_added');
        var account_name = $(this).closest('tr').find('[field="account_name"]').val();
        var account_number = $(this).closest('tr').find('[field="account_number"]').val();
        swal({
            title: "Are you sure?",
            text: "this account will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAccountDetails(client_type == 'client' ? client_id : client_joint_id,client_type,date_added,account_name,account_number);
            }
        });
    });

    function updateAccountDetails(id,type,date_added,account_name,account_number) {
        $.post('<?php echo base_url('admin/clients/updateAccountDetails') ?>',{id,type,date_added,account_name,account_number}, function(data){
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_equifax').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_experian').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_transunion').empty();
                if (client_type == 'joint') {
                    getResultsTracker(client_joint_id,client_type);
                } else {
                    getResultsTracker(client_id,client_type);
                }
                // console.log(data);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function deleteAccountDetails(id,type,date_added,account_name,account_number) {
        $.post('<?php echo base_url('admin/clients/deleteAccountDetails') ?>',{id,type,date_added,account_name,account_number}, function(data){
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_equifax').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_experian').empty();
                $('.customvtabDisputes').find('#custom_tab_content_disputes').find('#section_content_transunion').empty();
                if (client_type == 'joint') {
                    getResultsTracker(client_joint_id,client_type);
                } else {
                    getResultsTracker(client_id,client_type);
                }
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getCreditorsAccountInit(data) {
        var customvtabCreditors = $('.customvtabCreditors');
        var tabs_vertical = customvtabCreditors.find('.tabs-vertical-creditors');
        tabs_vertical.empty();


        $.each(data,function(key,value){
            var newLi = '<li  class="nav-item" style="line-height: 100%">\
                            <a class="nav-link customNavLink" data-toggle="tab" date_added="'+value.date_added+'" href="#an_'+value.account_number+'" role="tab">\
                                <span class="hidden-sm-up">\
                                    <i class="mdi mdi-account-card-details"></i>\
                                </span> \
                                <span class="hidden-xs-down" >\
                                    <span field="account_name" style="font-size: 60%">'+value.account_name+'</span>\
                                    <br>\
                                    <span field="account_number" style="font-size: 80%">'+value.account_number+'</span>\
                                </span>\
                            </a>\
                        </li>';
            tabs_vertical.append(newLi);
        });
    }
    function getClientGameplanInit(data){
        $.each(data,function(key,value){
            $('#broker_gameplan').val(value.broker);
            $('#creditlynx_gameplan').val(value.credit_lynx);
        });

        $('#Credit_Card_Needed').attr('checked',false);
        $('#Authorized_User').attr('checked',false);
        $('#Recent_Repossession').attr('checked',false);
        $('#High_Credit_Utilization').attr('checked',false);
        $('#Recent_Late').attr('checked',false);
        $('#Recent_Judgment').attr('checked',false);
        $('#New_Inquiries').attr('checked',false);
        $('#Recent_Charge_Off').attr('checked',false);
        $('#Recent_Moved').attr('checked',false);
        $.each(data, function(key, value){
            if (value.Credit_Card_Needed == '1') {
                $('#Credit_Card_Needed').attr('checked',(value.Credit_Card_Needed));
            }
            if (value.Authorized_User == '1') {
                $('#Authorized_User').attr('checked',(value.Authorized_User));
            }
            if (value.Recent_Repossession == '1') {
                $('#Recent_Repossession').attr('checked',(value.Recent_Repossession));
            }
            if (value.High_Credit_Utilization == '1') {
                $('#High_Credit_Utilization').attr('checked',(value.High_Credit_Utilization));
            }
            if (value.Recent_Late == '1') {
                $('#Recent_Late').attr('checked',(value.Recent_Late));
            }
            if (value.Recent_Judgment == '1') {
                $('#Recent_Judgment').attr('checked',(value.Recent_Judgment));
            }
            if (value.New_Inquiries == '1') {
                $('#New_Inquiries').attr('checked',(value.New_Inquiries));
            }
            if (value.Recent_Charge_Off == '1') {
                $('#Recent_Charge_Off').attr('checked',(value.Recent_Charge_Off));
            }
            if (value.Recent_Moved == '1') {
                $('#Recent_Moved').attr('checked',(value.Recent_Moved));
            }
        });
    }
    function getTasksInit(data) {
        $('#task_list').empty();
        $.each(data,function(key,value){
            var task_active = value.task_active;
            if (task_active == '1') {
                task_active = '<button class="task_update_button btn btn-default waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
            } else {
                task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
            }
            var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
            var newTask = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.task_id+'">\
                                <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                <div class="sl-right">\
                                    <div><a class="link">'+value.sender_name+'</a><br><a style="cursor: pointer" class="sl-date link editTaskDate">'+moment(value.task_date).format('lll')+'</a>\
                                        <div class="input-group">\
                                            <blockquote class="editTaskTask form-control" style="font-size: 70%;cursor: pointer">'+value.task+'</blockquote>\
                                            <div class="input-group-append">\
                                                '+task_active+'\
                                            </div>\
                                            <div class="input-group-append">\
                                                '+delete_button+'\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                            <hr>';
            $('#task_list').append(newTask);
        });

    }
    function getNotesInit(data) {
        $('#note_list').empty();
        $('#note_list_sticky').empty();
        $.each(data,function(key,value){
            var note_sticky = value.note_sticky;
            if (note_sticky == '1') {
                note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
            } else {
                note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
            }
            var delete_button = '';
            if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                delete_button = '<div class="input-group-append">\
                                    <button class="task_update_button btn btn-default waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                </div>';
            }
            var newNote = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.note_id+'">\
                                <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                <div class="sl-right">\
                                    <div><a >'+value.sender_name+'</a><br>\
                                    <a href="#" class="editNoteDate sl-date">'+moment(value.note_date).format('lll')+'</a>\
                                    <a href="#" class="deleteNote text-danger pull-right">Delete <i class="fas fa-sm fa-trash"></i></a>\
                                        <div class="input-group">\
                                            <blockquote style="cursor:pointer" class="form-control editNoteNote">'+value.note+'</blockquote>\
                                            <div class="input-group-append">\
                                                '+note_sticky+'\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                            <hr>';
            if (value.note_sticky == '1') {
                $('#note_list_sticky').append(newNote);
            } else {
                $('#note_list').append(newNote);
            }
        });
    }
    function getAlertsInit(data) {
        $('#alert_list').empty();
        $.each(data,function(key,value){
            var img = '';
            if (value.sender != '') {
                img = '<img width="30px" src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="" class="img-circle">';
            }

            var alert_class = '';
            if ($('.joint_container').hasClass('hide') == false) {
                alert_class = 'link changeAlertClientType';
            }
            var alert_notes = (value.alert_notes).replace(/\\/g,'');
            var client_name = (value.client_name).replace(/(\\|\/)/g,'');
            var newAlert = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.alert_id+'">\
                                <div class="sl-right">\
                                    <div>\
                                        <div class="input-group" style="margin-top: 5px">\
                                            <blockquote style="line-height: 100%;" class="form-control">\
                                                <span class="sl-date pull-left"><a class="deleteAlert" href="#"><i class="fas fa-trash text-danger"></i></a></span>\
                                                <span class="sl-date pull-right"><a class="editAlertDate" href="#">'+moment(value.alert_date).format('ll')+'</a></span><br>\
                                                <span style="line-height: 100%;font-size: 80%"><a class="'+alert_class+'">'+client_name+'</a>: <a class="editAlertSubject link" style="cursor:pointer;">'+value.alert_subject+'</a></span><br><br>\
                                                <p  style="line-height: 100%;font-size:70%;text-align: justify">Notes: <span class="editAlertNotes" style="cursor:pointer">'+alert_notes+'</span></p>\
                                            </blockquote>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                            <hr>';
            $('#alert_list').append(newAlert);
        });
    }
    function getClientCreditScoresInit(data) {
        $('#equifax_points').val('0');
        $('#experian_points').val('0');
        $('#transunion_points').val('0');
        $('#idealCreditScore').val('0');
        $('#eta_date').val('');
        if (data.length > 0) {
            $.each(data,function(key,value){
                var equifax = value.equifax_points;
                var experian = value.experian_points;
                var transunion = value.transunion_points;
                var credit_score = value.credit_score;
                var eta_date = value.eta_date;
                $('#equifax_points').val(equifax);
                $('#experian_points').val(experian);
                $('#transunion_points').val(transunion);
                $('.span_equifax_points').html(equifax);
                $('.span_experian_points').html(experian);
                $('.span_transunion_points').html(transunion);
                $('#idealCreditScore').val(credit_score);
                $('#eta_date').val(eta_date);
            });
        } else {

        }
    }
    function getClientEmailsInit(data) {
        $('#containerClientEmails').empty();
        var from = 'CreditLynx';
        var from_email = 'no-reply@creditlynx.com';
        var img = '<div>\
                        <a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/LynxLogo.png') ?>" alt="user" width="40" class="img-circle"></a>\
                    </div>';
        if (data.length != 0) {
            $.each(data,function(key,value){
                if (value.type != 'to_client') {
                    from = $('input[name="profile_name"]').val();
                    from_email = value.from;
                } else {
                    from = 'CreditLynx';
                    from_email = value.from;
                }
                var message = value.message != null ? value.message : '';
                var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                        <div value="'+value.subject+'" from="'+value.from+'" class="card-body emailHeader" style="cursor:pointer;padding-top:5px;padding-bottom:5px">\
                                            <h5 class="card-title m-b-0">'+value.subject+'<small class="pull-right">'+moment(value.date).format('lll')+'</small></h5>\
                                        </div>\
                                        <div id="email_container_'+value.email_id+'" class="card-body b-t hide animated fadeIn">\
                                            <div class="d-flex m-b-40">\
                                                '+img+'\
                                                <div class="p-l-10">\
                                                    <h4 class="m-b-0">'+from+'</h4>\
                                                    <small class="text-muted">From: '+from_email+'</small>\
                                                </div>\
                                            </div>\
                                            <p style="white-space: pre-wrap;">'+value.message+'</p>\
                                        </div>\
                                    </div>';
                $('#containerClientEmails').append(card);
                $('.emailHeader').on('click',function(){
                    var email_container = $(this).closest('.card').find('#email_container_'+value.email_id);
                    var subject = $(this).attr('value');
                    var from = $(this).attr('from');
                    $('#inputSubjectClientEmail').val(subject);
                    $('#inputFromClientEmail').val(from);
                    if (email_container.hasClass('hide')) {
                        email_container.removeClass('hide');
                    } else {
                        email_container.addClass('hide');
                    }
                });
            });
        } else {
            $('#containerClientEmails').append('No Emails Found');
        }
    }
    function getClientTextsInit(data) {
        $('#containerClientTexts').empty();
        var from_phone = 'no-reply@creditlynx.com';
        if (data.length != 0) {

            $.each(data,function(key,value){
                // console.log(value.type);
                if (value.type == 'from_client') {
                    from_phone = $('input[name="profile_name"]').val();
                } else {
                    from_phone = 'CreditLynx';
                }

                var text_message = value.text_message != null ? value.text_message : '';
                text_message = text_message.replace('style="font-family:verdana,sans-serif;color:#3333ff"','');
                text_message = text_message.replace(/<br>/g,'');
                if (value.title == 'mms') {
                    text_message = '<blockquote style="white-space: pre-wrap"><a target="_blank" href="'+$.trim(text_message)+'">Attachment</a></blockquote>';
                } else {
                    text_message = '<blockquote style="white-space: pre-wrap">'+$.trim(text_message)+'</blockquote>';
                }

                var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                        <div class="card-body emailHeader" style="padding-top:5px;padding-bottom:5px">\
                                            <h5 class="card-title m-b-0">From: '+from_phone+'<small class="pull-right">'+moment(value.date).format('lll')+'</small> '+text_message+'</h5>\
                                        </div>\
                                    </div>';
                $('#containerClientTexts').append(card);

            });
        } else {
            $('#containerClientTexts').append('No Texts Found');
        }
    }
    function getGameplanTimelineInit(data) {
        if (data.length > 0) {
            $.each(data, function(key, value){
                var enrolled = value['enrolled'];
                var dispute = value['dispute'];
                var result = value['result'];
                var repeat = value['repeat'];
                var complete = value['complete'];
                $('#Enrolled').val(enrolled);
                $('#Dispute').val(dispute);
                $('#Result').val(result);
                $('#Repeat').val(repeat);
                $('#Complete').val(complete);
            });
        } else {
            $('#Enrolled').val('');
            $('#Dispute').val('');
            $('#Result').val('');
            $('#Repeat').val('');
            $('#Complete').val('');
        }
    }
    function getFixedDeletedNotReportingInit(data) {
        /*<!-- ============================================================== -->*/
        /*<!-- Pie Chart -->*/
        /*<!-- ============================================================== -->*/
        // var chartLabelTotalFixed = $('.chartLabelTotalFixed');
        var chartLabelEquifax = $('.chartLabelEquifax');
        var chartLabelExperian = $('.chartLabelExperian');
        var chartLabelTransUnion = $('.chartLabelTransUnion');

        // chartLabelTotalFixed.empty();
        chartLabelEquifax.empty();
        chartLabelExperian.empty();
        chartLabelTransUnion.empty();


        var chartHeaderEquifax = $('.chartHeaderEquifax');
        var chartHeaderExperian = $('.chartHeaderExperian');
        var chartHeaderTransUnion = $('.chartHeaderTransUnion');

        var all_account_label = $('.all-account-label');

        var data_equifax = [];
        var data_experian = [];
        var data_transunion = [];
        if (data.length != 0) {
            all_account_label.empty();
            var total_accounts = data['total_accounts'];
            var fixed_accounts = data['fixed_accounts'];

            var total_equifax = data['total_equifax'];
            var fixed_equifax = data['fixed_equifax'];

            var total_experian = data['total_experian'];
            var fixed_experian = data['fixed_experian'];

            var total_transunion = data['total_transunion'];
            var fixed_transunion = data['fixed_transunion'];






            // ["#fb3a3a","#3F51B5","#26c6da"]

            if (total_accounts != 0) {
                chartHeaderEquifax.removeClass('hide');
                chartHeaderExperian.removeClass('hide');
                chartHeaderTransUnion.removeClass('hide');
                // // CHART FIXED

                // CHART Equifax

                    if (total_equifax != 0) {
                        var total_average_equifax = ((fixed_equifax/total_equifax) * 100).toFixed(2);
                        chartLabelEquifax.html('Deleted/Fixed: '+fixed_equifax+' out of '+total_equifax+' ('+total_average_equifax+'%)');
                        if (fixed_equifax == 0) {
                            data_equifax = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];
                        } else {
                            data_equifax = [
                                { value: fixed_equifax, name: 'Deleted/Fixed' },
                                { value: (total_equifax - fixed_equifax), name: 'Not Deleted/Fixed' }
                            ];
                        }

                        if (total_average_equifax > 0) {
                            $('.spanEquifaxPercent').html(total_average_equifax+'%');
                        } else {
                            $('.spanEquifaxPercent').html('-');
                            $('.spanEquifaxPercent').css('left','46%');
                        }

                    } else {
                        chartLabelEquifax.html('No Equifax Account');
                        data_equifax = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];
                    }



                // CHART Experian

                    if (total_experian != 0) {
                        var total_average_experian = ((fixed_experian/total_experian) * 100).toFixed(2);
                        chartLabelExperian.html('Deleted/Fixed: '+fixed_experian+' out of '+total_experian+' ('+total_average_experian+'%)');
                        if (fixed_experian == 0) {
                            data_experian = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];
                        } else {
                            data_experian = [
                                { value: fixed_experian, name: 'Deleted/Fixed' },
                                { value: (total_experian - fixed_experian), name: 'Not Deleted/Fixed' }
                            ];
                        }

                        if (total_average_experian > 0) {
                            $('.spanExperianPercent').html(total_average_experian+'%');
                        } else {
                            $('.spanExperianPercent').html('-');
                            $('.spanExperianPercent').css('left','46%');
                        }
                    } else {
                        chartLabelExperian.html('No Experian Account');
                        data_experian = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];
                    }



                // CHART TransUnion

                    if (total_transunion != 0) {
                        var total_average_transunion = ((fixed_transunion/total_transunion) * 100).toFixed(2);
                        chartLabelTransUnion.html('Deleted/Fixed: '+fixed_transunion+' out of '+total_transunion+' ('+total_average_transunion+'%)');
                        if (fixed_transunion == 0) {
                            data_transunion = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];
                        } else {
                            data_transunion = [
                                { value: fixed_transunion, name: 'Deleted/Fixed' },
                                { value: (total_transunion - fixed_transunion), name: 'Not Deleted/Fixed' }
                            ];
                        }

                        if (total_average_transunion > 0) {
                            $('.spanTransUnionPercent').html(total_average_transunion+'%');
                        } else {
                            $('.spanTransUnionPercent').html('-');
                            $('.spanTransUnionPercent').css('left','46%');
                        }
                    } else {
                        chartLabelTransUnion.html('No TransUnion Account');
                        data_transunion = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];
                    }







            } else {
                // chartLabelTotalFixed.html('No Accounts found!');
                all_account_label.html('No Accounts Found');
                chartLabelEquifax.html('No Equifax Account');
                data_equifax = [
                    { value: 0, name: 'Deleted/Fixed' },
                    { value: 1, name: 'Not Deleted/Fixed' }
                ];

                chartLabelExperian.html('No Experian Account');
                data_experian = [
                    { value: 0, name: 'Deleted/Fixed' },
                    { value: 1, name: 'Not Deleted/Fixed' }
                ];

                chartLabelTransUnion.html('No TransUnion Account');
                data_transunion = [
                    { value: 0, name: 'Deleted/Fixed' },
                    { value: 1, name: 'Not Deleted/Fixed' }
                ];
            }
        } else {
            all_account_label.html('No Accounts Found');
            chartLabelEquifax.html('No Equifax Account');
            data_equifax = [
                { value: 0, name: 'Deleted/Fixed' },
                { value: 1, name: 'Not Deleted/Fixed' }
            ];

            chartLabelExperian.html('No Experian Account');
            data_experian = [
                { value: 0, name: 'Deleted/Fixed' },
                { value: 1, name: 'Not Deleted/Fixed' }
            ];

            chartLabelTransUnion.html('No TransUnion Account');
            data_transunion = [
                { value: 0, name: 'Deleted/Fixed' },
                { value: 1, name: 'Not Deleted/Fixed' }
            ];
        }

        callChartEquifaxPercent(data_equifax);
        callChartExperianPercent(data_experian);
        callChartTransUnionPercent(data_transunion);
    }
    function getTableByAccountInit(data) {
        // console.log(data);
        tableByAccount.clear().draw();
        var total_accounts = 0;
        var total_fixed = 0;
        if (data.length != 0) {
            $.each(data,function(key,value){
                var account_name = value.account_name;
                var account_number = value.account_number;
                var account_type = value.account_type;
                var bureaus = value.bureaus;
                var account_status = value.account_status;

                bureaus = bureaus.split(',');
                account_status = account_status.split(',');

                total_accounts += bureaus.length;

                var Equifax = '-'
                var Experian = '-';
                var TransUnion = '-';
                $.each(bureaus,function(key,value){
                    var bg_color = '';
                    if (account_status[key] == 'Fixed' || account_status[key] == 'Deleted' || account_status[key] == 'Not Reporting') {
                        account_status[key] = '<div class="labelRemoved" >Removed</div>';
                        total_fixed++;
                    } else {
                        // account_status[key] = 'In Process';
                    }
                    if (value == 'Equifax') {
                        Equifax = account_status[key];
                    }
                    if (value == 'Experian') {
                        Experian = account_status[key];
                    }
                    if (value == 'TransUnion') {
                        TransUnion = account_status[key];
                    }
                });



                tableByAccount.row.add([account_name,account_number,account_type,Equifax,Experian,TransUnion]);
                tableByAccount.draw(false);

            });

            $('.labelRemoved').closest('td').addClass('border-green');

            var improvement_all = ((total_fixed/total_accounts) * 100).toFixed(0);
            $('.improvement_all').html(improvement_all+'%');
            $('#improvement_all_progressbar').css('width',improvement_all+'%');
        }
    }
    function getTableByBureausInit(data) {
        tableByBureauEquifax.clear().draw();
        tableByBureauExperian.clear().draw();
        tableByBureauTransUnion.clear().draw();

        var total_equifax = 0;
        var fixed_equifax = 0;
        var total_experian = 0;
        var fixed_experian = 0;
        var total_transunion = 0;
        var fixed_transunion = 0;
        var balance_equifax = 0;
        var balance_experian = 0;
        var balance_transunion = 0;

        var fixed_rnd1 = 0;
        var fixed_rnd2 = 0;
        var fixed_rnd3 = 0;
        var fixed_rnd4 = 0;
        var fixed_rnd5 = 0;
        var fixed_rnd6 = 0;
        var fixed_rnd7 = 0;
        var fixed_rnd8 = 0;


        if (data.length != 0) {
            $.each(data,function(key,value){
                var bureaus = value.bureaus;
                var status = 'In Process';
                if (bureaus == 'Equifax') {
                    total_equifax++;
                    if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                        status = 'Removed';
                        var newRow = tableByBureauEquifax.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                        $(newRow).addClass('table-success');
                        fixed_equifax++;
                        if (value.amount != '') {
                            var balance = ((value.amount).replace('$','')).replace(',',''); 
                            balance = parseFloat(balance);
                            balance_equifax += balance != 0 ? balance : 0; 
                        }
                            

                        $('.tableByAccountRound'+value.last_round+' tbody').append(
                                                        '<tr>\
                                                            <td style="color: #fc4b6c">Equifax</td>\
                                                            <td>'+value.account_name+'</td>\
                                                            <td>'+value.account_number+'</td>\
                                                            <td>'+value.account_type+'</td>\
                                                            <td>'+value.account_status+'</td>\
                                                        </tr>');
                         
                        fixed_rnd1 = value.last_round == 1 ? fixed_rnd1 + 1 : fixed_rnd1;
                        fixed_rnd2 = value.last_round == 2 ? fixed_rnd2 + 1 : fixed_rnd2;
                        fixed_rnd3 = value.last_round == 3 ? fixed_rnd3 + 1 : fixed_rnd3;
                        fixed_rnd4 = value.last_round == 4 ? fixed_rnd4 + 1 : fixed_rnd4;
                        fixed_rnd5 = value.last_round == 5 ? fixed_rnd5 + 1 : fixed_rnd5;
                        fixed_rnd6 = value.last_round == 6 ? fixed_rnd6 + 1 : fixed_rnd6;
                        fixed_rnd7 = value.last_round == 7 ? fixed_rnd7 + 1 : fixed_rnd7;
                        fixed_rnd8 = value.last_round == 8 ? fixed_rnd8 + 1 : fixed_rnd8;

                    } else {
                        status = value.account_status;
                        tableByBureauEquifax.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                    }
                }
                if (bureaus == 'Experian') {
                    total_experian++;
                    if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                        status = 'Removed';
                        var newRow = tableByBureauExperian.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                        $(newRow).addClass('table-success');
                        fixed_experian++;
                        if (value.amount != '') {
                            var balance = ((value.amount).replace('$','')).replace(',','');
                            balance = parseFloat(balance);
                            balance_experian += balance;
                        }
                           

                        $('.tableByAccountRound'+value.last_round+' tbody').append(
                                                        '<tr>\
                                                            <td style="color: #7460ee">Experian</td>\
                                                            <td>'+value.account_name+'</td>\
                                                            <td>'+value.account_number+'</td>\
                                                            <td>'+value.account_type+'</td>\
                                                            <td>'+value.account_status+'</td>\
                                                        </tr>'); 

                        fixed_rnd1 = value.last_round == 1 ? fixed_rnd1 + 1 : fixed_rnd1;
                        fixed_rnd2 = value.last_round == 2 ? fixed_rnd2 + 1 : fixed_rnd2;
                        fixed_rnd3 = value.last_round == 3 ? fixed_rnd3 + 1 : fixed_rnd3;
                        fixed_rnd4 = value.last_round == 4 ? fixed_rnd4 + 1 : fixed_rnd4;
                        fixed_rnd5 = value.last_round == 5 ? fixed_rnd5 + 1 : fixed_rnd5;
                        fixed_rnd6 = value.last_round == 6 ? fixed_rnd6 + 1 : fixed_rnd6;
                        fixed_rnd7 = value.last_round == 7 ? fixed_rnd7 + 1 : fixed_rnd7;
                        fixed_rnd8 = value.last_round == 8 ? fixed_rnd8 + 1 : fixed_rnd8;
                    } else {
                        status = value.account_status;
                        tableByBureauExperian.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                    }
                }
                if (bureaus == 'TransUnion') {
                    total_transunion++;
                    if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                        status = 'Removed';
                        var newRow = tableByBureauTransUnion.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                        $(newRow).addClass('table-success');
                        fixed_transunion++;
                        if (value.amount != '') {
                            var balance = ((value.amount).replace('$','')).replace(',','');
                            balance = parseFloat(balance);
                            balance_transunion += balance;
                        }
                            

                        $('.tableByAccountRound'+value.last_round+' tbody').append(
                                                        '<tr>\
                                                            <td style="color: #26c6da">TransUnion</td>\
                                                            <td>'+value.account_name+'</td>\
                                                            <td>'+value.account_number+'</td>\
                                                            <td>'+value.account_type+'</td>\
                                                            <td>'+value.account_status+'</td>\
                                                        </tr>');
                        console.log(value.last_round);
                        fixed_rnd1 = value.last_round == 1 ? fixed_rnd1 + 1 : fixed_rnd1;
                        fixed_rnd2 = value.last_round == 2 ? fixed_rnd2 + 1 : fixed_rnd2;
                        fixed_rnd3 = value.last_round == 3 ? fixed_rnd3 + 1 : fixed_rnd3;
                        fixed_rnd4 = value.last_round == 4 ? fixed_rnd4 + 1 : fixed_rnd4;
                        fixed_rnd5 = value.last_round == 5 ? fixed_rnd5 + 1 : fixed_rnd5;
                        fixed_rnd6 = value.last_round == 6 ? fixed_rnd6 + 1 : fixed_rnd6;
                        fixed_rnd7 = value.last_round == 7 ? fixed_rnd7 + 1 : fixed_rnd7;
                        fixed_rnd8 = value.last_round == 8 ? fixed_rnd8 + 1 : fixed_rnd8;

                    } else {
                        status = value.account_status;
                        tableByBureauTransUnion.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                    }
                }
            });


            $('a[href="#tab_by_bureau_round_1"]').html('Rnd 1 ('+fixed_rnd1+')');
            $('a[href="#tab_by_bureau_round_2"]').html('Rnd 2 ('+fixed_rnd2+')');
            $('a[href="#tab_by_bureau_round_3"]').html('Rnd 3 ('+fixed_rnd3+')');
            $('a[href="#tab_by_bureau_round_4"]').html('Rnd 4 ('+fixed_rnd4+')');
            $('a[href="#tab_by_bureau_round_5"]').html('Rnd 5 ('+fixed_rnd5+')');
            $('a[href="#tab_by_bureau_round_6"]').html('Rnd 6 ('+fixed_rnd6+')');
            $('a[href="#tab_by_bureau_round_7"]').html('Rnd 7 ('+fixed_rnd7+')');
            $('a[href="#tab_by_bureau_round_8"]').html('Rnd 8 ('+fixed_rnd8+')');

            $('.chartAmountEquifax').html('$'+(balance_equifax).toFixed(2));
            $('.chartAmountExperian').html('$'+(balance_experian).toFixed(2));
            $('.chartAmountTransUnion').html('$'+(balance_transunion).toFixed(2));

            $('.improvement_equifax').html(((fixed_equifax / total_equifax) * 100).toFixed(0) + '%');
            $('#improvement_equifax_progressbar').css('width',((fixed_equifax / total_equifax) * 100).toFixed(0) + '%');
            $('.improvement_experian').html(((fixed_experian / total_experian) * 100).toFixed(0) + '%');
            $('#improvement_experian_progressbar').css('width',((fixed_experian / total_experian) * 100).toFixed(0) + '%');
            $('.improvement_transunion').html(((fixed_transunion / total_transunion) * 100).toFixed(0) + '%');
            $('#improvement_transunion_progressbar').css('width',((fixed_transunion / total_transunion) * 100).toFixed(0) + '%');
        }
    }
    function getTableSupportTicketsInit(data) { 
        var tbodySupportTicketListRight = $('#tbodySupportTicketListRight');
        tbodySupportTicketListRight.empty();
        var total_tickets = 0;
        var total_responded = 0;
        var total_resolved = 0;
        var total_pending = 0;
        if (data.length != 0) {
            $.each(data,function(key,value){
                total_tickets++;
                var label_color = '';
                if (value.status == 'Pending') {
                    label_color = 'label-danger';
                    total_pending++;
                }
                if (value.status == 'Responded') {
                    total_responded++;
                    label_color = 'label-primary';
                }
                if (value.status == 'Resolved') {
                    total_resolved++;
                    label_color = 'label-success';
                }

                                    // <td>\
                                    //     <button type="button" class="btnDeleteSupportTicket btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>\
                                    // </td>\
                var newTicket = '<tr value="'+value.support_id+'">\
                                    <td><a href="#" class="supportTicket">'+value.subject+'</a></td>\
                                    <td><span class="label '+label_color+'">'+value.status+'</span> </td>\
                                    <td>'+value.assigned_to+'</td>\
                                    <td>'+moment(value.support_date).format('MM/DD/YYY h:mm:ss a')+'</td>\
                                </tr>';
                tbodySupportTicketListRight.append(newTicket);
            });
        } else {
            var newTicket = '<tr >\
                                <td colspan="5">No support tickets found</td>\
                            </tr>';
                tbodySupportTicketListRight.append(newTicket);
        }

        if (total_pending != 0) {
            $('#notif_support_right').html('( '+total_pending+' )');
        } else {
            $('#notif_support_right').html('');
        }


        // $('#label_total_tickets').html(total_tickets);
        $('#label_responded_tickets_right').html(total_responded);
        $('#label_resolved_tickets_right').html(total_resolved);
        $('#label_pending_tickets_right').html(total_pending);
    }
    function getClientLetterAccountsInit(data) {
        // console.log(data);
        var select_client_accounts = $('#select_client_accounts');
        // select_client_accounts.select2('destroy');
        select_client_accounts.empty();
        select_client_accounts.append('<option value="">Select Account</option>');
        $.each(data,function(key,value){
            select_client_accounts.append('<option value="'+value.account_number+'">'+value.account_name + ' - '+value.account_number+'</option>');
        });
        select_client_accounts.chosen({width: "100%",height: "40px"});
        select_client_accounts.trigger("chosen:updated");
    }

    $('#note_list_sticky').on('click','.btnUpdateNoteSticky',function(){
        var button = $(this);
        var note_id = $(this).closest('.sl-item').attr('value');
        if (button.hasClass('text-warning')) {
            updateNote(button,note_id,0);
        } else {
            updateNote(button,note_id,1);
        }
    });
    $('#note_list').on('click','.btnUpdateNoteSticky',function(){
        var button = $(this);
        var note_id = $(this).closest('.sl-item').attr('value');
        if (button.hasClass('text-warning')) {
            updateNote(button,note_id,0);
        } else {
            updateNote(button,note_id,1);
        }
    });

    $('#task_list').on('click','.btnUpdateTaskDone',function(){
        var button = $(this);
        var task_id = $(this).closest('.sl-item').attr('value');
        updateTask(button,task_id,0);
    });
    $('#task_list').on('click','.btnUpdateTaskActive',function(){
        var button = $(this);
        var task_id = $(this).closest('.sl-item').attr('value');
        updateTask(button,task_id,1);
    });
    $('#task_list').on('click','.btnUpdateTaskDelete',function(){
        var button = $(this);
        var task_id = $(this).closest('.sl-item').attr('value');
        updateTask(button,task_id,2);
    });

    $('#pageTable').on('click','.goToJointProfile',function(e){
        e.preventDefault();
        newUser = false;
        client_joint_id = $(this).attr('value');
        client_type = 'joint';
        client_id = $(this).closest('tr').attr('id');
        var name = $(this).find('u').html();
        getProfileDetails(client_id,client_joint_id,client_type);
    });

    $('.leads_table').on('click', '.goToProfile', function(event) {
        event.preventDefault();  
        client_id = $(this).closest('tr').attr('id');
        client_type = 'client';
        var name = $(this).find('u').html();

        getProfileDetails(client_id,client_joint_id,client_type);
    });

   $('.leads_table').on('click', '.goToJointProfile', function(event) {
        event.preventDefault();   
        client_joint_id = $(this).attr('value');
        client_type = 'joint';
        client_id = $(this).closest('tr').attr('id');
        var name = $(this).find('u').html();
        getProfileDetails(client_id,client_joint_id,client_type);
    });

    $('#pageTable').on('click','.btnClientEmailTab',function(e){
        e.preventDefault();
        var tab_to_go = 'tab_emails';
        $('#searchClient').val(client_id+'_client');
        $('#searchClient').trigger('change');
    });

    $('#btnAlertResponder').on('click',function(e){
        e.preventDefault();
        if ($(this).html() == 'ON') {
            $(this).html('OFF');
            updateFields(client_type == 'client' ? 'clients' : 'client_joints',client_type == 'client' ? client_id : client_joint_id,'auto_alert',0,'Alert is now OFF!');
            $(this).addClass('text-danger');
            $(this).removeClass('text-success');
        } else {
            $(this).html('ON');
            updateFields(client_type == 'client' ? 'clients' : 'client_joints',client_type == 'client' ? client_id : client_joint_id,'auto_alert',1,'Alert is now ON! ');
            $(this).removeClass('text-danger');
            $(this).addClass('text-success');
        }

    });

    var tab_to_go = '<?php echo $this->input->get('go') ?>';
    $('#pageTable').on('click','.rowTools a',function(){
        newUser = false;
        client_id = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').find('.goToProfile').find('u').html();
        var tr = $(this).closest('tr');
        if ($(this).find('.fa-trash').length == 0) {


            var tab_pane = $(this).attr('value');
            tab_to_go = tab_pane;
            $('#searchClient').val(client_id+'_client');

            $('#searchClient').trigger('change');
            // $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('active');
            // $('#'+tab_pane).closest('.tab-content').find('.tab-pane').removeClass('show');
            // $('#'+tab_pane).addClass('active');
            // $('#'+tab_pane).addClass('show');

            // $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('active');
            // $('#'+tab_pane+'-header').closest('.nav-tabs').find('.nav-link').removeClass('show');

            // $('#'+tab_pane+'-header').addClass('active');
            // $('#'+tab_pane+'-header').addClass('show');
        } else {
            swal({
                title: "Are you sure?",
                text: 'Client '+name+" will be deleted!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false ,
                preConfirm: function() {
                    updateFields('clients',client_id,'client_status','Archived');
                    tr.remove();
                    swal("Success!", "Client "+name+' successfully deleted!', "success");
                }
            }).then(function(){

            });
        }
    });
    $('#btnNewUser').on('click',function(){
        newUser = true;
        $('input[name=username]').prop('readonly',false);
        $('.dropify-render').find('img').attr('src','../assets/images/users/admins/LynxLogo.png');
        var inputs =  $('.formAddUser input');
        $.each(inputs,function(key,input){
            $(input).closest('.form-group').removeClass('focused');
        });
        get_sales_pool();
    });

    function get_sales_pool() {
        var data =  {
                        table: 'employees',
                        action: 'get',
                        select: {
                                    0:'employee_id'
                                },
                        where:  {
                                    status:1,
                                    pool:1
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('clients/modelTable', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                // alert(val.employee_id);
                $('#sale_id').val(val.employee_id);
                $('#sale_id').trigger('change');
            });
        });
    }
    $('#username_client').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'client');
        }
    });
    $('#joint_username_client').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'joint');
        }
    });
    $('#username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'client');
        }
    });
    $('#joint_username').on('keyup',function(){
        var username = $(this);
        if (newUser) {
            checkIfUsernameExist(username,'joint');
        }
    });
    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,client_id,client_joint_id,client_type,task,task_date);
            } else {
                task_date.focus();
            }
        } else {
            task.focus();
        }
    });
    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });
    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var new_note_date = $('#new_note_date').val();
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,client_id,client_joint_id,client_type,note,note_sticky,new_note_date);
        } else {
            note.focus();
        }
    });
    $('#btnSaveNewCall').on('click',function(){
        var button = $(this);
        var call = $('#new_call');
        var new_call_date = $('#new_call_date').val();  
        if (call.val() != '') {
            saveCall(button,client_id,client_joint_id,client_type,call,new_call_date,call_template_type);
        } else {
            call.focus();
        }
    });
    $('#btnSaveNewAlert').on('click',function(){
        var button = $(this);
        var alert_subject = $('#new_alert_subject');
        var alert_date = $('#new_alert_date');
        var alert_notes = $('#new_alert_notes');
        if (alert_subject.val() != '') {
            if (alert_notes.val() != '') {
                saveAlert(button,client_id,client_joint_id,client_type,alert_subject,alert_date,alert_notes);
            } else {
                alert_notes.focus();
            }
        } else {
            alert_subject.focus();
        }
    });

    $('#toggleJointContentClient').on('change',function(){
        var joint_content = $('#joint_content_client');
        if (joint_content.hasClass('hide')) {
            joint_content.removeClass('hide');
        } else {
            joint_content.addClass('hide');
        }
    });
    $('#toggleJointContentLead').on('change',function(){
        var joint_content = $('#joint_content_lead');
        if (joint_content.hasClass('hide')) {
            joint_content.removeClass('hide');
        } else {
            joint_content.addClass('hide');
        }
    });




    $('#btnSaveNewAlertTemplate').on('click',function(){
        var template_shortname = $('#new_template_shortname');
        var template_subject = $('#new_template_subject');
        var template_notes = $('#new_template_notes');
        if (template_shortname.val() != '') {
            if (template_subject.val() != '') {
                if (template_notes.val() != '') {
                    saveNewTemplate(template_shortname,template_subject,template_notes);
                } else {
                    template_notes.val();
                }
            } else {
                template_subject.focus();
            }
        } else {
            template_shortname.focus();
        }

    });

    $('#btnOpenAlertTemplates').on('click',function(e){
        e.preventDefault();
        var btnOpenAlertTemplates = $(this);
        var alert_templates_container = $('#alert_templates_container');
        if (alert_templates_container.hasClass('hide')) {
            alert_templates_container.removeClass('hide');
            btnOpenAlertTemplates.html('hide');
        } else {
            alert_templates_container.addClass('hide');
            btnOpenAlertTemplates.html('show');
        }
    });

    $('#btnShowHideSendAlert').on('click',function(e){
        e.preventDefault();
        var icon = $(this).find('i');
        var containerSendAlert = $(this).closest('.card-body').find('#containerSendAlert');
        if (containerSendAlert.hasClass('hide')) {
            containerSendAlert.removeClass('hide');
            icon.removeClass('fa-angle-right');
            icon.addClass('fa-angle-down');
        } else {
            containerSendAlert.addClass('hide');
            icon.removeClass('fa-angle-down');
            icon.addClass('fa-angle-right');
        }
    });

    $('#btnShowHideNewAccount').on('click',function(e){
        e.preventDefault();
        var containerNewAccount = $(this).closest('.card-body').find('#containerNewAccount');
        if (containerNewAccount.hasClass('hide')) {
            containerNewAccount.removeClass('hide');
        } else {
            containerNewAccount.addClass('hide');
        }
    });

    $('#selectDisputeVerbiage').on('change',function(){
        var dispute_verbiage = $(this).val();
        $('#bank_dispute_verbiage').val(dispute_verbiage);
    });

    $(document).on('focus', '.select2', function (e) {
      if (e.originalEvent) {
        $(this).siblings('select').select2('open');
      }
    });




    $('#tableAccountTypes tbody').on('click','.btnEditAccountType',function(e){
        e.preventDefault();
        var button = $(this);
        var at_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var account_type = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveAccountType(at_id,account_type);
        }
    });

    $('#tableAccountTypes tbody').on('click','.btnDeleteAccountType',function(e){
        e.preventDefault();
        var button = $(this);
        var at_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" account type will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAccountType(at_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
    });

    $('#tableCreditorsAccountType tbody').on('click','.btnEditAccountType',function(e){
        e.preventDefault();
        var button = $(this);
        var creditors_account_type_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var account_type = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveCreditorsAccountType(creditors_account_type_id,account_type);
        }
    });
    $('#tableCreditorsRec tbody').on('click','.btnEditCredtorsRec',function(e){
        e.preventDefault();
        var button = $(this);
        var creditors_rec_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var creditors_rec = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveCreditorsRec(creditors_rec_id,creditors_rec);
        }
    });

    $('#tableCreditorsAccountType tbody').on('click','.btnDeleteAccountType',function(e){
        e.preventDefault();
        var button = $(this);
        var creditors_account_type_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" account type will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteCreditorsAccountType(creditors_account_type_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
    });

    $('#tableCreditorsRec tbody').on('click','.btnDeleteCredtorsRec',function(e){
        e.preventDefault();
        var button = $(this);
        var creditors_rec_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" recommendation will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteCreditorsRec(creditors_rec_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
    });

    $('#tableAccountStatuses tbody').on('click','.btnEditAccountStatus',function(e){
        e.preventDefault();
        var button = $(this);
        var as_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var account_status = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveAccountStatus(as_id,account_status);
        }
    });

    $('#tableAccountStatuses tbody').on('click','.btnDeleteAccountStatus',function(e){
        e.preventDefault();
        var button = $(this);
        var as_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" account status will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAccountStatus(as_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });

    });

    $('#tableAccountPaymentStatuses tbody').on('click','.btnEditAccountPaymentStatus',function(e){
        e.preventDefault();
        var button = $(this);
        var ps_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var payment_status = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveAccountPaymentStatus(ps_id,payment_status);
        }
    });

    $('#tableAccountPaymentStatuses tbody').on('click','.btnDeleteAccountPaymentStatus',function(e){
        e.preventDefault();
        var button = $(this);
        var ps_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" account status will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAccountPaymentStatus(ps_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });

    });

    $('#tableAccountComments tbody').on('click','.btnEditAccountComment',function(e){
        e.preventDefault();
        var button = $(this);
        var ac_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');

        if (button.find('i').hasClass('fa-edit')) {
            button.find('i').removeClass('fa-edit');
            button.find('i').addClass('fa-save');
            input.removeClass('b-0');
            input.attr('readonly',false);
        } else {
            button.find('i').addClass('fa-edit');
            button.find('i').removeClass('fa-save');
            var payment_status = input.val();
            input.addClass('b-0');
            input.attr('readonly',true);
            saveAccountComment(ac_id,payment_status);
        }
    });

    $('#tableAccountComments tbody').on('click','.btnDeleteAccountComment',function(e){
        e.preventDefault();
        var button = $(this);
        var ac_id = button.closest('tr').attr('id');
        var input = button.closest('tr').find('input');
        swal({
            title: "Are you sure?",
            text: input.val()+" account status will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteAccountComment(ac_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });

    });

    var tbl_dva_at_selected = '';
    var tbl_dva_rounds_selected = '';
    $('#tbl_dva_at').on('click','.btnGetTableRoundsByAccountType',function(e){
        e.preventDefault();
        tbl_dva_at_selected = $(this).attr('account_type');
        $('#tbl_dva_at').addClass('hide');
        $('#tbl_dva_rounds').removeClass('hide');
        $('.btnShowATTable').closest('li').removeClass('hide');
        $('.btnShowRoundsTable').closest('li').removeClass('hide');
        $('.btnShowDVTable').closest('li').addClass('hide');
        $('.btnShowRoundsTable').html(tbl_dva_at_selected);
    });
    $('#tbl_dva_rounds').on('click','.btnGetTableDPByRounds',function(e){
        e.preventDefault();
        tbl_dva_rounds_selected = $(this).closest('tr').attr('round');
        $('#tbl_dva_rounds').addClass('hide');
        $('#tbl_dva_dv').removeClass('hide');
        $('.btnShowATTable').closest('li').removeClass('hide');
        $('.btnShowRoundsTable').closest('li').removeClass('hide');
        $('.btnShowDVTable').closest('li').removeClass('hide');
        $('.btnShowDVTable').html('Round '+tbl_dva_rounds_selected);
        $('.containerDisputeVerbiages').removeClass('hide');
        getDisputeVerbiagesByATandRound();
    });

    $('.btnSaveDisputeVerbiagesAssignment').on('click',function(){
        saveDisputeVerbiagesAssignment() ;
    });

    $('#tbl_dva_dv').on('click','.btnDeleteDisputeVerbiageFromAssignment',function(){
        var cdva_id = $(this).closest('tr').attr("cdva_id");
        var account_type = $('.btnShowRoundsTable').html();
        var round = $('.btnShowDVTable').html();
        swal({
            title: "Are you sure?",
            text: "Verbiage will be deleted on "+account_type+' '+round,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                deleteDisputeVerbiageAssignment(cdva_id);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
    });

    $('#btnClearAccountType').on('click',function(){
        at_id = '';
        $('#new_account_type').val('');
    });

    $('.btnShowATTable').on('click',function(e){
        e.preventDefault();
        $('.btnShowRoundsTable').closest('li').addClass('hide');
        $('.btnShowDVTable').closest('li').addClass('hide');
        $('#tbl_dva_at').removeClass('hide');
        $('#tbl_dva_rounds').addClass('hide');
        $('#tbl_dva_dv').addClass('hide');
        $('.containerDisputeVerbiages').addClass('hide');
    });
    $('.btnShowRoundsTable').on('click',function(e){
        e.preventDefault();
        $('.btnShowDVTable').closest('li').addClass('hide');
        $('#tbl_dva_at').addClass('hide');
        $('#tbl_dva_rounds').removeClass('hide');
        $('#tbl_dva_dv').addClass('hide');
        $('.containerDisputeVerbiages').addClass('hide');

    });
    $('#btnSaveAccountType').on('click',function(){
        var account_type = $('#new_account_type').val();
        saveAccountType('',account_type);
    });
    $('#btnSaveAccountStatus').on('click',function(){
        var account_status = $('#new_account_status').val();
        saveAccountStatus('',account_status);
    });
    $('#btnSaveAccountPaymentStatus').on('click',function(){
        var payment_status = $('#new_payment_status').val();
        saveAccountPaymentStatus('',payment_status);
    });
    $('#btnSaveAccountComment').on('click',function(){
        var comment = $('#new_comment').val();
        saveAccountComment('',comment);
    });


    $('#bank_account_type').on('change',function(){
        var account_type = $(this).val(); 
        getDisputeVerbiagesAccountTypesBy_account_type(account_type,false,false,1);
        $('#div_bank_amount').addClass('hide');
        $('#div_bank_credit_limit').addClass('hide');
        $('#div_bank_file_date').addClass('hide');
        $('#div_bank_status_date').addClass('hide');
        $('#div_bank_chapter').addClass('hide');
        $('#div_bank_correct_info').addClass('hide');
        $('#div_bank_inquiry_date ').addClass('hide');
        $('#div_bank_past_due').addClass('hide');
        $('#div_bank_original_creditor').removeClass('hide');
        $('#div_bank_date_opened').removeClass('hide');
        if (account_type == '120 Day Late' ||
            account_type == '180 Day Late' ||
            account_type == '150 Day Late' ||
            account_type == '30 Day Late' ||
            account_type == '60 Day Late' ||
            account_type == '90 Day Late' ||
            account_type == 'Charge-Off' ||
            account_type == 'Repossession' ||
            account_type == 'New 120 Day Late' ||
            account_type == 'New 180 Day Late' ||
            account_type == 'New 150 Day Late' ||
            account_type == 'New 30 Day Late' ||
            account_type == 'New 60 Day Late' ||
            account_type == 'New 90 Day Late' ||
            account_type == 'New Charge-Off' ||
            account_type == 'New Repossession' ||
            account_type == 'Foreclosure' ||
            account_type == 'Included In BK' ||
            account_type == 'Multiple Lates') {
            $('#div_bank_amount').removeClass('hide');
            $('#div_bank_past_due').removeClass('hide');
            $('#div_bank_credit_limit').removeClass('hide');
            $('#div_bank_original_creditor').addClass('hide');
        }
        if (account_type == 'Collection' ||
            account_type == 'New Collection') {
            $('#div_bank_past_due').removeClass('hide');
        }
        if (account_type == 'Inquiry' ||
            account_type == 'New Inquiry') {
            $('#div_bank_inquiry_date ').removeClass('hide');
        }
        if (account_type == 'Wrong Address' ||
            account_type == 'Wrong Social'  ||
            account_type == 'Wrong Name' ||
            account_type == 'Wrong Employer') {
            $('#div_bank_correct_info').removeClass('hide');
        }
        if (account_type == 'Bankruptcy' ||
            account_type == 'New Bankruptcy' ||
            account_type == 'Included In BK') {
            if (account_type != 'Included In BK') {
                $('#div_bank_file_date').removeClass('hide');
                $('#div_bank_status_date').removeClass('hide');
            }
            $('#div_bank_chapter').removeClass('hide');
        }
        if (account_type == 'Judgment' ||
            account_type == 'Tax Lien' ||
            account_type == 'New Judgment' ||
            account_type == 'New Tax Lien') {
            $('#div_bank_past_due').removeClass('hide');
            $('#div_bank_file_date').removeClass('hide'); 
        }


        if (account_type == 'Bankruptcy' ||
            account_type == 'New Bankruptcy' ||
            account_type == 'Inquiry' ||
            account_type == 'New Inquiry' ||
            account_type == 'Wrong Address' ||
            account_type == 'Wrong Social'  ||
            account_type == 'Wrong Name' ||
            account_type == 'Judgment' ||
            account_type == 'New Judgment' ||
            account_type == 'Wrong Employer') {
            $('#div_bank_date_opened').addClass('hide');
            $('#div_bank_original_creditor').addClass('hide');
        }
    });

    $('#checkbox_all').on('change',function(){
        var checkbox_all = $(this);
        if (this.checked) {
            checkbox_all.prop('checked',true);
            var checkbox_equifax = $('#checkbox_equifax').prop('checked',true);
            var checkbox_experian = $('#checkbox_experian').prop('checked',true);
            var checkbox_transunion = $('#checkbox_transunion').prop('checked',true);
        } else {
            checkbox_all.prop('checked',false);
            var checkbox_equifax = $('#checkbox_equifax').prop('checked',false);
            var checkbox_experian = $('#checkbox_experian').prop('checked',false);
            var checkbox_transunion = $('#checkbox_transunion').prop('checked',false);
        }
    });

    $('#btnSaveNewAccount').on('click',function(){
        var checkbox_equifax = $('#checkbox_equifax').is(":checked");
        var checkbox_experian = $('#checkbox_experian').is(":checked");
        var checkbox_transunion = $('#checkbox_transunion').is(":checked");
        var bank_account_name = $('#bank_account_name').val();
        var bank_account_number = $('#bank_account_number').val();
        var bank_account_status = $('#bank_account_status').val();
        var bank_account_type = $('#bank_account_type').val();
        var bank_dispute_verbiage = $('#bank_dispute_verbiage').val();
        var bank_amount = $('#bank_amount').val();
        var bank_past_due = $('#bank_past_due').val();
        var bank_credit_limit = $('#bank_credit_limit').val();
        var bank_file_date = $('#bank_file_date').val();
        var bank_chapter = $('#bank_chapter').val();
        var bank_correct_info = $('#bank_correct_info').val();
        var bank_inquiry_date  = $('#bank_inquiry_date').val();
        var bank_payment_status  = $('#bank_payment_status').val();
        var bank_comments  = $('#bank_comments').val();
        var bank_date_opened  = $('#bank_date_opened').val();
        var bank_original_creditor  = $('#bank_original_creditor').val();
        var bank_status_date  = $('#bank_status_date').val();
        var global_bureau_date  = $('#global_bureau_date').val();

        if (checkbox_equifax == true || checkbox_experian == true || checkbox_transunion == true) {

            if (bank_account_name != '') {
                if (bank_account_number != '') {
                    if (client_type == 'joint') {
                        saveNewAccount(client_joint_id,client_type,checkbox_equifax,checkbox_experian,checkbox_transunion,bank_account_status,bank_account_type,bank_account_name,bank_account_number,bank_amount,bank_dispute_verbiage,bank_past_due,bank_credit_limit,bank_file_date,bank_chapter,bank_correct_info,bank_inquiry_date,global_bureau_date,bank_payment_status,bank_comments,bank_date_opened,bank_original_creditor, bank_status_date);
                    } else {
                        saveNewAccount(client_id,client_type,checkbox_equifax,checkbox_experian,checkbox_transunion,bank_account_status,bank_account_type,bank_account_name,bank_account_number,bank_amount,bank_dispute_verbiage,bank_past_due,bank_credit_limit,bank_file_date,bank_chapter,bank_correct_info,bank_inquiry_date,global_bureau_date,bank_payment_status,bank_comments,bank_date_opened,bank_original_creditor, bank_status_date);
                    }
                } else {
                    if (( account_type == 'Inquiry' ||
                                                    account_type == 'New Inquiry' ||
                                                    account_type == 'Wrong Address' ||
                                                    account_type == 'Wrong Employer' ||
                                                    account_type == 'Wrong Name' ||
                                                    account_type == 'Wrong Social' )) {
                        if (client_type == 'joint') {
                            saveNewAccount(client_joint_id,client_type,checkbox_equifax,checkbox_experian,checkbox_transunion,bank_account_status,bank_account_type,bank_account_name,bank_account_number,bank_amount,bank_dispute_verbiage,bank_past_due,bank_credit_limit,bank_file_date,bank_chapter,bank_correct_info,bank_inquiry_date,global_bureau_date,bank_payment_status,bank_comments,bank_date_opened,bank_original_creditor, bank_status_date);
                        } else {
                            saveNewAccount(client_id,client_type,checkbox_equifax,checkbox_experian,checkbox_transunion,bank_account_status,bank_account_type,bank_account_name,bank_account_number,bank_amount,bank_dispute_verbiage,bank_past_due,bank_credit_limit,bank_file_date,bank_chapter,bank_correct_info,bank_inquiry_date,global_bureau_date,bank_payment_status,bank_comments,bank_date_opened,bank_original_creditor, bank_status_date);
                        }
                    } else {
                        $('#bank_account_number').focus();
                    }

                }
            } else {
                $('#bank_account_name').focus();
            }
        }


    });

    $('#pageTable').on('click','.changeClientCardSent',function(e){
        e.preventDefault();
        var btn = $(this);
        var client_id = $(this).attr('value');
        var value = '';
        if (btn.html() == 'No') {
            value = 'Yes';
        } else {
            value = 'No';
        }

        changeCardSent(btn,'clients',value,'client_id',client_id);
    });

    $('#pageTable').on('click','.changeJointCardSent',function(e){
        e.preventDefault();
        var btn = $(this);
        var client_joint_id = $(this).attr('value');
        var value = '';
        if (btn.html() == 'No') {
            value = 'Yes';
        } else {
            value = 'No';
        }

        changeCardSent(btn,'client_joints',value,'client_joint_id',client_joint_id);
    });

    $('#GamePlanForm').on('change','#eta_date,#equifax_points, #experian_points, #transunion_points, #idealCreditScore', function(e){
        var equifax_points = $('#equifax_points').val();
        var experian_points = $('#experian_points').val();
        var transunion_points = $('#transunion_points').val();
        var idealCreditScore = $('#idealCreditScore').val();
        var eta_date = $('#eta_date').val();
        // console.log(client_id);
        GamePlanCreditScore(eta_date,equifax_points, experian_points, transunion_points, idealCreditScore, client_id,client_joint_id,client_type);

    });

    $('.btn_get_defualt_timeline').on('click', function(event) {
        event.preventDefault();
        add_default_timeline_to_client();
    });

// EVENTS END

// FUNCTIONS START

    function get_comm_flow() {
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type;
        $.post('<?php echo base_url('admin/communication_flow/get_comm_flow') ?>', {id,type}, function(data, textStatus, xhr) {
            data = JSON.parse(data); 
            var timeline = $('#tab_conversation_history').find('.timeline');
            $('#tab_conversation_history').find('.nothing_found').addClass('hide');;
            timeline.empty();
            if (data.length == 0) { 
                $('#tab_conversation_history').find('.nothing_found').removeClass('hide');
            } else {
                data = data.reverse();
                $.each(data, function(index, val) { 
                    var li_class = val.from == 'to_client' ? 'timeline-inverted' : '';
                    var icon_style = val.convo_type == 'fa-sticky-note' ? '#008080' : val.convo_type == 'fa-envelope' ? '#727b84' : '#26c6da';
                    var animated_class = val.from == 'to_client' ? 'bounceInRight' : 'bounceInLeft'; 

                    var date = '<small><i class="far fa-clock"></i> '+moment(val.date).format('MM/DD/YYYY hh:mm:ss A')+'</small>' ;
                    var name = $('#tab_profile input[name="profile_name"]').val();
                    name = name.split(' ');
                    var first_name = '';
                    var last_name = name[name.length -1];
                    if (name.length > 2) {
                         name.splice(name.length -1, 1);
                         first_name = name.join(' ');
                    } else {
                        first_name = name[0];
                    }

                    var conv_name = val.from == 'to_client' ? 'You' : first_name;
                    
                    var subject = $.trim(val.subject) == 'mms' ? '<a target="_blank" href="'+$.trim(val.note)+'">Click here to preview</a>' : $.trim(val.subject);
                    var note = $.trim(val.subject) == 'mms' ? '<img width="200px" src="'+$.trim(val.note)+'"/>' : $.trim(val.note);

                    var note_note_class = val.id == 0 ? '' : 'editNoteNote';
                    var note_subject_class = val.id == 0 ? '' : '';

                    
                    $('#tab_conversation_history').find('.timeline').append('<li id="'+val.id+'" class="'+li_class+'"">\
                                    <div class="timeline-badge" style="background-color: '+icon_style+'"><i class="fas '+val.convo_type+'"></i></div>\
                                    <div class="timeline-panel animated '+animated_class+'">\
                                        <div class="timeline-heading">\
                                            <h5 class="timeline-title">'+conv_name+'<br>'+date+'</h5>\
                                        </div>\
                                        <div class="timeline-body b-t" style="font-size: 12px;">\
                                            <span class="">'+subject+'</span><br><span class="'+note_note_class+'">'+note+'</span>\
                                        </div>\
                                    </div>\
                                </li>'); 
                    $('#tab_conversation_history').find('.timeline').find('.gmail_quote').remove();
                    $('#tab_conversation_history').find('.timeline').find('.gmail_signature').remove();
                });
            }
        });
    }

    function get_auto_alert_schedule_clients(client_id) {
        $.post('<?php echo base_url('admin/alert_schedules/get_auto_alert_schedule_clients') ?>', {client_id}, function(data, textStatus, xhr) {
            data = JSON.parse(data); 
            console.log(data);
            $('#as_round_date').html('');
            $('#as_schedules').html('');
            if (data.length > 0) {
                $.each(data, function(index, val) {
                    $('#as_round_date').html(val.round_alert_date);
                    $('#as_schedules').html(val.message);
                });
                    
            }
                
        });
    };

    function get_auto_alert_schedule_joints(client_joint_id) {
        $.post('<?php echo base_url('admin/alert_schedules/get_auto_alert_schedule_joints') ?>', {client_joint_id}, function(data, textStatus, xhr) {
            data = JSON.parse(data); 
            console.log(data);
            $('#as_round_date').html('');
            $('#as_schedules').html('');
            if (data.length > 0) {
                $.each(data, function(index, val) {
                    $('#as_round_date').html(val.round_alert_date);
                    $('#as_schedules').html(val.message);
                });
                    
            }
                
        });
    }

    function add_default_timeline_to_client() {
        var status = $(global_converted == 1 ? '#profile_client_status' : '#profile_lead_status').val();
        var status_type = global_converted == 1 ? 'client' : 'lead';
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type;
        $.post('<?php echo base_url('admin/communication_flow/add_default_timeline_to_client') ?>', {id,type,status_type,status}, function(data, textStatus, xhr) {
            get_client_status_timelines();
        });
    }

     
    function get_client_status_timelines() {
        var status = $(global_converted == 1 ? '#profile_client_status' : '#profile_lead_status').val();
        var status_type = global_converted == 1 ? 'client' : 'lead';
        var data =  {
                        table: 'client_status_timelines',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
                                    'status':status,
                                    'status_type':status_type,
                                    'id': client_type == 'client' ? client_id : client_joint_id,
                                    'type': client_type
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    }; 
        $.post('<?php echo base_url('admin/communication_flow/modelTable') ?>', data, function(data, textStatus, xhr) {
            // console.log(data);
            data = JSON.parse(data); 
            // console.log(data);
            var timeline = $('#tab_campaign').find('.timeline');
            timeline.empty();
            timeline.append('<li class="">\
                                <div class="timeline-badge success"><a href="#" id="btn_add_st"><i class="fas fa-plus" style="color: white"></i></a></div> \
                                </li>');
            if (data.length == 0) { 
                // add_client_status_timelines(1);
            } else {
                data = data.reverse();
                $.each(data, function(index, val) {
                    var hide_body = val.notif_type == '' ? 'hide' : '';
                    var li_class = val.day == 1 ? '' : (val.day) % 2 == 0  ? 'timeline-inverted' : '';
                    var animated_class = val.day == 1 ? 'bounceInLeft' : (val.day) % 2 == 0  ? 'bounceInRight' : 'bounceInLeft';
                    var footer_class = (val.day) % 2 == 0  ? 'text-right' : '';
                     
                    $('#tab_campaign').find('.timeline').prepend('<li class="'+li_class+'" st_id="'+val.st_id+'">\
                                    <div class="timeline-badge success">'+val.day+'</div>\
                                    <div class="timeline-panel animated '+animated_class+'">\
                                        <div class="timeline-heading '+hide_body+'">\
                                            <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject" value="'+val.subject+'"></h4>\
                                        </div>\
                                        <div class="timeline-body '+hide_body+'">\
                                            <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note">'+val.note+'</textarea>\
                                        </div>\
                                        <div class="timeline-footer '+footer_class+'">\
                                            <hr>  \
                                                <button type="button" class="btn btn-default btn-sm " notif_type="sms"> \
                                                    <i class="fas fa-comments"></i>\
                                                    Sms\
                                                </button>\
                                                <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
                                                    <i class="fas fa-envelope"></i>\
                                                    Email\
                                                </button> \
                                                <button type="button" class="btn btn-default btn-sm " notif_type="both"> \
                                                    <i class="fa fa-reply-all"></i>\
                                                    Both\
                                                </button> \
                                        </div>\
                                    </div>\
                                </li>');
                    var notif_type_button = $('#tab_campaign').find('.timeline').find('[st_id="'+val.st_id+'"]').find('[notif_type="'+val.notif_type+'"]');
                    notif_type_button.removeClass('btn-default');
                    notif_type_button.addClass('btn-success');
                    notif_type_button.append('<i class="fas fa-check"></i>');
                });
            }
        });
    }

    function add_client_status_timelines(day) {
        var status = $(global_converted == 1 ? '#profile_client_status' : '#profile_lead_status').val();
        var status_type = global_converted == 1 ? 'client' : 'lead';
        var data =  {
                        table: 'client_status_timelines',
                        pk: '',
                        action: 'save',
                        fields: {
                                    "status":status,
                                    'status_type':status_type,
                                    'day':day,
                                    'id': client_type == 'client' ? client_id : client_joint_id,
                                    'type': client_type
                                },
                        return: 'st_id'
                    };
        // console.log(data);
        $.post('<?php echo base_url('admin/communication_flow/modelTable') ?>', data , function(data, textStatus, xhr) {  
            // console.log(data);
            var li_class = day == 1 ? '' : (day) % 2 == 0  ? 'timeline-inverted' : '';
            var animated_class = day == 1 ? 'bounceInLeft' : (day) % 2 == 0  ? 'bounceInRight' : 'bounceInLeft';
            var footer_class = (day) % 2 == 0  ? 'text-right' : '';
            var last_child = 2;
            if (day == 1) {
                $('#tab_campaign').find('.timeline').prepend('<li class="'+li_class+'" st_id='+data+'>\
                                        <div class="timeline-badge success">'+day+'</div>\
                                        <div class="timeline-panel animated '+animated_class+'">\
                                            <div class="timeline-heading hide">\
                                                <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject"></h4>\
                                            </div>\
                                            <div class="timeline-body hide">\
                                                <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note"></textarea>\
                                            </div>\
                                            <div class="timeline-footer '+footer_class+'">\
                                                <hr>  \
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="sms"> \
                                                        <i class="fas fa-comments"></i>\
                                                        Sms\
                                                    </button>\
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
                                                        <i class="fas fa-envelope"></i>\
                                                        Email\
                                                    </button> \
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="both"> \
                                                        <i class="fa fa-reply-all"></i>\
                                                        Both\
                                                    </button> \
                                            </div>\
                                        </div>\
                                    </li>   ');
            } else {
                $('<li class="'+li_class+'" st_id='+data+'>\
                                        <div class="timeline-badge success">'+day+'</div>\
                                        <div class="timeline-panel animated '+animated_class+'">\
                                            <div class="timeline-heading hide">\
                                                <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject"></h4>\
                                            </div>\
                                            <div class="timeline-body hide">\
                                                <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note"></textarea>\
                                            </div>\
                                            <div class="timeline-footer '+footer_class+'">\
                                                <hr>  \
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="sms"> \
                                                        <i class="fas fa-comments"></i>\
                                                        Sms\
                                                    </button>\
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
                                                        <i class="fas fa-envelope"></i>\
                                                        Email\
                                                    </button> \
                                                    <button type="button" class="btn btn-default btn-sm" notif_type="both"> \
                                                        <i class="fa fa-reply-all"></i>\
                                                        Both\
                                                    </button> \
                                            </div>\
                                        </div>\
                                    </li>   ').insertAfter($('.timeline li:nth-last-child('+last_child+')'));
            }
                
        });
    }

    function update_client_status_timelines(st_id,field,value) {
        var data =  {
                        table: 'client_status_timelines',
                        pk: st_id,
                        action: 'save',
                        fields: {
                                    [field]:value
                                },
                        return: ''
                    };
        $.post('<?php echo base_url('admin/communication_flow/modelTable') ?>', data , function(data, textStatus, xhr) { 
        
        });
    }

    function get_automation_list(email_address) {
        $('.select_automation').attr('multiple', 'multiple');
        $('.select_automation').empty(); 
        $('.select_automation').chosen();
        $('.select_automation').trigger("chosen:updated");

        $.post("<?php echo base_url('LynxCampaign/get_automation_list') ?>", {email_address}, function(data, textStatus, xhr) {
            data = JSON.parse(data); 
            $('.select_automation').attr('multiple', 'multiple');
            $('.select_automation').empty(); 
            $.each(data['automation_list'], function(automation_id, automation_name) {
                var selected = '';
                if ($.inArray(automation_id, data['client_automations']) !== -1) {
                    selected = 'selected';
                }
                 $('.select_automation').append('<option '+selected+' value="'+automation_id+'">'+automation_name+'</option>');
            });
            $('.select_automation').chosen().change(function(event,params) { 
                if (!$.isEmptyObject(params.selected)) {
                    console.log('add' + params.selected);
                    add_contact_to_automation($('input[name="profile_email_address"]').val(),params.selected);
                } else { 
                    console.log('remove' + params.deselected);
                    remove_contact_to_automation($('input[name="profile_email_address"]').val(),params.deselected);
                }
            });
            $('.select_automation').trigger("chosen:updated");

        });
    }

    function add_contact_to_automation(email_address,automation_id) {
        $.post("<?php echo base_url('LynxCampaign/add_contact_to_automation') ?>", {email_address,automation_id}, function(data, textStatus, xhr) {
            console.log(data);
        });
    }
    function remove_contact_to_automation(email_address,automation_id) {
        $.post("<?php echo base_url('LynxCampaign/remove_contact_to_automation') ?>", {email_address,automation_id}, function(data, textStatus, xhr) {
            console.log(data);
        });
    }

    function get_clients_a1() { 
        $('.container_clients_a1').empty();
        var data =  {
                        table: 'clients_a1',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'a1_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var container_clients_a1 = $('.container_clients_a1');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" a1_id="'+value.a1_id+'">\
                                    <div class="input-group-prepend">\
                                        <label style="min-width: 92px !important;" for="a1" class="input-group-text">Address 1 </label>\
                                    </div>\
                                    <input type="text" name="clients_a1" class="form-control date-inputmask" value="'+value.address1+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btn_delete_a1 input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                container_clients_a1.append(newInput);
            });

            $(".date-inputmask").inputmask("mm/dd/yyyy");

        });
    }

    function get_clients_a2() { 
        $('.container_clients_a2').empty();
        var data =  {
                        table: 'clients_a2',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'a2_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var container_clients_a2 = $('.container_clients_a2');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" a2_id="'+value.a2_id+'">\
                                    <div class="input-group-prepend">\
                                        <label style="min-width: 92px !important;" for="a2" class="input-group-text">Address 2 </label>\
                                    </div>\
                                    <input type="text" name="clients_a2" class="form-control date-inputmask" value="'+value.address2+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btn_delete_a2 input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                container_clients_a2.append(newInput);
            });

            $(".date-inputmask").inputmask("mm/dd/yyyy");

        });
    }

    function get_clients_cr() { 
        $('.containerClientsCR').empty();
        var data =  {
                        table: 'clients_cr',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'cr_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var containerClientsCR = $('.containerClientsCR');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" cr_id="'+value.cr_id+'">\
                                    <div class="input-group-prepend">\
                                        <label for="cr" class="input-group-text">Credit Report </label>\
                                    </div>\
                                    <input type="text" name="clients_cr" class="form-control date-inputmask" value="'+value.credit_report+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btn_delete_cr input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                containerClientsCR.append(newInput);
            });

            $(".date-inputmask").inputmask("mm/dd/yyyy");

        });
    }
    function get_clients_sa() { 
        $('.containerClientsSA').empty();
        var data =  {
                        table: 'clients_sa',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'sa_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var containerClientsSA = $('.containerClientsSA');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" sa_id="'+value.sa_id+'">\
                                    <div class="input-group-prepend">\
                                        <label for="sa" class="input-group-text">Service Agreement </label>\
                                    </div>\
                                    <input type="text" name="clients_sa" class="form-control date-inputmask" value="'+value.service_agreement+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btn_delete_sa input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                containerClientsSA.append(newInput);
            });

            $(".date-inputmask").inputmask("mm/dd/yyyy");

        });
    }
    function get_clients_enrolled() { 
        $('.containerClientsEnrolled').empty();
        var data =  {
                        table: 'clients_enrolled',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'enrolled_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
                    // console.log(data);
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            // console.log(data);
            var containerClientsEnrolled = $('.containerClientsEnrolled');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" enrolled_id="'+value.enrolled_id+'">\
                                    <div class="input-group-prepend">\
                                        <label for="enrolled" class="input-group-text">Enrolled </label>\
                                    </div>\
                                    <input type="text" name="clients_enrolled" class="form-control date-inputmask" value="'+value.enrolled+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btn_delete_enrolled input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                containerClientsEnrolled.append(newInput);
            });

            $(".date-inputmask").inputmask("mm/dd/yyyy");

        });
    }

    function get_clients_fax() {
        $('.containerClientsFax').removeClass('hide');
        $('.containerClientsFaxSection').empty();
        var data =  {
                        table: 'clients_fax',
                        action: 'get',
                        where: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type
                                },
                        field: 'fax_id',
                        order: 'ASC',
                        limit: 0,
                        offset: 0,
                        group_by: ''

                    };
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var containerClientsFaxSection = $('.containerClientsFaxSection');

            $.each(data, function(key,value) {
                var newInput = '<div class="input-group m-b-5" fax_id="'+value.fax_id+'">\
                                    <div class="input-group-prepend">\
                                        <label for="fax" class="input-group-text">Fax </label>\
                                    </div>\
                                    <input type="text" name="clients_fax" class="form-control" value="'+value.fax+'">\
                                    <div class="input-group-append">\
                                        <a href="#" class="btnDeleteClientsFax input-group-text text-danger"><i class="fas fa-minus-circle"></i></a>\
                                    </div>\
                                </div>';
                containerClientsFaxSection.append(newInput);
            });

        });
    }

    function deleteClientOtherEmail(coe_id) {
        $.post('<?php echo base_url('admin/clients/deleteClientOtherEmail') ?>', {coe_id}, function(data, textStatus, xhr) {
            console.log(data);
            getClientOtherEmails(client_type == 'client' ? client_id : client_joint_id, client_type);
        });
    }
    function saveClientOtherEmail(category,coe_id,id,type,email_address) {
        $.post('<?php echo base_url('admin/clients/saveClientOtherEmail') ?>', {coe_id,id,type,email_address}, function(data, textStatus, xhr) {
            if (category == 'add') {
                var newEmail = '\
                        <div class="input-group" coe_id="'+data+'">\
                            <div class="input-group-prepend">\
                                <label for="email_address" class="input-group-text">Email</label>\
                            </div>\
                            <input type="email" field="email_address" class="form-control" >\
                            <div class="input-group-append">\
                                <button class="btn btn-danger btn-sm btnDeleteClientOtherEmail"><i class="fas fa-minus"></i></button>\
                            </div>\
                        </div>';
                $('.containerClientOtherEmails').append(newEmail);
            }
        });
    }

    function getClientOtherEmails(id,type) {
        $.post('<?php echo base_url('admin/clients/getClientOtherEmails') ?>', {id,type}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            alert();
            $('.containerClientOtherEmails').empty();
            $.each(data, function(index, val) {
                var newEmail = '\
                        <div class="input-group" coe_id="'+val.coe_id+'">\
                            <div class="input-group-prepend">\
                                <label for="email_address" class="input-group-text">Email</label>\
                            </div>\
                            <input type="email" field="email_address" class="form-control" value="'+val.email_address+'">\
                            <div class="input-group-append">\
                                <button class="btn btn-danger btn-sm btnDeleteClientOtherEmail"><i class="fas fa-minus"></i></button>\
                            </div>\
                        </div>';
                $('.containerClientOtherEmails').append(newEmail);
            });
        });
    }

    function getClientOtherEmailsInit(data) {
        $('.containerClientOtherEmails').empty();
        $.each(data, function(index, val) {
            var newEmail = '\
                    <div class="input-group" coe_id="'+val.coe_id+'">\
                        <div class="input-group-prepend">\
                            <label for="email_address" class="input-group-text">Email</label>\
                        </div>\
                        <input type="email" field="email_address" class="form-control" value="'+val.email_address+'">\
                        <div class="input-group-append">\
                            <button class="btn btn-danger btn-sm btnDeleteClientOtherEmail"><i class="fas fa-minus"></i></button>\
                        </div>\
                    </div>';
            $('.containerClientOtherEmails').append(newEmail);
        });
    }

    function saveClientOtherCard(category,data) {
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
            if (category == 'add') {
                var newGroup = '\
                <div class="row" style="margin-bottom: 10px;">\
                    <input type="hidden" field="coc_id" value="'+data+'">\
                    <div class="col-md-6 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Card Number</span>\
                            <input type="text" class="form-control " field="card_number">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Exp</span>\
                            <input type="text" class="form-control " field="card_exp">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 p-r-0">\
                        <div class="input-group">\
                            <div class="rightLabelDiv">\
                                <span for="" class="rightLabel">Cvv</span>\
                                <input type="text" class="form-control " field="card_cvv">\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0">\
                        <button class="btn btn-block btn-warning btn_set_to_primary" style="height: 30px;font-size: 70%;" ><i class="fas fa-star"></i></button>\
                    </div>\
                    <div class="col-md-4 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Card Holder</span>\
                            <input type="text" class="form-control " field="card_holder">\
                        </div>\
                    </div>\
                    <div class="col-md-6 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Address</span>\
                            <input type="text" class="form-control " field="shipping_address"">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 <?php echo $userdata['login_type'] == 'Administrator' ? '' : 'hide'?>">\
                        <button class="btn btn-block btn-danger btnDeleteOtherCard" style="height: 30px;font-size: 70%;" ><i class="fas fa-trash"></i></button>\
                    </div>\
                    <div class="col-md-6 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">City</span>\
                            <input type="text" class="form-control " field="shipping_city">\
                        </div>\
                    </div>\
                    <div class="col-md-3 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">State</span>\
                            <input type="text" class="form-control " field="shipping_state"">\
                        </div>\
                    </div>\
                    <div class="col-md-3 p-l-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Zip</span>\
                            <input type="text" class="form-control " field="shipping_zip">\
                        </div>\
                    </div>\
                </div>';
                $('.containerOtherCards').append(newGroup);
            } else {
                getClientSubscriptions();
            }
        });
    }
    function deleteClientOtherCard(coc_id) {
        $.post('<?php echo base_url('admin/clients/deleteClientOtherCard') ?>', {coc_id}, function(data, textStatus, xhr) {
            getClientOtherCards(client_type == 'client' ? client_id : client_joint_id,client_type)
        });
    }

    function getClientOtherCards(id,type) {
        $.post('<?php echo base_url('admin/clients/getClientOtherCards') ?>', {id,type}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var containerOtherCards = $('.containerOtherCards');
            containerOtherCards.empty();
            $.each(data, function(index, val) {
                var newGroup = '\
                <div class="row" style="margin-bottom: 10px;">\
                    <input type="hidden" field="coc_id" value="'+val.coc_id+'">\
                    <div class="col-md-6 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Card Number</span>\
                            <input type="text" class="form-control " field="card_number" value="'+val.card_number+'">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Exp</span>\
                            <input type="text" class="form-control " field="card_exp" value="'+val.card_exp+'">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 p-r-0">\
                        <div class="input-group">\
                            <div class="rightLabelDiv">\
                                <span for="" class="rightLabel">Cvv</span>\
                                <input type="text" class="form-control " field="card_cvv" value="'+val.card_cvv+'">\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0">\
                        <button class="btn btn-block btn-warning btn_set_to_primary" style="height: 30px;font-size: 70%;" ><i class="fas fa-star"></i></button>\
                    </div>\
                    <div class="col-md-4 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Card Holder</span>\
                            <input type="text" class="form-control " field="card_holder" value="'+val.card_holder+'">\
                        </div>\
                    </div>\
                    <div class="col-md-6 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Address</span>\
                            <input type="text" class="form-control " field="shipping_address" value="'+val.shipping_address+'">\
                        </div>\
                    </div>\
                    <div class="col-md-2 p-l-0 <?php echo $userdata['login_type'] == 'Administrator' ? '' : 'hide'?>">\
                        <button class="btn btn-block btn-danger btnDeleteOtherCard" style="height: 30px;font-size: 70%;" ><i class="fas fa-trash"></i></button>\
                    </div>\
                    <div class="col-md-6 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">City</span>\
                            <input type="text" class="form-control " field="shipping_city" value="'+val.shipping_city+'">\
                        </div>\
                    </div>\
                    <div class="col-md-3 p-l-0 p-r-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">State</span>\
                            <input type="text" class="form-control " field="shipping_state" value="'+val.shipping_state+'">\
                        </div>\
                    </div>\
                    <div class="col-md-3 p-l-0">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Zip</span>\
                            <input type="text" class="form-control " field="shipping_zip" value="'+val.shipping_zip+'">\
                        </div>\
                    </div>\
                </div>';
                $('.containerOtherCards').append(newGroup);
            });

            getClientSubscriptions();;
        });
    }
    function getClientOtherCardsInit(data) { 
        var containerOtherCards = $('.containerOtherCards');
        containerOtherCards.empty();
        $.each(data, function(index, val) {
            var newGroup = '\
            <div class="row" style="margin-bottom: 10px;">\
                <input type="hidden" field="coc_id" value="'+val.coc_id+'">\
                <div class="col-md-6 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">Card Number</span>\
                        <input type="text" class="form-control " field="card_number" value="'+val.card_number+'">\
                    </div>\
                </div>\
                <div class="col-md-2 p-l-0 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">Exp</span>\
                        <input type="text" class="form-control " field="card_exp" value="'+val.card_exp+'">\
                    </div>\
                </div>\
                <div class="col-md-2 p-l-0 p-r-0">\
                    <div class="input-group">\
                        <div class="rightLabelDiv">\
                            <span for="" class="rightLabel">Cvv</span>\
                            <input type="text" class="form-control " field="card_cvv" value="'+val.card_cvv+'">\
                        </div>\
                    </div>\
                </div>\
                <div class="col-md-2 p-l-0">\
                    <button class="btn btn-block btn-warning btn_set_to_primary" style="height: 30px;font-size: 70%;" ><i class="fas fa-star"></i></button>\
                </div>\
                <div class="col-md-4 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">Card Holder</span>\
                        <input type="text" class="form-control " field="card_holder" value="'+val.card_holder+'">\
                    </div>\
                </div>\
                <div class="col-md-6 p-l-0 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">Address</span>\
                        <input type="text" class="form-control " field="shipping_address" value="'+val.shipping_address+'">\
                    </div>\
                </div>\
                <div class="col-md-2 p-l-0 <?php echo $userdata['login_type'] == 'Administrator' ? '' : 'hide'?>">\
                    <button class="btn btn-block btn-danger btnDeleteOtherCard" style="height: 30px;font-size: 70%;" ><i class="fas fa-trash"></i></button>\
                </div>\
                <div class="col-md-6 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">City</span>\
                        <input type="text" class="form-control " field="shipping_city" value="'+val.shipping_city+'">\
                    </div>\
                </div>\
                <div class="col-md-3 p-l-0 p-r-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">State</span>\
                        <input type="text" class="form-control " field="shipping_state" value="'+val.shipping_state+'">\
                    </div>\
                </div>\
                <div class="col-md-3 p-l-0">\
                    <div class="rightLabelDiv">\
                        <span for="" class="rightLabel">Zip</span>\
                        <input type="text" class="form-control " field="shipping_zip" value="'+val.shipping_zip+'">\
                    </div>\
                </div>\
            </div>';
            $('.containerOtherCards').append(newGroup);
        });

        getClientSubscriptions();;
    }

    function saveBureauLettersTemplateTitle(blt_id,template_header,template_title) {
        $.post('<?php echo base_url('admin/clients/saveBureauLettersTemplateTitle') ?>', {blt_id,template_header,template_title}, function(data, textStatus, xhr) {
            if (template_header != 'Creditor Letters') {
                $('#bureau_letters_template_header').trigger('change');
                $('#formSaveBureauLetterTemplateTitles')[0].reset();
                $('#formSaveBureauLetterTemplateTitles').find('button').html('Save');
                $('#formSaveBureauLetterTemplateTitles').find('button').attr('disabled', false);;
            } else {
                getTemplateTitles('Creditor Letters');
                $('#formSaveCreditorLetterTemplateTitles')[0].reset();
                $('#formSaveCreditorLetterTemplateTitles').find('button').html('Save');
                $('#formSaveCreditorLetterTemplateTitles').find('button').attr('disabled', false);;
            }
        });
    }

    function saveBureauLettersTemplateHeader(blth_id,template_header) {
        $.post('<?php echo base_url('admin/clients/saveBureauLettersTemplateHeader') ?>', {blth_id,template_header}, function(data, textStatus, xhr) {
            getBureauLetterTemplateHeaders();
            $('#formSaveBureauLetterTemplateHeaders')[0].reset();
            $('#formSaveBureauLetterTemplateHeaders').find('button').html('Save');
            $('#formSaveBureauLetterTemplateHeaders').find('button').attr('disabled', false);
        });
    }

    function getBureauLetterTemplateHeaders() {
        $.post('<?php echo base_url('admin/clients/getBureauLetterTemplateHeaders') ?>', function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var bureau_letters_template_header = $('#bureau_letters_template_header');
            bureau_letters_template_header.empty();
            var tableBureauLetterTemplateHeaders = $('#tableBureauLetterTemplateHeaders tbody');
            tableBureauLetterTemplateHeaders.empty();

            var tools = '<div class="btn-group">\
                            <button func="edit" class="btnEditBureauLetterTemplateHeader btn btn-success btn-sm"><i class="fas fa-edit"></i></button>\
                            <button class="btnDeleteBureauLetterTemplateHeader btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>\
                        </div>';
            bureau_letters_template_header.append('<option value="">Select Round</option>');;
            $.each(data, function(index, val) {
                tableBureauLetterTemplateHeaders.append('<tr blth_id="'+val.blth_id+'">\
                                                            <td>'+val.template_header+'</td>\
                                                            <td class="text-center">'+tools+'</td>\
                                                        </tr>');

                bureau_letters_template_header.append('<option value="'+val.template_header+'">'+val.template_header+'</option>');
            });
        });
    }

    function setTinyMce() {
        if ($("#bureauMCE").length > 0) {
            var font_fam = $('select[name="profile_font"]').val();
            tinymce.init({
                selector: "textarea#bureauMCE",
                theme: "modern",
                height: 1000,
                forced_root_block : 'div',
                forced_root_block_attrs: { "style": "margin: 0px 0;" },
                body_class : "paper_letter",
                content_style: 'p {margin:0px}\
                                html{background-color: #f8f9fa}\
                                .paper_letter {font-size: 10px; font-family: '+font_fam+';min-height: 279mm;width: 216mm;margin: auto;border: 0px 1px solid grey;padding: 1in;}\
                                .mce-pagebreak{margin-top: 1in;margin-bottom: 1in}\
                            @media print {\
                                .paper_letter{padding: 0px;margin-top: 0px;}\
                                .mce-pagebreak{margin-bottom: 0px;margin-top: 0px} \
                            }',
                setup : function(editor) {
                    editor.on('ExecCommand',function(ed) {
                        if (ed.command == 'fontName') {
                            ed.value = 'Courier New';
                        }
                    });
                },
                plugins: [
                    " advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect fontsizeselect | bold italic | pagebreak | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  | link image | print preview media fullpage | forecolor backcolor emoticons",
                fontsize_formats: "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px"
            });
            // tinymce.get('bureauMCE').setMode('readonly');
        }

        if ($("#creditorMCE").length > 0) {
            var font_fam = $('select[name="profile_font"]').val();
            tinymce.init({
                selector: "textarea#creditorMCE",
                theme: "modern",
                height: 1000,
                forced_root_block : 'div',
                forced_root_block_attrs: { "style": "margin: 0px 0;" },
                body_class : "paper_letter",
                content_style: 'p {margin:0px}\
                                html{background-color: #f8f9fa}\
                                .paper_letter {font-size: 10px; font-family: '+font_fam+';min-height: 279mm;width: 216mm;margin: auto;border: 0px 1px solid grey;padding: 1in;}\
                                .mce-pagebreak{margin-top: 1in;margin-bottom: 1in}\
                            @media print {\
                                .paper_letter{padding: 0px;margin-top: 0px;}\
                                .mce-pagebreak{margin-bottom: 0px;margin-top: 0px} \
                            }',
                setup : function(editor) {
                    editor.on('ExecCommand',function(ed) {
                        if (ed.command == 'fontName') {
                            ed.value = 'Courier New';
                        }
                    });
                },
                plugins: [
                    " advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect fontsizeselect | bold italic | pagebreak | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  | link image | print preview media fullpage | forecolor backcolor emoticons",
                fontsize_formats: "10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px"
            });
            // tinymce.get('bureauMCE').setMode('readonly');
        }
    }

    var clients_account_types = [];
    get_clients_account_types();
    function get_clients_account_types() {
        $.post('<?php echo base_url('admin/clients/get_clients_account_types') ?>', function(data, textStatus, xhr) {
            data = JSON.parse(data);
            clients_account_types = data;
        });
    }
    function getAccountsByBureau(bureau,client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getAccountsByBureau') ?>',{bureau,client_id,client_joint_id,client_type}, function(data){
                data = JSON.parse(data);
                if (data['data'].length > 0) {
                    var bureauAccountsDiv = $('#modal-BureauAccounts .modal-body .containerBureauAccounts');
                    bureauAccountsDiv.empty();

                    var account_status_select = '';
                    $.each(data['clients_account_statuses'],function(key,value){
                         account_status_select += '<option value="'+value.account_status+'">'+value.account_status+'</option>';
                    });


                    var selectAccountTypes = '';
                    $.each(clients_account_types,function(key,value){
                         selectAccountTypes += '<option value="'+value.account_type+'">'+value.account_type+'</option>';
                    });
                    selectAccountTypes += '';

                    var text_color = bureau == 'Equifax' ? 'text-equifax' : bureau == 'Experian' ? 'text-experian' : 'text-themecolor';
                    $.each(data['data'],function(key,value){

                    //
                        var account_type = value.account_type;
                        var boxes = '';
                        if (account_type == '120 Day Late' ||
                            account_type == '180 Day Late' ||
                            account_type == '150 Day Late' ||
                            account_type == '30 Day Late' ||
                            account_type == '60 Day Late' ||
                            account_type == '90 Day Late' ||
                            account_type == 'Charge-Off' ||
                            account_type == 'Repossession' ||
                            account_type == 'New 120 Day Late' ||
                            account_type == 'New 180 Day Late' ||
                            account_type == 'New 150 Day Late' ||
                            account_type == 'New 30 Day Late' ||
                            account_type == 'New 60 Day Late' ||
                            account_type == 'New 90 Day Late' ||
                            account_type == 'New Charge-Off' ||
                            account_type == 'New Repossession' ||
                            account_type == 'Foreclosure' ||
                            account_type == 'Included In BK' ||
                            account_type == 'Multiple Lates') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Balance</small>\
                                        <input label="Balance" field="amount" id="input_amount_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.amount) ? '' : value.amount)+'"/></div>\
                                        <div class="rightLabelDiv"><small class="rightLabel">Credit Limit</small>\
                                        <input label="Credit Limit" field="credit_limit" id="input_credit_limit_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.credit_limit) ? '' : value.credit_limit)+'"/></div>';
                        }
                        if (account_type == 'New Collection' ||
                            account_type == 'Judgment' ||
                            account_type == 'Tax Lien' ||
                            account_type == 'Collection' ||
                            account_type == '120 Day Late' ||
                            account_type == '180 Day Late' ||
                            account_type == '150 Day Late' ||
                            account_type == '30 Day Late' ||
                            account_type == '60 Day Late' ||
                            account_type == '90 Day Late' ||
                            account_type == 'Charge-Off' ||
                            account_type == 'Repossession' ||
                            account_type == 'New Judgment' ||
                            account_type == 'New Tax Lien' ||
                            account_type == 'New Collection' ||
                            account_type == 'New 120 Day Late' ||
                            account_type == 'New 180 Day Late' ||
                            account_type == 'New 150 Day Late' ||
                            account_type == 'New 30 Day Late' ||
                            account_type == 'New 60 Day Late' ||
                            account_type == 'New 90 Day Late' ||
                            account_type == 'New Charge-Off' ||
                            account_type == 'New Repossession' ||
                            account_type == 'Foreclosure' ||
                            account_type == 'Included In BK' ||
                            account_type == 'Multiple Lates') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Past Due</small>\
                                        <input label="Past Due" field="past_due" id="input_past_due_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.past_due) ? '' : value.past_due)+'"/></div>';
                        }
                        if (account_type == 'Inquiry' ||
                            account_type == 'New Inquiry') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Inquiry Date</small>\
                                        <input label="Inquiry Date" field="inquiry_date" id="input_inquiry_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.inquiry_date) ? '' : value.inquiry_date)+'"/></div>';
                        }

                        if (account_type == 'Wrong Address' ||
                            account_type == 'Wrong Social'  ||
                            account_type == 'Wrong Name') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Correct Info</small>\
                                        <input label="Correct Info" field="correct_info" id="input_correct_info_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.correct_info) ? '' : value.correct_info)+'"/></div>';
                        }

                        if (account_type == 'Bankruptcy' ||
                            account_type == 'New Bankruptcy') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Chapter</small>\
                                    <input label="Chapter" field="chapter" id="input_chapter_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.chapter) ? '' : value.chapter)+'"/></div>';
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Status Date</small>\
                                    <input label="Status Date" field="status_date" id="input_status_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control monthyear-inputmask" value="'+($.isEmptyObject(value.status_date) ? '' : value.status_date)+'"/></div>';
                        }

                        if (account_type == 'Judgment' ||
                            account_type == 'Tax Lien' ||
                            account_type == 'Bankruptcy' ||
                            account_type == 'New Judgment' ||
                            account_type == 'New Tax Lien' ||
                            account_type == 'New Bankruptcy') {
                            boxes += ' <div class="rightLabelDiv"><small class="rightLabel">File Date</small>\
                                        <input label="File Date" field="file_date" id="input_file_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control monthyear-inputmask" value="'+($.isEmptyObject(value.file_date) ? '' : value.file_date)+'"/></div>';
                        }
                    //  
                        var newDiv = '<div class="row b-b m-t-10 m-b-10" account_id="'+value.account_id+'"> \
                                        <div class="col-12 col-md-2 p-b-10" style="line-height: 100%"> \
                                            <h5 class="m-b-0 '+text_color+'">'+value.account_name + ' '+value.account_number+'</h5>\
                                            <div class="rightLabelDiv">\
                                                <small class="rightLabel">Account Status</small>\
                                                <select label="Account Status" field="account_status" class="triggerUpdateAccountDetails form-control account_status_'+value.account_id+'"> \
                                                    '+account_status_select+'\
                                                </select>\
                                            </div>\
                                            <div class="rightLabelDiv">\
                                                <small class="rightLabel">Account Type</small>\
                                                <select label="Account Type" field="account_type"  class="triggerUpdateAccountDetails form-control account_type_'+value.account_id+'">\
                                                '+selectAccountTypes+'\
                                                </select>\
                                            </div>\
                                            '+boxes+'\
                                        </div>\
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="1-5"> \
                                            <div>\
                                                <label>Dispute 1</label>\
                                                <textarea field="dispute_1"  class="cntrAccountRounds form-control" rows="3">'+(value.dispute_1 ? value.dispute_1 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 1</label>\
                                                <input field="dispute_1_date" value="'+(value.dispute_1_date ? value.dispute_1_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_1" class="cntrAccountRounds form-control" rows="3">'+(value.result_1 ? value.result_1 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="1-5"> \
                                            <div>\
                                            <label>Dispute 2</label>\
                                                <textarea field="dispute_2" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_2 ? value.dispute_2 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 2</label>\
                                                <input field="dispute_2_date" value="'+(value.dispute_2_date ? value.dispute_2_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_2" class="cntrAccountRounds form-control" rows="3">'+(value.result_2 ? value.result_2 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="1-5"> \
                                            <div>\
                                                <label>Dispute 3</label>\
                                                <textarea field="dispute_3" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_3 ? value.dispute_3 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 3</label>\
                                                <input field="dispute_3_date" value="'+(value.dispute_3_date ? value.dispute_3_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_3" class="cntrAccountRounds form-control" rows="3">'+(value.result_3 ? value.result_3 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="1-5"> \
                                            <div>\
                                                <label>Dispute 4</label>\
                                                <textarea field="dispute_4" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_4 ? value.dispute_4 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 4</label>\
                                                <input field="dispute_4_date" value="'+(value.dispute_4_date ? value.dispute_4_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_4" class="cntrAccountRounds form-control" rows="3">'+(value.result_4 ? value.result_4 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="1-5"> \
                                            <label class="pull-right"><a rNumber="6-9" class="btnViewRounds link text-themecolor">view 6-9</a></label>\
                                            <div>\
                                                <label>Dispute 5</label>\
                                                <textarea field="dispute_5" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_5 ? value.dispute_5 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 5</label>\
                                                <input field="dispute_5_date" value="'+(value.dispute_5_date ? value.dispute_5_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_5" class="cntrAccountRounds form-control" rows="3">'+(value.result_5 ? value.result_5 : '')+'</textarea>\
                                            </div> \
                                        </div>   \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="6-9"> \
                                            <div>\
                                                <label>Dispute 6</label>\
                                                <textarea field="dispute_6"  class="cntrAccountRounds form-control" rows="3">'+(value.dispute_6 ? value.dispute_6 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 6</label>\
                                                <input field="dispute_6_date" value="'+(value.dispute_6_date ? value.dispute_6_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_6" class="cntrAccountRounds form-control" rows="3">'+(value.result_6 ? value.result_6 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="6-9"> \
                                            <div>\
                                                <label>Dispute 7</label>\
                                                <textarea field="dispute_7" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_7 ? value.dispute_7 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 7</label>\
                                                <input field="dispute_7_date" value="'+(value.dispute_7_date ? value.dispute_7_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_7" class="cntrAccountRounds form-control" rows="3">'+(value.result_7 ? value.result_7 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="6-9"> \
                                            <div>\
                                                <label>Dispute 8</label>\
                                                <textarea field="dispute_8" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_8 ? value.dispute_8 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 8</label>\
                                                <input field="dispute_8_date" value="'+(value.dispute_8_date ? value.dispute_8_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_8" class="cntrAccountRounds form-control" rows="3">'+(value.result_8 ? value.result_8 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                        <div class="col-12 col-md-2 animated fadeIn" rounds="6-9"> \
                                            <label class="pull-right"><a rNumber="1-5" class="btnViewRounds link text-themecolor">view 1-5</a></label>\
                                            <div>\
                                                <label>Dispute 9</label>\
                                                <textarea field="dispute_9" class="cntrAccountRounds form-control" rows="3">'+(value.dispute_9 ? value.dispute_9 : '')+'</textarea>\
                                                <!--<button class="input-group-append text-equifax">+</button>-->\
                                            </div> \
                                            <div>\
                                                <label>Result 9</label>\
                                                <input field="dispute_9_date" value="'+(value.dispute_9_date ? value.dispute_9_date : '')+'" type="text" placeholder="mm/dd/yyyy" class="rounddate-inputmask cntrAccountRounds roundDates">\
                                                <textarea field="result_9" class="cntrAccountRounds form-control" rows="3">'+(value.result_9 ? value.result_9 : '')+'</textarea>\
                                            </div> \
                                        </div> \
                                    </div>';


                        bureauAccountsDiv.append(newDiv);




                        $(".account_status_"+value.account_id).val(value.account_status);
                        $(".account_type_"+value.account_id).val(value.account_type);

                        if (value.account_status == 'Deleted' || value.account_status == 'Fixed' || value.account_status == 'Not Reporting' || value.account_status == 'Hold') {
                            $('[account_id="'+value.account_id+'"]').find('input').attr('disabled',true);
                            $('[account_id="'+value.account_id+'"]').find('input').attr('disabled',true);
                            $('[account_id="'+value.account_id+'"]').remove();
                        }
                    });



                    $('[rounds="1-5"]').removeClass('hide');
                    $('[rounds="6-9"]').addClass('hide');
                } else {
                    var bureauAccountsDiv = $('#modal-BureauAccounts .modal-body .containerBureauAccounts');
                        bureauAccountsDiv.html('<h3 class="text-center m-t-20">No Accounts Found...</h3>');
                }

                // $(".rounddate-inputmask").inputmask("99/99/9999");
                $(".rounddate-inputmask").inputmask("99/99/9999");
                $(".monthyear-inputmask").inputmask("mm/yyyy");

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function deleteAccountType(at_id) {
        $.post('<?php echo base_url('admin/clients/deleteAccountType') ?>',
            {at_id},function(data){
                getAccountTypes();
            });
    }

    function deleteCreditorsAccountType(creditors_account_type_id) {
        $.post('<?php echo base_url('admin/clients/deleteCreditorsAccountType') ?>',
            {creditors_account_type_id},function(data){
                getTableCreditorsAccountType();
            });
    }
    function deleteCreditorsRec(creditors_rec_id) {
        $.post('<?php echo base_url('admin/clients/deleteCreditorsRec') ?>',
            {creditors_rec_id},function(data){
                getTableCreditorsRec();
            });
    }
    function saveAccountType(at_id,account_type) {
        $.post('<?php echo base_url('admin/clients/saveAccountType') ?>',
            {at_id,account_type},function(data){
                at_id = '';
                $('#new_account_type').val('');
                getAccountTypes();
            });
    }

    function deleteAccountStatus(as_id) {
        $.post('<?php echo base_url('admin/clients/deleteAccountStatus') ?>',
            {as_id},function(data){
                getAccountStatuses();
            });
    }
    function saveAccountStatus(as_id,account_status) {
        $.post('<?php echo base_url('admin/clients/saveAccountStatus') ?>',
            {as_id,account_status},function(data){
                as_id = '';
                $('#new_account_status').val('');
                getAccountStatuses();
            });
    }

    function deleteAccountPaymentStatus(ps_id) {
        $.post('<?php echo base_url('admin/clients/deleteAccountPaymentStatus') ?>',
            {ps_id},function(data){
                getAccountPaymentStatuses();
            });
    }
    function saveAccountPaymentStatus(ps_id,payment_status) {
        $.post('<?php echo base_url('admin/clients/saveAccountPaymentStatus') ?>',
            {ps_id,payment_status},function(data){
                ps_id = '';
                $('#new_payment_status').val('');
                getAccountPaymentStatuses();
            });
    }

    function deleteAccountComment(ac_id) {
        $.post('<?php echo base_url('admin/clients/deleteAccountComment') ?>',
            {ac_id},function(data){
                getAccountComments();
            });
    }
    function saveAccountComment(ac_id,comment) {
        $.post('<?php echo base_url('admin/clients/saveAccountComment') ?>',
            {ac_id,comment},function(data){
                ac_id = '';
                $('#new_comment').val('');
                getAccountComments();
            });
    }

    function GamePlanCreditScore(eta_date,equifax_points, experian_points, transunion_points, idealCreditScore, client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/gameplanCreditScore') ?>',
            {eta_date,equifax_points, experian_points, transunion_points, idealCreditScore, client_id,client_joint_id,client_type}, function(data) {
                // console.log(data);
            $.toast({
                heading: 'Success!',
                text: 'Game Plan Saved!',
                position: 'top-right',
                loaderBg:'#ffc107',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
        });
    }

    function saveDisputeVerbiagesAssignment() {
        var account_type = $('.btnShowRoundsTable').html();
        var round = $('.btnShowDVTable').html();
        round = round.replace('Round ','');
        var dispute_verbiage = $('#selectDisputeVergiageAssignmentInput').val();
        if (dispute_verbiage != '') {
            $.post('<?php echo base_url('admin/clients/saveDisputeVerbiagesAssignment') ?>',
                {account_type,round,dispute_verbiage}, function(data){
                    getDisputeVerbiagesByATandRound();
                    // console.log(data);
                    $('#selectDisputeVergiageAssignmentInput').val('');
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });
            }
        }


    function deleteDisputeVerbiageAssignment(cdva_id) {
        $.post('<?php echo base_url('admin/clients/deleteDisputeVerbiageAssignment') ?>',{cdva_id}, function(data){
                getDisputeVerbiagesByATandRound();
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
        }

    function getDisputeVerbiagesByATandRound() {
        var account_type = $('.btnShowRoundsTable').html();
        var round = $('.btnShowDVTable').html();
        round = round.replace('Round ','');
        $.post('<?php echo base_url('admin/clients/getDisputeVerbiagesByATandRound') ?>',{account_type,round}, function(data){
                data = JSON.parse(data);
                var tbl_dva_dv = $('#tbl_dva_dv tbody');
                tbl_dva_dv.empty();
                if (data.length > 0) {
                    $.each(data,function(key,value){
                        var newRow = '<tr cdva_id="'+value.cdva_id+'">\
                                        <td style="line-height: 1 !important">'+value.dispute_verbiage+'</td>\
                                        <td><button class="btnDeleteDisputeVerbiageFromAssignment btn btn-danger btn-sm btn-rounded"><i class="fas fa-trash"></i></button></td>\
                                    </tr>';
                        tbl_dva_dv.append(newRow);
                    });
                } else {
                    var newRow = '<tr>\
                                    <td rowspan="2" class="text-center">No Verbiage Found</td>\
                                </tr>';
                    tbl_dva_dv.append(newRow);
                }

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }


    function updateTableRow(table,field,from,to) {
        $.post('<?php echo base_url('admin/clients/updateTableRow') ?>',{table,field,from,to}, function(data){
                // data = JSON.parse(data);
                $.toast({
                    heading: 'Update Success!',
                    text: from+' changed to '+to,
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }


    function getClientCreditScores(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getCreditScore') ?>', {client_id,client_joint_id,client_type}, function(data) {
            data= JSON.parse(data);
            // console.log(data);
            $('#equifax_points').val('0');
            $('#experian_points').val('0');
            $('#transunion_points').val('0');
            $('#idealCreditScore').val('0');
            $('#eta_date').val('');
            if (data.length > 0) {
                $.each(data,function(key,value){
                    var equifax = value.equifax_points;
                    var experian = value.experian_points;
                    var transunion = value.transunion_points;
                    var credit_score = value.credit_score;
                    var eta_date = value.eta_date;
                    $('#equifax_points').val(equifax);
                    $('#experian_points').val(experian);
                    $('#transunion_points').val(transunion);
                    $('.span_equifax_points').html(equifax);
                    $('.span_experian_points').html(experian);
                    $('.span_transunion_points').html(transunion);
                    $('#idealCreditScore').val(credit_score);
                    $('#eta_date').val(eta_date);
                });
            } else {

            }
        });
        // console.log(client_id);
    }



    function getCallLogsByClient(client_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getCallLogsByClient') ?>', {client_id,client_type},function(data){
                data = JSON.parse(data);
                // console.log(data);
                var call_logs_table = $('#containerClientCallLogs tbody');
                call_logs_table.empty();
                $.each(data,function(key,value){
                    var type = value.direction;
                    var date = moment(value.startTime).format('MM/DD/YYYY hh:mm:ss A');;
                    var action = value.action;
                    var result = value.result;
                    var length = value.duration;
                    var minutes = Math.floor(length / 60); // 7
                    var seconds = length % 60; // 30
                    function n(n){
                        return n > 9 ? "" + n: "0" + n;
                    }
                    length = n(minutes)+':'+n(seconds);
                    call_logs_table.append('\
                                    <tr>\
                                        <td>'+type+'</td>\
                                        <td>'+date+'</td>\
                                        <td>'+action+'</td>\
                                        <td>'+result+'</td>\
                                        <td>'+length+'</td>\
                                    </tr>\
                                        ');
                });
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }


    $('#tbl_authorize_transactions').on('click', '.btn_void', function(event) {
        event.preventDefault();
        var transaction_id = $(this).closest('tr').find('td:nth-child(1)').html();
        swal({
            title: "Are you sure?",
            text: "this transaction will be voided!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo base_url('admin/authorize/transactions/voidTransaction') ?>', {transaction_id}, function(data, textStatus, xhr) {
                    swal(data);
                });
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
                
    });
    $('#tbl_authorize_transactions').on('click', '.btn_refund', function(event) {
        event.preventDefault();
        var transaction_id = $(this).closest('tr').find('td:nth-child(1)').html();
        var account_number = $(this).closest('tr').find('td:nth-child(3)').html();
        var amount = $(this).closest('tr').find('td:nth-child(5)').html();
        swal({
            title: "Are you sure?",
            text: amount+" will be refunded to "+account_number,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo base_url('admin/authorize/transactions/refundTransaction') ?>', {transaction_id,amount}, function(data, textStatus, xhr) {
                    swal(data);
                });
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
                
    });
    function getClientSubscriptions() { 
        var id = client_type == 'client' ? client_id : client_joint_id;
        var type = client_type; 
        tbl_authorize_transactions.clear().draw();
        tbl_authorize_subscriptions.clear().draw();
        $('.transaction_total_amount').html(0);
        $.post('<?php echo base_url('admin/authorize/subscriptions/getClientAuthorizeSubscriptions') ?>',
            {id,type}, function(data){
                data = JSON.parse(data);
                // console.log(data); 

                var billing_date_paid = $('input[name="billing_date_paid"]');
                if (data['transactions'].length > 0) {
                    var date_paid = '';
                    var transaction_total_amount = 0;
                    var setup_fee_paid = 1;
                    var setup_fee_amount = $('input[name="billing_setup_fee"]').val();
                    $.each(data['transactions'],function(key,value){
                        var btn_void = '<button class="btn_void btn btn-info btn-sm"><i class="fas fa-times"></i> Void</button>';
                        var btn_refund = '<button class="btn_refund btn btn-danger btn-sm"><i class="fas fa-refresh"></i> Refund</button>';
                        var btn_action = value.status == 'capturedPendingSettlement' ? btn_void : value.status == 'settledSuccessfully' ? btn_refund : '';
                        tbl_authorize_transactions.row.add([value.transaction_id,value.name,value.account_number,value.status,value.amount,moment(value.submitted_local).format('MM/DD/YYYY - h:mm a'),btn_action]);
                         
                        if (date_paid == '') {
                            if (value.status == 'settledSuccessfully' || value.status == 'capturedPendingSettlement') {
                                date_paid = moment(value.submitted_local).format('MM/DD/YYYY');
                                setup_fee_paid = 1;
                            } 
                        }


 
                        if (value.status == 'settledSuccessfully' || value.status == 'capturedPendingSettlement') {
                            transaction_total_amount += parseFloat(value.amount);
                            setup_fee_paid = 1;
                        }



                        if (value.status == 'refundSettledSuccessfully') {
                            transaction_total_amount -= parseFloat(value.amount);
                            if (parseFloat(value.amount) == parseFloat(setup_fee_amount)) {
                                setup_fee_paid = 0;    
                            }
                        }

                         
                    });
                    console.log(setup_fee_paid);
                    console.log(setup_fee_paid);
                    console.log(setup_fee_paid);
                    billing_date_paid.val(setup_fee_paid == 1 ? date_paid : '');
                    tbl_authorize_transactions.draw();

                    $('.transaction_total_amount').html((transaction_total_amount).toFixed(2));

                    $.each(data['subscriptions'],function(key,value){
                        tbl_authorize_subscriptions.row.add([value.account_number,value.status,moment(value.date_changed).format('MM/DD/YYYY h:mm:ss a')]); 
                    }); 
                    tbl_authorize_subscriptions.draw();
                }

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }



    function convertLeadToClient(client_id) {
        $.post('<?php echo base_url('admin/clients/convertLeadToClient') ?>',
            {client_id}, function(data){
                localStorage.id = client_id;
                localStorage.type = 'client';
                localStorage.joint_id = '';
                localStorage.stay = 'false';

                var login_type = '<?php echo $userdata['login_type'] ?>';
                if (login_type == 'Administrator') {
                    window.location.href = 'https://admin.creditlynx.com/admin/clients';
                } else {
                    window.location.href = 'https://admin.creditlynx.com/admin/leads';
                }
                

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
        }

    $('#formSendClientEmail').on('submit',function(e){
        e.preventDefault();
        var from = $('#inputFromClientEmail').val();
        var subject = $('#inputSubjectClientEmail').val();
        var message = $('#inputMessageClientEmail').val();
        sendClientEmail('Email',client_type == 'client' ? client_id : client_joint_id,subject,message,client_type,from);
        // getClientEmails(client_type == 'client' ? client_id : client_joint_id,client_type);
        get_comm_flow();
    });


    function sendClientEmail(type,client_id,subject,notes,client_type,from) {
        $.post('<?php echo base_url('admin/clients/sendClientEmail') ?>',
            {type,client_id,subject,notes,client_type,from},function(data){
                console.log(data);
                $('#inputFromtClientEmail').val('');
                $('#inputSubjectClientEmail').val('');
                $('#inputMessageClientEmail').val('');
                $('#modal_add_email').modal('hide');
            });
    }

    $('#formSendClientText').on('submit',function(e){
        e.preventDefault();
        var title = $('#inputTitleClientText').val();
        var text_message = $('#inputTextMessageClientText').val();
        // console.log(client_id);
        $(this).find('button').prop('disabled', true);
        sendClientText('Phone',client_id,title,text_message,client_type);
        //getClientTexts(client_type == 'client' ? client_id : client_joint_id,client_type);
    });

    function sendClientText(type,client_id,subject,notes,client_type) {
        $.post('<?php echo base_url('admin/clients/sendClientText') ?>',
            {type,client_id,subject,notes,client_type},function(data){
                console.log(data);
                $('#inputTitleClientText').val('');
                $('#inputTextMessageClientText').val('');
                $('#formSendClientText').find('button').prop('disabled', false);
                $('#modal_add_sms').modal('hide');
                get_comm_flow();
            });
    }


    function getClientEmails(client_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getClientEmails') ?>',
            {client_id,client_type},function(data){
                data = JSON.parse(data);
                $('#containerClientEmails').empty();
                var from = 'CreditLynx';
                var from_email = 'no-reply@creditlynx.com';
                var img = '<div>\
                                <a href="javascript:void(0)"><img src="<?php echo base_url('assets/images/LynxLogo.png') ?>" alt="user" width="40" class="img-circle"></a>\
                            </div>';
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        if (value.type != 'to_client') {
                            from = $('input[name="profile_name"]').val();
                            from_email = value.from;
                        } else {
                            from = 'CreditLynx';
                            from_email = value.from;
                        }
                        var message = value.message != null ? value.message : '';
                        var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                                <div value="'+value.subject+'" from="'+value.from+'" class="card-body emailHeader" style="cursor:pointer;padding-top:5px;padding-bottom:5px">\
                                                    <h5 class="card-title m-b-0">'+value.subject+'<small class="pull-right">'+moment(value.date).format('lll')+'</small></h5>\
                                                </div>\
                                                <div id="email_container_'+value.email_id+'" class="card-body b-t hide animated fadeIn">\
                                                    <div class="d-flex m-b-40">\
                                                        '+img+'\
                                                        <div class="p-l-10">\
                                                            <h4 class="m-b-0">'+from+'</h4>\
                                                            <small class="text-muted">From: '+from_email+'</small>\
                                                        </div>\
                                                    </div>\
                                                    <p style="white-space: pre-wrap;">'+value.message+'</p>\
                                                </div>\
                                            </div>';
                        $('#containerClientEmails').append(card);
                        $('.emailHeader').on('click',function(){
                            var email_container = $(this).closest('.card').find('#email_container_'+value.email_id);
                            var subject = $(this).attr('value');
                            var from = $(this).attr('from');
                            $('#inputSubjectClientEmail').val(subject);
                            $('#inputFromClientEmail').val(from);
                            if (email_container.hasClass('hide')) {
                                email_container.removeClass('hide');
                            } else {
                                email_container.addClass('hide');
                            }
                        });
                    });
                } else {
                    $('#containerClientEmails').append('No Emails Found');
                }


            });

    }



    function getClientTexts(id,type) {
        $.post('<?php echo base_url('admin/clients/getClientTexts') ?>',
            {id,type},function(data){
                data = JSON.parse(data);
                $('#containerClientTexts').empty();
                var from_phone = 'no-reply@creditlynx.com';
                if (data.length != 0) {

                    $.each(data,function(key,value){
                        // console.log(value.type);
                        if (value.type == 'from_client') {
                            from_phone = $('input[name="profile_name"]').val();
                        } else {
                            from_phone = 'CreditLynx';
                        }

                        var text_message = value.text_message != null ? value.text_message : '';
                        text_message = text_message.replace('style="font-family:verdana,sans-serif;color:#3333ff"','');
                        text_message = text_message.replace(/<br>/g,'');
                        if (value.title == 'mms') {
                            text_message = '<blockquote style="white-space: pre-wrap"><a target="_blank" href="'+$.trim(text_message)+'">Attachment</a></blockquote>';
                        } else {
                            text_message = '<blockquote style="white-space: pre-wrap">'+$.trim(text_message)+'</blockquote>';
                        }

                        var card = '<div class="card b-all shadow-none" style="margin-bottom:0px">\
                                                <div class="card-body emailHeader" style="padding-top:5px;padding-bottom:5px">\
                                                    <h5 class="card-title m-b-0">From: '+from_phone+'<small class="pull-right">'+moment(value.date).format('lll')+'</small> '+text_message+'</h5>\
                                                </div>\
                                            </div>';
                        $('#containerClientTexts').append(card);

                    });
                } else {
                    $('#containerClientTexts').append('No Texts Found');
                }


            });

    }




    function lookUpCardNumber(card_number) {
        $.post('<?php echo base_url('admin/clients/lookUpCardNumber') ?>',
            {card_number},function(data){
                data = JSON.parse(data);
                // console.log(data);
                var network = data['scheme'];
                var type = data['type'];
                var bank = data['bank']['name'];
                var brand = data['brand'];
                var prepaid = data['prepaid'];
                var country = data['country']['emoji']+data['country']['name'];
                var div = '<div class="row">\
                                <div class="col-6 text-left">\
                                    TYPE: '+type+'\
                                </div>\
                                <div class="col-6 text-left">\
                                    BANK: '+bank+'\
                                </div>\
                                <div class="col-6 text-left">\
                                    BRAND: '+brand+'\
                                </div>\
                                <div class="col-6 text-left">\
                                    PREPAID: '+prepaid+'\
                                </div>\
                            </div>';
                swal('Card Information',div);
                // console.log(network);
                // $('[name="billing_payment_method"]').val(network);
                // $('[name="billing_payment_method"]').trigger('change');
            });
    }

    function saveLetterCreditor(creditor_id,name,address,city_state_zip) {
        $.post('<?php echo base_url('admin/clients/saveLetterCreditor') ?>',
            {creditor_id,name,address,city_state_zip},function(data){
                getLetterCreditors();
                $('#formLetterCreditorsFront')[0].reset();
                $('#formLetterCreditors')[0].reset();


                $('#formLetterCreditorsFront').find('button').html('Save Creditor');
                $('#formLetterCreditorsFront').find('button').attr('disabled', false);;
                $('#formLetterCreditors').find('button').html('Save Creditor');
                $('#formLetterCreditors').find('button').attr('disabled', false);;
            });
    }

    function getClientLetterAccounts(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getClientLetterAccounts') ?>',
            {client_id,client_joint_id,client_type},function(data){
                // console.log('getClientLetterAccounts');
                // console.log(data);
                data = JSON.parse(data);
                var select_client_accounts = $('#select_client_accounts');
                // select_client_accounts.select2('destroy');
                select_client_accounts.empty();
                select_client_accounts.append('<option value="">Select Account</option>');
                $.each(data,function(key,value){
                    select_client_accounts.append('<option value="'+value.account_number+'">'+value.account_name + ' - '+value.account_number+'</option>');
                });
                select_client_accounts.chosen({width: "100%",height: "40px"});
                select_client_accounts.trigger("chosen:updated");
            });
    }

    function getLetterCreditors() {
        $.post('<?php echo base_url('admin/clients/getLetterCreditors') ?>'
            ,function(data){
                data = JSON.parse(data);
                var select_letter_creditors = $('#select_letter_creditors');
                var tableLetterCreditors = $('#tableLetterCreditors tbody');
                tableLetterCreditors.empty();
                select_letter_creditors.empty();
                select_letter_creditors.append('<option value="">Select Creditor</option>');
                var tools = '<div class="btn-group">\
                                <button func="edit" class="btnEditLetterCreditors btn btn-success btn-sm"><i class="fas fa-edit"></i></button>\
                                <button class="btnDeleteLetterCreditors btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>\
                            </div>';

                var creditor_names = [];
                $.each(data,function(key,value){
                    tableLetterCreditors.append('\
                                                <tr creditor_id="'+value.creditor_id+'">\
                                                    <td>'+value.name+'</td>\
                                                    <td>'+value.address+'</td>\
                                                    <td>'+value.city_state_zip+'</td>\
                                                    <td>'+tools+'</td>\
                                                </tr>');

                    select_letter_creditors.append('<option value="'+value.name+'<br>'+value.address+'<br>'+value.city_state_zip+'">'+value.name+'</option>');
                    creditor_names.push({value: value.name, label: value.name, desc: ''});
                });

                // console.log(creditor_names);
                $("#bank_account_name" ).autocomplete({
                    minLength: 0,
                    source: creditor_names,
                    focus: function( event, ui ) {
                        $( "#bank_account_name" ).val( ui.item.label );
                        return false;
                    },
                    select: function( event, ui ) {
                        $( "#bank_account_name" ).val( ui.item.label );

                        return false;
                    }
                });
            });
    }

    function fetchDataTemplateList(client_id,client_joint_id,client_type,template,my_tinymce) {
        $.post('<?php echo base_url('admin/clients/fetchDataTemplateList') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                var fetched_template = template;
                var equifax_list_r1 = '';
                var experian_list_r1 = '';
                var transunion_list_r1 = '';

                var equifax_list_r2 = '';
                var experian_list_r2 = '';
                var transunion_list_r2 = '';

                var equifax_list_r3 = '';
                var experian_list_r3 = '';
                var transunion_list_r3 = '';

                var equifax_list_r4 = '';
                var experian_list_r4 = '';
                var transunion_list_r4 = '';

                var equifax_list_r5 = '';
                var experian_list_r5 = '';
                var transunion_list_r5 = '';

                var equifax_list_r6 = '';
                var experian_list_r6 = '';
                var transunion_list_r6 = '';

                var equifax_list_r7 = '';
                var experian_list_r7 = '';
                var transunion_list_r7 = '';

                var equifax_list_r8 = '';
                var experian_list_r8 = '';
                var transunion_list_r8 = '';

                var equifax_list_r9 = '';
                var experian_list_r9 = '';
                var transunion_list_r9 = '';
                $.each(data,function(key,value){
                    if (fetched_template.indexOf('RND1') != -1) {
                        if (value.dispute_1 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r1 += value.account_name+' '+value.account_number+', '+value.dispute_1+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r1 += value.account_name+' '+value.account_number+', '+value.dispute_1+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r1 += value.account_name+' '+value.account_number+', '+value.dispute_1+'<br>';
                                }
                            }
                        }

                    }

                    if (fetched_template.indexOf('RND2') != -1) {
                        if (value.dispute_2 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r2 += value.account_name+' '+value.account_number+', '+value.dispute_2+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r2 += value.account_name+' '+value.account_number+', '+value.dispute_2+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r2 += value.account_name+' '+value.account_number+', '+value.dispute_2+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND3') != -1) {
                        if (value.dispute_3 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r3 += value.account_name+' '+value.account_number+', '+value.dispute_3+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r3 += value.account_name+' '+value.account_number+', '+value.dispute_3+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r3 += value.account_name+' '+value.account_number+', '+value.dispute_3+'<br>';
                                }
                            }
                        }
                    }


                    if (fetched_template.indexOf('RND4') != -1) {
                        if (value.dispute_4 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r4 += value.account_name+' '+value.account_number+', '+value.dispute_4+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r4 += value.account_name+' '+value.account_number+', '+value.dispute_4+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r4 += value.account_name+' '+value.account_number+', '+value.dispute_4+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND5') != -1) {
                        if (value.dispute_5 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r5 += value.account_name+' '+value.account_number+', '+value.dispute_5+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r5 += value.account_name+' '+value.account_number+', '+value.dispute_5+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r5 += value.account_name+' '+value.account_number+', '+value.dispute_5+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND6') != -1) {
                        if (value.dispute_6 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r6 += value.account_name+' '+value.account_number+', '+value.dispute_6+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r6 += value.account_name+' '+value.account_number+', '+value.dispute_6+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r6 += value.account_name+' '+value.account_number+', '+value.dispute_6+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND7') != -1) {
                        if (value.dispute_7 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r7 += value.account_name+' '+value.account_number+', '+value.dispute_7+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r7 += value.account_name+' '+value.account_number+', '+value.dispute_7+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r7 += value.account_name+' '+value.account_number+', '+value.dispute_7+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND8') != -1) {
                        if (value.dispute_8 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r8 += value.account_name+' '+value.account_number+', '+value.dispute_8+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r8 += value.account_name+' '+value.account_number+', '+value.dispute_8+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r8 += value.account_name+' '+value.account_number+', '+value.dispute_8+'<br>';
                                }
                            }
                        }
                    }

                    if (fetched_template.indexOf('RND9') != -1) {
                        if (value.dispute_9 != null) {
                            if (fetched_template.indexOf('EQUIFAX') != -1) {
                                if (value.bureaus == 'Equifax') {
                                    equifax_list_r9 += value.account_name+' '+value.account_number+', '+value.dispute_9+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('EXPERIAN') != -1) {
                                if (value.bureaus == 'Experian') {
                                    experian_list_r9 += value.account_name+' '+value.account_number+', '+value.dispute_9+'<br>';
                                }
                            }
                            if (fetched_template.indexOf('TRANSUNION') != -1) {
                                if (value.bureaus == 'TransUnion') {
                                    transunion_list_r9 += value.account_name+' '+value.account_number+', '+value.dispute_9+'<br>';
                                }
                            }
                        }
                    }


                });

                equifax_list_r1 = equifax_list_r1.split('<br>');
                $.each(equifax_list_r1,function(key,value){
                    equifax_list_r1[key] = (key+1)+ '. '+value;
                });

                experian_list_r1 = experian_list_r1.split('<br>');
                $.each(experian_list_r1,function(key,value){
                    experian_list_r1[key] = (key+1)+ '. '+value;
                });

                transunion_list_r1 = transunion_list_r1.split('<br>');
                $.each(transunion_list_r1,function(key,value){
                    transunion_list_r1[key] = (key+1)+ '. '+value;
                });

                equifax_list_r2 = equifax_list_r2.split('<br>');
                $.each(equifax_list_r2,function(key,value){
                    equifax_list_r2[key] = (key+1)+ '. '+value;
                });

                experian_list_r2 = experian_list_r2.split('<br>');
                $.each(experian_list_r2,function(key,value){
                    experian_list_r2[key] = (key+1)+ '. '+value;
                });

                transunion_list_r2 = transunion_list_r2.split('<br>');
                $.each(transunion_list_r2,function(key,value){
                    transunion_list_r2[key] = (key+1)+ '. '+value;
                });

                equifax_list_r3 = equifax_list_r3.split('<br>');
                $.each(equifax_list_r3,function(key,value){
                    equifax_list_r3[key] = (key+1)+ '. '+value;
                });

                experian_list_r3 = experian_list_r3.split('<br>');
                $.each(experian_list_r3,function(key,value){
                    experian_list_r3[key] = (key+1)+ '. '+value;
                });

                transunion_list_r3 = transunion_list_r3.split('<br>');
                $.each(transunion_list_r3,function(key,value){
                    transunion_list_r3[key] = (key+1)+ '. '+value;
                });

                equifax_list_r4 = equifax_list_r4.split('<br>');
                $.each(equifax_list_r4,function(key,value){
                    equifax_list_r4[key] = (key+1)+ '. '+value;
                });

                experian_list_r4 = experian_list_r4.split('<br>');
                $.each(experian_list_r4,function(key,value){
                    experian_list_r4[key] = (key+1)+ '. '+value;
                });

                transunion_list_r4 = transunion_list_r4.split('<br>');
                $.each(transunion_list_r4,function(key,value){
                    transunion_list_r4[key] = (key+1)+ '. '+value;
                });

                equifax_list_r5 = equifax_list_r5.split('<br>');
                $.each(equifax_list_r5,function(key,value){
                    equifax_list_r5[key] = (key+1)+ '. '+value;
                });

                experian_list_r5 = experian_list_r5.split('<br>');
                $.each(experian_list_r5,function(key,value){
                    experian_list_r5[key] = (key+1)+ '. '+value;
                });

                transunion_list_r5 = transunion_list_r5.split('<br>');
                $.each(transunion_list_r5,function(key,value){
                    transunion_list_r5[key] = (key+1)+ '. '+value;
                });

                equifax_list_r6 = equifax_list_r6.split('<br>');
                $.each(equifax_list_r6,function(key,value){
                    equifax_list_r6[key] = (key+1)+ '. '+value;
                });

                experian_list_r6 = experian_list_r6.split('<br>');
                $.each(experian_list_r6,function(key,value){
                    experian_list_r6[key] = (key+1)+ '. '+value;
                });

                transunion_list_r6 = transunion_list_r6.split('<br>');
                $.each(transunion_list_r6,function(key,value){
                    transunion_list_r6[key] = (key+1)+ '. '+value;
                });

                equifax_list_r7 = equifax_list_r7.split('<br>');
                $.each(equifax_list_r7,function(key,value){
                    equifax_list_r7[key] = (key+1)+ '. '+value;
                });

                experian_list_r7 = experian_list_r7.split('<br>');
                $.each(experian_list_r7,function(key,value){
                    experian_list_r7[key] = (key+1)+ '. '+value;
                });

                transunion_list_r7 = transunion_list_r7.split('<br>');
                $.each(transunion_list_r7,function(key,value){
                    transunion_list_r7[key] = (key+1)+ '. '+value;
                });

                equifax_list_r8 = equifax_list_r8.split('<br>');
                $.each(equifax_list_r8,function(key,value){
                    equifax_list_r8[key] = (key+1)+ '. '+value;
                });

                experian_list_r8 = experian_list_r8.split('<br>');
                $.each(experian_list_r8,function(key,value){
                    experian_list_r8[key] = (key+1)+ '. '+value;
                });

                transunion_list_r8 = transunion_list_r8.split('<br>');
                $.each(transunion_list_r8,function(key,value){
                    transunion_list_r8[key] = (key+1)+ '. '+value;
                });

                equifax_list_r9 = equifax_list_r9.split('<br>');
                $.each(equifax_list_r9,function(key,value){
                    equifax_list_r9[key] = (key+1)+ '. '+value;
                });

                experian_list_r9 = experian_list_r9.split('<br>');
                $.each(experian_list_r9,function(key,value){
                    experian_list_r9[key] = (key+1)+ '. '+value;
                });

                transunion_list_r9 = transunion_list_r9.split('<br>');
                $.each(transunion_list_r9,function(key,value){
                    transunion_list_r9[key] = (key+1)+ '. '+value;
                });
                equifax_list_r1.pop();
                experian_list_r1.pop();
                transunion_list_r1.pop();
                equifax_list_r2.pop();
                experian_list_r2.pop();
                transunion_list_r2.pop();
                equifax_list_r3.pop();
                experian_list_r3.pop();
                transunion_list_r3.pop();
                equifax_list_r4.pop();
                experian_list_r4.pop();
                transunion_list_r4.pop();
                equifax_list_r5.pop();
                experian_list_r5.pop();
                transunion_list_r5.pop();
                equifax_list_r6.pop();
                experian_list_r6.pop();
                transunion_list_r6.pop();
                equifax_list_r7.pop();
                experian_list_r7.pop();
                transunion_list_r7.pop();
                equifax_list_r8.pop();
                experian_list_r8.pop();
                transunion_list_r8.pop();
                equifax_list_r9.pop();
                experian_list_r9.pop();
                transunion_list_r9.pop();
                equifax_list_r1 = equifax_list_r1.join('<br>');
                experian_list_r1 = experian_list_r1.join('<br>');
                transunion_list_r1 = transunion_list_r1.join('<br>');
                equifax_list_r2 = equifax_list_r2.join('<br>');
                experian_list_r2 = experian_list_r2.join('<br>');
                transunion_list_r2 = transunion_list_r2.join('<br>');
                equifax_list_r3 = equifax_list_r3.join('<br>');
                experian_list_r3 = experian_list_r3.join('<br>');
                transunion_list_r3 = transunion_list_r3.join('<br>');
                equifax_list_r4 = equifax_list_r4.join('<br>');
                experian_list_r4 = experian_list_r4.join('<br>');
                transunion_list_r4 = transunion_list_r4.join('<br>');
                equifax_list_r5 = equifax_list_r5.join('<br>');
                experian_list_r5 = experian_list_r5.join('<br>');
                transunion_list_r5 = transunion_list_r5.join('<br>');
                equifax_list_r6 = equifax_list_r6.join('<br>');
                experian_list_r6 = experian_list_r6.join('<br>');
                transunion_list_r6 = transunion_list_r6.join('<br>');
                equifax_list_r7 = equifax_list_r7.join('<br>');
                experian_list_r7 = experian_list_r7.join('<br>');
                transunion_list_r7 = transunion_list_r7.join('<br>');
                equifax_list_r8 = equifax_list_r8.join('<br>');
                experian_list_r8 = experian_list_r8.join('<br>');
                transunion_list_r8 = transunion_list_r8.join('<br>');
                equifax_list_r9 = equifax_list_r9.join('<br>');
                experian_list_r9 = experian_list_r9.join('<br>');
                transunion_list_r9 = transunion_list_r9.join('<br>');



                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND1}/g,equifax_list_r1);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND1}/g,experian_list_r1);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND1}/g,transunion_list_r1);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND2}/g,equifax_list_r2);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND2}/g,experian_list_r2);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND2}/g,transunion_list_r2);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND3}/g,equifax_list_r3);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND3}/g,experian_list_r3);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND3}/g,transunion_list_r3);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND4}/g,equifax_list_r4);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND4}/g,experian_list_r4);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND4}/g,transunion_list_r4);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND5}/g,equifax_list_r5);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND5}/g,experian_list_r5);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND5}/g,transunion_list_r5);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND6}/g,equifax_list_r6);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND6}/g,experian_list_r6);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND6}/g,transunion_list_r6);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND7}/g,equifax_list_r7);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND7}/g,experian_list_r7);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND7}/g,transunion_list_r7);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND8}/g,equifax_list_r8);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND8}/g,experian_list_r8);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND8}/g,transunion_list_r8);

                fetched_template = fetched_template.replace(/{EQUIFAX ACCOUNT LIST RND9}/g,equifax_list_r9);
                fetched_template = fetched_template.replace(/{EXPERIAN ACCOUNT LIST RND9}/g,experian_list_r9);
                fetched_template = fetched_template.replace(/{TRANSUNION ACCOUNT LIST RND9}/g,transunion_list_r9); 
                

                

                if (my_tinymce == 'creditor') {
                    var creditor_details = $('#select_letter_creditors').val();
                    var account_number = $('#select_client_accounts').val();

                    // creditor_details = creditor_details.split('_');
                    // $.each(creditor_details,function(key,value){
                    //     value =
                    // // });
                    // console.log(creditor_details);
                    // console.log(account_number);

                    fetched_template = fetched_template.replace(/{CREDITOR DETAILS}/g,creditor_details);
                    fetched_template = fetched_template.replace(/{ACCOUNT#}/g,account_number);

                } else {

                    var pagebreak_avssn = '<!-- pagebreak -->\
                                                <div style="text-align: center"><img src="'+avssn_link+'" alt="no avssn"/></div>\
                                            <!-- pagebreak -->\
                                            ';
                    var pagebreak_avssn_end = '<!-- pagebreak -->\
                                                <div style="text-align: center"><img src="'+avssn_link+'" alt="no avssn"/></div>\
                                            ';
                    if (avssn_link != '') {
                        fetched_template = fetched_template+pagebreak_avssn_end;    
                    }
                    

                }

                    

                tinymce.get(my_tinymce+'MCE').setContent(fetched_template);


                if (client_type == 'joint') {
                    fetchDataTemplate(client_joint_id,'joint',fetched_template,my_tinymce);
                } else {
                    fetchDataTemplate(client_id,'client',fetched_template,my_tinymce);
                }


            });
    }

    function fetchDataTemplate(client_id,type,template,my_tinymce) {
        $.post('<?php echo base_url('admin/clients/getProfile') ?>',
            {client_id,type},function(data){
                data = JSON.parse(data);
                var curdate = moment().format('MMMM D, YYYY');
                var current_date = moment().format('MM/DD/YYYY');;
                $.each(data['profile'],function(key,value){
                    var name = $.trim(value.name);
                    var address = $.trim(value.address);
                    var city = $.trim(value.city);
                    var state = $.trim(value.state_province);
                    var zip = $.trim(value.zip_postal_code);
                    var social = $.trim(value.ss_restricted);
                    var ss = $.trim(value.ss);
                    var cell_phone = $.trim(value.cell_phone);
                    var dob = moment(value.date_of_birth).format('MM/DD/YYYY');

                    var fetched_template = template;
                    fetched_template = fetched_template.replace(/{NAME}/g,name);
                    fetched_template = fetched_template.replace(/{ADDRESS}/g,address);
                    fetched_template = fetched_template.replace(/{CITY}/g,city);
                    fetched_template = fetched_template.replace(/{STATE}/g,state);
                    fetched_template = fetched_template.replace(/{ZIP}/g,zip);
                    fetched_template = fetched_template.replace(/{CURDATE}/g,curdate);
                    fetched_template = fetched_template.replace(/{CURRENT_DATE}/g,current_date);
                    fetched_template = fetched_template.replace(/{SOCIAL}/g,social);
                    fetched_template = fetched_template.replace(/{SSN}/g,social);
                    fetched_template = fetched_template.replace(/{DOB}/g,dob);
                    fetched_template = fetched_template.replace(/{FULL_SOCIAL}/g,ss);
                    fetched_template = fetched_template.replace(/{PHONE}/g,cell_phone);
                    // template.html(fetched_template);
                    tinymce.get(my_tinymce+'MCE').setContent(fetched_template);
                });
            });
    }

    function jsUcfirst(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getTemplate(my_tinymce,template_title,template_header) {
        $.post('<?php echo base_url('admin/clients/getTemplate') ?>',
            {template_title,template_header},function(data){
                data = JSON.parse(data);
                console.log(data);
                if (data.length != 0) {
                    if (data[0]['template'] != null) {
                        // console.log(my_tinymce+'MCE');
                        tinymce.get(my_tinymce+'MCE').setContent(data[0]['template']);
                        if (!$('.btnFetchDataTemplate'+jsUcfirst(my_tinymce)).hasClass('hide')) {
                            $('.btnFetchDataTemplate'+jsUcfirst(my_tinymce)).trigger('click');
                        }
                        // $('.btnFetchDataTemplateBureau').trigger('click');

                        //var font = $('select[name="profile_font"]').val();
                        // $('#bureauMCE div').css('font-family',font);
                        // $('#bureauMCE font').css('font-family',font);
                        // $('#bureauMCE span').css('font-family',font);


                    } else {
                        tinymce.get(my_tinymce+'MCE').setContent('');
                    }

                } else {
                    tinymce.get(my_tinymce+'MCE').setContent('');
                }


            });
    }



    function getTemplateTitles(template_header) {
        $.post('<?php echo base_url('admin/clients/getTemplateTitles') ?>',
            {template_header},function(data){
                data = JSON.parse(data);
                var template_head = 'BureauLetter';
                var select_template_title = $('#bureau_letters_template_title');
                var table_name = 'table'+template_head+'TemplateTitles';
                if (template_header == 'Creditor Letters') {
                    template_head = 'CreditorLetter';
                    select_template_title = $('#template_title_creditor');
                    table_name = 'table'+template_head+'TemplateTitles';
                }
                select_template_title.empty();
                select_template_title.append('<option value="">Select Title</option>');
                var tableTemplateTitles = $('#'+table_name+' tbody');
                tableTemplateTitles.empty();

                var tools = '<div class="btn-group">\
                            <button func="edit" class="btnEdit'+template_head+'TemplateTitle btn btn-success btn-sm"><i class="fas fa-edit"></i></button>\
                            <button class="btnDelete'+template_head+'TemplateTitle btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>\
                        </div>';
                $.each(data, function(index, val) {
                    tableTemplateTitles.append('<tr blt_id="'+val.blt_id+'">\
                                                                <td>'+val.template_title+'</td>\
                                                                <td class="text-center">'+tools+'</td>\
                                                            </tr>');

                    select_template_title.append('<option value="'+val.template_title+'">'+val.template_title+'</option>');
                });
            });
    }

    function saveBureauLetterTemplate(template_header,template_title,template){
        $.post('<?php echo base_url('admin/clients/saveBureauLetterTemplate') ?>',
            {template_header,template_title,template},function(data){
                // getClientDocuments(client_id);
                // console.log(data);
            });
    }

    function deleteCLientDocument(client_type,doc_id){
        $.post('<?php echo base_url('admin/clients/deleteCLientDocument') ?>',
            {client_type,doc_id},function(data){
                if (client_type == 'joint') {
                    getClientDocuments(client_joint_id,client_type);
                } else {
                    getClientDocuments(client_id,client_type);
                }
            });
    }
    var avssn_link = '';
    function getClientDocuments(client_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getClientDocuments') ?>',
            {client_id,client_type},function(data){
                // console.log(data);
                data = JSON.parse(data);
                tableClientDocuments.clear().draw();
                var category_select = '\
                                <option value="Credit Report">Credit Report</option>\
                                <option value="Equifax">Equifax</option>\
                                <option value="Experian">Experian</option>\
                                <option value="TransUnion">TransUnion</option>\
                                <option value="Service Agreement">Service Agreement</option>\
                                <option value="New Payment">New Payment</option>\
                                <option value="Social">Social</option>\
                                <option value="Address (1)">Address (1)</option>\
                                <option value="Address (2)">Address (2)</option>\
                                <option value="AVSSN">AVSSN</option>\
                                <option value="Creditor Letters">Creditor Letters</option>\
                                <option value="Other">Other</option>\
                            ';
                $('#formDocumentFileUpload').find('[name="category"]').html(category_select);
                var folder_name = $('[name="profile_name"]').val();
                var hide_from_select_document_category = [];
                $.each(data,function(key,value){
                    var checkbox = '<input type="checkbox" id="basic_checkbox_'+value.doc_id+'" class="filled-in chk-col-cyan ">\
                            <label for="basic_checkbox_'+value.doc_id+'"></label>';
                    var btnDownload = '<a class="text-success btnDownloadDocument" download href="<?php echo base_url('assets/documents/') ?>'+folder_name+'/'+value.file_download+'"> <i class="fas fa-download  fa-lg"></i> </a>';
                    var btnPreview = '<a class="btnPreview text-info" href="#"> <i class="fas  fa-eye fa-lg"></i> </a>';
                    var btnPreviewHref = '<a target="_blank" class=" text-info" href="'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
                    var btnDelete = '<a class="btnDeleteClientDocument text-danger" href="#"> <i class="fas  fa-trash fa-lg"></i> </a>';
                    var file_size = ((value.file_size / 1000) / 1000).toFixed(2);

                    var select_category = ' <select class="select_document_category" name="document_category_'+value.doc_id+'">\
                                        <option value="Credit Report">Credit Report</option>\
                                        <option value="Equifax">Equifax</option>\
                                        <option value="Experian">Experian</option>\
                                        <option value="TransUnion">TransUnion</option>\
                                        <option value="Service Agreement">Service Agreement</option>\
                                        <option value="New Payment">New Payment</option>\
                                        <option value="Social">Social</option>\
                                        <option value="Address (1)">Address (1)</option>\
                                        <option value="Address (2)">Address (2)</option>\
                                        <option value="AVSSN">AVSSN</option>\
                                        <option value="Creditor Letters">Creditor Letters</option>\
                                        <option value="Other">Other</option>\
                                    </select>';

                    if (value.category == 'New Payment Authorization Form' || value.category == 'OH Premiere' || value.category == 'Service Agreement' || value.category == 'Federal Disclosure' || value.category == 'Esignature Disclosure' || value.category == 'Billing Authorization Form' || value.category == 'Cancellation' || (value.category).indexOf("Attachment from Gmail") !== -1) { 
                        var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id; 

                    } else if ((value.category).indexOf('Invoice') !== -1) { 
                        var btnPreviewHref = '<a target="_blank" class=" text-info" href="<?php echo base_url('emailCampaign/invoice?id=') ?>'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
                        var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id;    
                    } else if ((value.category).indexOf('Receipt') !== -1) { 
                        var btnPreviewHref = '<a target="_blank" class=" text-info" href="<?php echo base_url('emailCampaign/receipt?id=') ?>'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> </a>';
                        var newRow = tableClientDocuments.row.add([checkbox,value.category,'',moment(value.date_uploaded).format('lll'),btnPreviewHref+' '+btnDelete ]).node().id = value.doc_id;    
                    } else {
                        var newRow = tableClientDocuments.row.add([checkbox,select_category,file_size+'MB',moment(value.date_uploaded).format('lll'),btnPreview+' '+btnDownload+' '+btnDelete ]).node().id = value.doc_id;
                    }

                    if (value.category == 'Social' || value.category == 'Address (1)' || value.category == 'Address (2)') {
                        hide_from_select_document_category.push(value.category);
                    }

                    if (value.category == 'AVSSN') {
                        avssn_link = '<?php echo base_url('assets/documents/') ?>'+folder_name+'/'+value.file_download;
                    }

                    tableClientDocuments.draw(false);
                    $('select[name="document_category_'+value.doc_id+'"]').val(value.category);

                    $('select[name="document_category_'+value.doc_id+'"]').on('change', function(event) {
                        event.preventDefault();
                        var doc_id = $(this).closest('tr').attr('id');
                        var category = $(this).val();
                        var data =  {
                                        table: client_type == 'client' ? 'client_documents' : 'client_joint_documents',
                                        pk: doc_id,
                                        action: 'save',
                                        fields: {
                                                    category: category
                                                },
                                        return: 'doc_id'
                                    };
                        
                        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data , function(data, textStatus, xhr) { 
                            console.log(data);
                        });

                    });

                    tableClientDocuments.draw(false);
                });

                $.each(hide_from_select_document_category, function(index, val) {
                     $('.select_document_category').find('option[value="'+val+'"]').addClass('hide');
                     $('#formDocumentFileUpload').find('[name="category"]').find('option[value="'+val+'"]').addClass('hide');
                });


            });
    }

    function saveNewTaskTemplate(task_shortname,task_template) {
        task_shortname_ = task_shortname;
        task_shortname = task_shortname.val();
        task_template_ = task_template;
        task_template = task_template.val();
        var task_template_id = $('#new_task_template_id').val();
        // console.log(task_template_id);
        $.post('<?php echo base_url('admin/clients/saveTaskTemplate') ?>',
            {task_template_id,task_shortname,task_template},function(){
                task_shortname_.val('');
                task_template_.val('');
                $('#new_task_template_id').val('');
                getTaskTemplates();
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getTaskTemplates() {
        $.post('<?php echo base_url('admin/clients/getTaskTemplates') ?>',
            function(data){
                data = JSON.parse(data);
                tableTaskTemplates.clear().draw();
                $.each(data,function(key,value){
                    var editButton = '<a href="#">'+value.task_shortname+'</a>';
                    var newRow = tableTaskTemplates.row.add([editButton,value.task_template]).node().id = value.task_template_id;
                    tableTaskTemplates.draw(false);


                    var label = '<label class="col-md-6 lh" style="color: #6ec4d7; cursor: pointer;" task_temp="'+value.task_template+'">'+value.task_shortname+'</label>';
                    $('#task_templates_container').append(label);
                });
            });
    }

    function saveNewNoteTemplate(note_shortname,note_template) {
        note_shortname_ = note_shortname;
        note_shortname = note_shortname.val();
        note_template_ = note_template;
        note_template = note_template.val(); 
        // console.log(note_template_id);
        $.post('<?php echo base_url('admin/clients/saveNoteTemplate') ?>',
            {note_shortname,note_template},function(){
                note_shortname_.val('');
                note_template_.val('');
                $('#new_note_template_id').val('');
                getNoteTemplates();
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getNoteTemplates() {
        $.post('<?php echo base_url('admin/clients/getNoteTemplates') ?>',
            function(data){
                data = JSON.parse(data);
                tableNoteTemplates.clear().draw();
                $.each(data,function(key,value){
                    var note_template_id = value.note_template_id;
                    var note_shortname = value.note_shortname;
                    var note_template = value.note_template; 
                    var btn_delete_note_template = '<button class="btn_delete_note_template btn btn-danger"><i class="fas fa-trash"></i></button>';
                    var newRow = tableNoteTemplates.row.add([value.note_shortname,value.note_template,btn_delete_note_template]).node().id = value.note_template_id;
                    tableNoteTemplates.draw(false);

                    var label = '<label class="col-md-2 note_template" style="color: #6ec4d7; cursor: pointer;" note_template="'+note_template+'">'+note_shortname+'</label>';
                    $('.note_templates_container').append(label);
                });

            });
    }

    $('#tableNoteTemplates').on('click', '.btn_delete_note_template', function(event) {
        event.preventDefault();
        var note_template_id = $(this).closest('tr').attr('id');
        var tr = $(this).closest('tr');
        var data =  {
                        table: 'note_templates',
                        pk: note_template_id,
                        action: 'delete'
                    }; 
        $.post('<?php echo base_url('admin/clients/modelTable') ?>', data, function(data, textStatus, xhr) { 
            tr.remove();
        });
    });
    function saveNewCallTemplate(call_shortname,call_template,call_template_type) {
        call_shortname_ = call_shortname;
        call_shortname = call_shortname.val();
        call_template_ = call_template;
        call_template = call_template.val();
        var call_template_id = $('#new_call_template_id').val();
        // console.log(call_template_id);
        $.post('<?php echo base_url('admin/clients/saveCallTemplate') ?>',
            {call_template_id,call_shortname,call_template,call_template_type},function(){
                call_shortname_.val('');
                call_template_.val('');
                $('#new_call_template_id').val('');
                getCallTemplates(call_template_type);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function getCallTemplates(call_template_type) {
        $.post('<?php echo base_url('admin/clients/getCallTemplates') ?>',{call_template_type},
            function(data){
                data = JSON.parse(data);
                tableCallTemplates.clear().draw();
                $('.call_templates_container').empty();
                $.each(data,function(key,value){
                    var call_template_id = value.call_template_id;
                    var call_shortname = value.call_shortname;
                    var call_template = value.call_template;
                    var editButton = '<a href="#">'+value.call_shortname+'</a>';
                    var newRow = tableCallTemplates.row.add([editButton,value.call_template]).node().id = value.call_template_id;
                    tableCallTemplates.draw(false);

                    var label = '<label class="col-md-2 call_template" style="color: #6ec4d7; cursor: pointer;" call_template="'+call_template+'">'+call_shortname+'</label>';
                    $('.call_templates_container').append(label);
                });

            });
    }

    function saveSupportTicketResponse(support_id,response){
        $.post('<?php echo base_url('admin/clients/saveSupportTicketResponse') ?>',
            {support_id,response},function(data){
                getSupportTicket(support_id);
                getTableSupportTickets(client_id);
                $('#selectSupportResponses').val('');
            });
    }

    function updateSupportTicketStatus(support_id,status){
        $.post('<?php echo base_url('admin/clients/updateSupportTicketStatus') ?>',
            {support_id,status},function(data){
                getTableSupportTickets(client_id);
            });
    }

    function getSupportTicket(support_id){
        $.post('<?php echo base_url('admin/clients/getSupportTicket') ?>',
            {support_id},function(data){
                data = JSON.parse(data);
                var assigned_to = '';
                $.each(data['support_tickets'],function(key,value){
                    $.each(value,function(k,v){
                        $('.support_'+k).val(v);
                    });
                    $('.support_support_date').val(moment(value.support_date).format('MM/DD/YYYY h:mm:ss a'));
                    // $('#support_assigned_to').html(value.assigned_to);
                    assigned_to = value.assigned_to;
                });
                var tbodySupportTicketReponsesRight = $('#tbodySupportTicketReponsesRight');
                tbodySupportTicketReponsesRight.empty();
                if ((data['support_ticket_responses']).length != 0) {
                    $.each(data['support_ticket_responses'],function(key,value){
                        var responder = value.responder == 'Administrator' ? 'Support Team' : value.responder;
                        var newTr = '<tr>\
                                        <td >\
                                            <div>\
                                                <label>'+responder+'</label>\
                                                <span class="pull-right">'+moment(value.response_date).format('MM/DD/YYYY h:mm:ss a')+'</span>\
                                                <blockquote style="white-space: pre-wrap !important;line-height: 18px !important">'+value.response+'</blockquote>\
                                            </div>\
                                        </td>\
                                    </tr>';

                        tbodySupportTicketReponsesRight.append(newTr);


                    });
                    $('.supportTblScroll').slimScroll({
                            start: 'bottom',
                            position: 'right'
                            , size: "5px"
                            , height: '300'
                            , color: '#dcdcdc'
                        });
                        $('.supportTblScroll').slimscroll({ scrollBy: '100px' });
                } else {
                    var newTr = '<tr>\
                                    <td>\
                                        no response found \
                                    </td>\
                                </tr>';

                    tbodySupportTicketReponsesRight.append(newTr);
                }
            });
    }

    function deleteSupportTicket(support_id){
        $.post('<?php echo base_url('admin/clients/deleteSupportTicket') ?>',
            {support_id},function(data){
                getTableSupportTickets(client_id);
            });
    }



    function getTableSupportTickets(client_id) {
        $.post('<?php echo base_url('admin/clients/getTableSupportTickets') ?>',
            {client_id},function(data){
                data = JSON.parse(data);
                var tbodySupportTicketListRight = $('#tbodySupportTicketListRight');
                tbodySupportTicketListRight.empty();
                var total_tickets = 0;
                var total_responded = 0;
                var total_resolved = 0;
                var total_pending = 0;
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        total_tickets++;
                        var label_color = '';
                        if (value.status == 'Pending') {
                            label_color = 'label-danger';
                            total_pending++;
                        }
                        if (value.status == 'Responded') {
                            total_responded++;
                            label_color = 'label-primary';
                        }
                        if (value.status == 'Resolved') {
                            total_resolved++;
                            label_color = 'label-success';
                        }

                                            // <td>\
                                            //     <button type="button" class="btnDeleteSupportTicket btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>\
                                            // </td>\
                        var newTicket = '<tr value="'+value.support_id+'">\
                                            <td><a href="#" class="supportTicket">'+value.subject+'</a></td>\
                                            <td><span class="label '+label_color+'">'+value.status+'</span> </td>\
                                            <td>'+value.assigned_to+'</td>\
                                            <td>'+moment(value.support_date).format('MM/DD/YYY h:mm:ss a')+'</td>\
                                        </tr>';
                        tbodySupportTicketListRight.append(newTicket);
                    });
                } else {
                    var newTicket = '<tr >\
                                        <td colspan="5">No support tickets found</td>\
                                    </tr>';
                        tbodySupportTicketListRight.append(newTicket);
                }

                if (total_pending != 0) {
                    $('#notif_support_right').html('( '+total_pending+' )');
                    // $.toast({
                    //     heading: 'Pending Support Ticket!',
                    //     text: 'there is a pending support ticket, check it out on Support tab!',
                    //     position: 'top-right',
                    //     loaderBg:'#ffc107',
                    //     icon: 'warning',
                    //     hideAfter: 3500,
                    //     stack: 6
                    //   });
                } else {
                    $('#notif_support_right').html('');
                }


                // $('#label_total_tickets').html(total_tickets);
                $('#label_responded_tickets_right').html(total_responded);
                $('#label_resolved_tickets_right').html(total_resolved);
                $('#label_pending_tickets_right').html(total_pending);


            });
    }

    function getTableByBureaus(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getTableByBureaus') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                tableByBureauEquifax.clear().draw();
                tableByBureauExperian.clear().draw();
                tableByBureauTransUnion.clear().draw();

                var total_equifax = 0;
                var fixed_equifax = 0;
                var total_experian = 0;
                var fixed_experian = 0;
                var total_transunion = 0;
                var fixed_transunion = 0;
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        var bureaus = value.bureaus;
                        var status = 'In Process';
                        if (bureaus == 'Equifax') {
                            total_equifax++;
                            if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                                status = 'Removed';
                                var newRow = tableByBureauEquifax.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                                $(newRow).addClass('table-success');
                                fixed_equifax++;
                            } else {
                                status = value.account_status;
                                tableByBureauEquifax.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                            }
                        }
                        if (bureaus == 'Experian') {
                            total_experian++;
                            if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                                status = 'Removed';
                                var newRow = tableByBureauExperian.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                                $(newRow).addClass('table-success');
                                fixed_experian++;
                            } else {
                                status = value.account_status;
                                tableByBureauExperian.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                            }
                        }
                        if (bureaus == 'TransUnion') {
                            total_transunion++;
                            if (value.account_status == 'Fixed' || value.account_status == 'Deleted' || value.account_status == 'Not Reporting') {
                                status = 'Removed';
                                var newRow = tableByBureauTransUnion.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();
                                $(newRow).addClass('table-success');
                                fixed_transunion++;
                            } else {
                                status = value.account_status;
                                tableByBureauTransUnion.row.add([value.account_name,value.account_number,value.account_type,status]).draw().node();;
                            }
                        }
                    });



                    $('.improvement_equifax').html(((fixed_equifax / total_equifax) * 100).toFixed(0) + '%');
                    $('#improvement_equifax_progressbar').css('width',((fixed_equifax / total_equifax) * 100).toFixed(0) + '%');
                    $('.improvement_experian').html(((fixed_experian / total_experian) * 100).toFixed(0) + '%');
                    $('#improvement_experian_progressbar').css('width',((fixed_experian / total_experian) * 100).toFixed(0) + '%');
                    $('.improvement_transunion').html(((fixed_transunion / total_transunion) * 100).toFixed(0) + '%');
                    $('#improvement_transunion_progressbar').css('width',((fixed_transunion / total_transunion) * 100).toFixed(0) + '%');
                }



            });
    }

    function getTableByAccount(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getTableByAccount') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                // console.log(data);
                tableByAccount.clear().draw();
                var total_accounts = 0;
                var total_fixed = 0;
                if (data.length != 0) {
                    $.each(data,function(key,value){
                        var account_name = value.account_name;
                        var account_number = value.account_number;
                        var account_type = value.account_type;
                        var bureaus = value.bureaus;
                        var account_status = value.account_status;

                        bureaus = bureaus.split(',');
                        account_status = account_status.split(',');

                        total_accounts += bureaus.length;

                        var Equifax = '-'
                        var Experian = '-';
                        var TransUnion = '-';
                        $.each(bureaus,function(key,value){
                            var bg_color = '';
                            if (account_status[key] == 'Fixed' || account_status[key] == 'Deleted' || account_status[key] == 'Not Reporting') {
                                account_status[key] = '<div class="labelRemoved" >Removed</div>';
                                total_fixed++;
                            } else {
                                // account_status[key] = 'In Process';
                            }




                            if (value == 'Equifax') {
                                Equifax = account_status[key];
                            }
                            if (value == 'Experian') {
                                Experian = account_status[key];
                            }
                            if (value == 'TransUnion') {
                                TransUnion = account_status[key];
                            }
                        });



                        tableByAccount.row.add([account_name,account_number,account_type,Equifax,Experian,TransUnion]);
                        tableByAccount.draw(false);

                    });

                    $('.labelRemoved').closest('td').addClass('border-green');

                    var improvement_all = ((total_fixed/total_accounts) * 100).toFixed(0);
                    $('.improvement_all').html(improvement_all+'%');
                    $('#improvement_all_progressbar').css('width',improvement_all+'%');
                }
                // if (data[0]['account_name'] != null) {

                // }

            });
    }


    function findNextPreviousClient(type,client_id) {
        $.post('<?php echo base_url('admin/clients/findNextPreviousClient') ?>',
            {type,client_id},function(data){
                // console.log(data);
            });
    }

    function saveNewTemplate(template_shortname,template_subject,template_notes) {
        template_shortname_ = template_shortname;
        template_shortname = template_shortname.val();
        template_subject_ = template_subject;
        template_subject = template_subject.val();
        template_notes_ = template_notes;
        template_notes = template_notes.val();
        var alert_template_id = $('#new_alert_template_id').val();
        // console.log(alert_template_id);
        $.post('<?php echo base_url('admin/clients/saveTemplate') ?>',
            {alert_template_id,template_shortname,template_subject,template_notes},function(){
                template_shortname_.val('');
                template_subject_.val('');
                template_notes_.val('');
                $('#new_alert_template_id').val('');
                getAlertTemplates();
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getClientGrade(card_holder,card_number) {
        $.post('<?php echo base_url('admin/clients/getClientGrade') ?>', {card_holder,card_number}, function(data, textStatus, xhr) {
            // alert(data);
            $('.client_grade').val(data);
            $('.client_grade').html(data);
        });
    }
    function getProfile(client_id,type) {
        $.post('<?php echo base_url('admin/clients/getProfile') ?>',
            {type,client_id},function(data){
                data = JSON.parse(data);
                $('#tab_profile input').val('');
                $('#tab_documents input').val('');
                $('#tab_billing_info input').val('');
                unBindOnChange();
                setPlansSelections(type,
                    data['profile'][0]['client_type'],
                    data['profile'][0]['joint_client_status'],
                    data['profile'][0]['client_name'],
                    data['profile'][0]['joint_name']
                    );


                setProfileTab(data['profile']);
                setBillingTab(data['billing']);
                setDocumentsTab(data['enrollment']);


                var inputs =  $('.formAddUser input');
                $.each(inputs,function(key,input){
                    $(input).closest('.form-group').addClass('focused');
                });

                $('.tab-pane .select2').trigger('change');

                functionsForProfileTab();
                functionsForBillingTab();
                functionsForEnrollmentTab();
                $('.tab-pane .select2').select2();

                if ((data['billing']).length != 0 ) {
                    if(data['billing'][0]['address'] == null || data['billing'][0]['address'] == '') {
                        $.each(data['profile'],function(key,value){
                            $('input[name=billing_address]').val(value.address);
                            $('input[name=billing_city]').val(value.city);

                            $('select[name=billing_state]').val(value.state_province);
                            $('.zip_postal_code').val(value.zip_postal_code);
                            $('select[name=billing_state],input[name=billing_address],input[name=billing_city],input[name=billing_zip]').trigger('change');
                        });
                    } else {
                        $.each(data['billing'],function(key,value){
                            $('input[name=billing_address]').val(value.address);
                            $('input[name=billing_city]').val(value.city);

                            $('select[name=billing_state]').val(value.state);
                            $('.zip_postal_code').val(value.zip);
                        });
                    }
                } else {
                    $.each(data['profile'],function(key,value){
                        $('input[name=billing_address]').val(value.address);
                        $('input[name=billing_city]').val(value.city);

                        $('select[name=billing_state]').val(value.state_province);

                        $('.zip_postal_code').val(value.zip_postal_code);
                        $('select[name=billing_state],input[name=billing_address],input[name=billing_city],input[name=billing_zip]').trigger('change');
                    });
                }


                getClientSubscriptions();;
                $('.right-sidebar-loader').fadeOut(50);;


        });
    }


    function unBindOnChange() {
        $('#sectionClientAssignments select,\
                    #sectionProfileInformation select,\
                    #sectionBillingInformation select,\
                    #sectionBillingInformation textarea,\
                    #sectionProfileInformation input[type=text],\
                    #sectionProfileInformation input[type=email],\
                    #sectionClientAssignments input[type=text],\
                    #profile_username,\
                    #profile_password,\
                    #profile_status_line,\
                    #sectionBillingInformation input[type=text],\
                    .sectionEnrollmentInformation input[type=text],\
                    #profile_client_status,\
                    #profile_lead_status,\
                    #dispute_date_for_result,\
                    #creditors_date_for_result,\
                    #profile_fax').unbind('change');
    }

    function setDocumentsTab(data) {
        $('#checkbox_account').prop('checked',false);
        $('#checkbox_dropoff').prop('checked',false);
        $('#checkbox_email').prop('checked',false);
        $('#checkbox_fax').prop('checked',false);
        $('#checkbox_mail').prop('checked',false);
        // console.log(data['enrollment']);
        $.each(data,function(key,value){
            $.each(value,function(k,v){
                $('input[type=text][name=enrollment_'+k+']').val(v);
                $('input[type=email][name=enrollment_'+k+']').val(v);
                $('input[type=password][name=enrollment_'+k+']').val(v);
                $('input[type=hidden][name=enrollment_'+k+']').val(v);
                $('select[name=enrollment_'+k+']').val(v);
                $('.'+k).html(v);
            });


            if (value.checkbox_account == '1') {
                $('#checkbox_account').prop('checked',true);
            }
            if (value.checkbox_dropoff == '1') {
                $('#checkbox_dropoff').prop('checked',true);
            }
            if (value.checkbox_email == '1') {
                $('#checkbox_email').prop('checked',true);
            }
            if (value.checkbox_fax == '1') {
                $('#checkbox_fax').prop('checked',true);
                get_clients_fax();
            }
            if (value.checkbox_mail == '1') {
                $('#checkbox_mail').prop('checked',true);
            }
        });

        get_clients_enrolled();
        get_clients_sa();
        get_clients_cr();
        get_clients_a1();
        get_clients_a2();


    }

    function setBillingTab(data) {
        $.each(data,function(key,value){
            $.each(value,function(k,v){
                $('input[type=text][name=billing_'+k+']').val(v);
                $('input[type=email][name=billing_'+k+']').val(v);
                $('input[type=password][name=billing_'+k+']').val(v);
                $('input[type=hidden][name=billing_'+k+']').val(v);
                $('select[name=billing_'+k+']').val(v);
                $('textarea[name=billing_'+k+']').val(v);
                $('.'+k).html(v);
                $('.billing_'+k).html(v);
            });
            getClientGrade(value.card_holder,value.card_number);
        });
    }

    function setProfileTab(data) {
        // console.log(data);
        $.each(data,function(key,value){
            client_id = value.client_id;
            client_joint_id = value.client_joint_id;

            var broker_id = value.broker_id;

            if (broker_id != '') {
                getBrokerDetails(broker_id);
            }

            $.each(value,function(k,v){
                $('input[type=text][name=profile_'+k+']').val(v);
                $('input[type=email][name=profile_'+k+']').val(v);
                $('input[type=password][name=profile_'+k+']').val(v);
                $('input[type=hidden][name=profile_'+k+']').val(v);
                $('select[name=profile_'+k+']').val(v);
                $('textarea[name=profile_'+k+']').val(v);
                $('.'+k).html(v);
                $('.profile_'+k).html(v);
                if (k == 'date_created' || k == 'last_login') {
                    if (v != '') {
                        v = moment(v).format('MM/DD/YYYY hh:mm A')
                        $('.'+k).html(v);
                    }

                }
                if (k == 'auto_alert') {
                    if (v == 1) {
                        $('#btnAlertResponder').html('ON');
                        $('#btnAlertResponder').removeClass('text-danger');
                        $('#btnAlertResponder').addClass('text-success');
                    } else {
                        $('#btnAlertResponder').html('OFF');
                        $('#btnAlertResponder').addClass('text-danger');
                        $('#btnAlertResponder').removeClass('text-success');
                    }
                }

                if (k == 'client_status') {
                    if (v == 'Archived') {
                        $('.labelArchived').removeClass('hide');
                    }
                    if (v == 'NSF' || v == 'NSF2' || v == 'Suspended') {
                        $('#modal_warning_alert').modal('show');
                    }
                }
            });

            if (value.client_status == 'NSF' || value.client_status == 'NSF2' || value.client_status == 'NSF3' || value.client_status == 'Terminated' || value.client_status == 'Cancelled') {
                $('.summary-client-status').removeClass('badge-success');
                $('.summary-client-status').addClass('badge-danger');
            } else {
                $('.summary-client-status').removeClass('badge-danger');
                $('.summary-client-status').addClass('badge-success');
            }

            var broker_name = value.broker_name;
            if (broker_name != '') {
                $("#broker_gameplan").attr('placeholder','Pending Broker GamePlan Instructions');
            } else {
                $("#broker_gameplan").attr('placeholder',"Don't have a broker or lender to help you during or after this process? Let us know if you need a list of recommended professionals in your area!");
            }

            var font = value.font;
            var dfr = value.dfr;
            var dfr = value.dfr;
            $('#profile_dfr').html(dfr);
            $('#profile_dfr_creditors').html(value.dfr_creditors);


            var login_folder = '<?php echo $userdata['login_folder'] ?>';
            if (login_folder == 'employees') {
                $('input[name=profile_ss]').removeClass('ss-inputmask');
                $('input[name=profile_ss]').val('***-**-'+value.ss_restricted);  
                $('input[name=profile_ss]').attr('maxlength', '11');
                // $('input[name=profile_ss]').inputmask({ mask: "[*]{100}", greedy: false });
            }

            setTinyMce();
            // get_automation_list(value.email_address);
        });
    }

    function getBrokerDetails(broker_id) {
        $.post('<?php echo base_url('admin/clients/getBrokerDetails') ?>', {broker_id}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            // console.log(data);;
            $.each(data, function(index, val) {
                $('.broker_cell').html(val.cell);
            });
        });
    }

    function setPlansSelections(type,client_type,client_status,client_name,joint_name){
        var subscription_types = "<?php echo addslashes(json_encode($subscription_type)) ?>";
        subscription_types = JSON.parse(subscription_types);
        var selectSubscription = $('#sectionBillingInformation').find('select[name="billing_plan"]');
        // console.log(client_status);
        if (client_type == 'Joint') {
            if (client_status != 'Archived') {
                $.each(subscription_types,function(key,value){
                    if ((value.name).indexOf(' Plus') !== -1) {
                        selectSubscription.append('<option description="'+value.description+'" setup_fee="'+value.setup_fee+'" monthly_fee="'+value.monthly_fee+'" value="'+value.name+'">'+value.name+'</option> ');
                    }
                });
            } else {
                $.each(subscription_types,function(key,value){
                    if ((value.name).indexOf(' Plus') === -1) {
                        selectSubscription.append('<option description="'+value.description+'" setup_fee="'+value.setup_fee+'" monthly_fee="'+value.monthly_fee+'" value="'+value.name+'">'+value.name+'</option> ');
                    }
                });
            }
        } else {
            $.each(subscription_types,function(key,value){
                if ((value.name).indexOf(' Plus') === -1) {
                    selectSubscription.append('<option description="'+value.description+'" setup_fee="'+value.setup_fee+'" monthly_fee="'+value.monthly_fee+'" value="'+value.name+'">'+value.name+'</option> ');
                }
            });
        }


        if (type != 'joint') {
            if (client_type == 'Joint') {
                $('.joint_container').removeClass('hide');
                $('.changeToOtherAccount').html(joint_name);
            }  else {
                $('.joint_container').addClass('hide');
                $('#btnConvertToJoint').removeClass('hide');
            }
        } else {
            $('.joint_container').removeClass('hide');
            $('#btnConvertToJoint').addClass('hide');
            $('.changeToOtherAccount').html(client_name);
        }
    }


    function checkIfUsernameExist(username,type) {
        var username_val = username.val();
        $.post('<?php echo base_url('admin/clients/checkIfUsernameExist') ?>',
            {username_val,type},function(data){
                // console.log(data);
                if (data == '0') {
                    username.removeClass('form-control-danger');
                    username.closest('.form-group').removeClass('has-danger');
                    username.closest('.form-group').removeClass('has-error');
                    username.closest('.form-group').find('.errorUsername').addClass('hide');
                } else {
                    username.addClass('form-control-danger');
                    username.closest('.form-group').addClass('has-danger');
                    username.closest('.form-group').addClass('has-error');
                    username.closest('.form-group').find('.errorUsername').removeClass('hide');
                }
            });
    }

    $('#formTimelineContent').on('change', '#Enrolled, #Dispute, #Result, #Repeat, #Complete',function(e){
        var Enrolled = $('#Enrolled').val();
        var Dispute = $('#Dispute').val();
        var Result = $('#Result').val();
        var Repeat = $('#Repeat').val();
        var Complete = $('#Complete').val();
        AddGameplanTimeline(Enrolled, Dispute, Result, Repeat, Complete, client_id,client_joint_id,client_type);
        // console.log(client_id+' '+client_joint_id+' '+client_type);
    });

    function AddGameplanTimeline(Enrolled, Dispute, Result, Repeat, Complete, client_id,client_joint_id,client_type){
        $.post('<?php echo base_url('admin/clients/AddGameplanTimeline') ?>', {Enrolled, Dispute, Result, Repeat, Complete, client_id,client_joint_id,client_type},function(data){
            // console.log(data);
            $.toast({
                heading: 'Update Success!',
                text: 'Timeline successfully saved!',
                position: 'top-right',
                loaderBg:'#17a2b8',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
        });
        getGameplanTimeline(client_id,client_joint_id,client_type);
    }

    function getGameplanTimeline(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getGameplanTimeline') ?>',{client_id,client_joint_id,client_type}, function(data){
            data = JSON.parse(data);
            // console.log(data);
            if (data.length > 0) {
                $.each(data, function(key, value){
                    var enrolled = value['enrolled'];
                    var dispute = value['dispute'];
                    var result = value['result'];
                    var repeat = value['repeat'];
                    var complete = value['complete'];
                    $('#Enrolled').val(enrolled);
                    $('#Dispute').val(dispute);
                    $('#Result').val(result);
                    $('#Repeat').val(repeat);
                    $('#Complete').val(complete);
                });
            } else {
                $('#Enrolled').val('');
                $('#Dispute').val('');
                $('#Result').val('');
                $('#Repeat').val('');
                $('#Complete').val('');
            }

        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

    $('#formCreditorsAccountType').on('submit', function(e){
        e.preventDefault();
        var creditors_account_type = $('#creditors_account_type').val();
        var creditors_account_type_id = $('#creditors_account_type_id').val();
        saveCreditorsAccountType(creditors_account_type_id,creditors_account_type);
    });

    function saveCreditorsAccountType(creditors_account_type_id,creditors_account_type) {
        $.post('<?php echo base_url('admin/clients/saveCreditorsAccountType') ?>', {creditors_account_type_id,creditors_account_type}, function(data){
            // data = JSON.parse(data);
            $('#creditors_account_type').val('');
            $('#creditors_account_type_id').val('');
            getTableCreditorsAccountType();
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });

        // getDropdownGameplanAccountTable();
    }

    $('#formCreditorsRec').on('submit', function(e){
        e.preventDefault();
        var creditors_rec_id = $('#creditors_rec_id').val();
        var creditors_rec = $('#creditors_rec').val();
        saveCreditorsRec(creditors_rec_id,creditors_rec);
    });

    function saveCreditorsRec(creditors_rec_id,creditors_rec) {
        $.post('<?php echo base_url('admin/clients/saveCreditorsRec') ?>', {creditors_rec_id,creditors_rec}, function(data){
            // data = JSON.parse(data);
            $('#creditors_rec').val('');
            $('#creditors_rec_id').val('');
            getTableCreditorsRec();
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });

        // getDropdownGameplanAccountTable();
    }

    function getTableCreditorsAccountType() {
        $.post('<?php echo base_url('admin/clients/getCreditorsAccountType') ?>', function(data){
            data = JSON.parse(data);
            tableCreditorsAccountType.clear().draw();
            $('#select_creditors_account_type').empty();
            $.each(data, function(key, value) {
                var input = '<input readonly type="text" class="b-0" value="'+value.creditors_account_type+'" style="width: 100%"/>';
                var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditAccountType"><i class="fas fa-edit"></i></button>';
                var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteAccountType"><i class="fas fa-trash"></i></button>';
                var newRow = tableCreditorsAccountType.row.add([input,editButton+deleteButton]).node().id = value.creditors_account_type_id;
                tableCreditorsAccountType.draw(false);
                var cols = '<option  value="'+value.creditors_account_type+'">'+value.creditors_account_type+'</option>';
                $('#select_creditors_account_type').append(cols);
            });

        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }


    function getTableCreditorsRec() {
        $.post('<?php echo base_url('admin/clients/getTableCreditorsRec') ?>', function(data){
            data = JSON.parse(data);
            tableCreditorsRec.clear().draw();
            $('#select_creditors_rec').empty();
            $.each(data, function(key, value) {
                var input = '<input readonly type="text" class="b-0" value="'+value.creditors_rec+'" style="width: 100%"/>';
                var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditCredtorsRec"><i class="fas fa-edit"></i></button>';
                var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteCredtorsRec"><i class="fas fa-trash"></i></button>';
                var newRow = tableCreditorsRec.row.add([input,editButton+deleteButton]).node().id = value.creditors_rec_id;
                tableCreditorsRec.draw(false);
                // // var col = '<option value="'+creditors_rec_id+'">'+creditors_rec+'</option>';
                // var col = '<a href="#" creditors_rec_id="'+value.creditors_rec_id+'" class="optclick">'+value.creditors_rec+'</a>';
                // var newRow = tableCreditorsRec.row.add([col]).draw().node();

                var cols = '<option  value="'+value.creditors_rec+'">'+value.creditors_rec+'</option>';
                $('#select_creditors_rec').append(cols);
            });

        }).fail(function(xhr){
            console.log(xhr.responseText);
        });


    }

    $('#tableCreditorsRec').on('click','.optclick', function(e){
            e.preventDefault();
            var creditors_rec = $(this).html();
            var creditors_rec_id = $(this).attr('creditors_rec_id');

            $('#creditors_rec').val(creditors_rec);
            $('#creditors_rec_id').val(creditors_rec_id);
    });

    function getTasks(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getTasks') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-default waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }
                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a><br><a style="cursor: pointer" class="sl-date link editTaskDate">'+moment(value.task_date).format('lll')+'</a>\
                                                <div class="input-group">\
                                                    <blockquote class="editTaskTask form-control" style="font-size: 70%;cursor: pointer">'+value.task+'</blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });
            });
    }
    function saveTask(button,client_id,client_joint_id,client_type,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/saveTask') ?>',
            {client_id,client_joint_id,client_type,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(client_id,client_joint_id,client_type);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/updateTask') ?>',
            {task_id,task_active},function(data){
                getTasks(client_id,client_joint_id,client_type);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function getNotes(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getNotes') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-default waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }
                    var newNote = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a >'+value.sender_name+'</a><br>\
                                            <a href="#" class="editNoteDate sl-date">'+moment(value.note_date).format('lll')+'</a>\
                                            <a href="#" class="deleteNote text-danger pull-right">Delete <i class="fas fa-sm fa-trash"></i></a>\
                                                <div class="input-group">\
                                                    <blockquote style="cursor:pointer" class="form-control editNoteNote">'+value.note+'</blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });
            });
    }
    function saveNote(button,client_id,client_joint_id,client_type,note,note_sticky,note_date){
        var note_ = note;
        var note = note.val();
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/saveNote') ?>',
            {client_id,client_joint_id,client_type,note,note_sticky,note_date},function(data){
                // console.log(data);
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                //getNotes(client_id,client_joint_id,client_type);
                get_comm_flow();
                $('#modal_add_note').modal('hide');
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function saveCall(button,client_id,client_joint_id,client_type,call,call_date,call_type){
        var call_ = call;
        var call = call.val();
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/savecall') ?>',
            {client_id,client_joint_id,client_type,call,call_date,call_type},function(data){
                // console.log(data); 
                call_.val('');
                //getcalls(client_id,client_joint_id,client_type);
                get_comm_flow();
                $('#modal_add_call').modal('hide');
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/updateNote') ?>',
            {note_id,note_sticky},function(data){
                //getNotes(client_id,client_joint_id,client_type);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }



    function getAlerts(client_id,client_joint_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getAlerts') ?>',
            {client_id,client_joint_id,client_type},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#alert_list').empty();
                $.each(data,function(key,value){
                    var img = '';
                    if (value.sender != '') {
                        img = '<img width="30px" src="../assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="" class="img-circle">';
                    }

                                                // <span> '+img+'<a class="link">'+value.sender_name+'</a>\
                                                // </span>\

                    var alert_class = '';
                    if ($('.joint_container').hasClass('hide') == false) {
                        alert_class = 'link changeAlertClientType';
                    }
                    var alert_notes = (value.alert_notes).replace(/\\/g,'');
                    var client_name = (value.client_name).replace(/(\\|\/)/g,'');
                    var newAlert = '<div class="sl-item" style="margin-bottom: 5px;line-height: 100%" value="'+value.alert_id+'">\
                                        <div class="sl-right">\
                                            <div>\
                                                <div class="input-group" style="margin-top: 5px">\
                                                    <blockquote style="line-height: 100%;" class="form-control">\
                                                        <span class="sl-date pull-left"><a class="deleteAlert" href="#"><i class="fas fa-trash text-danger"></i></a></span>\
                                                        <span class="sl-date pull-right"><a class="editAlertDate" href="#">'+moment(value.alert_date).format('ll')+'</a></span><br>\
                                                        <span style="line-height: 100%;font-size: 80%"><a class="'+alert_class+'">'+client_name+'</a>: <a class="editAlertSubject link" style="cursor:pointer;">'+value.alert_subject+'</a></span><br><br>\
                                                        <p  style="line-height: 100%;font-size:70%;text-align: justify">Notes: <span class="editAlertNotes" style="cursor:pointer">'+alert_notes+'</span></p>\
                                                    </blockquote>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#alert_list').append(newAlert);
                });
            });
    }

    function saveAlert(button,client_id,client_joint_id,client_type,alert_subject,alert_date,alert_notes){
        var alert_subject_ = alert_subject;
        var alert_subject = alert_subject.val();
        var alert_date_ = alert_date;
        var alert_date = alert_date.val();
        var alert_notes_ = alert_notes;
        var alert_notes = alert_notes.val();
        button.prop('disabled',true);
        $.post('<?php echo base_url('admin/clients/saveAlert') ?>',
            {client_id,client_joint_id,client_type,alert_subject,alert_date,alert_notes},function(data){
                // console.log(data);
                alert_subject_.val('');
                alert_date_.val('<?php echo date('m/d/Y') ?>');
                alert_notes_.val('');
                getAlerts(client_id,client_joint_id,client_type);
                button.prop('disabled',false);

            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }
    function updateFields(table,client_id,field,value,title,selected) {
        // if (value != '') {
            $.post('<?php echo base_url('admin/clients/updateFields') ?>',
                {table,client_id,field,value},function(data){
                    // console.log(data);
                    if (title != null) {
                        if (selected != 'no prompt') {
                            if (selected != null) {
                                value = selected;
                            }
                            $.toast({
                                heading: 'Update Success!',
                                text: title+' successfully updated to '+value,
                                position: 'top-right',
                                loaderBg:'#17a2b8',
                                icon: 'success',
                                hideAfter: 3500,
                                stack: 6
                              });
                            if (field == 'date_for_result') { 
                                getProfileDetails(client_id,client_joint_id,client_type,false);
                            }
                            if (field == 'date_for_result_creditors') { 
                                getProfileDetails(client_id,client_joint_id,client_type,false);
                            }
                            if (field == 'date_of_birth') {
                                $('input[name=profile_date_of_birth]').val(moment(value).format('MM/DD/YYYY'));
                            }
                        }

                    }

                    if (field == 'font') {
                        // $('body').css('font-family',value);
                        // $('#bureauMCE').css('font-family',value);
                    }
                    // console.log(data);



                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });

        // }
    }

    function updateFieldsAccounts(by,table,account_id,field,value,title,selected) {

            $.post('<?php echo base_url('admin/clients/updateFieldsAccounts') ?>',
                {table,account_id,field,value},function(data){
                    console.log(data);
                    if (title != null) {
                        if (selected != 'no prompt') {
                            if (selected != null) {
                                value = selected;
                            }
                            $.toast({
                                heading: 'Update Success!',
                                text: title+' successfully updated to '+value,
                                position: 'top-right',
                                loaderBg:'#17a2b8',
                                icon: 'success',
                                hideAfter: 3500,
                                stack: 6
                              });

                            if (by == 'by_account_number') {
                                if (field == 'account_status' || field == 'account_type' ) {
                                    getAccountDetailsByAccountNumber(client_type,account_number,date_added);
                                }

                                if (field == 'account_status') {
                                    if (client_type == 'joint') {
                                        getResultsTracker(client_joint_id,client_type);
                                    } else {
                                        getResultsTracker(client_id,client_type);
                                    }
                                }
                            }
                        }

                    }

                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });


    }
    function updateFieldsCreditorsAccounts(table,ca_id,field,value,title,selected) {
            $.post('<?php echo base_url('admin/clients/updateFieldsCreditorsAccounts') ?>',
                {table,ca_id,field,value},function(data){
                    if (title != null) {
                        if (selected != 'no prompt') {
                            if (selected != null) {
                                value = selected;
                            }
                            $.toast({
                                heading: 'Update Success!',
                                text: title+' successfully updated to '+value,
                                position: 'top-right',
                                loaderBg:'#17a2b8',
                                icon: 'success',
                                hideAfter: 3500,
                                stack: 6
                            });

                        }
                    }

                }).fail(function(xhr){
                    console.log(xhr.responseText);
                });


    }

    function functionsForProfileTab() {
        if ($('input[name="billing_card_holder"]').val() == '') {
            $('input[name="billing_card_holder"]').val($('input[name="profile_name"]').val());
            $('input[name="billing_card_holder"]').trigger('change');
        }
        $('#sectionProfileInformation').find('input[name="profile_address"]').on('change', function(event) {
            event.preventDefault();
            update_billing_address(); 
        });
        $('#sectionProfileInformation').find('input[name="profile_city"]').on('change', function(event) {
            event.preventDefault();
            update_billing_address(); 
        });
        $('#sectionProfileInformation').find('select[name="profile_state_province"]').on('change', function(event) {
            event.preventDefault();
            update_billing_address(); 
        });
        $('#sectionProfileInformation').find('input[name="profile_zip_postal_code"]').on('change', function(event) {
            event.preventDefault();
            update_billing_address(); 
        });
        $('#sectionProfileInformation').find('input[name="profile_name"]').on('change', function(event) {
            event.preventDefault();
            update_billing_address(); 
        });
        $('#sectionClientAssignments select').on('change',function(){
            var select = $(this);
            var title = select.closest('.input-group').find('label').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var selected = select.find('option:selected').text();
            var value = select.val();
            if (client_type == 'joint') {
                updateFields('client_joint_assignments',client_joint_id,field,value,title,selected);
            } else {
                updateFields('client_assignments',client_id,field,value,title,selected);
            }

        });
        $('#sectionClientAssignments input[type=text]').on('change',function(){
            var input = $(this);
            var title = input.closest('.input-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('profile_','');
            var value = input.val();
            updateFields('clients',client_id,field,value,title);
        });
        $('#sectionProfileInformation input[type=text],#sectionProfileInformation input[type=email]').on('change',function(){
            var input = $(this);
            var title = input.closest('.input-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('profile_','');
            var value = input.val();
            if (!$(this).closest('section').hasClass('containerClientOtherEmails')) {
                if (client_type == 'joint') {
                    if (field=='address' || field=='city' || field=='state_province' || field=='zip_postal_code' || field=='date_of_birth' || field=='ss' || field=='email_address' || field=='name'|| field=='cell_phone'|| field=='date_for_result' || field=='date_for_result_creditors') {
                        updateFields('client_joints',client_joint_id,field,value,title); 
                    } else {
                        updateFields('clients',client_id,field,value,title);
                    }
                } else {
                    updateFields('clients',client_id,field,value,title);
                }
            }


        });

        $('#sectionProfileInformation select').on('change',function(){
            var select = $(this);
            var title = select.closest('.input-group').find('label').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var value = select.val();
            if (field == 'carrier' || field == 'font' || field=='state_province') {
                if (client_type == 'joint') {
                    updateFields('client_joints',client_joint_id,field,value,title);
                } else {
                    updateFields('clients',client_id,field,value,title);
                }
            } else {
                updateFields('clients',client_id,field,value,title);
            }

        });
        $('#profile_password').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'password',value,'Password');
            } else {
                updateFields('clients',client_id,'password',value,'Password');

            }

        });
        $('#profile_username').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'username',value,'Username');
            } else {
                updateFields('clients',client_id,'username',value,'Username');

            }

        });



        $('#profile_status_line').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'status_line',value,'Status Line');
            } else {
                updateFields('clients',client_id,'status_line',value,'Status Line');
            }

        });
        $('#profile_status_line_creditors').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'status_line_creditors',value,'Creditors Status Line');
            } else {
                updateFields('clients',client_id,'status_line_creditors',value,'Creditors Status Line');
            }

        });

        $('#profile_fax').on('change',function(){
            var input = $(this);
            var value = input.val();
            updateFields('clients',client_id,'fax',value,'Fax');
        });
        $('#dispute_date_for_result').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'date_for_result',value,'Date For Results');
            } else {
                updateFields('clients',client_id,'date_for_result',value,'Date For Results');
            }
        });
        $('#creditors_date_for_result').on('change',function(){
            var input = $(this);
            var value = input.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,'date_for_result_creditors',value,'Date For Results');
            } else {
                updateFields('clients',client_id,'date_for_result_creditors',value,'Date For Results');
            }
        });

        $('#profile_client_status').on('change',function(){
            var select = $(this);
            var title = select.closest('.input-group').find('label').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var value = select.val();
            if (client_type == 'joint') {
                updateFields('client_joints',client_joint_id,field,value,title);
            } else {
                updateFields('clients',client_id,field,value,title);
            }


            var client_status = $('#profile_client_status').val();
            var billing_monthly_due = moment($('[name="billing_monthly_due"]').val()).format('Do');
            $('#timeline_status').html(value);
 
            get_client_status_timelines();
 
            var restricted_status = ['Cancelled','Archived','Terminated'];
            $('.span_monthly_subscription').removeClass('badge-success');
            $('.span_monthly_subscription').removeClass('badge-danger');
            if ($.inArray(client_status, restricted_status) !== -1) {
                $('.span_monthly_subscription').html('Inactive'); 
                $('.span_monthly_due').html(''); 
                $('.span_monthly_subscription').addClass('badge-danger');
            } else {
                $('.span_monthly_subscription').html('Active');
                $('.span_monthly_due').html('Payments occur on day '+billing_monthly_due+' of every month.');
                $('.span_monthly_subscription').addClass('badge-success');
            }

        });
        $('#profile_lead_status').on('change',function(){
            var select = $(this);
            var title = select.closest('.input-group').find('label').html();
            var field = select.attr('name');
            field = field.replace('profile_','');
            var value = select.val();
                updateFields('clients',client_id,field,value,title);

            $('#timeline_status').html(value);
 
            get_client_status_timelines();
        });
    }

    function functionsForBillingTab() {
        $('#sectionBillingInformation input[type=text]').on('change',function(){
            var input = $(this);
            var title = input.closest('.input-group').find('label').html();
            var field = input.attr('name');
            field = field.replace('billing_','');
            var value = input.val();
            if (!$(this).closest('section').hasClass('containerOtherCards')) {
                if (client_type == 'joint') {
                    updateFields('client_joint_billings',client_joint_id,field,value,title);
                } else {
                    updateFields('client_billings',client_id,field,value,title);
                }
            }
        });
        $('#sectionBillingInformation select').on('change',function(){
            var select = $(this);
            var title = select.closest('.input-group').find('label').html();
            var field = select.attr('name');
            field = field.replace('billing_','');
            var value = select.val();
            if (client_type == 'joint') {
                updateFields('client_joint_billings',client_joint_id,field,value,title);
            } else {
                updateFields('client_billings',client_id,field,value,title);
            }


        });

        $('#sectionBillingInformation textarea').on('change',function(){
            var textarea = $(this);
            var title = textarea.closest('.input-group').find('label').html();
            var field = textarea.attr('name');
            field = field.replace('billing_','');
            var value = textarea.val();
            if (client_type == 'joint') {
                updateFields('client_joint_billings',client_joint_id,field,value,title);
            } else {
                updateFields('client_billings',client_id,field,value,title);
            }
        });

    }

    function get_client_crc() {
        var data =  {
                        table: 'credit_report_types',
                        action: 'get',
                        select: {
                                    
                                },
                        where:  {
        
                                },
                        field: '',
                        order: '',
                        limit: 0,
                        offset: 0,
                        group_by: ''
        
                    };
        $.post('clients/modelTable', data, function(data, textStatus, xhr) {
            var cr_types_ = JSON.parse(data);
            var cr_types = '';
            $.each(cr_types_, function(index, val) {
                cr_types += '<option value="'+val.cr_type+'">'+val.cr_type+'</option>';
            });
            var data =  {
                            table: 'client_credit_report_credentials',
                            action: 'get',
                            select: {
                                        
                                    },
                            where:  {
                                        id: client_type == 'client' ? client_id : client_joint_id,
                                        type: client_type
                                    },
                            field: '',
                            order: '',
                            limit: 0,
                            offset: 0,
                            group_by: ''
            
                        };
            $.post('clients/modelTable', data, function(data, textStatus, xhr) {
                data = JSON.parse(data);
                $('.crc_container').empty();
                if (data.length > 0) {
                    $.each(data, function(index, val) {
                        
                        if (index == 0) {
                            var btn = '<a href="#" class="btn_add_crc input-group-text text-success"><i class="fas fa-plus-circle"></i></a>';
                        } else {
                            var btn = '<a href="#" class="btn_delete_crc input-group-text text-danger"><i class="fas fa-trash"></i></a>';
                        }
                        var newCrc = '<section tbl="client_credit_report_credentials" crc_id="'+val.crc_id+'">\
                                        <div class="input-group " >\
                                            <div class="input-group-prepend">\
                                                <label for="" class="input-group-text p-0 b-0">\
                                                    <select name="" class="form-control" field="cr_type">\
                                                        <option value="">Select Credentials</option>\
                                                        '+cr_types+'\
                                                        <option value="Add New">Add New</option>\
                                                        \
                                                    </select>\
                                                </label>\
                                            </div>\
                                            <input type="text" name="" class="form-control" placeholder="Username"  field="username" value="'+val.username+'">\
                                            <div class="input-group-append">\
                                                '+btn+'\
                                            </div>\
                                        </div>  \
                                        <div class="input-group" tbl="credit_report_credentials" crc_id="">\
                                            <div class="input-group-prepend" >\
                                                <label for="" class="input-group-text" style="background-color: white;border-color: white"></label>\
                                            </div>\
                                            <input type="text" name="" class="form-control" placeholder="Password" field="password" value="'+val.password+'">\
                                        </div>  \
                                    </section>';
                        $('.crc_container').append(newCrc);
                        $('.crc_container').find('[crc_id="'+val.crc_id+'"]').find('select').val(val.cr_type);
                    });
                } else {
                    var btn = '<a href="#" class="btn_add_crc input-group-text text-success"><i class="fas fa-plus-circle"></i></a>';
                        
                    var newCrc = '<section tbl="client_credit_report_credentials" crc_id="">\
                                    <div class="input-group " >\
                                        <div class="input-group-prepend">\
                                            <label for="" class="input-group-text p-0 b-0">\
                                                <select name="" class="form-control" field="cr_type">\
                                                    <option value="">Select Credentials</option>\
                                                    '+cr_types+'\
                                                    <option value="Add New">Add New</option>\
                                                    \
                                                </select>\
                                            </label>\
                                        </div>\
                                        <input type="text" name="" class="form-control" placeholder="Username"  field="username" >\
                                        <div class="input-group-append">\
                                            '+btn+'\
                                        </div>\
                                    </div>  \
                                    <div class="input-group" tbl="credit_report_credentials" crc_id="">\
                                        <div class="input-group-prepend" >\
                                            <label for="" class="input-group-text" style="background-color: white;border-color: white"></label>\
                                        </div>\
                                        <input type="text" name="" class="form-control" placeholder="Password" field="password" ">\
                                    </div>  \
                                </section>';
                    $('.crc_container').append(newCrc);
                
                }
                    
            });
        });
            
    }


    $('.crc_container').on('change', 'input', function(event) {
        event.preventDefault();

        var section = $(this).closest('section');
        var tbl = section.attr('tbl');
        var crc_id = section.attr('crc_id');
        var field = $(this).attr('field');
        var value = $(this).val();
        var data =  {
                        table: tbl,
                        pk: crc_id,
                        action: 'save',
                        fields: {
                                    id: client_type == 'client' ? client_id : client_joint_id,
                                    type: client_type,
                                    [field]: value
                                },
                        return: 'crc_id'
                    };
        
        $.post('clients/modelTable', data , function(data, textStatus, xhr) { 
            section.attr('crc_id', data);
        });
    });
    $('.crc_container').on('change', 'select', function(event) {
        event.preventDefault();

        var section = $(this).closest('section');
        var tbl = section.attr('tbl');
        var crc_id = section.attr('crc_id');
        var field = $(this).attr('field');
        var value = $(this).val();
        if (value != 'Add New') {
            var data =  {
                            table: tbl,
                            pk: crc_id,
                            action: 'save',
                            fields: {
                                        id: client_type == 'client' ? client_id : client_joint_id,
                                        type: client_type,
                                        [field]: value
                                    },
                            return: 'crc_id'
                        };
            
            $.post('clients/modelTable', data , function(data, textStatus, xhr) { 
                section.attr('crc_id', data);
            });
        } else {
            get_credit_report_types();
            $('#add-edit-credit-report-types-modal').modal('show');
        }
            
    });

    function get_credit_report_types() {
        var data =  {
                            table: 'credit_report_types',
                            action: 'get',
                            select: {
                                        
                                    },
                            where:  {
            
                                    },
                            field: '',
                            order: '',
                            limit: 0,
                            offset: 0,
                            group_by: ''
            
                        };
            $.post('clients/modelTable', data, function(data, textStatus, xhr) {
                data = JSON.parse(data);
                tableCreditReportTypes.clear().draw();
                $.each(data, function(index, val) {
                    var btn_delete_cr_type = '<button class="btn_delete_cr_type btn btn-danger"><i class="fas fa-trash"></i></button>';
                    tableCreditReportTypes.row.add([val.cr_type,btn_delete_cr_type]).node().id = val.cr_type_id;
                    tableCreditReportTypes.draw(false);
                });

                
            });
    }


    $('#btn_save_cr_type').on('click', function(event) {
        event.preventDefault();
        var new_cr_type = $('#new_cr_type').val();
        if (new_cr_type != '') {
            var data =  {
                            table: 'credit_report_types',
                            pk: '',
                            action: 'save',
                            fields: {
                                        cr_type: new_cr_type
                                    },
                            return: ''
                        };
            
            $.post('clients/modelTable', data , function(data, textStatus, xhr) { 
                $('#new_cr_type').val('');
                get_credit_report_types();;
                get_client_crc();
            });
        }
            
    });


    $('#tableCreditReportTypes').on('click', '.btn_delete_cr_type', function(event) {
        event.preventDefault();
        var tr = $(this).closest('tr');
        var cr_type_id = tr.attr('id');
        var data =  {
                        table: 'credit_report_types',
                        pk: cr_type_id,
                        action: 'delete'
                    }; 
        $.post('clients/modelTable', data, function(data, textStatus, xhr) { 
            tr.remove();
            get_client_crc();
        });
    });
    function functionsForEnrollmentTab() {
        $('.sectionEnrollmentInformation input[type=text],.sectionEnrollmentInformation select').on('change',function(){
            var input = $(this);
            var section = input.closest('section');
            var input_group = input.closest('.input-group');
            var tbl = section.attr('tbl');
            if (tbl != null) {
                // alert(tbl);
                // console.log(crc_id);
                // var crc_id = section.attr('crc_id');
                // var field = $(this).attr('field');
                // var value = $(this).val();
                // var data =  {
                //                 table: tbl,
                //                 pk: crc_id,
                //                 action: 'save',
                //                 fields: {
                //                             id: client_type == 'client' ? client_id : client_joint_id,
                //                             type: client_type,
                //                             [field]: value
                //                         },
                //                 return: 'crc_id'
                //             };
                
                // $.post('clients/modelTable', data , function(data, textStatus, xhr) { 
                //     section.attr('crc_id', data);
                // });
            } else {
                var title = input.closest('.input-group').find('label').html();
                var field = input.attr('name');
                field = field.replace('enrollment_','');
                var value = input.val();
                if (client_type == 'joint') {
                    updateFields('client_joint_enrollments',client_joint_id,field,value,title);
                } else {
                    updateFields('client_enrollments',client_id,field,value,title);
                }

                if (field == 'date_of_cancellation') {
                    if (client_type == 'joint') {
                        updateFields('client_joints',client_joint_id,'client_status','Cancelled','Client Status');
                    } else {
                        updateFields('clients',client_id,'client_status','Cancelled','Client Status');
                    }

                    $('#profile_client_status').val('Cancelled');

                }                
            }
                

        });
    }

    function getAlertTemplates() {
        $.post('<?php echo base_url('admin/clients/getAlertTemplates') ?>',
            function(data){
                data = JSON.parse(data);
                var all_alert = $('#alert_templates_container');
                all_alert.empty();
                tableAlertTemplates.clear().draw();
                $.each(data,function(key,value){
                    var alert_template_id = value.alert_template_id;
                    var template_shortname = value.template_shortname;
                    var template_subject = value.template_subject;
                    var template_notes = value.template_notes;
                    var body = '\
                    ';
                    var editButton = '<a href="#" class="btnEditAlertTemplate">'+value.template_shortname+'</a>';
                    var btnDelete = '<a href="#" class="btnDeleteAlertTemplate"><i class="fas fa-trash text-danger"></i></a>';
                    var newRow = tableAlertTemplates.row.add([editButton,value.template_subject,value.template_notes,btnDelete]).node().id = value.alert_template_id;
                    tableAlertTemplates.draw(false);

                    var label = '<label  class="col-md-6 alert_notes" style="color: #6ec4d7; cursor: pointer; padding: 0px;" template_subject="'+template_subject+'" alert_temp="'+template_notes+'">'+template_shortname+'</label>';
                    $('#alert_templates_container').append(label);
                });

                $('#alert_templates_container').on('click','.alert_notes',function(e){
                    e.preventDefault();
                    // var template_subject = $(this).html();
                    var template_notes = $(this).attr('alert_temp');
                    var template_subject = $(this).attr('template_subject');
                    // alert(template_subject);

                    $('.new_alert_subject').val(template_subject);
                    $('.new_alert_notes').val(template_notes);
                });



                $('#tableAlertTemplates').on('click','.btnEditAlertTemplate',function(e){
                    e.preventDefault();
                    var tr = $(this).closest('tr');
                    var alert_template_id = tr.attr('id');
                    var tdata = tr.find('td');

                    $('#new_alert_template_id').val(alert_template_id);

                    var template_shortname = $(tdata[0]).find('a').html();
                    var template_subject = $(tdata[1]).html();
                    var template_notes = $(tdata[2]).html();

                    $('#new_template_shortname').val(template_shortname);
                    $('#new_template_subject').val(template_subject);
                    $('#new_template_notes').val(template_notes);
                });

                $('#tableAlertTemplates').on('click','.btnDeleteAlertTemplate',function(e){
                    e.preventDefault();
                    var tr = $(this).closest('tr');
                    var alert_template_id = tr.attr('id');
                    swal({
                        title: "Are you sure?",
                        text: "Alert Template will be deleted!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false ,
                        preConfirm: function() {
                            deleteAlertTemplate(alert_template_id);
                            function deleteAlertTemplate(alert_template_id) {
                                $.post('<?php echo base_url('admin/clients/deleteAlertTemplate') ?>',
                                    {alert_template_id}, function(data){
                                        // data = JSON.parse(data);
                                        // console.log(data);

                                        getAlertTemplates();
                                    }).fail(function(xhr){
                                        console.log(xhr.responseText);
                                    });
                            }
                        }
                    }).then(function(){
                        // swal("Success!", 'Account successfully deleted!', "success");
                    });
                });

            });
    }
    get_alert_templates_init();
    function get_alert_templates_init() {
        $.post('<?php echo base_url('admin/clients/get_alert_templates_init') ?>', function(data, textStatus, xhr) {
            data = JSON.parse(data);
            getAlertTemplatesInit(data);
        });
    }
    function getAlertTemplatesInit(alert_templates_init) {
                var all_alert = $('#alert_templates_container');
                all_alert.empty();
                $.each(alert_templates_init,function(key,value){
                    var alert_template_id = value.alert_template_id;
                    var template_shortname = value.template_shortname;
                    var template_subject = value.template_subject;
                    var template_notes = value.template_notes;
                    var body = '\
                    ';
                    var editButton = '<a href="#" class="btnEditAlertTemplate">'+value.template_shortname+'</a>';
                    var btnDelete = '<a href="#" class="btnDeleteAlertTemplate"><i class="fas fa-trash text-danger"></i></a>';
                    var newRow = tableAlertTemplates.row.add([editButton,value.template_subject,value.template_notes,btnDelete]).node().id = value.alert_template_id;
                    tableAlertTemplates.draw(false);

                    var label = '<label  class="col-md-6 alert_notes" style="color: #6ec4d7; cursor: pointer; padding: 0px;" template_subject="'+template_subject+'" alert_temp="'+template_notes+'">'+template_shortname+'</label>';
                    $('#alert_templates_container').append(label);
                });

                $('#alert_templates_container').on('click','.alert_notes',function(e){
                    e.preventDefault();
                    // var template_subject = $(this).html();
                    var template_notes = $(this).attr('alert_temp');
                    var template_subject = $(this).attr('template_subject');
                    // alert(template_subject);

                    $('.new_alert_subject').val(template_subject);
                    $('.new_alert_notes').val(template_notes);
                });



                $('#tableAlertTemplates').on('click','.btnEditAlertTemplate',function(e){
                    e.preventDefault();
                    var tr = $(this).closest('tr');
                    var alert_template_id = tr.attr('id');
                    var tdata = tr.find('td');

                    $('#new_alert_template_id').val(alert_template_id);

                    var template_shortname = $(tdata[0]).find('a').html();
                    var template_subject = $(tdata[1]).html();
                    var template_notes = $(tdata[2]).html();

                    $('#new_template_shortname').val(template_shortname);
                    $('#new_template_subject').val(template_subject);
                    $('#new_template_notes').val(template_notes);
                });

                $('#tableAlertTemplates').on('click','.btnDeleteAlertTemplate',function(e){
                    e.preventDefault();
                    var tr = $(this).closest('tr');
                    var alert_template_id = tr.attr('id');
                    swal({
                        title: "Are you sure?",
                        text: "Alert Template will be deleted!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false ,
                        preConfirm: function() {
                            deleteAlertTemplate(alert_template_id);
                            function deleteAlertTemplate(alert_template_id) {
                                $.post('<?php echo base_url('admin/clients/deleteAlertTemplate') ?>',
                                    {alert_template_id}, function(data){
                                        // data = JSON.parse(data);
                                        // console.log(data);

                                        getAlertTemplates();
                                    }).fail(function(xhr){
                                        console.log(xhr.responseText);
                                    });
                            }
                        }
                    }).then(function(){
                        // swal("Success!", 'Account successfully deleted!', "success");
                    });
                });

    }

    function getAccountTypes() {
        $.post('<?php echo base_url('admin/clients/getAccountTypes') ?>',
            function(data){
                data = JSON.parse(data);
                tableAccountTypes.clear().draw();
                tbl_dva_at.clear().draw();

                var bank_account_type = $('#bank_account_type');
                bank_account_type.empty();
                // var bank_account_type_for_dispute = $('#bank_account_type_for_dispute');
                // bank_account_type_for_dispute.empty();
                var newOption = '<option value="">Select Account Type</option>';
                    bank_account_type.append(newOption);
                $.each(data,function(key,value){
                    var input = '<input readonly type="text" class="b-0" value="'+value.account_type+'" style="width: 100%"/>';
                    var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditAccountType"><i class="fas fa-edit"></i></button>';
                    var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteAccountType"><i class="fas fa-trash"></i></button>';
                    var newRow = tableAccountTypes.row.add([input,editButton+deleteButton]).node().id = value.at_id;
                    tableAccountTypes.draw(false);

                    var btnGetTableRoundsByAccountType = '<a href="#" account_type="'+value.account_type+'" class="btnGetTableRoundsByAccountType btn btn-success btn-rounded btn-sm">Rounds <i class="fa fa-angle-double-right"></i></a>';
                    var newRow = tbl_dva_at.row.add([value.account_type,btnGetTableRoundsByAccountType]).node().id = value.at_id;
                    tbl_dva_at.draw(false);
                    var newOption = '<option value="'+value.account_type+'">'+value.account_type+'</option>';
                    bank_account_type.append(newOption);
                });

            });
    }

    function getAccountStatuses() {
        $.post('<?php echo base_url('admin/clients/getAccountStatuses') ?>',
            function(data){
                data = JSON.parse(data);
                tableAccountStatuses.clear().draw();

                var bank_account_status = $('#bank_account_status');
                bank_account_status.empty();

                $.each(data,function(key,value){
                    var input = '<input readonly type="text" class="b-0" value="'+value.account_status+'" style="width: 100%;padding-left: 10px"/>';
                    var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditAccountStatus"><i class="fas fa-edit"></i></button>';
                    var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteAccountStatus"><i class="fas fa-trash"></i></button>';
                    var newRow = tableAccountStatuses.row.add([input,editButton+deleteButton]).node().id = value.as_id;
                    tableAccountStatuses.draw(false);

                    var newOption = '<option value="'+value.account_status+'">'+value.account_status+'</option>';
                    bank_account_status.append(newOption);
                });
                bank_account_status.val('In Process');

            });
    }

    function getAccountPaymentStatuses() {
        $.post('<?php echo base_url('admin/clients/getAccountPaymentStatuses') ?>',
            function(data){
                data = JSON.parse(data);
                tableAccountPaymentStatuses.clear().draw();

                var bank_payment_status = $('#bank_payment_status');
                bank_payment_status.empty();

                var newOption = '<option value="">Select Payment Status</option>';
                bank_payment_status.append(newOption);

                $.each(data,function(key,value){
                    var input = '<input readonly type="text" class="b-0" value="'+value.payment_status+'" style="width: 100%;padding-left: 10px"/>';
                    var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditAccountPaymentStatus"><i class="fas fa-edit"></i></button>';
                    var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteAccountPaymentStatus"><i class="fas fa-trash"></i></button>';
                    var newRow = tableAccountPaymentStatuses.row.add([input,editButton+deleteButton]).node().id = value.ps_id;
                    tableAccountPaymentStatuses.draw(false);

                    var newOption = '<option value="'+value.payment_status+'">'+value.payment_status+'</option>';
                    bank_payment_status.append(newOption);
                });

            });
    }

    function getAccountComments() {
        $.post('<?php echo base_url('admin/clients/getAccountComments') ?>',
            function(data){
                data = JSON.parse(data);
                tableAccountComments.clear().draw();

                var bank_comments = $('#bank_comments');
                bank_comments.empty();

                var newOption = '<option value="">Select Comment</option>';
                bank_comments.append(newOption);

                $.each(data,function(key,value){
                    var input = '<input readonly type="text" class="b-0" value="'+value.comment+'" style="width: 100%;padding-left: 10px"/>';
                    var editButton = '<button class="btn btn-sm btn-rounded btn-success btnEditAccountComment"><i class="fas fa-edit"></i></button>';
                    var deleteButton = '<button class="btn btn-sm btn-rounded btn-danger btnDeleteAccountComment"><i class="fas fa-trash"></i></button>';
                    var newRow = tableAccountComments.row.add([input,editButton+deleteButton]).node().id = value.ac_id;
                    tableAccountComments.draw(false);

                    var newOption = '<option value="'+value.comment+'">'+value.comment+'</option>';
                    bank_comments.append(newOption);
                });

            });
    }



    function getDisputeVerbiagesAccountTypesBy_account_type(account_type,tab,textarea,round) {
        $.post('<?php echo base_url('admin/clients/getDisputeVerbiagesAccountTypesBy_account_type') ?>',
            {account_type,round},function(data){
                data = JSON.parse(data);

                if (tab == 'Results Tracker') {
                    var selectDisputeVerbiage = $('#select_dv_for_results_tracker');
                    selectDisputeVerbiage.empty();
                    // console.log(data);
                    var newVerbiage = '<option value="">Select Dispute Verbiage</option>';
                        selectDisputeVerbiage.append(newVerbiage);
                    $.each(data,function(key,value){

                        var newVerbiage = '<option value="'+value.dispute_verbiage+'">'+value.dispute_verbiage+'</option>';
                        selectDisputeVerbiage.append(newVerbiage);
                    });

                    selectDisputeVerbiage.select2();


                    function matchStart (term, text) {
                      if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                        return true;
                      }

                      return false;
                    }

                    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
                      selectDisputeVerbiage.select2({
                        matcher: oldMatcher(matchStart)
                      })
                      // selectDisputeVerbiage.select2('focus');
                    });
                    selectDisputeVerbiage.on('change',function(){
                        textarea.val($(this).val());
                        $('#add-edit-dispute-verbiage-for-results-tracker').modal('toggle');
                        textarea.focus();
                        textarea.trigger('change');
                    });

                } else {
                    var selectDisputeVerbiage = $('#selectDisputeVerbiage');
                    selectDisputeVerbiage.empty();
                    $.each(data,function(key,value){
                        var newVerbiage = '<option value="'+value.dispute_verbiage+'">'+value.dispute_verbiage+'</option>';
                        selectDisputeVerbiage.append(newVerbiage);
                    });

                    $('#bank_dispute_verbiage').val(data[getRandomInt(0,3)]['dispute_verbiage']);
                    function getRandomInt(min, max) {
                      return Math.floor(Math.random() * (max - min + 1) + min);
                    }
                    selectDisputeVerbiage.select2();

                    function matchStart (term, text) {
                      if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                        return true;
                      }

                      return false;
                    }

                    $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
                      selectDisputeVerbiage.select2({
                        matcher: oldMatcher(matchStart)
                      })
                      // selectDisputeVerbiage.select2('focus');
                    });
                }




            });
    }

    function saveNewAccount(client_id,client_type,equifax,experian,transunion,account_status,account_type,account_name,account_number,amount,dispute_verbiage,past_due,credit_limit,file_date,chapter,correct_info,inquiry_date,bureau_date,payment_status,comments,date_opened,original_creditor, status_date) {
        $.post('<?php echo base_url('admin/clients/saveNewAccount') ?>',
            {client_id,client_type,equifax,experian,transunion,account_status,account_type,account_name,account_number,amount,dispute_verbiage,past_due,credit_limit,file_date,chapter,correct_info,inquiry_date,bureau_date,payment_status,comments,date_opened,original_creditor,status_date},function(data){
                // $('#checkbox_all').attr('checked',false);
                // $('#checkbox_equifax').attr('checked',false);
                // $('#checkbox_experian').attr('checked',false);
                // $('#checkbox_transunion').attr('checked',false);

                $('#bank_account_name').val('');
                $('#bank_account_number').val('');
                // $('#bank_account_type').val('');
                $('#bank_amount').val('');
                $('#bank_account_type').val('');
                $('#bank_dispute_verbiage').val('');
                $('#bank_past_due').val('');
                $('#bank_date_opened').val('');
                $('#bank_original_creditor').val('');
                $('#bank_status_date').val('');
                $('#bank_comments').val('');
                $('#bank_payment_status').val('');
                // $('#global_bureau_date').val('');
                // $('#bank_dispute_verbiage').val('');
                if (client_type == 'joint') {
                    getResultsTracker(client_joint_id,client_type);
                } else {
                    getResultsTracker(client_id,client_type);
                }

                getFixedDeletedNotReporting(client_id,client_joint_id,client_type);
                getTableByAccount(client_id,client_joint_id,client_type);
                getTableByBureaus(client_id,client_joint_id,client_type);

                $('#bank_account_type').val('');
                $('#bank_account_type').trigger('change');
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getResultsTracker(client_id,client_type) {
        $.post('<?php echo base_url('admin/clients/getResultsTracker') ?>',
            {client_id,client_type},function(data){

                data = JSON.parse(data);
                // console.log(data);
                var customvtabDisputes = $('.customvtabDisputes');
                var tabs_vertical = customvtabDisputes.find('.tabs-vertical-disputes');
                tabs_vertical.empty();

                var tableDisputeAccounts = $('#tableDisputeAccounts tbody');
                tableDisputeAccounts.empty();


                $.each(data,function(key,value){
                    var newLi = '<li  class="nav-item" style="line-height: 100%">\
                                    <a class="nav-link customNavLink" data-toggle="tab" credit_limit="'+value.credit_limit+'" date_added="'+value.date_added+'" href="#an_'+value.account_number+'" role="tab">\
                                        <span class="hidden-sm-up">\
                                            <i class="mdi mdi-account-card-details"></i>\
                                        </span> \
                                        <span class="hidden-xs-down" >\
                                            <span field="account_name" style="font-size: 60%">'+value.account_name+'</span>\
                                            <br>\
                                            <span field="account_number" style="font-size: 80%">'+value.account_number+'</span>\
                                        </span>\
                                    </a>\
                                </li>';
                    tabs_vertical.append(newLi);


                    var account_name = '<input readonly type="text" class="b-0" field="account_name" value="'+value.account_name+'" style="width: 100%"/>';
                    var account_number = '<input readonly type="text" class="b-0" field="account_number" value="'+value.account_number+'" style="width: 100%"/>';
                    var editButton = '<div class="btn-group"><button class="btn btn-sm btn-success btnEditDisputesAccount"><i class="fas fa-edit"></i></button>';
                    var deleteButton = '<button class="btn btn-sm btn-danger btnDeleteDisputesAccount"><i class="fas fa-trash"></i></button></div>';
                    var newTr = '\
                                <tr date_added="'+value.date_added+'">\
                                    <td>'+account_name+'</td>\
                                    <td>'+account_number+'</td>\
                                    <td>'+value.credit_limit+'</td>\
                                    <td>'+value.amount+'</td>\
                                    <td>'+value.past_due+'</td>\
                                    <td>'+value.date_opened+'</td>\
                                    <td>'+editButton+deleteButton+'</td>\
                                </tr>';
                    tableDisputeAccounts.append(newTr);
                });

            });
    }

    $('.customvtabDisputes').on('click','.customNavLink',function(){
        account_number = $(this).attr('href');
        account_number = account_number.replace('#an_','');
        credit_limit = $(this).attr('credit_limit');
        date_added = $(this).attr('date_added');
        // console.log(credit_limit);
        $('.right-sidebar-loader').fadeIn(50);
        getAccountDetailsByAccountNumber(client_type,account_number,date_added);
    });

    function getCreditorsAccount(id,client_type) {
        $.post('<?php echo base_url('admin/clients/getCreditorsAccount') ?>',
            {id,client_type},function(data){
                // console.log(data);
                data = JSON.parse(data);
                var customvtabCreditors = $('.customvtabCreditors');
                var tabs_vertical = customvtabCreditors.find('.tabs-vertical-creditors');
                tabs_vertical.empty();


                $.each(data,function(key,value){
                    var newLi = '<li  class="nav-item" style="line-height: 100%">\
                                    <a class="nav-link customNavLink" data-toggle="tab" date_added="'+value.date_added+'" href="#an_'+value.account_number+'" role="tab">\
                                        <span class="hidden-sm-up">\
                                            <i class="mdi mdi-account-card-details"></i>\
                                        </span> \
                                        <span class="hidden-xs-down" >\
                                            <span style="font-size: 60%">'+value.account_name+'</span>\
                                            <br>\
                                            <span style="font-size: 80%">'+value.account_number+'</span>\
                                        </span>\
                                    </a>\
                                </li>';
                    tabs_vertical.append(newLi);
                });
            });
    }

    $('.customvtabCreditors').on('click','.customNavLink',function(e){
        e.preventDefault();
        account_number = $(this).attr('href');
        account_number = account_number.replace('#an_','');
        date_added = $(this).attr('date_added');
        // console.log(credit_limit);
        getCreditorsAccountByAccount(account_number,date_added);
    });

    function getCreditorsAccountByAccount(account_number,date_added) {
        $.post('<?php echo base_url('admin/clients/getCreditorsAccountByAccount') ?>',
            {account_number,date_added}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            var customvtabCreditors = $('.customvtabCreditors');
            var tab_content = customvtabCreditors.find('#custom_tab_content_creditors');
            var section_content_equifax = tab_content.find('#section_content_equifax');
            var section_content_experian = tab_content.find('#section_content_experian');
            var section_content_transunion = tab_content.find('#section_content_transunion');
                section_content_equifax.empty();
                section_content_experian.empty();
                section_content_transunion.empty();

                var newAccount = '<div class="text-center"><small><a bureau="Equifax" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddCreditorsAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_equifax.append(newAccount);

                var newAccount = '<div class="text-center"><small><a bureau="Experian" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddCreditorsAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_experian.append(newAccount);

                var newAccount = '<div class="text-center"><small><a bureau="TransUnion" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddCreditorsAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_transunion.append(newAccount);

                $.each(data, function(index, value) {
                    // console.log(value);
                    var selectCreditorAccountTypes = $('#select_creditors_account_type').html();
                    selectCreditorAccountTypes = '<select label="Account Type" field="account_type" class="form-control selectCreditorAccountTypes" ca_id="'+value.ca_id+'" id="creditors_account_'+value.ca_id+'">'+selectCreditorAccountTypes+'</select>';


                    var recommendation = '<textarea label="Recommendation" field="recommendation" class="form-control inputCreditorsRecommendation" ca_id="'+value.ca_id+'">'+value.recommendation+'</textarea>';
                    var missing_balance = '<input label="Balance" field="missing_balance" class="form-control inputCreditorsMissingBalance" ca_id="'+value.ca_id+'" value="'+value.missing_balance+'">';
                    var credit_limit = '<input label="Credit Limit" field="credit_limit" class="form-control inputCreditorsCreditLimit" ca_id="'+value.ca_id+'" value="'+value.credit_limit+'">';
                    var past_due = '<input label="Past Due" field="past_due" class="form-control inputCreditorsPastDue" ca_id="'+value.ca_id+'" value="'+value.past_due+'">';
                    var newDiv =    '<section id="bureaus_container_'+value.ca_id+'" b-r">\
                                        <h5 class="text-center">'+value.account_name+' '+value.account_number+'</h5>\
                                        '+selectCreditorAccountTypes+'\
                                        <br>Recommendation: '+recommendation+'\
                                        <br>Missing Balance: '+missing_balance+'\
                                        <br>Credit Limit: '+credit_limit+'\
                                        <br>Past Due: '+past_due+'\
                                        <div class="text-center"><small><a value="'+value.ca_id+'" class="btnRemoveCreditorsAccount text-danger" href="#">(remove account)</a></small></div>\
                                    </section>';
                    if (value.bureau == 'Equifax') {
                        section_content_equifax.empty();
                        section_content_equifax.append(newDiv);
                    }
                    if (value.bureau == 'Experian') {
                        section_content_experian.empty();
                        section_content_experian.append(newDiv);
                    }
                    if (value.bureau == 'TransUnion') {
                        section_content_transunion.empty();
                        section_content_transunion.append(newDiv);
                    }

                    $('#creditors_account_'+value.ca_id).val(value.account_type);
                });
        });
    }

    $('.customvtabCreditors').on('change', '.selectCreditorAccountTypes,.inputCreditorsRecommendation,.inputCreditorsMissingBalance,.inputCreditorsCreditLimit,.inputCreditorsPastDue', function(event) {
        event.preventDefault();
        var value = $(this).val();
        var field = $(this).attr('field');
        var ca_id = $(this).attr('ca_id');
        var label = $(this).attr('label');

        updateFieldsCreditorsAccounts('creditors_account',ca_id,field,value,label);
    });
    $('.customvtabCreditors').on('click', '.btnRemoveCreditorsAccount', function(event) {
        event.preventDefault();
        var ca_id = $(this).attr('value');
        swal({
            title: "Are you sure?",
            text: "Account will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo base_url('admin/clients/deleteCreditorsAccount') ?>', {ca_id}, function(data, textStatus, xhr) {
                   var li_active = $('.customvtabCreditors').find('.tabs-vertical-creditors').find('.active');
                   li_active.trigger('click');
                });
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });

    });

    $('.customvtabCreditors').on('click','.btnAddCreditorsAccount',function(e){
        e.preventDefault();
        var account_number = $(this).attr('account_number');
        var date_added = $(this).attr('date_added');
        var bureau = $(this).attr('bureau');
        swal({
            title: "Are you sure?",
            text: "Account will be created for "+bureau,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false ,
            preConfirm: function() {
                addCreditorsAccount(account_number,date_added,bureau);
                // console.log(client_type);
                // console.log(account_id);
                // console.log(bureau);
            }
        }).then(function(){
            // swal("Success!", 'Account successfully deleted!', "success");
        });
    });
    // $('.customvtabCreditors').on('change', '.', function(event) {
    //     event.preventDefault();
    //     var value = $(this).val();
    //     var field = $(this).attr('field');
    //     var ca_id = $(this).attr('ca_id');
    //     var label = $(this).attr('label');

    //     updateFields('creditors_account',ca_id,field,value,label);
    // });

    function getAccountDetailsByAccountNumber(client_type,account_number,date_added) {
        $.post('<?php echo base_url('admin/clients/getAccountDetailsByAccountNumber') ?>',
            {client_type,account_number,date_added},function(data){
                data = JSON.parse(data);
                // console.log(data);
                var customvtabDisputes = $('.customvtabDisputes');
                var tab_content = customvtabDisputes.find('#custom_tab_content_disputes');
                var section_content_equifax = tab_content.find('#section_content_equifax');
                var section_content_experian = tab_content.find('#section_content_experian');
                var section_content_transunion = tab_content.find('#section_content_transunion');


                section_content_equifax.empty();
                section_content_experian.empty();
                section_content_transunion.empty();


                var newAccount = '<div class="text-center"><small><a bureau="Equifax" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_equifax.append(newAccount);

                var newAccount = '<div class="text-center"><small><a bureau="Experian" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_experian.append(newAccount);

                var newAccount = '<div class="text-center"><small><a bureau="TransUnion" account_number="'+account_number+'" date_added="'+date_added+'" class="btnAddAccount text-success" href="#">(add account)</a></small></div>';
                    section_content_transunion.append(newAccount);


                $.each(data['data'],function(key,value){
                    var selectClientsAccountStatuses = '<div class="rightLabelDiv"><small class="rightLabel">Account Status</small><select label="Account Status" field="account_status" account_id="'+value.account_id+'"  class="triggerUpdateAccountDetails form-control" id="select_as_'+value.account_id+'">';
                    $.each(data['clients_account_statuses'],function(key,value){
                         selectClientsAccountStatuses += '<option value="'+value.account_status+'">'+value.account_status+'</option>';
                    });
                    selectClientsAccountStatuses += '</select></div>';

                    var selectAccountTypes = '<div class="rightLabelDiv"><small class="rightLabel">Account Type</small><select label="Account Type" field="account_type" account_id="'+value.account_id+'"  class="triggerUpdateAccountDetails form-control" id="select_at_'+value.account_id+'">';
                    $.each(data['account_types'],function(key,value){
                         selectAccountTypes += '<option value="'+value.account_type+'">'+value.account_type+'</option>';
                    });
                    selectAccountTypes += '</select></div>';


                    // SECTION ROUND START
                        var hide_class = 'hide';

                        if (value.dispute_9) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            }
                        }
                        var section_round_9 = '<section class="'+(value.dispute_9 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R9 Disputes</a></small><br>\
                                                <span class="'+hide_class+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_9" id="dv9_'+value.account_id+'" rows="6" class="'+hide_class+' roundDisputeResults form-control">'+(value.dispute_9 ? value.dispute_9 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R9 Results</a></small>\
                                                    <input field="dispute_9_date" type="text" class="roundDates rounddate-inputmask" value="'+(value.dispute_9_date ? value.dispute_9_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_9" account_id="'+value.account_id+'" rows="6" class="'+hide_class+' roundDisputeResults form-control">'+(value.result_9 ? value.result_9 : '')+'</textarea>\
                                                </div></section>';
                        var hide_8 = '';
                        if (value.dispute_8) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_8 = 'hide';
                            }
                        } else {
                            hide_8 = 'hide';
                        }
                        var section_round_8 = '<section class="'+(value.dispute_8 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R8 Disputes</a></small><br>\
                                                <span class="'+hide_8+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_8" account_id="'+value.account_id+'" rows="6" class="'+hide_8+' roundDisputeResults form-control">'+(value.dispute_8 ? value.dispute_8 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R8 Results</a></small>\
                                                    <input field="dispute_8_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_8_date ? value.dispute_8_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_8" account_id="'+value.account_id+'" rows="6" class="'+hide_8+' roundDisputeResults form-control">'+(value.result_8 ? value.result_8 : '')+'</textarea>\
                                                </div>\
                                                </section>';
                        var hide_7 = '';
                        if (value.dispute_7) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_7 = 'hide';
                            }
                        } else {
                            hide_7 = 'hide';
                        }
                        var section_round_7 = '<section class="'+(value.dispute_7 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R7 Disputes</a></small><br>\
                                                <span class="'+hide_7+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_7" account_id="'+value.account_id+'" rows="6" class="'+hide_7+' roundDisputeResults form-control">'+(value.dispute_7 ? value.dispute_7 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R7 Results</a></small>\
                                                    <input field="dispute_7_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_7_date ? value.dispute_7_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_7" account_id="'+value.account_id+'" rows="6" class="'+hide_7+' roundDisputeResults form-control">'+(value.result_7 ? value.result_7 : '')+'</textarea>\
                                                </div></section>';
                        var hide_6 = '';
                        if (value.dispute_6) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_6 = 'hide';
                            }
                        } else {
                            hide_6 = 'hide';
                        }
                        var section_round_6 = '<section class="'+(value.dispute_6 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R6 Disputes</a></small><br>\
                                                <span class="'+hide_6+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_6" account_id="'+value.account_id+'" rows="6" class="'+hide_6+' roundDisputeResults form-control">'+(value.dispute_6 ? value.dispute_6 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R6 Results</a></small>\
                                                    <input field="dispute_6_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_6_date ? value.dispute_6_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_6" account_id="'+value.account_id+'" rows="6" class="'+hide_6+' roundDisputeResults form-control">'+(value.result_6 ? value.result_6 : '')+'</textarea>\
                                                </div></section>';
                        var hide_5 = '';
                        if (value.dispute_5) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_5 = 'hide';
                            }
                        } else {
                            hide_5 = 'hide';
                        }
                        var section_round_5 = '<section class="'+(value.dispute_5 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R5 Disputes</a></small><br>\
                                                <span class="'+hide_5+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_5" account_id="'+value.account_id+'" rows="6" class="'+hide_5+' roundDisputeResults form-control">'+(value.dispute_5 ? value.dispute_5 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R5 Results</a></small>\
                                                    <input field="dispute_5_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_5_date ? value.dispute_5_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_5" account_id="'+value.account_id+'" rows="6" class="'+hide_5+' roundDisputeResults form-control">'+(value.result_5 ? value.result_5 : '')+'</textarea>\
                                                </div></section>';
                        var hide_4 = '';
                        if (value.dispute_4) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_4 = 'hide';
                            }
                        } else {
                            hide_4 = 'hide';
                        }
                        var section_round_4 = '<section class="'+(value.dispute_4 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R4 Disputes</a></small><br>\
                                                <span class="'+hide_4+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_4" account_id="'+value.account_id+'" rows="6" class="'+hide_4+' roundDisputeResults form-control">'+(value.dispute_4 ? value.dispute_4 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R4 Results</a></small>\
                                                    <input field="dispute_4_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_4_date ? value.dispute_4_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_4" account_id="'+value.account_id+'" rows="6" class="'+hide_4+' roundDisputeResults form-control">'+(value.result_4 ? value.result_4 : '')+'</textarea>\
                                                </div></section>';
                        var hide_3 = '';
                        if (value.dispute_3) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_3 = 'hide';
                            }
                        } else {
                            hide_3 = 'hide';
                        }
                        var section_round_3 = '<section class="'+(value.dispute_3 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R3 Disputes</a></small><br>\
                                                <span class="'+hide_3+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_3" account_id="'+value.account_id+'" rows="6" class="'+hide_3+' roundDisputeResults form-control">'+(value.dispute_3 ? value.dispute_3 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R3 Results</a></small>\
                                                    <input field="dispute_3_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_3_date ? value.dispute_3_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_3" account_id="'+value.account_id+'" rows="6" class="'+hide_3+' roundDisputeResults form-control">'+(value.result_3 ? value.result_3 : '')+'</textarea>\
                                                </div></section>';
                        var hide_2 = '';
                        if (value.dispute_2) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_2 = 'hide';
                            }
                        } else {
                            hide_2 = 'hide';
                        }
                        var section_round_2 = '<section class="'+(value.dispute_2 ? '' : 'hide')+'"><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R2 Disputes</a></small><br>\
                                                <span class="'+hide_2+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_2" account_id="'+value.account_id+'" rows="6" class="'+hide_2+' roundDisputeResults form-control">'+(value.dispute_2 ? value.dispute_2 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R2 Results</a></small>\
                                                    <input field="dispute_2_date" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_2_date ? value.dispute_2_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_2" account_id="'+value.account_id+'" rows="6" class="'+hide_2+' roundDisputeResults form-control">'+(value.result_2 ? value.result_2 : '')+'</textarea>\
                                                </div></section>';
                        var hide_1 = '';
                        if (value.dispute_1) {
                            if (hide_class == 'hide') {
                                hide_class = '';
                            } else {
                                hide_1 = 'hide';
                            }
                        } else {
                            hide_1 = 'hide';
                        }
                        var section_round_1 = '<section><div class="rounds b-b" value="'+value.account_type+'">\
                                                <small><a class="rt_rounds" href="#">R1 Disputes</a></small><br>\
                                                <span class="'+hide_1+'"><a class="btnSelectDisputeForResultsTracker" href="#">select dispute verbiage</a></span>\
                                                <textarea field="dispute_1" account_id="'+value.account_id+'" rows="6" class="'+hide_1+' roundDisputeResults form-control">'+(value.dispute_1 ? value.dispute_1 : '')+'</textarea>\
                                                </div>\
                                                <div class="rounds b-b">\
                                                    <small><a class="rt_rounds  text-danger" href="#">R1 Results</a></small>\
                                                    <input field="dispute_1_date" style="min-height: 0px !important;font-size: 60%;" type="text" class="roundDates rounddate-inputmask" placeholder="mm/dd/yyyy" value="'+(value.dispute_1_date ? value.dispute_1_date : '')+'" account_id="'+value.account_id+'" >\
                                                    <textarea field="result_1" account_id="'+value.account_id+'" rows="6" class="'+hide_1+' roundDisputeResults form-control">'+(value.result_1 ? value.result_1 : '')+'</textarea>\
                                                </div></section>';
                    // SECTION ROUND END

                    // BOXES
                        var account_type = value.account_type;
                        var boxes = '';
                        if (account_type == '120 Day Late' ||
                            account_type == '180 Day Late' ||
                            account_type == '150 Day Late' ||
                            account_type == '30 Day Late' ||
                            account_type == '60 Day Late' ||
                            account_type == '90 Day Late' ||
                            account_type == 'Charge-Off' ||
                            account_type == 'Repossession' ||
                            account_type == 'New 120 Day Late' ||
                            account_type == 'New 180 Day Late' ||
                            account_type == 'New 150 Day Late' ||
                            account_type == 'New 30 Day Late' ||
                            account_type == 'New 60 Day Late' ||
                            account_type == 'New 90 Day Late' ||
                            account_type == 'New Charge-Off' ||
                            account_type == 'New Repossession' ||
                            account_type == 'Foreclosure' ||
                            account_type == 'Included In BK' ||
                            account_type == 'Multiple Lates') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Balance</small>\
                                        <input account_id="'+value.account_id+'" label="Balance" field="amount" id="input_amount_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.amount) ? '' : value.amount)+'"/></div>\
                                        <div class="rightLabelDiv"><small class="rightLabel">Credit Limit</small>\
                                        <input account_id="'+value.account_id+'" label="Credit Limit" field="credit_limit" id="input_credit_limit_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.credit_limit) ? '' : value.credit_limit)+'"/></div>';
                        }

                        if (account_type == 'New Collection' ||
                            account_type == 'Judgment' ||
                            account_type == 'Tax Lien' ||
                            account_type == 'Collection' ||
                            account_type == '120 Day Late' ||
                            account_type == '180 Day Late' ||
                            account_type == '150 Day Late' ||
                            account_type == '30 Day Late' ||
                            account_type == '60 Day Late' ||
                            account_type == '90 Day Late' ||
                            account_type == 'Charge-Off' ||
                            account_type == 'Repossession' ||
                            account_type == 'New Judgment' ||
                            account_type == 'New Tax Lien' ||
                            account_type == 'New Collection' ||
                            account_type == 'New 120 Day Late' ||
                            account_type == 'New 180 Day Late' ||
                            account_type == 'New 150 Day Late' ||
                            account_type == 'New 30 Day Late' ||
                            account_type == 'New 60 Day Late' ||
                            account_type == 'New 90 Day Late' ||
                            account_type == 'New Charge-Off' ||
                            account_type == 'New Repossession' ||
                            account_type == 'Foreclosure' ||
                            account_type == 'Included In BK' ||
                            account_type == 'Multiple Lates') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Past Due</small>\
                                        <input account_id="'+value.account_id+'" label="Past Due" field="past_due" id="input_past_due_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.past_due) ? '' : value.past_due)+'"/></div>';
                        }
                        if (account_type == 'Inquiry' ||
                            account_type == 'New Inquiry') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Inquiry Date</small>\
                                        <input account_id="'+value.account_id+'" label="Inquiry Date" field="inquiry_date" id="input_inquiry_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.inquiry_date) ? '' : value.inquiry_date)+'"/></div>';
                        }

                        if (account_type == 'Wrong Address' ||
                            account_type == 'Wrong Social'  ||
                            account_type == 'Wrong Name') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Correct Info</small>\
                                        <input account_id="'+value.account_id+'" label="Correct Info" field="correct_info" id="input_correct_info_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.correct_info) ? '' : value.correct_info)+'"/></div>';
                        }

                        if (account_type == 'Bankruptcy' ||
                            account_type == 'New Bankruptcy' ||
                            account_type == 'Included In BK') {
                            boxes += '<div class="rightLabelDiv"><small class="rightLabel">Chapter</small>\
                                    <input account_id="'+value.account_id+'" label="Chapter" field="chapter" id="input_chapter_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control" value="'+($.isEmptyObject(value.chapter) ? '' : value.chapter)+'"/></div>';
                            if (account_type != 'Included In BK') {
                                boxes += '<div class="rightLabelDiv"><small class="rightLabel">Status Date</small>\
                                    <input account_id="'+value.account_id+'" label="Status Date" field="status_date" id="input_status_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control monthyear-inputmask" value="'+($.isEmptyObject(value.status_date) ? '' : value.status_date)+'"/></div>';
                            }

                        }

                        if (account_type == 'Judgment' ||
                            account_type == 'Tax Lien' ||
                            account_type == 'Bankruptcy' ||
                            account_type == 'New Judgment' ||
                            account_type == 'New Tax Lien' ||
                            account_type == 'New Bankruptcy') {
                            boxes += ' <div class="rightLabelDiv"><small class="rightLabel">File Date</small>\
                                        <input account_id="'+value.account_id+'" label="File Date" field="file_date" id="input_file_date_'+value.account_id+'" type="text" class="triggerUpdateAccountDetails form-control monthyear-inputmask" value="'+($.isEmptyObject(value.file_date) ? '' : value.file_date)+'"/></div>';
                        }
                        if (account_type == '120 Day Late' ||
                            account_type == '180 Day Late' ||
                            account_type == '150 Day Late' ||
                            account_type == '30 Day Late' ||
                            account_type == '60 Day Late' ||
                            account_type == '90 Day Late' ||
                            account_type == 'Repossession' ||
                            account_type == 'New 120 Day Late' ||
                            account_type == 'New 180 Day Late' ||
                            account_type == 'New 150 Day Late' ||
                            account_type == 'New 30 Day Late' ||
                            account_type == 'New 60 Day Late' ||
                            account_type == 'New 90 Day Late' ||
                            account_type == 'New Repossession' ||
                            account_type == 'Foreclosure' ||
                            account_type == 'Included In BK' ||
                            account_type == 'Multiple Lates')  {

                            if (
                                account_type == 'Judgment' ||
                                account_type == 'New Judgment' ||
                                
                                account_type == 'Foreclosure' ||
                                account_type == 'Multiple Lates') {

                            } else {
                                boxes += '<div class="rightLabelDiv"><small class="rightLabel">Date Opened</small><input label="Date Opened" field="date_opened" account_id="'+value.account_id+'" id="input_date_opened_'+value.account_id+'" value="'+(value.date_opened)+'"  type="text" class="triggerUpdateAccountDetails monthyear-inputmask form-control"></div>';
                            }
                        } else {
                            if (account_type == 'Collection' || account_type == 'New Collection') {
                                boxes += '<div class="rightLabelDiv"><small class="rightLabel">Original Creditor</small><input  label="Original Creditor" field="original_creditor" account_id="'+value.account_id+'" id="input_original_creditor_'+value.account_id+'" value="'+(!$.isEmptyObject(value.original_creditor) ? value.original_creditor : '')+'"  type="text" class="form-control triggerUpdateAccountDetails"></div>';
                                boxes += '<div class="rightLabelDiv"><small class="rightLabel">Date Opened</small><input label="Date Opened" field="date_opened" account_id="'+value.account_id+'" id="input_date_opened_'+value.account_id+'" value="'+(value.date_opened)+'"  type="text" class="triggerUpdateAccountDetails monthyear-inputmask form-control"></div>';
                            } else if (account_type == 'Charge-Off' || account_type == 'New Charge-Off') {
                                boxes += '<div class="rightLabelDiv"><small class="rightLabel">Date Opened</small><input label="Date Opened" field="date_opened" account_id="'+value.account_id+'" id="input_date_opened_'+value.account_id+'" value="'+(value.date_opened)+'"  type="text" class="triggerUpdateAccountDetails monthyear-inputmask form-control"></div>';
                            }

                        }

                    // EXTRA BOXES
                        var bureau_date_ = '<div class="rightLabelDiv"><small class="rightLabel">Bureau Date</small><input label="Bureau Date" field="bureau_date" account_id="'+value.account_id+'" id="input_bureau_date_'+value.account_id+'" value="'+(!$.isEmptyObject(value.bureau_date) ? moment(value.bureau_date).format('MM/DD/YYYY') : '')+'"  type="text" class="triggerUpdateAccountDetails date-inputmask form-control"></div>';
                        var payment_status_ = '<div class="rightLabelDiv"><small class="rightLabel">Payment Status</small><input label="Payment Status" field="payment_status" account_id="'+value.account_id+'" id="input_payment_status_'+value.account_id+'" value="'+(!$.isEmptyObject(value.payment_status) ? value.payment_status : '')+'"  type="text" class="triggerUpdateAccountDetails form-control"></div>';
                        var comments_ = '<div class="rightLabelDiv"><small class="rightLabel">Comments</small><input label="Comments" field="comments" account_id="'+value.account_id+'" id="input_comments_'+value.account_id+'" value="'+(!$.isEmptyObject(value.comments) ? value.comments : '')+'"  type="text" class="triggerUpdateAccountDetails form-control"></div>';


                        // var note_ = '<small class="hide">Note</small><input id="input_note_'+value.account_id+'" type="text" class="form-control hide" value="'+(!$.isEmptyObject(value.note) ? value.note : '')+'"/>';
                        var newDiv = '<section id="bureaus_container_'+value.account_id+'" b-r">\
                                        <h5 class="text-center">'+value.account_name+' '+value.account_number+'</h5>\
                                        '+selectClientsAccountStatuses+'\
                                        '+selectAccountTypes+'\
                                        '+boxes+'\
                                        '+payment_status_+'\
                                        '+comments_+'\
                                        '+bureau_date_+'\
                                        '+section_round_1+'\
                                        '+section_round_2+'\
                                        '+section_round_3+'\
                                        '+section_round_4+'\
                                        '+section_round_5+'\
                                        '+section_round_6+'\
                                        '+section_round_7+'\
                                        '+section_round_8+'\
                                        '+section_round_9+'\
                                        <div class="text-center"><a class="btnShowHideSectionRound" href="#"><i class="fas fa-plus-circle text-success"></i></a></div><hr>\
                                        <div class="text-center"><small><a value="'+value.account_id+'" class="btnRemoveAccount text-danger" href="#">(remove account)</a></small></div>\
                                    </section>';



                    if (value.bureaus == 'Equifax') {
                        section_content_equifax.empty();
                        section_content_equifax.append(newDiv);
                    }

                    if (value.bureaus == 'Experian') {
                        section_content_experian.empty();
                        section_content_experian.append(newDiv);
                    }

                    if (value.bureaus == 'TransUnion') {
                        section_content_transunion.empty();
                        section_content_transunion.append(newDiv);
                    }

                    $('#select_as_'+value.account_id).val(value.account_status);
                    $('#select_at_'+value.account_id).val(value.account_type);

                    if (value.account_status == 'Deleted' || value.account_status == 'Fixed' || value.account_status == 'Not Reporting' || value.account_status == 'Hold') {
                        $('#bureaus_container_'+value.account_id+' input,#bureaus_container_'+value.account_id+' select,#bureaus_container_'+value.account_id+' textarea').prop('disabled',true);
                        $('#select_as_'+value.account_id).prop('disabled',false);
                        $('#bureaus_container_'+value.account_id).find('.roundDates').prop('disabled',false);
                    }



                });

                $(".date-inputmask").inputmask("mm/dd/yyyy");
                $(".rounddate-inputmask").inputmask("99/99/9999");
                $(".monthyear-inputmask").inputmask("mm/yyyy");
                $(".phone-inputmask").inputmask("(999) 999-9999");
                $(".ss-inputmask").inputmask("999-99-9999");


                $('.rt_rounds').on('click',function(e){
                    e.preventDefault();
                    if (!$(this).hasClass('text-danger')) {
                        var div = $(this).closest('div');
                        var textarea = div.find('textarea');
                        var span = div.find('span');
                        var date = div.find('.roundDates').closest('div');
                        if (textarea.hasClass('hide')) {
                            textarea.removeClass('hide');
                            span.removeClass('hide');
                            date.removeClass('hide');
                        } else {
                            textarea.addClass('hide');
                            span.addClass('hide');
                            date.addClass('hide');
                        }
                    } else {
                        var div = $(this).closest('div');
                        var textarea = div.find('textarea');
                        // var date = div.find('.roundDates');
                        if (textarea.hasClass('hide')) {
                            textarea.removeClass('hide');
                            // date.removeClass('hide');
                        } else {
                            textarea.addClass('hide');
                            // date.addClass('hide');
                        }
                    }

                });

                $('.btnSelectDisputeForResultsTracker').on('click',function(e){
                    e.preventDefault();
                    var div = $(this).closest('div');
                    var textarea = div.find('textarea');
                    var account_id = textarea.attr('account_id');
                    var account_type = div.attr('value');
                    var round = textarea.attr('field');
                    round = round.replace('dispute_','');

                    $('#add-edit-dispute-verbiage-for-results-tracker').modal('toggle');
                    getDisputeVerbiagesAccountTypesBy_account_type(account_type,'Results Tracker',textarea,round);

                });

                $('.btnShowHideSectionRound').on('click',function(e){
                    e.preventDefault();
                    var section = $(this).closest('section');
                    var rounds = section.find('section');
                    $.each(rounds,function(key,value){
                        if ($(value).hasClass('hide')) {
                            $(value).removeClass('hide');
                            return false;
                        }
                    });
                });


                $('.btnRemoveAccount').on('click',function(e){
                    e.preventDefault();
                    var account_id = $(this).attr('value');
                    swal({
                        title: "Are you sure?",
                        text: "Account will be deleted!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false ,
                        preConfirm: function() {
                            removeAccount(client_type,account_id);
                        }
                    }).then(function(){
                        // swal("Success!", 'Account successfully deleted!', "success");
                    });
                });

                $('.btnAddAccount').on('click',function(e){
                    e.preventDefault();
                    var account_number = $(this).attr('account_number');
                    var date_added = $(this).attr('date_added');
                    var bureau = $(this).attr('bureau');
                    swal({
                        title: "Are you sure?",
                        text: "Account will be created for "+bureau,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes!",
                        closeOnConfirm: false ,
                        preConfirm: function() {
                            addAccount(client_type,account_number,date_added,bureau);
                            // console.log(client_type);
                            // console.log(account_id);
                            // console.log(bureau);
                        }
                    }).then(function(){
                        // swal("Success!", 'Account successfully deleted!', "success");
                    });
                });

                $('.right-sidebar-loader').fadeOut(50);


            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function changeClientAccountStatus(client_type,account_id,account_status) {
        $.post('<?php echo base_url('admin/clients/changeClientAccountStatus') ?>',
            {client_type,account_id,account_status},function(data){
                $.toast({
                    heading: 'Update Success!',
                    text: 'Account Status successfully updated to '+account_status,
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
            });
    }

    function changeClientAccountType(client_type,account_id,account_type) {
        $.post('<?php echo base_url('admin/clients/changeClientAccountType') ?>',
            {client_type,account_id,account_type},function(data){
                $.toast({
                    heading: 'Update Success!',
                    text: 'Account Type successfully updated to '+account_type,
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
                getAccountDetailsByAccountNumber(client_type,account_number,date_added);
            });
    }




    function changeClientNote(client_type,account_id,note) {
        $.post('<?php echo base_url('admin/clients/changeClientNote') ?>',
            {client_type,account_id,note},function(data){
                $.toast({
                    heading: 'Update Success!',
                    text: 'Note successfully updated to '+note,
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
            });
    }

    function updateClientRounds(client_type,account_id,field,value,label) {
        $.post('<?php echo base_url('admin/clients/updateClientRounds') ?>',
            {client_type,account_id,field,value},function(data){
                $.toast({
                    heading: 'Update Success!',
                    text: label+' successfully updated to '+value,
                    position: 'top-right',
                    loaderBg:'#17a2b8',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });

            });
    }

    function removeAccount(client_type,account_id){
        $.post('<?php echo base_url('admin/clients/removeAccount') ?>',
            {client_type,account_id},function(data){
                swal("Success!", 'Account successfully deleted!', "success");
                getAccountDetailsByAccountNumber(client_type,account_number,date_added);
            });
    }

    function addAccount(client_type,account_number,date_added,bureau){
        $.post('<?php echo base_url('admin/clients/addAccount') ?>',
            {client_type,account_number,date_added,bureau},function(data){
                swal("Success!", 'Account successfully added!', "success");
                getAccountDetailsByAccountNumber(client_type,account_number,date_added);
            });
    }
    function addCreditorsAccount(account_number,date_added,bureau){
        $.post('<?php echo base_url('admin/clients/addCreditorsAccount') ?>',
            {account_number,date_added,bureau},function(data){
                swal("Success!", 'Account successfully added!', "success");
                getCreditorsAccountByAccount(account_number,date_added);
            });
    }

    function changeCardSent(btn,table,value,where,id){
        $.post('<?php echo base_url('admin/clients/changeCardSent') ?>',
            {table,value,where,id},function(data){
                swal("Success!", 'Card Sent!', "success");
                if (value == 'Yes') {
                    btn.html('Yes');
                } else {
                    btn.html('No');
                }
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }




// FUNCTIONS END






    // CHARTS START

        // var chartTotalFixed = echarts.init(document.getElementById('chartTotalFixed'));

        var chartEquifaxPercent = echarts.init(document.getElementById('chartEquifaxPercent'));
        var chartExperianPercent = echarts.init(document.getElementById('chartExperianPercent'));
        var chartTransUnionPercent = echarts.init(document.getElementById('chartTransUnionPercent'));


        function getFixedDeletedNotReporting(client_id,client_joint_id,client_type) {
            $.post('<?php echo base_url('admin/clients/getFixedDeletedNotReporting') ?>',
                {client_id,client_joint_id,client_type},function(data){
                    data = JSON.parse(data);
                    /*<!-- ============================================================== -->*/
                    /*<!-- Pie Chart -->*/
                    /*<!-- ============================================================== -->*/
                    // var chartLabelTotalFixed = $('.chartLabelTotalFixed');
                    var chartLabelEquifax = $('.chartLabelEquifax');
                    var chartLabelExperian = $('.chartLabelExperian');
                    var chartLabelTransUnion = $('.chartLabelTransUnion');

                    // chartLabelTotalFixed.empty();
                    chartLabelEquifax.empty();
                    chartLabelExperian.empty();
                    chartLabelTransUnion.empty();


                    var chartHeaderEquifax = $('.chartHeaderEquifax');
                    var chartHeaderExperian = $('.chartHeaderExperian');
                    var chartHeaderTransUnion = $('.chartHeaderTransUnion');

                    var all_account_label = $('.all-account-label');

                    var data_equifax = [];
                    var data_experian = [];
                    var data_transunion = [];
                    if (data.length != 0) {
                        all_account_label.empty();
                        var total_accounts = data['total_accounts'];
                        var fixed_accounts = data['fixed_accounts'];

                        var total_equifax = data['total_equifax'];
                        var fixed_equifax = data['fixed_equifax'];

                        var total_experian = data['total_experian'];
                        var fixed_experian = data['fixed_experian'];

                        var total_transunion = data['total_transunion'];
                        var fixed_transunion = data['fixed_transunion'];






                        // ["#fb3a3a","#3F51B5","#26c6da"]

                        if (total_accounts != 0) {
                            chartHeaderEquifax.removeClass('hide');
                            chartHeaderExperian.removeClass('hide');
                            chartHeaderTransUnion.removeClass('hide');
                            // // CHART FIXED

                            // CHART Equifax

                                if (total_equifax != 0) {
                                    var total_average_equifax = ((fixed_equifax/total_equifax) * 100).toFixed(2);
                                    chartLabelEquifax.html('Deleted/Fixed: '+fixed_equifax+' out of '+total_equifax+' ('+total_average_equifax+'%)');
                                    if (fixed_equifax == 0) {
                                        data_equifax = [
                                            { value: 0, name: 'Deleted/Fixed' },
                                            { value: 1, name: 'Not Deleted/Fixed' }
                                        ];
                                    } else {
                                        data_equifax = [
                                            { value: fixed_equifax, name: 'Deleted/Fixed' },
                                            { value: (total_equifax - fixed_equifax), name: 'Not Deleted/Fixed' }
                                        ];
                                    }

                                    if (total_average_equifax > 0) {
                                        $('.spanEquifaxPercent').html(total_average_equifax+'%');
                                    } else {
                                        $('.spanEquifaxPercent').html('-');
                                        $('.spanEquifaxPercent').css('left','46%');
                                    }

                                } else {
                                    chartLabelEquifax.html('No Equifax Account');
                                    data_equifax = [
                                        { value: 0, name: 'Deleted/Fixed' },
                                        { value: 1, name: 'Not Deleted/Fixed' }
                                    ];
                                }



                            // CHART Experian

                                if (total_experian != 0) {
                                    var total_average_experian = ((fixed_experian/total_experian) * 100).toFixed(2);
                                    chartLabelExperian.html('Deleted/Fixed: '+fixed_experian+' out of '+total_experian+' ('+total_average_experian+'%)');
                                    if (fixed_experian == 0) {
                                        data_experian = [
                                            { value: 0, name: 'Deleted/Fixed' },
                                            { value: 1, name: 'Not Deleted/Fixed' }
                                        ];
                                    } else {
                                        data_experian = [
                                            { value: fixed_experian, name: 'Deleted/Fixed' },
                                            { value: (total_experian - fixed_experian), name: 'Not Deleted/Fixed' }
                                        ];
                                    }

                                    if (total_average_experian > 0) {
                                        $('.spanExperianPercent').html(total_average_experian+'%');
                                    } else {
                                        $('.spanExperianPercent').html('-');
                                        $('.spanExperianPercent').css('left','46%');
                                    }
                                } else {
                                    chartLabelExperian.html('No Experian Account');
                                    data_experian = [
                                        { value: 0, name: 'Deleted/Fixed' },
                                        { value: 1, name: 'Not Deleted/Fixed' }
                                    ];
                                }



                            // CHART TransUnion

                                if (total_transunion != 0) {
                                    var total_average_transunion = ((fixed_transunion/total_transunion) * 100).toFixed(2);
                                    chartLabelTransUnion.html('Deleted/Fixed: '+fixed_transunion+' out of '+total_transunion+' ('+total_average_transunion+'%)');
                                    if (fixed_transunion == 0) {
                                        data_transunion = [
                                            { value: 0, name: 'Deleted/Fixed' },
                                            { value: 1, name: 'Not Deleted/Fixed' }
                                        ];
                                    } else {
                                        data_transunion = [
                                            { value: fixed_transunion, name: 'Deleted/Fixed' },
                                            { value: (total_transunion - fixed_transunion), name: 'Not Deleted/Fixed' }
                                        ];
                                    }

                                    if (total_average_transunion > 0) {
                                        $('.spanTransUnionPercent').html(total_average_transunion+'%');
                                    } else {
                                        $('.spanTransUnionPercent').html('-');
                                        $('.spanTransUnionPercent').css('left','46%');
                                    }
                                } else {
                                    chartLabelTransUnion.html('No TransUnion Account');
                                    data_transunion = [
                                        { value: 0, name: 'Deleted/Fixed' },
                                        { value: 1, name: 'Not Deleted/Fixed' }
                                    ];
                                }







                        } else {
                            // chartLabelTotalFixed.html('No Accounts found!');
                            all_account_label.html('No Accounts Found');
                            chartLabelEquifax.html('No Equifax Account');
                            data_equifax = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];

                            chartLabelExperian.html('No Experian Account');
                            data_experian = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];

                            chartLabelTransUnion.html('No TransUnion Account');
                            data_transunion = [
                                { value: 0, name: 'Deleted/Fixed' },
                                { value: 1, name: 'Not Deleted/Fixed' }
                            ];
                        }
                    } else {
                        all_account_label.html('No Accounts Found');
                        chartLabelEquifax.html('No Equifax Account');
                        data_equifax = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];

                        chartLabelExperian.html('No Experian Account');
                        data_experian = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];

                        chartLabelTransUnion.html('No TransUnion Account');
                        data_transunion = [
                            { value: 0, name: 'Deleted/Fixed' },
                            { value: 1, name: 'Not Deleted/Fixed' }
                        ];
                    }

                    callChartEquifaxPercent(data_equifax);
                    callChartExperianPercent(data_experian);
                    callChartTransUnionPercent(data_transunion);



                });
        }

    // CHARTS FUNCTIONS START
        function callChartTotalFixed(data) {
            // ==============================================================
            // doughnut chart option
            // ==============================================================


            // specify chart configuration item and data
            // formatter: "{a} <br/>{b} : {c} ({d}%)"
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c}"
                },
                legend: {
                    orient: 'vertical',
                    x: 'bottom',
                    data: ['Equifax', 'Experian', 'TransUnion']
                },
                toolbox: {
                    show: false,
                    feature: {
                        dataView: { show: false, readOnly: false },
                        magicType: {
                            show: false,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'center',
                                    max: 100
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                color: ["#fb3a3a","#3F51B5","#26c6da"],
                calculable: true,
                series: [{
                    name: 'Bureau',
                    type: 'pie',
                    radius: ['80%', '90%'],
                    itemStyle: {
                        normal: {
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                position: 'center',
                                textStyle: {
                                    fontSize: '14',
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    data: data
                }]
            };



            // use configuration item and data specified to show chart
            chartTotalFixed.setOption(option, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        chartTotalFixed.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize) , $("a[href='#tab_results']").on('shown.bs.tab',resize)
                // , $("a[href='#tab_results']").on('shown.bs.tab',resize)
            });
        }

        // ["#fb3a3a","#3F51B5","#26c6da"]
        function callChartEquifaxPercent(data) {
            // ==============================================================
            // doughnut chart option
            // ==============================================================


            // specify chart configuration item and data
            // formatter: "{a} <br/>{b} : {c} ({d}%)"
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{c} ({d}%)"
                },
                legend: {
                    orient: 'horizontal',
                    x: 'bottom',
                    data: ['Deleted/Fixed', 'Not Deleted/Fixed']
                },
                toolbox: {
                    show: false,
                    feature: {
                        dataView: { show: false, readOnly: false },
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'center',
                                    max: 50
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                color: ["#fc4b6c","grey"],
                calculable: true,
                series: [{
                    name: 'Bureau',
                    type: 'pie',
                    radius: ['80%', '90%'],
                    itemStyle: {
                        normal: {
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                position: 'center',
                                textStyle: {
                                    fontSize: '14',
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    data: data
                }]
            };



            // use configuration item and data specified to show chart
            chartEquifaxPercent.setOption(option, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        chartEquifaxPercent.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize) , $("a[href='#tab_results']").on('shown.bs.tab',resize)
                // , $("a[href='#tab_results']").on('shown.bs.tab',resize)
            });
        }

        function callChartExperianPercent(data) {
            // ==============================================================
            // doughnut chart option
            // ==============================================================


            // specify chart configuration item and data
            // formatter: "{a} <br/>{b} : {c} ({d}%)"
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{c} ({d}%)"
                },
                legend: {
                    orient: 'horizontal',
                    x: 'bottom',
                    data: ['Deleted/Fixed', 'Not Deleted/Fixed']
                },
                toolbox: {
                    show: false,
                    feature: {
                        dataView: { show: false, readOnly: false },
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'center',
                                    max: 50
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                color: ["#3F51B5","grey"],
                calculable: true,
                series: [{
                    name: 'Bureau',
                    type: 'pie',
                    radius: ['80%', '90%'],
                    itemStyle: {
                        normal: {
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                position: 'center',
                                textStyle: {
                                    fontSize: '14',
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    data: data
                }]
            };



            // use configuration item and data specified to show chart
            chartExperianPercent.setOption(option, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        chartExperianPercent.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize) , $("a[href='#tab_results']").on('shown.bs.tab',resize)
                // , $("a[href='#tab_results']").on('shown.bs.tab',resize)
            });


        }
        function callChartTransUnionPercent(data) {
            // ==============================================================
            // doughnut chart option
            // ==============================================================


            // specify chart configuration item and data
            // formatter: "{a} <br/>{b} : {c} ({d}%)"
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{c} ({d}%)"
                },
                legend: {
                    orient: 'horizontal',
                    x: 'bottom',
                    data: ['Deleted/Fixed', 'Not Deleted/Fixed']
                },
                toolbox: {
                    show: false,
                    feature: {
                        dataView: { show: false, readOnly: false },
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'center',
                                    max: 50
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                color: ["#26c6da","grey"],
                calculable: true,
                series: [{
                    name: 'Bureau',
                    type: 'pie',
                    radius: ['80%', '90%'],
                    itemStyle: {
                        normal: {
                            label: {
                                show: false
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                position: 'center',
                                textStyle: {
                                    fontSize: '14',
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    data: data
                }]
            };



            // use configuration item and data specified to show chart
            chartTransUnionPercent.setOption(option, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        chartTransUnionPercent.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize) , $("a[href='#tab_results']").on('shown.bs.tab',resize)
                // , $("a[href='#tab_results']").on('shown.bs.tab',resize)
            });


        }

// FROM EMAIL

    if (localStorage.stay == 'true' && localStorage.stay) {
        getProfileDetails(localStorage.id,localStorage.joint_id,localStorage.type);
        client_id = localStorage.id;
        client_joint_id = localStorage.joint_id;
        client_type = localStorage.type;

    }

    $('a[href="clients"],#sidebarnav li a,.right-side-toggle').on('click', function(event) {
        localStorage.stay = 'false';
        localStorage.id = '';
        localStorage.type = '';
        localStorage.joint_id = '';
    });





</script>