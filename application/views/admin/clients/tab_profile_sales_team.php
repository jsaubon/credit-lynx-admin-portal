<style>
    .div_automations .chosen-container { 
        flex: 1 1 auto;
        width: 1% !important;
        line-height: 2.4; 
    }

    .div_automations .chosen-container-multi .chosen-choices li.search-choice {
        line-height: 17.5px; 
    }

    .div_automations .chosen-container-multi .chosen-choices li.search-field {
        padding-left: 5px;
    }

    .div_automations .chosen-container-multi .chosen-results { 
        margin: 2px;
        padding: 5px;
    }

    .div_automations .chosen-container-multi .chosen-choices { 
        padding-left: 5px;
    }

    @media (max-width: 700px) {
        .w-100 {
            width: 100%;
        }
    }

    #profile_lead_status {
        height: calc(2.25rem + 2px) !important;
    }

    .select2-container--default .select2-selection--single {
        border-radius: 0px !important;
        border: 1px solid #ced4da !important;
        height: 38px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
         line-height: 35px !important; 
    }


    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 5px !important;
    }

    #sectionBillingInformation .form-control {
        height: 30px !important;
    }

    .input-group-text {
        line-height: 1 !important;
    }
</style>
<div style="padding-top: 0;" class="tab-pane b-t active show" id="tab_profile" role="tabpanel">
    <div class="card-body">
        <div class="row">
            <div class="col-md-4 col-xs-12 text-center b-r" >
                <!-- <form id="uploadProfilePictureForm" enctype="multipart/form-data"> -->
                    <!-- <img src="../assets/images/LynxLogo.png" width="120"> -->
                    <!-- <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="" data-show-remove="false" data-height="150" /> -->
                <!-- </form>    -->
                <br>
                <h3 class=" m-b-0"><u class="name"></u> </h3> 
                <button id="btnConvertToJoint" class="hide btn btn-sm btn-success waves-effect waves-light" type="button"  data-toggle="modal" data-target="#add-edit-client-joint-account">Convert to Joint</button>
                <br>
                <br>
                <div class="text-left"> 
                    <div id="sectionClientAssignments"> 
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Lead Status</label>
                            </div>

                            <select class="form-control" id="profile_lead_status" name="profile_lead_status" >
                                <option value="0">Select Status</option>
                                <?php foreach ($lead_statuses as $key => $value): ?>
                                    <option value="<?php echo $value['lead_status'] ?>"><?php echo $value['lead_status'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div> 
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Agent</label>
                            </div>
                            <select class="select2" name="profile_agent_id"  style="width: 100%">
                                <option value="0">Select Agent</option>
                                <?php foreach ($agents as $key => $value): ?>
                                    <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Broker</label>
                            </div>
                            <select class="select2" name="profile_broker_id"  style="width: 100%">
                                <option value="0">Select Broker</option>
                                <?php foreach ($brokers as $key => $value): ?>
                                    <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Sales</label>
                            </div>
                            <select class="select2" name="profile_sale_id" id="sales"   style="width: 100%">
                                <option value="0">Select Sales</option>
                                <?php foreach ($sales as $key => $value): ?>
                                    <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        
                            <div class="input-group show_client_content">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Processor</label>
                                </div>
                                <select class="select2" name="profile_processor_id"  style="width: 100%">
                                    <option value="0">Select Processor</option>
                                    <?php foreach ($processors as $key => $value): ?>
                                        <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                        


                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row" id="sectionBillingInformation">
                    <div class="col-12 col-md-6 p-r-0">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Plan</label>
                            </div> 
                            <select class="form-control" name="billing_plan" >
                            </select>
                        </div> 
                    </div> 
                    <div class="col-12 col-md-6 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="paid" class="rightLabel">Setup Date</label>
                            <input type="text" name="billing_paid" class="form-control date-inputmask">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="setup_fee" class="rightLabel">Setup Fee</label>
                            <input type="text" name="billing_setup_fee" class="form-control" style="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-l-0">
                        <div class="rightLabelDiv">
                            <label for="monthly_fee" class="rightLabel">Monthly Fee</label>
                            <input type="text" name="billing_monthly_fee" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="rightLabelDiv">
                            <label for="card_number" class="rightLabel">Card Number</label>
                            <input type="text" name="billing_card_number" class="form-control">
                        </div>
                    </div>

                    <div class="col-12 col-md-4 p-r-0">
                        <div class="rightLabelDiv">
                            <label for="card_expiration" class="rightLabel">Expiration</label>
                            <input type="text" name="billing_card_expiration" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-4 p-r-0 p-l-0" >
                        <div class="rightLabelDiv">
                            <label for="cvv_code" class="rightLabel">CVV Code</label>
                            <input type="text" name="billing_cvv_code" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-4 p-l-0 ">
                        <div class="rightLabelDiv">
                            <label for="billing_payment_method" class="rightLabel">Payment Method</label>
                            <select class="form-control" name="billing_payment_method">
                                <option value="American Express">American Express</option>
                                <option value="MasterCard">MasterCard</option>
                                <option value="Visa">Visa</option>
                                <option value="Cash">Cash</option>
                                <option value="Prepaid">Prepaid</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-12 text-center m-t-20 m-b-20">
                        <a href="#" class="btn_show_mailing btn btn-success btn-sm" style="font-size: small"><i class="fas fa-plus"></i> if billing is different than mailing</a>
                        <script>
                            $('.btn_show_mailing').on('click', function(event) {
                                event.preventDefault();
                                if ($('.container_mailing').hasClass('hide')) {
                                    $('.container_mailing').removeClass('hide');
                                } else {
                                    $('.container_mailing').addClass('hide');
                                }
                            });
                        </script>
                    </div>
                    <div class="col-md-12 container_mailing hide animated fadeInUp">
                        <div class="row"> 
                             
                            <div class="col-md-4 p-r-0">
                                <div class="rightLabelDiv">
                                    <label for="account_holder" class="rightLabel">Account Holder</label>
                                    <input type="text" name="billing_card_holder" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8 p-l-0">
                                <div class="rightLabelDiv">
                                    <label for="address" class="rightLabel">Address</label>
                                    <input type="text" name="billing_address" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 p-r-0">
                                <div class="rightLabelDiv">
                                    <label for="city" class="rightLabel">City</label>
                                    <input type="text" name="billing_city" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 p-r-0 p-l-0">
                                <div class="rightLabelDiv">
                                    <label for="state_province" class="rightLabel">State</label>
                                    <select class="form-control" name="billing_state" >
                                        <?php foreach ($userdata['state_provinces'] as $key => $value): ?>
                                            <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                        <?php endforeach?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 p-l-0">
                                <div class="rightLabelDiv">
                                    <label for="zip" class="rightLabel">Zip</label>
                                    <input type="text" name="billing_zip" class="form-control zip_postal_code">
                                </div>
                            </div>
                            <div class="col-md-12 m-t-20 hide">
                                <label style="margin-bottom: 0px;" for="">Other Cards <a href="#" class="btnAddOtherCard"><i class="fas fa-plus"></i></a></label>
                                <section class="containerOtherCards">

                                </section>
                            </div>
                            <div class="col-md-12 m-t-20 hide">
                                <div class="rightLabelDiv">
                                    <label for="note" class="rightLabel">Billing Notes</label>
                                    <textarea name="billing_note" class="form-control" rows="2" style="height: 100px;"></textarea>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>

        </div>
        <hr>
        <h3>Personal Information </h3> 
        <div class="row" id="sectionProfileInformation">
            <div class="col-md-6 col-xs-6 b-r">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="name" class="input-group-text">Name</label>
                    </div>
                    <input type="text" name="profile_name" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="ss" class="input-group-text">Social</label>
                    </div>
                    <?php if ($userdata['login_folder'] == 'employees'): ?>
                        <input type="text" name="profile_ss" class="form-control " >
                    <?php endif?>
                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                        <input type="text" name="profile_ss" class="form-control ss-inputmask" >
                    <?php endif?>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="date_of_birth" class="input-group-text">Date of Birth</label>
                    </div>
                    <input type="text" name="profile_date_of_birth" class="form-control date-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="email_address" class="input-group-text">Email
                             <a href="#" class="btnAddNewOtherEmail" style="margin-left: 5px;"><i class="fas fa-plus-circle"></i></a>
                        </label>
                    </div>
                    <input type="email" name="profile_email_address" class="form-control" >
                </div>
                <section class="containerClientOtherEmails">
                </section>

                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="alt_phone" class="input-group-text">Phone</label>
                    </div>
                    <input type="text" name="profile_alt_phone" class="form-control phone-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="cell_phone" class="input-group-text">Cell Phone</label>
                    </div>
                    <input type="text" name="profile_cell_phone" class="form-control phone-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="profile_carrier" class="input-group-text">Carrier</label>
                    </div>
                    <select class="form-control" name="profile_carrier" >
                        <option value="0">Select Carrier</option>
                        <option value="ATT">ATT</option>
                        <option value="Boost">Boost</option>
                        <option value="Cricket">Cricket</option>
                        <option value="Metro PCS">Metro PCS</option>
                        <option value="Simple">Simple</option>
                        <option value="Sprint">Sprint</option>
                        <option value="TMobile">TMobile</option>
                        <option value="Verizon">Verizon</option>
                        <option value="Virgin">Virgin</option>
                        <option value="Tracfone">Tracfone</option>
                        <option value="Do Not SMS">Do Not SMS</option>
                        <option value="Landline">Landline</option>
                        <option value="Bluegrass Cellular">Bluegrass Cellular</option>
                    </select>
                </div>



            </div>
            <div class="col-md-6 col-xs-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="address" class="input-group-text">Address</label>
                    </div>
                    <input type="text" name="profile_address" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="city" class="input-group-text">City</label>
                    </div>
                    <input type="text" name="profile_city" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="state" class="input-group-text">State</label>
                    </div>
                    <select class=" form-control" name="profile_state_province" style="height: calc(2.25rem + 2px) !important">
                        <?php foreach ($state_provinces as $key => $value): ?>
                            <option value="<?php echo $value ?>"><?php echo $value ?></option>
                        <?php endforeach?>
                    </select>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="zip_postal_code" class="input-group-text">Zip</label>
                    </div>
                    <input type="text" name="profile_zip_postal_code" class="form-control" >
                </div>
                <br>
                <br>
                <div class="input-group hide">
                    <div class="input-group-prepend">
                        <label for="font" class="input-group-text">Font</label>
                    </div>
                    <select class="form-control" name="profile_font">
                        <option value='Georgia, serif'>Georgia</option>
                        <option value='"Palatino Linotype", "Book Antiqua", Palatino, serif'>Palatino Linotype</option>
                        <option value='"Times New Roman", Times, serif'>Times New Roman</option>
                        <option value='Arial, Helvetica, sans-serif'>Arial</option>
                        <option value='"Arial Black", Gadget, sans-serif'>Arial Black</option>
                        <option value='"Comic Sans MS", cursive, sans-serif'>Comic Sans MS</option>
                        <option value='Impact, Charcoal, sans-serif'>Impact</option>
                        <option value='"Lucida Sans Unicode", "Lucida Grande", sans-serif'>Lucida Sans Unicode</option>
                        <option value='Tahoma, Geneva, sans-serif'>Tahoma</option>
                        <option value='"Trebuchet MS", Helvetica, sans-serif'>Trebuchet MS</option>
                        <option value='Verdana, Geneva, sans-serif'>Verdana</option>
                        <option value='"Courier New", Courier, monospace'>Courier New</option>
                        <option value='"Lucida Console", Monaco, monospace'>Lucida Console</option>
                    </select>
                </div>

                <div class="row m-t-10">
                    <div class="col-md-6 col-12">
                        <button id="SendSignUpLink" class="btn btn-success btn-block waves-effect waves-light" type="button">Send Short Signup</button>
                    </div>
                    <div class="col-md-6 col-12">
                        <button id="SendSignUpLinkRenew" style="background: #008080;color: white;box-shadow: 0 2px 2px 0 rgba(118, 232, 32, 0.14), 0 3px 1px -2px rgba(78, 156, 38, 0.2), 0 1px 5px 0 rgba(63, 255, 51, 0.12);border: 1px solid #008080;transition: 0.2s ease-in; " class="btn btn-block waves-effect waves-light" type="button">Send Long Signup</button>
                    </div>
                </div>   



            </div>
        </div>
    </div>


</div>

