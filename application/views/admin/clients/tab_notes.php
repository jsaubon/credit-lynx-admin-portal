<div class="tab-pane" id="notes-tab" role="tabpanel">
    <div class="card-body">
        <!-- <h5><a href="#" id="btnShowHideMakeNote">Make a note <i class="fas fa-angle-right"></i></a></h5>
        <script>
            $('#btnShowHideMakeNote').on('click',function(e){
                e.preventDefault();
                if ($('#containerMakeNote').hasClass('hide')) {
                    $('#containerMakeNote').removeClass('hide');
                } else {
                    $('#containerMakeNote').addClass('hide');
                }
            });
        </script>
        <div id="containerMakeNote">
            <label class="m-b-0">Templates <small>(<a href="#" id="btnOpenNoteTemplatesModal" data-toggle="modal" data-target="#add-edit-note-templates-modal">manage</a>)(<a id="btnOpenNoteTemplates" href="#">hide</a>)</small>
            </label>
            <script>
                $('#btnOpenNoteTemplates').on('click',function(e){
                    e.preventDefault();
                    if ($('#note_templates_container').hasClass('hide')) {
                        $('#note_templates_container').removeClass('hide');
                    } else {
                        $('#note_templates_container').addClass('hide');
                    }
                });
            </script>
            <div class="row " id="note_templates_container" style="border: 1px solid #6ec4d7!important; padding-top: 9px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 13px; margin-bottom: 7px!important;line-height: 70%;">
                <?php foreach ($note_templates as $key => $value): ?>
                    <label class="col-md-2 note_template" style="color: #6ec4d7; cursor: pointer;" note_template="<?php echo $value['note_template'] ?>"><?php echo $value['note_shortname'] ?></label>
                <?php endforeach?>
            </div>

            <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>
            <input type="text" name="" class="form-control material_date_time text-center" id="new_note_date" value="<?php echo date('Y-m-d H:i:s') ?>">
            <div class="input-group">
                <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"></i>
                <button style="width: 1%;flex: 1 1 auto;border-radius: 0" id="btnSaveNewNote" class="btn btn-block btn-success waves-effect waves-light" type="button">Save Note</button>

            </div>
        </div>
 -->
        <div class="message-scroll ">
            <div id="note_list_sticky" class="profiletimeline m-t-40 ">
            </div>
            <div id="note_list" class="profiletimeline">
            </div>
        </div>
    </div>
</div>