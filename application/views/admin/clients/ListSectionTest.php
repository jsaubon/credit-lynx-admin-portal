<style>
    /*#pageTable .b-r {
        border-right: 1px solid #dee2e6 !important;
    }*/

    #pageTable_length {
        display: none;;
    }

    #pageTable tbody tr td {
        line-height: 18px !important;
    }
</style>
<div id="listSection" class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <!-- <h5 class="card-title"><?php echo $page_title ?> (<?php echo $user_count ?>)
                         
                    </h5>  -->

                    <div class="table-responsive" >
                        <?php if ($page_title == 'Clients'): ?>
                            <a style="margin-top: 14px" href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#add-edit-client-modal" >New <?php echo $page_title_s ?> </a>
                        <?php endif ?>
                        <?php if ($page_title == 'Leads'): ?>
                            <a style="margin-top: 14px" href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#add-edit-lead-modal" >New <?php echo $page_title_s ?> </a>
                        <?php endif ?>
                        
                        <table id="pageTable" class="table stylish-table">
                            <thead>
                                <?php if ($page_title == 'Leads'): ?>
                                    <tr>   
                                        <th order_by="name">Name</th> 
                                        <th order_by="cell_phone">Phone</th> 
                                        <th order_by="email_address">Email</th>
                                        <th order_by="last_contact">Last Contact</th>
                                        <th order_by="date_of_birth">Date of Birth</th>
                                        <th order_by="client_card_sent">Card Sent</th>
                                        <th order_by="broker">Broker</th>
                                        <th order_by="agent">Agent</th>
                                        <th order_by="lead_status">Status</th> 
                                        <th class="text-center">Tools</th>
                                    </tr>
                                <?php endif ?>
                                <?php if ($page_title == 'Clients'): ?>
                                    <tr>  
                                        <th order_by="name">Name</th> 
                                        <th order_by="cell_phone">Phone</th> 
                                        <th order_by="email_address">Email</th>
                                        <th order_by="date_of_birth">Date of Birth</th>
                                        <th order_by="client_card_sent">Card Sent</th>
                                        <th order_by="paid">Date Paid</th>
                                        <th order_by="day_due">Cycle</th>
                                        <th order_by="client_status">Status</th>
                                        <th order_by="dfr">DFR</th>
                                        <th order_by="last_alert">Alert Sent</th>
                                        <th class="text-center">Tools</th>
                                    </tr>
                                <?php endif ?>
                                    
                            </thead>
                            <tbody>  
                                    
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div> 

 
<script>
    var pageTable;
    var pageTable_data = [{ name: 'page_title', value: '<?php echo $page_title ?>' }];
    // $(function() {

    pageTable = $('#pageTable').DataTable({
        language: { search: "<span style='font-size: 14px'><?php echo $page_title ?> (<?php echo $user_count ?>)</span>",
        searchPlaceholder: 'Search' },
        'aaSorting': [],  
        dom: 'Bfrtip',
        "pageLength": 20,
        dom: 'Rlfrtip',
        "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/clients/getTableClientsLeadsTest") ?>',
        fnServerParams: function(aoData) { 
            $.each(pageTable_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
        },
        fnDrawCallback: function(data) {
            // console.log(data);
        },createdRow: function( row, data, dataIndex ) {
            var row_id = $(row).find('.rowClientID').html();
            $(row).attr('id',row_id);
        }
    });
    // });
</script>