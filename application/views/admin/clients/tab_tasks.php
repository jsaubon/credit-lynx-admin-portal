<div class="tab-pane" id="tasks-tab" role="tabpanel">
    <div class="card-body">
        <h5><a href="#" id="btnShowHideMakeTask">Make a task <i class="fas fa-angle-right"></i></a></h5>
        <script>
            $('#btnShowHideMakeTask').on('click',function(e){
                e.preventDefault();
                if ($('#containerMakeTask').hasClass('hide')) {
                    $('#containerMakeTask').removeClass('hide');
                } else {
                    $('#containerMakeTask').addClass('hide');
                }
            });
        </script>
        <div id="containerMakeTask">

            <label class="m-b-0">Templates <small>(<a href="#" id="btnOpeTaskeTemplatesModal" data-toggle="modal" data-target="#add-edit-task-templates-modal">manage</a>)(<a id="btnOpenTaskTemplates" href="#">hide</a>)</small>
            </label>
            <script>
                $('#btnOpenTaskTemplates').on('click',function(e){
                    e.preventDefault();
                    if ($('#task_templates_container').hasClass('hide')) {
                        $('#task_templates_container').removeClass('hide');
                    } else {
                        $('#task_templates_container').addClass('hide');
                    }
                });
            </script>
            <div class="row " id="task_templates_container" style="border: 1px solid #6ec4d7!important; padding-top: 9px; margin-left: 0px; margin-right: 0px; border-radius: 5px; font-size: 13px; margin-bottom: 7px!important;line-height: 70%;">
                <?php foreach ($task_templates as $key => $value): ?>
                    <label class="col-md-6 lh" style="color: #6ec4d7; cursor: pointer;" task_temp="<?php echo $value['task_template'] ?>"><?php echo $value['task_shortname'] ?></label>
                <?php endforeach?>
            </div>


            <textarea style="border-radius: 0px;padding-top: 5px !important;font-size: 70%" id="new_task" class="form-control" rows="10"></textarea>
            <div class="input-group">
                <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Due Date" style="width: 100%">
                <button id="btnSaveNewTask" style="border-radius: 0;width: 1%;flex: 1 1 auto;" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
            </div>

        </div>
        <div class="message-scroll ">
            <div id="task_list" class="profiletimeline m-t-40 ">
            </div>
        </div>
    </div>
</div>