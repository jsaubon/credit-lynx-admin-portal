<style>
    #tab_disputes select {
        /* for Firefox */
        -moz-appearance: none;
        /* for Chrome */
        -webkit-appearance: none;
        appearance: none;
        min-height: 28px;
        font-size: 65%;
        height: 28px;
    }


    /* For IE10 */
    #tab_disputes select::-ms-expand {
        display: none;
        min-height: 28px;
        font-size: 65%;
    }

    #tab_disputes .rightLabel {
        font-size: 8px;
        right: 8px;
        top: 6px;
    }

    #tab_disputes .rightLabelDiv:hover > small{
        display: none;
    }

    #tab_disputes input {
        min-height: 0px;
        font-size:60%;
    }

    #tab_disputes .roundDates {
        min-height: 0px !important;
        font-size: 60%;
        margin-top: 6.4px;
        text-align: right;
        padding-right: 5px;
        float: right;
        border: none;
    }

    .ui-autocomplete {
        z-index: 1300;
    }
</style>
<div style="padding-top: 0;" class="tab-pane b-t" id="tab_disputes" role="tabpanel">
    <div class="card-body">
        <div class="">
            <div class="row">

                <div class="text-center col-12 col-md-4">
                    <h3><a href="#" id="btnOpenBureauLettersModal">Bureau Letters</a></h3>
                </div>
                 <div class="text-center col-12 col-md-4">
                    <h3><a href="#" id="btnAddNewAccountWithSC">Source Code</a></h3>
                    <!-- <h3><a href="#" id="btnOpenCreditorLettersModal">Creditor Letters</a></h3> -->
                </div>
                <div class="text-center col-12 col-md-4">
                    <h3><a id="btnShowHideNewAccount" href="#">New account </a></h3>
                </div>
            </div>

            <!-- <hr> -->
            <div id="containerNewAccount" class="hide">
                <div class="text-center">
                    <span id="span_checkbox_all">
                        <input type="checkbox" id="checkbox_all" class="chk-col-blue-grey">
                        <label  for="checkbox_all">All</label>
                    </span>
                    <br>
                    <span id="span_checkbox_equifax">
                        <input type="checkbox" id="checkbox_equifax" class="chk-col-red">
                        <label  for="checkbox_equifax">Equifax</label>
                    </span>
                    <span id="span_checkbox_experian">
                        <input type="checkbox" id="checkbox_experian" class="chk-col-indigo">
                        <label for="checkbox_experian">Experian</label>
                    </span>
                    <span id="span_checkbox_transunion">
                        <input type="checkbox" id="checkbox_transunion" class="chk-col-green">
                        <label for="checkbox_transunion">TransUnion</label>
                    </span>

                </div>
                <hr style="margin-top: 0">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="bank_account_name">Account Name</label>
                            <input class="form-control" type="text" name="bank_account_name" id="bank_account_name">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="bank_account_number">Account Number</label>
                            <input class="form-control" type="text" name="bank_account_number" id="bank_account_number">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="bank_account_status">Account Status</label>
                            <a tabindex="99999" href="#" data-toggle="modal" data-target="#add-edit-account-status-modal"><i class="fas fa-plus-circle"></i></a>
                            <select class="form-control" name="bank_account_status" id="bank_account_status">

                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="bank_account_type">Account Type</label>
                            <a tabindex="99999" href="#" data-toggle="modal" data-target="#add-edit-account-types-modal"><i class="fas fa-plus-circle"></i></a>
                            <select class="form-control" name="bank_account_type" id="bank_account_type">
                                <option value="">Select Account Type</option>
                                <?php foreach ($userdata['account_types'] as $key => $value): ?>
                                    <option value="<?php echo $value['account_type'] ?>"><?php echo $value['account_type'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="div_bank_date_opened">
                        <div class="">
                            <label class="control-label">Date Opened</label>
                            <input type="text" name="bank_date_opened" id="bank_date_opened" class="form-control monthyear-inputmask"  value="">
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="div_bank_amount">
                        <div class="form-group">
                            <label class="control-label" for="bank_amount">Balance</label>
                            <input class="form-control" type="text" name="bank_amount" id="bank_amount">
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="div_bank_credit_limit">
                        <div class="form-group">
                            <label class="control-label" for="bank_credit_limit">Credit Limit</label>
                            <input class="form-control" name="bank_credit_limit" type="text" id="bank_credit_limit">
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="div_bank_past_due">
                        <div class="form-group">
                            <label class="control-label" for="bank_past_due">Past Due</label>
                            <input class="form-control" type="text" name="bank_past_due" id="bank_past_due">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 hide" id="div_bank_status_date">
                        <div class="form-group">
                            <label class="control-label" for="bank_status_date">Status Date</label>
                            <input class="form-control monthyear-inputmask" name="bank_status_date" type="text" id="bank_status_date">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 hide" id="div_bank_file_date">
                        <div class="form-group">
                            <label class="control-label" for="bank_file_date">File Date</label>
                            <input class="form-control monthyear-inputmask" name="bank_file_date" type="text" id="bank_file_date">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 hide" id="div_bank_chapter">
                        <div class="form-group">
                            <label class="control-label" for="bank_chapter">Chapter</label>
                            <input class="form-control" name="bank_chapter" type="text" id="bank_chapter">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 hide" id="div_bank_correct_info">
                        <div class="form-group">
                            <label class="control-label" for="bank_correct_info">Correct Info</label>
                            <input class="form-control" name="bank_correct_info" type="text" id="bank_correct_info">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 hide" id="div_bank_inquiry_date">
                        <div class="form-group">
                            <label class="control-label" for="bank_inquiry_date">Inquiry Date</label>
                            <input class="form-control" name="bank_inquiry_date" type="text" id="bank_inquiry_date">
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="div_bank_original_creditor">
                        <div class="form-group">
                            <label class="control-label">Original Creditor</label>
                            <input type="text" name="bank_original_creditor" id="bank_original_creditor" class="form-control"  value="">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="">
                            <label class="control-label">Payment Status</label>
                            <a tabindex="99999" href="#" data-toggle="modal" data-target="#add-edit-account-payment-status-modal"><i class="fas fa-plus-circle"></i></a>
                            <select id="bank_payment_status" name="bank_payment_status" class="form-control" style="height: 28px;" >
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="">
                            <label class="control-label">Comments</label>
                            <a tabindex="99999" href="#" data-toggle="modal" data-target="#add-edit-account-comments-modal"><i class="fas fa-plus-circle"></i></a>
                            <select id="bank_comments" name="bank_comments" class="form-control" style="height: 28px;" >

                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="">
                            <label class="control-label">Bureau Date</label>
                            <input type="text" name="global_bureau_date" id="global_bureau_date" class="form-control date-inputmask"  value="">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label" for="bank_dispute_verbiage">
                            Dispute Verbiage</label>
                            <a tabindex="999999" href="#" data-toggle="modal" data-target="#add-edit-dispute-verbiage-modal"><i class="fas fa-plus-circle"></i></a>
                            <select id="selectDisputeVerbiage" class="select2" style="width: 100%;font-size: 60%" >

                            </select>
                            <textarea style="border-radius: 0;font-size: 60%" name="bank_dispute_verbiage" id="bank_dispute_verbiage" rows="3" class="form-control"></textarea>
                        </div>

                    </div>


                    <div class="col-12">
                        <button id="btnSaveNewAccount" class="pull-right btn btn-success waves-effect waves-light">Add account</button>
                    </div>

                </div>

                <hr>
            </div>

        </div>

        <div class="row">
            <div class="col-12" style="padding: 0px">
                <div class="col-12 m-b-20">
                    <div class=" rightLabelDiv">
                        <small class="rightLabel">DFR <u id="profile_dfr" ></u> days</small>
                        <textarea cols="3" id="profile_status_line" name="profile_status_line" class="form-control hide" ></textarea>
                        <!-- <div class="input-group-append" style="width: 110px"> -->
                            <input type="text" id="dispute_date_for_result" name="profile_date_for_result" class="form-control text-center date-inputmask" placeholder="Date for Results">
                        <!-- </div> -->
                    </div>
                    <script>
                        $("#profile_status_line" ).autocomplete({
                          minLength: 0,
                          source: [{
                                    value: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted second round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted third round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted fourth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted fifth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted sixth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted sixth round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Drafted seventh round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    label: 'Drafted seventh round letters to bureaus. Awaiting Equifax, Experian, and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting Equifax and Experian reports.',
                                    label: 'Awaiting Equifax and Experian reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting Equifax and TransUnion reports.',
                                    label: 'Awaiting Equifax and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting Equifax reports.',
                                    label: 'Awaiting Equifax reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting Experian and TransUnion reports.',
                                    label: 'Awaiting Experian and TransUnion reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting Experian reports.',
                                    label: 'Awaiting Experian reports.',
                                    desc: '',
                                  },{
                                    value: 'Awaiting TransUnion reports.',
                                    label: 'Awaiting TransUnion reports.',
                                    desc: '',
                                  },{
                                        value: 'Pending all verifications. Letters are drafted and ready to go.',
                                        label: 'Pending all verifications. Letters are drafted and ready to go.',
                                        desc: ''
                                    },
                                    {
                                        value: 'Pending (1) Address Verification. Letters are drafted and ready to go.',
                                        label: 'Pending (1) Address Verification. Letters are drafted and ready to go.',
                                        desc: ''
                                    },
                                    {
                                        value: 'Pending (1) Address and (1) Social Security Verification. Letters are drafted and ready to go.',
                                        label: 'Pending (1) Address and (1) Social Security Verification. Letters are drafted and ready to go.',
                                        desc: ''
                                    },
                                    {
                                        value: 'Pending (2) Address Verifications. Letters are drafted and ready to go.',
                                        label: 'Pending (2) Address Verifications. Letters are drafted and ready to go.',
                                        desc: ''
                                    },
                                    {
                                        value: 'Pending (1) Social Security Verification. Letters are drafted and ready to go.',
                                        label: 'Pending (1) Social Security Verification. Letters are drafted and ready to go.',
                                        desc: ''
                                    },{
                                      value: 'Pending Social Security Verification and Dispute Input.',
                                      label: 'Pending Social Security Verification and Dispute Input.',
                                      desc: '', 
                                    },{
                                      value: 'Pending Dispute Input.',
                                      label: 'Pending Dispute Input.',
                                      desc: '', 
                                    },{
                                      value: 'Pending Result Review and Account Update.',
                                      label: 'Pending Result Review and Account Update.',
                                      desc: '', 
                                    },{
                                      value: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
                                      label: 'NSF - Account Suspended due to Non-Payment of Previous Month\'s Service.',
                                      desc: '', 
                                    },{
                                      value: 'NSF - Account Terminated due to Non-Payment of Previous Month\'s Service.',
                                      label: 'NSF - Account Terminated due to Non-Payment of Previous Month\'s Service.',
                                      desc: '', 
                                    },{
                                      value: 'Cancelled',
                                      label: 'Cancelled',
                                      desc: '', 
                                    }
                                  ],
                          focus: function( event, ui ) {
                            $( "#profile_status_line" ).val( ui.item.label );
                            return false;
                          },
                          select: function( event, ui ) {
                            $( "#profile_status_line" ).val( ui.item.label );

                            return false;
                          }
                        })
                    </script>

                </div>
                <div class="text-left" style="padding-left: 15px;">
                    <h5><a href="" data-toggle="modal" data-target="#add-edit-disputes-account">Edit Accounts</a></h5>
                </div>
                <div class="vtabs customvtab customvtabDisputes" style="width: 100%">
                    <div class="toSlimScrollDisputes">
                    <ul class="nav nav-tabs tabs-vertical tabs-vertical-disputes" role="tablist" style="width: 20%">
                    </ul>
                    </div>

                    <!-- Tab panes -->
                    <div  class="tab-content" style="padding: 5px;width: 100% !important">
                        <div class="row" id="custom_tab_content_disputes" style="margin-left: 0px;margin-right: 0px">
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-equifax" ><span span-color="text-equifax" bureau="Equifax" class="btnGetBureauAccounts spanPointer">Equifax</span></h3>
                                <div  id="section_content_equifax" >
                                </div>
                            </section>
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-experian" ><span span-color="text-experian" bureau="Experian" class="btnGetBureauAccounts spanPointer">Experian</span></h3>
                                <div id="section_content_experian" >
                                </div>
                            </section>
                            <section class="col-12 col-md-4">
                                <h3 class="text-center text-transunion" ><span span-color="text-transunion" bureau="TransUnion" class="btnGetBureauAccounts spanPointer">TransUnion</span></h3>
                                <div id="section_content_transunion" >
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

