 

<div style="padding-top: 0" class="tab-pane b-t" id="tab_evaluation" role="tabpanel">
    <div class="card-body">  
        <h2 class="text-themecolor">Credit Evaluation</h2>
        <div class="row parent_div" question="1">
            <div class="col-lg-8 col-md-8 col-12">
                1. Do they have an open credit card?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_1_yes" class="filled-in checkbox_yes" value="1" />
                            <label for="question_1_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_1_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_1_no">No</label>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-7 col-md-7 offset-1 col-12 animated fadeInDown hide sub_div">
                1a. Are their balances greater than 20%?
            </div>
            <div class="col-lg-4 col-md-4 col-12 fadeInDown hide sub_div">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_1a_yes" class="filled-in" value="1" />
                            <label for="question_1a_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_1a_no" class="filled-in" value="0" />
                            <label for="question_1a_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>

        <div class="row parent_div hide animated fadeInDown" question="2">
            <div class="col-lg-8 col-md-8 col-12">
                2. Are there any late payments for the past 12 months?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_2_yes" class="filled-in yu" value="1" />
                            <label for="question_2_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_2_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_2_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>

        <div class="row parent_div hide animated fadeInDown" question="3">
            <div class="col-lg-8 col-md-8 col-12">
                3. Are there any collections?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3_yes" class="filled-in checkbox_yes" value="1" />
                            <label for="question_3_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_3_no">No</label>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-5 col-md-5 offset-1 col-12 animated fadeInDown hide sub_div_1">
                3a. How many? 
            </div>
            <div class="col-lg-6 col-md-6 col-12 fadeInDown hide sub_div_1">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3a_1to3" class="filled-in" value="1-3" />
                            <label for="question_3a_1to3">1-3</label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3a_4to9" class="filled-in" value="4-9" />
                            <label for="question_3a_4to9">4-9</label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3a_10plus" class="filled-in" value="10+" />
                            <label for="question_3a_10plus">10+</label>
                        </div>
                    </div>
                </div> 
            </div> 
            <div class="col-lg-7 col-md-7 offset-1 col-12 animated fadeInDown hide sub_div_2">
                3b. Do non-medical collections total more than $2,000?
            </div>
            <div class="col-lg-4 col-md-4 col-12 fadeInDown hide sub_div_2">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3b_yes" class="filled-in" value="1" />
                            <label for="question_3b_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_3b_no" class="filled-in" value="0" />
                            <label for="question_3b_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>


        <div class="row parent_div hide animated fadeInDown" question="4">
            <div class="col-lg-8 col-md-8 col-12">
                4. Do they have any charge-offs?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_4_yes" class="filled-in yu" value="1" />
                            <label for="question_4_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_4_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_4_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <div class="row parent_div hide animated fadeInDown" question="5">
            <div class="col-lg-8 col-md-8 col-12">
                5. Do they have a Reposession?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_5_yes" class="filled-in yu" value="1" />
                            <label for="question_5_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_5_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_5_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <div class="row parent_div hide animated fadeInDown" question="6">
            <div class="col-lg-8 col-md-8 col-12">
                6. Do they have a Bankruptcy?
            </div>
            <div class="col-lg-4 col-md-4 col-12"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_6_yes" class="filled-in yu" value="1" />
                            <label for="question_6_yes">Yes</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="question_6_no" class="filled-in checkbox_no" value="0" />
                            <label for="question_6_no">No</label>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>


        <div class="row hide p-l-20 p-r-20 animated fadeInDown" id="evaluation_rec">
            <div class="col-12 text-center b-t b-b b-l b-r p-20 ">
                <h3 class="text-center text-themecolor">Great News!</h3>
                <p class="m-b-0">We recommend the Premiere Plan for this customer.</p>
            </div>

        </div>
    </div>

</div> 
<script>

</script>