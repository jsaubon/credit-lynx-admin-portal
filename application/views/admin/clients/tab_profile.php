<style>
    .div_automations .chosen-container { 
        flex: 1 1 auto;
        width: 1% !important;
        line-height: 2.4; 
    }

    .div_automations .chosen-container-multi .chosen-choices li.search-choice {
        line-height: 17.5px; 
    }

    .div_automations .chosen-container-multi .chosen-choices li.search-field {
        padding-left: 5px;
    }

    .div_automations .chosen-container-multi .chosen-results { 
        margin: 2px;
        padding: 5px;
    }

    .div_automations .chosen-container-multi .chosen-choices { 
        padding-left: 5px;
    }

    @media (max-width: 700px) {
        .w-100 {
            width: 100%;
        }
    }
</style>
<div style="padding-top: 0;" class="tab-pane b-t active show" id="tab_profile" role="tabpanel">
    <div class="card-body">
        <div class="row">
            <div class="col-md-4 col-xs-12 text-center b-r" >
                <!-- <form id="uploadProfilePictureForm" enctype="multipart/form-data"> -->
                    <!-- <img src="../assets/images/LynxLogo.png" width="120"> -->
                    <!-- <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="" data-show-remove="false" data-height="150" /> -->
                <!-- </form>    -->
                <br>
                <h3 class=" m-b-0"><u class="name"></u>
                    <a id="btnCallClient" href="#"><i class="fas fa-phone"></i></a>
                    <br>
                    <span class="labelArchived hide text-danger">Archived</span>
                </h3>
                <!-- <strong>Name</strong><br> -->
                <br>
                <div class="text-left">
                    
                        <div class="input-group show_lead_content">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Lead Status</label>
                            </div>

                            <select class="form-control" id="profile_lead_status" name="profile_lead_status" >
                                <option value="0">Select Status</option>
                                <?php foreach ($userdata['lead_statuses'] as $key => $value): ?>
                                    <option value="<?php echo $value['lead_status'] ?>"><?php echo $value['lead_status'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="text-center show_lead_content">
                            <a href="#" id="btnConvertToClient">Convert to Client <i class="fas fa-thumbs-up"></i></a>
                        </div>

                    
                    
                        <div class="input-group show_client_content">
                            <div class="input-group-prepend w-100">
                                <label class="input-group-text w-100">Client Status</label>
                            </div>
                            <select class="form-control" id="profile_client_status" name="profile_client_status" >
                                <option value="0">Select Status</option>
                                <?php foreach ($userdata['client_statuses'] as $key => $value): ?>
                                    <option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="input-group show_client_content">
                            <div class="input-group-prepend w-100">
                                <label class="input-group-text w-100">Client Grade</label>
                            </div>
                            <input type="text" disabled class="form-control client_grade">
                        </div>
                    
                    <hr>
                    
                        <span class="show_client_content">Last Login: <span class="last_login"></span></span><br>
                    
                    
                        <span class="show_lead_content">Date Entered: <span class="date_created"></span></span><br>
                    
                    <small class="joint_container hide">Access
                        <a class="changeToOtherAccount" href="#"></a> account?
                    </small>
                </div>
            </div>
            <div class="col-md-8">
                <div id="sectionClientAssignments">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Agent</label>
                        </div>
                        <select class="select2" name="profile_agent_id"  style="width: 100%">
                            <option value="0">Select Agent</option>
                            <?php foreach ($userdata['agents'] as $key => $value): ?>
                                <option value="<?php echo $value['agent_id'] ?>"><?php echo $value['name'] ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Broker</label>
                        </div>
                        <select class="select2" name="profile_broker_id"  style="width: 100%">
                            <option value="0">Select Broker</option>
                            <?php foreach ($userdata['brokers'] as $key => $value): ?>
                                <option value="<?php echo $value['broker_id'] ?>"><?php echo $value['name'] ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Sales</label>
                        </div>
                        <select class="select2" name="profile_sale_id" id="sales"   style="width: 100%">
                            <option value="0">Select Sales</option>
                            <?php foreach ($userdata['sales'] as $key => $value): ?>
                                <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    
                        <div class="input-group show_client_content">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Processor</label>
                            </div>
                            <select class="select2" name="profile_processor_id"  style="width: 100%">
                                <option value="0">Select Processor</option>
                                <?php foreach ($userdata['processors'] as $key => $value): ?>
                                    <option value="<?php echo $value['employee_id'] ?>"><?php echo $value['name'] ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    


                </div>

                <div class="m-t-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Username</label>
                        </div>
                        <input type="text" name="profile_username" id="profile_username" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Password</label>
                        </div>
                        <input type="password" autocomplete="new-password" name="profile_password" id="profile_password" class="form-control" value="********" >
                    </div>

                    <!-- <div class="input-group div_automations">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Automations</label>
                        </div>
                        <select class="select_automation form-control" data-placeholder="Pick Automation" >
                            
                        </select>
                    </div> -->
                    <div class="row m-t-10">
                        <div class="col-md-4 col-12">
                            
                        </div>
                        <div class="col-md-4 col-12">
                            <button id="SendSignUpLinkRenew" class="btn btn-success btn-block waves-effect waves-light" type="button">Send Signup <i class="fas fa-edit"></i></button>
                        </div>
                        <div class="col-md-4 col-12">
                            
                        </div>
                    </div>  
                    <div class="row m-t-10"> 
                        <div class="col-md-4 col-12">
                            
                        </div>
                        <div class="col-md-4 col-12">
                            <button class="btn btn-primary btn_set_appointment btn-block">Set Appointment <i class="fas fa-calendar"></i></button>
                        </div>
                        <div class="col-md-4 col-12">
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <hr>
        <h3>Personal Information</h3>
        <div class="row" id="sectionProfileInformation">
            <div class="col-md-6 col-xs-6 b-r">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="name" class="input-group-text">Name</label>
                    </div>
                    <input type="text" name="profile_name" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="ss" class="input-group-text">Social</label>
                    </div>
                    <?php if ($userdata['login_folder'] == 'employees'): ?>
                        <input type="text" name="profile_ss" class="form-control " >
                    <?php endif?>
                    <?php if ($userdata['login_folder'] == 'admins'): ?>
                        <input type="text" name="profile_ss" class="form-control ss-inputmask" >
                    <?php endif?>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="date_of_birth" class="input-group-text">Date of Birth</label>
                    </div>
                    <input type="text" name="profile_date_of_birth" class="form-control date-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="email_address" class="input-group-text">Email
                             <a href="#" class="btnAddNewOtherEmail" style="margin-left: 5px;"><i class="fas fa-plus-circle"></i></a>
                        </label>
                    </div>
                    <input type="email" name="profile_email_address" class="form-control" >
                </div>
                <section class="containerClientOtherEmails">
                </section>

                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="alt_phone" class="input-group-text">Phone</label>
                    </div>
                    <input type="text" name="profile_alt_phone" class="form-control phone-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="cell_phone" class="input-group-text">Cell Phone</label>
                    </div>
                    <input type="text" name="profile_cell_phone" class="form-control phone-inputmask" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="profile_carrier" class="input-group-text">Carrier</label>
                    </div>
                    <select class="form-control" name="profile_carrier" >
                        <option value="0">Select Carrier</option>
                        <option value="ATT">ATT</option>
                        <option value="Boost">Boost</option>
                        <option value="Cricket">Cricket</option>
                        <option value="Metro PCS">Metro PCS</option>
                        <option value="Simple">Simple</option>
                        <option value="Sprint">Sprint</option>
                        <option value="TMobile">TMobile</option>
                        <option value="Verizon">Verizon</option>
                        <option value="Virgin">Virgin</option>
                        <option value="Tracfone">Tracfone</option>
                        <option value="Do Not SMS">Do Not SMS</option>
                        <option value="Landline">Landline</option>
                        <option value="Bluegrass Cellular">Bluegrass Cellular</option>
                    </select>
                </div>



            </div>
            <div class="col-md-6 col-xs-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="address" class="input-group-text">Address</label>
                    </div>
                    <input type="text" name="profile_address" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="city" class="input-group-text">City</label>
                    </div>
                    <input type="text" name="profile_city" class="form-control" >
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="state" class="input-group-text">State</label>
                    </div>
                    <select class=" form-control" name="profile_state_province" style="height: calc(2.25rem + 2px) !important">
                        <?php foreach ($userdata['state_provinces'] as $key => $value): ?>
                            <option value="<?php echo $value ?>"><?php echo $value ?></option>
                        <?php endforeach?>
                    </select>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="zip_postal_code" class="input-group-text">Zip</label>
                    </div>
                    <input type="text" name="profile_zip_postal_code" class="form-control" >
                </div>
                <br>
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="font" class="input-group-text">Font</label>
                    </div>
                    <select class="form-control" name="profile_font">
                        <option value='Georgia, serif'>Georgia</option>
                        <option value='"Palatino Linotype", "Book Antiqua", Palatino, serif'>Palatino Linotype</option>
                        <option value='"Times New Roman", Times, serif'>Times New Roman</option>
                        <option value='Arial, Helvetica, sans-serif'>Arial</option>
                        <option value='"Arial Black", Gadget, sans-serif'>Arial Black</option>
                        <option value='"Comic Sans MS", cursive, sans-serif'>Comic Sans MS</option>
                        <option value='Impact, Charcoal, sans-serif'>Impact</option>
                        <option value='"Lucida Sans Unicode", "Lucida Grande", sans-serif'>Lucida Sans Unicode</option>
                        <option value='Tahoma, Geneva, sans-serif'>Tahoma</option>
                        <option value='"Trebuchet MS", Helvetica, sans-serif'>Trebuchet MS</option>
                        <option value='Verdana, Geneva, sans-serif'>Verdana</option>
                        <option value='"Courier New", Courier, monospace'>Courier New</option>
                        <option value='"Lucida Console", Monaco, monospace'>Lucida Console</option>
                    </select>
                </div>



            </div>
        </div>
    </div>


</div>

