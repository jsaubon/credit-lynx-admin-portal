<style>
    /*#pageTable .b-r {
        border-right: 1px solid #dee2e6 !important;
    }*/

    #listSection  .dataTables_length {
        float: right;;
    }

    #pageTable tbody tr td {
        line-height: 18px !important;
    }

    .client-dashboard-loader {
        background-color: rgba(255,255,255,.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
    }
</style>
<div id="listSection" class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body" >
                    <!-- <h5 class="card-title"><?php echo $page_title ?> (<?php echo $user_count ?>)

                    </h5>  -->
                    <div class="client-dashboard-loader" style="display: none;" >
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                        </svg>
                    </div>

                    <div class="" >
                        <div class="row">
                            <div class="col-12 text-center">  
                                <?php if ($page_title == 'Leads'): ?>
                                    <a href="#" class="btn btn-success waves-effect waves-light p-l-20 p-r-20" id="btnNewUser" data-toggle="modal" data-target="#add-edit-lead-modal" ><i class="fas fa-user-plus"></i> Add New <?php echo $page_title_s ?> </a>
                                <?php endif?>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="">Global Search</label>
                                <br>
                                <input type="text" name="" class="form-control" placeholder="" id="searchInputClient" style="width: 70%">

                                <?php $this->load->view('admin/clients/global_search');?>
                                <select  id="searchClient" class="hide">
                                    <option value="">select client</option>s
                                    <?php foreach ($all_clients as $key => $value): ?>

                                        <option value="<?php echo $value['client_id'] . '_client' ?>"><?php echo $value['name'] ?></option>
                                        <?php if ($value['client_type'] == 'Joint'): ?>
                                            <option value="<?php echo $value['client_joint_id'] . '_joint_' . $value['client_id'] ?>"><?php echo $value['joint_name'] ?></option>
                                        <?php endif?>
                                    <?php endforeach?>
                                </select>
                            </div>

                        </div>
                        <div class="table-responsive" >

                            <table id="pageTable" class="table stylish-table" style="width: 100% !important">
                                <thead>
                                    <?php if ($page_title == 'Leads'): ?>
                                        <tr>
                                            <th order_by="name">Name</th>
                                            <th order_by="cell_phone">Phone</th>
                                            <th order_by="email_address">Email</th>
                                            <th order_by="last_contact">Last Contact</th>
                                            <th order_by="paid">Start Date</th>
                                            <!-- <th order_by="client_card_sent">Card Sent</th> -->
                                            <th order_by="broker">Broker</th>
                                            <th order_by="agent">Agent</th>
                                            <th order_by="lead_status">Status</th>
                                            <th order_by="">Tools</th>
                                        </tr>
                                    <?php endif?>
                                    <?php if ($page_title == 'Clients'): ?>
                                        <tr>
                                            <th order_by="name">Name</th>
                                            <th order_by="cell_phone">Phone</th>
                                            <th order_by="email_address">Email</th>
                                            <!-- <th order_by="date_of_birth">Date of Birth</th> -->
                                            <!-- <th order_by="client_card_sent">Card Sent</th> -->
                                            <th order_by="paid">Date Paid</th>
                                            <th order_by="day_due">Cycle</th>
                                            <th order_by="client_status">Status</th>
                                            <th order_by="dfr">DFR</th>
                                            <th order_by="broker">Broker</th>
                                        </tr>
                                    <?php endif?>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</div>
<div class="div_select_status_dashboard">
    <?php if ($page_title == 'Clients'): ?>
        <select class="form-control hide select_status_dashboard" style="border-radius: 0px;height: calc(2.15rem);width: 120px">
            <?php foreach ($client_statuses as $key => $value): ?>
                <option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
            <?php endforeach ?>
        </select>
    <?php endif ?>
    <?php if ($page_title == 'Leads'): ?>
        <select class="form-control hide select_status_dashboard" style="border-radius: 0px;height: calc(2.15rem);width: 120px">
            <?php foreach ($lead_statuses as $key => $value): ?>
                <option value="<?php echo $value['lead_status'] ?>"><?php echo $value['lead_status'] ?></option>
            <?php endforeach ?>
        </select>
    <?php endif ?>
</div>
    
    

<script>


    var pageTable;
    var pageTable_data =    [
                                { name: 'page_title', value: '<?php echo $page_title ?>' },
                                { name: 'filter', value: '0' }
                            ];



    var pageTable_label = "<span style='font-size: 14px'><?php echo $page_title ?> (<?php echo $user_count ?>)</span>";

    var pageTable_filter_client = '<select class="form-control selectPageTableFilter" style="border-radius: 0px;height: calc(2.15rem);width: 120px">\
                                <option value="">All</option>\
                                <option value="name">Name</option>\
                                <option value="cell_phone">Phone</option>\
                                <option value="email_address">Email</option>\
                                <option value="paid">Date Paid</option>\
                                <option value="cycle">Cycle</option>\
                                <option value="client_status">Client Status</option>\
                                <option value="dfr">DFR</option>\
                                <option value="broker">Broker</option>\
                                <option value="NSF">NSF</option>\
                                <option value="NSF2">NSF2</option>\
                                <option value="Terminated">Terminated</option>\
                            </select>\ ';
    var pageTable_filter_lead = '<select class="form-control selectPageTableFilter" style="border-radius: 0px;height: calc(2.15rem);width: 120px">\
                                <option value="">All</option>\
                                <option value="name">Name</option>\
                                <option value="cell_phone">Phone</option>\
                                <option value="email_address">Email</option>\
                                <option value="last_contact">Last Contact</option>\
                                <option value="paid">Start Date</option>\
                                <option value="broker">Broker</option>\
                                <option value="agent">Agent</option>\
                                <option value="lead_status">Lead Status</option>\
                            </select>\ ';
    var pageTable_statuses = $('.div_select_status_dashboard').html();

    var pageTable_filter_select = pageTable_filter_client+pageTable_statuses;
    var exportButton = exportButton = '<a style="margin-right: 10px;" target="_blank" href="<?php echo base_url('leadsrain/generateClientsDataExcel') ?>" class="btn btn-success">Export <i class="fas fa-download"></i></a>';;
    if ('<?php echo $page_title ?>' == 'Leads') {
        pageTable_filter_select = pageTable_filter_lead+pageTable_statuses;
        exportButton = '<a style="margin-right: 10px;" target="_blank" href="<?php echo base_url('leadsrain/generateLeadsDataExcel') ?>" class="btn btn-success">Export <i class="fas fa-download"></i></a>';
    }


    var btn_bulk_sms = '<button class="btn btn-success btn_bulk_sms pull-right"><i class="fas fa-envelope"></i> Send Bulk Sms</button>';
    pageTable = $('#pageTable').DataTable({
        language: {
            search: pageTable_label+'<br>'+pageTable_filter_select,
            searchPlaceholder: 'Search'
        },
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 50,
        dom: 'Rlfrtip',
        // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
        bProcessing: true,
        bServerSide: true,
        sServerMethod: 'POST',
        sAjaxSource: '<?php echo base_url("admin/clients/getTableClientsLeads") ?>',
        fnServerParams: function(aoData) {
            $.each(pageTable_data, function(i, field) {
                aoData.push({ name: field.name, value: field.value });
            });
            $('.client-dashboard-loader').fadeIn('50');
        },
        fnDrawCallback: function(data) {
            // console.log(data);
            $('.client-dashboard-loader').fadeOut('50');
        },createdRow: function( row, data, dataIndex ) {
            var row_id = $(row).find('.rowClientID').html();
            $(row).attr('id',row_id);

            if ('<?php echo $page_title ?>' == 'Clients') {
                var row_status = $(row).find('.rowClientStatus').html();
                var bg_color = 'white';
                var label_color = '#26c6da';
                var other_color = '#67757c';
                if (row_status == 'AV' || row_status == 'AV2' || row_status == 'AVSSN' || row_status == 'SSN' || row_status == 'Verifications') {
                    bg_color = '#7460ee';
                    other_color = 'white';
                    label_color = 'white';
                } else if (row_status == 'Hold' || row_status == 'Review') {
                    bg_color = '#26c6da';
                    other_color = 'white';
                    label_color = 'white';
                } else if (row_status == 'NSF' || row_status == 'NSF2' || row_status == 'NSF3' || row_status == 'Suspended') {
                    bg_color = '#fc4b6c';
                    other_color = 'white';
                    label_color = 'white';
                } else if (row_status == 'Terminated') {
                    bg_color = '#a50005';
                    label_color = 'white';
                    other_color = 'white';
                } else if (row_status == 'Cancelled') {
                    bg_color = '#ececf1';
                    label_color = '#777777';
                } else if (row_status == 'Cancelling') {
                    bg_color = '#bbbbbb';
                    label_color = '#777777';
                }

                $(row).css('background-color',bg_color);
                $(row).find('.goToProfile,.goToJointProfile,.btnCallClientRC').css('color',label_color);
                $(row).find('td:nth-child(3),td:nth-child(4),td:nth-child(5),td:nth-child(6),td:nth-child(7),td:nth-child(8)').css('color',other_color);
                if (row_status == 'Active') {
                    $(row).find('td:nth-child(6)').css('color','#26c6da');
                }

                var joint_status = $(row).find('.selectClientJointStatus').val();
                if (!$.isEmptyObject(joint_status)) {
                    var joint_color = '';
                    if (joint_status == 'AV' || joint_status == 'AV2' || joint_status == 'AVSSN' || joint_status == 'SSN' || joint_status == 'Verifications') {
                        joint_color = '#7460ee';
                    } else if (joint_status == 'Hold' || joint_status == 'Review') {
                        joint_color = '#26c6da';
                    } else if (joint_status == 'NSF' || joint_status == 'NSF2' || joint_status == 'NSF3' || joint_status == 'Suspended') {
                        joint_color = '#fc4b6c';
                    } else if (joint_status == 'Terminated') {
                        joint_color = '#a50005';
                    } else if (joint_status == 'Cancelled') {
                        joint_color = '#ececf1';
                    } else if (joint_status == 'Cancelled') {
                        joint_color = '#bbbbbb';
                    }
                    if (joint_color != bg_color) {
                        $(row).find('.goToJointProfile').find('u').css('color', joint_color);
                    }

                }

                var client_dfr = $(row).find('td:nth-child(7)').find('.client_dfr');
                var joint_dfr = $(row).find('td:nth-child(7)').find('.joint_dfr');
                if (!$.isEmptyObject(client_dfr)) {
                    if (parseInt(client_dfr.html()) <= 5) {
                        if (row_status == 'NSF') {
                            client_dfr.css('color', 'black');
                        } else {
                            client_dfr.css('color', 'red');
                        }
                    }
                }
                if (!$.isEmptyObject(joint_dfr)) {
                    if (parseInt(joint_dfr.html()) <= 5) {
                        if (row_status == 'NSF') {
                            joint_dfr.css('color', 'black');
                        } else {
                            joint_dfr.css('color', 'red');
                        }
                    }
                }
            } else {
                var row_status = $(row).find('.rowClientStatus').html();
                var bg_color = 'white';
                var label_color = '#26c6da';
                var other_color = '#67757c';
                if (row_status == 'AV') {
                    bg_color = 'green';
                    other_color = 'white';
                    label_color = 'white';
                }  

                $(row).css('background-color',bg_color);
                $(row).find('.goToProfile,.goToJointProfile,.btnCallClientRC').css('color',label_color);
                $(row).find('td:nth-child(3),td:nth-child(4),td:nth-child(5),td:nth-child(6),td:nth-child(7),td:nth-child(8)').css('color',other_color);
                if (row_status == 'Active') {
                    $(row).find('td:nth-child(6)').css('color','#26c6da');
                }
            }


        }
    });

    $('.dataTables_length').append('<br>'+btn_bulk_sms);
    $('#pageTable_wrapper input[type="search"]').unbind();
    var pageTable_filter = '<?php echo $filter ?>';
    var pageTable_search = '<?php echo $search ?>';
    $("#pageTable_wrapper").on("change",'.selectPageTableFilter',function(){
        var filter = $(this).val();
        pageTable_data.push({name: 'filter', value: filter});
        pageTable_filter = filter;
        if (filter == 'NSF' || filter == 'NSF2' || filter == 'Terminated') {
            pageTable.search($('#pageTable_wrapper').find('input[type="search"]').val()).draw();
        }
        if (filter == 'client_status' || filter == 'lead_status') {
            $('#pageTable_wrapper').find('.select_status_dashboard').removeClass('hide');
            $('#pageTable_wrapper').find('input[type="search"]').addClass('hide');
        } else {
            $('#pageTable_wrapper').find('.select_status_dashboard').addClass('hide');
            $('#pageTable_wrapper').find('input[type="search"]').removeClass('hide');
        }
    });

    $('#pageTable_wrapper').on('keyup','input[type="search"]',function(e){
        e.preventDefault();
        pageTable_search = $(this).val();
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if(code==13) {
            pageTable.search($(this).val()).draw();
        }
    });

    $('#pageTable_wrapper').on('change', '.select_status_dashboard', function(event) {
        event.preventDefault();
        pageTable.search($(this).val()).draw();
    });

    $('.btn_bulk_sms').on('click', function(event) {
        event.preventDefault();
        var send_bulk_sms_to = $('.send_bulk_sms_to');
        $('.to_count').html('0');
        send_bulk_sms_to.empty();

        var tbody_tr = $('#pageTable tbody tr');
        var to =    [ 
                        
                    ];
        $.each(tbody_tr, function(index, tr) {
            if (($(tr).find('.goToJointProfile')).length == 0) {
                to.push(    {
                                'name': $(tr).find('td:nth-child(1)').find('.goToProfile').find('u').html(),
                                'phoneNumber': $(tr).find('td:nth-child(2)').find('.btnCallClientRC').html(),
                                'id': $(tr).attr('id'),
                                'type': 'client'
                            }
                        );
            } else {
                to.push(    {
                                'name': $(tr).find('td:nth-child(1)').find('.goToProfile').find('u').html(),
                                'phoneNumber': $(tr).find('td:nth-child(2)').find('[client_type="client"]').html(),
                                'id': $(tr).attr('id'),
                                'type': 'client'
                            }
                        );
                to.push(    {
                                'name': $(tr).find('td:nth-child(1)').find('.goToJointProfile').find('u').html(),
                                'phoneNumber': $(tr).find('td:nth-child(2)').find('[client_type="joint"]').html(),
                                'id': $(tr).find('td:nth-child(1)').find('.goToJointProfile').attr('value'),
                                'type': 'joint'
                            }
                        );
            }
            
        });

        $.each(to, function(index, val) {
            if (val.phoneNumber != '') {
                send_bulk_sms_to.append('<option selected="" id="'+val.id+'" type="'+val.type+'" value="'+trim_phone(val.phoneNumber)+'">'+val.name+' '+val.phoneNumber+'</option>');
            }
            
        });
        $('.to_count').html(to.length);
 
        $('#modal_send_bulk_sms').modal('show');
    });

    function trim_phone(str) {
        str = str.replace(/ /g,'');
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.replace(/-/g,'');

        return str;
    }

        

        




</script>