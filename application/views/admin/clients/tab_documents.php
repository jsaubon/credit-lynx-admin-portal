<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script src="<?php echo base_url('assets/js/dom-to-image.min.js') ?>"></script>
<style>
    .dropify-render > img{
            border-radius: 0 !important;
    }

    .dropify-message p {
        text-align: center !important;
    }

    #tableClientDocuments [type=checkbox].filled-in:checked+label:before,#tableClientDocuments [type=checkbox].filled-in:checked+label:after,#tableClientDocuments [type=checkbox].filled-in:not(:checked)+label:after {
        top: 10px !important;
    }

    #tableClientDocuments thead [type=checkbox].filled-in:checked+label:before,#tableClientDocuments thead [type=checkbox].filled-in:checked+label:after,#tableClientDocuments thead [type=checkbox].filled-in:not(:checked)+label:after {
        top: 20px !important;
        /*left: 2.1px !important;*/
    }

    .containerClientsFax .input-group-text{
        min-width: 0px !important;
    }

    #tableClientDocuments select::-ms-expand {
        display: none;
    } 

    #tableClientDocuments select{
        -webkit-appearance: none;
        appearance: none;
    }

    #tableClientDocuments .select_document_category {
        border: none !important; 
        background-color: white;
    }

</style>
<div style="padding-top: 0;" class="tab-pane b-t" id="tab_documents" role="tabpanel">
    <div class="card-body">
        <div class="row 
        <?php if ($userdata['login_type'] == 'Sales Team'): ?>
            hide
        <?php endif ?>">
            <div class="text-center col-12">
                <a href="#" id="btnSendClientWelcomeEmail" class="btn  btn-primary waves-effect waves-light">Resend Welcome Email <i class="fas fa-paper-plane"></i></a>
                <a href="#" id="btnSendClientVerifEmail" class="btn  btn-info waves-effect waves-light">Send Verification Email</a>
                <button id="SendSignUpLink" class="btn  btn-success waves-effect waves-light" type="button">Send Agreement</button>
                <button id="btnConvertToJoint" class="hide btn  btn-info waves-effect waves-light" type="button"  data-toggle="modal" data-target="#add-edit-client-joint-account">Convert to Joint Account</button>
                <button id="btnCancellClient" class="btn  btn-danger waves-effect waves-light" type="button"  >Send Cancellation</button>
            </div>
        </div>

        <div class="row p-10" >

            <div class="col-md-4 col-12 b-r">
                <div class="col-12 sectionEnrollmentInformation 
                <?php if ($userdata['login_type'] == 'Sales Team'): ?>
                    hide
                <?php endif ?>">
                    <span id="span_checkbox_account">
                        <input type="checkbox" id="checkbox_account" class="chk-col-green">
                        <label  for="checkbox_account">Account</label>
                    </span>
                    <span id="span_checkbox_dropoff">
                        <input type="checkbox" id="checkbox_dropoff" class="chk-col-green">
                        <label for="checkbox_dropoff">Dropoff</label>
                    </span>
                    <span id="span_checkbox_email">
                        <input type="checkbox" id="checkbox_email" class="chk-col-green">
                        <label for="checkbox_email">Email</label>
                    </span>
                    <span id="span_checkbox_fax">
                        <input type="checkbox" id="checkbox_fax" class="chk-col-green">
                        <label for="checkbox_fax">Fax</label>
                    </span>
                    <span id="span_checkbox_mail">
                        <input type="checkbox" id="checkbox_mail" class="chk-col-green">
                        <label for="checkbox_mail">Mail</label>
                    </span>
                </div>
                <div class="col-12  containerClientsFax 
                <?php if ($userdata['login_type'] == 'Sales Team'): ?>
                    hide
                <?php endif ?>">
                    <div class="input-group m-b-5">
                        <div class="input-group-prepend">
                            <label for="fax" class="input-group-text">Fax </label>
                        </div>
                        <input type="text" name="profile_fax" id="profile_fax" class="form-control" >
                        <div class="input-group-append">
                            <a href="#" class="btnAddClientsFax input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                    <div class="containerClientsFaxSection">

                    </div>
                </div>
                <hr>
                <div class="col-12">
                    <form enctype="multipart/form-data" id="formDocumentFileUpload" method="POST">
                        <input id="inputFileDocument" name="document[]" type="file" class="dropify" multiple />
                        <input type="text" name="" class="form-control" id="url_attachment" placeholder="Drag and Drop from Gmail">
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <label class="input-group-text" style="min-width: 100px !important">Category</label>
                            </div>
                            <select name="category" style="height: calc(2.25rem + 2px) !important" class="form-control">
                                <option value="Credit Report">Credit Report</option>
                                <option value="Equifax">Equifax</option>
                                <option value="Experian">Experian</option>
                                <option value="TransUnion">TransUnion</option>
                                <option value="Service Agreement">Service Agreement</option>
                                <option value="New Payment">New Payment</option>
                                <option value="Social">Social</option>
                                <option value="Address (1)">Address (1)</option>
                                <option value="Address (2)">Address (2)</option>
                                <option value="AVSSN">AVSSN</option>
                                <option value="Creditor Letters">Creditor Letters</option>
                                <option value="Other">Other</option>
                            </select>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-success btn_upload_doc">Upload  </button>
                            </div>
                        </div>

                    </form>

 
                </div>
            </div>
            <div class="col-md-4 col-12 sectionEnrollmentInformation b-r" >
                
                    <div class="input-group m-b-5 show_client_content">
                        <div class="input-group-prepend">
                            <label for="date_of_enrollment" class="input-group-text">Enrolled</label>
                        </div>
                        <input type="text"  name="enrollment_date_of_enrollment" class="form-control date-inputmask">
                        <div class="input-group-append">
                            <a href="#" class="btn_add_enrolled input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                    <div class="containerClientsEnrolled">
                        
                    </div>
                
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label for="service_agreement" class="input-group-text">Service Agreement</label>
                    </div>
                    <input type="text" name="enrollment_service_agreement" class="form-control date-inputmask"> 
                    <div class="input-group-append">
                        <a href="#" class="btn_add_sa input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                    </div>
                </div> 
                <div class="containerClientsSA">
                    
                </div>
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label for="reports_received" class="input-group-text">Credit Report</label>
                    </div>
                    <input type="text" name="enrollment_reports_received" class="form-control date-inputmask">
                    <div class="input-group-append">
                        <a href="#" class="btn_add_cr input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                    </div>
                </div>
                <div class="containerClientsCR">
                    
                </div>
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label for="date_of_cancellation" class="input-group-text">Cancelled</label>
                    </div>
                    <input type="text" name="enrollment_date_of_cancellation" class="form-control date-inputmask">
                </div>

                    
                <div class="crc_container">
                    
                </div>
                <script>
                    $('.crc_container').on('click','.btn_add_crc', function(event) {
                        event.preventDefault();
                        var crc_container = $('.crc_container');
                        var new_crc = '<section tbl="client_credit_report_credentials" crc_id="">\
                                        <div class="input-group " >\
                                            <div class="input-group-prepend">\
                                                <label for="" class="input-group-text p-0 b-0">\
                                                    <select name="" class="form-control" field="cr_type">\
                                                        <option value="">Select Credentials</option>\
                                                        <option value="MyFico">MyFico</option>\
                                                        <option value="CreditKarma">CreditKarma</option>\
                                                        <option value="Experian">Experian</option>\
                                                        <option value="IdentityIQ">IdentityIQ</option>\
                                                        <option value="Add New">Add New</option>\
                                                        \
                                                    </select>\
                                                </label>\
                                            </div>\
                                            <input type="text" name="" class="form-control" placeholder="Username"  field="username">\
                                            <div class="input-group-append">\
                                                <a href="#" class="btn_delete_crc input-group-text text-danger"><i class="fas fa-trash"></i></a>\
                                            </div>\
                                        </div>  \
                                        <div class="input-group" tbl="credit_report_credentials" crc_id="">\
                                            <div class="input-group-prepend" >\
                                                <label for="" class="input-group-text" style="background-color: white;border-color: white"></label>\
                                            </div>\
                                            <input type="text" name="" class="form-control" placeholder="Password" field="password">\
                                        </div>  \
                                    </section>';
                        crc_container.append(new_crc);
                    });
                    $('.crc_container').on('click','.btn_delete_crc', function(event) {
                        event.preventDefault();
                        var section = $(this).closest('section');
                        var crc_id = $(this).closest('section').attr('crc_id'); 
                        var data =  {
                                        table: 'client_credit_report_credentials',
                                        pk: crc_id,
                                        action: 'delete'
                                    }; 
                        $.post('clients/modelTable', data, function(data, textStatus, xhr) { 
                            
                        });

                        section.remove();
                    });
                </script>
                <!-- <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label for="fax" class="input-group-text">Fax</label>
                    </div>
                    <input type="text" name="profile_fax" id="profile_fax" class="form-control" >
                </div> -->


            </div>
            <div class="col-md-4 col-12 sectionEnrollmentInformation"> 
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label style="min-width: 92px !important;" for="address_verification_received" class="input-group-text">Address 1</label>
                    </div>
                    <input type="text" name="enrollment_address_verification_received" class="form-control date-inputmask">
                    <div class="input-group-append">
                        <a href="#" class="btn_add_a1 input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                    </div>
                </div>
                <div class="container_clients_a1">
                    
                </div>
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label style="min-width: 92px !important;" for="address_verification_received" class="input-group-text">Address 2</label>
                    </div>
                    <input type="text" name="enrollment_address_verification_received_2" class="form-control date-inputmask">
                    <div class="input-group-append">
                        <a href="#" class="btn_add_a2 input-group-text text-success"><i class="fas fa-plus-circle"></i></a>
                    </div>
                </div>
                <div class="container_clients_a2">
                    
                </div>
                <div class="input-group m-b-5">
                    <div class="input-group-prepend">
                        <label style="min-width: 92px !important;" for="ss_proof_received" class="input-group-text">Social</label>
                    </div>
                    <input type="text" name="enrollment_ss_proof_received" class="form-control date-inputmask">
                </div>
            </div>
            <div class="col-12">
                <table id="tableClientDocuments" class=" table stylish-table" width="100%">
                    <thead>
                        <tr>
                            <th><a href="#" class="btn_open_documents"><i class="fas fa-eye fa-lg"></i></a></th>
                            <th>Category</th>
                            <th>File Size</th>
                            <th>Date Uploaded</th>
                            <th style="width: 42px !important">Tools</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>