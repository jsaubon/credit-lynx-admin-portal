<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php');
    $this->load->view('admin/clients/style');
?>


<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css') ?>">

<script src="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.js') ?>"></script> 
<script src="<?php echo base_url('assets/plugins/echarts/echarts-all.js') ?>"></script> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/summernote/summernote.css') ?>">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/summernote/summernote-bs3.css') ?>"> -->
<script src="<?php echo base_url('assets/js/summernote/summernote.min.js') ?>"></script> 
<script src="<?php echo base_url('assets/js/summernote/summernote-ext-print.js') ?>"></script>
<script src="<?php echo base_url('assets/js/summernote/summernote-pagebreak.js') ?>"></script> 

            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"  style="color: #33cccc!important;"><a href="<?php echo $page_controller ?>">Home</a>
                            <a id="btnSearchArrowLeft" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-left"></i></a>
                            <a id="btnSearchArrowRight" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-right"></i></a>
                             
                        </h2>
                        
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>"></a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 text-right">
                        <input type="text" name="" class="form-control" placeholder="search client" id="searchInputClient" style="width: 70%">
                        <script>
                            
                            getSearchClient();
                            function getSearchClient() {
                                var clients = [];
                                var users = '<?php echo addslashes(json_encode($all_clients)) ?>'; 
                                users = JSON.parse(users);
                                $.each(users,function(key,value){
                                    var client_name = value.name;
                                    var client_type = value.client_type;   

                                    var newOption = {
                                                    value: value.client_id+'_client',
                                                    label: client_name,
                                                    desc: value.cell_phone+ ' ' + value.email_address + ' ' +value.ss_restricted+'-xxxx', 
                                                  };
                                    clients.push(newOption);

                                    if (client_type == 'Joint') {  
                                        var newOption = {
                                                    value: value.client_joint_id+'_joint_'+value.client_id,
                                                    label: value.joint_name,
                                                    desc: value.joint_cell_phone+ ' ' + value.joint_email_address + ' ' +value.ss_restricted+'-xxxx', 
                                                  };
                                        clients.push(newOption);
                                    }
                                    
                                    
                                }); 
 
 

                                $( "#searchInputClient" ).autocomplete({
                                  minLength: 2,
                                  source: clients,
                                  focus: function( event, ui ) {
                                    $( "#searchInputClient" ).val( ui.item.label );
                                    return false;
                                  },
                                  select: function( event, ui ) {
                                    // $( "#searchInputClient" ).val( ui.item.label );  
                                    $('#searchClient').val(ui.item.value);
                                    $('#searchClient').trigger('change');
                                    if ($('#profileSection').hasClass('hide')) { 
                                        $('#profileSection').removeClass('hide');
                                        $('#listSection').addClass('hide');
                                        $('#profile_active').removeClass('hide');
                                        $('#profile_active').html(ui.item.label);
                                    }




                                    // $( "#project-id" ).val( ui.item.value );
                                    // $( "#project-description" ).html( ui.item.desc );
                                    // $( "#project-icon" ).attr( "src", "images/" + ui.item.icon );
                             
                                    return false;
                                  }
                                })
                                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                                  return $( "<li>" )
                                    .append( "<div>" + item.label + "<br>" + item.desc + "</div>" )
                                    .appendTo( ul );
                                };
                            }
                            
                         
                                
                        </script>
                        <select  id="searchClient" class="hide"> 
                            <option value="">select client</option>s
                            <?php foreach ($all_clients as $key => $value): ?>

                                <option value="<?php echo $value['client_id'].'_client' ?>"><?php echo $value['name'] ?></option>
                                <?php if ($value['client_type'] == 'Joint'): ?>
                                    <option value="<?php echo $value['client_joint_id'].'_joint_'.$value['client_id'] ?>"><?php echo $value['joint_name'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php $this->load->view('admin/clients/ListSectionTest') ?>

                <div id="profileSection" class="row hide"> 
                    <!-- Column -->
                    <div class="col-lg-9 col-xlg-9 col-md-9 col-sm-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul id="containerMainTabs" class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item" style="margin: auto"> 
                                    <a class="nav-link active show" data-toggle="tab" href="#tab_profile" role="tab" aria-selected="true">
                                        Profile
                                    </a> 
                                </li>
                                <li class="nav-item" style="margin: auto"> 
                                    <a id="tab_documents-header" class="nav-link " data-toggle="tab" href="#tab_documents" role="tab" aria-selected="false">
                                        Documents 
                                    </a> 
                                </li>
                                <?php if ($userdata['login_folder'] == 'admins'): ?>
                                <li class="nav-item" style="margin: auto"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_billing_info" role="tab" aria-selected="false">
                                        Billing 
                                    </a> 
                                </li>
                                <?php endif ?>
                                
                                <li class="nav-item" style="margin: auto"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_gameplan" role="tab" aria-selected="false">
                                        GamePlan 
                                    </a> 
                                </li>
                                <li class="nav-item" style="margin: auto"> 
                                    <a id="tab_support-header" class="nav-link btnBackToSupportTicketList" data-toggle="tab" href="#tab_support" role="tab" aria-selected="false">
                                        Support <span class="text-danger" id="notif_support"></span>
                                    </a> 
                                </li>
                                <li class="nav-item" style="margin: auto"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_results" role="tab" aria-selected="false">
                                        Results
                                    </a> 
                                </li>
                                <li class="nav-item" style="margin: auto"> 
                                    <a class="nav-link" data-toggle="tab" href="#tab_disputes" role="tab" aria-selected="false">
                                        Disputes
                                    </a> 
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">  
                                <?php $data['subscription_type'] = $subscription_type; ?>
                                <?php $this->load->view('admin/clients/tab_profile') ?>
                                <?php $this->load->view('admin/clients/tab_documents') ?>
                                <?php $this->load->view('admin/clients/tab_billing_info', $data) ?>
                                <?php $this->load->view('admin/clients/tab_gameplan') ?>
                                <?php $this->load->view('admin/clients/tab_support') ?>
                                <?php $this->load->view('admin/clients/tab_results') ?>
                                <?php $this->load->view('admin/clients/tab_disputes') ?> 
                            </div>
                        </div>
                        <div class="card" id="containerEmailTextTabs">
                            <div class="card-body" style="padding: 0px">
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                            <li class="nav-item"> 
                                                <a id="notes-tab-header" class="nav-link active show" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> 
                                            </li>
                                            <li class="nav-item" > 
                                                <a id="tab_emails-header" class="nav-link " data-toggle="tab" href="#tab_emails" role="tab" aria-selected="true">
                                                    Emails
                                                </a> 
                                            </li>
                                            <li class="nav-item" > 
                                                <a class="nav-link" data-toggle="tab" href="#tab_texts" role="tab" aria-selected="false">
                                                    Text 
                                                </a> 
                                            </li> 
                                            <li class="nav-item" > 
                                                <a class="nav-link" data-toggle="tab" href="#tab_call_logs" role="tab" aria-selected="false">
                                                    Call Logs 
                                                </a> 
                                            </li> 

                                        </ul>
                                        <div class="tab-content">

                                        <?php $this->load->view('admin/clients/tab_notes') ?>
                                        <?php $this->load->view('admin/clients/tab_emails') ?>
                                        <?php $this->load->view('admin/clients/tab_texts') ?>
                                        <?php $this->load->view('admin/clients/tab_call_logs') ?>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <?php $this->load->view('admin/clients/RightSideDiv') ?>
                    <!-- Column --> 
                    
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<?php $this->load->view('admin/clients/Modals') ?>
<?php $this->load->view('admin/clients/script') ?>

<script>
    $('.left_nav_clients').addClass('active');
</script> 
<?php   $this->load->view('admin/includes/footer.php');?>