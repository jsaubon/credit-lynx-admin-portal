<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 

<style>
	.timeline-title input {
		border: none !important;
		font-weight: 400;
		color: inherit;
		font-size: 18px;
		padding: 0px !important;
	}

	.st_note {
		border: none !important; 
		padding: 0px !important;
	}

	hr {
		border: inset !important;
		border-top: 1px solid rgba(0,0,0,.1) !important;

	}

	.timeline:before {
		left: 5%;
	}

	.timeline>li>.timeline-badge {
		left: 5%;
	}

	.timeline>li>.timeline-panel {
		width: 88%;
	}
</style>
<div class="container-fluid p-t-20">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-12 col-lg-6">
							<h4>Lead</h4>
							<select class="form-control select_status" status_type="lead">
								<option>Choose a Lead Status</option>
								<?php foreach ($lead_statuses as $key => $value): ?>
									<option value="<?php echo $value['lead_status'] ?>"><?php echo $value['lead_status'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-12 col-lg-6">
							<h4>Client</h4>
							<select class="form-control select_status" status_type="client">
								<option>Choose a Client Status</option> 
								<?php foreach ($client_statuses as $key => $value): ?>
									<option value="<?php echo $value['client_status'] ?>"><?php echo $value['client_status'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="card hide animated fadeIn" id="container_timeline">
				<div class="card-body" style="padding-bottom: 60px;">
					<h3>Status Timeline for <u id="span_status"></u></h3> 
					<ul class="timeline">
                         
                        <li class="">
                            <div class="timeline-badge success"><a href="#" id="btn_add_st"><i class="fas fa-plus" style="color: white"></i></a></div> 
                        </li>   
                    </ul>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var status = '';
	var status_type = '';
	$('.select_status').on('change', function(event) {
		event.preventDefault();
		status = $(this).val();
		status_type = $(this).attr('status_type');

		$('#span_status').html(status);
		$('#container_timeline').removeClass('hide');

		get_client_status_timeline_default(status,status_type);
	});
	$('.timeline').on('click', '#btn_add_st',function(event) {
		event.preventDefault();
 		var day = $('.timeline li').length;
		add_client_status_timeline_default(status,status_type,day);
	});


	$('.timeline').on('click', 'button', function(event) {
		event.preventDefault();
		var st_id = $(this).closest('li').attr('st_id');
		var value = $(this).attr('notif_type');
		var buttons = $('.timeline button'); 
		if ($(this).hasClass('btn-success')) {  
			$(this).addClass('btn-default');
			$(this).removeClass('btn-success'); 
			$(this).find('.fa-check').remove();


			$(this).closest('.timeline-panel').find('.timeline-heading').addClass('hide');
			$(this).closest('.timeline-panel').find('.timeline-body').addClass('hide');
			update_client_status_timeline_default(st_id,'notif_type','');
		} else { 
			buttons.removeClass('btn-success');
			buttons.addClass('btn-default');
			buttons.find('.fa-check').remove();

			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			$(this).append('<i class="fas fa-check"></i>');
			$(this).closest('.timeline-panel').find('.timeline-heading').removeClass('hide');
			$(this).closest('.timeline-panel').find('.timeline-body').removeClass('hide');

			update_client_status_timeline_default(st_id,'notif_type',value);
		}
		
	});

	$('.timeline').on('change', '.st_note', function(event) {
		event.preventDefault(); 
		var st_id = $(this).closest('li').attr('st_id');
		var value = $(this).val();
		update_client_status_timeline_default(st_id,'note',value);
	});
	$('.timeline').on('change', '.st_subject', function(event) {
		event.preventDefault(); 
		var st_id = $(this).closest('li').attr('st_id');
		var value = $(this).val();
		update_client_status_timeline_default(st_id,'subject',value);
	});
 
	function get_client_status_timeline_default(status,status_type) {
		var data =  {
		                table: 'client_status_timeline_default',
		                action: 'get',
		                select: {
		                            
		                        },
		                where:  {
									'status':status,
									'status_type':status_type
		                        },
		                field: '',
		                order: '',
		                limit: 0,
		                offset: 0,
		                group_by: ''
		
		            };
		$.post('communication_flow/modelTable', data, function(data, textStatus, xhr) {
			// console.log(data);
			data = JSON.parse(data);
			var timeline = $('.timeline');
			timeline.empty();
			timeline.append('<li class="">\
		                        <div class="timeline-badge success"><a href="#" id="btn_add_st"><i class="fas fa-plus" style="color: white"></i></a></div> \
		                    	</li>');
			if (data.length == 0) { 
				add_client_status_timeline_default(status,status_type,1);
			} else {
				data = data.reverse();
				$.each(data, function(index, val) {
					var hide_body = val.notif_type == '' ? 'hide' : '';
					var li_class = val.day == 1 ? '' : (val.day) % 2 == 0  ? 'timeline-inverted' : 'timeline-inverted';
					var animated_class = val.day == 1 ? 'bounceInLeft' : (val.day) % 2 == 0  ? 'bounceInRight' : 'bounceInLeft';
					var footer_class = (val.day) % 2 == 0  ? 'text-right' : '';
					 
					$('.timeline').prepend('<li class="timeline-inverted" st_id="'+val.st_id+'">\
		                            <div class="timeline-badge success">'+val.day+'</div>\
		                            <div class="timeline-panel animated bounceInRight">\
		                                <div class="timeline-heading '+hide_body+'">\
		                                    <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject" value="'+val.subject+'"></h4>\
		                                </div>\
		                                <div class="timeline-body '+hide_body+'">\
		                                    <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note">'+val.note+'</textarea>\
		                                </div>\
		                                <div class="timeline-footer '+footer_class+'">\
		                                	<hr>  \
		                                        <button type="button" class="btn btn-default btn-sm " notif_type="sms"> \
		                                        	<i class="fas fa-comments"></i>\
		                                        	Sms\
		                                        </button>\
		                                        <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
		                                        	<i class="fas fa-envelope"></i>\
		                                        	Email\
		                                        </button> \
		                                        <button type="button" class="btn btn-default btn-sm " notif_type="both"> \
		                                        	<i class="fa fa-reply-all"></i>\
		                                        	Both\
		                                        </button> \
		                                </div>\
		                            </div>\
		                        </li>');
					var notif_type_button = $('.timeline').find('[st_id="'+val.st_id+'"]').find('[notif_type="'+val.notif_type+'"]');
					notif_type_button.removeClass('btn-default');
					notif_type_button.addClass('btn-success');
					notif_type_button.append('<i class="fas fa-check"></i>');
				});
			}
		});
	}

	function add_client_status_timeline_default(status,status_type,day) {
		var data =  {
		                table: 'client_status_timeline_default',
		                pk: '',
		                action: 'save',
		                fields: {
		                            "status":status,
		                            'status_type':status_type,
		                            'day':day
		                        },
		                return: 'st_id'
		            };
		
		$.post('communication_flow/modelTable', data , function(data, textStatus, xhr) {  
			// console.log(data);
			var li_class = day == 1 ? '' : (day) % 2 == 0  ? 'timeline-inverted' : 'timeline-inverted';
			var animated_class = day == 1 ? 'bounceInLeft' : (day) % 2 == 0  ? 'bounceInRight' : 'bounceInLeft';
			var footer_class = (day) % 2 == 0  ? 'text-right' : '';
			var last_child = 2;
			if (day == 1) {
				$('.timeline').prepend('<li class="timeline-inverted" st_id='+data+'>\
			                            <div class="timeline-badge success">'+day+'</div>\
			                            <div class="timeline-panel animated bounceInRight">\
			                                <div class="timeline-heading hide">\
			                                    <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject"></h4>\
			                                </div>\
			                                <div class="timeline-body hide">\
			                                    <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note"></textarea>\
			                                </div>\
			                                <div class="timeline-footer '+footer_class+'">\
			                                	<hr>  \
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="sms"> \
			                                        	<i class="fas fa-comments"></i>\
			                                        	Sms\
			                                        </button>\
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
			                                        	<i class="fas fa-envelope"></i>\
			                                        	Email\
			                                        </button> \
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="both"> \
			                                        	<i class="fa fa-reply-all"></i>\
			                                        	Both\
			                                        </button> \
			                                </div>\
			                            </div>\
			                        </li>   ');
			} else {
				$('<li class="timeline-inverted" st_id='+data+'>\
			                            <div class="timeline-badge success">'+day+'</div>\
			                            <div class="timeline-panel animated '+animated_class+'">\
			                                <div class="timeline-heading hide">\
			                                    <h4 class="timeline-title"><input type="text" name="" class="form-control st_subject" placeholder="Subject"></h4>\
			                                </div>\
			                                <div class="timeline-body hide">\
			                                    <textarea rows="5" placeholder="Click here to edit ..." class="form-control st_note"></textarea>\
			                                </div>\
			                                <div class="timeline-footer '+footer_class+'">\
			                                	<hr>  \
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="sms"> \
			                                        	<i class="fas fa-comments"></i>\
			                                        	Sms\
			                                        </button>\
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="email"> \
			                                        	<i class="fas fa-envelope"></i>\
			                                        	Email\
			                                        </button> \
			                                        <button type="button" class="btn btn-default btn-sm" notif_type="both"> \
			                                        	<i class="fa fa-reply-all"></i>\
			                                        	Both\
			                                        </button> \
			                                </div>\
			                            </div>\
			                        </li>   ').insertAfter($('.timeline li:nth-last-child('+last_child+')'));
			}
				
		});
	}

	function update_client_status_timeline_default(st_id,field,value) {
		var data =  {
		                table: 'client_status_timeline_default',
		                pk: st_id,
		                action: 'save',
		                fields: {
		                            [field]:value
		                        },
		                return: ''
		            };
		$.post('communication_flow/modelTable', data , function(data, textStatus, xhr) { 
		
		});
	}
</script>
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 