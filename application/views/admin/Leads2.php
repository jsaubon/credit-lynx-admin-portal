<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
$this->load->view('admin/clients/style');
?>

<style>
    .dataTables_wrapper {
        padding-top: 0px
    }
    .leads_table {
        margin-top: 0px !important;
    }
</style>

            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">
                            <!-- <a id="btnSearchArrowLeft" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-left"></i></a> -->
                            <a href="leads2">Leads</a>
                            <!-- <a id="btnSearchArrowRight" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-right"></i></a> -->

                        </h2>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>"></a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <?php foreach ($lead_cards['lead_statuses'] as $key => $status): ?>
                        <div class="col-12 col-xlg-3 col-lg-6">
                            <div class="card">
                                <div class="card-body bg-success">
                                    <h4 class="card-title m-b-0" > 
                                        <span class="lead_status" style="color: white"><?php echo $status ?></span> 
                                        <a href="#" class="btn_bulk_sms"><i class="fas fa-envelope " style="color: white"></i></a>
                                        <a href="#" class="btn_export"><i class="mdi mdi-file-excel " style="color: white"></i></a>
                                        <?php
                                            $status_count = 0;
                                            foreach ($lead_cards['data_cards'] as $key => $value) {
                                                if ($status == $value['lead_status']) {
                                                    if ($value['joint_name'] == '') {
                                                        $status_count++;
                                                    } else { 
                                                        $status_count = $status_count + 2;
                                                    }
                                                }
                                            }
                                        ?>
                                        <span class="pull-right" style="color: white"><?php echo $status_count ?></span>
                                    </h4>
                                </div>
                                <div class="card-body" style="padding: 0px"> 
                                    
                                    <div class="table-responsive" style="max-height: 400px;min-height: 400px">
                                        <table class="table stylish-table leads_table" style="margin-top: 0px !important">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <?php foreach ($lead_cards['data_cards'] as $index => $value): ?>
                                                    <?php if ($value['lead_status'] == $status): ?> 
                                                        <?php
                                                            $email_address = $value['email_address'] == '' ? 'No Email' : $value['email_address'];
                                                            $joint_email_address = $value['joint_email_address'] == '' ? 'No Email' : $value['joint_email_address'];
                                                        ?>
                                                        <tr id="<?php echo $value['client_id'] ?>">
                                                            <td style="">
                                                                <div style="line-height: 1.4" class="my_draggable">
                                                                    <?php if ($value['client_type'] != 'Joint'): ?>
                                                                        <a href="#" class="goToProfile lead_name" style="display: block;"><u><?php echo stripslashes($value['name']) ?></u></a>
                                                                         
                                                                    <?php endif ?>
                                                                    <?php if ($value['client_type'] == 'Joint'): ?>
                                                                        <a href="#" class="goToProfile lead_name" style="display: block;"><u><?php echo stripslashes($value['name']) ?></u></a> 
                                                                        <a href="#" value="<?php echo $value['client_joint_id'] ?>" class="goToJointProfile joint_name" style="display: block;"><u><?php echo stripslashes($value['joint_name']) ?></u></a> 
                                                                    <?php endif ?>
                                                                        
                                                                </div>
                                                            </td>  
                                                            <td> 
                                                                    <?php if ($value['client_type'] != 'Joint'): ?> 
                                                                        <span style="word-break: break-word" class="lead_email" ><?php echo $email_address ?></span> 
                                                                    <?php endif ?>
                                                                    <?php if ($value['client_type'] == 'Joint'): ?> 
                                                                        <span style="word-break: break-word" class="lead_email" ><?php echo $email_address ?></span>
                                                                        <br> 
                                                                        <span style="word-break: break-word" class="joint_email" ><?php echo $joint_email_address ?></span> 
                                                                    <?php endif ?>
                                                            </td>
                                                            <td> 
                                                                    <?php if ($value['client_type'] != 'Joint'): ?> 
                                                                        <span style="word-break: break-word" class="lead_phone"><?php echo $value['cell_phone'] ?></span>
                                                                    <?php endif ?>
                                                                    <?php if ($value['client_type'] == 'Joint'): ?> 
                                                                        <span style="word-break: break-word" class="lead_phone"><?php echo $value['cell_phone'] ?></span> 
                                                                        <br>
                                                                        <span style="word-break: break-word" class="joint_phone"><?php echo $value['joint_cell_phone'] ?></span>
                                                                    <?php endif ?>
                                                            </td>
                                                            <td>
                                                               <!--  <button class="btn btn-danger btn_delete_lead"><i class="fas fa-times"></i></button> -->
                                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item btn_delete_lead" href="#">Delete</a> 
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                                          
                                            </tbody>
                                        </table>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                            
                    <!-- <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body" >
                                leads
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body" >
                                leads
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body" >
                                leads
                            </div>
                        </div>
                    </div> -->
                </div>



                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script>
    $('.leads_table').DataTable({ 
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 200,
        'bFilter': false
    });
    //                                 );
    $('.left_nav_leads').addClass('active');

    $('.right-side-toggle').on('click', function(event) {
        $('.right-sidebar').removeClass('right-sidebar-fullscreen');
        /* Act on the event */
    });

    $('.btn_bulk_sms').on('click', function(event) {
        event.preventDefault();
        var send_bulk_sms_to = $('.send_bulk_sms_to');
        $('.to_count').html('0');
        send_bulk_sms_to.empty();

        var table_tr = $(this).closest('.card').find('table tbody tr');
        var to =    [ 
                        
                    ];
        $.each(table_tr, function(index, tr) {
            if (($(tr).find('.goToJointProfile')).length == 0) {
                to.push(    {
                                'name': $(tr).find('.goToProfile').find('u').html(),
                                'phoneNumber': $(tr).find('.lead_phone').html(),
                                'id': $(tr).attr('id'),
                                'type': 'client'
                            }
                        );
            } else {
                to.push(    {
                                'name': $(tr).find('.goToProfile').find('u').html(),
                                'phoneNumber': $(tr).find('.lead_phone').html(),
                                'id': $(tr).attr('id'),
                                'type': 'client'
                            }
                        );
                to.push(    {
                                'name': $(tr).find('td:nth-child(1)').find('.goToJointProfile').find('u').html(),
                                'phoneNumber': $(tr).find('.lead_joint_phone').html(),
                                'id': $(tr).find('td:nth-child(1)').find('.goToJointProfile').attr('value'),
                                'type': 'joint'
                            }
                        );
            }
            
        });

        $.each(to, function(index, val) {
            if (val.phoneNumber != '') {
                send_bulk_sms_to.append('<option selected="" id="'+val.id+'" type="'+val.type+'" value="'+trim_phone(val.phoneNumber)+'">'+val.name+' '+val.phoneNumber+'</option>');
            }
                
        });
        $('.to_count').html(to.length);
 
        $('#modal_send_bulk_sms').modal('show');
    });

    $('.leads_table').on('click', '.btn_delete_lead', function(event) {
        event.preventDefault();
        var client_id = $(this).closest('tr').attr('id');
        var name1 = $(this).closest('tr').find('.goToProfile').find('u').html();
        var name2 = $(this).closest('tr').find('.goToJointProfile').find('u').html();
        var names = name1;
        if (!$.isEmptyObject(name2)) {
            names = name1 + ' and ' + name2;
        }
        var tr = $(this).closest('tr');

        swal({
                title: "Delete Confirmation",
                text: 'Are you sure you want to delete '+names+'?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                closeOnConfirm: true ,
                preConfirm: function() {  
                    var data =  {
                                    table: 'clients',
                                    pk: client_id,
                                    action: 'delete'
                                }; 
                    $.post('clients/modelTable', data, function(data, textStatus, xhr) { 
                        tr.remove();
                    });
                }
            });
    });

    function trim_phone(str) {
        str = str.replace(/ /g,'');
        str = str.replace('(','');
        str = str.replace(')','');
        str = str.replace(/-/g,'');

        return str;
    }


    $('.btn_export').on('click', function(event) {
        event.preventDefault();
        var lead_status = $(this).closest('.card-title').find('.lead_status').html();
        var leads_table = $(this).closest('.card').find('.leads_table');
        var data_ = [['Name','Email','Phone']];
        $.each(leads_table.find('tbody').find('tr'), function(index, tr) {
            
            var lead_name = $(tr).find('.lead_name').find('u').html();
            var lead_email = $(tr).find('.lead_email').html();
            var lead_phone = $(tr).find('.lead_phone').html(); 

            data_.push([
                    lead_name,
                    lead_email,
                    lead_phone
            ]);

            if (!$.isEmptyObject($(tr).find('.joint_name').find('u').html())) {
                var joint_name = $(tr).find('.joint_name').find('u').html();
                var joint_email = $(tr).find('.joint_email').html();
                var joint_phone = $(tr).find('.joint_phone').html(); 

                data_.push([
                        joint_name,
                        joint_email,
                        joint_phone
                ]);
            }

        });


        console.log(data_);
        var csvGenerator = new CsvGenerator(data_,'Leads '+lead_status+'.csv');
        csvGenerator.download(true);
    });
 
    function CsvGenerator(dataArray, fileName, separator, addQuotes) {
        this.dataArray = dataArray;
        this.fileName = fileName;
        this.separator = separator || ',';
        this.addQuotes = !!addQuotes;

        if (this.addQuotes) {
            this.separator = '"' + this.separator + '"';
        }

        this.getDownloadLink = function () {
            var separator = this.separator;
            var addQuotes = this.addQuotes;

            var rows = this.dataArray.map(function (row) {
                var rowData = row.join(separator);

                if (rowData.length && addQuotes) {
                    return '"' + rowData + '"';
                }

                return rowData;
            });

            var type = 'data:text/csv;charset=utf-8';
            var data = rows.join('\n');

            if (typeof btoa === 'function') {
                type += ';base64';
                data = btoa(data);
            } else {
                data = encodeURIComponent(data);
            }

            return this.downloadLink = this.downloadLink || type + ',' + data;
        };

        this.getLinkElement = function (linkText) {
            var downloadLink = this.getDownloadLink();
            var fileName = this.fileName;
            this.linkElement = this.linkElement || (function() {
                var a = document.createElement('a');
                a.innerHTML = linkText || '';
                a.href = downloadLink;
                a.download = fileName;
                return a;
            }());
            return this.linkElement;
        };

        // call with removeAfterDownload = true if you want the link to be removed after downloading
        this.download = function (removeAfterDownload) {
            var linkElement = this.getLinkElement();
            linkElement.style.display = 'none';
            document.body.appendChild(linkElement);
            linkElement.click();
            if (removeAfterDownload) {
                document.body.removeChild(linkElement);
            }
        };
    }
    
</script>
<?php $this->load->view('admin/includes/footer.php');?>
