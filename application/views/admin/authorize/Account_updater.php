<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    #pageTable tbody tr td:nth-child(8) {
        text-align: center;;
    }

    #pageTable tbody tr td:nth-child(7) {
        text-align: right;;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Account Updater</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div id="containerList" class="card-body"> 
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <select class="form-control select_month">
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>

                                    <div class="col-12 col-lg-4">
                                        <select class="form-control select_year">
                                            <?php for ($i=date('Y') - 10; $i < date('Y'); $i++) { ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php }?>
                                            <?php for ($i=date('Y'); $i < date('Y') + 10; $i++) { ?>
                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <button class="btn btn-block btn-success btn_get_account_updater">Get Account Updater</button>
                                    </div>
                                </div>
                                        
                                
                                <div class="table-responsive"> 
                                    <table class="table stylish-table m-t-20" id="pageTable">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th> 
                                                <th>Reason Code</th>
                                                <th>Reason Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>  
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script>
$(document).ready(function(){ 
    $('.select_year').val('<?php echo date('Y') ?>');
    $('.select_month').val('<?php echo date('m') ?>');
    get_account_updater();
    $('.btn_get_account_updater').on('click', function(event) {
        event.preventDefault();
        get_account_updater();        
    });

    function get_account_updater() {
        var month = $('.select_month').val();
        var year = $('.select_year').val();
        var year_month = year+'-'+month;
        $.post('account_updater/getAccountUpdaterJobDetails', {year_month}, function(data, textStatus, xhr) {
            $('#pageTable tbody').empty();
            data = JSON.parse(data);
            console.log(data);
            if (data.length > 0) {
                $.each(data, function(index, val) {
                    var new_tr = '<tr>\
                                    <td>'+val['full_name']+'</td>\
                                    <td>'+val['reason_code']+'</td>\
                                    <td>'+val['reason_description']+'</td>\
                                </tr>';
                    $('#pageTable tbody').append(new_tr);
                });
            } else {
                $('#pageTable tbody').append('<tr><td colspan="3" class="text-center">No Account Updater Details for this month.</td></tr>')
            }
                
        });
    }
});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 