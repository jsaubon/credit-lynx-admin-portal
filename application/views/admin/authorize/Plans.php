<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    #pageTable tbody tr td:nth-child(8) {
        text-align: center;;
    }

    #pageTable tbody tr td:nth-child(7) {
        text-align: right;;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Subscriptions Types</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div id="containerList" class="card-body">
                                <div class="table-responsive">
                                    <a style="margin-top: 14px" href="#" class="btn btn-success waves-effect waves-light pull-right" id="btnNewUser" data-toggle="modal" data-target="#edit-subscription-type-modal">New Subscription Type</a>
                                    <table class="table stylish-table" id="pageTable">
                                        <thead>
                                            <tr>
                                                <th>Plan</th>
                                                <th>Setup Fee</th>
                                                <th>Monthy Fee</th>
                                                <th>Description</th>
                                                <th>Tools</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<div id="edit-subscription-type-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;width: 100%">
    <div class="modal-dialog  modal-md" >
        <form method="POST" id="subscriptionsFormEdit">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- TITLE -->
                    <h3>Subscription Type Form</h3>  
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" >   
                    <input name="s_subscription_id" type="hidden">
                    <div class="rightLabelDiv">
                        <label class="rightLabel">Plan</label>
                        <input style="border-radius: 0;"  type="text" name="s_name" class="form-control" placeholder="Plan">
                    </div>  
                    <div class="rightLabelDiv">
                        <label class="rightLabel">Setup Fee</label>
                        <input style="border-radius: 0;"  type="text" name="s_setup_fee" class="form-control" placeholder="Setup Fee">
                    </div>     
                    <div class="rightLabelDiv">
                        <label class="rightLabel">Monthly Fee</label>
                        <input style="border-radius: 0;"  type="text" name="s_monthly_fee" class="form-control" placeholder="Monthly Fee">
                    </div>   
                    <div class="rightLabelDiv">
                        <label class="rightLabel">Description</label>
                        <textarea style="border-radius: 0;"  type="text" name="s_description" class="form-control"></textarea> 
                    </div>        
                </div> 
                <div class="modal-footer">
                    <button type="submit" id="subscriptionsSubmit" class="btn btn-success waves-effect" >Submit</button> 
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                </div>
            </div>
        </form>
    </div>
</div>
<script>
$(document).ready(function(){
    subscriptionsTypes();

    $('#btnNewUser').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('#subscriptionsFormEdit')[0].reset();
    });

    $('#btnBackToList').on('click',function(e){
        e.preventDefault();
        $('#containerList').removeClass('hide');
        $('#containerDetail').addClass('hide');
    });

    var pageTable = $('#pageTable').DataTable({ 
        'aaSorting': [],  
        dom: 'Bfrtip',
        "pageLength": 20 
    });
 

    $('#subscriptionsFormEdit').on('submit',function(e){
        e.preventDefault();  
        var subscription_id = $('#subscriptionsFormEdit').find('[name="s_subscription_id"]').val();
        var name = $('#subscriptionsFormEdit').find('[name="s_name"]').val();
        var setup_fee = $('#subscriptionsFormEdit').find('[name="s_setup_fee"]').val();
        var monthly_fee = $('#subscriptionsFormEdit').find('[name="s_monthly_fee"]').val();
        var description = $('#subscriptionsFormEdit').find('[name="s_description"]').val();
        EditSubs(subscription_id,name,setup_fee,monthly_fee,description);
    });

    function EditSubs(subscription_id,name,setup_fee,monthly_fee,description) {
        $.post('<?php echo base_url()?>/admin/authorize/plans/subscriptionsEdit',
        {subscription_id,name,setup_fee,monthly_fee,description},function(data){ 
            subscriptionsTypes();
            $('#subscriptionsFormEdit')[0].reset();
            $('#edit-subscription-type-modal').modal('hide');
        });
    }
    

    $('#pageTable').removeClass('dataTables');
    function subscriptionsTypes() {
        $.post('<?php echo base_url('admin/authorize/plans/subscriptionsTypes') ?>/', function(data){
            data = JSON.parse(data);
            console.log(data);
            pageTable.clear().draw();
            $.each(data,function(key,value){
                var td1 = value.name;
                var td2 = value.setup_fee;
                var td3 = value.monthly_fee;
                var td4 = value.description;
                var td5 = '\
                <button class="btn btn-sm btn-info subscriptetionEdit" subscription_id="'+value.subscription_id+'" data-toggle="modal" data-target="#edit-subscription-type-modal"><i class="fas fa-edit"></i> Edit</button>\
                <button subscription_id="'+value.subscription_id+'" class="subscriptetionDelete btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete</button>';
                var newRow = pageTable.row.add([td1, td2, td3, td4, td5]);
                pageTable.draw(false);

            });
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
    }

    $("#pageTable").on('click','.subscriptetionEdit',function(){
        var subscription_id = $(this).attr('subscription_id');
        $.post('<?php echo base_url();?>admin/authorize/plans/getSubscriptionId', {subscription_id}, function(data){
            data = JSON.parse(data);
            $.each(data,function(key, value){
                $.each(value,function(k,v){
                    $('#subscriptionsFormEdit').find('[name="s_'+k+'"]').val(v);
                });
            });
            console.log(data);
        }).fail(function(xhr){
            console.log(xhr.responseText);
        });
        console.log(subscription_id);
    });

    $('#pageTable').on('click','.subscriptetionDelete', function(){
        var subscription_id = $(this).attr('subscription_id');
        console.log(subscription_id);
        swal({   
            title: "Are you sure you want to delete this Subscription?",   
            text: 'please review the Information!',   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes!",   
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo base_url('admin/authorize/plans/subscriptionsDelete') ?>',{subscription_id}, function(data){
                    // data = JSON.parse(data);
                    subscriptionsTypes();
                    console.log(data);
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                }); 
            }
        });
    });

    
});
    
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 