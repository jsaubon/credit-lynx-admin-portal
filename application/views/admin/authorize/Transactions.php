<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    .dataTables_filter {
        float: right !important;
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Transactions</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h6 class="card-subtitle">March  2017</h6> -->
                                <div class="table-responsive">
                                    <table class="table stylish-table" id="tbl_transactions">
                                        <thead>
                                            <tr>
                                                <th>Transaction ID</th>
                                                <th>Name</th>
                                                <th>Account Number</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                                <th>Submitted (Local)</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<script>
    $(document).ready(function($) {
        var tbl_transactions;
        var tbl_transactions_data = [{ name: '', value: '' }];
        // $(function() {

        tbl_transactions = $('#tbl_transactions').DataTable({
            'aaSorting': [],  
            dom: 'Bfrtip',
            "pageLength": 50,
            dom: 'Rlfrtip',
            // "aoColumnDefs": [{ "asSorting": [ "desc", "asc" ], "aTargets": [ 8 ] }],
            bProcessing: true,
            bServerSide: true,
            sServerMethod: 'POST',
            sAjaxSource: '<?php echo base_url("admin/authorize/transactions/getTransactions") ?>',
            fnServerParams: function(aoData) { 
                $.each(tbl_transactions_data, function(i, field) {
                    aoData.push({ name: field.name, value: field.value });
                });
            },
            fnDrawCallback: function(data) {
                // console.log(data);
            },createdRow: function( row, data, dataIndex ) {

            }
        });   
    });
</script>          
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 