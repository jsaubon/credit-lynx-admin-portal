<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    #pageTable tbody tr td:nth-child(8) {
        text-align: center;;
    }

    #pageTable tbody tr td:nth-child(7) {
        text-align: right;;
    }

    #pageTable tbody tr td {
        line-height: 15px !important; 
    }


</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0">Subscriptions</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-group">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3>Active</h3>
                                            <h3><span id="status_active"><?php echo $subscriptionCount['active'] ?></span> <small class="card-subtitle">Subscriptions</small></h3> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <!-- Column -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3>Expiring</h3>
                                            <h3><span id="status_expiring"><?php echo $subscriptionCount['expiring'] ?></span> <small class="card-subtitle">Subscriptions</small></h3> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <!-- Column -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3>Inactive</h3>
                                            <h3><span id="status_cancelled"><?php echo $subscriptionCount['cancelled'] ?></span> <small class="card-subtitle">Subscriptions</small></h3> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <!-- Column -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3>Expiring Credit Cards</h3>
                                            <h3><span id="status_expiring_now"><?php echo $subscriptionCount['expiring_now'] ?></span> <small class="card-subtitle">Subscriptions</small></h3> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div id="containerList" class="card-body">

                                <div class="loader-container" style="display: none;" >
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                    </svg>
                                </div>
                                <div class="d-flex no-block">
                                    <h4 class="card-title" id="tableLabel">Active</h4>
                                    <div class="ml-auto">
                                        <select class="custom-select" id="selectSearchType">
                                            <option value="Active" selected="">Active</option>
                                            <option value="Expiring">Expiring</option>
                                            <option value="Cancelled">Inactive</option>
                                            <option value="Expiring Now">Expiring Card</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <h6 class="card-subtitle">March  2017</h6> -->
                                <div class="table-responsive">
                                    <table class="table stylish-table" id="pageTable">
                                        <thead>
                                            <tr>
                                                <!-- <th>Created Date</th> -->
                                                <!-- <th>Subs ID</th> -->
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Customer Name</th>
                                                <th>Payment Method</th>
                                                <th>Card Exp</th>
                                                <th width="1">Payment Amount</th>
                                                <th class="text-center">Payment Count</th>
                                                <th>Last Payment Date</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="containerDetail" class="card-body hide">
                                <div class="row">
                                    <div class="col-12">
                                        <h1>Subscription Detail <small><a id="btnBackToList" class="pull-right" href="#"><i class="fas fa-arrow-left"></i> Back</a></small></h1>
                                        <h3 id="s_name">Premiere Plan</h3>
                                        <h5>Subscription ID: <span id="s_id"></span> <small><a id="btnCancelSubscription" href="#" class="pull-right text-danger">Cancel Subscription</a></small></h5>

                                    </div>
                                    <div class="col-4">
                                        Status: <span id="s_status"></span>
                                    </div>
                                    <div class="col-4">
                                        Completed Payment(s): <span id="s_completed_payments"></span>
                                    </div>
                                    <div class="col-4">
                                        Scheduled Payment(s): <span id="s_total_occurrences">[ongoing]</span>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                        <div class="col-md-6 offset-md-3">
                                            <h3>Payment/Authorization Information</h3>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label style="width: 146.19px" class="input-group-text">Card Number</label>
                                                </div>
                                                <input id="s_cardNumber" type="text" name="" class="form-control">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text">Expiration Date</label>
                                                </div>
                                                <input id="s_expirationDate" type="text" name="" class="form-control">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label style="width: 146.19px" class="input-group-text">Amount</label>
                                                </div>
                                                <input id="s_amount" type="text" name="" class="form-control">
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-12">
                                        <hr>
                                        <h3>Subscription Interval</h3>
                                        <h4>Payments occur on day <span id="s_monthly_due"></span> of every month.</h4>
                                    </div> 
                                    <div class="col-12 text-center">
                                        <button id="btnUpdateSubscription" class="btn btn-success">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
<script>
$(document).ready(function(){
    getTableBySearchType('Active');
    $('#selectSearchType').on('change',function(){
        var searchType = $(this).val();
        $('#tableLabel').html($(this).find('option:selected').text()); 
        getTableBySearchType(searchType);
    });

    $('#btnBackToList').on('click',function(e){
        e.preventDefault();
        $('#containerList').removeClass('hide');
        $('#containerDetail').addClass('hide');
    });

    var pageTable = $('#pageTable').DataTable({ 
        'aaSorting': [],  
        dom: 'Bfrtip',
        "pageLength": 20 
    });


    
    $('#pageTable').removeClass('dataTables');
    function getTableBySearchType(status) {

        $('.loader-container').fadeIn(50);
        $.post('<?php echo base_url('admin/billing/get_client_subscriptions') ?>',{status}, function(data){
                data = JSON.parse(data);
                // console.log(data);
                pageTable.clear().draw();
                $.each(data,function(key,value){
                    // var td1 = moment((value.date_created)['date']).format('MM/DD/YYYY hh:mm A');
                    var td3 = value.plan_name;
                    var td4 = value.status;
                    var td5 = value.full_name;
                    var td6 = value.payment_method ;
                    var td7 = value.card_exp ;
                    var td8 = (value.monthly_fee);
                    var td9 = value.status == 'Active' ? value.past_occurrences + ' of [ongoing]' : value.past_occurrences; 
                    var td10 = value.last_payment_date;  
                    if (td10 != 'no record') {
                        td10 = moment(td10).format('MM/DD/YYYY hh:mm A')
                    }
                    var newRow = pageTable.row.add([td3,td4,td5,td6,td7,td8,td9,td10]); 
                });
                pageTable.draw(false);

            $('.loader-container').fadeOut(50);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

});
    
</script>          
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 