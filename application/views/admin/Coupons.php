<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
	
	.containerAddCoupon label {
		margin-bottom: 0px;
		margin-top: .5rem;
	}
</style>

<div class="container-fluid p-t-20">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					 <h3>Coupons <small class="pull-right"><a href="#" id="btnAddEditCoupon"><i class="fas fa-plus"></i> Add Coupon</small></a></h3>
					 <div class="containerAddCoupon hide animated fadeInLeft">
					 	<form id="formAddEditCoupon" action="coupons/saveDetail" method="POST"> 
						 	<div class="row">
						 		<div class="col-4 offset-4">
						 			<input type="hidden" name="coupon_id">
						 			<h3 class="text-center couponAddEditHeader">New Coupon</h3>
						 			<label for="coupon_code">Coupon Code</label>
						 			<input required type="text" name="coupon_code" class="form-control" >
						 			<div class="row">
						 				<div class="col-6">
						 					<label for="limit">Limit</label>
								 			<input required type="number" name="limit" class="form-control" >
						 				</div>
						 				<div class="col-6">
						 					<label for="limit">Less Amount</label>
								 			<input required type="number" name="amount" class="form-control" >
						 				</div>
						 			</div>
								 			
						 			<div class="row">
						 				<div class="col-6">
						 					 <label for="date_start">Start Date</label>
						 					<input required type="text" name="date_start" class="form-control date-inputmask" >
						 				</div>
						 				<div class="col-6">
						 					 <label for="date_end">End Date</label>
								 			<input required type="text" name="date_end" class="form-control date-inputmask" >
						 				</div>
						 			</div>  
						 			<div class="text-center">
						 				<button  type="submit" class="btn btn-success p-l-20 p-r-20 m-t-10 ">Save</button>
						 			</div>	
						 		</div>
						 	</div>
					 	</form>
					 </div>
					 <hr>
					 <div class="table-responsive">
					 	<table class="table stylish-table">
					 		<thead>
					 			<tr>
					 				<th>Coupon Code</th>
					 				<th>Less Amount</th>
					 				<th>Used</th>
					 				<th>Date</th>  
					 				<th>Tools</th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<?php foreach ($coupons as $key => $coupon): ?>
					 				<tr coupon_id="<?php echo $coupon['coupon_id'] ?>">
					 					<td><?php echo $coupon['coupon_code'] ?></td>
					 					<td><?php echo $coupon['amount'] ?></td>
					 					<td><?php echo empty($coupon['used']) ? 0 : $coupon['used']; echo ' of '.$coupon['limit'] ?></td>
					 					<td><?php echo date('M j, Y',strtotime($coupon['date_start'])) .' - '.date('M j, Y',strtotime($coupon['date_end'])) ?></td> 
					 					<td>
					 						<a href="#" class="btnEditCoupon text-success"><i class="fas fa-edit"></i> Edit</a>
					 						<a href="#" class="btnDeleteCoupon text-danger"><i class="fas fa-trash"></i> Delete</a>
					 					</td>
					 				</tr>
					 			<?php endforeach ?>
					 		</tbody>
					 	</table>
					 </div>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$('#btnAddEditCoupon').on('click',function(e){
		e.preventDefault();
		if ($('.containerAddCoupon').hasClass('hide')) {
			$('.couponAddEditHeader').html('New Coupon');
			$('#formAddEditCoupon')[0].reset();
			$('[name="coupon_id"]').val('');
			$('.containerAddCoupon').removeClass('hide');
		} else { 
			$('.containerAddCoupon').addClass('hide');
			$('#formAddEditCoupon')[0].reset();
			$('[name="coupon_id"]').val('');
		}
			
	});

	$('table').on('click','.btnEditCoupon',function(e){
		e.preventDefault();
		if ($('.containerAddCoupon').hasClass('hide')) {
			$('.couponAddEditHeader').html('Edit Coupon'); 
			$('.containerAddCoupon').removeClass('hide');
			var coupon_id = $(this).closest('tr').attr('coupon_id');
			getDetails(coupon_id);
			function getDetails(coupon_id) {
				$.post('<?php echo $page_controller ?>/getDetails',{coupon_id}, function(data){
						data = JSON.parse(data);
						console.log(data);
						$.each(data,function(key,value){
							$.each(value,function(k,v){
								if (k == 'date_start' || k == 'date_end') { v = moment(v).format('MM/DD/YYYY')}; 
								$('[name="'+k+'"]').val(v);
							});
						});
					}).fail(function(xhr){
						console.log(xhr.responseText);
					});
				}
		} else { 
			$('.containerAddCoupon').addClass('hide');
			$('#formAddEditCoupon')[0].reset();
			$('[name="coupon_id"]').val('');
		}
			
	});

	$('table').on('click','.btnDeleteCoupon',function(e){
		e.preventDefault();
		var tr = $(this).closest('tr');
		var coupon_id = tr.attr('coupon_id');
		swal({   
            title: "Deleting this Coupon!",   
            text: 'Are you sure you want to delete this Coupon?',   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes!",   
            closeOnConfirm: false ,
            preConfirm: function() {
                $.post('<?php echo $page_controller ?>/deleteCoupon',{coupon_id}, function(data){
                    // data = JSON.parse(data);
                    console.log(data);
                    if (data == 'deleted') {tr.remove();}
                    
                }).fail(function(xhr){
                    console.log(xhr.responseText);
                }); 
            }
        });
	});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 