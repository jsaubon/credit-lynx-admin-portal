<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>


            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"  style="color: #33cccc!important;">
                            <!-- <a id="btnSearchArrowLeft" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-left"></i></a> -->
                            <a href="<?php echo $page_controller ?>">Home</a>
                            <!-- <a id="btnSearchArrowRight" href="#"><i style="font-size: 25px;" class="fa fa-angle-double-right"></i></a> -->

                        </h2>

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>"></a></li>
                            <li id="profile_active" class="breadcrumb-item active hide"></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php $this->load->view('admin/clients/ListSection')?>


                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script>
    $('.left_nav_clients').addClass('active');
    
</script>
<?php $this->load->view('admin/includes/footer.php');?>