<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 
<style>
    #pageTable tbody tr td:nth-child(8) {
        text-align: center;;
    }

    #pageTable tbody tr td:nth-child(7) {
        text-align: right;;
    }

    .dataTables_filter {
        float: right !important; 
    }
</style>
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $this->input->get('partner_type') ?>s Companies</h2> 
                    </div> 
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div id="containerList" class="card-body"> 
                                <form action="partner_companies/save_new_company" method="POST">
                                    <div class="row">
                                        <div class="col-8">
                                            <label>Company Name</label>
                                            <input type="text" name="company_name" class="form-control">
                                            <input type="hidden" name="partner_type" class="form-control" value="<?php echo $this->input->get('partner_type') ?>">
                                        </div>
                                        <div class="col-4">
                                            <label style="color: white">Submit</label>
                                            <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-plus-o"></i> Add Company</button>
                                        </div>
                                    </div>
                                </form>
                                    
                                <div class="table-responsive"> 
                                    <table class="table stylish-table m-t-20" id="pageTable">
                                        <thead>
                                            <tr>
                                                <th>Company Name</th>  
                                                <th width="1">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>   
                                            <?php foreach ($partner_companies as $key => $value): ?>
                                                <tr company_id="<?php echo $value['company_id'] ?>">
                                                    <td><?php echo $value['company_name'] ?></td>
                                                    <td><button class="btn btn-outline-danger btn-block btn-delete-company"><i class="fas fa-trash"></i></button></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
<script>
$(document).ready(function(){  
    $('#pageTable').DataTable(); 

    $('#pageTable').on('click', '.btn-delete-company', function(event) {
        event.preventDefault();
        var company_id = $(this).closest('tr').attr('company_id');
        var tr = $(this).closest('tr');
        var data =  {
                        table: 'partner_companies',
                        pk: company_id,
                        action: 'delete'
                    }; 
        $.post('partner_companies/modelTable', data, function(data, textStatus, xhr) { 
            tr.remove();
        });         
    });
});
</script>     
<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 