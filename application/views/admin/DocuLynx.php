<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<style>
	/*.page-wrapper {
		margin-left: 0 !important;
	} */
	.m-b-10 {
		margin-bottom: 10px !important;
	}

	tr > td {
		line-height: 14px !important;
	}
</style>


	<div class="container-fluid p-t-20">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<?php if ($view == ''): ?>
							<div class="row">
								<div class="col-12">
									<h1>Templates
										<button class="btn btn-success pull-right" data-target="#modalTemplateSettings" data-toggle="modal" ><i class="fas fa-cogs"></i></button>
										<button class="btn btn-success pull-right" data-target="#modalUploadTemplate" data-toggle="modal" >Upload Template</button>

									</h1>
									<hr>
									<div style="overflow-x: auto;">
										<?php foreach ($templates as $key => $template): ?>
											<div id="<?php echo $template['template_id'] ?>" class="card" style="width: 20rem;display: inline-block;">
										      <!-- <img class="card-img-top" src="../../assets/service_agreement/Credit Lynx Service Agreement OH-01.jpg" alt="Card image cap"> -->
										      	<iframe  height="450px" src="http://docs.google.com/gview?url=https://admin.creditlynx.com/assets/templates/<?php echo $template['template_file'] ?>&embedded=true"></iframe>
										      	<div class="card-body text-center">
											        <h4 class="card-title text-center"><?php echo $template['template_name'] ?></h4>
											        <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
											        <a href="#" data-toggle="modal" data-target="#modalClientInformation" class="btnUseTemplate btn btn-success">Use this template</a>
											        <br>
											        <a href="<?php echo base_url('DocuLynx/template') . '?template_id=' . $template['template_id'] . '&action=edit&view' . $view ?>" >Edit this template</a>
											        <br>
											        <a class="text-danger btnDeleteTemplate" href="#" >Delete this template</a>
										      </div>
										    </div>
										<?php endforeach?>
									</div>
										<script>
											$('.btnDeleteTemplate').on('click',function(e){
												e.preventDefault();
												var template_id = $(this).closest('.card').attr('id');
												// alert(template_id);
												swal({
										            title: "Are you sure?",
										            text: 'Are you sure you want to delete this template?',
										            type: "warning",
										            showCancelButton: true,
										            confirmButtonColor: "#DD6B55",
										            confirmButtonText: "Yes!",
										            closeOnConfirm: true ,
										            preConfirm: function() {
										                deleteTemplate(template_id);
										                function deleteTemplate(template_id) {
										                    $.post('<?php echo base_url('DocuLynx/deleteTemplate') ?>',{template_id}, function(data){
										                            console.log(data);
										                            location.reload();
										                        }).fail(function(xhr){
										                            console.log(xhr.responseText);
										                        });
										                    }
										            }
										        });
											});
										</script>
								</div>
							</div>
						<?php endif?>

						<?php if ($view != ''): ?>
							<div class="row">
								<div class="col-12">
									<h1><?php echo ucfirst($view); ?></h1>
									<hr>
									<table id="pageTable" class="table stylish-table">
										<thead>
											<tr>
												<th>Subject</th>
												<th>Status</th>
												<th>Last Changed</th>
												<th>Date Sent</th>
												<th>Action </th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($template_sent as $key => $sent): ?>
												<tr>
													<td style="line-height: 10px !important">
														<strong style="font-size: 100%"><?php echo $sent['template_name'] ?></strong>
														<br>
														<small style="font-size: 80%"><?php echo 'To: ' . $sent['name1'] . ' (' . $sent['email1'] . ')' ?></small>
														<?php if ($sent['name2'] != ''): ?>
															<br>
															<small style="font-size: 80%"><?php echo 'To: ' . $sent['name2'] . ' (' . $sent['email2'] . ')' ?></small>
														<?php endif?>

													</td>
													<td><?php echo $sent['status'] ?></td>
													<td>

														<?php if ($sent['subject'] != 'Email Sent'): ?>
															<?php if ($sent['last_changed'] != ''): ?>
																<?php echo date('m/d/Y h:i:s A', strtotime($sent['last_changed'])) ?>
															<?php endif?>
														<?php endif?>

													</td>
													<td>
														<?php echo date('m/d/Y', strtotime($sent['date_sent'])) ?>
													</td>
													<td>
														<?php if ($sent['status'] != 'voided'): ?>
															<?php if ($sent['template_name'] != ''): ?>
																<a target="_blank" href="<?php echo $sent['href'] . '&sent_id=' . $sent['sent_id'] ?>">View <i class="fas fa-eye"></i></a>
																<br>
																<a  class="text-info" target="_blank" href="<?php echo $sent['href'] . '&view=' . $view ?>">Edit <i class="fas fa-pencil-square-o"></i></a>
																<br>
																<a id="<?php echo $sent['sent_id'] ?>" href="#" class="btnVoidTemplateSent text-danger">Void <i class="fas fa-eye-slash"></i></a>
															<?php endif?>

														<?php endif?>
														<?php if ($sent['status'] == 'voided'): ?>
															Void Reason: <?php echo $sent['void_reason'] ?>
														<?php endif?>

													</td>
												</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						<?php endif?>
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="modalUploadTemplate" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" id="formUploadTemplate" action="DocuLynx/uploadTemplate">
                	<label>Template Type</label>
                	<select class="form-control" name="template_type">
                		<option value="OH Essentials">OH Essentials SA</option>
                		<option value="OH Essentials Plus">OH Essentials Plus SA</option>
                		<option value="OH Premiere">OH Premiere SA</option>
                		<option value="OH Premiere Plus">OH Premiere Plus SA</option>
                		<option value="OH Exclusive">OH Exclusive SA</option>
                		<option value="OH Exclusive Plus">OH Exclusive Plus SA</option>
                		<option value="OH Limited">OH Limited SA</option>
                		<option value="OH Limited Plus">OH Limited Plus SA</option>
                		<option value="Esignature Disclosure">Esignature Disclosure</option>
                		<option value="Federal Disclosure">Federal Disclosure</option>
                		<option value="OH Monitor">OH Monitor SA</option>
                		<option value="OH Monitor Plus">OH Monitor Plus SA</option>
                		<option value="OH Optimize">OH Optimize SA</option>
                		<option value="OH Optimize Plus">OH Optimize Plus SA</option>
                		<option value="Cancellation">Cancellation</option>
                		<option value="New Payment Authorization Form">New Payment Authorization Form</option>
                		<option value="Billing Authorization Form">Billing Authorization Form</option>
                		<option value="Combined SA">Combined SA</option>
                	</select>
                	<!-- <label>Template Name</label>
                	<input required="" type="text" name="template_name" class="form-control"> -->
                	<!-- <label>Template Description</label> -->
                	<!-- <input  type="text" name="template_description" class="form-control"> -->
                	<br>
                	<br>
					<input required="" type="file" name="template_file" id="">
					<br>
					<br>
					<button class="btn btn-success" type="submit">Upload</button>
				</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modalVoidTemplate" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-body">
                <form method="POST" action="DocuLynx/voidTemplate">
                	<input type="hidden" name="sent_id" id="void_sent_id">
                	<label>Reason</label>
                	<textarea required="" name="reason" class="form-control" rows="5"></textarea>
                	<button class="btn pull-right btn-danger waves-effect waves-light" type="submit">Save</button>
				</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modalClientInformation" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<form method="GET" action="<?php echo base_url('DocuLynx/template') ?>">
            <div class="modal-body">
            		<input type="hidden" name="action" value="send">
            		<input type="hidden" name="template_id">
            		<input type="hidden" name="id1">
            		<input type="hidden" name="id2">
            		<div class="row">
                		<div class="col-12 p-30 p-t-10">
                			<h3 class="m-b-0">Add Recipients</h3>
		                	<span>As the sender, you automatically receive a copy of the completed envelope.</span>
		                	<br>
		                	<div class="card m-b-10" >
		                		<div class="card-body">
		                			<label>Client 1</label>
		                			<select id="selectClient1" class=" select2" style="width: 100%">
		                				<option value="">Select Client 1</option>
		                				<?php foreach ($leads as $key => $lead): ?>
		                					<option email="<?php echo $lead['email_address'] ?>" value="<?php echo $lead['client_id'] ?>"><?php echo $lead['name'] ?></option>
		                				<?php endforeach?>
		                			</select>
				                	<input required="" type="text" name="name1" placeholder="Name" class="form-control">
				                	<input required="" type="email" name="email1" placeholder="Email" class="form-control">
		                		</div>
		                	</div>
		                	<div class="card m-b-10">
		                		<div class="card-body">
		                			<label>Client 2</label>
		                			<select id="selectClient2" class=" select2" style="width: 100%">
		                				<option value="">Select Client 2</option>
		                				<?php foreach ($joint_leads as $key => $joint): ?>
		                					<?php if ($joint['client_joint_id'] != ''): ?>
		                						<option email="<?php echo $joint['email_address'] ?>" value="<?php echo $joint['client_joint_id'] ?>"><?php echo $joint['name'] ?></option>
		                					<?php endif?>

		                				<?php endforeach?>
		                			</select>
				                	<input type="text" name="name2" placeholder="Name" class="form-control">
				                	<input type="email" name="email2" placeholder="Email" class="form-control">
		                		</div>
		                	</div>
		                	<div class="card m-b-10">
		                		<div class="card-body">
			                		<h4>Message to All Recipients</h4>
			                		<div class="form-group m-b-10">
			                			<label>Email Subject</label>
			                			<input required="" type="text" name="subject" class="form-control" >
			                		</div>
			                		<div class="form-group m-b-10">
			                			<label>Email Message</label>
				                		<textarea required="" class="form-control" name="message" rows="4">Please review and sign.</textarea>
			                		</div>

			                	</div>
		                	</div>
                		</div>
                	</div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success waves-effect" type="submit">Next</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="modalTemplateSettings" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<form method="POST" action="<?php echo base_url('DocuLynx/saveEmailSettings') ?>">
            <div class="modal-body">
            	<h1>Email Settings</h1>
            	<div class="row">
            		<div class="col-md-12 col-12">
            			<h3>For Signers</h3>
            			<label>Notify me when:</label>
            			<br>
            			<input type="checkbox" name="select_all_for_signers" <?php if ($template_email_settings[0]['select_all_for_signers'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="select_all_for_signers">
            			<label for="select_all_for_signers">Select All</label>
            			<br>
            			<input type="checkbox" name="for_signers_to_sign" <?php if ($template_email_settings[0]['for_signers_to_sign'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_signers_to_sign">
            			<label for="for_signers_to_sign">I have an envelope to sign</label>
            			<br>
            			<input type="checkbox" name="for_signers_is_completed" <?php if ($template_email_settings[0]['for_signers_is_completed'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_signers_is_completed">
            			<label for="for_signers_is_completed">An Envelope is complete</label>
            			<br>
            			<input type="checkbox" name="for_signers_is_corrected" <?php if ($template_email_settings[0]['for_signers_is_corrected'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_signers_is_corrected">
            			<label for="for_signers_is_corrected">An Envelope is corrected</label>
            			<br>
            			<input type="checkbox" name="for_signers_declines_to_sign" <?php if ($template_email_settings[0]['for_signers_declines_to_sign'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_signers_declines_to_sign">
            			<label for="for_signers_declines_to_sign">A signer declines to sign</label>
            			<br>
            			<input type="checkbox" name="for_signers_is_vioded" <?php if ($template_email_settings[0]['for_signers_is_vioded'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_signers_is_vioded">
            			<label for="for_signers_is_vioded">Envelope is vioded</label>
            			<br>
            			<h3>For Sender</h3>
            			<label>Notify me when:</label>
            			<br>
            			<input type="checkbox" name="select_all_for_sender" <?php if ($template_email_settings[0]['select_all_for_sender'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="select_all_for_sender">
            			<label for="select_all_for_sender">Select All</label>
            			<br>
            			<input type="checkbox" name="for_sender_is_completed" <?php if ($template_email_settings[0]['for_sender_is_completed'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_sender_is_completed">
            			<label for="for_sender_is_completed">An Envelope is complete</label>
            			<br>
            			<input type="checkbox" name="for_sender_view_envelope" <?php if ($template_email_settings[0]['for_sender_view_envelope'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_sender_view_envelope">
            			<label for="for_sender_view_envelope">A signer view the envelope</label>
            			<br>
            			<input type="checkbox" name="for_sender_declines_to_sign" <?php if ($template_email_settings[0]['for_sender_declines_to_sign'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_sender_declines_to_sign">
            			<label for="for_sender_declines_to_sign">A signer declines to sign</label>
            			<br>
            			<input type="checkbox" name="for_sender_to_recipient_fails" <?php if ($template_email_settings[0]['for_sender_to_recipient_fails'] == 'on'): ?>
            				checked=""
            			<?php endif?> id="for_sender_to_recipient_fails">
            			<label for="for_sender_to_recipient_fails">Envelope Delivery to a recipient fails</label>
            			<br>
            		</div>
            	</div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success waves-effect" type="submit" >Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){


	$('#pageTable').DataTable({
        'aaSorting': [],
        dom: 'Bfrtip',
        "pageLength": 100
    });

    $('#pageTable').removeClass('dataTable');

	$('.btnVoidTemplateSent').on('click',function(){
		var sent_id = $(this).attr('id');
		$('#void_sent_id').val(sent_id);
		$('#modalVoidTemplate').modal('show');

	});

	$('#select_all_for_sender').on('change',function(){
		if ($('#select_all_for_sender').is(':checked')) {
			$('#for_sender_is_completed').prop('checked',true);
			$('#for_sender_view_envelope').prop('checked',true);
			$('#for_sender_declines_to_sign').prop('checked',true);
			$('#for_sender_to_recipient_fails').prop('checked',true);
		} else {
			$('#for_sender_is_completed').prop('checked',false);
			$('#for_sender_view_envelope').prop('checked',false);
			$('#for_sender_declines_to_sign').prop('checked',false);
			$('#for_sender_to_recipient_fails').prop('checked',false);
		}
	});

	$('#select_all_for_signers').on('change',function(){
		if ($('#select_all_for_signers').is(':checked')) {
			$('#select_all_for_signers').prop('checked',true);
			$('#for_signers_to_sign').prop('checked',true);
			$('#for_signers_is_completed').prop('checked',true);
			$('#for_signers_is_corrected').prop('checked',true);
			$('#for_signers_declines_to_sign').prop('checked',true);
			$('#for_signers_to_recipient_fails').prop('checked',true);
			$('#for_signers_is_vioded').prop('checked',true);
		} else {
			$('#select_all_for_signers').prop('checked',false);
			$('#for_signers_to_sign').prop('checked',false);
			$('#for_signers_is_completed').prop('checked',false);
			$('#for_signers_is_corrected').prop('checked',false);
			$('#for_signers_declines_to_sign').prop('checked',false);
			$('#for_signers_to_recipient_fails').prop('checked',false);
			$('#for_signers_is_vioded').prop('checked',false);
		}
	});

	$('.btnUseTemplate').on('click',function(){
		var template_id = $(this).closest('.card').attr('id');
		var template_name = $(this).closest('.card').find('.card-title').html();
		$('input[name="template_id"]').val(template_id);
		$('input[name="subject"]').val('Credit Lynx '+template_name);
	});

	$('#selectClient1').on('change',function(){
		var name = $('#selectClient1 option:selected').text();
		var email_address = $('#selectClient1 option:selected').attr('email');
		var client_id = $(this).val();
		$('input[name="id1"]').val(client_id);
		$('input[name="name1"]').val(name);
		$('input[name="email1"]').val(email_address);
	});
	$('#selectClient2').on('change',function(){
		var name = $('#selectClient2 option:selected').text();
		var email_address = $('#selectClient2 option:selected').attr('email');
		var client_joint_id = $(this).val();
		$('input[name="id2"]').val(client_joint_id);
		$('input[name="name2"]').val(name);
		$('input[name="email2"]').val(email_address);
	});
});
</script>
<?php
$this->load->view('admin/includes/footer.php');
?>