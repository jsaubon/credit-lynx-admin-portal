<?php 
    $this->load->view('admin/includes/header.php');
    $this->load->view('admin/includes/nav-left.php');
    $this->load->view('admin/includes/nav-top.php'); 
?> 

<style>
	.timeline-title input {
		border: none !important;
		font-weight: 400;
		color: inherit;
		font-size: 18px;
		padding: 0px !important;
	}

	.st_note {
		border: none !important; 
		padding: 0px !important;
	}

	hr {
		border: inset !important;
		border-top: 1px solid rgba(0,0,0,.1) !important;

	}

	.timeline:before {
		left: 5%;
	}

	.timeline>li>.timeline-badge {
		left: 5%;
	}

	.timeline>li>.timeline-panel {
		width: 88%;
	}
 	.card .card-header {
 		border-bottom: 1px solid #d7dfe3;
 	}

 	.card-outline-success {
 		color: white;
 	}

 	.btn-outline-success {
 		border-color: white;
 	}

 	.btn-outline-success:not(:disabled):not(.disabled).active, .btn-outline-success:not(:disabled):not(.disabled):active, .show>.btn-outline-success.dropdown-toggle {
 		background-color: #26c6da;
 		border-color: #26c6da;
 	}
</style>
<div class="container-fluid p-t-20">
	<div class="row">
		<div class="col-12">
			<div class="card ">
				<div class="card-header">
					<span style="font-size: 22px">Campaigns <small>Manage all campaigns</small></span>
					<div class="card-actions">
                        <a class="btn btn-success btn-rounded btn_add_new_campaign"  style="padding-left: 20px;color: white"><i class="fas fa-plus"></i> New Campaign</a>
                    </div>
				</div>
				<div class="card-body">
					<div class="card card_add_edit_campaign_container hide animated fadeIn">
						<div class="card-header">
							<span style="font-size: 22px">Campaign</span>
							<div class="card-actions"> 
		                        <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
		                    </div>
						</div>
						<div class="card-body">
							<form onsubmit="return false" id="form_campaigns">
								<input type="hidden" name="campaign_id">
								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-md-12">
												<label class="m-b-0">Campaign Name</label>
												<input required type="text" name="campaign_name" class="form-control">
											</div>
											<div class="col-md-6 m-t-10">
												<label class="m-b-0">Campaign Type</label>
												<select required class="form-control select_campaign_type" name="campaign_type">
													<option value="">Select Campaign Type</option>
													<option value="Clients">Clients</option>
													<option value="Leads">Leads</option>
												</select>
											</div>
											<div class="col-md-6 m-t-10">
												<label class="m-b-0" class="">Status</label>
												<select required class="form-control" name="campaign_status">
													<option value="">Select Status</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card m-b-0">
											<div class="card-body">
												<button type="submit" class="btn btn-block btn-success"><i class="fas fa-play"></i> Start Campaign</button>
												<button type="button" class="btn btn-block btn-primary"><i class="fas fa-users"></i> Show Contacts</button>
												<button type="button" class="btn btn-block btn-danger"><i class="fas fa-trash"></i> Delete Campaign</button>
											</div>
										</div>
												
									</div>
											
								</div>
							</div>
							</form>
							
					</div>
					<div class="card_campaigns_container">
						
					</div>
				</div>
			</div>
			<div class="card hide animated fadeIn" id="container_campaign">
				<div class="card-body" style="padding-bottom: 60px;"> 
					<ul class="timeline">
                         
                         
                    </ul>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.btn_add_new_campaign').on('click', function(event) {
		event.preventDefault();
		if ($('.card_add_edit_campaign_container').hasClass('hide')) {
			$('.card_add_edit_campaign_container').removeClass('hide');	
		} else {
			$('.card_add_edit_campaign_container').addClass('hide');
		}
		
	});

	$('#form_campaigns').on('submit', function(event) {
		event.preventDefault();
		var campaign_id = $(this).find('[name="campaign_id"]').val(); 
		var data =  {
		                table: 'campaigns',
		                pk: campaign_id,
		                action: 'save',
		                fields: {
		                            campaign_name: $(this).find('[name="campaign_name"]').val(),
		                            campaign_type: $(this).find('[name="campaign_type"]').val(),
		                            status: $(this).find('[name="campaign_status"]').val(),
		                            date_updated: moment().format('YYYY-MM-DD HH:mm:ss'),
		                            active: 1
		                        },
		                return: 'campaign_id'
		            };
		
		$.post('campaigns/modelTable', data , function(data, textStatus, xhr) { 
			$('#form_campaigns')[0].reset();
			$('[name="campaign_status"]').empty(); 
			$('.card_add_edit_campaign_container').addClass('hide');
			get_campaigns();;
		});
	});

	$('.select_campaign_type').on('change', function(event) {
		event.preventDefault();
		var campaign_type = $(this).val();
		get_campaign_statuses(campaign_type,'');
	});	

	function get_campaign_statuses(campaign_type,campaign_status_) {
		var data =  {
		                table: campaign_type == 'Clients' ? 'client_statuses'  : 'lead_statuses',
		                action: 'get',
		                select: {
		                            
		                        },
		                where:  {
									
		                        },
		                field: '',
		                order: '',
		                limit: 0,
		                offset: 0,
		                group_by: ''
		
		            };
		$.post('campaigns/modelTable', data, function(data, textStatus, xhr) {
			data = JSON.parse(data);
			var campaign_status = $('[name="campaign_status"]');
			campaign_status.empty();
			$.each(data, function(index, val) {
				if (campaign_type == 'Clients') {
					campaign_status.append('<option value="'+val.client_status+'">'+val.client_status+'</option>');
				} else {
					campaign_status.append('<option value="'+val.lead_status+'">'+val.lead_status+'</option>');
				}
				
			});

			campaign_status.val(campaign_status_); 
		});
	}
	get_campaigns();

	function get_campaigns() {
		var data =  {
		                table: 'campaigns',
		                action: 'get',
		                select: {
		                            
		                        },
		                where:  {
		
		                        },
		                field: 'date_updated',
		                order: 'DESC',
		                limit: 0,
		                offset: 0,
		                group_by: ''
		
		            };
		$.post('campaigns/modelTable', data, function(data, textStatus, xhr) {
			data = JSON.parse(data); 
			var card_campaigns_container = $('.card_campaigns_container');
			card_campaigns_container.empty();
			$.each(data, function(index, val) {
			 	var new_campaign = '<div class="card card-outline-success" campaign_id="'+val.campaign_id+'">\
										<div class="card-header">\
											<span style="font-size: 22px">'+val.campaign_name+'</span>\
											<div class="card-actions">\
						                        <a class="btn btn-outline-success btn-rounded"  style="padding-left: 18px;color: white"><i class="fas fa-cogs"></i> Manage</a>\
						                        <a class="btn btn-outline-success btn-rounded btn_edit_campaign"  style="padding-left: 18px;color: white"><i class="fas fa-edit"></i> Edit</a>\
						                        <a class="btn btn-outline-success btn-rounded"  style="padding-left: 18px;color: white"><i class="fas fa-copy"></i> Copy</a>\
						                        <a class="btn btn-outline-success btn-rounded btn_delete_campaign"  style="padding-left: 18px;color: white"><i class="fas fa-times"></i> Delete</a>\
						                    </div>\
										</div>\
									</div>';
				card_campaigns_container.append(new_campaign);
			});
		});
	}

	$('.card_campaigns_container').on('click','.btn_edit_campaign', function(event) {
		event.preventDefault(); 
		$('.card_add_edit_campaign_container').removeClass('hide');	
		var campaign_id = $(this).closest('.card').attr('campaign_id');
		get_campaign_by_id(campaign_id);
	});
	$('.card_campaigns_container').on('click','.btn_delete_campaign', function(event) {
		event.preventDefault();  
		var campaign_id = $(this).closest('.card').attr('campaign_id');
		var card = $(this).closest('.card');
		swal({
                title: "Delete Campaign Confirmation",
                text: 'Are you sure you want to delete this Campaign?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                closeOnConfirm: true ,
                preConfirm: function() { 
                    delete_campaign_by_id(campaign_id,card);
                }
            });
		
	});

	function delete_campaign_by_id(campaign_id,card) {
		var data =  {
		                table: 'campaigns',
		                pk: campaign_id,
		                action: 'delete'
		            }; 
		$.post('campaigns/modelTable', data, function(data, textStatus, xhr) { 
			card.fadeOut('slow', function() {
				card.remove();
			});
			
		});
	}

 
	function get_campaign_by_id(campaign_id) {
		var data =  {
		                table: 'campaigns',
		                action: 'get',
		                select: {
		                            
		                        },
		                where:  {
									campaign_id: campaign_id
		                        },
		                field: '',
		                order: '',
		                limit: 0,
		                offset: 0,
		                group_by: ''
		
		            };
		$.post('campaigns/modelTable', data, function(data, textStatus, xhr) {
			data = JSON.parse(data);
			$.each(data, function(index, val) { 
				$('[name="campaign_id"]').val(val.campaign_id);
				$('[name="campaign_name"]').val(val.campaign_name);
				$('[name="campaign_type"]').val(val.campaign_type); 
				get_campaign_statuses(val.campaign_type,val.status);
			});
		});
	}


</script>

<?php  
    $this->load->view('admin/includes/footer.php'); 
?> 