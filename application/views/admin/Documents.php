<?php
$this->load->view('admin/includes/header.php');
$this->load->view('admin/includes/nav-left.php');
$this->load->view('admin/includes/nav-top.php');
?>
<link href="<?php echo base_url('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') ?>"></script>
<style>
    .tr_active {
        background-color: #f2f4f8;
    }
</style>
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><a href="">Documents</a> </h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Documents</a> </li>
                            <li class="breadcrumb-item active"><span id="folderName"></span></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body containerAllFolder">
                                <div class="row el-element-overlay">
                                    <div class="col-12">
                                        <h4 class="card-title">Quick Access</h4>
                                        <!-- <h6 class="card-subtitle m-b-20 text-muted">you can make gallery like this</h6> -->
                                    </div>
                                    <?php foreach ($last4 as $key => $value): ?>
                                        <?php if (strpos($value['file_download'], '.png') !== false || strpos($value['file_download'], '.jpg') !== false || strpos($value['file_download'], '.bmp') !== false || strpos($value['file_download'], '.jpeg') !== false) {?>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card">
                                                    <div class="el-card-item">
                                                        <div class="el-card-avatar el-overlay-1" style=" height: 204.047px">
                                                            <img src="<?php echo base_url("assets/documents/") . $value['folder_name'] . '/' . $value['file_download'] ?>" alt="user" />
                                                            <div class="el-overlay">
                                                                <ul class="el-info">
                                                                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo base_url("assets/documents/") . $value['folder_name'] . '/' . $value['file_download'] ?>"><i class="icon-magnifier"></i></a></li>
                                                                    <li><a download class="btn default btn-outline" href="<?php echo base_url('assets/images/LynxImage.png') ?>"><i class="fas fa-download"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="el-card-content">
                                                            <h3 class="box-title"><?php echo $value['category'] ?></h3><small><?php echo $value['folder_name'] ?></small>
                                                            <br/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else {?>
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card">
                                                    <div class="el-card-item">
                                                        <div class="el-card-avatar el-overlay-1" style=" height: 204.047px">
                                                            <?php if ($value['category'] != 'Service Agreement'): ?>
                                                                <iframe  height="204.047px" src="http://docs.google.com/gview?url=<?php echo base_url("assets/documents/") . $value['folder_name'] . '/' . $value['file_download'] ?>&embedded=true"></iframe>
                                                            <?php endif?>
                                                            <?php if ($value['category'] == 'Service Agreement' || $value['category'] == 'Payment Change Request Form'): ?>
                                                                <iframe allowfullscreen  style="position:absolute;top:0;left:0;width:100%;height:100%;" height="204.047px" src="<?php echo $value['file_download'] ?>&embedded=true"></iframe>
                                                            <?php endif?>


                                                        </div>
                                                        <div class="el-card-content">
                                                            <h3 class="box-title"><?php echo $value['category'] ?></h3><small><?php echo $value['folder_name'] ?></small>
                                                            <br/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php endforeach?>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <table id="pageTable" class="table stylish-table table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width:60vw">Name</th>
                                                    <th style="width:20vw">Last Modified</th>
                                                    <th style="width:20vw">Size</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($pageTable as $key => $value): ?>
                                                    <tr class="b-b" id="<?php echo $value['d_id'] ?>">
                                                        <td><?php echo $value['NAME'] ?></td>
                                                        <td><?php echo $value['last_modified'] ?></td>
                                                        <td><?php echo round($value['size'], 2) . ' MB' ?></td>
                                                    </tr>
                                                <?php endforeach?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body hide containerIndividualFolder">
                                <table id="folderDocuments" class="table stylish-table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width:40vw">Category</th>
                                            <th style="width:20vw">Date Uploaded</th>
                                            <th style="width:20vw">Size</th>
                                            <th style="width:20vw">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<div id="modalShowFileContentPreview" class="modal fade "  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog " style="margin: 30px;max-width: 9999999px">
        <div class="modal-content" style="width: 50vw;margin: auto;min-height: 10vh">
             <div class="row">
                 <div class="col-12 text-center" id="previewFile">

                 </div>
             </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#pageTable').DataTable({
            'aaSorting': [],
            dom: 'Bfrtip',
            "pageLength": 20,
            'bInfo': false
        });

        var folderDocuments = $('#folderDocuments').DataTable({
            'aaSorting': [],
            dom: 'Bfrtip',
            "pageLength": 20,
            'bInfo': false
        });

        $('#pageTable,#folderDocuments').removeClass('dataTable');
        var folder_name = '';
        $('#pageTable tbody').on('click','tr',function(){
            $('#pageTable tr').removeClass('tr_active');
            $(this).addClass('tr_active');
            var tr = $(this);
            folder_name = tr.find('td:nth-child(1)').html();
            $('#folderName').html(folder_name);
            $('.containerAllFolder').addClass('hide');
            $('.containerIndividualFolder').removeClass('hide');

            var d_id = tr.attr('id');
            d_id = d_id.split('_');
            var id = d_id[0];
            var client_type = d_id[1];
            getFilesFromFolder(id,client_type);
        });

        function getFilesFromFolder(id,client_type) {
            $.post('<?php echo base_url('admin/documents/getFilesFromFolder') ?>',
                {id,client_type},function(data){
                    data = JSON.parse(data);
                    folderDocuments.clear().draw();
                    $.each(data,function(key,value){
                        var file_size = ((value.file_size / 1000) / 1000).toFixed(2);

                        var btnPreview = '<a class="btnPreview text-info" href="#"> <i class="fas  fa-eye fa-lg"></i> Preview</a>';
                        var btnPreviewHref = '<a target="_blank" class=" text-info" href="'+value.file_download+'"> <i class="fas  fa-eye fa-lg"></i> Preview</a>';
                        var btnDownload = '<a class="text-success btnDownload" download href="<?php echo base_url('assets/documents/') ?>'+folder_name+'/'+value.file_download+'"> <i class="fas fa-download  fa-lg"></i> Download</a>';



                        if (value.category == 'Service Agreement' || value.category == 'Payment Change Request Form') {
                            folderDocuments.row.add([value.category,value.date_uploaded,file_size+' MB',btnPreviewHref]);
                        } else {
                            folderDocuments.row.add([value.category,value.date_uploaded,file_size+' MB',btnDownload+' '+btnPreview]);
                        }

                        folderDocuments.draw();
                    });


                    $('#folderDocuments').on('click','.btnPreview',function(e){
                        e.preventDefault();
                        var tr = $(this).closest('tr');
                        var href = tr.find('.btnDownload').attr('href');
                        $('#modalShowFileContentPreview').modal('show');
                        $('#previewFile').empty();
                        if (~href.indexOf('.png') || ~href.indexOf('.jpg') || ~href.indexOf('.jpeg') || ~href.indexOf('.bmp') ) {
                            var image = '<a download href="'+href+'"><img width="100%" src="'+href+'" alt="user" /></a>';
                            $('#previewFile').append(image);
                        } else {
                            var frame = '<iframe  width="100%" style="height: 90vh" src="http://docs.google.com/gview?url='+href+'&embedded=true"></iframe>';
                            $('#previewFile').append(frame);
                        }


                    });
                });
        }

    });

</script>
<?php
$this->load->view('admin/includes/footer.php');
?>