 
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h2 class="text-themecolor m-b-0 m-t-0"><?php echo $page_title ?></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo $page_controller ?>">Home</a></li>
                            <li id="profile_active" class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                 

                <div id="profileSection" class="row fade show"> 
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                <li id="profile_tab" class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#profile" role="tab" aria-selected="true">Profile</a> </li>
                                <li id="worked_hours_tab" class="nav-item"> <a class="nav-link" data-toggle="tab" href="#worked_hours" role="tab" aria-selected="true">Worked Hours</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"> 
                                <div class="tab-pane active show" id="profile" role="tabpanel">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 text-center b-r" > 
                                                <input type="hidden" name="current_photo" id="current_photo">
                                                <form id="uploadProfilePictureForm" enctype="multipart/form-data">
                                                    <input name="user_photo" type="file" id="uploadProfilePicture" class="dropify" data-default-file="<?php echo base_url() ?>/assets/images/LynxLogo.png" data-show-remove="false" data-height="150" />
                                                    <input type="hidden" name="processor_id">
                                                </form>   
                                                <h3 class="name m-b-0"></h3>
                                            </div>
 
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <strong>Job Title</strong>
                                                <br>
                                                <p class="text-muted job_title"></p>
                                                <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted phone_number"></p>
                                            </div>
                                            <div class="col-md-5 col-xs-6 "> 
                                                <strong>Email</strong>
                                                <br>
                                                <p class="text-muted email_address"></p>
                                                <strong>Location</strong>
                                                <br>
                                                <p class="text-muted ">
                                                    <span class="address"></span>, 
                                                    <span class="city"></span><br>
                                                    <span class="state"></span>, 
                                                    <span class="zip_code"></span>
                                                </p>
                                            </div> 
                                        </div>
                                        <hr> 
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h3>Other Information</h3>
                                                <span>Last Login: <span class="last_login"></span></span><br>
                                                <span>Date of Birth: <span class="date_of_birth"></span></span><br>  
                                                <span>SS#: xxx-xx-<span class="ss_restricted"></span></span><br>
                                                <span>Salary: <span class="salary"></span></span><br>
                                                <span>Hourly: <span class="hourly"></span></span><br>
                                                <span>Commission: <span class="commission"></span></span>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Bank Information</h5>
                                                <span>Bank Name: <span class="bank_name"></span></span><br>
                                                <span>Bank Address: <span class="bank_address"></span></span><br>
                                                <span>Account #: <span class="account_number"></span></span><br>
                                                <span>Routing #: <span class="routing_number"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>Emergency Information</h5>
                                                <span>Name: <span class="emergency_name"></span></span><br>
                                                <span>Phone: <span class="emergency_phone"></span></span><br>
                                                <span>Address: <span class="emergency_address"></span></span><br>
                                                <span>City: <span class="emergency_city"></span></span><br>
                                                <span>State: <span class="emergency_state"></span></span><br>
                                                <span>Zip: <span class="emergency_zip"></span></span><br>
                                                <span>Relationship: <span class="emergency_relationship"></span></span><br>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r">
                                                <h5>User Account Information</h5>
                                                <span>Username: <span class="username"></span></span><br>
                                                <span>Password: <span class="password"></span></span><br>
                                            </div>
                                        </div>


                                        <ul class="nav nav-tabs profile-tab" role="tablist"> 
                                            <li class="nav-item"> <a id="tasks-tab-header" class="nav-link active show" data-toggle="tab" href="#tasks-tab" role="tab" aria-selected="true">Tasks</a> </li>
                                            <li class="nav-item"> <a id="notes-tab-header" class="nav-link" data-toggle="tab" href="#notes-tab" role="tab" aria-selected="false">Notes</a> </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a task</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_task" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <input type="text" name="" class="material_date_time text-center" id="new_task_date" placeholder="Task Date">
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewTask" class="btn btn-success waves-effect waves-light" type="button">Save Task</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="task_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                        
                                                        
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="notes-tab" role="tabpanel">
                                                <div class="card-body">
                                                    <h5>Make a note</h5>
                                                    <div class="input-group">  
                                                        <textarea style="border-radius: 0px;padding-top: 5px !important" id="new_note" class="form-control" rows="2"></textarea>  
                                                        <div class="input-group-append">
                                                            <i id="new_note_sticky" class="fa-note fa fa-star fa-lg text-default"><br>sticky?</i>
                                                            
                                                        </div>
                                                        <div class="input-group-append">
                                                            <button id="btnSaveNewNote" class="btn btn-success waves-effect waves-light" type="button">Save Note</button>
                                                        </div>
                                                    </div>  
                                                    <div class="message-scroll ">
                                                        <div id="note_list_sticky" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                        <div id="note_list" class="profiletimeline m-t-40 ">  
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                                    
                                    </div>
                                </div> 
                                <div class="tab-pane" id="worked_hours" role="tabpanel">
                                    <div class="card-body">
                                        <table id="tblWorkedHours" class="table table-bordered table-striped">
                                            <thead>
                                                <th>Date</th>
                                                <th>Worked Hours</th>
                                                <th>Total Paid</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->




 
 <script>
$(document).ready(function() {
    // $('.left_nav_'+'<?php echo $page_controller ?>').addClass('active');
    var newUser = false;
    var processor_id = '<?php echo $userdata['user_id'] ?>';  
    var tblWorkedHours = $('#tblWorkedHours').DataTable();
    getDetails(processor_id);
    getTasks(processor_id);
    getNotes(processor_id);
    getMyTimeWorked();



    


    $('#btnSaveNewTask').on('click',function(){
        var button = $(this);
        var task = $('#new_task');
        var task_date = $('#new_task_date');
        if (task.val() != '') {
            if (task_date.val() != '') {
                saveTask(button,processor_id,task,task_date);
            } else {
                task_date.focus();
            }
            
        } else {
            task.focus();
        }
    });

    $('#new_note_sticky').on('click',function(){
        var note_sticky = $(this);
        if (note_sticky.hasClass('text-warning')) {
            note_sticky.removeClass('text-warning');
            note_sticky.addClass('text-default');
        } else {
            note_sticky.addClass('text-warning');
            note_sticky.removeClass('text-default');
        }
    });

    $('#btnSaveNewNote').on('click',function(){
        var button = $(this);
        var note = $('#new_note');
        var note_sticky = $('#new_note_sticky');
        note_sticky = note_sticky.hasClass('text-warning') ? 1 : 0;
        if (note.val() != '') {
            saveNote(button,processor_id,note,note_sticky);
        } else {
            note.focus();
        }
    });

    $('#uploadProfilePicture').on('change',function(){
        $.ajax({ 
            url: '<?php echo $page_controller ?>/uploadProfilePicture',
            type: 'POST', 
            data: new FormData($('#uploadProfilePictureForm')[0]), 
            cache: false,
            contentType: false,
            processData: false, 
            success: function(data) { 

            }
        });
    });

    function getDetails(processor_id) {
        $.post('<?php echo $page_controller ?>/getDetails',
            {processor_id},function(data){
                data = JSON.parse(data);  
                $('input[name=current_photo]').val(data[0]['photo']);
                $('.dropify-render').find('img').attr('src','<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/'+data[0]['photo']);
                
                $('#profileSection').find('#profile_picture').attr('src','<?php echo base_url() ?>/assets/images/users/<?php echo $page_folder ?>/'+data[0]['photo']); 
                $('input[name=date_of_birth]').val(moment(data[0]['date_of_birth']).format('YYYY-MM-DD'));
                $.each(data,function(key,value){ 
                    $.each(value,function(k,v){
                        $('input[type=text][name='+k+']').val(v);
                        $('input[type=email][name='+k+']').val(v);
                        $('input[type=password][name='+k+']').val(v);
                        $('input[type=hidden][name='+k+']').val(v);
                        $('select[name='+k+']').val(v);

                        $('.'+k).html(v);
                        // console.log(k+ ' ' +v);
                    })
                });

                $('select').trigger('change');

                var inputs =  $('#formAddUser input'); 
                $.each(inputs,function(key,input){ 
                    $(input).closest('.form-group').addClass('focused');
                });
 
                $('input[name=username]').prop('readonly',true); 

                
        });
    }

    function getMyTimeWorked() {
        tblWorkedHours.clear().draw();
        $.post('<?php echo $page_controller ?>/getMyTimeWorked', 
            function(data){
                data = JSON.parse(data); 
                $.each(data,function(key,value){
                    tblWorkedHours.row.add([value.work_date,value.hours+'h '+value.minutes+' m',parseInt(value.hours) * parseInt(value.hourly)]).draw(false);
                });
            });
    }
 
    function getTasks(processor_id) {
        $.post('<?php echo $page_controller ?>/getTasks',
            {processor_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#task_list').empty();
                $.each(data,function(key,value){
                    var task_active = value.task_active;
                    if (task_active == '1') {
                        task_active = '<button class="task_update_button btn btn-warning waves-effect waves-light btnUpdateTaskDone" type="button"><i class="fa fa-times"></i></button>';
                    } else {
                        task_active = '<button class="task_update_button btn btn-success waves-effect waves-light btnUpdateTaskActive" type="button"><i class="fa fa-check"></i></button>';
                    }

                    var delete_button = '<button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateTaskDelete" type="button"><i class="fa fa-trash"></i></button>';
                    var newTask = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.task_id+'">\
                                        <div class="sl-left"> <img src="<?php echo base_url() ?>/assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.task_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.task+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+task_active+'\
                                                    </div>\
                                                    <div class="input-group-append">\
                                                        '+delete_button+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    $('#task_list').append(newTask);
                });

                $('.btnUpdateTaskDone').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,0);
                });

                $('.btnUpdateTaskActive').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,1);
                });

                $('.btnUpdateTaskDelete').on('click',function(){
                    var button = $(this);
                    var task_id = $(this).closest('.sl-item').attr('value');
                    updateTask(button,task_id,2);
                });
            });
    }

    function saveTask(button,processor_id,task,task_date){
        var task_ = task;
        var task = task.val();
        var task_date_ = task_date;
        var task_date = task_date.val();
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveTask',
            {processor_id,task,task_date},function(data){
                // console.log(data);
                task_date_.val('');
                task_.val('');
                getTasks(processor_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateTask(button,task_id,task_active) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateTask',
            {task_id,task_active},function(data){
                getTasks(processor_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function getNotes(processor_id) {
        $.post('<?php echo $page_controller ?>/getNotes',
            {processor_id},function(data){
                data = JSON.parse(data);
                // console.log(data);
                $('#note_list').empty();
                $('#note_list_sticky').empty();
                $.each(data,function(key,value){
                    var note_sticky = value.note_sticky;
                    if (note_sticky == '1') {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-warning"></i>';
                    } else {
                        note_sticky = '<i class="btnUpdateNoteSticky fa-note fa fa-star fa-lg text-default"></i>';
                    }
                    var delete_button = '';
                    if ('<?php echo $userdata['login_type'] ?>' == 'Administrator') {
                        delete_button = '<div class="input-group-append">\
                                            <button class="task_update_button btn btn-danger waves-effect waves-light btnUpdateNoteDelete" type="button"><i class="fa fa-trash"></i></button>\
                                        </div>';
                    }

                    var newNote = '<div class="sl-item" style="margin-bottom: 5px" value="'+value.note_id+'">\
                                        <div class="sl-left"> <img src="<?php echo base_url() ?>/assets/images/users/'+value.sender+'/'+value.sender_photo+'" alt="user" class="img-circle"> </div>\
                                        <div class="sl-right">\
                                            <div><a class="link">'+value.sender_name+'</a> <span class="sl-date">'+moment(value.note_date).format('lll')+'</span>\
                                                <div class="input-group">\
                                                    <blockquote class="form-control">\
                                                        '+value.note+'\
                                                    </blockquote>\
                                                    <div class="input-group-append">\
                                                        '+note_sticky+'\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <hr>';
                    if (value.note_sticky == '1') {
                        $('#note_list_sticky').append(newNote);
                    } else {
                        $('#note_list').append(newNote);
                    }
                });

                $('.btnUpdateNoteSticky').on('click',function(){
                    var button = $(this);
                    var note_id = $(this).closest('.sl-item').attr('value');
                    if (button.hasClass('text-warning')) {
                        updateNote(button,note_id,0);
                    } else {
                        updateNote(button,note_id,1);
                    }
                    
                });
 
            });
    }

    function saveNote(button,processor_id,note,note_sticky){
        var note_ = note;
        var note = note.val(); 
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/saveNote',
            {processor_id,note,note_sticky},function(data){
                // console.log(data); 
                $('#new_note_sticky').removeClass('text-warning');
                $('#new_note_sticky').addClass('text-default');
                note_.val('');
                getNotes(processor_id);
                button.prop('disabled',false);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

    function updateNote(button,note_id,note_sticky) {
        button.prop('disabled',true);
        $.post('<?php echo $page_controller ?>/updateNote',
            {note_id,note_sticky},function(data){
                getNotes(processor_id);
            }).fail(function(xhr){
                console.log(xhr.responseText);
            });
    }

});
</script>

