        <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <!--  <footer class="footer">
                Credit Lynx
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

<script> 
    $(".select2").select2(); 
    $('.material_date').bootstrapMaterialDatePicker({ weekStart: 0, time: false,format: 'MM/DD/YYYY' });
    $('.material_date_time').bootstrapMaterialDatePicker({ format: 'YYYY-MM-DD HH:mm',shortTime: true });
    $('.dropify').dropify(); 
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    $('.dropify-filename-inner').html('');
</script>
</body>

</html>