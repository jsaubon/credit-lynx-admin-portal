<!DOCTYPE html>
<html>
<head>
<title></title> 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
    table{border-collapse: collapse !important;}
    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* MOBILE STYLES */
    @media screen and (max-width: 400px) {
        a:hover {
            color: white;
        }

        /* ALLOWS FOR FLUID TABLES */
        .wrapper {
          width: 100% !important;
            max-width: 100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        .logo img {
          margin: 0 auto !important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        .mobile-hide {
          display: none !important;
        }

        .img-max {
          max-width: 100% !important;
          width: 100% !important;
          height: auto !important;
        }

        /* FULL-WIDTH TABLES */
        .responsive-table {
          width: 100% !important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        .padding {
          padding: 10px 5% 15px 5% !important;
        }

        .padding-meta {
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        .padding-copy {
             padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        .no-padding {
          padding: 0 !important;
        }

        .section-padding {
          padding: 50px 15px 50px 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        .mobile-button-container {
            margin: 0 auto;
            width: 100% !important;
        }

        .mobile-button {
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
            display: block !important;
        }

        .containerBtn {
            padding-right: 175px;
            padding-left: 175px;
        }

    }

    /* ANDROID CENTER FIX */
    div[style*="margin: 16px 0;"] { margin: 0 !important; }


    a:hover {
        color: white;
    }
</style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">
 
<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                        <a href="http://creditlynx.com" target="_blank">
                            <img alt="" src="<?php echo base_url('assets/images/LynxImage.png')?>" width="200px" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="300px" style="max-width: 600px;" class="responsive-table">
                <tr>
                    <td>
                        <!-- COPY -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: white; padding-top: 30px;background-color: #17a2b8;padding-bottom: 40px;" class="padding-copy">
                                    <img src="https://ci5.googleusercontent.com/proxy/6yFR0NIga2a6bd6n0Lj_KSLucNYQWIJdgBnKIcP5l4Rgqpkopu6GYAAK2EzeELOCtxVhSNriMRFDlR1FxY8ws8bWx7MXSgbcf87IIESmD5zl3Ah3XQ=s0-d-e1-ft#https://NA3.docusign.net/member/Images/email/docComplete-white.png" width="90px" >
                                    <p style="font-size: 60%;padding-left: 5px;padding-right: 5px"><?php echo $header_message ?></p>
                                    <div style="display: block;">
                                        <a id="containerBtn" href="<?php echo $href?>" class="btn" style="text-decoration: none;padding: 18px;border: 1px solid white;background-color: #17a2b8;color: white;font-size: 50%;border-radius: 0px"><b>REVIEW DOCUMENT</b></a>
                                    </div>
                                    
                                </td>
                            </tr>  
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr> 
    <tr>
        <td bgcolor="#ffffff" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            <tr>
            <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="responsive-table">
                <tr>
                    <td>
                        <!-- COPY -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" style="padding: 10px;text-align: left !important" class="padding-copy">
                                    <?php echo $message ?>
                                </td>
                            </tr>  
                            <tr>
                                <td align="left" style="padding: 20px;text-align: left !important" class="padding-copy">
                                     <b>Do Not Share This Email</b>
                                     <br>
                                    This email contains a secure link to DocuLynx. Please do not share this email, link, or access code with others.


                                </td>
                            </tr> 
                            <tr align="center">
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://www.facebook.com/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank">
                                            <img src="https://ci6.googleusercontent.com/proxy/ZVab2CiSeSyWBbrVDqS_nJeOgI9eIpFqD1eaB59kIN-HOaEPO8FbIk40aPhqnzbQG_J9ed4TwcqFGVNIRzJ_d4BXh529lgfD9WEnfDYzOrn_YqUYqew=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-facebook.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://twitter.com/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/aMVLfllXv4IHHDoj4qyOE7penm6t2qAffzc7tW910Dj4cXY-h8D2ATiZlPOdOB4_6v41kMoKdUtSa5APzDkeEz8S7ro9wP-Th-qrirS4847tF0fHAQ=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-twitter.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>

                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://www.linkedin.com/company/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/8-FQNCsPRZjDClma6f7Na5MQiuRIMFExrUO7O1HZbCEJ5KAaNW_lN68zPqCj8W4w2Ax-98BQLhjBhYl3QLu7zu7rDs61_Wonm4TPeLnADUmGVbSDeB8=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-linkedin.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://plus.google.com/u/1/106061541017772714208" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/7fVM6H7sxKB4ye-dHYfYCa-LY4fa9qrzc6xJDbCSnhMgdRMpzkYdI48xG-FHQS4CjO9r5rxiIqMQklbXd5aOROQ1nsafYVjQq4JqtCCf2_bJfAjgh-l0Kw=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-googleplus.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://www.instagram.com/lynxcredit/" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/fXorhRTnqcqMqiFllBOoe0DXF2s_4E6FFV5GP0W6Csm-6Fzn_UqF5Nc42fgsaSSKpxhDGaJhAx7ATcTtrm2LwJ1YnAca6Vq06HHRE0p_qMtk0rSoHto1=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-instagram.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="https://www.creditlynx.com/" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/OjVKW1LwXW1B6-NI1EDI-dbiNQY9jIZ9YeKqgnZeICIeGRw8WViMCRgg2fWYq4KqRqVDV7WqEpF47Xxe0VzlmgqLjKryloTAGT52Yq17iPhrk1hLEg=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-website.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
                                <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
                                    <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                        <a href="mailto:info@creditlynx.com" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/RhEnbjXTltYwn_sCeB8DtqmerLQdm-WTjTosKzT3bLpostuqThebrGYxwZidftMnWbX4xAAlYTSoGvSWgL-FcpqzU60_6hH9VQENe2lHcDSoooc=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-email.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
                                        </a>
                                    </div>
                                </td>
                                <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;
                                </td>
                                
                            </tr>
                            <tr style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                <td align="center" style="padding: 20px;color: rgb(51,204,204)" class="padding-copy"> 
                                    Hours of Operaion <strong style="color: grey !important">|</strong> Monday - Friday <strong style="color: grey !important">|</strong> 9am-6pm
                                    <hr>
                                    

                                </td>
                                
                            </tr>
                            <tr style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
                                <td align="center" class="padding-copy">Sent to <?php echo $to ?></td>
                                 
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr> 
</table>

</body>
</html>
