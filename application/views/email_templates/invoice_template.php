<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
    <title>Credit Lynx | Invoice</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/popper/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/waves.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/dropify/dist/js/dropify.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>
    <style>






        /**/

        html {
            background: rgb(241,241,241);
        }
        body {
            background: rgb(241,241,241);
            height: 100%;
        }

        b {
            font-weight: 700;
        }

        .container {
            background-color: rgb(254,254,254);
            margin: 0 auto;
            min-width: 500px;
            max-width: 500px;
            display: block;
            padding: 0px;
            clear: both !important;
        }

        .header {
            color: rgb(210,210,210);
            background: #555555;
            padding: 10px;
            text-align: center;
            font-size: 9px;
            text-align: justify;
        }

        .top-container {
            width: 100%;
            height: 280px;
            position: relative;
            background: linear-gradient(
                     rgba(20,20,20, .8),
                     rgba(20,20,20, .8)),
                     url( 'https://admin.creditlynx.com/assets/images/background/invoice_bg.jpg');
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            text-align: center;
            color: #fff;
        }

        .welcome-containers {
            padding-top: 50px;
            width: 100%;
        }

        .btn_access_account {
            padding: 10px 25px;
            background-color: #33cccc;
            border-radius: 20px;
            color: white;
            font-size: 80%;
            border: none;
        }

        .btn_access_account:hover {
            color: #fff;
        }

        .mid-container {
            padding: 20px;
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .note-container {
            background-color: #33cccc;
            text-align: center;
            color: white;
            padding: 20px;
            font-size: 9px;
            line-height: 1;
            text-align: justify;
            border-bottom: 10px solid #00adad;
        }
        .media-container {
            background-color: rgb(241,241,241);
            text-align: center;
            color: white;
            /*padding: 20px;*/
            /*padding-bottom: 0px;*/
            font-size: 80%;
        }

        .bottom-container {
            padding: 30px;
            text-align: center;
            font-size: 9px;
            line-height: 1.5    ;
            color: #67757c !important;
            background-color: rgb(241,241,241);
            padding-top: 5px;
        }

        .logo-container {
            background-color: rgb(241,241,241);
            text-align: center;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .hoo-container {
            text-align: center;
            color: #67757c;
            background-color: rgb(241,241,241);
            padding-top: 20px;
            padding-bottom: 5px;
            font-size: 9px;
        }


    </style>
</head>
<body >
    <div class="container">
        <!-- <div class="header">

        </div> -->

        <div class="logo-container">
            <img width="100px" src="https://admin.creditlynx.com/assets/images/cl_logo.png" alt="">
        </div>
        <div class="top-container">
            <div class="welcome-containers">
                <h2 style="color: white;">Welcome <?php echo $name ?>!</h2>
                <p style="color: rgb(191,191,191);font-size: 78%;padding: 0px 50px">Thank you for signing up with Credit Lynx! <br> We are excited to be working together with you towards achieving your credit goals!</p>
                <br>
                <a href="<?php echo $href ?>" target="_blank" class="btn_access_account">ACCESS ACCOUNT</a>
            </div>
        </div>
        <div class="mid-container">
            <h5 style="color: #67757c;margin-bottom: 1rem;margin-top: 0px;text-align: center;">Your Dashboard Credentials</h5>
            <!-- <p style="color: #67757c;margin-bottom: 0px;font-size: 80%;color: #67757c">You can also access your dashboard on our website at CreditLynx.com by clicking on Client Login located at the top of the page. -->
            <!-- <br> -->
            <div style="color: #67757c;text-align: center;font-size: 70%;border: 1px solid #80808029;border-radius: 3px;padding-top: 15px;padding-bottom: 15px;">
                <!-- <b>Your account information</b> -->
                Username: <b><a style="text-decoration: none;;color: #67757c;" href="joshuasaubon@gmail.com"><?php echo $username ?></a></b>
                <br>
                Password: <b><?php echo $password ?></b>
            </div>

           <!--  <b style="font-size: 80%">Username: <a style="text-decoration: none;;color: black;" href="joshuasaubon@gmail.com">joshuasaubon@gmail.com</a> | Password: </b>
            </p> -->
        </div>
        <div class="note-container">
            Please note that this is an automated notification. This email address is not monitored and replying to this email will not reach CreditLynx's mailbox. If you have any further questions, please create a Support Ticket through your online dashboard or visit our website to contact our team.
        </div>
        <div class="hoo-container">
            Hours of Operation <strong style="color: grey !important">|</strong> Monday - Friday <strong style="color: grey !important">|</strong> 9am-6pm EST
        </div>
        <div class="media-container">
            <a href="https://www.facebook.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/ZVab2CiSeSyWBbrVDqS_nJeOgI9eIpFqD1eaB59kIN-HOaEPO8FbIk40aPhqnzbQG_J9ed4TwcqFGVNIRzJ_d4BXh529lgfD9WEnfDYzOrn_YqUYqew=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-facebook.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://twitter.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/aMVLfllXv4IHHDoj4qyOE7penm6t2qAffzc7tW910Dj4cXY-h8D2ATiZlPOdOB4_6v41kMoKdUtSa5APzDkeEz8S7ro9wP-Th-qrirS4847tF0fHAQ=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-twitter.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.linkedin.com/company/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/8-FQNCsPRZjDClma6f7Na5MQiuRIMFExrUO7O1HZbCEJ5KAaNW_lN68zPqCj8W4w2Ax-98BQLhjBhYl3QLu7zu7rDs61_Wonm4TPeLnADUmGVbSDeB8=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-linkedin.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.instagram.com/lynxcredit/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/fXorhRTnqcqMqiFllBOoe0DXF2s_4E6FFV5GP0W6Csm-6Fzn_UqF5Nc42fgsaSSKpxhDGaJhAx7ATcTtrm2LwJ1YnAca6Vq06HHRE0p_qMtk0rSoHto1=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-instagram.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.creditlynx.com/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/OjVKW1LwXW1B6-NI1EDI-dbiNQY9jIZ9YeKqgnZeICIeGRw8WViMCRgg2fWYq4KqRqVDV7WqEpF47Xxe0VzlmgqLjKryloTAGT52Yq17iPhrk1hLEg=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-website.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="mailto:info@creditlynx.com" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/RhEnbjXTltYwn_sCeB8DtqmerLQdm-WTjTosKzT3bLpostuqThebrGYxwZidftMnWbX4xAAlYTSoGvSWgL-FcpqzU60_6hH9VQENe2lHcDSoooc=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-email.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
        </div>
        <div class="bottom-container">
            Credit Lynx
            <br>
            This email was sent by Credit Lynx to <a style="color: #67757c;text-decoration: none;" href="<?php echo $to ?>"><?php echo $to ?></a>
            <br>
            <a style="text-decoration: none;" href="https://admin.creditlynx.com/emailCampaign/Unsubscribe" target="_blank">Unsubscribe</a>
            <br>
            Copyright &copy; 2019 Credit Lynx, All Rights Reserved
        </div>
    </div>
</body>
</html>