<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
	<style>
		* {

		}
		/* CLIENT-SPECIFIC STYLES */
	    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
	    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
	    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

	    /* RESET STYLES */
	    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
	    table{border-collapse: collapse !important;}
	    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

	    /* iOS BLUE LINKS */
	    a[x-apple-data-detectors] {
	        color: inherit !important;
	        text-decoration: none !important;
	        font-size: inherit !important;
	        font-family: inherit !important;
	        font-weight: inherit !important;
	        line-height: inherit !important;
	    }
	</style>
</head>
<body > 
	
	<div style="display: block;text-align: center;">
		<a href="#">
			<img alt="" title="" width="650px" src="<?php echo base_url('assets/images/autoresponders_images/birthday_image.png') ?>">
		</a>
	</div> 
	<br> 
	<section style="max-width: 650px;margin: auto;">
		<div style="padding: 20px 20px 40px 20px">
			<div class="">
				<div style="font-size: 10px; line-height: 1.5;" data-line-height="1.5" class=""> <span class="" style="font-size: 10px;"> </span><p style="text-align: center;" class=""><span class="" style="font-size: 47px; color: rgb(51, 204, 204); ">Happy Birthday</span></p><p style="text-align: center;" class=""><br style="font-size: 17px;" class=""></p><p style="text-align: center;" class=""></p><br class=""><div align="center" style="text-align: center; font-size: 12px; color: rgb(85, 85, 85);" class=""><span style="color: rgb(85, 85, 85); " class="">We are thinking of you on this important day and hoping it is filled with happiness. Wishing you many joyous years ahead! We value our customer relationships and thank you for being our client. We look forward to continuing our partnership in the coming year.</span></div><span style=" font-size: 12px; color: rgb(85, 85, 85);" class=""><br style="color: rgb(85, 85, 85);" class=""><br style="color: rgb(85, 85, 85);" class=""></span><div style="text-align: center; font-size: 12px; color: rgb(85, 85, 85);" class=""><span style="color: rgb(85, 85, 85); text-align: inherit; " class="">All the best to you and your family</span></div> </div>
			</div>
		</div> 
		<hr>
		<div style="display: block;text-align: center;margin-bottom: 50px;padding-top: 50px">
			<a href="http://creditlynx.com" target="_blank">
				<img width="271px"  src="<?php echo base_url('assets/images/LynxImage.png') ?>">	
			</a>
		</div>
		<div >
			<center style="margin:0;outline:none; padding:0;font-size:0px;">
	            <table class="_ac_social_table" cellspacing="0" cellpadding="0" align="center" style="min-width:auto!important;margin:auto!important;display:inline-block!important;font-size:0;text-align:center!important;">
	            	<tbody>
	            		<tr>
	            			<tr align="center">
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://www.facebook.com/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank">
	                                        <img src="https://ci6.googleusercontent.com/proxy/ZVab2CiSeSyWBbrVDqS_nJeOgI9eIpFqD1eaB59kIN-HOaEPO8FbIk40aPhqnzbQG_J9ed4TwcqFGVNIRzJ_d4BXh529lgfD9WEnfDYzOrn_YqUYqew=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-facebook.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://twitter.com/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/aMVLfllXv4IHHDoj4qyOE7penm6t2qAffzc7tW910Dj4cXY-h8D2ATiZlPOdOB4_6v41kMoKdUtSa5APzDkeEz8S7ro9wP-Th-qrirS4847tF0fHAQ=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-twitter.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>

	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://www.linkedin.com/company/creditlynx" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/8-FQNCsPRZjDClma6f7Na5MQiuRIMFExrUO7O1HZbCEJ5KAaNW_lN68zPqCj8W4w2Ax-98BQLhjBhYl3QLu7zu7rDs61_Wonm4TPeLnADUmGVbSDeB8=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-linkedin.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://plus.google.com/u/1/106061541017772714208" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/7fVM6H7sxKB4ye-dHYfYCa-LY4fa9qrzc6xJDbCSnhMgdRMpzkYdI48xG-FHQS4CjO9r5rxiIqMQklbXd5aOROQ1nsafYVjQq4JqtCCf2_bJfAjgh-l0Kw=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-googleplus.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://www.instagram.com/lynxcredit/" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/fXorhRTnqcqMqiFllBOoe0DXF2s_4E6FFV5GP0W6Csm-6Fzn_UqF5Nc42fgsaSSKpxhDGaJhAx7ATcTtrm2LwJ1YnAca6Vq06HHRE0p_qMtk0rSoHto1=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-instagram.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="https://www.creditlynx.com/" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/OjVKW1LwXW1B6-NI1EDI-dbiNQY9jIZ9YeKqgnZeICIeGRw8WViMCRgg2fWYq4KqRqVDV7WqEpF47Xxe0VzlmgqLjKryloTAGT52Yq17iPhrk1hLEg=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-website.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;</td>
	                            <td align="center" valign="middle" width="34" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:34px">
	                                <div class="" style="margin:0px;padding:0px;border:0px none;outline:none;font-family:inherit">
	                                    <a href="mailto:info@creditlynx.com" style="color:rgb(51,204,204);outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;display:block" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/RhEnbjXTltYwn_sCeB8DtqmerLQdm-WTjTosKzT3bLpostuqThebrGYxwZidftMnWbX4xAAlYTSoGvSWgL-FcpqzU60_6hH9VQENe2lHcDSoooc=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-email.png" border="0" width="34" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;display:block" >
	                                    </a>
	                                </div>
	                            </td>
	                            <td width="10" class="" style="padding:0px;border:0px none;outline:none;font-family:inherit;border-collapse:collapse;overflow:visible;text-align:inherit;line-height:inherit;display:inline-block;width:10px">&nbsp;
	                            </td>
	                            
	                        </tr>
	                         
	            		</tr>
			</tbody></table>
			
	        </center>
		</div>
		<div style="text-align: center;padding: 20px;padding-bottom: 0px;color: rgb(51,204,204)"> 
	            Hours of Operaion <strong style="color: grey !important">|</strong> Monday - Friday <strong style="color: grey !important">|</strong> 9am-6pm
	            <hr> 
		</div>
		<div style="text-align: center;color: grey"> 
	             Sent to <?php echo $to ?>
		</div>
	</section>
		
		
		 
	
</body>
</html>