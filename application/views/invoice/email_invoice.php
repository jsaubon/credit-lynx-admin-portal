<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
    <title>Credit Lynx | Invoice</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/popper/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/waves.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/dropify/dist/js/dropify.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>
    <style>






        /**/

        html {
            background: white;
        }
        body {
            background: white;
            height: 100%;
        }

        b {
            font-weight: 700;
        }

        .container {
            background-color: rgb(254,254,254);
            margin: 0 auto;
            min-width: 500px;
            max-width: 500px;
            display: block;
            padding: 0px;
            clear: both !important;
        }

        .top_container { 
            background-color: rgb(241,241,241);
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            font-size: 70%;
            margin: 10px;
        }

        .mid_container {
            background-image: url('https://admin.creditlynx.com/assets/images/invoice/invoice_bg.png');
            background-size: cover;
            padding: 30px 20px;
        }

        .inner_mid_container { 
            padding: 20px;
            color: white;
            background-color: white;
            border-radius: 10px;
            text-align: center;
            border: 1px solid #dddddd;


        }


        .inner_mid_container h2 {
            margin-top: 15px;
            color: #455a64;
            font-weight: 300;
        }

        .message {
            color: darkgray;
            margin-top: 20px;
            margin-bottom: 30px;
            font-size: 80%;
        }

        .btn_link_invoice { 
            border: none;
            background: #3cc;
            color: white !important;
            border-radius: 15px;
            padding-left: 70px;
            padding-right: 70px;
            padding-top: 10px;
            padding-bottom: 10px; 
            text-decoration: none;
        }

        .btn_link_invoice:hover {
            color: white !important;
        }

        .image_container {
            padding-top: 45px;
            padding-bottom: 15px;
        }

        .hoo-container {
            text-align: center;
            color: #ffffff;
            background-color: #3cc;
            padding-top: 20px;
            padding-bottom: 5px;
            font-size: 9px;
            margin-top: 20px;
        }

        .media-container {
            background-color: #3cc;
            text-align: center;
            color: white;
            /*padding: 20px;*/
            /*padding-bottom: 0px;*/
            font-size: 80%;
            padding-top: 10px;
            padding-bottom: 15px;
            border-bottom: 7px solid #00adad;
        }

        .bottom-container {
            padding: 30px;
            text-align: center;
            font-size: 9px;
            line-height: 1.5    ;
            color: #67757c !important;
            background-color: white;
            padding-top: 5px;
        }

       a {
        color: #3cc;
       }

       a:hover {
        color: #3cc;
       }

       .note-container {
            background-color: #3cc;
            text-align: center;
            color: white;
            padding: 20px;
            font-size: 9px;
            line-height: 1;
            text-align: justify;
            border-bottom: 7px solid #00adad;
        }

    </style>
</head>
<body >
    <div class="container">

        <!-- <div class="note-container">
            Please note that this is an automated notification. This email address is not monitored and replying to this email will not reach CreditLynx's mailbox. If you have any further questions, please create a Support Ticket through your online dashboard or visit our website to contact our team.
        </div> -->
        <div class="top_container">
            <small >Hello, <a href="<?php echo $to?>" style="color: #455a64;text-decoration: none;" ><?php echo $to?></a></small>
        </div>
        <div class="mid_container">
            <div class="inner_mid_container ">
                <h2>Here's your invoice</h2>
                <p class="message">Credit Lynx sent you an invoice for $<?php echo $amount?> USD
                    <br>
                    <br>
                    Due on receipt 
                </p>

                <a href="<?php echo $href ?>" target="_blank" class="btn_link_invoice">View and Pay Invoice</a>
                <div class="image_container">
                    <img src="https://admin.creditlynx.com/assets/images/LynxLogo.png" width="50px">
                </div> 
            </div>
            <div class="hoo-container">
                Hours of Operation <strong style="color: white !important">|</strong> Monday - Friday <strong style="color: white !important">|</strong> 9am-6pm EST
            </div>
            <div class="media-container">
                <a href="https://www.facebook.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/ZVab2CiSeSyWBbrVDqS_nJeOgI9eIpFqD1eaB59kIN-HOaEPO8FbIk40aPhqnzbQG_J9ed4TwcqFGVNIRzJ_d4BXh529lgfD9WEnfDYzOrn_YqUYqew=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-facebook.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
                <a href="https://twitter.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/aMVLfllXv4IHHDoj4qyOE7penm6t2qAffzc7tW910Dj4cXY-h8D2ATiZlPOdOB4_6v41kMoKdUtSa5APzDkeEz8S7ro9wP-Th-qrirS4847tF0fHAQ=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-twitter.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
                <a href="https://www.linkedin.com/company/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/8-FQNCsPRZjDClma6f7Na5MQiuRIMFExrUO7O1HZbCEJ5KAaNW_lN68zPqCj8W4w2Ax-98BQLhjBhYl3QLu7zu7rDs61_Wonm4TPeLnADUmGVbSDeB8=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-linkedin.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
                <a href="https://www.instagram.com/lynxcredit/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/fXorhRTnqcqMqiFllBOoe0DXF2s_4E6FFV5GP0W6Csm-6Fzn_UqF5Nc42fgsaSSKpxhDGaJhAx7ATcTtrm2LwJ1YnAca6Vq06HHRE0p_qMtk0rSoHto1=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-instagram.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
                <a href="https://www.creditlynx.com/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/OjVKW1LwXW1B6-NI1EDI-dbiNQY9jIZ9YeKqgnZeICIeGRw8WViMCRgg2fWYq4KqRqVDV7WqEpF47Xxe0VzlmgqLjKryloTAGT52Yq17iPhrk1hLEg=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-website.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
                <a href="mailto:info@creditlynx.com" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/RhEnbjXTltYwn_sCeB8DtqmerLQdm-WTjTosKzT3bLpostuqThebrGYxwZidftMnWbX4xAAlYTSoGvSWgL-FcpqzU60_6hH9VQENe2lHcDSoooc=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-email.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
                </a>
            </div>
            <div class="bottom-container"> 
                <br> 
                This email was sent by Credit Lynx to <a style="color: #67757c;text-decoration: none;" href="<?php echo $to?>"><?php echo $to?></a>
                <br>
                if you prefer not to receive further emails from Credit Lynx, <a style="text-decoration: none;" href="https://admin.creditlynx.com/emailCampaign/Unsubscribe" target="_blank">click here to unsubscribe</a> or write us at <a href="#">228 Mill Street, Suite 200 Milford, Ohio 45150</a>. Please review our <a href="#">Privacy Policy</a> for Details. Our email practices and procedures are fully compliant with the CAN-SPAM Act of 2003.
                <br><br>
                Copyright &copy; 2019 Credit Lynx, All Rights Reserved
            </div>
        </div>
    </div>
</body>
</html>