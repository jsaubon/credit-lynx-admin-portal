<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
    <title>Credit Lynx | Invoice</title> 
    <style>






        /**/

        html {
            background: white;
        }
        body {
            background: white;
            height: 100%;
            font-family: "Poppins", sans-serif;
        }

        b {
            font-weight: 700;
        }



        .container {
            background-color: rgb(254,254,254);
            margin: 0 auto;
            min-width: 600px;
            max-width: 600px;
            display: block;
            padding: 0px;
            clear: both !important;
        }

        .top_container { 
            background-color: white;
            padding-top: 10px;
            padding-left: 20px;
            padding-bottom: 10px;
            font-size: 70%; 
        }

        .mid_container {
            background-color: rgb(241,241,241); 
            padding: 20px 20px;
            color: #66757c;
            font-size: 10px;
        }
  
        .image_container {
            padding-top: 45px;
            padding-bottom: 15px;
        }

        .hoo-container {
            text-align: center;
            color: #ffffff;
            background-color: #3cc;
            padding-top: 20px;
            padding-bottom: 5px;
            font-size: 9px;
            margin-top: 10px;
        }

        .media-container {
            background-color: #3cc;
            text-align: center;
            color: white;
            /*padding: 20px;*/
            /*padding-bottom: 0px;*/
            font-size: 80%;
            padding-top: 10px;
            padding-bottom: 15px;
            border-bottom: 7px solid #00adad;
        }

        .bottom-container {
            padding: 30px;
            text-align: center;
            font-size: 9px;
            line-height: 1.5    ;
            color: #67757c !important;
            background-color: white;
            padding-top: 5px;
        }

       a {
        color: #66757 !important;
       }

       a:hover {
        color: #66757 !important;
       }

       .bottom-container a {
        color: #3cc !important;
       }

       .bottom-container a:hover {
        color: #3cc !important;
       }

       .note-container {
            background-color: #3cc;
            text-align: center;
            color: white;
            padding: 20px;
            font-size: 9px;
            line-height: 1;
            text-align: justify;
            border-bottom: 7px solid #00adad;
        }

        .pull-left {
            float: left;
        }

        .pull-right {
            float: right;
        }

        .text-left {
            text-align: left;
        }

        .item_list {
            /*padding: 10px;*/
            margin-top: 10px;
            font-size: 10px;
        }

        .table_item_list {
            width: 100%; 
            font-size: 10px;
        }

       

        .table_item_list tr th, .table_item_list tr td {
            padding: 10px 10px;
        }


        .b-b {
            border-bottom: 1px solid #f3f1f1;
        }
 
        .b-r {
            border-right: 1px solid #f3f1f1;   
        }

        .table_item_list tbody, .table_item_list tfoot {
            border-bottom: 1px solid #f3f1f1;
        }
 

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .p-r-10 {
            padding-right: 10px;
        }

        .text-center {
            text-align: center;
        }

        .row {
            display: flex;
        }

        .col-5 {
            width: 50%;
            padding: 10px;
        }

        .col-2-5 {
            width: 25%;
            padding: 10px;
        }

        .thead {
            background-color: #3cc;
            color: white;
        }

        .tbody, .tfoot {
            color: #67757c
        }

        @media (max-width: 480px) {
            .tbody .col-5 {
                line-height: 10px;
            }

            .container {
                min-width: 500px;
                max-width: 500px;
            }
        }
        .tbody .col-2-5 {
            line-height: 24px;
        }

    </style>
</head>
<body >
    <div class="container">

        <?php
        if (count($month_lates) > 0) {
            $late_fee = number_format(15 * count($month_lates),2);
        } else {
            $late_fee = 0;
        }
            $sub_total = number_format((count($month_list) * $monthly_fee) + $late_fee,2);
        
        ?>
        <!-- <div class="note-container">
            Please note that this is an automated notification. This email address is not monitored and replying to this email will not reach CreditLynx's mailbox. If you have any further questions, please create a Support Ticket through your online dashboard or visit our website to contact our team.
        </div> -->
        <div class="top_container">
            <div class="text-left" style="padding-top: 5px;">
                <img src="<?php echo base_url('assets/images/LynxLogo.png')?>" width="35px">  
                <label class="pull-right" style="margin-right: 20px;margin-bottom: 0px;font-size: 23px;line-height: 45px;color: #67757c">Receipt</label>   
            </div>
            
        </div>
        <div class="mid_container" style="display: flex;"> 
            <div class="text-center" style="width: 25%">
                SERVICES FOR
                <br><b><?php echo $name ?></b>
            </div>
            <div class="text-center" style="width: 25%">
                AMOUNT PAID
                <br><b style="color: #67757;text-decoration: none;">$<?php echo $sub_total;?></b> 
            </div>
            <div class="text-center" style="width: 25%">
                DATE PAID
                <br><b><?php echo date('F d, Y');?></b>
            </div>
            <div class="text-center" style="width: 25%">
                PAYMENT METHOD
                <br><b><?php echo $payment_method ?></b> - <?php echo $card_number ?>
            </div>
        </div>
        <div class="item_list">
            <div class="row thead">
                 <div class="col-5">
                     Program Details
                 </div>
                 <div class="col-2-5 text-center">
                     Quantity
                 </div>
                 <div class="col-2-5 text-center">
                     Total
                 </div>
            </div>
            <?php foreach ($month_list as $key => $value): ?>
                <div class="row tbody b-b">
                     <div class="col-5">
                        <b><?php echo $plan ?></b><br>
                        <?php echo $value ?> Service 
                        <?php if ($value != date('F')): ?>
                            Overdue
                        <?php endif ?>
                        <?php if ($value == date('F')): ?>
                            <?php if ($no_overdue_date != date('d')): ?>
                                Overdue
                            <?php endif ?>
                        <?php endif ?> 
                     </div>
                     <div class="col-2-5 text-center">
                         1
                     </div>
                     <div class="col-2-5 text-center">
                         $<?php echo number_format($monthly_fee,2) ?></b>
                     </div>
                </div>
            <?php endforeach ?>
            <?php if (count($month_lates) > 0): ?>
                <div class="row tbody b-b">
                     <div class="col-5">
                        <b>Late Fees</b><br>
                        Overdue <?php echo implode(', ', $month_lates); ?>
                     </div>
                     <div class="col-2-5 text-center">
                         <?php echo count($month_lates) ?>
                     </div>
                     <div class="col-2-5 text-center">
                         <b>$<?php echo $late_fee ?></b>
                     </div>
                </div>
            <?php endif ?>
                
            <div class="row tfoot b-b">
                 <div class="col-5"> 
                 </div>
                 <div class="col-2-5 text-center b-r">
                     Amount Paid
                 </div>
                 <div class="col-2-5 text-center">
                     <b>$<?php echo $sub_total ?></b>
                 </div>
            </div>
                
             
        </div>
        <div class="hoo-container">
            Hours of Operation <strong style="color: white !important">|</strong> Monday - Friday <strong style="color: white !important">|</strong> 9am-6pm EST
        </div>
        <div class="media-container">
            <a href="https://www.facebook.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/ZVab2CiSeSyWBbrVDqS_nJeOgI9eIpFqD1eaB59kIN-HOaEPO8FbIk40aPhqnzbQG_J9ed4TwcqFGVNIRzJ_d4BXh529lgfD9WEnfDYzOrn_YqUYqew=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-facebook.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://twitter.com/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/aMVLfllXv4IHHDoj4qyOE7penm6t2qAffzc7tW910Dj4cXY-h8D2ATiZlPOdOB4_6v41kMoKdUtSa5APzDkeEz8S7ro9wP-Th-qrirS4847tF0fHAQ=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-twitter.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.linkedin.com/company/creditlynx" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/8-FQNCsPRZjDClma6f7Na5MQiuRIMFExrUO7O1HZbCEJ5KAaNW_lN68zPqCj8W4w2Ax-98BQLhjBhYl3QLu7zu7rDs61_Wonm4TPeLnADUmGVbSDeB8=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-linkedin.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.instagram.com/lynxcredit/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/fXorhRTnqcqMqiFllBOoe0DXF2s_4E6FFV5GP0W6Csm-6Fzn_UqF5Nc42fgsaSSKpxhDGaJhAx7ATcTtrm2LwJ1YnAca6Vq06HHRE0p_qMtk0rSoHto1=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-instagram.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="https://www.creditlynx.com/" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/OjVKW1LwXW1B6-NI1EDI-dbiNQY9jIZ9YeKqgnZeICIeGRw8WViMCRgg2fWYq4KqRqVDV7WqEpF47Xxe0VzlmgqLjKryloTAGT52Yq17iPhrk1hLEg=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-website.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
            <a href="mailto:info@creditlynx.com" style="color:transparent;outline:none;margin:0px;padding:0px;border:0px none;font-family:inherit;" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/RhEnbjXTltYwn_sCeB8DtqmerLQdm-WTjTosKzT3bLpostuqThebrGYxwZidftMnWbX4xAAlYTSoGvSWgL-FcpqzU60_6hH9VQENe2lHcDSoooc=s0-d-e1-ft#http://creditlynx.img-us3.com/_social_/flat-dark-round-email.png" border="0" width="24" style="border:none;margin:0px;padding:0px;outline:none;font-family:inherit;text-decoration-line:none;" >
            </a>
        </div>
        <div class="bottom-container"> 
            <br> 
            This email was sent by Credit Lynx to <a style="color: #67757c;text-decoration: none;" href="<?php echo $to?>"><?php echo $to?></a>
            <br>
            if you prefer not to receive further emails from Credit Lynx, <a style="text-decoration: none;" href="https://admin.creditlynx.com/emailCampaign/Unsubscribe" target="_blank">click here to unsubscribe</a> or write us at <a href="#">228 Mill Street, Suite 200 Milford, Ohio 45150</a>. Please review our <a href="#">Privacy Policy</a> for Details. Our email practices and procedures are fully compliant with the CAN-SPAM Act of 2003.
            <br><br>
            Copyright &copy; 2019 Credit Lynx, All Rights Reserved
        </div>
    </div>
</body>
</html>