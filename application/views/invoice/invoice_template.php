<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
    <title>Credit Lynx | Invoice</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet" type="text/css">


    <link href="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/popper/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/waves.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/dropify/dist/js/dropify.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables/datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sweetalert/sweetalert.min.js') ?>"></script>
     <script src="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>
<style>
    .overlay{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 10;
        background-color: rgba(0,0,0,0.2); /*dim the background*/
        text-align: center;
        padding-top: 300px;
    }
 
 
@media (max-width: 767px) {  

      .p-l-5 {
          padding-left: 15px !important;
      }

      .p-r-5 {
          padding-right: 15px !important;
      }

      .p-r-0 {
          padding-right: 15px !important;
      }

      .div_images {
        display: none;
      }

      .row small {
        position: absolute;
        right: 22px !important;
        top: 5px;
        font-size: 55%;
      } 
}
      #form_billing_inputs {
           padding: 20px;
      }
       
      .div_images {
           float: right;  
      }
      .div_dc {
           border: 1px solid #ddd !important;
           padding: 5px;
      }
      .div_cc {
           border-left: 1px solid #ddd !important;
           border-right: 1px solid #ddd !important;
           border-bottom: 1px solid #ddd !important;
           padding: 5px;
      }
      .p-r-5 {
           padding-right: 5px;
      }
      .p-l-5 {
           padding-left: 5px;
      }
      .btnSameAdd {
           padding: 20px 40px 15px;
           background-color: #3cc;
           border: #3cc;
           color: #fff;
      }
      .btnSameAdd .fa {
           font-size: 2em;
      }
      .divBillingInputs input, .divBillingInputs select{
           font-size: 70%;
      }

      .pm_choice {
           cursor: pointer;
      }

       
      .fieldInvalid {
          border: 2px solid red !important;
          height: 47px !important;
          padding-top: 22px !important;
      }

      .row small {
          position: absolute;
          right: 22px;
          top: 5px;
          font-size: 55%;
      }

      .btn-card-choice {

      }

      .btn-card-choice, .btn-card-choice.disabled {
           background: #ffffff;
           border: 1px solid #26c6da;
           color: #26c6da;
      }

      .btn-card-choice:not(:disabled):not(.disabled).active, .btn-card-choice:not(:disabled):not(.disabled):active, .show>.btn-card-choice.dropdown-toggle {
           color: #fff;
           background-color: #26c6da;
           border-color: #26c6da;
      }

      .btn-secondary.active, .btn-secondary.disabled.active, .btn-secondary.disabled:focus, .btn-secondary:focus { 
           background: #26c6da;
           -webkit-box-shadow: none;
           box-shadow: none;
           color: #ffffff;
      }

      .btn_check_agreement { 
          width: 25px !important;
          padding: 0px !important;
          height: 25px !important;
      }

      .btn_check_agreement_focused {
           border: 1px solid #fc4b6c !important;
           -webkit-box-shadow: 0 2px 2px 0 rgba(239, 83, 80, 0.14), 0 3px 1px -2px rgba(239, 83, 80, 0.2), 0 1px 5px 0 rgba(239, 83, 80, 0.12);
          box-shadow: 0 2px 2px 0 rgba(239, 83, 80, 0.14), 0 3px 1px -2px rgba(239, 83, 80, 0.2), 0 1px 5px 0 rgba(239, 83, 80, 0.12);
          -webkit-transition: 0.2s ease-in;
          -o-transition: 0.2s ease-in;
          transition: 0.2s ease-in;
      }






        /**/

        html {
            background: rgb(140,144,145);
        }
        body {
            background: rgb(140,144,145);
            height: 100%;
        }

        b {
            font-weight: 700;
        }

        .container {
            background-color: #fff;
            margin: 0 auto;
            min-width: 800px;
            max-width: 800px;
            display: block;
            padding: 0px;
            box-shadow: 5px 5px 5px rgb(78,82,85);
        }

        .top-container {
            background-color: rgb(78,82,85);
            height: 140px;
        }

        .top-container .LynxLogo {
            vertical-align: middle;
        }

        .top-container .col-3 {
            height: 140px;
            text-align: center;
        }

        .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        .top-container .col-2,.top-container .col-3,.top-container .col-4{
            padding: 0px !important;
        }

        .mid-container {
            padding: 20px;
            position: relative;
        }

        .invoice-ribbon {
            right: 0px;
            height: auto;
            padding: 5px;
            top: -24px;
            padding-right: 75px;
            padding-left: 37px;
        }

        .invoice-ribbon h1{
            color: #fff;
            margin: 0px;
            font-weight: 100;
            font-size: 30px;
            margin-left: 45px;
        }

        .invoice-ribbon:before {
            border-right-color: #26c6da;
            border-left-color: transparent;
            right: 100%;
            left: auto;
            position: absolute;
            top: 0px;
            display: block;
            width: 0;
            height: 0;
            content: '';
            border: 46px solid #26c6da;
            border-left: 0px solid transparent;
            border-top: 24px solid transparent;
            border-bottom: 25.5px solid transparent;
        }


        .table-ribbon {
            /*border: 1px solid #fff;*/
            padding-right: 100px;
            padding-left: 40px;
            background-color: rgb(78,82,85);
            color: #fff;
        }
        .table-ribbon:before {
            border: 18px solid rgb(78,82,85);
            border-right-color: rgb(78,82,85);
            border-left-color: transparent;
            border-color: rgb(78,82,85);
            border-right: 0px solid rgb(78,82,85);
            border-left: 30px solid transparent;
            position: absolute;
            display: block;
            width: 0;
            height: 0;
            content: '';
            top: 185px;
            right: 385px;
        }

        .table-ribbon2 {
            /*border: 1px solid #fff;*/
            padding-right: 100px;
            padding-left: 40px;
            background-color: rgb(140,144,145);
            color: #fff;
        }
        .table-ribbon2:before {
            border: 18px solid rgb(140,144,145);
            border-right-color: rgb(140,144,145);
            border-left-color: transparent;
            border-color: rgb(140,144,145);
            border-right: 0px solid rgb(140,144,145);
            border-left: 30px solid transparent;
            position: absolute;
            display: block;
            width: 0;
            height: 0;
            content: '';
            top: 185px;
            right: 136px;
        }
        .table-ribbon-footer {
            /*border: 1px solid #fff;*/
            padding-right: 100px;
            padding-left: 40px;
            background-color: rgb(78,82,85);
            color: #fff;
        }
        .table-ribbon-footer2 {
            /*border: 1px solid #fff;*/
            padding-right: 100px;
            padding-left: 40px;
            background-color: rgb(78,82,85);
            color: #fff;
        }

        .table-ribbon-footer:before {
            border-right-color: rgb(78,82,85);
            border-left-color: transparent;
            right: 385px;
            position: absolute;
            top: <?php echo (304 + (count($month_lates) > 0 ? 43 : 0) + (43 * (count($month_list)))).'px' ?> ;
            display: block;
            width: 0;
            height: 0;
            content: '';
            border: 42px solid rgb(78,82,85);
            border-left: 0px solid transparent;
            border-top: 18px solid transparent;
            border-bottom: 18px solid transparent;
        }

        .invoice-table {
            margin-right: -20px;
            margin-left: -20px;
        }

        .invoice-table .table td,.invoice-table  .table th {
            padding: 0px;
        }
        .invoice-table .table tbody tr td:nth-child(1) {
            padding-left: 60px !important;
        }
        .invoice-table .table thead tr th:nth-child(1) {
            padding-left: 60px !important;
        }
        .invoice-table .table thead tr th:nth-child(1) div {
            padding-left: 0px !important;
        }

        .invoice-table tbody tr {
            line-height: 1;
        }
        .invoice-table tbody tr td{
            vertical-align: middle;
            padding: 10px !important;
        }
        .invoice-table tbody tr td:nth-child(3){
            text-align: center;
            padding-right: 20px !important;
        }
        .invoice-table tbody tr td:nth-child(2){
            text-align: center;
            padding-right: 20px !important;
        }
        .invoice-table tbody tr td:nth-child(1){
            padding-left: 20px;
        }

        .form-control {
            min-height: 28px;
            font-size: 60%;
        }

        .input-group-text {
            font-size: 60%;
        }

        .input-group {
            /*margin-top: 5px;*/
        }

        .bot-container {
            padding: 0px 15px;
        }

        .bot-container .col-3 {
            padding: 15px;
            border-top: 5px solid #26c6da;
        }
        .bot-container .col-9 {
            padding: 15px;
            border-top: 5px solid rgb(78,82,85);
        }
        .bot-container .col-9 h3 {
            line-height: 1;
        }

        .invoice-table thead th , .invoice-table tbody tr td, .invoice-table tfoot tr th {
            font-size: 70%;
        }

        .top-container table tr td {
            padding-bottom: 34px;
            font-size: 65%;
        }

        @media (max-width: 768px) {
            .table-ribbon-footer:before {
                top: 405px;
            }
        }

        .pay-container{
            padding: 0 40px;
        }

        .bot-container h3 {
            padding-left: 45px;
        }

        .terms-container {
            padding-left: 40px;
            padding-right: 40px;
            font-size: 65%;
            margin-top: 20px;
        }

        .terms-container h5{
            margin-bottom: 0px;
        }


        .rightLabelDiv {
            position: relative;
        }

        .rightLabel {
            position: absolute;
            top: 4px;
            right: 5px;;
            font-size: 8px;
        }

        .p-r-5 {
            padding-right: 5px;
        }

        .p-l-5 {
            padding-left: 5px;
        }

        .current-address {
            font-size: 65%;
            margin-top: 20px;
            width: 100%;
        }

        .address-buttons {
            text-align: center;
            margin-bottom: 20px;
        }

        .address-buttons button {
            padding: 15px 30px;
        }
        .address-buttons i {
            font-size: 22px;
        }

        .btn-success:not(:disabled):not(.disabled).active, .btn-success:not(:disabled):not(.disabled):active, .show>.btn-success.dropdown-toggle {
            background-color: #1096a7;
            border-color: #63d6e4;
        }

        .btn-success:not(:disabled):not(.disabled).active:focus, .btn-success:not(:disabled):not(.disabled):active:focus, .show>.btn-success.dropdown-toggle:focus {
            box-shadow: 0 0 0 0.2rem #34d2e685;
        }

        .btn-pay-later {
            background-color: rgb(51,51,51);
            color: #fff;
            border-radius: 0px;
            font-size: 23px;
            margin-top: 3px;
            margin-left: -3px;
        }

        .btn-pay-later:after {
            border-right-color: rgb(51, 51, 51);
            border-left-color: transparent;
            left: 344px;
            position: absolute;
            top: 3px;
            display: block;
            width: 0;
            height: 0;
            content: '';
            border: 47px solid rgb(51, 51, 51);
            border-right: 0px solid transparent;
            border-top: 24.5px solid transparent;
            border-bottom: 24.5px solid transparent;
        }
        .btn-pay-later:before {
            left: 168px;
            top: 3px;
            border: 24.8px solid rgb(51, 51, 51);
            border-right-color: rgb(51, 51, 51);
            border-left-color: transparent;
            border-color: rgb(51, 51, 51);
            border-right: 0px solid rgb(51, 51, 51);
            border-left: 46px solid transparent;
            position: absolute;
            display: block;
            width: 0;
            height: 0;
            content: '';
        }
        .btn-pay-now {
            background-color: #26c6da;
            color: #fff;
            border-radius: 0px;
            margin-right: 43px;
            font-size: 23px;
            padding-left: 58px;
            margin-top: 3px;
        }

        .btn-pay-now:after {
            border-right-color: #26c6da;
            border-left-color: transparent;
            left: 169px;
            position: absolute;
            top: 3px;
            display: block;
            width: 0;
            height: 0;
            content: '';
            border: 46px solid #26c6da;
            border-right: 0px solid transparent;
            border-top: 24.5px solid transparent;
            border-bottom: 24.5px solid transparent;
        }
        /*.btn-pay-later:not(:disabled):not(.disabled).active, .btn-pay-later:not(:disabled):not(.disabled):active, .show>.btn-pay-later.dropdown-toggle {
            background-color: #1c282d;
            border-color: #101619;
        }
        .btn-pay-later:not(:disabled):not(.disabled).active:focus, .btn-pay-later:not(:disabled):not(.disabled):active:focus, .show>.btn-pay-later.dropdown-toggle:focus {
            box-shadow: 0 0 0 0.2rem #2632388c;
        }

        .btn-pay-later:focus {
            box-shadow: 0 0 0 0.2rem #2632388c;
        }*/

        .pay_buttons {
            position: absolute;
            left: 0px;
            top: -27px;
        }

        .h-30 {
            height: 30px !important;
        }

        .hide {
            display: none !important;
        }

        .swal2-icon.swal2-warning {
            border-color: #26c6da !important;
            color: #26c6da !important;
        }

        .swal2-popup .swal2-styled.swal2-confirm {
            background-color: #21b3c6 !important;
            border-left-color: #21b3c6 !important;
            border-right-color: #21b3c6 !important
        }

        .swal2-icon.swal2-success .swal2-success-ring {
            border: 0.25em solid rgba(91, 214, 226, 0.3);
        }

        .swal2-icon.swal2-success [class^='swal2-success-line'] {
            background-color: #3cc;
        }

        .swal2-icon.swal2-success [class^='swal2-success-line'] {
            background-color: #3cc;
        }
</style>
</head>
<body >
    <input type="hidden" name="" value="<?php echo $session ?>" id="session_id">
    <div class="overlay hide">
        <img style="width: 400px" src="<?php echo base_url('assets/images/paid.png') ?>">
    </div>
    <div class="container">
        <div class="top-container">
            <div class="row">
                <div class="col-3">
                    <span class="helper"></span><img class="LynxLogo img-center" src="<?php echo base_url('assets/images/LynxLogo.png') ?>" width="70px" alt="">
                </div>
                <div class="col-2">
                    <table style="color: #fff;height: 140px;">
                        <tr>
                            <td><i class="fas fa-mobile-alt fa-lg p-r-10"></i> (877) 820-2425</td>
                        </tr>
                    </table>
                </div>
                <div class="col-3">
                    <table style="color: #fff;height: 140px;width: 100%">
                        <tr>
                            <td style="padding-left: 30px;"><i class="fas fa-map-marker-alt fa-lg"></i></td>
                            <td class="text-left p-l-10">
                                <address style="margin-bottom: 0px;">
                                    228 Mill Street
                                    Suite 200 <br>
                                    Milford, Ohio 45150
                                </address>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-4">
                    <table style="color: #fff;height: 140px;;width: 100%;">
                        <tr class="text-center">
                            <td>
                                <i class="fas fa-envelope fa-lg p-r-10"></i> <a style="color: white;text-decoration: none;" href="mailto:billing@creditlynx.com" target="_blank">billing@creditlynx.com</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="mid-container">
            <div class="ribbon ribbon-success ribbon-right invoice-ribbon"><h1>I N V O I C E</h1></div>
            <div class="pay_buttons">
                <button class="btn btn-pay-now">Pay Now</button>
                <button class="btn btn-pay-later">Pay Later</button>
            </div>
            <div class="row">
                <div class="col-8 m-t-40" style="padding-left: 55px">
                    <h6 style="font-size: 11px" class="m-b-0">Invoice For</h6>
                    <h5 style="font-size: 14px" class="text-success m-b-0"><?php echo $name ?></h5>
                    <address style="margin-top: 10px;white-space: pre-line;font-size: 10px"><b style="margin-right: 10px;">P</b> <?php echo $cell_phone ?>

                        <b style="margin-right: 10px;">E</b> <?php echo $to?>

                        <b style="margin-right: 10px;">A</b> <?php echo $full_address ?>
                    </address>
                </div>
                <div class="col-4" style="margin-top: 30px;padding-right: 55px;">
                    <address style="white-space: pre-line;font-size: 10px;">
                        Invoice No. <span style="font-weight: 100;float: right;"><?php echo $invoice_number?></span>
                        Invoice Date <span style="font-weight: 100;float: right;"><?php echo $invoice_date?></span>
                    </address>
                </div>
            </div>
            <div class="m-t-20 invoice-table">
                <table class="table ">
                    <thead>
                        <tr>
                            <th style="background-color: #26c6da;color: #fff;width: 415px;"><div class="p-10">Program Details</div></th>
                            <th class=""><div class="table-ribbon p-10">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspQuantity</div></th>
                            <th class=""><div class="table-ribbon2 p-10">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspTotal</div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                             if (count($month_lates) > 0) {
                                $late_fee = number_format(15 * count($month_lates),2);
                            } else {
                                $late_fee = 0;
                            }
                            $sub_total = number_format((count($month_list) * $monthly_fee) + $late_fee,2);
                        ?>
                        <?php foreach ($month_list as $key => $value): ?>
                            <tr>
                                <td><b><?php echo $plan ?></b><br>
                                    <small>
                                        <?php echo $value ?> Service 
                                        <?php if ($value != date('F')): ?>
                                            Overdue
                                        <?php endif ?>
                                        <?php if ($value == date('F')): ?>
                                            <?php if ($no_overdue_date != date('d')): ?>
                                                Overdue
                                            <?php endif ?>
                                        <?php endif ?> 
                                    </small>
                                </td>
                                <td>1</td>
                                <td>$<?php echo number_format($monthly_fee,2) ?></td>
                            </tr>
                        <?php endforeach ?> 
                        <?php if (count($month_lates) > 0): ?>
                            <tr>
                                <td><b>Late Fees</b><br>
                                    <small>Overdue <?php echo implode(', ', $month_lates); ?></small>
                                </td>
                                <td><?php echo count($month_lates) ?></td>
                                <td>$<?php echo $late_fee ?></td>
                            </tr>
                        <?php endif ?>
                            
                        <tr>
                            <td ></td>
                            <td>Sub Total</td>
                            <td>$<span class="sub_total"><?php echo $sub_total ?></span></td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px !important;"></td>
                            <td style="border-top: 0px !important;">Discount</td>
                            <td style="border-top: 0px !important;">-</td>
                        </tr>
                        <tr>
                            <td style="border-top: 0px !important;" colspan="3"></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th><div class="table-ribbon-footer p-10">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspBalance Due</div></th>
                            <th><div class="table-ribbon-footer2 p-10">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp$<span class="balance_due"><?php echo $sub_total ?></span></div></th>
                        </tr>
                    </tfoot>
                </table>

            </div>
            <div class="pay-container" style="margin-top: -55px" >
                <h6 style="font-size: 70%" class="m-b-0">Payment Method</h6>
                <img width="30px" src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-americanexpress1.png" alt="">
                <img width="30px" src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-visa.png" alt="">
                <img width="30px" src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-mastercard.png" alt="">
  
                
            </div>
            <div class="terms-container">
                <h5 style="font-size: 70%"><b style="font-weight: 700">Terms &</b> Conditions</h5>
                <p style="font-size: 70%;text-align: justify;">Per your signed Service Agreement, you understand and agree that your monthly fee is not a prepaid payment of Services; all accounts are billed in arrears (after Services have been performed) similar to how a utility bill is charged. By providing new or existing payment information, you authorize Credit Lynx to automatically process your fees either immediately upon receipt, or on a specified later date for the amount due, at Credit Lynx's discretion, until payment is captured and accepted. You understand that by providing any new payment information, Credit Lynx will keep all old payment information on file. You authorize Credit Lynx to charge any and/or all old payment information if fees are unable to be collected using the new payment information in order to keep your account(s) current since Services are billed in arrears.</p>
            </div>
        </div>
        <div class="bot-container">
            <div class="row">
                <div class="col-9">
                    <!-- <h3>
                        Thank you for<br>
                        Your Business
                    </h3> -->
                </div>
                <div class="col-3 text-center">
                    <img class="LynxLogo img-center" src="<?php echo base_url('assets/images/LynxLogo.png') ?>" width="50px" alt="">
                </div>
            </div>
        </div>
    </div>

<div id="modal_payment" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 

            <div class="modal-body">
                <div class="text-center">
                    <p>Pay balance due of $<span class="sub_total"><?php echo $sub_total ?></span></p>
                    <button class="btn btn-success p-l-10 p-r-10 btn_pay_now">Pay Now</button> 
                    <input type="text" name="" class="form-control input_pay_later_date material_date_time text-center" style="width: 150px" placeholder="MM/DD/YYYY" value=""  >
                    <br>
                    <button class="btn btn-success p-l-10 p-r-10 btn_pay_later m-t-10">Pay Later</button>
                    <p style="font-size: 70%" class="m-t-10 msg_pay_now text-default">By clicking "Pay Now", a payment of $<span class="sub_total"><?php echo $sub_total ?></span> will be charged using the card on file ending in <?php echo $card_number ?>.</p>
                    <p style="font-size: 70%" class="m-t-10 msg_pay_later text-default ">By clicking "Pay Later", you authorize Credit Lynx to process payment of $<?php echo $sub_total ?> on the selected date.</p>
                    <hr>
                    <p>Would you like to use a different card?</p>
                    <button class="btn btn-success p-l-10 p-r-10 btn_add_account">Add New Card</button>
                </div>
                <div class="row container_new_card hide m-t-20">
                    <div class="col-12 div_card_choices">
                        <div class="div_dc">
                                <!-- <span class="lynx_check pm_choice"></span> -->
                            <button type="button" class="btn btn-card-choice btn-circle pm_choice btn-sm lynx_check"></button>
                            <label class="pm_choice" style="margin-bottom: 0px;margin-left: 3px;line-height: 2.2;">Debit Card</label>
                            <div class="div_images">
                                     <img src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-visa.png" width="50px">
                                     <img src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-mastercard.png" width="50px">
                            </div>
                            <div class="divBillingInputs " >
                                     <form id="form_billing_inputs" class="hide animated fadeIn" method="POST" onsubmit="return false">
                                          <div class="row">
                                               <div class="col-sm-12 m-t-5">
                                                    <small class="hide lbl-sm">Cardholder Name</small>
                                                    <input required name="billing_card_holder" type="text" class="required form-control" placeholder="Cardholder Name">
                                               </div>
                                               <div class="col-sm-12 m-t-5 col-md-6 p-r-5">
                                                    <small class="hide lbl-sm" style="right: 11px;">Card Number</small>
                                                    <input required name="billing_card_number" type="text" class="required form-control ccn-inputmask" placeholder="Card Number"> 
                                               </div>
                                               <div class="col-sm-12 col-md-3 p-r-5 m-t-5 p-l-5 p-r-5">
                                                    <small class="hide lbl-sm" style="right: 11px;">Exp Date</small>
                                                    <input required name="billing_card_expiration" type="text" class="required form-control expiration-inputmask" placeholder="Exp Date">
                                               </div>
                                               <div class="col-sm-12 col-md-3 p-l-5 m-t-5">
                                                    <small class="hide" style="right: 22px;">CVV</small>
                                                    <input required name="billing_cvv_code" type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="4" class="required form-control" placeholder="CVV">
                                               </div>
                                               <div class="col-sm-12 m-b-5 m-t-5 text-center">
                                                    <label for="" class="text-danger hide billing_card_number_error">
                                                         Oh no, it looks like your card is ineligible. Here's why
                                                         <a href="#" class="btnHeresWhy"><i class="fa fa-info-circle text-success"></i> </a>
                                                    </label>
                                               </div>
                                                
                                               <div class="row divAddress" style="margin-left: 0px;margin-right: 0px;">
                                                    <div class="col-sm-12 m-t-5">
                                                         <small class="hide">Billing Address</small>
                                                         <input required type="text" name="billing_address" class="form-control required" placeholder="Address">
                                                    </div>
                                                    <div class="col-sm-12 m-t-5 col-md-6 p-r-5">
                                                         <small class="hide">City</small>
                                                         <input required type="text" name="billing_city" class="form-control required" placeholder="City" onkeypress="return ((event.charCode >= 65 &amp;&amp; event.charCode <= 90) || (event.charCode >= 97 &amp;&amp; event.charCode <= 122) || (event.charCode == 32))">
                                                    </div>
                                                    <div class="col-sm-12 m-t-5 col-md-3 p-l-5 p-r-5">
                                                         <select required name="billing_state" class="h-30 form-control required">
                                                              <option value="">Select State</option>
                                                              <option value="Alabama">Alabama</option>
                                                              <option value="Alaska">Alaska</option>
                                                              <option value="Arizona">Arizona</option>
                                                              <option value="Arkansas">Arkansas</option>
                                                              <option value="California">California</option>
                                                              <option value="Colorado">Colorado</option>
                                                              <option value="Connecticut">Connecticut</option>
                                                              <option value="Delaware">Delaware</option>
                                                              <option value="Florida">Florida</option>
                                                              <option value="Georgia">Georgia</option>
                                                              <option value="Hawaii">Hawaii</option>
                                                              <option value="Idaho">Idaho</option>
                                                              <option value="Illinois">Illinois</option>
                                                              <option value="Indiana">Indiana</option>
                                                              <option value="Iowa">Iowa</option>
                                                              <option value="Kansas">Kansas</option>
                                                              <option value="Kentucky">Kentucky</option>
                                                              <option value="Louisiana">Louisiana</option>
                                                              <option value="Maine">Maine</option>
                                                              <option value="Maryland">Maryland</option>
                                                              <option value="Massachusetts">Massachusetts</option>
                                                              <option value="Michigan">Michigan</option>
                                                              <option value="Minnesota">Minnesota</option>
                                                              <option value="Mississippi">Mississippi</option>
                                                              <option value="Missouri">Missouri</option>
                                                              <option value="Montana">Montana</option>
                                                              <option value="Nebraska">Nebraska</option>
                                                              <option value="Nevada">Nevada</option>
                                                              <option value="New Hampshire">New Hampshire</option>
                                                              <option value="New Jersey">New Jersey</option>
                                                              <option value="New Mexico">New Mexico</option>
                                                              <option value="New York">New York</option>
                                                              <option value="North Carolina">North Carolina</option>
                                                              <option value="North Dakota">North Dakota</option>
                                                              <option value="Ohio">Ohio</option>
                                                              <option value="Oklahoma">Oklahoma</option>
                                                              <option value="Oregon">Oregon</option>
                                                              <option value="Pennsylvania">Pennsylvania</option>
                                                              <option value="Rhode Island">Rhode Island</option>
                                                              <option value="South Carolina">South Carolina</option>
                                                              <option value="South Dakota">South Dakota</option>
                                                              <option value="Tennessee">Tennessee</option>
                                                              <option value="Texas">Texas</option>
                                                              <option value="Utah">Utah</option>
                                                              <option value="Vermont">Vermont</option>
                                                              <option value="Virginia">Virginia</option>
                                                              <option value="Washington">Washington</option>
                                                              <option value="West Virginia">West Virginia</option>
                                                              <option value="Wisconsin">Wisconsin</option>
                                                              <option value="Wyoming">Wyoming</option>
                                                         </select>
                                                    </div>
                                                    <div class="col-sm-12 m-t-5 col-md-3 p-l-5">
                                                         <small class="hide">Zip</small> <input required type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="5" name="billing_zip" class="form-control required" placeholder="Zip">
                                                    </div>
                                               </div>
                                          </div>
                                     </form> 
                            </div>
                        </div>
                        <div class="div_cc">
                                <!-- <span class="lynx_check pm_choice"></span> -->
                                <button type="button" class="btn btn-card-choice btn-circle pm_choice btn-sm lynx_check"></button>
                                <label class="pm_choice" style="margin-bottom: 0px;margin-left: 3px;line-height: 2.2;">Credit Card</label>
                                <div class="div_images">
                                     <img src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-americanexpress1.png" width="50px">
                                     <img src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-visa.png" width="50px">
                                     <img src="https://www.creditlynx.com/wp-new-signup/assets/icons/icon-mastercard.png" width="50px">
                                </div>
                                <div class="divBillingInputs ">
                                     
                                </div>
                        </div>
                    </div>
                    <div class="col-12 m-t-30"> 
                           <label style="font-size: 70%">
                                <button type="button" class="btn btn-card-choice btn-circle btn-sm btn_check_agreement"></button> 
                                <span style="line-height: 2.1">Yes, I have read and agree to the <u>Terms and Conditions</u> which is outlined in the Service Agreement.</span>
                           </label>
                    </div>
                    <div class="col-12">
                           <div class="row">
                                <div class="col-md-4 p-r-5 hide">
                                     <small class="" style="right: 11px;">Full Name</small>
                                     <input type="text" style="font-size: 70%" class="form-control required" disabled placeholder="Full Name" value="<?php echo $name ?>">
                                </div>
                                <div class="col-md-8 p-r-5">
                                     <!-- <small class="">Type <?php echo $name ?></small> -->
                                     <input type="text" style="font-size: 70%" name="personal_signature" class="form-control required" placeholder="Type: <?php echo $name ?>">
                                </div>
                                <div class="col-md-4 p-l-5">
                                     <small class="">Today's Date</small>
                                     <input type="text" style="font-size: 70%" class="form-control required" disabled value="<?php echo date('m/d/Y') ?>">
                                </div>
                           </div>
                           <div class="text-center m-t-20"> 
                                <button class="btn btn-success btn_pay_with_new_card">Pay with this card</button>
                                <button class="btn btn-success btn_pay_later_with_new_card">Pay Later with this card</button>
                           </div>
                    </div>
                 </div>
                 <!-- <div class="text-right"> 
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                 </div> -->
            </div> 
        </div>
    </div>
</div>


<script>

    $('.material_date_time').bootstrapMaterialDatePicker({ format: 'MM/DD/YYYY',minDate: new Date('<?php echo date('Y-m-d',strtotime($invoice_date. ' + 1 days')) ?>'), maxDate: new Date('<?php echo date('Y-m-d',strtotime($invoice_date. ' + 8 days')) ?>'),time: false });

    function get_empty_field() {
          var billing_card_holder = $('#form_billing_inputs').find('[name="billing_card_holder"]');
          var billing_card_number = $('#form_billing_inputs').find('[name="billing_card_number"]');
          var billing_card_expiration = $('#form_billing_inputs').find('[name="billing_card_expiration"]');
          var billing_cvv_code = $('#form_billing_inputs').find('[name="billing_cvv_code"]');
          var billing_address = $('#form_billing_inputs').find('[name="billing_address"]');
          var billing_city = $('#form_billing_inputs').find('[name="billing_city"]');
          var billing_state = $('#form_billing_inputs').find('[name="billing_state"]');
          var billing_zip = $('#form_billing_inputs').find('[name="billing_zip"]');
          var personal_signature = $('[name="personal_signature"]');

          var stop = false;

          stop = billing_card_holder.val() == '' ? billing_card_holder : false;
          stop = billing_card_number.val() == '' ? stop == false ? billing_card_number : stop : false;
          stop = billing_card_expiration.val() == '' ? stop == false ? billing_card_expiration : stop : false;
          stop = billing_cvv_code.val() == '' ? stop == false ? billing_cvv_code : stop : false;
          stop = billing_address.val() == '' ? stop == false ? billing_address : stop : false;
          stop = billing_city.val() == '' ? stop == false ? billing_city : stop : false;
          stop = billing_state.val() == '' ? stop == false ? billing_state : stop : false;
          stop = billing_zip.val() == '' ? stop == false ? billing_zip : stop : false;

          stop = personal_signature.val() != '<?php echo $name ?>' ? stop  == false ? personal_signature : stop : false; 
          return stop;
    }

    function get_invalid_field() {
          var stop = false;

          var billing_card_holder = $('#form_billing_inputs').find('[name="billing_card_holder"]');
          var billing_card_number = $('#form_billing_inputs').find('[name="billing_card_number"]');
          var billing_card_expiration = $('#form_billing_inputs').find('[name="billing_card_expiration"]');
          var billing_cvv_code = $('#form_billing_inputs').find('[name="billing_cvv_code"]');
          var billing_address = $('#form_billing_inputs').find('[name="billing_address"]');
          var billing_city = $('#form_billing_inputs').find('[name="billing_city"]');
          var billing_state = $('#form_billing_inputs').find('[name="billing_state"]');
          var billing_zip = $('#form_billing_inputs').find('[name="billing_zip"]'); 
 

          stop = billing_card_holder.hasClass('fieldInvalid') == true ? billing_card_holder : false;
          stop = stop == false ? billing_card_number.hasClass('fieldInvalid') ? billing_card_number : false : stop; 
          stop = stop == false ? billing_card_expiration.hasClass('fieldInvalid') ? billing_card_expiration : false : stop; 
          stop = stop == false ? billing_cvv_code.hasClass('fieldInvalid') ? billing_cvv_code : false : stop; 
          stop = stop == false ? billing_address.hasClass('fieldInvalid') ? billing_address : false : stop; 
          stop = stop == false ? billing_city.hasClass('fieldInvalid') ? billing_city : false : stop; 
          stop = stop == false ? billing_state.hasClass('fieldInvalid') ? billing_state : false : stop; 
          stop = stop == false ? billing_zip.hasClass('fieldInvalid') ? billing_zip : false : stop; 

          return stop;
    }
        
    $('.btn_add_account').on('click', function(event) {
        event.preventDefault();
        $('.container_new_card').removeClass('hide');
    });


    $('.form-control').on('change', function(event) {
        event.preventDefault(); 
        if ($(this).val() != '') { 
            $(this).closest('div').find('small').removeClass('hide'); 
          if (($(this).closest('div').find('small')).hasClass('text-danger')) {
               $(this).addClass('fieldInvalid');
          } else {
               $(this).removeClass('fieldInvalid');
          } 
        } else {
            $(this).closest('div').find('small').addClass('hide'); 
        }
    });

    $('.btn_check_agreement').on('click', function(event) {
          event.preventDefault();
          if ($(this).hasClass('active')) {
               $(this).removeClass('active');
               $(this).html('');
          } else {
               $(this).addClass('active');
               $(this).html('<i class="fas fa-check"></i>');
          }
     }); 
    $('.expiration-inputmask').inputmask("9999");
    $(".ccn-inputmask").inputmask("9999999999999999");
    
    $('.div_card_choices').on('click', '.lynx_check', function(event) {
          event.preventDefault();
          $('.div_card_choices .lynx_check').empty();
          $('.div_card_choices .lynx_check').removeClass('active');
          $(this).html('<i class="fas fa-check"></i>');
          $(this).addClass('active');
     });

    $('.pm_choice').on('click', function(event) {
        event.preventDefault();
        $('.div_card_choices .lynx_check').empty();
        $('.div_card_choices .lynx_check').removeClass('active');
        $(this).closest('div').find('.lynx_check').html('<i class="fas fa-check"></i>');
        $(this).closest('div').find('.lynx_check').addClass('active');                   
        $('#form_billing_inputs').appendTo($(this).closest('div').find('.divBillingInputs'));
        $('#form_billing_inputs').removeClass('hide'); 
    });


    $('.btn-pay-now').on('click', function(event) {
        event.preventDefault();
        $('#modal_payment').modal('show');
        $('.btn_pay_now').removeClass('hide');
        $('.btn_pay_later').addClass('hide');
        $('.input_pay_later_date').addClass('hide');
        $('.msg_pay_later').addClass('hide');
        $('.msg_pay_now').removeClass('hide');
        $('.btn_pay_later_with_new_card').addClass('hide');
        $('.btn_pay_with_new_card').removeClass('hide');
    });
    $('.btn-pay-later').on('click', function(event) {
        event.preventDefault();
        $('#modal_payment').modal('show');
        $('.btn_pay_later').removeClass('hide');
        $('.input_pay_later_date').removeClass('hide');
        $('.btn_pay_now').addClass('hide');
        $('.msg_pay_now').addClass('hide');
        $('.msg_pay_later').removeClass('hide');
        $('.btn_pay_with_new_card').addClass('hide');
        $('.btn_pay_later_with_new_card').removeClass('hide');
    });  

    $('[name="billing_card_number"]').on('change', function(event) {
        event.preventDefault();
        var input = $(this);
        validateCreditCard(input,input.val());

    });
    function validateCreditCard(input,card_number) {
        card_number = card_number.replace(/ /g,'');
        card_number = card_number.replace(/-/g,'');
        $.post('lookUpCardNumber', {card_number}, function(data, textStatus, xhr) {
            
            if (data != '') {
                data = JSON.parse(data); 
                console.log(data);
                if ($.isEmptyObject(data['prepaid'])) { 
                  // if (data['prepaid'] == true) {
                    // input.closest('div').find('small').html('Ineligible Card');
                    // input.closest('div').find('small').addClass('text-danger'); 
                    // input.addClass('fieldInvalid'); 
                    // $('.billing_card_number_error').removeClass('hide');
                  // } else {
                    input.closest('div').find('small').html('Cardholder Number');
                    input.closest('div').find('small').removeClass('text-danger'); 
                    input.removeClass('fieldInvalid'); 
                    $('.billing_card_number_error').addClass('hide'); 
                  // }  
                } else { 
                  if (data['prepaid'] == true) {
                    input.closest('div').find('small').html('Ineligible Card');
                    input.closest('div').find('small').addClass('text-danger'); 
                    input.addClass('fieldInvalid'); 
                    $('.billing_card_number_error').removeClass('hide');
                  } else {
                    input.closest('div').find('small').html('Cardholder Number');
                    input.closest('div').find('small').removeClass('text-danger'); 
                    input.removeClass('fieldInvalid'); 
                    $('.billing_card_number_error').addClass('hide'); 
                  } 
                }
            } else {
                input.closest('div').find('small').html('Ineligible Card');
                input.closest('div').find('small').addClass('text-danger'); 
                input.addClass('fieldInvalid'); 
                $('.billing_card_number_error').removeClass('hide');
            }   
        }); 
     }

    $('.btnHeresWhy').on('click', function(event) {
        event.preventDefault();
        
        var content = document.createElement('div');
        content.innerHTML = 'Please make sure that you are using a Visa or MasterCard debit card or American Express credit card tied to a bank account in the U.S. <strong>You will not be able to use gift cards, or prepaid cards, or cards linked to international accounts.</strong>';

        swal('Further Details','Please make sure that you are using a Visa or MasterCard debit card or American Express credit card tied to a bank account in the U.S. You will not be able to use gift cards, or prepaid cards, or cards linked to international accounts.','info');
     });
    
    
    $('.btn_pay_now').on('click', function(event) {
        event.preventDefault();
        var sub_total = $('.modal .sub_total').html(); 
        swal({
            title: "Payment Confirmation",
            text: "Payment will charge $"+sub_total+" to XXXX <?php echo $card_number ?> now",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Pay Now!",
            closeOnConfirm: true ,
            preConfirm: function() {
                var session = '<?php echo $session ?>';
                var fee = sub_total;
                $.post('<?php echo base_url('emailCampaign/pay_now') ?>', {session,fee}, function(data, textStatus, xhr) {
                    swal("Payment Success!","Thank you so much!",'success');
                    $('.modal_payment').modal('hide');
                        setTimeout(function() {
                            location.reload();
                        },3000);
                });
            }
        }); 
    });

    $('.btn_pay_with_new_card').on('click', function(event) { 

        if (!$('.pm_choice').hasClass('active')) {
               $('.pm_choice')[0].focus();
               return false;
        }
          
        var empty_field = get_empty_field();
 
        if (empty_field != false) {
               empty_field.focus();
               empty_field.addClass('fieldInvalid');
               return false;
        } 
 
        var invalid_field = get_invalid_field();
           
        if (invalid_field != false) {
               invalid_field.focus(); 
               invalid_field.addClass('fieldInvalid');
               return false;
        }  

        if (!$('.btn_check_agreement').hasClass('active')) {
               $('.btn_check_agreement').focus(); 
               $('.btn_check_agreement').addClass('btn_check_agreement_focused');
                return false;
        } else {
               $('.btn_check_agreement').removeClass('btn_check_agreement_focused');
        }
        
        var card_number = $('[name="billing_card_number"]').val();
        card_number = card_number.substr(card_number.length - 4);
        swal({
            title: "Payment Confirmation",
            text: "Payment will charge $<?php echo $sub_total ?> to XXXX "+card_number+" today",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Pay Later!",
            closeOnConfirm: true ,
            preConfirm: function() {
                var data = $('#form_billing_inputs').serializeArray();
                data.push({name: "session", value: '<?php echo $session ?>'});
                data.push({name: "fee", value: '<?php echo $sub_total ?>'});
                $.post('<?php echo base_url('emailCampaign/use_other_account') ?>', data, function(data, textStatus, xhr) {
                    swal("Payment Success!","Thank you so much!",'success');
                    $('.modal_payment').modal('hide');
                        setTimeout(function() {
                            location.reload();
                        },3000);
                });
            }
        });  
    });

    $('.btn_pay_later').on('click', function(event) {
        event.preventDefault();
        var pay_later_date = $('.input_pay_later_date').val();
        if (pay_later_date) {
            pay_later_date = moment(pay_later_date).format('MM/DD/YYYY');
            swal({
                title: "Payment Confirmation",
                text: "Your payment of $<?php echo $sub_total ?> will be charged on "+pay_later_date,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Pay Later!",
                closeOnConfirm: true ,
                preConfirm: function() {
                    var session = '<?php echo $session ?>'; 
                    
                    $.post('<?php echo base_url('emailCampaign/pay_later_date') ?>', {session,pay_later_date}, function(data, textStatus, xhr) { 
                        swal("Payment Scheduled on "+pay_later_date,"Thank you so much!",'success');
                        $('.modal_payment').modal('hide');
                        setTimeout(function() {
                            location.reload();
                        },3000);

                    });
                }
            }); 
        } else {
            $('.input_pay_later_date').focus();
        }
            
    });


    $('.btn_pay_later_with_new_card').on('click', function(event) { 

        var pay_later_date = $('.input_pay_later_date').val();
        if (pay_later_date) {
            pay_later_date = moment(pay_later_date).format('MM/DD/YYYY');
            if (!$('.pm_choice').hasClass('active')) {
                $('.pm_choice')[0].focus();
                return false;
            }
              
            var empty_field = get_empty_field();
 
            if (empty_field != false) {
                empty_field.focus();
                empty_field.addClass('fieldInvalid');
                return false;
            } 
     
            var invalid_field = get_invalid_field();
           
            if (invalid_field != false) {
                invalid_field.focus(); 
                invalid_field.addClass('fieldInvalid');
                return false;
            }  

            if (!$('.btn_check_agreement').hasClass('active')) {
                $('.btn_check_agreement').focus(); 
                $('.btn_check_agreement').addClass('btn_check_agreement_focused');
                return false;
            } else {
                $('.btn_check_agreement').removeClass('btn_check_agreement_focused');
            }
            
            var card_number = $('[name="billing_card_number"]').val();
            card_number = card_number.substr(card_number.length - 4);
            swal({
                title: "Payment Confirmation",
                text: "Your payment of $<?php echo $sub_total ?> will be charged on "+pay_later_date,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Pay Later!",
                closeOnConfirm: true ,
                preConfirm: function() {
                    var data = $('#form_billing_inputs').serializeArray();
                    data.push({name: "session", value: '<?php echo $session ?>'});
                    data.push({name: "pay_date", value: pay_later_date});
                    $.post('<?php echo base_url('emailCampaign/use_other_account') ?>', data, function(data, textStatus, xhr) {
                        swal("Payment Scheduled on "+pay_later_date,"Thank you so much!",'success');
                        $('.modal_payment').modal('hide');
                        setTimeout(function(){
                            location.reload();
                        },3000); 
                    });
                }
            });  
        } else {
            $('.input_pay_later_date').focus();
        }

            

              
    });

    
</script>

</body>
</html>