<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client_status_changes extends MY_Model {

	const DB_TABLE = 'client_status_changes';
    const DB_TABLE_PK = 'sc_id';

 	public $sc_id;
 	public $id;
 	public $type;
 	public $date_changed;
 	public $new_status; 
 	public $old_status;
 	public $status_type;

}

/* End of file Model_client_status_changes.php */
/* Location: ./application/models/Model_client_status_changes.php */