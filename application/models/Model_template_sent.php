<?php

class Model_template_sent extends MY_Model
{
    const DB_TABLE = 'template_sent';
    const DB_TABLE_PK = 'sent_id';

    public $sent_id;
    public $href;
	public $template_id;
	public $client_id;
	public $name1;
	public $email1;
	public $name2;
	public $email2;
	public $subject;
	public $message;
	public $date_sent;
	public $status;

}