<?php

class Model_client_enrollments extends MY_Model
{
    const DB_TABLE = 'client_enrollments';
    const DB_TABLE_PK = 'client_enrollment_id';

	public $client_enrollment_id;
	public $client_id;
	public $date_of_enrollment; 
	public $date_of_cancellation;
	public $reports_received;
	public $service_agreement;
	public $ss_proof_received;
	public $address_verification_received;
	public $address_verification_received_2;
	public $checkbox_account;
	public $checkbox_dropoff;
	public $checkbox_email;
	public $checkbox_mail; 



}