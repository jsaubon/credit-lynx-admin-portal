<?php

class Model_agent_emails extends MY_Model
{
    const DB_TABLE = 'agent_emails';
    const DB_TABLE_PK = 'email_id';

 	public $email_id;
 	public $agent_id;
 	public $type;
 	public $subject;
 	public $message; 
 	public $uid; 
 	public $date;
 	public $from;
}