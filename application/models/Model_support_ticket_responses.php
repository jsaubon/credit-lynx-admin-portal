<?php

class Model_support_ticket_responses extends MY_Model
{
    const DB_TABLE = 'support_ticket_responses';
    const DB_TABLE_PK = 'response_id';

 	public $response_id; 
 	public $support_id;
 	public $responder;
 	public $response_date; 
 	public $response; 
}