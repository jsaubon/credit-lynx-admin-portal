<?php

class Model_client_joints extends MY_Model {
	const DB_TABLE = 'client_joints';
	const DB_TABLE_PK = 'client_joint_id';

	public $client_joint_id;
	public $client_id;
	public $client_status;
	public $status_line;
	public $status_line_creditors;
	public $name;
	public $alt_phone;
	public $cell_phone;
	public $carrier;
	public $email_address;
	public $ss;
	public $address;
	public $city;
	public $state_province;
	public $zip_postal_code;
	public $date_of_birth;
	public $username;
	public $password;
	public $auto_alert;

}