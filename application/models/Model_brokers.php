<?php

class Model_brokers extends MY_Model
{
    const DB_TABLE = 'brokers';
    const DB_TABLE_PK = 'broker_id';

	public $broker_id;
	// public $agent_id;
	public $photo;
	public $first_name;
	public $last_name;
	public $company;
	public $address;
	public $city;
	public $state_province;
	public $zip_postal_code;
	public $cell;
	public $NMLS;
	public $office;
	public $email_address;
	public $username;
	public $password;
	public $fax;
	public $website;
	public $bio;



}