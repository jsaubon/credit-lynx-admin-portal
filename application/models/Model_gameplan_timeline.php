<?php

class Model_gameplan_timeline extends MY_Model
{
    const DB_TABLE = 'gameplan_timeline';
    const DB_TABLE_PK = 'timeline_id';

 	public $timeline_id;
 	public $enrolled; 
 	public $dispute; 
 	public $result; 
 	public $repeat; 
 	public $complete; 
 	public $client_id; 
 	public $client_type; 

 
}