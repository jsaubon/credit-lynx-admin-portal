<?php

class Model_templates extends MY_Model
{
    const DB_TABLE = 'templates';
    const DB_TABLE_PK = 'template_id';

	public $template_id;
	public $template_type;
	public $template_name;
	public $template_description;

}