<?php

class Model_client_statuses extends MY_Model
{
    const DB_TABLE = 'client_statuses';
    const DB_TABLE_PK = 'client_status_id';

	public $client_status_id;
	public $client_status; 
}