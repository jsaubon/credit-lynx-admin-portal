<?php

class Model_template_images extends MY_Model
{
    const DB_TABLE = 'template_images';
    const DB_TABLE_PK = 'ti_id';

	public $ti_id;
	public $template_id;
	public $image_name; 

}