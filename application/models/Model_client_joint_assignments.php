<?php

class Model_client_joint_assignments extends MY_Model {
	const DB_TABLE = 'client_joint_assignments';
	const DB_TABLE_PK = 'client_assigned_id';

	public $client_assigned_id;
	public $client_joint_id;
	public $agent_id;
	public $broker_id;
	public $sale_id;
	public $processor_id;

}