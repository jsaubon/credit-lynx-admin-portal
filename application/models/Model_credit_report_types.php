<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_credit_report_types extends MY_Model {
 
    const DB_TABLE = 'credit_report_types';
    const DB_TABLE_PK = 'cr_type_id';

 	public $cr_type_id;
 	public $cr_type;
 	
}
