<?php

class Model_appointments extends MY_Model
{
    const DB_TABLE = 'appointments';
    const DB_TABLE_PK = 'app_id';

 	public $app_id; 
	public $app_from;
	public $app_from_id;
	public $app_to;
	public $app_to_id;
	public $appointment;
	public $date_start;
	public $date_end;
	public $app_color; 
	public $app_phone;
	public $app_location;
	public $app_email; 
	public $app_note;
}