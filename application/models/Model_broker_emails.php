<?php

class Model_broker_emails extends MY_Model
{
    const DB_TABLE = 'broker_emails';
    const DB_TABLE_PK = 'email_id';

 	public $email_id;
 	public $broker_id;
 	public $type;
 	public $subject;
 	public $message; 
 	public $uid; 
 	public $date;
 	public $from;
}