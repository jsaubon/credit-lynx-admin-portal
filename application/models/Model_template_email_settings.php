<?php

class Model_template_email_settings extends MY_Model
{
    const DB_TABLE = 'template_email_settings';
    const DB_TABLE_PK = 'es_id';

 	public $es_id;
 	public $select_all_for_signers;
	public $for_signers_to_sign;
	public $for_signers_is_completed;
	public $for_signers_is_corrected;
	public $for_signers_declines_to_sign; 
	public $for_signers_is_vioded;
	public $select_all_for_sender;
	public $for_sender_is_completed;
	public $for_sender_view_envelope;
	public $for_sender_declines_to_sign;
	public $for_sender_to_recipient_fails;
}