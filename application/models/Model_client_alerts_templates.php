<?php

class Model_client_alerts_templates extends MY_Model
{
    const DB_TABLE = 'client_alerts_templates';
    const DB_TABLE_PK = 'alert_template_id';

	public $alert_template_id;
	public $template_shortname;
	public $template_subject;
	public $template_notes;

}