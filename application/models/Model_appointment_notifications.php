<?php

class Model_appointment_notifications extends MY_Model
{
    const DB_TABLE = 'appointment_notifications';
    const DB_TABLE_PK = 'an_id';

    public $an_id;
 	public $app_id;  
	public $notif_type;
	public $notif_time;
	public $notif_schedule;
}