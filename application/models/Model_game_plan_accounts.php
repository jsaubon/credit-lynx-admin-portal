<?php

class Model_game_plan_accounts extends MY_Model
{
    const DB_TABLE = 'game_plan_accounts';
    const DB_TABLE_PK = 'game_plan_accounts_id';

 	public $game_plan_accounts_id;
 	public $client_id;
 	public $installment;
 	public $revolving;
 	public $education;
 	public $recommendations;
 	public $account_type; 

 
}