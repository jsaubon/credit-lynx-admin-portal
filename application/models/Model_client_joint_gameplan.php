<?php

class Model_client_joint_gameplan extends MY_Model
{
    const DB_TABLE = 'client_joint_gameplan';
    const DB_TABLE_PK = 'gameplan_id';

    public $gameplan_id;
 	public $broker;
 	public $client_joint_id;
 	public $credit_lynx;
}