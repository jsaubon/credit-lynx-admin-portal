<?php

class Model_authorize_subscriptions extends MY_Model
{
    const DB_TABLE = 'authorize_subscriptions';
    const DB_TABLE_PK = 's_id';

 	public $s_id;
 	public $subscription_id;
 	public $name;
	public $status;
	public $date_created;
	public $full_name; 
	public $total_occurrences;
	public $monthly_due;
	public $past_occurrences;
	public $payment_method;
	public $account_number;
	public $invoice;
	public $amount;
	public $currency_code;
	public $last_payment_date;
}