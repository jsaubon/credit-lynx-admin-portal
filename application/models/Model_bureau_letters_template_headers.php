<?php

class Model_bureau_letters_template_headers extends MY_Model {
	const DB_TABLE = 'bureau_letters_template_headers';
	const DB_TABLE_PK = 'blth_id';

	public $blth_id;
	public $template_header;
}