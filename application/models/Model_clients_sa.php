<?php

class Model_clients_sa extends MY_Model {
	const DB_TABLE = 'clients_sa';
	const DB_TABLE_PK = 'sa_id';

	public $sa_id;
	public $id;
	public $type;
	public $service_agreement;

}