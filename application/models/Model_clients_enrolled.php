<?php

class Model_clients_enrolled extends MY_Model {
	const DB_TABLE = 'clients_enrolled';
	const DB_TABLE_PK = 'enrolled_id';

	public $enrolled_id;
	public $id;
	public $type;
	public $enrolled;

}