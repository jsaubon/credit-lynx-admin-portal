<?php

class Model_support_tickets extends MY_Model
{
    const DB_TABLE = 'support_tickets';
    const DB_TABLE_PK = 'support_id';

 	public $support_id; 
 	public $sender;
 	public $sender_id;
 	public $assigned_to; 
 	public $status;
 	public $subject; 
 	public $message; 
 	public $support_date; 
}