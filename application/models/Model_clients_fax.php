<?php

class Model_clients_fax extends MY_Model {
	const DB_TABLE = 'clients_fax';
	const DB_TABLE_PK = 'fax_id';

	public $fax_id;
	public $id;
	public $type;
	public $fax;

}