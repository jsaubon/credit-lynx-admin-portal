<?php

class Model_tasks extends MY_Model
{
    const DB_TABLE = 'tasks';
    const DB_TABLE_PK = 'task_id';

    public $task_id;
    public $task_assigned;
    public $task_assigned_id;
    public $task_assigned_name;
    public $sender; 
	public $sender_id;
	public $sender_photo;
	public $sender_name; 
	public $task;
	public $task_active;
	public $task_date;



}