<?php

class Model_subscription_list extends MY_Model
{
    const DB_TABLE = 'subscription_list';
    const DB_TABLE_PK = 'subs_id';

 	public $subs_id;
 	public $id;
 	public $type; 
 	public $date_changed; 
 	public $status; 
 
}