<?php

class Model_clients extends MY_Model {
	const DB_TABLE = 'clients';
	const DB_TABLE_PK = 'client_id';

	public $client_id;
	public $client_type;
	public $client_status;
	public $status_line;
	public $status_line_creditors;
	public $name;
	public $address;
	public $city;
	public $state_province;
	public $zip_postal_code;
	public $alt_phone;
	public $cell_phone;
	public $carrier;
	public $fax;
	public $email_address;
	public $ss;
	public $date_of_birth;
	public $date_for_result;
	public $date_for_result_creditors;
	public $username;
	public $password;
	public $font;
	public $date_created;
	public $converted;
	public $converted_date;
	public $auto_alert;
}