<?php

class Model_client_joint_account_details extends MY_Model
{
    const DB_TABLE = 'client_joint_account_details';
    const DB_TABLE_PK = 'account_id';

	 public $account_id;
	 public $client_joint_id;
	 public $bureaus;
	 public $account_name;
	 public $account_number; 
	 public $account_type;
	 public $amount;  
	 public $note;
	 public $past_due;
	 public $credit_limit;
	 public $status_date;
	 public $file_date;
	 public $chapter;
	 public $inquiry_date;
	 public $correct_info;
	 public $payment_status;
	 public $comments;
	 public $date_opened;
	 public $original_creditor; 
	 public $bureau_date;
	 public $date_added;
}