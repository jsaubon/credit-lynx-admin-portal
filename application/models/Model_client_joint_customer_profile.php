<?php

class Model_client_joint_customer_profile extends MY_Model
{
    const DB_TABLE = 'client_joint_customer_profile';
    const DB_TABLE_PK = 'acp_id';

 	public $acp_id;
 	public $client_joint_id;
 	public $customer_profile_id;
 	public $customer_payment_profile_id;
 	public $customer_address_id;
 	public $email; 
 	public $card_info; 
 	public $customer_address;
 	public $shipping_address;
 	public $date_added; 
}