<?php

class Model_call_templates extends MY_Model
{
    const DB_TABLE = 'call_templates';
    const DB_TABLE_PK = 'call_template_id';

	public $call_template_id;
	public $call_shortname;
	public $call_template; 
	public $call_template_type; 

}