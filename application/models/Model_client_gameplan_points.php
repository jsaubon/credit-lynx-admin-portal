<?php

class Model_client_gameplan_points extends MY_Model
{
    const DB_TABLE = 'client_gameplan_points';
    const DB_TABLE_PK = 'gameplan_points_id';

 	public $gameplan_points_id;
 	public $client_id;
 	public $equifax_points;
 	public $experian_points;
 	public $transunion_points;
 	public $credit_score;
 	public $eta_date;

 
}