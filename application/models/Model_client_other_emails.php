<?php

class Model_client_other_emails extends MY_Model {
	const DB_TABLE = 'client_other_emails';
	const DB_TABLE_PK = 'coe_id';

	public $coe_id;
	public $id;
	public $type;
	public $email_address;

}