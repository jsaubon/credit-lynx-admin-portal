<?php

class Model_chat_employees extends MY_Model
{
    const DB_TABLE = 'chat_employees';
    const DB_TABLE_PK = 'chat_id';

	 public $chat_id;
	 public $from_type;
	 public $from_name;
	 public $from_photo;
	 public $from_id; 
	 public $to_type;
	 public $to_name;
	 public $to_photo;
	 public $to_id;
	 public $message;
	 public $message_date;
}