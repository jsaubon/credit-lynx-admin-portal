<?php

class Model_task_templates extends MY_Model
{
    const DB_TABLE = 'task_templates';
    const DB_TABLE_PK = 'task_template_id';

	public $task_template_id;
	public $task_shortname;
	public $task_template; 

}