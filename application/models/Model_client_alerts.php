<?php

class Model_client_alerts extends MY_Model
{
    const DB_TABLE = 'client_alerts';
    const DB_TABLE_PK = 'alert_id';

 	public $alert_id;
 	public $client_id;
 	public $client_type;
	public $sender;
	public $sender_id;
	public $sender_photo;
	public $sender_name;
	public $alert_subject;
	public $alert_notes;
	public $alert_date;

}