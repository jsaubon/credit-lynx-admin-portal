<?php

class Model_note_templates extends MY_Model
{
    const DB_TABLE = 'note_templates';
    const DB_TABLE_PK = 'note_template_id';

	public $note_template_id;
	public $note_shortname;
	public $note_template; 

}