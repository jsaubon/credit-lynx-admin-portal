<?php

class Model_creditors_account extends MY_Model {
	const DB_TABLE = 'creditors_account';
	const DB_TABLE_PK = 'ca_id';

	public $ca_id;
	public $id;
	public $client_type;
	public $bureau;
	public $account_name;
	public $account_number;
	public $account_type;
	public $missing_balance;
	public $credit_limit;
	public $past_due;
	public $date_added;

}