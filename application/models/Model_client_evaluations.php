<?php

class Model_client_evaluations extends MY_Model
{
    const DB_TABLE = 'client_evaluations';
    const DB_TABLE_PK = 'eval_id';

 	public $eval_id;
 	public $id;
 	public $type;
 	public $question_1_yes;
	public $question_1_no;
	public $question_1a_yes;
	public $question_1a_no;
	public $question_2_yes;
	public $question_2_no;
	public $question_3_yes;
	public $question_3_no;
	public $question_3a_1to3;
	public $question_3a_4to9;
	public $question_3a_10plus;
	public $question_3b_yes;
	public $question_3b_no;
	public $question_4_yes;
	public $question_4_no;
	public $evaluation_rec;

 
}