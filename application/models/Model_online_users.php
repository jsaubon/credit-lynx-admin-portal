<?php

class Model_online_users extends MY_Model
{
    const DB_TABLE = 'online_users';
    const DB_TABLE_PK = 'online_id';

 	public $online_id;
 	public $user_type;
 	public $user_id;
 	public $ip_address;  
}