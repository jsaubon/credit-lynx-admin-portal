<?php

class Model_lead_joints extends MY_Model
{
    const DB_TABLE = 'lead_joints';
    const DB_TABLE_PK = 'lead_joint_id';

    public $lead_joint_id;
    public $lead_id;
     
	public $name; 
	public $cell_phone; 
	public $email_address; 
	public $date_of_birth;   

}