<?php

class Model_coupons extends MY_Model
{
    const DB_TABLE = 'coupons';
    const DB_TABLE_PK = 'coupon_id';

 public $coupon_id;
 public $coupon_code;
 public $limit;
 public $amount;
 public $date_start;
 public $date_end; 
}