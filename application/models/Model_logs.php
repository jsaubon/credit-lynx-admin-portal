<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_logs extends MY_Model {

	const DB_TABLE = 'logs';
	const DB_TABLE_PK = 'log_id';

	public $log_id;
	public $user_type;
	public $user_id;
	public $json_log;
	public $date;
	public $ip_address;

}

/* End of file Model_logs.php */
/* Location: ./application/models/Model_logs.php */