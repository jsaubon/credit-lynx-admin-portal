<?php

class Model_creditors_account_type extends MY_Model {
	const DB_TABLE = 'creditors_account_type';
	const DB_TABLE_PK = 'creditors_account_type_id';

	public $creditors_account_type_id;
	public $creditors_account_type;

}