<?php

class Model_client_joint_rounds extends MY_Model
{
    const DB_TABLE = 'client_joint_rounds';
    const DB_TABLE_PK = 'cr_id';

	public $cr_id;
	public $account_id; 
	public $dispute_1;
	public $result_1;
	public $dispute_2;
	public $result_2;
	public $dispute_3;
	public $result_3;
	public $dispute_4;
	public $result_4;
	public $dispute_5;
	public $result_5;
	public $dispute_6;
	public $result_6;
	public $dispute_7;
	public $result_7;
	public $dispute_8;
	public $result_8;
	public $dispute_9;
	public $result_9;


}