<?php

class Model_authorize_transactions extends MY_Model
{
    const DB_TABLE = 'authorize_transactions';
    const DB_TABLE_PK = 't_id';

 	public $t_id;
 	public $batch_id;
	public $transaction_id;
	public $name;
	public $account_number;
	public $status;
	public $amount;
	public $submitted_local;
}