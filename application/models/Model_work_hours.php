<?php

class Model_work_hours extends MY_Model
{
    const DB_TABLE = 'work_hours';
    const DB_TABLE_PK = 'work_id';

 	public $work_id;
 	public $worker;
	public $worker_id;
	public $start_time;
	public $stop_time; 

}