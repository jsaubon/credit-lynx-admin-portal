<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client_status_timelines extends MY_Model {

	const DB_TABLE = 'client_status_timelines';
    const DB_TABLE_PK = 'st_id';

 	public $st_id;
 	public $id;
 	public $type;
 	public $status_type;
 	public $status;
 	public $day;
 	public $subject; 
 	public $note;
 	public $notif_type;


}

/* End of file Model_client_status_timelines.php */
/* Location: ./application/models/Model_client_status_timelines.php */