<?php

class Model_calls extends MY_Model
{
    const DB_TABLE = 'calls';
    const DB_TABLE_PK = 'call_id';

    public $call_id;
    public $call_assigned;
    public $call_assigned_id;
    public $sender;
	public $sender_id;
	public $sender_photo;
	public $sender_name; 
	public $call;
	public $call_sticky;
	public $call_date; 
	public $call_type; 
}