<?php

class Model_client_emails extends MY_Model {
	const DB_TABLE = 'client_emails';
	const DB_TABLE_PK = 'email_id';

	public $email_id;
	public $client_id;
	public $client_type;
	public $type;
	public $subject;
	public $message;
	public $uid;
	public $date;
	public $from;
	public $to;
}