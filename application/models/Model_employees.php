<?php

class Model_employees extends MY_Model
{
    const DB_TABLE = 'employees';
    const DB_TABLE_PK = 'employee_id';

	public $employee_id;
	public $photo;
	public $name;
	public $job_title;
	public $address;
	public $city;
	public $state;
	public $zip_code;
	public $date_of_birth;
	public $email_address;
	public $phone_number;
	public $ss;
	public $salary;
	public $hourly;
	public $commission;
	public $bank_name;
	public $bank_address;
	public $account_number;
	public $routing_number; 
	public $emergency_name;
	public $emergency_phone;
	public $emergency_address;
	public $emergency_city;
	public $emergency_state;
	public $emergency_zip;
	public $emergency_relationship;
	public $username;
	public $password;
	public $status;
	public $pool;




}