<?php

class Model_signup_sessions extends MY_Model
{
    const DB_TABLE = 'signup_sessions';
    const DB_TABLE_PK = 'session_id';

 	public $session_id; 
 	public $session;
 	public $status;
 	public $id; 
 	public $type; 
}