<?php

class Model_clients_a2 extends MY_Model {
	const DB_TABLE = 'clients_a2';
	const DB_TABLE_PK = 'a2_id';

	public $a2_id;
	public $id;
	public $type;
	public $address2;

}