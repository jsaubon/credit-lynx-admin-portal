<?php

class Model_letter_creditors extends MY_Model
{
    const DB_TABLE = 'letter_creditors';
    const DB_TABLE_PK = 'creditor_id';

	public $creditor_id;
	public $name;
	public $address;
	public $city_state_zip; 
}