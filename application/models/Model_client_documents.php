<?php

class Model_client_documents extends MY_Model
{
    const DB_TABLE = 'client_documents';
    const DB_TABLE_PK = 'doc_id';

	 public $doc_id;
	 public $client_id;
	 public $category;
	 public $file_name;
	 public $file_download;
	 public $file_size; 
	 public $date_uploaded;
}