<?php

class Model_agent_etexts extends MY_Model
{
    const DB_TABLE = 'agent_etexts';
    const DB_TABLE_PK = 'text_id';

 	public $text_id;
 	public $agent_id;
 	public $type;
 	public $title;
 	public $text_message; 
 	public $uid; 
 	public $date;
 	public $phone_number;
}