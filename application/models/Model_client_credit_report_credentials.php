<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client_credit_report_credentials extends MY_Model {
 
    const DB_TABLE = 'client_credit_report_credentials';
    const DB_TABLE_PK = 'crc_id';

 	public $crc_id;
 	public $id;
 	public $type;
 	public $cr_type;
 	public $username; 
 	public $password;

 

}

/* End of file Model_client_credit_report_credentials.php */
/* Location: ./application/models/Model_client_credit_report_credentials.php */