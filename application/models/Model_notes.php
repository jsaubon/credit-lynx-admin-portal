<?php

class Model_notes extends MY_Model
{
    const DB_TABLE = 'notes';
    const DB_TABLE_PK = 'note_id';

    public $note_id;
    public $note_assigned;
    public $note_assigned_id;
    public $sender;
	public $sender_id;
	public $sender_photo;
	public $sender_name; 
	public $note;
	public $note_sticky;
	public $note_date; 
}