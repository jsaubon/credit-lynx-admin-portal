<?php

class Model_clients_cr extends MY_Model {
	const DB_TABLE = 'clients_cr';
	const DB_TABLE_PK = 'cr_id';

	public $cr_id;
	public $id;
	public $type;
	public $credit_report;

}