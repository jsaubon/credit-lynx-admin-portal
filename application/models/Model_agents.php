<?php

class Model_agents extends MY_Model
{
    const DB_TABLE = 'agents';
    const DB_TABLE_PK = 'agent_id';

	public $agent_id;
	public $photo;
	public $first_name;
	public $last_name;
	public $company;
	public $address;
	public $city;
	public $state_province;
	public $zip_postal_code;
	public $cell;
	public $office;
	public $email_address;
	public $username;
	public $password;
	public $fax;
	public $website;
	public $bio;


}