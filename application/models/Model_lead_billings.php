<?php

class Model_lead_billings extends MY_Model
{
    const DB_TABLE = 'lead_billings';
    const DB_TABLE_PK = 'lead_billing_id';

	public $lead_billing_id;
	public $lead_id;
	public $setup_fee;
	public $paid;
	public $monthly_fee;
	public $monthly_due;
	public $payment_method;
	public $bank;
	public $routing_number;
	public $account_number;
	public $aba_number;
	public $start_check;
	public $payment_link;
	public $account_holder;
	public $address;
	public $city;
	public $state;
	public $zip;


}