<?php

class Model_broker_etexts extends MY_Model
{
    const DB_TABLE = 'broker_etexts';
    const DB_TABLE_PK = 'text_id';

 	public $text_id;
 	public $broker_id;
 	public $type;
 	public $title;
 	public $text_message; 
 	public $uid; 
 	public $date;
 	public $phone_number;
}