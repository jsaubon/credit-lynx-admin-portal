<?php

class Model_client_etexts extends MY_Model {
	const DB_TABLE = 'client_etexts';
	const DB_TABLE_PK = 'text_id';

	public $text_id;
	public $client_id;
	public $client_type;
	public $type;
	public $title;
	public $text_message;
	public $uid;
	public $date;
	public $phone_number;
}