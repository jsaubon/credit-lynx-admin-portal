<?php

class Model_client_billings extends MY_Model
{
    const DB_TABLE = 'client_billings';
    const DB_TABLE_PK = 'client_billing_id';

	public $client_billing_id;
	public $client_id;
	public $coupon_code;
	public $plan;
	public $setup_fee;
	public $paid;
	public $monthly_fee;
	public $monthly_due;
	public $payment_method;
	public $card_number;
	public $card_expiration;
	public $cvv_code;
	public $card_holder;
	public $note; 
	public $address;
	public $city;
	public $state;
	public $zip;


}