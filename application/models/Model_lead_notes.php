<?php

class Model_lead_notes extends MY_Model
{
    const DB_TABLE = 'lead_notes';
    const DB_TABLE_PK = 'note_id';

 	public $note_id;
	public $lead_id;
	public $sender;
	public $sender_id;
	public $sender_photo;
	public $sender_name;
	public $note_type;
	public $note; 
	public $note_date;
}