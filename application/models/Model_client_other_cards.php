<?php

class Model_client_other_cards extends MY_Model {
	const DB_TABLE = 'client_other_cards';
	const DB_TABLE_PK = 'coc_id';

	public $coc_id;
	public $id;
	public $type;
	public $card_number;
	public $card_exp;
	public $card_cvv;
	public $shipping_address;
	public $shipping_city;
	public $shipping_state;
	public $shipping_zip;

}