<?php

class Model_campaigns extends MY_Model
{
    const DB_TABLE = 'campaigns';
    const DB_TABLE_PK = 'campaign_id';

 	public $campaign_id;
 	public $campaign_name;
 	public $campaign_type;
 	public $status;
 	public $date_updated; 
 	public $active;

 
}