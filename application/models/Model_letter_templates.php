<?php

class Model_letter_templates extends MY_Model
{
    const DB_TABLE = 'letter_templates';
    const DB_TABLE_PK = 'blt_id';

	public $blt_id;
	public $template_header;
	public $template_title;
	public $template; 
}