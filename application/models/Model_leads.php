<?php

class Model_leads extends MY_Model
{
    const DB_TABLE = 'leads';
    const DB_TABLE_PK = 'lead_id';

    public $lead_id;
    public $agent_id;
	public $broker_id;
	public $sale_id;

	public $name;
	public $lead_type;
	public $lead_status;
	public $address;
	public $city;
	public $state_province;
	public $zip_postal_code;
	public $phone;
	public $cell_phone;
	public $alt_phone;
	public $carrier;
	public $email_address; 
	public $date_of_birth; 
	public $date_entered;  

}